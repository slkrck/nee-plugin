/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.catchup.CatchupComponentsFactory;
import io.nee.causalclustering.common.ClusterMonitors;
import io.nee.causalclustering.core.state.BootstrapContext;
import io.nee.causalclustering.core.state.ClusterStateLayout;
import io.nee.causalclustering.core.state.CoreEditionKernelComponents;
import io.nee.causalclustering.core.state.CoreKernelResolvers;
import io.nee.causalclustering.core.state.snapshot.StoreDownloadContext;
import io.nee.dbms.database.ClusteredDatabaseContext;
import io.nee.dbms.database.ClusteredMultiDatabaseManager;
import java.io.File;
import java.io.IOException;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.api.DatabaseManagementException;
import org.neo4j.dbms.database.DatabaseConfig;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.graphdb.factory.module.ModularDatabaseCreationContext;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.FileUtils;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.io.pagecache.tracing.cursor.context.VersionContextSupplier;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.DatabaseCreationContext;
import org.neo4j.kernel.database.DatabaseNameLogContext;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.DatabaseLogService;
import org.neo4j.monitoring.Monitors;

public final class CoreDatabaseManager extends ClusteredMultiDatabaseManager {

  protected final CoreEditionModule edition;

  CoreDatabaseManager(GlobalModule globalModule, CoreEditionModule edition,
      CatchupComponentsFactory catchupComponentsFactory, FileSystemAbstraction fs,
      PageCache pageCache, LogProvider logProvider, Config config,
      ClusterStateLayout clusterStateLayout) {
    super(globalModule, edition, catchupComponentsFactory, fs, pageCache, logProvider, config,
        clusterStateLayout);
    this.edition = edition;
  }

  protected ClusteredDatabaseContext createDatabaseContext(NamedDatabaseId namedDatabaseId) {
    LifeSupport clusterComponents = new LifeSupport();
    Dependencies coreDatabaseDependencies = new Dependencies(
        this.globalModule.getGlobalDependencies());
    DatabaseLogService coreDatabaseLogService = new DatabaseLogService(
        new DatabaseNameLogContext(namedDatabaseId), this.globalModule.getLogService());
    Monitors coreDatabaseMonitors = ClusterMonitors
        .create(this.globalModule.getGlobalMonitors(), coreDatabaseDependencies);
    DatabaseLayout databaseLayout = this.globalModule.getNeo4jLayout()
        .databaseLayout(namedDatabaseId.name());
    LogFiles transactionLogs = this.buildTransactionLogs(databaseLayout);
    BootstrapContext bootstrapContext = new BootstrapContext(namedDatabaseId, databaseLayout,
        this.storeFiles, transactionLogs);
    CoreRaftContext raftContext =
        this.edition.coreDatabaseFactory()
            .createRaftContext(namedDatabaseId, clusterComponents, coreDatabaseMonitors,
                coreDatabaseDependencies,
                bootstrapContext, coreDatabaseLogService);
    DatabaseConfig databaseConfig = new DatabaseConfig(this.config, namedDatabaseId);
    VersionContextSupplier versionContextSupplier = this
        .createVersionContextSupplier(databaseConfig);
    CoreKernelResolvers kernelResolvers = new CoreKernelResolvers();
    CoreEditionKernelComponents kernelContext =
        this.edition.coreDatabaseFactory()
            .createKernelComponents(namedDatabaseId, clusterComponents, raftContext,
                kernelResolvers,
                coreDatabaseLogService, versionContextSupplier);
    DatabaseCreationContext databaseCreationContext =
        this.newDatabaseCreationContext(namedDatabaseId, coreDatabaseDependencies,
            coreDatabaseMonitors, kernelContext, versionContextSupplier,
            databaseConfig, coreDatabaseLogService);
    Database kernelDatabase = new Database(databaseCreationContext);
    StoreDownloadContext downloadContext = new StoreDownloadContext(kernelDatabase, this.storeFiles,
        transactionLogs, this.internalDbmsOperator());
    CoreDatabase coreDatabase =
        this.edition.coreDatabaseFactory()
            .createDatabase(namedDatabaseId, clusterComponents, coreDatabaseMonitors,
                coreDatabaseDependencies,
                downloadContext, kernelDatabase, kernelContext, raftContext,
                this.internalDbmsOperator());
    ClusteredDatabaseContext ctx =
        this.contextFactory
            .create(kernelDatabase, kernelDatabase.getDatabaseFacade(), transactionLogs,
                this.storeFiles, this.logProvider,
                this.catchupComponentsFactory, coreDatabase, coreDatabaseMonitors);
    kernelResolvers.registerDatabase(ctx.database());
    return ctx;
  }

  private DatabaseCreationContext newDatabaseCreationContext(NamedDatabaseId namedDatabaseId,
      Dependencies parentDependencies, Monitors parentMonitors,
      CoreEditionKernelComponents kernelComponents, VersionContextSupplier versionContextSupplier,
      DatabaseConfig databaseConfig,
      DatabaseLogService databaseLogService) {
    Config config = this.globalModule.getGlobalConfig();
    CoreDatabaseComponents coreDatabaseComponents = new CoreDatabaseComponents(config, this.edition,
        kernelComponents, databaseLogService);
    GlobalProcedures globalProcedures = this.edition.getGlobalProcedures();
    return new ModularDatabaseCreationContext(namedDatabaseId, this.globalModule,
        parentDependencies, parentMonitors, coreDatabaseComponents,
        globalProcedures, versionContextSupplier, databaseConfig, kernelComponents.leaseService());
  }

  protected void dropDatabase(NamedDatabaseId namedDatabaseId, ClusteredDatabaseContext context) {
    super.dropDatabase(namedDatabaseId, context);
    this.cleanupClusterState(namedDatabaseId.name());
  }

  public void cleanupClusterState(String databaseName) {
    try {
      this.deleteCoreStateThenRaftId(databaseName);
    } catch (IOException n3) {
      throw new DatabaseManagementException("Was unable to delete cluster state as part of drop. ",
          n3);
    }
  }

  private void deleteCoreStateThenRaftId(String databaseName) throws IOException {
    File raftIdState = this.clusterStateLayout.raftIdStateFile(databaseName);
    File raftIdStateDir = raftIdState.getParentFile();
    File raftGroupDir = this.clusterStateLayout.raftGroupDir(databaseName);
    if (this.fs.fileExists(raftGroupDir)) {
      assert raftIdStateDir.getParentFile().equals(raftGroupDir);

      File[] files = this.fs.listFiles(raftGroupDir, (ignored, name) ->
      {
        return !raftIdStateDir.getName().equals(name);
      });
      File[] n6 = files;
      int n7 = files.length;

      for (int n8 = 0; n8 < n7; ++n8) {
        File file = n6[n8];
        if (file.isDirectory()) {
          this.fs.deleteRecursively(file);
        } else if (!this.fs.deleteFile(file)) {
          throw new IOException(String
              .format("Unable to delete file %s when dropping database %s", file.getAbsolutePath(),
                  databaseName));
        }
      }

      FileUtils.tryForceDirectory(raftGroupDir);
      if (files.length != 0 && !this.fs.fileExists(raftIdState)) {
        this.log.warn(
            String.format(
                "The cluster state directory for %s is non-empty but does not contain a raftId. Cleanup has still been attempted.",
                databaseName));
      }

      if (!this.fs.deleteFile(raftIdState)) {
        throw new IOException(String.format("Unable to delete file %s when dropping database %s",
            raftIdState.getAbsolutePath(), databaseName));
      } else {
        this.fs.deleteRecursively(raftGroupDir);
      }
    }
  }
}
