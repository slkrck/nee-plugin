/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.helper.TemporaryDatabase;
import io.nee.causalclustering.helper.TemporaryDatabaseFactory;
import io.nee.dbms.api.EnterpriseDatabaseManagementServiceBuilder;
import io.nee.kernel.impl.enterprise.configuration.MetricsSettings;
import io.nee.kernel.impl.enterprise.configuration.OnlineBackupSettings;
import java.io.File;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.dbms.api.DatabaseManagementServiceBuilder;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.ExternallyManagedPageCache;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.logging.NullLogProvider;

public class EnterpriseTemporaryDatabaseFactory implements TemporaryDatabaseFactory {

  private final PageCache pageCache;
  private final FileSystemAbstraction fileSystem;

  public EnterpriseTemporaryDatabaseFactory(PageCache pageCache, FileSystemAbstraction fileSystem) {
    this.pageCache = pageCache;
    this.fileSystem = fileSystem;
  }

  private static void augmentConfig(DatabaseManagementServiceBuilder managementServiceBuilder,
      Config originalConfig, File rootDirectory) {
    managementServiceBuilder.setConfig(GraphDatabaseSettings.record_format,
        originalConfig.get(GraphDatabaseSettings.record_format));
    managementServiceBuilder.setConfig(GraphDatabaseSettings.databases_root_path,
        rootDirectory.toPath().toAbsolutePath());
    managementServiceBuilder.setConfig(GraphDatabaseSettings.transaction_logs_root_path,
        rootDirectory.toPath().toAbsolutePath());
    managementServiceBuilder.setConfig(GraphDatabaseSettings.pagecache_warmup_enabled, false);
    managementServiceBuilder.setConfig(OnlineBackupSettings.online_backup_enabled, false);
    managementServiceBuilder.setConfig(MetricsSettings.metricsEnabled, false);
    managementServiceBuilder.setConfig(MetricsSettings.csvEnabled, false);
  }

  public TemporaryDatabase startTemporaryDatabase(File rootDirectory, Config originalConfig) {
    Dependencies dependencies = new Dependencies();
    dependencies.satisfyDependency(new ExternallyManagedPageCache(this.pageCache));
    dependencies.satisfyDependency(this.fileSystem);
    DatabaseManagementServiceBuilder managementServiceBuilder =
        (new EnterpriseDatabaseManagementServiceBuilder(rootDirectory))
            .setUserLogProvider(NullLogProvider.getInstance()).setExternalDependencies(
            dependencies);
    augmentConfig(managementServiceBuilder, originalConfig, rootDirectory);
    return new TemporaryDatabase(managementServiceBuilder.build());
  }
}
