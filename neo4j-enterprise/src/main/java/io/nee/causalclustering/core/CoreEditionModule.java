/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.catchup.CatchupComponentsProvider;
import io.nee.causalclustering.catchup.CatchupServerHandler;
import io.nee.causalclustering.catchup.MultiDatabaseCatchupServerHandler;
import io.nee.causalclustering.common.ClusteringEditionModule;
import io.nee.causalclustering.common.PipelineBuilders;
import io.nee.causalclustering.common.state.ClusterStateStorageFactory;
import io.nee.causalclustering.core.consensus.LeaderLocator;
import io.nee.causalclustering.core.consensus.RaftGroupFactory;
import io.nee.causalclustering.core.consensus.protocol.v2.RaftProtocolClientInstallerV2;
import io.nee.causalclustering.core.state.ClusterStateLayout;
import io.nee.causalclustering.core.state.ClusterStateMigrator;
import io.nee.causalclustering.core.state.DiscoveryModule;
import io.nee.causalclustering.core.state.storage.SimpleStorage;
import io.nee.causalclustering.core.state.version.ClusterStateVersion;
import io.nee.causalclustering.diagnostics.GlobalTopologyStateDiagnosticProvider;
import io.nee.causalclustering.diagnostics.RaftMonitor;
import io.nee.causalclustering.discovery.CoreTopologyService;
import io.nee.causalclustering.discovery.DiscoveryServiceFactory;
import io.nee.causalclustering.discovery.member.DefaultDiscoveryMemberFactory;
import io.nee.causalclustering.discovery.member.DiscoveryMemberFactory;
import io.nee.causalclustering.discovery.procedures.ClusterOverviewProcedure;
import io.nee.causalclustering.discovery.procedures.CoreRoleProcedure;
import io.nee.causalclustering.discovery.procedures.InstalledProtocolsProcedure;
import io.nee.causalclustering.error_handling.PanicService;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.logging.BetterRaftMessageLogger;
import io.nee.causalclustering.logging.NullRaftMessageLogger;
import io.nee.causalclustering.logging.RaftMessageLogger;
import io.nee.causalclustering.messaging.RaftChannelPoolService;
import io.nee.causalclustering.messaging.RaftSender;
import io.nee.causalclustering.net.BootstrapConfiguration;
import io.nee.causalclustering.net.InstalledProtocolHandler;
import io.nee.causalclustering.net.Server;
import io.nee.causalclustering.protocol.ModifierProtocolInstaller;
import io.nee.causalclustering.protocol.ProtocolInstaller;
import io.nee.causalclustering.protocol.ProtocolInstallerRepository;
import io.nee.causalclustering.protocol.application.ApplicationProtocols;
import io.nee.causalclustering.protocol.handshake.ApplicationProtocolRepository;
import io.nee.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import io.nee.causalclustering.protocol.handshake.HandshakeClientInitializer;
import io.nee.causalclustering.protocol.handshake.ModifierProtocolRepository;
import io.nee.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import io.nee.causalclustering.protocol.handshake.ProtocolStack;
import io.nee.causalclustering.protocol.init.ClientChannelInitializer;
import io.nee.causalclustering.protocol.modifier.ModifierProtocols;
import io.nee.causalclustering.routing.load_balancing.DefaultLeaderService;
import io.nee.causalclustering.routing.load_balancing.LeaderLocatorForDatabase;
import io.nee.causalclustering.routing.load_balancing.LeaderService;
import io.nee.dbms.ClusterSystemGraphDbmsModel;
import io.nee.dbms.ClusterSystemGraphInitializer;
import io.nee.dbms.ClusteredDbmsReconcilerModule;
import io.nee.dbms.DatabaseStartAborter;
import io.nee.dbms.SystemDbOnlyReplicatedDatabaseEventService;
import io.nee.dbms.database.ClusteredDatabaseContext;
import io.nee.dbms.procedures.ClusteredDatabaseStateProcedure;
import io.nee.enterprise.edition.AbstractEnterpriseEditionModule;
import io.nee.kernel.enterprise.api.security.provider.EnterpriseNoAuthSecurityProvider;
import io.nee.procedure.enterprise.builtin.EnterpriseBuiltInDbmsProcedures;
import io.nee.procedure.enterprise.builtin.EnterpriseBuiltInProcedures;
import io.nee.procedure.enterprise.builtin.SettingsWhitelist;
import io.nee.server.security.enterprise.EnterpriseSecurityModule;
import io.netty.channel.socket.SocketChannel;
import java.io.File;
import java.time.Duration;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.cypher.internal.javacompat.EnterpriseCypherEngineProvider;
import org.neo4j.dbms.DatabaseStateService;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.dbms.database.SystemGraphInitializer;
import org.neo4j.exceptions.KernelException;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.module.DatabaseInitializer;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.graphdb.factory.module.edition.CommunityEditionModule;
import org.neo4j.graphdb.factory.module.edition.context.EditionDatabaseComponents;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import org.neo4j.kernel.api.security.provider.SecurityProvider;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.DatabaseStartupController;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.query.QueryEngineProvider;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.kernel.recovery.Recovery;
import org.neo4j.kernel.recovery.RecoveryFacade;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.LogService;
import org.neo4j.procedure.builtin.routing.BaseRoutingProcedureInstaller;
import org.neo4j.ssl.config.SslPolicyLoader;

public class CoreEditionModule extends ClusteringEditionModule implements
    AbstractEnterpriseEditionModule {

  private final IdentityModule identityModule;
  private final SslPolicyLoader sslPolicyLoader;
  private final ClusterStateStorageFactory storageFactory;
  private final ClusterStateLayout clusterStateLayout;
  private final CatchupComponentsProvider catchupComponentsProvider;
  private final DiscoveryServiceFactory discoveryServiceFactory;
  private final PanicService panicService;
  private final PipelineBuilders pipelineBuilders;
  private final Supplier<Stream<Pair<SocketAddress, ProtocolStack>>> clientInstalledProtocols;
  private final Supplier<Stream<Pair<SocketAddress, ProtocolStack>>> serverInstalledProtocols;
  private final ApplicationSupportedProtocols supportedRaftProtocols;
  private final Collection<ModifierSupportedProtocols> supportedModifierProtocols;
  private final InstalledProtocolHandler serverInstalledProtocolHandler;
  private final Map<NamedDatabaseId, DatabaseInitializer> databaseInitializerMap = new HashMap();
  private final LogProvider logProvider;
  private final Config globalConfig;
  private final GlobalModule globalModule;
  private final EnterpriseTemporaryDatabaseFactory temporaryDatabaseFactory;
  private final RaftSender raftSender;
  private CoreDatabaseFactory coreDatabaseFactory;
  private CoreTopologyService topologyService;
  private DatabaseStartAborter databaseStartAborter;
  private ClusteredDbmsReconcilerModule reconcilerModule;

  public CoreEditionModule(GlobalModule globalModule,
      DiscoveryServiceFactory discoveryServiceFactory) {
    super(globalModule);
    Dependencies globalDependencies = globalModule.getGlobalDependencies();
    LogService logService = globalModule.getLogService();
    LifeSupport globalLife = globalModule.getGlobalLife();
    this.globalModule = globalModule;
    this.globalConfig = globalModule.getGlobalConfig();
    this.logProvider = logService.getInternalLogProvider();
    SettingsWhitelist settingsWhiteList = new SettingsWhitelist(this.globalConfig);
    globalDependencies.satisfyDependency(settingsWhiteList);
    RaftMonitor
        .register(logService, globalModule.getGlobalMonitors(), globalModule.getGlobalClock());
    FileSystemAbstraction fileSystem = globalModule.getFileSystem();
    File dataDir = this.globalConfig.get(GraphDatabaseSettings.data_directory).toFile();
    this.clusterStateLayout = ClusterStateLayout.of(dataDir);
    globalDependencies.satisfyDependency(this.clusterStateLayout);
    this.storageFactory = new ClusterStateStorageFactory(fileSystem, this.clusterStateLayout,
        this.logProvider, this.globalConfig);
    ClusterStateMigrator clusterStateMigrator = createClusterStateMigrator(globalModule,
        this.clusterStateLayout, this.storageFactory);
    globalLife.add(clusterStateMigrator);
    this.temporaryDatabaseFactory = new EnterpriseTemporaryDatabaseFactory(
        globalModule.getPageCache(), globalModule.getFileSystem());
    this.panicService = new PanicService(globalModule.getJobScheduler(), logService);
    globalDependencies.satisfyDependencies(this.panicService);
    this.watcherServiceFactory = (layout) ->
    {
      return this.createDatabaseFileSystemWatcher(globalModule.getFileWatcher(), layout, logService,
          fileWatcherFileNameFilter());
    };
    this.identityModule = new IdentityModule(globalModule, this.storageFactory);
    this.discoveryServiceFactory = discoveryServiceFactory;
    this.sslPolicyLoader = SslPolicyLoader.create(this.globalConfig, this.logProvider);
    globalDependencies.satisfyDependency(this.sslPolicyLoader);
    this.pipelineBuilders = new PipelineBuilders(this.sslPolicyLoader);
    this.catchupComponentsProvider = new CatchupComponentsProvider(globalModule,
        this.pipelineBuilders);
    SupportedProtocolCreator supportedProtocolCreator = new SupportedProtocolCreator(
        this.globalConfig, this.logProvider);
    this.supportedRaftProtocols = supportedProtocolCreator
        .getSupportedRaftProtocolsFromConfiguration();
    this.supportedModifierProtocols = supportedProtocolCreator.createSupportedModifierProtocols();
    RaftChannelPoolService raftChannelPoolService = this.buildRaftChannelPoolService(globalModule);
    globalLife.add(raftChannelPoolService);
    Objects.requireNonNull(raftChannelPoolService);
    this.clientInstalledProtocols = raftChannelPoolService::installedProtocols;
    this.serverInstalledProtocolHandler = new InstalledProtocolHandler();
    InstalledProtocolHandler n10001 = this.serverInstalledProtocolHandler;
    Objects.requireNonNull(n10001);
    this.serverInstalledProtocols = n10001::installedProtocols;
    this.raftSender = new RaftSender(this.logProvider, raftChannelPoolService);
    this.satisfyEnterpriseOnlyDependencies(this.globalModule);
    this.editionInvariants(globalModule, globalDependencies);
  }

  private static RaftMessageLogger<MemberId> createRaftLogger(GlobalModule globalModule,
      MemberId myself) {
    Config config = globalModule.getGlobalConfig();
    Object raftMessageLogger;
    if (config.get(CausalClusteringSettings.raft_messages_log_enable)) {
      File logFile = config.get(CausalClusteringSettings.raft_messages_log_path).toFile();
      BetterRaftMessageLogger<MemberId> logger =
          new BetterRaftMessageLogger(myself, logFile, globalModule.getFileSystem(),
              globalModule.getGlobalClock());
      raftMessageLogger = globalModule.getGlobalLife().add(logger);
    } else {
      raftMessageLogger = new NullRaftMessageLogger();
    }

    return (RaftMessageLogger) raftMessageLogger;
  }

  private static ClusterStateMigrator createClusterStateMigrator(GlobalModule globalModule,
      ClusterStateLayout clusterStateLayout,
      ClusterStateStorageFactory storageFactory) {
    SimpleStorage<ClusterStateVersion> clusterStateVersionStorage = storageFactory
        .createClusterStateVersionStorage();
    FileSystemAbstraction fs = globalModule.getFileSystem();
    LogProvider logProvider = globalModule.getLogService().getInternalLogProvider();
    return new ClusterStateMigrator(fs, clusterStateLayout, clusterStateVersionStorage,
        logProvider);
  }

  private void createCoreServers(LifeSupport life, DatabaseManager<?> databaseManager,
      FileSystemAbstraction fileSystem) {
    int maxChunkSize = this.globalConfig.get(CausalClusteringSettings.store_copy_chunk_size);
    CatchupServerHandler catchupServerHandler = new MultiDatabaseCatchupServerHandler(
        databaseManager, fileSystem, maxChunkSize, this.logProvider);
    Server catchupServer = this.catchupComponentsProvider
        .createCatchupServer(this.serverInstalledProtocolHandler, catchupServerHandler);
    life.add(catchupServer);
    this.globalModule.getGlobalDependencies().satisfyDependencies(catchupServer);
    Optional<Server> optionalBackupServer = this.catchupComponentsProvider
        .createBackupServer(this.serverInstalledProtocolHandler, catchupServerHandler);
    if (optionalBackupServer.isPresent()) {
      Server backupServer = optionalBackupServer.get();
      life.add(backupServer);
    }
  }

  public QueryEngineProvider getQueryEngineProvider() {
    return new EnterpriseCypherEngineProvider();
  }

  public EditionDatabaseComponents createDatabaseComponents(NamedDatabaseId namedDatabaseId) {
    throw new UnsupportedOperationException("TODO");
  }

  public void registerEditionSpecificProcedures(GlobalProcedures globalProcedures,
      DatabaseManager<?> databaseManager) throws KernelException {
    globalProcedures.registerProcedure(EnterpriseBuiltInDbmsProcedures.class, true);
    globalProcedures.registerProcedure(EnterpriseBuiltInProcedures.class, true);
    globalProcedures.register(
        new ClusterOverviewProcedure(this.topologyService, databaseManager.databaseIdRepository()));
    globalProcedures.register(new CoreRoleProcedure(databaseManager));
    globalProcedures.register(
        new ClusteredDatabaseStateProcedure(databaseManager.databaseIdRepository(),
            this.topologyService, this.reconcilerModule.reconciler()));
    globalProcedures.register(new InstalledProtocolsProcedure(this.clientInstalledProtocols,
        this.serverInstalledProtocols));
  }

  protected BaseRoutingProcedureInstaller createRoutingProcedureInstaller(GlobalModule globalModule,
      DatabaseManager<?> databaseManager) {
    LeaderLocatorForDatabase leaderLocatorForDatabase = (databaseId) ->
    {
      return databaseManager.getDatabaseContext(databaseId).map(DatabaseContext::dependencies)
          .map((dep) ->
          {
            return dep.resolveDependency(
                LeaderLocator.class);
          });
    };
    LogProvider logProvider = globalModule.getLogService().getInternalLogProvider();
    LeaderService leaderService = new DefaultLeaderService(leaderLocatorForDatabase,
        this.topologyService, logProvider);
    Config config = globalModule.getGlobalConfig();
    return new CoreRoutingProcedureInstaller(this.topologyService, leaderService, databaseManager,
        config, logProvider);
  }

  private void createDatabaseManagerDependentModules(CoreDatabaseManager databaseManager) {
    SystemDbOnlyReplicatedDatabaseEventService databaseEventService = new SystemDbOnlyReplicatedDatabaseEventService(
        this.logProvider);
    LifeSupport globalLife = this.globalModule.getGlobalLife();
    FileSystemAbstraction fileSystem = this.globalModule.getFileSystem();
    MemberId myIdentity = this.identityModule.myself();
    Supplier<GraphDatabaseService> systemDbSupplier = () ->
    {
      return databaseManager.getDatabaseContext(
          DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID).orElseThrow().databaseFacade();
    };
    ClusterSystemGraphDbmsModel dbmsModel = new ClusterSystemGraphDbmsModel(systemDbSupplier);
    this.reconcilerModule =
        new ClusteredDbmsReconcilerModule(this.globalModule, databaseManager, databaseEventService,
            this.storageFactory, this.reconciledTxTracker,
            this.panicService, dbmsModel);
    this.databaseStartAborter = new DatabaseStartAborter(
        this.globalModule.getGlobalAvailabilityGuard(), dbmsModel,
        this.globalModule.getGlobalClock(),
        Duration.ofSeconds(5L));
    Dependencies dependencies = this.globalModule.getGlobalDependencies();
    dependencies.satisfyDependencies(databaseEventService);
    dependencies.satisfyDependency(this.reconciledTxTracker);
    this.topologyService = this
        .createTopologyService(myIdentity, databaseManager, this.reconcilerModule.reconciler());
    dependencies.satisfyDependency(new GlobalTopologyStateDiagnosticProvider(this.topologyService));
    this.reconcilerModule.reconciler().registerListener(this.topologyService);
    RaftMessageLogger<MemberId> raftLogger = createRaftLogger(this.globalModule, myIdentity);
    RaftMessageDispatcher raftMessageDispatcher = new RaftMessageDispatcher(this.logProvider,
        this.globalModule.getGlobalClock());
    RaftGroupFactory raftGroupFactory =
        new RaftGroupFactory(myIdentity, this.globalModule, this.clusterStateLayout,
            this.topologyService, this.storageFactory);
    RecoveryFacade recoveryFacade = Recovery
        .recoveryFacade(this.globalModule.getFileSystem(), this.globalModule.getPageCache(),
            this.globalConfig,
            this.globalModule.getStorageEngineFactory());
    this.coreDatabaseFactory = new CoreDatabaseFactory(this.globalModule, this.panicService,
        databaseManager, this.topologyService, this.storageFactory,
        this.temporaryDatabaseFactory, this.databaseInitializerMap, myIdentity, raftGroupFactory,
        raftMessageDispatcher, this.catchupComponentsProvider,
        recoveryFacade, raftLogger, this.raftSender, databaseEventService, dbmsModel,
        this.databaseStartAborter);
    RaftServerFactory raftServerFactory =
        new RaftServerFactory(this.globalModule, this.identityModule,
            this.pipelineBuilders.server(), raftLogger, this.supportedRaftProtocols,
            this.supportedModifierProtocols);
    Server raftServer = raftServerFactory
        .createRaftServer(raftMessageDispatcher, this.serverInstalledProtocolHandler);
    this.globalModule.getGlobalDependencies().satisfyDependencies(raftServer);
    globalLife.add(raftServer);
    this.createCoreServers(globalLife, databaseManager, fileSystem);
    this.globalModule.getGlobalLife().add(this.reconcilerModule);
  }

  public DatabaseManager<?> createDatabaseManager(GlobalModule globalModule) {
    CatchupComponentsProvider n10004 = this.catchupComponentsProvider;
    Objects.requireNonNull(n10004);
    CoreDatabaseManager databaseManager =
        new CoreDatabaseManager(globalModule, this, n10004::createDatabaseComponents,
            globalModule.getFileSystem(), globalModule.getPageCache(),
            this.logProvider, globalModule.getGlobalConfig(), this.clusterStateLayout);
    globalModule.getGlobalLife().add(databaseManager);
    globalModule.getGlobalDependencies().satisfyDependency(databaseManager);
    this.createDatabaseManagerDependentModules(databaseManager);
    return databaseManager;
  }

  public SystemGraphInitializer createSystemGraphInitializer(GlobalModule globalModule,
      DatabaseManager<?> databaseManager) {
    SystemGraphInitializer initializer =
        CommunityEditionModule.tryResolveOrCreate(SystemGraphInitializer.class,
            globalModule.getExternalDependencyResolver(),
            () ->
            {
              return new ClusterSystemGraphInitializer(databaseManager, globalModule
                  .getGlobalConfig());
            });
    this.databaseInitializerMap.put(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID, (db) ->
    {
      try {
        initializer.initializeSystemGraph(db);
      } catch (Exception n3) {
        throw new RuntimeException(n3);
      }
    });
    return globalModule.getGlobalDependencies().satisfyDependency(initializer);
  }

  public void createSecurityModule(GlobalModule globalModule) {
    Object securityProvider;
    if (globalModule.getGlobalConfig().get(GraphDatabaseSettings.auth_enabled)) {
      EnterpriseSecurityModule securityModule =
          new EnterpriseSecurityModule(globalModule.getLogService().getUserLogProvider(),
              this.globalConfig, this.globalProcedures,
              globalModule.getJobScheduler(), globalModule.getFileSystem(),
              globalModule.getGlobalDependencies(),
              globalModule.getTransactionEventListeners());
      securityModule.setup();
      this.databaseInitializerMap.put(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID,
          securityModule.getDatabaseInitializer());
      globalModule.getGlobalLife().add(securityModule);
      securityProvider = securityModule;
    } else {
      securityProvider = EnterpriseNoAuthSecurityProvider.INSTANCE;
    }

    this.setSecurityProvider((SecurityProvider) securityProvider);
  }

  public DatabaseStartupController getDatabaseStartupController() {
    return this.databaseStartAborter;
  }

  CoreDatabaseFactory coreDatabaseFactory() {
    return this.coreDatabaseFactory;
  }

  private RaftChannelPoolService buildRaftChannelPoolService(GlobalModule globalModule) {
    ClientChannelInitializer clientChannelInitializer = this
        .buildClientChannelInitializer(globalModule.getLogService());
    BootstrapConfiguration<? extends SocketChannel> bootstrapConfig = BootstrapConfiguration
        .clientConfig(this.globalConfig);
    return new RaftChannelPoolService(bootstrapConfig, globalModule.getJobScheduler(),
        this.logProvider, clientChannelInitializer);
  }

  private ClientChannelInitializer buildClientChannelInitializer(LogService logService) {
    ApplicationProtocolRepository applicationProtocolRepository =
        new ApplicationProtocolRepository(ApplicationProtocols.values(),
            this.supportedRaftProtocols);
    ModifierProtocolRepository modifierProtocolRepository = new ModifierProtocolRepository(
        ModifierProtocols.values(), this.supportedModifierProtocols);
    ProtocolInstallerRepository<ProtocolInstaller.Orientation.Client> protocolInstallerRepository =
        new ProtocolInstallerRepository(List.of(
            new RaftProtocolClientInstallerV2.Factory(this.pipelineBuilders.client(),
                this.logProvider)),
            ModifierProtocolInstaller.allClientInstallers);
    Duration handshakeTimeout = this.globalConfig.get(CausalClusteringSettings.handshake_timeout);
    HandshakeClientInitializer handshakeInitializer =
        new HandshakeClientInitializer(applicationProtocolRepository, modifierProtocolRepository,
            protocolInstallerRepository,
            this.pipelineBuilders.client(), handshakeTimeout, this.logProvider,
            logService.getUserLogProvider());
    return new ClientChannelInitializer(handshakeInitializer, this.pipelineBuilders.client(),
        handshakeTimeout, this.logProvider);
  }

  private CoreTopologyService createTopologyService(MemberId myIdentity,
      DatabaseManager<ClusteredDatabaseContext> databaseManager,
      DatabaseStateService databaseStateService) {
    DiscoveryMemberFactory discoveryMemberFactory = new DefaultDiscoveryMemberFactory(
        databaseManager, databaseStateService);
    DiscoveryModule discoveryModule =
        new DiscoveryModule(myIdentity, this.discoveryServiceFactory, discoveryMemberFactory,
            this.globalModule, this.sslPolicyLoader);
    return discoveryModule.topologyService();
  }
}
