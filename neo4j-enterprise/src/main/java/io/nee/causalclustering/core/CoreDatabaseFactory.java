/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.SessionTracker;
import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.catchup.CatchupComponentsProvider;
import io.nee.causalclustering.catchup.CatchupComponentsRepository;
import io.nee.causalclustering.common.DatabaseTopologyNotifier;
import io.nee.causalclustering.common.state.ClusterStateStorageFactory;
import io.nee.causalclustering.core.consensus.LeaderLocator;
import io.nee.causalclustering.core.consensus.RaftGroup;
import io.nee.causalclustering.core.consensus.RaftGroupFactory;
import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.log.pruning.PruningScheduler;
import io.nee.causalclustering.core.consensus.membership.RaftMembershipManager;
import io.nee.causalclustering.core.replication.ProgressTracker;
import io.nee.causalclustering.core.replication.ProgressTrackerImpl;
import io.nee.causalclustering.core.replication.RaftReplicator;
import io.nee.causalclustering.core.replication.Replicator;
import io.nee.causalclustering.core.replication.session.GlobalSession;
import io.nee.causalclustering.core.replication.session.GlobalSessionTrackerState;
import io.nee.causalclustering.core.replication.session.LocalSessionPool;
import io.nee.causalclustering.core.state.BootstrapContext;
import io.nee.causalclustering.core.state.BootstrapSaver;
import io.nee.causalclustering.core.state.CommandApplicationProcess;
import io.nee.causalclustering.core.state.CoreEditionKernelComponents;
import io.nee.causalclustering.core.state.CoreKernelResolvers;
import io.nee.causalclustering.core.state.CoreSnapshotService;
import io.nee.causalclustering.core.state.RaftBootstrapper;
import io.nee.causalclustering.core.state.RaftLogPruner;
import io.nee.causalclustering.core.state.machines.CommandIndexTracker;
import io.nee.causalclustering.core.state.machines.CoreStateMachines;
import io.nee.causalclustering.core.state.machines.StateMachineCommitHelper;
import io.nee.causalclustering.core.state.machines.dummy.DummyMachine;
import io.nee.causalclustering.core.state.machines.lease.ClusterLeaseCoordinator;
import io.nee.causalclustering.core.state.machines.lease.LeaderOnlyLockManager;
import io.nee.causalclustering.core.state.machines.lease.ReplicatedLeaseState;
import io.nee.causalclustering.core.state.machines.lease.ReplicatedLeaseStateMachine;
import io.nee.causalclustering.core.state.machines.token.ReplicatedLabelTokenHolder;
import io.nee.causalclustering.core.state.machines.token.ReplicatedPropertyKeyTokenHolder;
import io.nee.causalclustering.core.state.machines.token.ReplicatedRelationshipTypeTokenHolder;
import io.nee.causalclustering.core.state.machines.token.ReplicatedTokenStateMachine;
import io.nee.causalclustering.core.state.machines.tx.RecoverConsensusLogIndex;
import io.nee.causalclustering.core.state.machines.tx.ReplicatedTransactionStateMachine;
import io.nee.causalclustering.core.state.snapshot.CoreDownloader;
import io.nee.causalclustering.core.state.snapshot.CoreDownloaderService;
import io.nee.causalclustering.core.state.snapshot.SnapshotDownloader;
import io.nee.causalclustering.core.state.snapshot.StoreDownloadContext;
import io.nee.causalclustering.core.state.snapshot.StoreDownloader;
import io.nee.causalclustering.core.state.storage.SimpleStorage;
import io.nee.causalclustering.core.state.storage.StateStorage;
import io.nee.causalclustering.discovery.CoreTopologyService;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.error_handling.DatabasePanicker;
import io.nee.causalclustering.error_handling.PanicService;
import io.nee.causalclustering.helper.TemporaryDatabaseFactory;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftBinder;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.logging.RaftMessageLogger;
import io.nee.causalclustering.messaging.LifecycleMessageHandler;
import io.nee.causalclustering.messaging.LoggingOutbound;
import io.nee.causalclustering.messaging.Message;
import io.nee.causalclustering.messaging.Outbound;
import io.nee.causalclustering.messaging.RaftOutbound;
import io.nee.causalclustering.monitoring.ThroughputMonitor;
import io.nee.causalclustering.upstream.NoOpUpstreamDatabaseStrategiesLoader;
import io.nee.causalclustering.upstream.UpstreamDatabaseSelectionStrategy;
import io.nee.causalclustering.upstream.UpstreamDatabaseStrategiesLoader;
import io.nee.causalclustering.upstream.UpstreamDatabaseStrategySelector;
import io.nee.causalclustering.upstream.strategies.TypicallyConnectToRandomReadReplicaStrategy;
import io.nee.dbms.ClusterInternalDbmsOperator;
import io.nee.dbms.ClusterSystemGraphDbmsModel;
import io.nee.dbms.DatabaseStartAborter;
import io.nee.dbms.ReplicatedDatabaseEventService;
import io.nee.dbms.database.ClusteredDatabaseContext;
import java.time.Clock;
import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.dbms.database.DatabasePageCache;
import org.neo4j.graphdb.factory.EditionLocksFactories;
import org.neo4j.graphdb.factory.module.DatabaseInitializer;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.graphdb.factory.module.id.DatabaseIdContext;
import org.neo4j.graphdb.factory.module.id.IdContextFactory;
import org.neo4j.graphdb.factory.module.id.IdContextFactoryBuilder;
import org.neo4j.internal.helpers.ExponentialBackoffStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.id.IdGeneratorFactory;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.io.pagecache.tracing.cursor.PageCursorTracerSupplier;
import org.neo4j.io.pagecache.tracing.cursor.context.EmptyVersionContextSupplier;
import org.neo4j.io.pagecache.tracing.cursor.context.VersionContextSupplier;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.api.CommitProcessFactory;
import org.neo4j.kernel.impl.factory.AccessCapabilityFactory;
import org.neo4j.kernel.impl.locking.Locks;
import org.neo4j.kernel.impl.locking.LocksFactory;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.kernel.recovery.RecoveryFacade;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.DatabaseLogProvider;
import org.neo4j.logging.internal.DatabaseLogService;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.storageengine.api.StorageEngine;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.time.Clocks;
import org.neo4j.time.SystemNanoClock;
import org.neo4j.token.TokenHolders;
import org.neo4j.token.TokenRegistry;

class CoreDatabaseFactory {

  private final Config config;
  private final SystemNanoClock clock;
  private final JobScheduler jobScheduler;
  private final FileSystemAbstraction fileSystem;
  private final PageCache pageCache;
  private final StorageEngineFactory storageEngineFactory;
  private final DatabaseManager<ClusteredDatabaseContext> databaseManager;
  private final CatchupComponentsRepository catchupComponentsRepository;
  private final CatchupComponentsProvider catchupComponentsProvider;
  private final PanicService panicService;
  private final CoreTopologyService topologyService;
  private final ClusterStateStorageFactory storageFactory;
  private final TemporaryDatabaseFactory temporaryDatabaseFactory;
  private final Map<NamedDatabaseId, DatabaseInitializer> databaseInitializers;
  private final MemberId myIdentity;
  private final RaftGroupFactory raftGroupFactory;
  private final RaftMessageDispatcher raftMessageDispatcher;
  private final RaftMessageLogger<MemberId> raftLogger;
  private final PageCursorTracerSupplier cursorTracerSupplier;
  private final RecoveryFacade recoveryFacade;
  private final Outbound<SocketAddress, Message> raftSender;
  private final ReplicatedDatabaseEventService databaseEventService;
  private final ClusterSystemGraphDbmsModel dbmsModel;
  private final DatabaseStartAborter databaseStartAborter;
  private final BootstrapSaver bootstrapSaver;

  CoreDatabaseFactory(GlobalModule globalModule, PanicService panicService,
      DatabaseManager<ClusteredDatabaseContext> databaseManager,
      CoreTopologyService topologyService, ClusterStateStorageFactory storageFactory,
      TemporaryDatabaseFactory temporaryDatabaseFactory,
      Map<NamedDatabaseId, DatabaseInitializer> databaseInitializers, MemberId myIdentity,
      RaftGroupFactory raftGroupFactory,
      RaftMessageDispatcher raftMessageDispatcher,
      CatchupComponentsProvider catchupComponentsProvider, RecoveryFacade recoveryFacade,
      RaftMessageLogger<MemberId> raftLogger, Outbound<SocketAddress, Message> raftSender,
      ReplicatedDatabaseEventService databaseEventService,
      ClusterSystemGraphDbmsModel dbmsModel, DatabaseStartAborter databaseStartAborter) {
    this.config = globalModule.getGlobalConfig();
    this.clock = globalModule.getGlobalClock();
    this.jobScheduler = globalModule.getJobScheduler();
    this.fileSystem = globalModule.getFileSystem();
    this.pageCache = globalModule.getPageCache();
    this.storageEngineFactory = globalModule.getStorageEngineFactory();
    this.databaseManager = databaseManager;
    this.catchupComponentsRepository = new CatchupComponentsRepository(databaseManager);
    this.panicService = panicService;
    this.topologyService = topologyService;
    this.storageFactory = storageFactory;
    this.temporaryDatabaseFactory = temporaryDatabaseFactory;
    this.databaseInitializers = databaseInitializers;
    this.myIdentity = myIdentity;
    this.raftGroupFactory = raftGroupFactory;
    this.raftMessageDispatcher = raftMessageDispatcher;
    this.catchupComponentsProvider = catchupComponentsProvider;
    this.recoveryFacade = recoveryFacade;
    this.raftLogger = raftLogger;
    this.raftSender = raftSender;
    this.databaseEventService = databaseEventService;
    this.cursorTracerSupplier = globalModule.getTracers().getPageCursorTracerSupplier();
    this.dbmsModel = dbmsModel;
    this.databaseStartAborter = databaseStartAborter;
    this.bootstrapSaver = new BootstrapSaver(this.fileSystem,
        globalModule.getLogService().getInternalLogProvider());
  }

  CoreRaftContext createRaftContext(NamedDatabaseId namedDatabaseId, LifeSupport life,
      Monitors monitors, Dependencies dependencies,
      BootstrapContext bootstrapContext, DatabaseLogService logService) {
    DatabaseLogProvider debugLog = logService.getInternalLogProvider();
    DatabaseInitializer databaseInitializer =
        this.databaseInitializers
            .getOrDefault(namedDatabaseId, DatabaseInitializer.NO_INITIALIZATION);
    SimpleStorage<RaftId> raftIdStorage = this.storageFactory
        .createRaftIdStorage(namedDatabaseId.name(), debugLog);
    RaftBinder raftBinder = this
        .createRaftBinder(namedDatabaseId, this.config, monitors, raftIdStorage, bootstrapContext,
            this.temporaryDatabaseFactory,
            databaseInitializer, debugLog, this.dbmsModel);
    CommandIndexTracker commandIndexTracker = dependencies
        .satisfyDependency(new CommandIndexTracker());
    this.initialiseStatusDescriptionEndpoint(dependencies, commandIndexTracker, life, debugLog);
    long logThresholdMillis = this.config
        .get(CausalClusteringSettings.unknown_address_logging_throttle).toMillis();
    LoggingOutbound<MemberId, RaftMessages.RaftMessage> raftOutbound = new LoggingOutbound(
        new RaftOutbound(this.topologyService, this.raftSender, this.raftMessageDispatcher,
            raftBinder, debugLog, logThresholdMillis, this.myIdentity,
            this.clock), this.myIdentity, this.raftLogger);
    RaftGroup raftGroup = this.raftGroupFactory
        .create(namedDatabaseId, raftOutbound, life, monitors, dependencies, logService);
    GlobalSession myGlobalSession = new GlobalSession(UUID.randomUUID(), this.myIdentity);
    LocalSessionPool sessionPool = new LocalSessionPool(myGlobalSession);
    ProgressTracker progressTracker = new ProgressTrackerImpl(myGlobalSession);
    RaftReplicator replicator =
        this.createReplicator(namedDatabaseId, raftGroup.raftMachine(), sessionPool,
            progressTracker, monitors, raftOutbound, debugLog);
    return new CoreRaftContext(raftGroup, replicator, commandIndexTracker, progressTracker,
        raftBinder, raftIdStorage);
  }

  CoreEditionKernelComponents createKernelComponents(NamedDatabaseId namedDatabaseId,
      LifeSupport life, CoreRaftContext raftContext,
      CoreKernelResolvers kernelResolvers, DatabaseLogService logService,
      VersionContextSupplier versionContextSupplier) {
    RaftGroup raftGroup = raftContext.raftGroup();
    Replicator replicator = raftContext.replicator();
    DatabaseLogProvider debugLog = logService.getInternalLogProvider();
    ReplicatedLeaseStateMachine replicatedLeaseStateMachine = this
        .createLeaseStateMachine(namedDatabaseId, life, debugLog, kernelResolvers);
    DatabaseIdContext idContext = this.createIdContext(namedDatabaseId);
    Supplier<StorageEngine> storageEngineSupplier = kernelResolvers.storageEngine();
    TokenRegistry relationshipTypeTokenRegistry = new TokenRegistry("RelationshipType");
    ReplicatedRelationshipTypeTokenHolder relationshipTypeTokenHolder =
        new ReplicatedRelationshipTypeTokenHolder(namedDatabaseId, relationshipTypeTokenRegistry,
            replicator, idContext.getIdGeneratorFactory(),
            storageEngineSupplier);
    TokenRegistry propertyKeyTokenRegistry = new TokenRegistry("PropertyKey");
    ReplicatedPropertyKeyTokenHolder propertyKeyTokenHolder =
        new ReplicatedPropertyKeyTokenHolder(namedDatabaseId, propertyKeyTokenRegistry, replicator,
            idContext.getIdGeneratorFactory(),
            storageEngineSupplier);
    TokenRegistry labelTokenRegistry = new TokenRegistry("Label");
    ReplicatedLabelTokenHolder labelTokenHolder =
        new ReplicatedLabelTokenHolder(namedDatabaseId, labelTokenRegistry, replicator,
            idContext.getIdGeneratorFactory(), storageEngineSupplier);
    ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch =
        this.databaseEventService.getDatabaseEventDispatch(namedDatabaseId);
    StateMachineCommitHelper commitHelper =
        new StateMachineCommitHelper(raftContext.commandIndexTracker(), this.cursorTracerSupplier,
            versionContextSupplier, databaseEventDispatch);
    ReplicatedTokenStateMachine labelTokenStateMachine = new ReplicatedTokenStateMachine(
        commitHelper, labelTokenRegistry, debugLog);
    ReplicatedTokenStateMachine propertyKeyTokenStateMachine = new ReplicatedTokenStateMachine(
        commitHelper, propertyKeyTokenRegistry, debugLog);
    ReplicatedTokenStateMachine relTypeTokenStateMachine = new ReplicatedTokenStateMachine(
        commitHelper, relationshipTypeTokenRegistry, debugLog);
    ReplicatedTransactionStateMachine replicatedTxStateMachine = new ReplicatedTransactionStateMachine(
        commitHelper, replicatedLeaseStateMachine,
        this.config
            .get(CausalClusteringSettings.state_machine_apply_max_batch_size),
        debugLog);
    Locks lockManager = this.createLockManager(this.config, this.clock, logService);
    RecoverConsensusLogIndex consensusLogIndexRecovery = new RecoverConsensusLogIndex(
        kernelResolvers.txIdStore(), kernelResolvers.txStore(), debugLog);
    CoreStateMachines stateMachines =
        new CoreStateMachines(replicatedTxStateMachine, labelTokenStateMachine,
            relTypeTokenStateMachine, propertyKeyTokenStateMachine,
            replicatedLeaseStateMachine, new DummyMachine(), consensusLogIndexRecovery);
    TokenHolders tokenHolders = new TokenHolders(propertyKeyTokenHolder, labelTokenHolder,
        relationshipTypeTokenHolder);
    ClusterLeaseCoordinator leaseCoordinator =
        new ClusterLeaseCoordinator(this.myIdentity, replicator, raftGroup.raftMachine(),
            replicatedLeaseStateMachine, namedDatabaseId);
    CommitProcessFactory commitProcessFactory = new CoreCommitProcessFactory(namedDatabaseId,
        replicator, stateMachines, leaseCoordinator);
    AccessCapabilityFactory accessCapabilityFactory = AccessCapabilityFactory
        .fixed(new LeaderCanWrite(raftGroup.raftMachine()));
    return new CoreEditionKernelComponents(commitProcessFactory, lockManager, tokenHolders,
        idContext, stateMachines, accessCapabilityFactory,
        leaseCoordinator);
  }

  CoreDatabase createDatabase(NamedDatabaseId namedDatabaseId, LifeSupport clusterComponents,
      Monitors monitors, Dependencies dependencies,
      StoreDownloadContext downloadContext, Database kernelDatabase,
      CoreEditionKernelComponents kernelComponents,
      CoreRaftContext raftContext,
      ClusterInternalDbmsOperator internalOperator) {
    DatabasePanicker panicker = this.panicService.panickerFor(namedDatabaseId);
    RaftGroup raftGroup = raftContext.raftGroup();
    DatabaseLogProvider debugLog = kernelDatabase.getInternalLogProvider();
    SessionTracker sessionTracker = this
        .createSessionTracker(namedDatabaseId, clusterComponents, debugLog);
    StateStorage<Long> lastFlushedStateStorage = this.storageFactory
        .createLastFlushedStorage(namedDatabaseId.name(), clusterComponents, debugLog);
    CoreState coreState = new CoreState(sessionTracker, lastFlushedStateStorage,
        kernelComponents.stateMachines());
    CommandApplicationProcess applicationProcess =
        this.createCommandApplicationProcess(raftGroup, panicker, this.config, clusterComponents,
            this.jobScheduler, dependencies, monitors,
            raftContext.progressTracker(), sessionTracker, coreState, debugLog,
            kernelDatabase.getNamedDatabaseId());
    CoreSnapshotService snapshotService =
        new CoreSnapshotService(applicationProcess, raftGroup.raftLog(), coreState,
            raftGroup.raftMachine(), namedDatabaseId,
            kernelDatabase.getLogService(), this.clock);
    dependencies.satisfyDependencies(snapshotService);
    CoreDownloaderService downloadService =
        this.createDownloader(this.catchupComponentsProvider, panicker, this.jobScheduler, monitors,
            applicationProcess, snapshotService,
            downloadContext, debugLog);
    TypicallyConnectToRandomReadReplicaStrategy defaultStrategy = new TypicallyConnectToRandomReadReplicaStrategy(
        2);
    defaultStrategy.inject(this.topologyService, this.config, debugLog, this.myIdentity);
    UpstreamDatabaseStrategySelector catchupStrategySelector =
        this.createUpstreamDatabaseStrategySelector(this.myIdentity, this.config, debugLog,
            this.topologyService, defaultStrategy);
    LeaderProvider leaderProvider = new LeaderProvider(raftGroup.raftMachine(),
        this.topologyService);
    CatchupAddressProvider.LeaderOrUpstreamStrategyBasedAddressProvider catchupAddressProvider =
        new CatchupAddressProvider.LeaderOrUpstreamStrategyBasedAddressProvider(leaderProvider,
            this.topologyService, catchupStrategySelector);
    dependencies.satisfyDependency(raftGroup.raftMachine());
    RaftMembershipManager n10000 = raftGroup.raftMembershipManager();
    Objects.requireNonNull(lastFlushedStateStorage);
    n10000.setRecoverFromIndexSupplier(lastFlushedStateStorage::getInitialState);
    RaftMessageHandlerChainFactory raftMessageHandlerChainFactory =
        new RaftMessageHandlerChainFactory(this.jobScheduler, this.clock, debugLog, monitors,
            this.config, this.raftMessageDispatcher,
            catchupAddressProvider, panicker, kernelDatabase.getNamedDatabaseId());
    LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> messageHandler =
        raftMessageHandlerChainFactory
            .createMessageHandlerChain(raftGroup, downloadService, applicationProcess);
    DatabaseTopologyNotifier topologyNotifier = new DatabaseTopologyNotifier(namedDatabaseId,
        this.topologyService);
    CorePanicHandlers panicHandler =
        new CorePanicHandlers(raftGroup.raftMachine(), kernelDatabase, applicationProcess,
            internalOperator, this.panicService);
    TempBootstrapDir tempBootstrapDir = new TempBootstrapDir(this.fileSystem,
        kernelDatabase.getDatabaseLayout());
    CoreBootstrap bootstrap =
        new CoreBootstrap(kernelDatabase, raftContext.raftBinder(), messageHandler, snapshotService,
            downloadService, internalOperator,
            this.databaseStartAborter, raftContext.raftIdStorage(), this.bootstrapSaver,
            tempBootstrapDir);
    return new CoreDatabase(raftGroup.raftMachine(), kernelDatabase, applicationProcess,
        messageHandler, downloadService, this.recoveryFacade,
        clusterComponents, panicHandler, bootstrap, topologyNotifier);
  }

  private RaftBinder createRaftBinder(NamedDatabaseId namedDatabaseId, Config config,
      Monitors monitors, SimpleStorage<RaftId> raftIdStorage,
      BootstrapContext bootstrapContext, TemporaryDatabaseFactory temporaryDatabaseFactory,
      DatabaseInitializer databaseInitializer,
      DatabaseLogProvider debugLog, ClusterSystemGraphDbmsModel systemGraph) {
    DatabasePageCache pageCache = new DatabasePageCache(this.pageCache,
        EmptyVersionContextSupplier.EMPTY);
    RaftBootstrapper raftBootstrapper =
        new RaftBootstrapper(bootstrapContext, temporaryDatabaseFactory, databaseInitializer,
            pageCache, this.fileSystem, debugLog,
            this.storageEngineFactory, config, this.bootstrapSaver);
    int minimumCoreHosts = config
        .get(CausalClusteringSettings.minimum_core_cluster_size_at_formation);
    Duration clusterBindingTimeout = config.get(CausalClusteringSettings.cluster_binding_timeout);
    return new RaftBinder(namedDatabaseId, this.myIdentity, raftIdStorage, this.topologyService,
        systemGraph, Clocks.systemClock(), () ->
    {
      Thread.sleep(100L);
    }, clusterBindingTimeout, raftBootstrapper, minimumCoreHosts, monitors);
  }

  private UpstreamDatabaseStrategySelector createUpstreamDatabaseStrategySelector(MemberId myself,
      Config config, LogProvider logProvider,
      TopologyService topologyService,
      UpstreamDatabaseSelectionStrategy defaultStrategy) {
    Object loader;
    if (config.get(CausalClusteringSettings.multi_dc_license)) {
      loader = new UpstreamDatabaseStrategiesLoader(topologyService, config, myself, logProvider);
      logProvider.getLog(this.getClass()).info("Multi-Data Center option enabled.");
    } else {
      loader = new NoOpUpstreamDatabaseStrategiesLoader();
    }

    return new UpstreamDatabaseStrategySelector(defaultStrategy, (Iterable) loader, logProvider);
  }

  private CommandApplicationProcess createCommandApplicationProcess(RaftGroup raftGroup,
      DatabasePanicker panicker, Config config, LifeSupport life,
      JobScheduler jobScheduler, Dependencies dependencies, Monitors monitors,
      ProgressTracker progressTracker, SessionTracker sessionTracker,
      CoreState coreState, DatabaseLogProvider debugLog, NamedDatabaseId namedDatabaseId) {
    CommandApplicationProcess commandApplicationProcess =
        new CommandApplicationProcess(raftGroup.raftLog(),
            config.get(CausalClusteringSettings.state_machine_apply_max_batch_size),
            config.get(CausalClusteringSettings.state_machine_flush_window_size), debugLog,
            progressTracker,
            sessionTracker, coreState,
            raftGroup.inFlightCache(), monitors, panicker, jobScheduler,
            namedDatabaseId.toString());
    dependencies.satisfyDependency(commandApplicationProcess);
    RaftLogPruner raftLogPruner = new RaftLogPruner(raftGroup.raftMachine(),
        commandApplicationProcess);
    dependencies.satisfyDependency(raftLogPruner);
    life.add(new PruningScheduler(raftLogPruner, jobScheduler,
        config.get(CausalClusteringSettings.raft_log_pruning_frequency).toMillis(),
        debugLog));
    return commandApplicationProcess;
  }

  private SessionTracker createSessionTracker(NamedDatabaseId namedDatabaseId, LifeSupport life,
      DatabaseLogProvider databaseLogProvider) {
    StateStorage<GlobalSessionTrackerState> sessionTrackerStorage =
        this.storageFactory
            .createSessionTrackerStorage(namedDatabaseId.name(), life, databaseLogProvider);
    return new SessionTracker(sessionTrackerStorage);
  }

  private RaftReplicator createReplicator(NamedDatabaseId namedDatabaseId,
      LeaderLocator leaderLocator, LocalSessionPool sessionPool,
      ProgressTracker progressTracker, Monitors monitors,
      Outbound<MemberId, RaftMessages.RaftMessage> raftOutbound,
      DatabaseLogProvider debugLog) {
    Duration initialBackoff = this.config
        .get(CausalClusteringSettings.replication_retry_timeout_base);
    Duration upperBoundBackoff = this.config
        .get(CausalClusteringSettings.replication_retry_timeout_limit);
    TimeoutStrategy progressRetryStrategy = new ExponentialBackoffStrategy(initialBackoff,
        upperBoundBackoff);
    long availabilityTimeoutMillis = this.config
        .get(CausalClusteringSettings.replication_retry_timeout_base).toMillis();
    Duration leaderAwaitDuration = this.config
        .get(CausalClusteringSettings.replication_leader_await_timeout);
    return new RaftReplicator(namedDatabaseId, leaderLocator, this.myIdentity, raftOutbound,
        sessionPool, progressTracker, progressRetryStrategy,
        availabilityTimeoutMillis, debugLog, this.databaseManager, monitors, leaderAwaitDuration);
  }

  private CoreDownloaderService createDownloader(
      CatchupComponentsProvider catchupComponentsProvider, DatabasePanicker panicker,
      JobScheduler jobScheduler,
      Monitors monitors, CommandApplicationProcess commandApplicationProcess,
      CoreSnapshotService snapshotService,
      StoreDownloadContext downloadContext,
      DatabaseLogProvider debugLog) {
    SnapshotDownloader snapshotDownloader = new SnapshotDownloader(debugLog,
        catchupComponentsProvider.catchupClientFactory());
    StoreDownloader storeDownloader = new StoreDownloader(this.catchupComponentsRepository,
        debugLog);
    CoreDownloader downloader = new CoreDownloader(snapshotDownloader, storeDownloader, debugLog);
    ExponentialBackoffStrategy backoffStrategy = new ExponentialBackoffStrategy(1L, 30L,
        TimeUnit.SECONDS);
    return new CoreDownloaderService(jobScheduler, downloader, downloadContext, snapshotService,
        this.databaseEventService, commandApplicationProcess,
        debugLog, backoffStrategy, panicker, monitors, this.databaseStartAborter);
  }

  private DatabaseIdContext createIdContext(NamedDatabaseId namedDatabaseId) {
    Function<NamedDatabaseId, IdGeneratorFactory> idGeneratorProvider =
        IdContextFactoryBuilder.defaultIdGeneratorFactoryProvider(this.fileSystem, this.config);
    IdContextFactory idContextFactory = IdContextFactoryBuilder
        .of(this.fileSystem, this.jobScheduler, this.config).withIdGenerationFactoryProvider(
            idGeneratorProvider).withFactoryWrapper((generator) ->
        {
          return generator;
        }).build();
    return idContextFactory.createIdContext(namedDatabaseId);
  }

  private ReplicatedLeaseStateMachine createLeaseStateMachine(NamedDatabaseId namedDatabaseId,
      LifeSupport life, DatabaseLogProvider databaseLogProvider,
      CoreKernelResolvers resolvers) {
    StateStorage<ReplicatedLeaseState> leaseStorage = this.storageFactory
        .createLeaseStorage(namedDatabaseId.name(), life, databaseLogProvider);
    return new ReplicatedLeaseStateMachine(leaseStorage, () ->
    {
      resolvers.idGeneratorFactory().get().clearCache();
    });
  }

  private Locks createLockManager(Config config, Clock clock, LogService logService) {
    LocksFactory lockFactory = EditionLocksFactories.createLockFactory(config, logService);
    Locks localLocks = EditionLocksFactories.createLockManager(lockFactory, config, clock);
    return new LeaderOnlyLockManager(localLocks);
  }

  private void initialiseStatusDescriptionEndpoint(Dependencies dependencies,
      CommandIndexTracker commandIndexTracker, LifeSupport life,
      DatabaseLogProvider debugLog) {
    Duration samplingWindow = this.config.get(CausalClusteringSettings.status_throughput_window);
    SystemNanoClock n10003 = this.clock;
    JobScheduler n10004 = this.jobScheduler;
    Objects.requireNonNull(commandIndexTracker);
    ThroughputMonitor throughputMonitor =
        new ThroughputMonitor(debugLog, n10003, n10004, samplingWindow,
            commandIndexTracker::getAppliedCommandIndex);
    life.add(throughputMonitor);
    dependencies.satisfyDependency(throughputMonitor);
  }
}
