/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.core.consensus.RaftGroup;
import io.nee.causalclustering.core.replication.ProgressTracker;
import io.nee.causalclustering.core.replication.Replicator;
import io.nee.causalclustering.core.state.machines.CommandIndexTracker;
import io.nee.causalclustering.core.state.storage.SimpleStorage;
import io.nee.causalclustering.identity.RaftBinder;
import io.nee.causalclustering.identity.RaftId;

class CoreRaftContext {

  private final RaftGroup raftGroup;
  private final Replicator replicator;
  private final CommandIndexTracker commandIndexTracker;
  private final ProgressTracker progressTracker;
  private final RaftBinder raftBinder;
  private final SimpleStorage<RaftId> raftIdStorage;

  CoreRaftContext(RaftGroup raftGroup, Replicator replicator,
      CommandIndexTracker commandIndexTracker, ProgressTracker progressTracker,
      RaftBinder raftBinder, SimpleStorage<RaftId> raftIdStorage) {
    this.raftGroup = raftGroup;
    this.replicator = replicator;
    this.commandIndexTracker = commandIndexTracker;
    this.progressTracker = progressTracker;
    this.raftBinder = raftBinder;
    this.raftIdStorage = raftIdStorage;
  }

  RaftGroup raftGroup() {
    return this.raftGroup;
  }

  Replicator replicator() {
    return this.replicator;
  }

  CommandIndexTracker commandIndexTracker() {
    return this.commandIndexTracker;
  }

  ProgressTracker progressTracker() {
    return this.progressTracker;
  }

  RaftBinder raftBinder() {
    return this.raftBinder;
  }

  SimpleStorage<RaftId> raftIdStorage() {
    return this.raftIdStorage;
  }
}
