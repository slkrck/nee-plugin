/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.core.state.CoreEditionKernelComponents;
import java.util.function.Function;
import org.neo4j.configuration.Config;
import org.neo4j.graphdb.factory.module.edition.AbstractEditionModule;
import org.neo4j.graphdb.factory.module.edition.context.EditionDatabaseComponents;
import org.neo4j.graphdb.factory.module.id.DatabaseIdContext;
import org.neo4j.io.fs.watcher.DatabaseLayoutWatcher;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.IOLimiter;
import org.neo4j.kernel.database.DatabaseStartupController;
import org.neo4j.kernel.impl.api.CommitProcessFactory;
import org.neo4j.kernel.impl.constraints.ConstraintSemantics;
import org.neo4j.kernel.impl.factory.AccessCapabilityFactory;
import org.neo4j.kernel.impl.factory.StatementLocksFactorySelector;
import org.neo4j.kernel.impl.locking.Locks;
import org.neo4j.kernel.impl.locking.StatementLocksFactory;
import org.neo4j.kernel.impl.query.QueryEngineProvider;
import org.neo4j.kernel.impl.transaction.stats.DatabaseTransactionStats;
import org.neo4j.logging.internal.DatabaseLogService;
import org.neo4j.token.TokenHolders;

public class CoreDatabaseComponents implements EditionDatabaseComponents {

  private final AbstractEditionModule editionModule;
  private final CoreEditionKernelComponents kernelComponents;
  private final StatementLocksFactory statementLocksFactory;
  private final DatabaseTransactionStats transactionMonitor;

  CoreDatabaseComponents(Config config, AbstractEditionModule editionModule,
      CoreEditionKernelComponents kernelComponents, DatabaseLogService logService) {
    this.editionModule = editionModule;
    this.kernelComponents = kernelComponents;
    this.statementLocksFactory = (new StatementLocksFactorySelector(kernelComponents.lockManager(),
        config, logService)).select();
    this.transactionMonitor = editionModule.createTransactionMonitor();
  }

  public DatabaseIdContext getIdContext() {
    return this.kernelComponents.idContext();
  }

  public TokenHolders getTokenHolders() {
    return this.kernelComponents.tokenHolders();
  }

  public Function<DatabaseLayout, DatabaseLayoutWatcher> getWatcherServiceFactory() {
    return this.editionModule.getWatcherServiceFactory();
  }

  public IOLimiter getIoLimiter() {
    return this.editionModule.getIoLimiter();
  }

  public ConstraintSemantics getConstraintSemantics() {
    return this.editionModule.getConstraintSemantics();
  }

  public CommitProcessFactory getCommitProcessFactory() {
    return this.kernelComponents.commitProcessFactory();
  }

  public Locks getLocks() {
    return this.kernelComponents.lockManager();
  }

  public StatementLocksFactory getStatementLocksFactory() {
    return this.statementLocksFactory;
  }

  public DatabaseTransactionStats getTransactionMonitor() {
    return this.transactionMonitor;
  }

  public QueryEngineProvider getQueryEngineProvider() {
    return this.editionModule.getQueryEngineProvider();
  }

  public AccessCapabilityFactory getAccessCapabilityFactory() {
    return this.kernelComponents.accessCapabilityFactory();
  }

  public DatabaseStartupController getStartupController() {
    return this.editionModule.getDatabaseStartupController();
  }
}
