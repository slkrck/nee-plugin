/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import io.nee.causalclustering.catchup.CatchupClientFactory;
import io.nee.causalclustering.catchup.CatchupComponentsProvider;
import io.nee.causalclustering.catchup.CatchupComponentsRepository;
import io.nee.causalclustering.catchup.MultiDatabaseCatchupServerHandler;
import io.nee.causalclustering.common.ClusteringEditionModule;
import io.nee.causalclustering.common.PipelineBuilders;
import io.nee.causalclustering.common.state.ClusterStateStorageFactory;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.core.state.ClusterStateLayout;
import io.nee.causalclustering.discovery.DiscoveryServiceFactory;
import io.nee.causalclustering.discovery.RemoteMembersResolver;
import io.nee.causalclustering.discovery.ResolutionResolverFactory;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.discovery.member.DefaultDiscoveryMemberFactory;
import io.nee.causalclustering.discovery.member.DiscoveryMemberFactory;
import io.nee.causalclustering.discovery.procedures.ClusterOverviewProcedure;
import io.nee.causalclustering.discovery.procedures.ReadReplicaRoleProcedure;
import io.nee.causalclustering.error_handling.PanicService;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.net.InstalledProtocolHandler;
import io.nee.causalclustering.net.Server;
import io.nee.dbms.ClusterSystemGraphDbmsModel;
import io.nee.dbms.ClusteredDbmsReconcilerModule;
import io.nee.dbms.DatabaseStartAborter;
import io.nee.dbms.SystemDbOnlyReplicatedDatabaseEventService;
import io.nee.dbms.database.ClusteredDatabaseContext;
import io.nee.dbms.procedures.ClusteredDatabaseStateProcedure;
import io.nee.enterprise.edition.AbstractEnterpriseEditionModule;
import io.nee.kernel.enterprise.api.security.provider.EnterpriseNoAuthSecurityProvider;
import io.nee.kernel.impl.net.DefaultNetworkConnectionTracker;
import io.nee.procedure.enterprise.builtin.EnterpriseBuiltInDbmsProcedures;
import io.nee.procedure.enterprise.builtin.EnterpriseBuiltInProcedures;
import io.nee.procedure.enterprise.builtin.SettingsWhitelist;
import io.nee.server.security.enterprise.EnterpriseSecurityModule;
import java.io.File;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.cypher.internal.javacompat.EnterpriseCypherEngineProvider;
import org.neo4j.dbms.DatabaseStateService;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.dbms.database.SystemGraphInitializer;
import org.neo4j.exceptions.KernelException;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.graphdb.factory.module.edition.context.EditionDatabaseComponents;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.api.net.NetworkConnectionTracker;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import org.neo4j.kernel.api.security.provider.SecurityProvider;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.DatabaseStartupController;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.query.QueryEngineProvider;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.LogService;
import org.neo4j.procedure.builtin.routing.BaseRoutingProcedureInstaller;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.ssl.config.SslPolicyLoader;

public class ReadReplicaEditionModule extends ClusteringEditionModule implements
    AbstractEnterpriseEditionModule {

  protected final LogProvider logProvider;
  private final Config globalConfig;
  private final GlobalModule globalModule;
  private final MemberId myIdentity;
  private final JobScheduler jobScheduler;
  private final PanicService panicService;
  private final CatchupComponentsProvider catchupComponentsProvider;
  private final DiscoveryServiceFactory discoveryServiceFactory;
  private final SslPolicyLoader sslPolicyLoader;
  private final ClusterStateStorageFactory storageFactory;
  private final ClusterStateLayout clusterStateLayout;
  private TopologyService topologyService;
  private ReadReplicaDatabaseFactory readReplicaDatabaseFactory;
  private ClusteredDbmsReconcilerModule reconcilerModule;
  private DatabaseStartAborter databaseStartAborter;

  public ReadReplicaEditionModule(GlobalModule globalModule,
      DiscoveryServiceFactory discoveryServiceFactory, MemberId myIdentity) {
    super(globalModule);
    this.globalModule = globalModule;
    this.discoveryServiceFactory = discoveryServiceFactory;
    this.myIdentity = myIdentity;
    LogService logService = globalModule.getLogService();
    this.globalConfig = globalModule.getGlobalConfig();
    this.logProvider = logService.getInternalLogProvider();
    this.logProvider.getLog(this.getClass())
        .info(String.format("Generated new id: %s", myIdentity));
    this.jobScheduler = globalModule.getJobScheduler();
    this.jobScheduler.setTopLevelGroupName("ReadReplica " + myIdentity);
    Dependencies globalDependencies = globalModule.getGlobalDependencies();
    SettingsWhitelist settingsWhiteList = new SettingsWhitelist(this.globalConfig);
    globalDependencies.satisfyDependency(settingsWhiteList);
    this.panicService = new PanicService(this.jobScheduler, logService);
    globalDependencies.satisfyDependencies(this.panicService);
    this.watcherServiceFactory = (layout) ->
    {
      return this.createDatabaseFileSystemWatcher(globalModule.getFileWatcher(), layout, logService,
          fileWatcherFileNameFilter());
    };
    this.sslPolicyLoader = SslPolicyLoader.create(this.globalConfig, this.logProvider);
    globalDependencies.satisfyDependency(this.sslPolicyLoader);
    PipelineBuilders pipelineBuilders = new PipelineBuilders(this.sslPolicyLoader);
    this.catchupComponentsProvider = new CatchupComponentsProvider(globalModule, pipelineBuilders);
    FileSystemAbstraction fileSystem = globalModule.getFileSystem();
    File dataDir = this.globalConfig.get(GraphDatabaseSettings.data_directory).toFile();
    this.clusterStateLayout = ClusterStateLayout.of(dataDir);
    this.storageFactory = new ClusterStateStorageFactory(fileSystem, this.clusterStateLayout,
        this.logProvider, this.globalConfig);
    this.satisfyEnterpriseOnlyDependencies(this.globalModule);
    this.editionInvariants(globalModule, globalDependencies);
  }

  public QueryEngineProvider getQueryEngineProvider() {
    return new EnterpriseCypherEngineProvider();
  }

  public void registerEditionSpecificProcedures(GlobalProcedures globalProcedures,
      DatabaseManager<?> databaseManager) throws KernelException {
    globalProcedures.registerProcedure(EnterpriseBuiltInDbmsProcedures.class, true);
    globalProcedures.registerProcedure(EnterpriseBuiltInProcedures.class, true);
    globalProcedures.register(new ReadReplicaRoleProcedure(databaseManager));
    globalProcedures.register(
        new ClusterOverviewProcedure(this.topologyService, databaseManager.databaseIdRepository()));
    globalProcedures.register(
        new ClusteredDatabaseStateProcedure(databaseManager.databaseIdRepository(),
            this.topologyService, this.reconcilerModule.reconciler()));
  }

  protected BaseRoutingProcedureInstaller createRoutingProcedureInstaller(GlobalModule globalModule,
      DatabaseManager<?> databaseManager) {
    ConnectorPortRegister portRegister = globalModule.getConnectorPortRegister();
    Config config = globalModule.getGlobalConfig();
    LogProvider logProvider = globalModule.getLogService().getInternalLogProvider();
    return new ReadReplicaRoutingProcedureInstaller(databaseManager, portRegister, config,
        logProvider);
  }

  public EditionDatabaseComponents createDatabaseComponents(NamedDatabaseId namedDatabaseId) {
    return new ReadReplicaDatabaseComponents(this.globalModule, this, namedDatabaseId);
  }

  public DatabaseManager<?> createDatabaseManager(GlobalModule globalModule) {
    CatchupComponentsProvider n10004 = this.catchupComponentsProvider;
    Objects.requireNonNull(n10004);
    ReadReplicaDatabaseManager databaseManager =
        new ReadReplicaDatabaseManager(globalModule, this, n10004::createDatabaseComponents,
            globalModule.getFileSystem(),
            globalModule.getPageCache(), this.logProvider, this.globalConfig,
            this.clusterStateLayout);
    globalModule.getGlobalLife().add(databaseManager);
    globalModule.getGlobalDependencies().satisfyDependency(databaseManager);
    this.createDatabaseManagerDependentModules(databaseManager);
    return databaseManager;
  }

  private void createDatabaseManagerDependentModules(ReadReplicaDatabaseManager databaseManager) {
    SystemDbOnlyReplicatedDatabaseEventService databaseEventService = new SystemDbOnlyReplicatedDatabaseEventService(
        this.logProvider);
    LifeSupport globalLife = this.globalModule.getGlobalLife();
    LogService globalLogService = this.globalModule.getLogService();
    FileSystemAbstraction fileSystem = this.globalModule.getFileSystem();
    Dependencies dependencies = this.globalModule.getGlobalDependencies();
    dependencies.satisfyDependency(this.reconciledTxTracker);
    dependencies.satisfyDependencies(databaseEventService);
    Supplier<GraphDatabaseService> systemDbSupplier = () ->
    {
      return databaseManager.getDatabaseContext(
          DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID).orElseThrow().databaseFacade();
    };
    ClusterSystemGraphDbmsModel dbmsModel = new ClusterSystemGraphDbmsModel(systemDbSupplier);
    this.reconcilerModule =
        new ClusteredDbmsReconcilerModule(this.globalModule, databaseManager, databaseEventService,
            this.storageFactory, this.reconciledTxTracker,
            this.panicService, dbmsModel);
    this.topologyService = this
        .createTopologyService(databaseManager, this.reconcilerModule.reconciler(),
            globalLogService);
    this.reconcilerModule.reconciler().registerListener(this.topologyService);
    globalLife.add(dependencies.satisfyDependency(this.topologyService));
    int maxChunkSize = this.globalConfig.get(CausalClusteringSettings.store_copy_chunk_size);
    MultiDatabaseCatchupServerHandler catchupServerHandler =
        new MultiDatabaseCatchupServerHandler(databaseManager, fileSystem, maxChunkSize,
            this.logProvider);
    InstalledProtocolHandler installedProtocolsHandler = new InstalledProtocolHandler();
    Server catchupServer = this.catchupComponentsProvider
        .createCatchupServer(installedProtocolsHandler, catchupServerHandler);
    Optional<Server> backupServerOptional = this.catchupComponentsProvider
        .createBackupServer(installedProtocolsHandler, catchupServerHandler);
    CatchupComponentsRepository catchupComponentsRepository = new CatchupComponentsRepository(
        databaseManager);
    CatchupClientFactory catchupClientFactory = this.catchupComponentsProvider
        .catchupClientFactory();
    globalLife.add(catchupServer);
    Objects.requireNonNull(globalLife);
    backupServerOptional.ifPresent(globalLife::add);
    globalLife.add(this.reconcilerModule);
    this.databaseStartAborter = new DatabaseStartAborter(
        this.globalModule.getGlobalAvailabilityGuard(), dbmsModel,
        this.globalModule.getGlobalClock(),
        Duration.ofSeconds(5L));
    this.readReplicaDatabaseFactory =
        new ReadReplicaDatabaseFactory(this.globalConfig, this.globalModule.getGlobalClock(),
            this.jobScheduler, this.topologyService, this.myIdentity,
            catchupComponentsRepository,
            this.globalModule.getTracers().getPageCursorTracerSupplier(), catchupClientFactory,
            databaseEventService,
            this.storageFactory, this.panicService, this.databaseStartAborter);
  }

  public SystemGraphInitializer createSystemGraphInitializer(GlobalModule globalModule,
      DatabaseManager<?> databaseManager) {
    return globalModule.getGlobalDependencies().satisfyDependency(SystemGraphInitializer.NO_OP);
  }

  public void createSecurityModule(GlobalModule globalModule) {
    Object securityProvider;
    if (this.globalConfig.get(GraphDatabaseSettings.auth_enabled)) {
      EnterpriseSecurityModule securityModule =
          new EnterpriseSecurityModule(globalModule.getLogService().getUserLogProvider(),
              this.globalConfig, this.globalProcedures,
              this.jobScheduler, globalModule.getFileSystem(), globalModule.getGlobalDependencies(),
              globalModule.getTransactionEventListeners());
      securityModule.setup();
      globalModule.getGlobalLife().add(securityModule);
      securityProvider = securityModule;
    } else {
      securityProvider = EnterpriseNoAuthSecurityProvider.INSTANCE;
    }

    this.setSecurityProvider((SecurityProvider) securityProvider);
  }

  public DatabaseStartupController getDatabaseStartupController() {
    return this.databaseStartAborter;
  }

  protected NetworkConnectionTracker createConnectionTracker() {
    return new DefaultNetworkConnectionTracker();
  }

  private TopologyService createTopologyService(
      DatabaseManager<ClusteredDatabaseContext> databaseManager,
      DatabaseStateService databaseStateService,
      LogService logService) {
    DiscoveryMemberFactory discoveryMemberFactory = new DefaultDiscoveryMemberFactory(
        databaseManager, databaseStateService);
    RemoteMembersResolver hostnameResolver = ResolutionResolverFactory
        .chooseResolver(this.globalConfig, logService);
    return this.discoveryServiceFactory
        .readReplicaTopologyService(this.globalConfig, this.logProvider, this.jobScheduler,
            this.myIdentity,
            hostnameResolver, this.sslPolicyLoader, discoveryMemberFactory,
            this.globalModule.getGlobalClock());
  }

  ReadReplicaDatabaseFactory readReplicaDatabaseFactory() {
    return this.readReplicaDatabaseFactory;
  }
}
