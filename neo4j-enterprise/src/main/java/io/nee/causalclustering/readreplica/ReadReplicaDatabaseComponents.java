/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import java.util.function.Function;
import org.neo4j.configuration.Config;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.graphdb.factory.module.edition.context.EditionDatabaseComponents;
import org.neo4j.graphdb.factory.module.id.DatabaseIdContext;
import org.neo4j.graphdb.factory.module.id.IdContextFactory;
import org.neo4j.graphdb.factory.module.id.IdContextFactoryBuilder;
import org.neo4j.io.fs.watcher.DatabaseLayoutWatcher;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.IOLimiter;
import org.neo4j.kernel.database.DatabaseNameLogContext;
import org.neo4j.kernel.database.DatabaseStartupController;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.api.CommitProcessFactory;
import org.neo4j.kernel.impl.api.ReadOnlyTransactionCommitProcess;
import org.neo4j.kernel.impl.constraints.ConstraintSemantics;
import org.neo4j.kernel.impl.factory.AccessCapabilityFactory;
import org.neo4j.kernel.impl.factory.ReadOnly;
import org.neo4j.kernel.impl.factory.StatementLocksFactorySelector;
import org.neo4j.kernel.impl.locking.Locks;
import org.neo4j.kernel.impl.locking.StatementLocksFactory;
import org.neo4j.kernel.impl.query.QueryEngineProvider;
import org.neo4j.kernel.impl.transaction.stats.DatabaseTransactionStats;
import org.neo4j.logging.internal.DatabaseLogService;
import org.neo4j.token.DelegatingTokenHolder;
import org.neo4j.token.ReadOnlyTokenCreator;
import org.neo4j.token.TokenHolders;

public class ReadReplicaDatabaseComponents implements EditionDatabaseComponents {

  private final Locks locksManager;
  private final StatementLocksFactory statementLocksFactory;
  private final DatabaseIdContext idContext;
  private final TokenHolders tokenHolders;
  private final CommitProcessFactory commitProcessFactory;
  private final DatabaseTransactionStats transactionMonitor;
  private final ReadReplicaEditionModule editionModule;
  private final AccessCapabilityFactory accessCapabilityFactory;

  public ReadReplicaDatabaseComponents(GlobalModule globalModule,
      ReadReplicaEditionModule editionModule, NamedDatabaseId namedDatabaseId) {
    this.editionModule = editionModule;
    this.locksManager = new ReadReplicaLockManager();
    Config globalConfig = globalModule.getGlobalConfig();
    DatabaseLogService databaseLogService = new DatabaseLogService(
        new DatabaseNameLogContext(namedDatabaseId), globalModule.getLogService());
    this.statementLocksFactory = (new StatementLocksFactorySelector(this.locksManager, globalConfig,
        databaseLogService)).select();
    IdContextFactory idContextFactory = IdContextFactoryBuilder
        .of(globalModule.getFileSystem(), globalModule.getJobScheduler(), globalConfig).build();
    this.idContext = idContextFactory.createIdContext(namedDatabaseId);
    this.tokenHolders = new TokenHolders(
        new DelegatingTokenHolder(new ReadOnlyTokenCreator(), "PropertyKey"),
        new DelegatingTokenHolder(new ReadOnlyTokenCreator(), "Label"),
        new DelegatingTokenHolder(new ReadOnlyTokenCreator(), "RelationshipType"));
    this.commitProcessFactory = (appender, storageEngine, config) ->
    {
      return new ReadOnlyTransactionCommitProcess();
    };
    this.transactionMonitor = editionModule.createTransactionMonitor();
    this.accessCapabilityFactory = AccessCapabilityFactory.fixed(new ReadOnly());
  }

  public DatabaseIdContext getIdContext() {
    return this.idContext;
  }

  public TokenHolders getTokenHolders() {
    return this.tokenHolders;
  }

  public Function<DatabaseLayout, DatabaseLayoutWatcher> getWatcherServiceFactory() {
    return this.editionModule.getWatcherServiceFactory();
  }

  public IOLimiter getIoLimiter() {
    return this.editionModule.getIoLimiter();
  }

  public ConstraintSemantics getConstraintSemantics() {
    return this.editionModule.getConstraintSemantics();
  }

  public CommitProcessFactory getCommitProcessFactory() {
    return this.commitProcessFactory;
  }

  public Locks getLocks() {
    return this.locksManager;
  }

  public StatementLocksFactory getStatementLocksFactory() {
    return this.statementLocksFactory;
  }

  public DatabaseTransactionStats getTransactionMonitor() {
    return this.transactionMonitor;
  }

  public QueryEngineProvider getQueryEngineProvider() {
    return this.editionModule.getQueryEngineProvider();
  }

  public AccessCapabilityFactory getAccessCapabilityFactory() {
    return this.accessCapabilityFactory;
  }

  public DatabaseStartupController getStartupController() {
    return this.editionModule.getDatabaseStartupController();
  }
}
