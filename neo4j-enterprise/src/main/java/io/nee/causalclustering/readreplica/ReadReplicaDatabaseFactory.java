/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import io.nee.causalclustering.catchup.CatchupClientFactory;
import io.nee.causalclustering.catchup.CatchupComponentsRepository;
import io.nee.causalclustering.common.DatabaseTopologyNotifier;
import io.nee.causalclustering.common.state.ClusterStateStorageFactory;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.core.consensus.schedule.TimerService;
import io.nee.causalclustering.core.state.machines.CommandIndexTracker;
import io.nee.causalclustering.core.state.storage.SimpleStorage;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.error_handling.DatabasePanicker;
import io.nee.causalclustering.error_handling.PanicService;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.monitoring.ThroughputMonitor;
import io.nee.causalclustering.upstream.NoOpUpstreamDatabaseStrategiesLoader;
import io.nee.causalclustering.upstream.UpstreamDatabaseStrategiesLoader;
import io.nee.causalclustering.upstream.UpstreamDatabaseStrategySelector;
import io.nee.causalclustering.upstream.strategies.ConnectToRandomCoreServerStrategy;
import io.nee.dbms.ClusterInternalDbmsOperator;
import io.nee.dbms.DatabaseStartAborter;
import io.nee.dbms.ReplicatedDatabaseEventService;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.function.Supplier;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.io.pagecache.tracing.cursor.PageCursorTracerSupplier;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.DatabaseLogProvider;
import org.neo4j.logging.internal.DatabaseLogService;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.time.SystemNanoClock;

class ReadReplicaDatabaseFactory {

  private final Config config;
  private final SystemNanoClock clock;
  private final JobScheduler jobScheduler;
  private final TopologyService topologyService;
  private final MemberId myIdentity;
  private final CatchupComponentsRepository catchupComponentsRepository;
  private final PageCursorTracerSupplier pageCursorTracerSupplier;
  private final CatchupClientFactory catchupClientFactory;
  private final ReplicatedDatabaseEventService databaseEventService;
  private final ClusterStateStorageFactory clusterStateFactory;
  private final PanicService panicService;
  private final DatabaseStartAborter databaseStartAborter;

  ReadReplicaDatabaseFactory(Config config, SystemNanoClock clock, JobScheduler jobScheduler,
      TopologyService topologyService, MemberId myIdentity,
      CatchupComponentsRepository catchupComponentsRepository,
      PageCursorTracerSupplier pageCursorTracerSupplier,
      CatchupClientFactory catchupClientFactory,
      ReplicatedDatabaseEventService databaseEventService,
      ClusterStateStorageFactory clusterStateFactory,
      PanicService panicService, DatabaseStartAborter databaseStartAborter) {
    this.config = config;
    this.clock = clock;
    this.jobScheduler = jobScheduler;
    this.topologyService = topologyService;
    this.myIdentity = myIdentity;
    this.catchupComponentsRepository = catchupComponentsRepository;
    this.pageCursorTracerSupplier = pageCursorTracerSupplier;
    this.catchupClientFactory = catchupClientFactory;
    this.databaseEventService = databaseEventService;
    this.panicService = panicService;
    this.clusterStateFactory = clusterStateFactory;
    this.databaseStartAborter = databaseStartAborter;
  }

  ReadReplicaDatabase createDatabase(ReadReplicaDatabaseContext databaseContext,
      ClusterInternalDbmsOperator clusterInternalOperator) {
    NamedDatabaseId namedDatabaseId = databaseContext.databaseId();
    Database kernelDatabase = databaseContext.kernelDatabase();
    DatabaseLogService databaseLogService = kernelDatabase.getLogService();
    DatabaseLogProvider internalLogProvider = databaseLogService.getInternalLogProvider();
    DatabaseLogProvider userLogProvider = databaseLogService.getUserLogProvider();
    LifeSupport clusterComponents = new LifeSupport();
    Executor catchupExecutor = this.jobScheduler.executor(Group.CATCHUP_CLIENT);
    CommandIndexTracker commandIndexTracker = databaseContext.dependencies()
        .satisfyDependency(new CommandIndexTracker());
    this.initialiseStatusDescriptionEndpoint(commandIndexTracker, clusterComponents,
        databaseContext.dependencies(), internalLogProvider);
    TimerService timerService = new TimerService(this.jobScheduler, internalLogProvider);
    ConnectToRandomCoreServerStrategy defaultStrategy = new ConnectToRandomCoreServerStrategy();
    defaultStrategy.inject(this.topologyService, this.config, internalLogProvider, this.myIdentity);
    UpstreamDatabaseStrategySelector upstreamDatabaseStrategySelector =
        this.createUpstreamDatabaseStrategySelector(this.myIdentity, this.config,
            internalLogProvider, this.topologyService, defaultStrategy);
    DatabasePanicker panicker = this.panicService.panickerFor(namedDatabaseId);
    ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch =
        this.databaseEventService.getDatabaseEventDispatch(namedDatabaseId);
    CatchupProcessManager catchupProcess =
        new CatchupProcessManager(catchupExecutor, this.catchupComponentsRepository,
            databaseContext, panicker, this.topologyService,
            this.catchupClientFactory, upstreamDatabaseStrategySelector, timerService,
            commandIndexTracker, internalLogProvider,
            this.pageCursorTracerSupplier, this.config, databaseEventDispatch);
    databaseContext.dependencies().satisfyDependency(catchupProcess);
    SimpleStorage<RaftId> raftIdStorage = this.clusterStateFactory
        .createRaftIdStorage(databaseContext.databaseId().name(), internalLogProvider);
    Supplier<CatchupComponentsRepository.CatchupComponents> catchupComponentsSupplier = () ->
    {
      return (CatchupComponentsRepository.CatchupComponents) this.catchupComponentsRepository
          .componentsFor(namedDatabaseId).orElseThrow(() ->
          {
            return new IllegalStateException(
                String.format(
                    "No per database catchup components exist for database %s.",
                    namedDatabaseId
                        .name()));
          });
    };
    ReadReplicaBootstrap bootstrap =
        new ReadReplicaBootstrap(databaseContext, upstreamDatabaseStrategySelector,
            internalLogProvider, userLogProvider, this.topologyService,
            catchupComponentsSupplier, clusterInternalOperator, this.databaseStartAborter);
    RaftIdCheck raftIdCheck = new RaftIdCheck(raftIdStorage, namedDatabaseId);
    DatabaseTopologyNotifier topologyNotifier = new DatabaseTopologyNotifier(namedDatabaseId,
        this.topologyService);
    ReadReplicaPanicHandlers panicHandler = new ReadReplicaPanicHandlers(this.panicService,
        kernelDatabase, clusterInternalOperator);
    return new ReadReplicaDatabase(catchupProcess, kernelDatabase, clusterComponents, bootstrap,
        panicHandler, raftIdCheck, topologyNotifier);
  }

  private UpstreamDatabaseStrategySelector createUpstreamDatabaseStrategySelector(MemberId myself,
      Config config, LogProvider logProvider,
      TopologyService topologyService,
      ConnectToRandomCoreServerStrategy defaultStrategy) {
    Object loader;
    if (config.get(CausalClusteringSettings.multi_dc_license)) {
      loader = new UpstreamDatabaseStrategiesLoader(topologyService, config, myself, logProvider);
      logProvider.getLog(this.getClass()).info("Multi-Data Center option enabled.");
    } else {
      loader = new NoOpUpstreamDatabaseStrategiesLoader();
    }

    return new UpstreamDatabaseStrategySelector(defaultStrategy, (Iterable) loader, logProvider);
  }

  private void initialiseStatusDescriptionEndpoint(CommandIndexTracker commandIndexTracker,
      LifeSupport life, Dependencies dependencies,
      DatabaseLogProvider debugLog) {
    Duration samplingWindow = this.config.get(CausalClusteringSettings.status_throughput_window);
    SystemNanoClock n10003 = this.clock;
    JobScheduler n10004 = this.jobScheduler;
    Objects.requireNonNull(commandIndexTracker);
    ThroughputMonitor throughputMonitor =
        new ThroughputMonitor(debugLog, n10003, n10004, samplingWindow,
            commandIndexTracker::getAppliedCommandIndex);
    life.add(throughputMonitor);
    dependencies.satisfyDependency(throughputMonitor);
  }
}
