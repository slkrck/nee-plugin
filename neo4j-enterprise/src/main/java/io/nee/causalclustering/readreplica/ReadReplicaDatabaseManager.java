/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import io.nee.causalclustering.catchup.CatchupComponentsFactory;
import io.nee.causalclustering.common.ClusterMonitors;
import io.nee.causalclustering.core.state.ClusterStateLayout;
import io.nee.dbms.database.ClusteredDatabaseContext;
import io.nee.dbms.database.ClusteredMultiDatabaseManager;
import java.io.File;
import java.io.IOException;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.api.DatabaseManagementException;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.FileUtils;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.DatabaseCreationContext;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;

public class ReadReplicaDatabaseManager extends ClusteredMultiDatabaseManager {

  protected final ReadReplicaEditionModule edition;

  ReadReplicaDatabaseManager(GlobalModule globalModule, ReadReplicaEditionModule edition,
      CatchupComponentsFactory catchupComponentsFactory,
      FileSystemAbstraction fs, PageCache pageCache, LogProvider logProvider, Config config,
      ClusterStateLayout clusterStateLayout) {
    super(globalModule, edition, catchupComponentsFactory, fs, pageCache, logProvider, config,
        clusterStateLayout);
    this.edition = edition;
  }

  protected ClusteredDatabaseContext createDatabaseContext(NamedDatabaseId namedDatabaseId) {
    Dependencies readReplicaDependencies = new Dependencies(
        this.globalModule.getGlobalDependencies());
    Monitors readReplicaMonitors = ClusterMonitors
        .create(this.globalModule.getGlobalMonitors(), readReplicaDependencies);
    DatabaseCreationContext databaseCreationContext = this
        .newDatabaseCreationContext(namedDatabaseId, readReplicaDependencies, readReplicaMonitors);
    Database kernelDatabase = new Database(databaseCreationContext);
    LogFiles transactionLogs = this.buildTransactionLogs(kernelDatabase.getDatabaseLayout());
    ReadReplicaDatabaseContext databaseContext =
        new ReadReplicaDatabaseContext(kernelDatabase, readReplicaMonitors, readReplicaDependencies,
            this.storeFiles, transactionLogs,
            this.internalDbmsOperator());
    ReadReplicaDatabase readReplicaDatabase = this.edition.readReplicaDatabaseFactory()
        .createDatabase(databaseContext, this.internalDbmsOperator());
    return this.contextFactory
        .create(kernelDatabase, kernelDatabase.getDatabaseFacade(), transactionLogs,
            this.storeFiles, this.logProvider,
            this.catchupComponentsFactory, readReplicaDatabase, readReplicaMonitors);
  }

  protected void dropDatabase(NamedDatabaseId namedDatabaseId, ClusteredDatabaseContext context) {
    super.dropDatabase(namedDatabaseId, context);
    this.cleanupClusterState(namedDatabaseId.name());
  }

  public void cleanupClusterState(String databaseName) {
    try {
      this.deleteRaftId(databaseName);
    } catch (IOException n3) {
      throw new DatabaseManagementException("Was unable to delete cluster state as part of drop. ",
          n3);
    }
  }

  private void deleteRaftId(String databaseName) throws IOException {
    File raftGroupDir = this.clusterStateLayout.raftGroupDir(databaseName);
    File raftIdState = this.clusterStateLayout.raftIdStateFile(databaseName);
    File raftIdStateDir = raftIdState.getParentFile();
    if (!this.fs.deleteFile(raftIdState)) {
      throw new IOException(String.format("Unable to delete file %s when dropping database %s",
          raftIdState.getAbsolutePath(), databaseName));
    } else {
      FileUtils.tryForceDirectory(raftIdStateDir);
      this.fs.deleteRecursively(raftGroupDir);
    }
  }
}
