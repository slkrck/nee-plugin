/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import io.nee.causalclustering.common.state.ClusterStateStorageFactory;
import io.nee.causalclustering.error_handling.PanicService;
import io.nee.dbms.database.ClusteredMultiDatabaseManager;
import java.util.stream.Stream;
import org.neo4j.bolt.txtracking.ReconciledTransactionTracker;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.kernel.database.DatabaseIdRepository;

public class ClusteredDbmsReconcilerModule extends StandaloneDbmsReconcilerModule {

  private final ReplicatedDatabaseEventService databaseEventService;
  private final ClusterInternalDbmsOperator internalOperator;

  public ClusteredDbmsReconcilerModule(GlobalModule globalModule,
      ClusteredMultiDatabaseManager databaseManager,
      ReplicatedDatabaseEventService databaseEventService,
      ClusterStateStorageFactory stateStorageFactory,
      ReconciledTransactionTracker reconciledTxTracker, PanicService panicService,
      ClusterSystemGraphDbmsModel dbmsModel) {
    super(globalModule, databaseManager, reconciledTxTracker,
        createReconciler(globalModule, databaseManager, stateStorageFactory, panicService),
        dbmsModel);
    this.databaseEventService = databaseEventService;
    this.internalOperator = databaseManager.internalDbmsOperator();
  }

  private static ClusteredDbmsReconciler createReconciler(GlobalModule globalModule,
      ClusteredMultiDatabaseManager databaseManager,
      ClusterStateStorageFactory stateStorageFactory, PanicService panicService) {
    return new ClusteredDbmsReconciler(databaseManager, globalModule.getGlobalConfig(),
        globalModule.getLogService().getInternalLogProvider(),
        globalModule.getJobScheduler(), stateStorageFactory, panicService);
  }

  protected Stream<DbmsOperator> operators() {
    return Stream.concat(super.operators(), Stream.of(this.internalOperator));
  }

  protected void registerWithListenerService(GlobalModule globalModule,
      SystemGraphDbmsOperator systemOperator) {
    this.databaseEventService.registerListener(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID,
        new SystemOperatingDatabaseEventListener(systemOperator));
  }
}
