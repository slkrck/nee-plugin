/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.database;

import io.nee.causalclustering.catchup.CatchupComponentsFactory;
import io.nee.causalclustering.catchup.storecopy.StoreFiles;
import io.nee.causalclustering.common.ClusteredDatabaseContextFactory;
import io.nee.causalclustering.core.state.ClusterStateLayout;
import io.nee.dbms.ClusterInternalDbmsOperator;
import java.io.IOException;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.api.DatabaseManagementException;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.graphdb.factory.module.edition.AbstractEditionModule;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.kernel.impl.transaction.log.files.LogFilesBuilder;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public abstract class ClusteredMultiDatabaseManager extends
    MultiDatabaseManager<ClusteredDatabaseContext> {

  protected final FileSystemAbstraction fs;
  protected final ClusteredDatabaseContextFactory contextFactory = DefaultClusteredDatabaseContext::new;
  protected final LogProvider logProvider;
  protected final Log log;
  protected final Config config;
  protected final StoreFiles storeFiles;
  protected final CatchupComponentsFactory catchupComponentsFactory;
  protected final ClusterStateLayout clusterStateLayout;
  private final PageCache pageCache;
  private final ClusterInternalDbmsOperator internalDbmsOperator = new ClusterInternalDbmsOperator();

  public ClusteredMultiDatabaseManager(GlobalModule globalModule, AbstractEditionModule edition,
      CatchupComponentsFactory catchupComponentsFactory,
      FileSystemAbstraction fs, PageCache pageCache, LogProvider logProvider, Config config,
      ClusterStateLayout clusterStateLayout) {
    super(globalModule, edition);
    this.logProvider = logProvider;
    this.fs = fs;
    this.log = logProvider.getLog(this.getClass());
    this.config = config;
    this.pageCache = pageCache;
    this.catchupComponentsFactory = catchupComponentsFactory;
    this.storeFiles = new StoreFiles(fs, pageCache);
    this.clusterStateLayout = clusterStateLayout;
  }

  protected final void startDatabase(NamedDatabaseId namedDatabaseId,
      ClusteredDatabaseContext context) {
    try {
      context = this.recreateContextIfNeeded(namedDatabaseId, context);
      this.log.info("Starting '%s' database.", namedDatabaseId.name());
      context.clusteredDatabase().start();
    } catch (Throwable n4) {
      throw new DatabaseManagementException(
          String.format("Unable to start database `%s`", namedDatabaseId), n4);
    }
  }

  private ClusteredDatabaseContext recreateContextIfNeeded(NamedDatabaseId namedDatabaseId,
      ClusteredDatabaseContext context) throws Exception {
    if (context.clusteredDatabase().hasBeenStarted()) {
      context.clusteredDatabase().stop();
      ClusteredDatabaseContext updatedContext = this.createDatabaseContext(namedDatabaseId);
      this.databaseMap.put(namedDatabaseId, updatedContext);
      return updatedContext;
    } else {
      return context;
    }
  }

  protected final void stopDatabase(NamedDatabaseId namedDatabaseId,
      ClusteredDatabaseContext context) {
    try {
      this.log.info("Stopping '%s' database.", namedDatabaseId.name());
      context.clusteredDatabase().stop();
    } catch (Throwable n4) {
      throw new DatabaseManagementException(
          String.format("An error occurred! Unable to stop database `%s`.", namedDatabaseId), n4);
    }
  }

  public void stopDatabaseBeforeStoreCopy(NamedDatabaseId namedDatabaseId) {
    this.forSingleDatabase(namedDatabaseId, (id, context) ->
    {
      try {
        this.log.info("Stopping '%s' database for store copy.", namedDatabaseId.name());
        context.database().stop();
      } catch (Throwable n5) {
        throw new DatabaseManagementException(
            String.format("Unable to stop database '%s' for store copy.", namedDatabaseId.name()),
            n5);
      }
    });
  }

  public void startDatabaseAfterStoreCopy(NamedDatabaseId namedDatabaseId) {
    this.forSingleDatabase(namedDatabaseId, (id, context) ->
    {
      try {
        this.log.info("Starting '%s' database after store copy.", namedDatabaseId.name());
        context.database().start();
      } catch (Throwable n5) {
        throw new DatabaseManagementException(String
            .format("Unable to start database '%s' after store copy.", namedDatabaseId.name()), n5);
      }
    });
  }

  public abstract void cleanupClusterState(String n1);

  protected final LogFiles buildTransactionLogs(DatabaseLayout dbLayout) {
    try {
      return LogFilesBuilder.activeFilesBuilder(dbLayout, this.fs, this.pageCache)
          .withConfig(this.config).build();
    } catch (IOException n3) {
      throw new RuntimeException(n3);
    }
  }

  public final ClusterInternalDbmsOperator internalDbmsOperator() {
    return this.internalDbmsOperator;
  }
}
