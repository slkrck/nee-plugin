/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.api;

import io.nee.enterprise.edition.EnterpriseEditionModule;
import java.io.File;
import java.util.function.Function;
import org.neo4j.annotations.api.PublicApi;
import org.neo4j.common.Edition;
import org.neo4j.dbms.api.DatabaseManagementServiceBuilder;
import org.neo4j.graphdb.facade.ExternalDependencies;
import org.neo4j.graphdb.facade.GraphDatabaseDependencies;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.graphdb.factory.module.edition.AbstractEditionModule;
import org.neo4j.kernel.impl.factory.DatabaseInfo;

@PublicApi
public class EnterpriseDatabaseManagementServiceBuilder extends DatabaseManagementServiceBuilder {

  public EnterpriseDatabaseManagementServiceBuilder(File homeDirectory) {
    super(homeDirectory);
  }

  public String getEdition() {
    return Edition.ENTERPRISE.toString();
  }

  protected ExternalDependencies databaseDependencies() {
    return GraphDatabaseDependencies.newDependencies().monitors(this.monitors)
        .userLogProvider(this.userLogProvider).dependencies(
            this.dependencies).urlAccessRules(this.urlAccessRules).extensions(this.extensions)
        .databaseEventListeners(this.databaseEventListeners);
  }

  protected DatabaseInfo getDatabaseInfo() {
    return DatabaseInfo.ENTERPRISE;
  }

  protected Function<GlobalModule, AbstractEditionModule> getEditionFactory() {
    return EnterpriseEditionModule::new;
  }
}
