/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.commandline;

import io.nee.dbms.commandline.storeutil.StoreCopy;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import org.neo4j.cli.AbstractCommand;
import org.neo4j.cli.CommandFailedException;
import org.neo4j.cli.Converters.DatabaseNameConverter;
import org.neo4j.cli.ExecutionContext;
import org.neo4j.commandline.dbms.LockChecker;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.ConfigUtils;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.helpers.NormalizedDatabaseName;
import org.neo4j.internal.helpers.Strings;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.layout.Neo4jLayout;
import org.neo4j.kernel.impl.transaction.log.files.TransactionLogFilesHelper;
import org.neo4j.kernel.impl.util.Validators;
import org.neo4j.kernel.internal.locker.FileLockException;
import org.neo4j.kernel.recovery.Recovery;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Help.Visibility;
import picocli.CommandLine.Option;

@Command(name = "copy", header = {"Copy a database and optionally apply filters."}, description = {
    "This command will create a copy of a database."})
public class StoreCopyCommand extends AbstractCommand {

  @ArgGroup(multiplicity = "1")
  private final StoreCopyCommand.SourceOption source = new StoreCopyCommand.SourceOption();
  @Option(names = {"--delete-nodes-with-labels"}, description = {
      "A comma separated list of labels. All nodes that have ANY of the specified labels will be deleted."}, split = ",", paramLabel = "<label>", showDefaultValue = Visibility.NEVER)
  private final List<String> deleteNodesWithLabels = new ArrayList();
  @Option(names = {"--skip-labels"}, description = {
      "A comma separated list of labels to ignore."}, split = ",", paramLabel = "<label>", showDefaultValue = Visibility.NEVER)
  private final List<String> skipLabels = new ArrayList();
  @Option(names = {"--skip-properties"}, description = {
      "A comma separated list of property keys to ignore."}, split = ",", paramLabel = "<property>", showDefaultValue = Visibility.NEVER)
  private final List<String> skipProperties = new ArrayList();
  @Option(names = {"--skip-relationships"}, description = {
      "A comma separated list of relationships to ignore."}, split = ",", paramLabel = "<relationship>", showDefaultValue = Visibility.NEVER)
  private final List<String> skipRelationships = new ArrayList();
  @Option(names = {"--from-path-tx"}, description = {
      "Path to the transaction files, if they are not in the same folder as '--from-path'."}, paramLabel = "<path>")
  private Path sourceTxLogs;
  @Option(names = {"--to-database"}, description = {
      "Name of database to copy to."}, required = true, converter = {DatabaseNameConverter.class})
  private NormalizedDatabaseName database;
  @Option(names = {"--force"}, description = {
      "Force the command to run even if the integrity of the database can not be verified."})
  private boolean force;
  @Option(names = {"--to-format"}, defaultValue = "same", description = {
      "Set the format for the new database. Must be one of ${COMPLETION-CANDIDATES}. 'same' will use the same format as the source. WARNING: If you go from 'high_limit' to 'standard' there is no validation that the data will actually fit."})
  private StoreCopy.FormatEnum format;
  @Option(names = {"--from-pagecache"}, paramLabel = "<size>", defaultValue = "8m", description = {
      "The size of the page cache to use for reading."})
  private String fromPageCacheMemory;
  @Option(names = {"--to-pagecache"}, paramLabel = "<size>", defaultValue = "8m", description = {
      "The size of the page cache to use for writing."})
  private String toPageCacheMemory;

  public StoreCopyCommand(ExecutionContext ctx) {
    super(ctx);
  }

  private static void validateSource(DatabaseLayout fromDatabaseLayout) {
    try {
      Validators.CONTAINS_EXISTING_DATABASE.validate(fromDatabaseLayout.databaseDirectory());
    } catch (IllegalArgumentException n2) {
      throw new CommandFailedException(
          "Database does not exist: " + fromDatabaseLayout.getDatabaseName(), n2);
    }
  }

  private static void validateTarget(DatabaseLayout toDatabaseLayout) {
    File targetFile = toDatabaseLayout.databaseDirectory();
    if (targetFile.exists()) {
      if (!targetFile.isDirectory()) {
        throw new CommandFailedException(
            "Specified path is a file: " + targetFile.getAbsolutePath());
      }

      String[] files = targetFile.list();
      if (files == null || files.length > 0) {
        throw new CommandFailedException(
            "The directory is not empty: " + targetFile.getAbsolutePath());
      }
    } else {
      try {
        Files.createDirectories(targetFile.toPath());
      } catch (IOException n3) {
        throw new CommandFailedException(
            "Unable to create directory: " + targetFile.getAbsolutePath());
      }
    }
  }

  private static void checkDbState(DatabaseLayout databaseLayout, Config additionalConfiguration) {
    if (checkRecoveryState(databaseLayout, additionalConfiguration)) {
      throw new CommandFailedException(Strings.joinAsLines(
          "The database " + databaseLayout.getDatabaseName() + "  was not shut down properly.",
          "Please perform a recovery by starting and stopping the database.",
          "If recovery is not possible, you can force the command to continue with the '--force' flag."));
    }
  }

  private static boolean checkRecoveryState(DatabaseLayout databaseLayout,
      Config additionalConfiguration) {
    try {
      return Recovery.isRecoveryRequired(databaseLayout, additionalConfiguration);
    } catch (Exception n3) {
      throw new CommandFailedException(
          "Failure when checking for recovery state: '%s'." + n3.getMessage(), n3);
    }
  }

  public void execute() throws Exception {
    Config config = this.buildConfig();
    DatabaseLayout fromDatabaseLayout = this.getFromDatabaseLayout(config);
    validateSource(fromDatabaseLayout);
    DatabaseLayout toDatabaseLayout = Neo4jLayout.of(config).databaseLayout(this.database.name());
    validateTarget(toDatabaseLayout);

    try {
      Closeable ignored = LockChecker.checkDatabaseLock(fromDatabaseLayout);

      try {
        if (!this.force) {
          checkDbState(fromDatabaseLayout, config);
        }

        try {
          Closeable ignored2 = LockChecker.checkDatabaseLock(toDatabaseLayout);

          try {
            StoreCopy copy =
                new StoreCopy(fromDatabaseLayout, config, this.format, this.deleteNodesWithLabels,
                    this.skipLabels, this.skipProperties,
                    this.skipRelationships, this.verbose, this.ctx.out());

            try {
              copy.copyTo(toDatabaseLayout, this.fromPageCacheMemory, this.toPageCacheMemory);
            } catch (Exception n10) {
              throw new CommandFailedException("There was a problem during copy.", n10);
            }
          } catch (Throwable n11) {
            if (ignored2 != null) {
              try {
                ignored2.close();
              } catch (Throwable n9) {
                n11.addSuppressed(n9);
              }
            }

            throw n11;
          }

          if (ignored2 != null) {
            ignored2.close();
          }
        } catch (FileLockException n12) {
          throw new CommandFailedException("Unable to lock destination.", n12);
        }
      } catch (Throwable n13) {
        if (ignored != null) {
          try {
            ignored.close();
          } catch (Throwable n8) {
            n13.addSuppressed(n8);
          }
        }

        throw n13;
      }

      if (ignored != null) {
        ignored.close();
      }
    } catch (FileLockException n14) {
      throw new CommandFailedException(
          "The database is in use. Stop database '" + fromDatabaseLayout.getDatabaseName()
              + "' and try again.", n14);
    }
  }

  private DatabaseLayout getFromDatabaseLayout(Config config) {
    if (this.source.path != null) {
      this.source.path = this.source.path.toAbsolutePath();
      if (!Files.isDirectory(this.source.path)) {
        throw new CommandFailedException(
            "The path doesn't exist or not a directory: " + this.source.path);
      } else if ((new TransactionLogFilesHelper(this.ctx.fs(), this.source.path.toFile()))
          .getLogFiles().length > 0) {
        return DatabaseLayout.ofFlat(this.source.path.toFile());
      } else if (this.sourceTxLogs == null) {
        throw new CommandFailedException(
            "Unable to find transaction logs, please specify the location with '--from-path-tx'.");
      } else {
        Path databaseName = this.source.path.getFileName();
        if (!databaseName.equals(this.sourceTxLogs.getFileName())) {
          throw new CommandFailedException(
              "The directory with data and the directory with transaction logs need to have the same name.");
        } else {
          this.sourceTxLogs = this.sourceTxLogs.toAbsolutePath();
          Config cfg =
              Config.newBuilder()
                  .set(GraphDatabaseSettings.default_database, databaseName.toString())
                  .set(GraphDatabaseSettings.neo4j_home,
                      this.source.path.getParent())
                  .set(GraphDatabaseSettings.databases_root_path, this.source.path.getParent()).set(
                  GraphDatabaseSettings.transaction_logs_root_path, this.sourceTxLogs.getParent())
                  .build();
          return DatabaseLayout.of(cfg);
        }
      }
    } else {
      return Neo4jLayout.of(config).databaseLayout(this.source.database.name());
    }
  }

  private Config buildConfig() {
    Config cfg = Config.newBuilder().fromFileNoThrow(this.ctx.confDir().resolve("neo4j.conf"))
        .set(GraphDatabaseSettings.neo4j_home,
            this.ctx.homeDir()).build();
    ConfigUtils.disableAllConnectors(cfg);
    return cfg;
  }

  private static class SourceOption {

    @Option(names = {"--from-database"}, description = {
        "Name of database to copy from."}, required = true, converter = {
        DatabaseNameConverter.class})
    private NormalizedDatabaseName database;
    @Option(names = {"--from-path"}, description = {
        "Path to the database to copy from."}, required = true)
    private Path path;
  }
}
