/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.commandline.storeutil;

import java.util.Iterator;
import org.neo4j.internal.kernel.api.TokenRead;
import org.neo4j.internal.kernel.api.exceptions.LabelNotFoundKernelException;
import org.neo4j.internal.kernel.api.exceptions.PropertyKeyIdNotFoundKernelException;
import org.neo4j.internal.kernel.api.exceptions.RelationshipTypeIdNotFoundKernelException;
import org.neo4j.token.TokenHolders;
import org.neo4j.token.api.NamedToken;
import org.neo4j.token.api.TokenNotFoundException;

public class ReadOnlyTokenRead implements TokenRead {

  private final TokenHolders tokenHolders;

  ReadOnlyTokenRead(TokenHolders tokenHolders) {
    this.tokenHolders = tokenHolders;
  }

  public int nodeLabel(String name) {
    return this.tokenHolders.labelTokens().getIdByName(name);
  }

  public String nodeLabelName(int labelId) throws LabelNotFoundKernelException {
    try {
      return this.tokenHolders.labelTokens().getTokenById(labelId).name();
    } catch (TokenNotFoundException n3) {
      throw new LabelNotFoundKernelException(labelId, n3);
    }
  }

  public int relationshipType(String name) {
    return this.tokenHolders.relationshipTypeTokens().getIdByName(name);
  }

  public String relationshipTypeName(int relationshipTypeId)
      throws RelationshipTypeIdNotFoundKernelException {
    try {
      return this.tokenHolders.relationshipTypeTokens().getTokenById(relationshipTypeId).name();
    } catch (TokenNotFoundException n3) {
      throw new RelationshipTypeIdNotFoundKernelException(relationshipTypeId, n3);
    }
  }

  public int propertyKey(String name) {
    return this.tokenHolders.propertyKeyTokens().getIdByName(name);
  }

  public String propertyKeyName(int propertyKeyId) throws PropertyKeyIdNotFoundKernelException {
    try {
      return this.tokenHolders.propertyKeyTokens().getTokenById(propertyKeyId).name();
    } catch (TokenNotFoundException n3) {
      throw new PropertyKeyIdNotFoundKernelException(propertyKeyId, n3);
    }
  }

  public Iterator<NamedToken> labelsGetAllTokens() {
    return this.tokenHolders.labelTokens().getAllTokens().iterator();
  }

  public Iterator<NamedToken> propertyKeyGetAllTokens() {
    return this.tokenHolders.propertyKeyTokens().getAllTokens().iterator();
  }

  public Iterator<NamedToken> relationshipTypesGetAllTokens() {
    return this.tokenHolders.relationshipTypeTokens().getAllTokens().iterator();
  }

  public int labelCount() {
    return this.tokenHolders.labelTokens().size();
  }

  public int propertyKeyCount() {
    return this.tokenHolders.propertyKeyTokens().size();
  }

  public int relationshipTypeCount() {
    return this.tokenHolders.relationshipTypeTokens().size();
  }
}
