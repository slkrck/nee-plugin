/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.commandline.storeutil;

import java.io.IOException;
import org.neo4j.internal.batchimport.InputIterator;
import org.neo4j.internal.batchimport.input.InputChunk;
import org.neo4j.kernel.impl.store.CommonAbstractStore;

public abstract class LenientInputChunkIterator implements InputIterator {

  private final int batchSize;
  private final long highId;
  private long id;

  LenientInputChunkIterator(CommonAbstractStore<?, ?> store) {
    this.batchSize = store.getRecordsPerPage() * 10;

    try {
      this.highId = (long) store.getRecordsPerPage() * (store.getLastPageId() + 1L);
    } catch (IOException n3) {
      throw new RuntimeException(n3);
    }
  }

  public synchronized boolean next(InputChunk chunk) {
    if (this.id >= this.highId) {
      return false;
    } else {
      long startId = this.id;
      this.id = Long.min(this.highId, startId + (long) this.batchSize);
      ((LenientStoreInputChunk) chunk).setChunkRange(startId, this.id);
      return true;
    }
  }

  public void close() {
  }
}
