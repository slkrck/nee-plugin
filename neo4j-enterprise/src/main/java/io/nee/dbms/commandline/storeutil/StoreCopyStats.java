/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.commandline.storeutil;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;
import org.neo4j.internal.schema.ConstraintDescriptor;
import org.neo4j.internal.schema.IndexDescriptor;
import org.neo4j.kernel.impl.store.record.PrimitiveRecord;
import org.neo4j.logging.Log;
import org.neo4j.values.storable.Value;

class StoreCopyStats {

  private static final int MAX_LOG_LENGTH = 120;
  final LongAdder count = new LongAdder();
  final LongAdder unused = new LongAdder();
  final LongAdder removed = new LongAdder();
  private final long startTime;
  private final Log log;

  StoreCopyStats(Log log) {
    this.log = log;
    this.startTime = System.nanoTime();
  }

  private static int percent(long part, long total) {
    return total == 0L ? 0 : (int) (100.0D * (double) part / (double) total);
  }

  private static String trimToMaxLength(String value) {
    return value.length() <= 120 ? value : value.substring(0, 120) + "..";
  }

  void addCorruptToken(String type, int id) {
    this.log.error("%s(%d): Missing token name", type, id);
  }

  void brokenPropertyToken(String type, PrimitiveRecord record, Value newPropertyValue,
      int keyIndexId) {
    String value = newPropertyValue.toString();
    this.log.error("%s(%d): Ignoring property with missing token(%d). Value of the property is %s.",
        type, record.getId(), keyIndexId, trimToMaxLength(value));
  }

  void brokenPropertyChain(String type, PrimitiveRecord record, Exception e) {
    this.log
        .error(String.format("%s(%d): Ignoring broken property chain.", type, record.getId()), e);
  }

  void brokenRecord(String type, long id, Exception e) {
    this.log.error(String.format("%s(%d): Ignoring broken record.", type, id), e);
  }

  void printSummary() {
    long seconds = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - this.startTime);
    long count = this.count.sum();
    long unused = this.unused.sum();
    long removed = this.removed.sum();
    this.log.info(
        "Import summary: Copying of %d records took %d seconds (%d rec/s). Unused Records %d (%d%%) Removed Records %d (%d%%)",
        count, seconds, count / Math.max(1L, seconds), unused, percent(unused, count), removed,
        percent(removed, count));
  }

  void invalidIndex(IndexDescriptor indexDescriptor, Exception e) {
    this.log.error(
        String.format("Unable to format statement for index '%s'%n", indexDescriptor.getName()), e);
  }

  void invalidConstraint(ConstraintDescriptor constraintDescriptor, Exception e) {
    this.log.error(String
            .format("Unable to format statement for constraint '%s'%n", constraintDescriptor.getName()),
        e);
  }
}
