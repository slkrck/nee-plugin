/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.commandline.storeutil;

import java.io.IOException;
import org.neo4j.internal.batchimport.input.Group;
import org.neo4j.internal.batchimport.input.InputEntityVisitor;
import org.neo4j.kernel.impl.store.NodeLabelsField;
import org.neo4j.kernel.impl.store.NodeStore;
import org.neo4j.kernel.impl.store.PropertyStore;
import org.neo4j.kernel.impl.store.record.NodeRecord;
import org.neo4j.kernel.impl.store.record.RecordLoad;
import org.neo4j.token.TokenHolders;
import org.neo4j.token.api.TokenHolder;

class LenientNodeReader extends LenientStoreInputChunk {

  private final NodeStore nodeStore;
  private final NodeRecord record;
  private final StoreCopyFilter.TokenLookup tokenLookup;

  LenientNodeReader(StoreCopyStats stats, NodeStore nodeStore, PropertyStore propertyStore,
      TokenHolders tokenHolders, StoreCopyFilter storeCopyFilter) {
    super(stats, propertyStore, tokenHolders, nodeStore.openPageCursorForReading(0L),
        storeCopyFilter);
    this.nodeStore = nodeStore;
    this.record = nodeStore.newRecord();
    TokenHolder tokenHolder = tokenHolders.labelTokens();
    this.tokenLookup = (id) ->
    {
      return tokenHolder.getTokenById(id).name();
    };
  }

  void readAndVisit(long id, InputEntityVisitor visitor) throws IOException {
    this.nodeStore.getRecordByCursor(id, this.record, RecordLoad.NORMAL, this.cursor);
    if (this.record.inUse()) {
      this.nodeStore.ensureHeavy(this.record);
      long[] labelIds = NodeLabelsField.parseLabelsField(this.record).get(this.nodeStore);
      if (!this.storeCopyFilter.shouldDeleteNode(labelIds)) {
        String[] labels = this.storeCopyFilter.filterLabels(labelIds, this.tokenLookup);
        visitor.id(id, Group.GLOBAL);
        visitor.labels(labels);
        this.visitPropertyChainNoThrow(visitor, this.record);
        visitor.endOfEntity();
      } else {
        this.stats.removed.increment();
      }
    } else {
      this.stats.unused.increment();
    }
  }

  String recordType() {
    return "Node";
  }
}
