/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.commandline.storeutil;

import java.io.IOException;
import java.util.Iterator;
import org.neo4j.internal.batchimport.input.InputChunk;
import org.neo4j.internal.batchimport.input.InputEntityVisitor;
import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.kernel.impl.store.InvalidRecordException;
import org.neo4j.kernel.impl.store.PropertyStore;
import org.neo4j.kernel.impl.store.record.PrimitiveRecord;
import org.neo4j.kernel.impl.store.record.PropertyBlock;
import org.neo4j.kernel.impl.store.record.PropertyRecord;
import org.neo4j.kernel.impl.store.record.Record;
import org.neo4j.kernel.impl.store.record.RecordLoad;
import org.neo4j.token.TokenHolders;
import org.neo4j.token.api.TokenNotFoundException;
import org.neo4j.values.storable.Value;

public abstract class LenientStoreInputChunk implements InputChunk {

  protected final StoreCopyStats stats;
  protected final TokenHolders tokenHolders;
  protected final PageCursor cursor;
  final StoreCopyFilter storeCopyFilter;
  private final PropertyStore propertyStore;
  private long id;
  private long endId;

  LenientStoreInputChunk(StoreCopyStats stats, PropertyStore propertyStore,
      TokenHolders tokenHolders, PageCursor cursor, StoreCopyFilter storeCopyFilter) {
    this.stats = stats;
    this.propertyStore = propertyStore;
    this.tokenHolders = tokenHolders;
    this.cursor = cursor;
    this.storeCopyFilter = storeCopyFilter;
  }

  void setChunkRange(long startId, long endId) {
    this.id = startId;
    this.endId = endId;
  }

  public boolean next(InputEntityVisitor visitor) {
    if (this.id < this.endId) {
      this.stats.count.increment();

      try {
        this.readAndVisit(this.id, visitor);
      } catch (Exception n3) {
        if (n3 instanceof InvalidRecordException && n3.getMessage().endsWith("not in use")) {
          this.stats.unused.increment();
        } else {
          this.stats.removed.increment();
          this.stats.brokenRecord(this.recordType(), this.id, n3);
        }
      }

      ++this.id;
      return true;
    } else {
      return false;
    }
  }

  public void close() throws IOException {
    this.cursor.close();
  }

  abstract void readAndVisit(long n1, InputEntityVisitor n3) throws IOException;

  abstract String recordType();

  void visitPropertyChainNoThrow(InputEntityVisitor visitor, PrimitiveRecord record) {
    try {
      if (record.getNextProp() == (long) Record.NO_NEXT_PROPERTY.intValue()) {
        return;
      }

      PropertyRecord propertyRecord;
      for (long nextProp = record.getNextProp(); !Record.NO_NEXT_PROPERTY.is(nextProp);
          nextProp = propertyRecord.getNextProp()) {
        propertyRecord = this.propertyStore
            .getRecord(nextProp, this.propertyStore.newRecord(), RecordLoad.NORMAL);
        Iterator n6 = propertyRecord.iterator();

        while (n6.hasNext()) {
          PropertyBlock propBlock = (PropertyBlock) n6.next();
          this.propertyStore.ensureHeavy(propBlock);
          if (this.storeCopyFilter.shouldKeepProperty(propBlock.getKeyIndexId())) {
            try {
              String key = this.tokenHolders.propertyKeyTokens()
                  .getTokenById(propBlock.getKeyIndexId()).name();
              Value propertyValue = propBlock.newPropertyValue(this.propertyStore);
              visitor.property(key, propertyValue.asObject());
            } catch (TokenNotFoundException n10) {
              this.stats.brokenPropertyToken(this.recordType(), record,
                  propBlock.newPropertyValue(this.propertyStore),
                  propBlock.getKeyIndexId());
            }
          }
        }
      }
    } catch (Exception n11) {
      this.stats.brokenPropertyChain(this.recordType(), record, n11);
    }
  }
}
