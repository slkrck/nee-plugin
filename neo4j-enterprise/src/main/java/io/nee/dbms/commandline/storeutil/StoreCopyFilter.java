/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.commandline.storeutil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;
import org.eclipse.collections.api.set.primitive.ImmutableIntSet;
import org.eclipse.collections.impl.factory.primitive.IntSets;
import org.neo4j.token.api.TokenNotFoundException;

class StoreCopyFilter {

  private final StoreCopyStats stats;
  private final ImmutableIntSet deleteNodesWithLabelsIds;
  private final Set<Integer> skipLabelsIds;
  private final Set<Integer> skipPropertyIds;
  private final Set<Integer> skipRelationshipIds;

  StoreCopyFilter(StoreCopyStats stats, int[] deleteNodesWithLabelsIds, int[] skipLabelsIds,
      int[] skipPropertyIds, int[] skipRelationshipIds) {
    this.stats = stats;
    this.deleteNodesWithLabelsIds = IntSets.immutable.of(deleteNodesWithLabelsIds);
    this.skipLabelsIds = ConcurrentHashMap.newKeySet();
    this.skipPropertyIds = ConcurrentHashMap.newKeySet();
    this.skipRelationshipIds = ConcurrentHashMap.newKeySet();
    IntStream n10000 = Arrays.stream(skipLabelsIds);
    Set n10001 = this.skipLabelsIds;
    Objects.requireNonNull(n10001);
    n10000.forEach(n10001::add);
    n10000 = Arrays.stream(skipPropertyIds);
    n10001 = this.skipPropertyIds;
    Objects.requireNonNull(n10001);
    n10000.forEach(n10001::add);
    n10000 = Arrays.stream(skipRelationshipIds);
    n10001 = this.skipRelationshipIds;
    Objects.requireNonNull(n10001);
    n10000.forEach(n10001::add);
  }

  boolean shouldDeleteNode(long[] labelIds) {
    long[] n2 = labelIds;
    int n3 = labelIds.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      long labelId = n2[n4];
      if (this.deleteNodesWithLabelsIds.contains(Math.toIntExact(labelId))) {
        return true;
      }
    }

    return false;
  }

  String[] filterLabels(long[] labelIds, StoreCopyFilter.TokenLookup tokenLookup) {
    ArrayList<String> labels = new ArrayList(labelIds.length);
    long[] n4 = labelIds;
    int n5 = labelIds.length;

    for (int n6 = 0; n6 < n5; ++n6) {
      long longLabelId = n4[n6];
      int labelId = Math.toIntExact(longLabelId);
      if (!this.skipLabelsIds.contains(labelId)) {
        try {
          labels.add(tokenLookup.lookup(labelId));
        } catch (TokenNotFoundException n11) {
          this.skipLabelsIds.add(labelId);
          this.stats.addCorruptToken("Label", labelId);
        }
      }
    }

    return labels.toArray(new String[0]);
  }

  boolean shouldKeepProperty(int keyIndexId) {
    return !this.skipPropertyIds.contains(keyIndexId);
  }

  String filterRelationship(int relTypeId, StoreCopyFilter.TokenLookup tokenLookup) {
    if (!this.skipRelationshipIds.contains(relTypeId)) {
      try {
        return tokenLookup.lookup(relTypeId);
      } catch (TokenNotFoundException n4) {
        this.skipRelationshipIds.add(relTypeId);
        this.stats.addCorruptToken("Relationship", relTypeId);
      }
    }

    return null;
  }

  @FunctionalInterface
  interface TokenLookup {

    String lookup(int n1) throws TokenNotFoundException;
  }
}
