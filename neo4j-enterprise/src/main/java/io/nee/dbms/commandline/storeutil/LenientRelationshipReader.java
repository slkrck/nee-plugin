/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.commandline.storeutil;

import java.io.IOException;
import org.neo4j.internal.batchimport.input.Group;
import org.neo4j.internal.batchimport.input.InputEntityVisitor;
import org.neo4j.kernel.impl.store.PropertyStore;
import org.neo4j.kernel.impl.store.RelationshipStore;
import org.neo4j.kernel.impl.store.record.RecordLoad;
import org.neo4j.kernel.impl.store.record.RelationshipRecord;
import org.neo4j.token.TokenHolders;
import org.neo4j.token.api.TokenHolder;

class LenientRelationshipReader extends LenientStoreInputChunk {

  private final RelationshipStore relationshipStore;
  private final RelationshipRecord record;
  private final StoreCopyFilter.TokenLookup tokenLookup;

  LenientRelationshipReader(StoreCopyStats stats, RelationshipStore relationshipStore,
      PropertyStore propertyStore, TokenHolders tokenHolders,
      StoreCopyFilter storeCopyFilter) {
    super(stats, propertyStore, tokenHolders, relationshipStore.openPageCursorForReading(0L),
        storeCopyFilter);
    this.relationshipStore = relationshipStore;
    this.record = relationshipStore.newRecord();
    TokenHolder tokenHolder = tokenHolders.relationshipTypeTokens();
    this.tokenLookup = (id) ->
    {
      return tokenHolder.getTokenById(id).name();
    };
  }

  void readAndVisit(long id, InputEntityVisitor visitor) throws IOException {
    this.relationshipStore.getRecordByCursor(id, this.record, RecordLoad.NORMAL, this.cursor);
    if (this.record.inUse()) {
      this.relationshipStore.ensureHeavy(this.record);
      int relType = this.record.getType();
      String relName = this.storeCopyFilter.filterRelationship(relType, this.tokenLookup);
      if (relName != null) {
        visitor.type(relName);
        visitor.startId(this.record.getFirstNode(), Group.GLOBAL);
        visitor.endId(this.record.getSecondNode(), Group.GLOBAL);
        this.visitPropertyChainNoThrow(visitor, this.record);
        visitor.endOfEntity();
      } else {
        this.stats.removed.increment();
      }
    } else {
      this.stats.unused.increment();
    }
  }

  String recordType() {
    return "Relationship";
  }
}
