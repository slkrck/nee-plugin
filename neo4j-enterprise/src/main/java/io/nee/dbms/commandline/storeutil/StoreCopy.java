/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.commandline.storeutil;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.neo4j.batchinsert.internal.TransactionLogsInitializer;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.internal.batchimport.AdditionalInitialIds;
import org.neo4j.internal.batchimport.BatchImporter;
import org.neo4j.internal.batchimport.BatchImporterFactory;
import org.neo4j.internal.batchimport.Configuration;
import org.neo4j.internal.batchimport.ImportLogic;
import org.neo4j.internal.batchimport.input.Collector;
import org.neo4j.internal.batchimport.input.Groups;
import org.neo4j.internal.batchimport.input.IdType;
import org.neo4j.internal.batchimport.input.Input;
import org.neo4j.internal.batchimport.input.Input.Estimates;
import org.neo4j.internal.batchimport.input.InputChunk;
import org.neo4j.internal.batchimport.staging.ExecutionMonitor;
import org.neo4j.internal.batchimport.staging.ExecutionMonitors;
import org.neo4j.internal.batchimport.staging.SpectrumExecutionMonitor;
import org.neo4j.internal.id.ScanOnOpenReadOnlyIdGeneratorFactory;
import org.neo4j.internal.kernel.api.TokenRead;
import org.neo4j.internal.recordstorage.SchemaRuleAccess;
import org.neo4j.internal.recordstorage.StoreTokens;
import org.neo4j.internal.schema.ConstraintDescriptor;
import org.neo4j.internal.schema.IndexDescriptor;
import org.neo4j.io.fs.DefaultFileSystemAbstraction;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.mem.MemoryAllocator;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.io.pagecache.impl.SingleFilePageSwapperFactory;
import org.neo4j.io.pagecache.impl.muninn.MuninnPageCache;
import org.neo4j.io.pagecache.tracing.PageCacheTracer;
import org.neo4j.io.pagecache.tracing.cursor.DefaultPageCursorTracerSupplier;
import org.neo4j.io.pagecache.tracing.cursor.context.EmptyVersionContextSupplier;
import org.neo4j.kernel.impl.scheduler.JobSchedulerFactory;
import org.neo4j.kernel.impl.store.CommonAbstractStore;
import org.neo4j.kernel.impl.store.NeoStores;
import org.neo4j.kernel.impl.store.NodeStore;
import org.neo4j.kernel.impl.store.PropertyStore;
import org.neo4j.kernel.impl.store.RelationshipStore;
import org.neo4j.kernel.impl.store.SchemaStore;
import org.neo4j.kernel.impl.store.StoreFactory;
import org.neo4j.kernel.impl.store.StoreHeader;
import org.neo4j.kernel.impl.store.format.RecordFormatSelector;
import org.neo4j.kernel.impl.store.format.RecordFormats;
import org.neo4j.kernel.impl.store.format.standard.Standard;
import org.neo4j.kernel.impl.store.record.AbstractBaseRecord;
import org.neo4j.logging.DuplicatingLogProvider;
import org.neo4j.logging.FormattedLogProvider;
import org.neo4j.logging.Level;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;
import org.neo4j.logging.internal.SimpleLogService;
import org.neo4j.memory.EmptyMemoryTracker;
import org.neo4j.procedure.builtin.SchemaStatementProcedure;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.token.TokenHolders;
import org.neo4j.token.api.TokenHolder;

public class StoreCopy {

  private final DatabaseLayout from;
  private final Config config;
  private final boolean verbose;
  private final StoreCopy.FormatEnum format;
  private final List<String> deleteNodesWithLabels;
  private final List<String> skipLabels;
  private final List<String> skipProperties;
  private final List<String> skipRelationships;
  private final PrintStream out;
  private StoreCopyFilter storeCopyFilter;
  private TokenHolders tokenHolders;
  private NodeStore nodeStore;
  private PropertyStore propertyStore;
  private RelationshipStore relationshipStore;
  private StoreCopyStats stats;

  public StoreCopy(DatabaseLayout from, Config config, StoreCopy.FormatEnum format,
      List<String> deleteNodesWithLabels, List<String> skipLabels,
      List<String> skipProperties, List<String> skipRelationships, boolean verbose,
      PrintStream out) {
    this.from = from;
    this.config = config;
    this.format = format;
    this.deleteNodesWithLabels = deleteNodesWithLabels;
    this.skipLabels = skipLabels;
    this.skipProperties = skipProperties;
    this.skipRelationships = skipRelationships;
    this.out = out;
    this.verbose = verbose;
  }

  private static PageCache createPageCache(FileSystemAbstraction fileSystem, String memory,
      JobScheduler jobScheduler) {
    SingleFilePageSwapperFactory swapperFactory = new SingleFilePageSwapperFactory();
    swapperFactory.open(fileSystem);
    MemoryAllocator memoryAllocator = MemoryAllocator
        .createAllocator(memory, EmptyMemoryTracker.INSTANCE);
    return new MuninnPageCache(swapperFactory, memoryAllocator, PageCacheTracer.NULL,
        DefaultPageCursorTracerSupplier.INSTANCE,
        EmptyVersionContextSupplier.EMPTY, jobScheduler);
  }

  private static Path getLogFilePath(Config config) {
    return config.get(GraphDatabaseSettings.logs_directory).resolve(
        String.format("neo4j-admin-copy-%s.log",
            (new SimpleDateFormat("yyyy-MM-dd.HH.mm.ss")).format(new Date())));
  }


  private static Map<String, String> getSchemaStatements(StoreCopyStats stats,
      SchemaStore schemaStore, TokenHolders tokenHolders) {
    TokenRead tokenRead = new ReadOnlyTokenRead(tokenHolders);
    SchemaRuleAccess schemaRuleAccess = SchemaRuleAccess
        .getSchemaRuleAccess(schemaStore, tokenHolders);
    Map<String, IndexDescriptor> indexes = new HashMap();
    List<ConstraintDescriptor> constraints = new ArrayList();
    schemaRuleAccess.indexesGetAll().forEachRemaining((i) ->
    {
      indexes.put(i.getName(), i);
    });
    Iterator<ConstraintDescriptor> n10000 = schemaRuleAccess.constraintsGetAllIgnoreMalformed();
    Objects.requireNonNull(constraints);
    n10000.forEachRemaining(constraints::add);
    Map<String, String> schemaStatements = new HashMap();
    Iterator n8 = indexes.entrySet().iterator();

    String statement;
    while (n8.hasNext()) {
      Entry entry = (Entry) n8.next();

      try {
        statement = SchemaStatementProcedure
            .createStatement(tokenRead, (IndexDescriptor) entry.getValue());
      } catch (Exception n13) {
        stats.invalidIndex((IndexDescriptor) entry.getValue(), n13);
        continue;
      }

      schemaStatements.put((String) entry.getKey(), statement);
    }

    n8 = constraints.iterator();

    while (n8.hasNext()) {
      ConstraintDescriptor constraint = (ConstraintDescriptor) n8.next();

      try {
        Objects.requireNonNull(indexes);
        statement = SchemaStatementProcedure.createStatement(indexes::get, tokenRead, constraint);
      } catch (Exception n12) {
        stats.invalidConstraint(constraint, n12);
        continue;
      }

      schemaStatements.put(constraint.getName(), statement);
    }

    return schemaStatements;
  }

  private static RecordFormats setupRecordFormats(NeoStores neoStores,
      StoreCopy.FormatEnum format) {
    if (format == StoreCopy.FormatEnum.same) {
      return neoStores.getRecordFormats();
    } else if (format == StoreCopy.FormatEnum.high_limit) {
      try {
        return RecordFormatSelector
            .selectForConfig(Config.defaults(GraphDatabaseSettings.record_format, "high_limit"),
                NullLogProvider.getInstance());
      } catch (IllegalArgumentException n3) {
        throw new IllegalArgumentException(
            "Unable to load high-limit format, make sure you are using the correct version of neo4j.",
            n3);
      }
    } else {
      return Standard.LATEST_RECORD_FORMATS;
    }
  }

  private static StoreCopyFilter convertFilter(List<String> deleteNodesWithLabels,
      List<String> skipLabels, List<String> skipProperties,
      List<String> skipRelationships, TokenHolders tokenHolders, StoreCopyStats stats) {
    int[] deleteNodesWithLabelsIds = getTokenIds(deleteNodesWithLabels, tokenHolders.labelTokens());
    int[] skipLabelsIds = getTokenIds(skipLabels, tokenHolders.labelTokens());
    int[] skipPropertyIds = getTokenIds(skipProperties, tokenHolders.propertyKeyTokens());
    int[] skipRelationshipIds = getTokenIds(skipRelationships,
        tokenHolders.relationshipTypeTokens());
    return new StoreCopyFilter(stats, deleteNodesWithLabelsIds, skipLabelsIds, skipPropertyIds,
        skipRelationshipIds);
  }

  private static int[] getTokenIds(List<String> tokenNames, TokenHolder tokenHolder) {
    int[] labelIds = new int[tokenNames.size()];
    int i = 0;

    int labelId;
    for (Iterator n4 = tokenNames.iterator(); n4.hasNext(); labelIds[i++] = labelId) {
      String tokenName = (String) n4.next();
      labelId = tokenHolder.getIdByName(tokenName);
      if (labelId == -1) {
        throw new RuntimeException("Unable to find token: " + tokenName);
      }
    }

    return labelIds;
  }

  private static long storeSize(
      CommonAbstractStore<? extends AbstractBaseRecord, ? extends StoreHeader> store) {
    try {
      return store.getStoreSize();
    } catch (IOException n2) {
      throw new UncheckedIOException(n2);
    }
  }

  public void copyTo(DatabaseLayout toDatabaseLayout, String fromPageCacheMemory,
      String toPageCacheMemory) throws Exception {
    Path logFilePath = getLogFilePath(this.config);
    BufferedOutputStream logFile = new BufferedOutputStream(Files.newOutputStream(logFilePath));

    try {
      LogProvider logProvider = new DuplicatingLogProvider(this.getLog(logFile),
          this.getLog(this.out));
      Log log = logProvider.getLog("StoreCopy");
      DefaultFileSystemAbstraction fs = new DefaultFileSystemAbstraction();

      try {
        JobScheduler scheduler = JobSchedulerFactory.createInitialisedScheduler();

        try {
          PageCache fromPageCache = createPageCache(fs, fromPageCacheMemory, scheduler);

          try {
            PageCache toPageCache = createPageCache(fs, toPageCacheMemory, scheduler);

            try {
              NeoStores neoStores = (new StoreFactory(this.from, this.config,
                  new ScanOnOpenReadOnlyIdGeneratorFactory(), fromPageCache, fs,
                  NullLogProvider.getInstance())).openAllNeoStores();

              try {
                this.out.println("Starting to copy store, output will be saved to: " + logFilePath
                    .toAbsolutePath());
                this.nodeStore = neoStores.getNodeStore();
                this.propertyStore = neoStores.getPropertyStore();
                this.relationshipStore = neoStores.getRelationshipStore();
                this.tokenHolders = StoreTokens.readOnlyTokenHolders(neoStores);
                this.stats = new StoreCopyStats(log);
                SchemaStore schemaStore = neoStores.getSchemaStore();
                this.storeCopyFilter = convertFilter(this.deleteNodesWithLabels, this.skipLabels,
                    this.skipProperties, this.skipRelationships,
                    this.tokenHolders, this.stats);
                RecordFormats recordFormats = setupRecordFormats(neoStores, this.format);
                ExecutionMonitor executionMonitor =
                    this.verbose ? new SpectrumExecutionMonitor(2L, TimeUnit.SECONDS, this.out, 100)
                        : ExecutionMonitors.defaultVisible();
                log.info("### Copy Data ###");
                log.info("Source: %s (page cache %s)", this.from.databaseDirectory(),
                    fromPageCacheMemory);
                log.info("Target: %s (page cache %s)", toDatabaseLayout.databaseDirectory(),
                    toPageCacheMemory);
                log.info(
                    "Empty database created, will start importing readable data from the source.");
                BatchImporter batchImporter =
                    BatchImporterFactory.withHighestPriority()
                        .instantiate(toDatabaseLayout, fs, toPageCache, Configuration.DEFAULT,
                            new SimpleLogService(logProvider),
                            executionMonitor, AdditionalInitialIds.EMPTY,
                            this.config, recordFormats, ImportLogic.NO_MONITOR,
                            null, Collector.EMPTY,
                            TransactionLogsInitializer.INSTANCE);
                batchImporter.doImport(
                    Input.input(this::nodeIterator, this::relationshipIterator, IdType.INTEGER,
                        this.getEstimates(), new Groups()));
                this.stats.printSummary();
                log.info("### Extracting schema ###");
                log.info("Trying to extract schema...");
                Map<String, String> schemaStatements = getSchemaStatements(this.stats, schemaStore,
                    this.tokenHolders);
                log.info(
                    "... found %d schema definition. The following can be used to recreate the schema:",
                    schemaStatements.size());
                String n10001 = System.lineSeparator();
                log.info(n10001 + System.lineSeparator() + String
                    .join(";" + System.lineSeparator(), schemaStatements.values()));
                log.info(
                    "You have to manually apply the above commands to the database when it is stared to recreate the indexes and constraints. The commands are saved to "
                        +
                        logFilePath.toAbsolutePath() + " as well for reference.");
              } catch (Throwable n24) {
                if (neoStores != null) {
                  try {
                    neoStores.close();
                  } catch (Throwable n23) {
                    n24.addSuppressed(n23);
                  }
                }

                throw n24;
              }

              if (neoStores != null) {
                neoStores.close();
              }
            } catch (Throwable n25) {
              if (toPageCache != null) {
                try {
                  toPageCache.close();
                } catch (Throwable n22) {
                  n25.addSuppressed(n22);
                }
              }

              throw n25;
            }

            if (toPageCache != null) {
              toPageCache.close();
            }
          } catch (Throwable n26) {
            if (fromPageCache != null) {
              try {
                fromPageCache.close();
              } catch (Throwable n21) {
                n26.addSuppressed(n21);
              }
            }

            throw n26;
          }

          if (fromPageCache != null) {
            fromPageCache.close();
          }
        } catch (Throwable n27) {
          if (scheduler != null) {
            try {
              scheduler.close();
            } catch (Throwable n20) {
              n27.addSuppressed(n20);
            }
          }

          throw n27;
        }

        if (scheduler != null) {
          scheduler.close();
        }
      } catch (Throwable n28) {
        try {
          fs.close();
        } catch (Throwable n19) {
          n28.addSuppressed(n19);
        }

        throw n28;
      }

      fs.close();
    } catch (Throwable n29) {
      try {
        logFile.close();
      } catch (Throwable n18) {
        n29.addSuppressed(n18);
      }

      throw n29;
    }

    logFile.close();
  }

  private LogProvider getLog(OutputStream out) {
    return FormattedLogProvider
        .withZoneId(this.config.get(GraphDatabaseSettings.db_timezone).getZoneId())
        .withDefaultLogLevel(
            this.verbose ? Level.DEBUG : Level.INFO).toOutputStream(out);
  }

  private Estimates getEstimates() {
    long propertyStoreSize =
        storeSize(this.propertyStore) / 2L + storeSize(this.propertyStore.getStringStore()) / 2L +
            storeSize(this.propertyStore.getArrayStore()) / 2L;
    return Input.knownEstimates(this.nodeStore.getNumberOfIdsInUse(),
        this.relationshipStore.getNumberOfIdsInUse(),
        this.propertyStore.getNumberOfIdsInUse(), this.propertyStore.getNumberOfIdsInUse(),
        propertyStoreSize / 2L,
        propertyStoreSize / 2L,
        this.tokenHolders.labelTokens().size());
  }

  private LenientInputChunkIterator nodeIterator() {
    return new LenientInputChunkIterator(this.nodeStore) {
      public InputChunk newChunk() {
        return new LenientNodeReader(StoreCopy.this.stats, StoreCopy.this.nodeStore,
            StoreCopy.this.propertyStore, StoreCopy.this.tokenHolders,
            StoreCopy.this.storeCopyFilter);
      }
    };
  }

  private LenientInputChunkIterator relationshipIterator() {
    return new LenientInputChunkIterator(this.relationshipStore) {
      public InputChunk newChunk() {
        return new LenientRelationshipReader(StoreCopy.this.stats, StoreCopy.this.relationshipStore,
            StoreCopy.this.propertyStore,
            StoreCopy.this.tokenHolders, StoreCopy.this.storeCopyFilter);
      }
    };
  }

  public enum FormatEnum {
    same,
    standard,
    high_limit
  }
}
