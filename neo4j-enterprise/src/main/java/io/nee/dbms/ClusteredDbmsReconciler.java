/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import io.nee.causalclustering.common.state.ClusterStateStorageFactory;
import io.nee.causalclustering.core.state.storage.SimpleStorage;
import io.nee.causalclustering.error_handling.DatabasePanicker;
import io.nee.causalclustering.error_handling.PanicService;
import io.nee.causalclustering.identity.RaftId;
import io.nee.dbms.database.ClusteredMultiDatabaseManager;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.api.DatabaseManagementException;
import org.neo4j.kernel.database.DatabaseIdFactory;
import org.neo4j.kernel.database.DatabaseNameLogContext;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.DatabaseLogProvider;
import org.neo4j.scheduler.JobScheduler;

public class ClusteredDbmsReconciler extends DbmsReconciler {

  private final ClusteredMultiDatabaseManager databaseManager;
  private final LogProvider logProvider;
  private final ClusterStateStorageFactory stateStorageFactory;
  private final PanicService panicService;

  ClusteredDbmsReconciler(ClusteredMultiDatabaseManager databaseManager, Config config,
      LogProvider logProvider, JobScheduler scheduler,
      ClusterStateStorageFactory stateStorageFactory, PanicService panicService) {
    super(databaseManager, config, logProvider, scheduler);
    this.databaseManager = databaseManager;
    this.logProvider = logProvider;
    this.stateStorageFactory = stateStorageFactory;
    this.panicService = panicService;
  }

  protected EnterpriseDatabaseState getReconcilerEntryFor(NamedDatabaseId namedDatabaseId) {
    return this.currentStates.getOrDefault(namedDatabaseId.name(), this.initial(namedDatabaseId));
  }

  private EnterpriseDatabaseState initial(NamedDatabaseId namedDatabaseId) {
    Optional<RaftId> raftIdOpt = this
        .readRaftIdForDatabase(namedDatabaseId, this.databaseLogProvider(namedDatabaseId));
    if (raftIdOpt.isPresent()) {
      RaftId raftId = raftIdOpt.get();
      NamedDatabaseId previousDatabaseId = DatabaseIdFactory
          .from(namedDatabaseId.name(), raftId.uuid());
      if (!Objects.equals(namedDatabaseId, previousDatabaseId)) {
        return EnterpriseDatabaseState.unknown(previousDatabaseId);
      }
    }

    return EnterpriseDatabaseState.initial(namedDatabaseId);
  }

  private Optional<RaftId> readRaftIdForDatabase(NamedDatabaseId namedDatabaseId,
      DatabaseLogProvider logProvider) {
    String databaseName = namedDatabaseId.name();
    SimpleStorage<RaftId> raftIdStorage = this.stateStorageFactory
        .createRaftIdStorage(databaseName, logProvider);
    if (!raftIdStorage.exists()) {
      return Optional.empty();
    } else {
      try {
        return Optional.ofNullable(raftIdStorage.readState());
      } catch (IOException n6) {
        throw new DatabaseManagementException(String
            .format("Unable to read potentially dirty cluster state while starting %s.",
                databaseName));
      }
    }
  }

  protected Transitions prepareLifecycleTransitionSteps() {
    Transitions standaloneTransitions = super.prepareLifecycleTransitionSteps();
    Transitions clusteredTransitions = Transitions.builder().from(EnterpriseOperatorState.UNKNOWN)
        .to(EnterpriseOperatorState.DROPPED).doTransitions(
            this::logCleanupAndDrop).from(EnterpriseOperatorState.STORE_COPYING)
        .to(EnterpriseOperatorState.DROPPED).doTransitions(this::stop,
            this::drop)
        .from(EnterpriseOperatorState.STORE_COPYING).to(EnterpriseOperatorState.STOPPED)
        .doTransitions(this::stop).from(
            EnterpriseOperatorState.STORE_COPYING).to(EnterpriseOperatorState.STARTED)
        .doTransitions(this::startAfterStoreCopy).from(
            EnterpriseOperatorState.STARTED).to(EnterpriseOperatorState.STORE_COPYING)
        .doTransitions(this::stopBeforeStoreCopy).build();
    return standaloneTransitions.extendWith(clusteredTransitions);
  }

  protected void panicDatabase(NamedDatabaseId namedDatabaseId, Throwable error) {
    DatabasePanicker databasePanicker = this.panicService.panickerFor(namedDatabaseId);
    databasePanicker.panic(error);
  }

  private EnterpriseDatabaseState startAfterStoreCopy(NamedDatabaseId namedDatabaseId) {
    this.databaseManager.startDatabaseAfterStoreCopy(namedDatabaseId);
    return new EnterpriseDatabaseState(namedDatabaseId, EnterpriseOperatorState.STARTED);
  }

  private EnterpriseDatabaseState stopBeforeStoreCopy(NamedDatabaseId namedDatabaseId) {
    this.databaseManager.stopDatabaseBeforeStoreCopy(namedDatabaseId);
    return new EnterpriseDatabaseState(namedDatabaseId, EnterpriseOperatorState.STORE_COPYING);
  }

  private EnterpriseDatabaseState logCleanupAndDrop(NamedDatabaseId namedDatabaseId) {
    Log log = this.logProvider.getLog(this.getClass());
    log.warn(String.format(
        "Pre-existing cluster state found with an unexpected id %s. This may indicate a previous DROP operation for %s did not complete. Cleanup of both the database and cluster-sate has been attempted. You may need to re-seed",
        namedDatabaseId.databaseId().uuid(), namedDatabaseId.name()));
    this.databaseManager.dropDatabase(namedDatabaseId);
    return new EnterpriseDatabaseState(namedDatabaseId, EnterpriseOperatorState.DROPPED);
  }

  private DatabaseLogProvider databaseLogProvider(NamedDatabaseId namedDatabaseId) {
    return new DatabaseLogProvider(new DatabaseNameLogContext(namedDatabaseId), this.logProvider);
  }
}
