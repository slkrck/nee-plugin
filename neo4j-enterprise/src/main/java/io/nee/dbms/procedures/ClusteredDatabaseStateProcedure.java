/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.procedures;

import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.discovery.DiscoveryServerInfo;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.RoleInfo;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import io.nee.causalclustering.identity.MemberId;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;
import org.neo4j.collection.RawIterator;
import org.neo4j.dbms.DatabaseStateService;
import org.neo4j.dbms.OperatorState;
import org.neo4j.dbms.procedures.DatabaseStateProcedure;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.kernel.api.ResourceTracker;
import org.neo4j.kernel.api.procedure.Context;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.values.AnyValue;

public class ClusteredDatabaseStateProcedure extends DatabaseStateProcedure {

  private final TopologyService topologyService;
  private final MemberId myself;
  private final DatabaseStateService stateService;

  public ClusteredDatabaseStateProcedure(DatabaseIdRepository idRepository,
      TopologyService topologyService, DatabaseStateService stateService) {
    super(idRepository);
    this.topologyService = topologyService;
    this.myself = topologyService.memberId();
    this.stateService = stateService;
  }

  public RawIterator<AnyValue[], ProcedureException> apply(Context ctx, AnyValue[] input,
      ResourceTracker resourceTracker) throws ProcedureException {
    NamedDatabaseId databaseId = this.extractDatabaseId(input);
    Map<MemberId, CoreServerInfo> coreServerInfos = this.topologyService.allCoreServers();
    Map<MemberId, ReadReplicaInfo> rrServerInfos = this.topologyService.allReadReplicas();
    Map<MemberId, DiscoveryDatabaseState> coreStates = this.topologyService
        .allCoreStatesForDatabase(databaseId);
    Stream<AnyValue[]> coreResultRows = coreStates.keySet().stream().flatMap((member) ->
    {
      return this.resultRowsForExistingMembers(coreServerInfos, member,
          databaseId);
    });
    Map<MemberId, DiscoveryDatabaseState> rrStates = this.topologyService
        .allReadReplicaStatesForDatabase(databaseId);
    Stream<AnyValue[]> rrResultRows = rrStates.keySet().stream().flatMap((member) ->
    {
      return this.resultRowsForExistingMembers(rrServerInfos, member, databaseId);
    });
    return RawIterator.wrap(Stream.concat(coreResultRows, rrResultRows).iterator());
  }

  private Stream<AnyValue[]> resultRowsForExistingMembers(
      Map<MemberId, ? extends DiscoveryServerInfo> serverInfos, MemberId memberId,
      NamedDatabaseId namedDatabaseId) {
    return Stream.ofNullable((DiscoveryServerInfo) serverInfos.get(memberId)).map((discoveryInfo) ->
    {
      return this
          .resultRowFactory(namedDatabaseId, this.topologyService,
              memberId, discoveryInfo);
    });
  }

  private AnyValue[] resultRowFactory(NamedDatabaseId namedDatabaseId,
      TopologyService topologyService, MemberId memberId, DiscoveryServerInfo serverInfo) {
    RoleInfo role = topologyService.lookupRole(namedDatabaseId, memberId);
    String roleString = role.name().toLowerCase();
    String address = serverInfo.boltAddress().toString();
    boolean isMe = Objects.equals(memberId, this.myself);
    DatabaseStateService stateService = isMe ? this.stateService
        : new ClusteredDatabaseStateProcedure.RemoteDatabaseStateService((id) ->
        {
          return topologyService
              .lookupDatabaseState(
                  id,
                  memberId);
        });
    return this.resultRowFactory(namedDatabaseId, roleString, address, stateService);
  }

  private static class RemoteDatabaseStateService implements DatabaseStateService {

    private final Function<NamedDatabaseId, DiscoveryDatabaseState> stateLookup;

    RemoteDatabaseStateService(Function<NamedDatabaseId, DiscoveryDatabaseState> stateLookup) {
      this.stateLookup = stateLookup;
    }

    public OperatorState stateOfDatabase(NamedDatabaseId namedDatabaseId) {
      return this.stateLookup.apply(namedDatabaseId).operatorState();
    }

    public Optional<Throwable> causeOfFailure(NamedDatabaseId namedDatabaseId) {
      return this.stateLookup.apply(namedDatabaseId).failure();
    }
  }
}
