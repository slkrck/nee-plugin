/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.enterprise.edition;

import io.nee.causalclustering.catchup.MultiDatabaseCatchupServerHandler;
import io.nee.causalclustering.common.PipelineBuilders;
import io.nee.causalclustering.common.TransactionBackupServiceProvider;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.core.SupportedProtocolCreator;
import io.nee.causalclustering.net.InstalledProtocolHandler;
import io.nee.causalclustering.net.Server;
import io.nee.dbms.DatabaseStartAborter;
import io.nee.dbms.EnterpriseSystemGraphDbmsModel;
import io.nee.dbms.StandaloneDbmsReconcilerModule;
import io.nee.dbms.database.EnterpriseMultiDatabaseManager;
import io.nee.dbms.database.MultiDatabaseManager;
import io.nee.fabric.auth.FabricAuthManagerWrapper;
import io.nee.fabric.bolt.BoltFabricDatabaseManagementService;
import io.nee.fabric.bootstrap.FabricServicesBootstrap;
import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.executor.FabricExecutor;
import io.nee.fabric.localdb.FabricDatabaseManager;
import io.nee.fabric.localdb.FabricSystemGraphInitializer;
import io.nee.fabric.routing.FabricRoutingProcedureInstaller;
import io.nee.fabric.transaction.TransactionManager;
import io.nee.kernel.enterprise.api.security.EnterpriseAuthManager;
import io.nee.kernel.enterprise.api.security.provider.EnterpriseNoAuthSecurityProvider;
import io.nee.kernel.impl.enterprise.EnterpriseConstraintSemantics;
import io.nee.kernel.impl.enterprise.transaction.log.checkpoint.ConfigurableIOLimiter;
import io.nee.kernel.impl.net.DefaultNetworkConnectionTracker;
import io.nee.procedure.enterprise.builtin.EnterpriseBuiltInDbmsProcedures;
import io.nee.procedure.enterprise.builtin.EnterpriseBuiltInProcedures;
import io.nee.procedure.enterprise.builtin.SettingsWhitelist;
import io.nee.server.security.enterprise.EnterpriseSecurityModule;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import org.neo4j.bolt.dbapi.BoltGraphDatabaseManagementServiceSPI;
import org.neo4j.bolt.dbapi.BoltGraphDatabaseServiceSPI;
import org.neo4j.bolt.dbapi.CustomBookmarkFormatParser;
import org.neo4j.bolt.txtracking.DefaultReconciledTransactionTracker;
import org.neo4j.bolt.txtracking.ReconciledTransactionTracker;
import org.neo4j.bolt.txtracking.SimpleReconciledTransactionTracker;
import org.neo4j.bolt.txtracking.TransactionIdTracker;
import org.neo4j.collection.Dependencies;
import org.neo4j.common.DependencyResolver;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.cypher.internal.javacompat.EnterpriseCypherEngineProvider;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.dbms.api.DatabaseNotFoundException;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.dbms.database.StandaloneDatabaseContext;
import org.neo4j.dbms.database.SystemGraphInitializer;
import org.neo4j.exceptions.KernelException;
import org.neo4j.function.Predicates;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.graphdb.factory.module.edition.CommunityEditionModule;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.api.Kernel;
import org.neo4j.kernel.api.net.NetworkConnectionTracker;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import org.neo4j.kernel.api.security.AuthManager;
import org.neo4j.kernel.api.security.provider.SecurityProvider;
import org.neo4j.kernel.availability.UnavailableException;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.DatabaseStartupController;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.constraints.ConstraintSemantics;
import org.neo4j.kernel.impl.factory.StatementLocksFactorySelector;
import org.neo4j.kernel.impl.locking.Locks;
import org.neo4j.kernel.impl.locking.StatementLocksFactory;
import org.neo4j.kernel.impl.query.QueryEngineProvider;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.procedure.builtin.routing.BaseRoutingProcedureInstaller;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.time.SystemNanoClock;
import org.neo4j.token.DelegatingTokenHolder;
import org.neo4j.token.TokenHolders;

/*
import io.nee.fabric.auth.FabricAuthManagerWrapper;
import io.nee.fabric.bolt.BoltFabricDatabaseManagementService;
import io.nee.fabric.bootstrap.FabricServicesBootstrap;
import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.executor.FabricExecutor;
import io.nee.fabric.localdb.FabricDatabaseManager;
import io.nee.fabric.localdb.FabricSystemGraphInitializer;
import io.nee.fabric.routing.FabricRoutingProcedureInstaller;
import io.nee.fabric.transaction.TransactionManager;

 */
//import org.neo4j.cypher.internal.javacompat.EnterpriseCypherEngineProvider;

public class EnterpriseEditionModule extends CommunityEditionModule implements
    AbstractEnterpriseEditionModule {

  private final ReconciledTransactionTracker reconciledTxTracker;
  private final FabricDatabaseManager fabricDatabaseManager;
  private final FabricServicesBootstrap fabricServicesBootstrap;
  private DatabaseStartAborter databaseStartAborter;

  public EnterpriseEditionModule(GlobalModule globalModule) {
    this(globalModule, globalModule.getGlobalDependencies());
  }

  protected EnterpriseEditionModule(GlobalModule globalModule, Dependencies dependencies) {
    super(globalModule);
    this.satisfyEnterpriseOnlyDependencies(globalModule);
    this.ioLimiter = new ConfigurableIOLimiter(globalModule.getGlobalConfig());
    this.reconciledTxTracker = new DefaultReconciledTransactionTracker(
        globalModule.getLogService());
    this.fabricServicesBootstrap = new FabricServicesBootstrap(globalModule.getGlobalLife(),
        dependencies, globalModule.getLogService());
    this.fabricDatabaseManager = dependencies.resolveDependency(FabricDatabaseManager.class);
    SettingsWhitelist settingsWhiteList = new SettingsWhitelist(globalModule.getGlobalConfig());
    dependencies.satisfyDependency(settingsWhiteList);
  }

  public QueryEngineProvider getQueryEngineProvider() {
    return new EnterpriseCypherEngineProvider();
  }

  public void registerEditionSpecificProcedures(GlobalProcedures globalProcedures,
      DatabaseManager<?> databaseManager) throws KernelException {
    super.registerEditionSpecificProcedures(globalProcedures, databaseManager);
    globalProcedures.registerProcedure(EnterpriseBuiltInDbmsProcedures.class, true);
    globalProcedures.registerProcedure(EnterpriseBuiltInProcedures.class, true);
    this.fabricServicesBootstrap.registerProcedures(globalProcedures);
  }

  protected Predicate<String> fileWatcherFileNameFilter() {
    return Predicates.any(new Predicate[]{(fileName) ->
    {
      return ((String) fileName).startsWith("neostore.transaction.db");
    }, (fileName2) ->
    {
      return ((String) fileName2).endsWith(".cacheprof");
    }});
  }

  protected ConstraintSemantics createSchemaRuleVerifier() {
    return new EnterpriseConstraintSemantics();
  }

  protected NetworkConnectionTracker createConnectionTracker() {
    return new DefaultNetworkConnectionTracker();
  }

  protected StatementLocksFactory createStatementLocksFactory(Locks locks, Config config,
      LogService logService) {
    return (new StatementLocksFactorySelector(locks, config, logService)).select();
  }

  protected Function<NamedDatabaseId, TokenHolders> createTokenHolderProvider(
      GlobalModule platform) {
    Config globalConfig = platform.getGlobalConfig();
    return (databaseId) ->
    {
      DatabaseManager<?> databaseManager = (DatabaseManager) platform.getGlobalDependencies()
          .resolveDependency(DatabaseManager.class);
      Supplier<Kernel> kernelSupplier = () ->
      {
        DatabaseContext databaseContext = databaseManager.getDatabaseContext(databaseId)
            .orElseThrow(() ->
            {
              return new IllegalStateException(
                  String.format(
                      "Database %s not found.",
                      databaseId
                          .name()));
            });
        return (Kernel) databaseContext.dependencies().resolveDependency(Kernel.class);
      };
      return new TokenHolders(new DelegatingTokenHolder(
          createPropertyKeyCreator(globalConfig, databaseId, kernelSupplier), "PropertyKey"),
          new DelegatingTokenHolder(createLabelIdCreator(globalConfig, databaseId, kernelSupplier),
              "Label"),
          new DelegatingTokenHolder(
              createRelationshipTypeCreator(globalConfig, databaseId, kernelSupplier),
              "RelationshipType"));
    };
  }

  public DatabaseManager<?> createDatabaseManager(GlobalModule globalModule) {
    EnterpriseMultiDatabaseManager databaseManager = new EnterpriseMultiDatabaseManager(
        globalModule, this);
    this.createDatabaseManagerDependentModules(databaseManager);
    globalModule.getGlobalLife().add(databaseManager);
    globalModule.getGlobalDependencies().satisfyDependency(databaseManager);
    Supplier<GraphDatabaseService> systemDbSupplier = () ->
    {
      return databaseManager.getDatabaseContext(
          DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID).orElseThrow().databaseFacade();
    };
    EnterpriseSystemGraphDbmsModel dbmsModel = new EnterpriseSystemGraphDbmsModel(systemDbSupplier);
    StandaloneDbmsReconcilerModule reconcilerModule =
        new StandaloneDbmsReconcilerModule(globalModule, databaseManager, this.reconciledTxTracker,
            dbmsModel);
    this.databaseStateService = reconcilerModule.reconciler();
    this.databaseStartAborter =
        new DatabaseStartAborter(globalModule.getGlobalAvailabilityGuard(), dbmsModel,
            globalModule.getGlobalClock(), Duration.ofSeconds(5L));
    globalModule.getGlobalLife().add(reconcilerModule);
    globalModule.getGlobalDependencies().satisfyDependency(this.reconciledTxTracker);
    return databaseManager;
  }

  private void createDatabaseManagerDependentModules(
      MultiDatabaseManager<StandaloneDatabaseContext> databaseManager) {
    this.initBackupIfNeeded(this.globalModule, this.globalModule.getGlobalConfig(),
        databaseManager);
  }

  public SystemGraphInitializer createSystemGraphInitializer(GlobalModule globalModule,
      DatabaseManager<?> databaseManager) {
    SystemGraphInitializer initializer =
        tryResolveOrCreate(SystemGraphInitializer.class,
            globalModule.getExternalDependencyResolver(), () ->
            {
              return new FabricSystemGraphInitializer(databaseManager,
                  globalModule.getGlobalConfig(), this.fabricDatabaseManager);
            });
    return globalModule.getGlobalDependencies().satisfyDependency(
        globalModule.getGlobalLife().add(initializer));
  }

  public void createSecurityModule(GlobalModule globalModule) {
    Object securityProvider;
    if (globalModule.getGlobalConfig().get(GraphDatabaseSettings.auth_enabled)) {
      EnterpriseSecurityModule securityModule =
          new EnterpriseSecurityModule(globalModule.getLogService().getUserLogProvider(),
              globalModule.getGlobalConfig(), this.globalProcedures,
              globalModule.getJobScheduler(), globalModule.getFileSystem(),
              globalModule.getGlobalDependencies(),
              globalModule.getTransactionEventListeners());
      securityModule.setup();
      globalModule.getGlobalLife().add(securityModule);
      securityProvider = securityModule;
    } else {
      securityProvider = EnterpriseNoAuthSecurityProvider.INSTANCE;
    }

    this.setSecurityProvider((SecurityProvider) securityProvider);
  }

  protected BaseRoutingProcedureInstaller createRoutingProcedureInstaller(GlobalModule globalModule,
      DatabaseManager<?> databaseManager) {
    ConnectorPortRegister portRegister = globalModule.getConnectorPortRegister();
    Config config = globalModule.getGlobalConfig();
    LogProvider logProvider = globalModule.getLogService().getInternalLogProvider();
    FabricConfig fabricConfig = globalModule.getGlobalDependencies()
        .resolveDependency(FabricConfig.class);
    return new FabricRoutingProcedureInstaller(databaseManager, portRegister, config,
        this.fabricDatabaseManager, fabricConfig, logProvider);
  }

  public AuthManager getBoltAuthManager(DependencyResolver dependencyResolver) {
    AuthManager authManager = super.getBoltAuthManager(dependencyResolver);
    FabricConfig fabricConfig = dependencyResolver.resolveDependency(FabricConfig.class);
    if (!fabricConfig.isEnabled()) {
      return authManager;
    } else if (!(authManager instanceof EnterpriseAuthManager)) {
      throw new IllegalStateException("Unexpected type of Auth manager: " + authManager.getClass());
    } else {
      return new FabricAuthManagerWrapper((EnterpriseAuthManager) authManager);
    }
  }

  public DatabaseStartupController getDatabaseStartupController() {
    return this.databaseStartAborter;
  }

  public BoltGraphDatabaseManagementServiceSPI createBoltDatabaseManagementServiceProvider(
      Dependencies dependencies,
      DatabaseManagementService managementService, Monitors monitors,
      SystemNanoClock clock, LogService logService) {
    FabricConfig config = dependencies.resolveDependency(FabricConfig.class);
    final BoltGraphDatabaseManagementServiceSPI kernelDatabaseManagementService =
        super.createBoltDatabaseManagementServiceProvider(dependencies, managementService, monitors,
            clock, logService);
    if (!config.isEnabled()) {
      return kernelDatabaseManagementService;
    } else {
      FabricExecutor fabricExecutor = dependencies.resolveDependency(FabricExecutor.class);
      TransactionManager transactionManager = dependencies
          .resolveDependency(TransactionManager.class);
      final FabricDatabaseManager fabricDatabaseManager = dependencies
          .resolveDependency(FabricDatabaseManager.class);
      Config serverConfig = dependencies.resolveDependency(Config.class);
      Duration bookmarkTimeout = serverConfig.get(GraphDatabaseSettings.bookmark_ready_timeout);
      SimpleReconciledTransactionTracker reconciledTxTracker = new SimpleReconciledTransactionTracker(
          managementService, logService);
      TransactionIdTracker transactionIdTracker = new TransactionIdTracker(managementService,
          reconciledTxTracker, monitors, clock);
      final BoltFabricDatabaseManagementService fabricDatabaseManagementService =
          new BoltFabricDatabaseManagementService(fabricExecutor, config, transactionManager,
              fabricDatabaseManager, bookmarkTimeout,
              transactionIdTracker);
      return new BoltGraphDatabaseManagementServiceSPI() {
        public BoltGraphDatabaseServiceSPI database(String databaseName)
            throws UnavailableException, DatabaseNotFoundException {
          return fabricDatabaseManager.isFabricDatabase(databaseName)
              ? fabricDatabaseManagementService.database(databaseName)
              : kernelDatabaseManagementService.database(databaseName);
        }

        public Optional<CustomBookmarkFormatParser> getCustomBookmarkFormatParser() {
          return fabricDatabaseManagementService.getCustomBookmarkFormatParser();
        }
      };
    }
  }

  private void initBackupIfNeeded(GlobalModule globalModule, Config config,
      DatabaseManager<StandaloneDatabaseContext> databaseManager) {
    FileSystemAbstraction fs = globalModule.getFileSystem();
    JobScheduler jobScheduler = globalModule.getJobScheduler();
    ConnectorPortRegister portRegister = globalModule.getConnectorPortRegister();
    LogProvider internalLogProvider = globalModule.getLogService().getInternalLogProvider();
    SupportedProtocolCreator supportedProtocolCreator = new SupportedProtocolCreator(config,
        internalLogProvider);
    PipelineBuilders pipelineBuilders = new PipelineBuilders(this.sslPolicyLoader);
    int maxChunkSize = config.get(CausalClusteringSettings.store_copy_chunk_size);
    TransactionBackupServiceProvider backupServiceProvider =
        new TransactionBackupServiceProvider(internalLogProvider,
            supportedProtocolCreator.getSupportedCatchupProtocolsFromConfiguration(),
            supportedProtocolCreator.createSupportedModifierProtocols(),
            pipelineBuilders.backupServer(),
            new MultiDatabaseCatchupServerHandler(databaseManager, fs, maxChunkSize,
                internalLogProvider),
            new InstalledProtocolHandler(),
            jobScheduler, portRegister);
    Optional<Server> backupServer = backupServiceProvider.resolveIfBackupEnabled(config);
    LifeSupport n10001 = globalModule.getGlobalLife();
    Objects.requireNonNull(n10001);
    backupServer.ifPresent(n10001::add);
  }
}
