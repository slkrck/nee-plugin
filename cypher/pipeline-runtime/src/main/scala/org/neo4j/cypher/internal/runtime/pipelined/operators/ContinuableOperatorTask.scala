/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

trait ContinuableOperatorTask extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask with org.neo4j.cypher.internal.runtime.WithHeapUsageEstimation {
  def canContinue: scala.Boolean

  def close(operatorCloser: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser,
            resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
    null
  }

  def producingWorkUnitEvent: org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent

  def filterCancelledArguments(operatorCloser: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser): scala.Boolean

  protected def closeInput(operatorCloser: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser): scala.Unit

  protected def closeCursors(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit
}

trait ContinuableOperatorTaskWithAccumulator[DATA <: scala.AnyRef, ACC <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[DATA]] extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTask {
  val accumulator: ACC

  override def filterCancelledArguments(operatorCloser: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser): scala.Boolean = {
    false
  }

  override def producingWorkUnitEvent: org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent = {
    null
  }

  override def setExecutionEvent(event: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit = {
    null
  }

  override def estimatedHeapUsage: scala.Long = {
    0
  }

  protected override def closeInput(operatorCloser: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser): scala.Unit = {
    null
  }

  protected override def closeCursors(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
    null
  }
}

trait ContinuableOperatorTaskWithMorsel extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTask {
  val inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext

  override def filterCancelledArguments(operatorCloser: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser): scala.Boolean = {
    false
  }

  override def producingWorkUnitEvent: org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent = {
    null
  }

  override def estimatedHeapUsage: scala.Long = {
    0
  }

  protected override def closeInput(operatorCloser: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser): scala.Unit = {
    null
  }
}

trait ContinuableOperatorTaskWithMorselAndAccumulator[DATA <: scala.AnyRef, ACC <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[DATA]] extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorsel with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithAccumulator[DATA, ACC] {
  override def filterCancelledArguments(operatorCloser: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser): scala.Boolean = {
    false
  }

  override def producingWorkUnitEvent: org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent = {
    null
  }

  protected override def closeInput(operatorCloser: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser): scala.Unit = {
    null
  }
}

object ContinuableOperatorTaskWithMorselGenerator extends scala.AnyRef {
  def compileOperator(template: org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorselTemplate,
                      workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                      argumentStates: scala.Seq[scala.Tuple2[org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId, org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateFactory[_ <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentState]]],
                      codeGenerationMode: org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode): org.neo4j.cypher.internal.runtime.pipelined.operators.CompiledStreamingOperator = {
    null
  }
}

trait ContinuableOperatorTaskWithMorselTemplate extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate {
  final override def genOperate: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def genAdvanceOnCancelledRow: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def genClassDeclaration(packageName: String,
                                   className: String,
                                   staticFields: scala.Seq[org.neo4j.codegen.api.StaticField]): org.neo4j.codegen.api.ClassDeclaration[org.neo4j.cypher.internal.runtime.pipelined.operators.CompiledTask] = {
    null
  }

  protected def isHead: scala.Boolean

  protected def genOperateHead: org.neo4j.codegen.api.IntermediateRepresentation

  protected def genOperateMiddle: org.neo4j.codegen.api.IntermediateRepresentation

  protected def genScopeWithLocalDeclarations(scopeId: String,
                                              genBody: => org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected def genClearStateOnCancelledRow: org.neo4j.codegen.api.IntermediateRepresentation
}

