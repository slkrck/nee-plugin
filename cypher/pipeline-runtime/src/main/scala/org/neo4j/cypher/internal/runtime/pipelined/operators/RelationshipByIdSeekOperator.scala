/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

abstract class RelationshipByIdSeekOperator(val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                                            relationship: scala.Int,
                                            startNode: scala.Int,
                                            endNode: scala.Int,
                                            relId: org.neo4j.cypher.internal.runtime.interpreted.pipes.SeekArgs,
                                            argumentSize: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.StreamingOperator {

  abstract class RelationshipByIdTask(val inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext) extends org.neo4j.cypher.internal.runtime.pipelined.operators.InputLoopTask {
    protected var ids: java.util.Iterator[org.neo4j.values.AnyValue] = {
      null
    }
    protected var cursor: org.neo4j.internal.kernel.api.RelationshipScanCursor = {
      null
    }

    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def setExecutionEvent(event: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit = {
      null
    }

    protected override def initializeInnerLoop(context: org.neo4j.cypher.internal.runtime.QueryContext,
                                               state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                               resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                               initExecutionContext: org.neo4j.cypher.internal.runtime.ExecutionContext): scala.Boolean = {
      false
    }

    protected override def closeInnerLoop(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }
  }

}

object RelationshipByIdSeekOperator extends scala.AnyRef {
  val asIdMethod: org.neo4j.codegen.api.Method = {
    null
  }

  def taskTemplate(isDirected: scala.Boolean,
                   inner: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate,
                   id: org.neo4j.cypher.internal.v4_0.util.attribution.Id,
                   innermost: org.neo4j.cypher.internal.runtime.pipelined.operators.DelegateOperatorTaskTemplate,
                   relationshipOffset: scala.Int,
                   fromOffset: scala.Int,
                   toOffset: scala.Int,
                   relIds: org.neo4j.cypher.internal.logical.plans.SeekableArgs,
                   argumentSize: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size,
                   codeGen: org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler): org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate = {
    null
  }
}
