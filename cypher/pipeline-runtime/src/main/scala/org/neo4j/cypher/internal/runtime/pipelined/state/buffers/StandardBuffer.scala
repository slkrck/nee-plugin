/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.state.buffers

class StandardBuffer[T <: scala.AnyRef]() extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffer[T] {

  override def put(t: T): scala.Unit = {
    null
  }

  override def canPut: scala.Boolean = {
    false
  }

  override def hasData: scala.Boolean = {
    false
  }

  override def take(): T = {
    null.asInstanceOf[T]
  }

  override def foreach(f: scala.Function1[T, scala.Unit]): scala.Unit = {
    null
  }

  override def toString(): String = {
    null
  }

  override def iterator: java.util.Iterator[T] = {
    null
  }
}

class StandardOptionalBuffer[T <: scala.AnyRef](inner: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffer[T]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffer[T] with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.OptionalBuffer {
  override def put(t: T): scala.Unit = {
    null
  }

  def didReceiveData: scala.Boolean = {
    false
  }

  override def foreach(f: scala.Function1[T, scala.Unit]): scala.Unit = {
    null
  }

  override def hasData: scala.Boolean = {
    false
  }

  override def take(): T = {
    null.asInstanceOf[T]
  }

  override def canPut: scala.Boolean = {
    false
  }

  override def iterator: java.util.Iterator[T] = {
    null
  }
}

class StandardSingletonBuffer[T <: scala.AnyRef]() extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.SingletonBuffer[T] {
  override def put(t: T): scala.Unit = {
    null
  }

  override def tryPut(t: T): scala.Unit = {
    null
  }

  override def canPut: scala.Boolean = {
    false
  }

  override def hasData: scala.Boolean = {
    false
  }

  override def take(): T = {
    null.asInstanceOf[T]
  }

  override def foreach(f: scala.Function1[T, scala.Unit]): scala.Unit = {
    null
  }

  override def toString(): String = {
    null
  }

  override def iterator: java.util.Iterator[T] = {
    null
  }
}
