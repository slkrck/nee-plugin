/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

class DistinctOperator(argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                       val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                       groupings: org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.MiddleOperator {
  override def createTask(argumentStateCreator: org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator,
                          stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                          queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                          state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                          resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask = {
    null
  }

  class DistinctOperatorTask(argumentStateMap: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap[DistinctOperator.this.DistinctState]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask {
    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def operate(output: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                         context: org.neo4j.cypher.internal.runtime.QueryContext,
                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }

    override def setExecutionEvent(event: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit = {
      null
    }
  }

  class DistinctState(override val argumentRowId: scala.Long,
                      seen: java.util.Set[DistinctOperator.this.groupings.KeyType],
                      override val argumentRowIdsForReducers: scala.Array[scala.Long],
                      memoryTracker: org.neo4j.cypher.internal.runtime.QueryMemoryTracker) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentState {
    def filterOrProject(row: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                        queryState: org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState): scala.Boolean = {
      false
    }

    override def toString(): String = {
      null
    }
  }

  class DistinctStateFactory(memoryTracker: org.neo4j.cypher.internal.runtime.QueryMemoryTracker) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateFactory[DistinctOperator.this.DistinctState] {
    override def newStandardArgumentState(argumentRowId: scala.Long,
                                          argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                          argumentRowIdsForReducers: scala.Array[scala.Long]): DistinctOperator.this.DistinctState = {
      null
    }

    override def newConcurrentArgumentState(argumentRowId: scala.Long,
                                            argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                            argumentRowIdsForReducers: scala.Array[scala.Long]): DistinctOperator.this.DistinctState = {
      null
    }
  }

}
