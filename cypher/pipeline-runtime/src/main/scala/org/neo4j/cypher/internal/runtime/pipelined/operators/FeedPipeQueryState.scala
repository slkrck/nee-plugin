/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

class FeedPipeQueryState(query: org.neo4j.cypher.internal.runtime.QueryContext,
                         resources: org.neo4j.cypher.internal.runtime.interpreted.pipes.ExternalCSVResource,
                         params: scala.Array[org.neo4j.values.AnyValue],
                         cursors: org.neo4j.cypher.internal.runtime.ExpressionCursors,
                         queryIndexes: scala.Array[org.neo4j.internal.kernel.api.IndexReadSession],
                         expressionVariables: scala.Array[org.neo4j.values.AnyValue],
                         subscriber: org.neo4j.kernel.impl.query.QuerySubscriber,
                         memoryTracker: org.neo4j.cypher.internal.runtime.QueryMemoryTracker,
                         decorator: org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeDecorator = {
                           null
                         },
                         initialContext: scala.Option[org.neo4j.cypher.internal.runtime.ExecutionContext] = {
                           null
                         },
                         cachedIn: org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.SingleThreadedLRUCache[scala.Any, org.neo4j.cypher.internal.runtime.interpreted.commands.predicates.InCheckContainer] = {
                           null
                         },
                         lenientCreateRelationship: scala.Boolean = {
                           false
                         },
                         prePopulateResults: scala.Boolean = {
                           false
                         },
                         input: org.neo4j.cypher.internal.runtime.InputDataStream = {
                           null
                         },
                         val profileInformation: org.neo4j.cypher.internal.runtime.interpreted.profiler.InterpretedProfileInformation = {
                           null
                         },
                         var morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
                           null
                         }) extends org.neo4j.cypher.internal.runtime.slotted.SlottedQueryState(query, resources, params, cursors, queryIndexes, expressionVariables, subscriber, memoryTracker, decorator, initialContext, cachedIn, lenientCreateRelationship, prePopulateResults, input) {
  override def withDecorator(decorator: org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeDecorator): org.neo4j.cypher.internal.runtime.pipelined.operators.FeedPipeQueryState = {
    null
  }

  override def withInitialContext(initialContext: org.neo4j.cypher.internal.runtime.ExecutionContext): org.neo4j.cypher.internal.runtime.pipelined.operators.FeedPipeQueryState = {
    null
  }

  override def withQueryContext(query: org.neo4j.cypher.internal.runtime.QueryContext): org.neo4j.cypher.internal.runtime.pipelined.operators.FeedPipeQueryState = {
    null
  }
}
