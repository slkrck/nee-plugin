/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

trait CompiledStreamingOperator extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.StreamingOperator {
  protected override def nextTasks(context: org.neo4j.cypher.internal.runtime.QueryContext,
                                   state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                   inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer,
                                   parallelism: scala.Int,
                                   resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                   argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps): scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorsel] = {
    null
  }

  protected def compiledNextTask(dataRead: org.neo4j.internal.kernel.api.Read,
                                 inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                 argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps): org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorsel
}

object CompiledStreamingOperator extends scala.AnyRef {
  type CompiledTaskFactory = scala.Function3[org.neo4j.internal.kernel.api.Read, org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer, org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps, scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorsel]]

  def getClassDeclaration(packageName: String,
                          className: String,
                          taskClazz: _root_.scala.Predef.Class[org.neo4j.cypher.internal.runtime.pipelined.operators.CompiledTask],
                          workIdentityField: org.neo4j.codegen.api.StaticField,
                          argumentStates: scala.Seq[scala.Tuple2[org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId, org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateFactory[_ <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentState]]]): org.neo4j.codegen.api.ClassDeclaration[org.neo4j.cypher.internal.runtime.pipelined.operators.CompiledStreamingOperator] = {
    null
  }
}
