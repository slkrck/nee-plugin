/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined

sealed trait Sleeper extends scala.AnyRef {
  def reportStartWorkUnit(): scala.Unit

  def reportStopWorkUnit(): scala.Unit

  def reportIdle(): scala.Unit

  def isSleeping: scala.Boolean

  def isWorking: scala.Boolean

  def statusString: String
}

object Sleeper extends scala.AnyRef {

  def concurrentSleeper(workerId: scala.Int, sleepDuration: scala.concurrent.duration.Duration = {
    null
  }): org.neo4j.cypher.internal.runtime.pipelined.Sleeper = {
    null
  }

  sealed trait Status extends scala.AnyRef {
  }

  case object ACTIVE extends scala.AnyRef with Sleeper.Status with scala.Product with scala.Serializable {
  }

  case object WORKING extends scala.AnyRef with Sleeper.Status with scala.Product with scala.Serializable {
  }

  case object SLEEPING extends scala.AnyRef with Sleeper.Status with scala.Product with scala.Serializable {
  }

  case object noSleep extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.Sleeper with scala.Product with scala.Serializable {
    override def reportStartWorkUnit(): scala.Unit = {
      null
    }

    override def reportStopWorkUnit(): scala.Unit = {
      null
    }

    override def reportIdle(): scala.Unit = {
      null
    }

    override def isSleeping: scala.Boolean = {
      false
    }

    override def isWorking: scala.Boolean = {
      false
    }

    override def statusString: String = {
      null
    }
  }

}

class ConcurrentSleeper(val workerId: scala.Int, private val sleepDuration: scala.concurrent.duration.Duration = {
  null
}) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.Sleeper {
  override def reportStartWorkUnit(): scala.Unit = {
    null
  }

  override def reportStopWorkUnit(): scala.Unit = {
    null
  }

  override def reportIdle(): scala.Unit = {
    null
  }

  override def isSleeping: scala.Boolean = {
    false
  }

  override def isWorking: scala.Boolean = {
    false
  }

  override def statusString: String = {
    null
  }
}
