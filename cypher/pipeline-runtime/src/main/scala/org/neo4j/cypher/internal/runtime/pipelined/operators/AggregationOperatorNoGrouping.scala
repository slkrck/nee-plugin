/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

case class AggregationOperatorNoGrouping(val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                                         val aggregations: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator]) extends scala.AnyRef with scala.Product with scala.Serializable {
  def mapper(argumentSlotOffset: scala.Int,
             outputBufferId: org.neo4j.cypher.internal.physicalplanning.BufferId,
             expressionValues: scala.Array[org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression]): AggregationOperatorNoGrouping.this.AggregationMapperOperatorNoGrouping = {
    null
  }

  def reducer(argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
              reducerOutputSlots: scala.Array[scala.Int]): AggregationOperatorNoGrouping.this.AggregationReduceOperatorNoGrouping = {
    null
  }

  class AggregationMapperOperatorNoGrouping(val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                                            argumentSlotOffset: scala.Int,
                                            outputBufferId: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                            aggregations: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator],
                                            expressionValues: scala.Array[org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperator {
    override def outputBuffer: scala.Option[org.neo4j.cypher.internal.physicalplanning.BufferId] = {
      null
    }

    override def createState(executionState: org.neo4j.cypher.internal.runtime.pipelined.ExecutionState,
                             pipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId): org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState = {
      null
    }

    class State(sink: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater]]]]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState {
      override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
        null
      }

      override def trackTime: scala.Boolean = {
        false
      }

      override def prepareOutput(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                 context: org.neo4j.cypher.internal.runtime.QueryContext,
                                 state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                 resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                 operatorExecutionEvent: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): AggregationMapperOperatorNoGrouping.this.PreAggregatedOutput = {
        null
      }
    }

    class PreAggregatedOutput(preAggregated: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater]]],
                              sink: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater]]]]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput {
      override def produce(): scala.Unit = {
        null
      }
    }

  }

  class AggregationReduceOperatorNoGrouping(val argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                            val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                                            aggregations: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator],
                                            reducerOutputSlots: scala.Array[scala.Int]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.Operator with org.neo4j.cypher.internal.runtime.pipelined.operators.ReduceOperatorState[scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater], org.neo4j.cypher.internal.runtime.pipelined.aggregators.AggregatingAccumulator] {
    override def createState(argumentStateCreator: org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator,
                             stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                             queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                             state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                             resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.operators.ReduceOperatorState[scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater], org.neo4j.cypher.internal.runtime.pipelined.aggregators.AggregatingAccumulator] = {
      null
    }

    override def nextTasks(queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                           state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                           input: org.neo4j.cypher.internal.runtime.pipelined.aggregators.AggregatingAccumulator,
                           resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithAccumulator[scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater], org.neo4j.cypher.internal.runtime.pipelined.aggregators.AggregatingAccumulator]] = {
      null
    }

    class OTask(override val accumulator: org.neo4j.cypher.internal.runtime.pipelined.aggregators.AggregatingAccumulator) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithAccumulator[scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater], org.neo4j.cypher.internal.runtime.pipelined.aggregators.AggregatingAccumulator] {
      override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
        null
      }

      override def operate(outputRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                           context: org.neo4j.cypher.internal.runtime.QueryContext,
                           state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                           resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
        null
      }

      override def canContinue: scala.Boolean = {
        false
      }
    }

  }

}
