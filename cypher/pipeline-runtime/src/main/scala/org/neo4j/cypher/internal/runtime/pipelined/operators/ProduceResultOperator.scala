/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

class ProduceResultOperator(val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                            slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                            columns: scala.Seq[scala.Tuple2[String, org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression]]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.StreamingOperator with org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperator {
  override def toString(): String = {
    null
  }

  override def outputBuffer: scala.Option[org.neo4j.cypher.internal.physicalplanning.BufferId] = {
    null
  }

  override def createState(executionState: org.neo4j.cypher.internal.runtime.pipelined.ExecutionState,
                           pipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId): org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState = {
    null
  }

  protected override def nextTasks(context: org.neo4j.cypher.internal.runtime.QueryContext,
                                   state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                   inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer,
                                   parallelism: scala.Int,
                                   resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                   argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps): scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorsel] = {
    null
  }

  protected def produceOutputWithProfile(output: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                         context: org.neo4j.cypher.internal.runtime.QueryContext,
                                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                         operatorExecutionEvent: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit = {
    null
  }

  protected def produceOutput(output: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                              context: org.neo4j.cypher.internal.runtime.QueryContext,
                              state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                              resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Int = {
    0
  }

  class InputOTask(val inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorsel {
    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def toString(): String = {
      null
    }

    override def canContinue: scala.Boolean = {
      false
    }

    override def operateWithProfile(outputIgnore: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                    context: org.neo4j.cypher.internal.runtime.QueryContext,
                                    state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                    resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                    queryProfiler: org.neo4j.cypher.internal.profiling.QueryProfiler): scala.Unit = {
      null
    }

    override def operate(output: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                         context: org.neo4j.cypher.internal.runtime.QueryContext,
                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }

    override def setExecutionEvent(event: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit = {
      null
    }

    protected override def closeCursors(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }
  }

  class OutputOOperatorState() extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState with org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput {
    override def toString(): String = {
      null
    }

    override def canContinueOutput: scala.Boolean = {
      false
    }

    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def prepareOutput(outputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                               context: org.neo4j.cypher.internal.runtime.QueryContext,
                               state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                               resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                               operatorExecutionEvent: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput = {
      null
    }

    override def produce(): scala.Unit = {
      null
    }

    override def trackTime: scala.Boolean = {
      false
    }
  }

}

class ProduceResultOperatorTaskTemplate(val inner: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate,
                                        override val id: org.neo4j.cypher.internal.v4_0.util.attribution.Id,
                                        columns: scala.Seq[String],
                                        slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration)
                                       (protected val codeGen: org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate {
  override def toString(): String = {
    null
  }

  override def genInit: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def genOperate: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def genFields: scala.Seq[org.neo4j.codegen.api.Field] = {
    null
  }

  override def genLocalVariables: scala.Seq[org.neo4j.codegen.api.LocalVariable] = {
    null
  }

  override def genExpressions: scala.Seq[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression] = {
    null
  }

  override def genCanContinue: scala.Option[org.neo4j.codegen.api.IntermediateRepresentation] = {
    null
  }

  override def genCloseCursors: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def genSetExecutionEvent(event: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
}