/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined

trait ExecutionState extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator {
  def pipelineStates: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.PipelineState]

  def getSinkInt[T <: scala.AnyRef](fromPipeline: scala.Int, bufferId: scala.Int): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[T] = {
    null
  }

  def getSink[T <: scala.AnyRef](fromPipeline: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                 bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[T]

  def putMorsel(fromPipeline: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId,
                morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit

  def takeMorsel(bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer

  def takeAccumulator[DATA <: scala.AnyRef, ACC <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[DATA]](bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): ACC

  def takeAccumulatorAndMorsel[DATA <: scala.AnyRef, ACC <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[DATA]](bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatorAndMorsel[DATA, ACC]

  def takeData[DATA <: scala.AnyRef](bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): DATA

  def closeMorselTask(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                      inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit

  def closeData[DATA <: scala.AnyRef](pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline, data: DATA): scala.Unit

  def closeWorkUnit(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Unit

  def closeAccumulatorTask(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                           accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Unit

  def closeMorselAndAccumulatorTask(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                                    inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                    accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Unit

  def filterCancelledArguments(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                               inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Boolean

  def filterCancelledArguments(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                               accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Boolean

  def filterCancelledArguments(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                               inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                               accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Boolean

  def canPut(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Boolean

  def takeContinuation(p: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): org.neo4j.cypher.internal.runtime.pipelined.PipelineTask

  def putContinuation(task: org.neo4j.cypher.internal.runtime.pipelined.PipelineTask,
                      wakeUp: scala.Boolean,
                      resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit

  def tryLock(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Boolean

  def unlock(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Unit

  def canContinueOrTake(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Boolean

  def initializeState(): scala.Unit

  def failQuery(throwable: scala.Throwable,
                resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                failedPipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Unit

  def cancelQuery(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit

  def scheduleCancelQuery(): scala.Unit

  def cleanUpTask(): org.neo4j.cypher.internal.runtime.pipelined.CleanUpTask

  def hasEnded: scala.Boolean

  def prettyString(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): String

  def argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps
}
