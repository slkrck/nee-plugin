/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

case class TopOperator(val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                       val orderBy: scala.Seq[org.neo4j.cypher.internal.runtime.slotted.ColumnOrder],
                       val countExpression: org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) extends scala.AnyRef with scala.Product with scala.Serializable {
  override def toString(): String = {
    null
  }

  def mapper(argumentSlotOffset: scala.Int, outputBufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): TopOperator.this.TopMapperOperator = {
    null
  }

  def reducer(argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId): TopOperator.this.TopReduceOperator = {
    null
  }

  class TopMapperOperator(argumentSlotOffset: scala.Int,
                          outputBufferId: org.neo4j.cypher.internal.physicalplanning.BufferId) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperator {
    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def outputBuffer: scala.Option[org.neo4j.cypher.internal.physicalplanning.BufferId] = {
      null
    }

    override def createState(executionState: org.neo4j.cypher.internal.runtime.pipelined.ExecutionState,
                             pipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId): org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState = {
      null
    }

    class State(sink: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext]]]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState {
      override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
        null
      }

      override def trackTime: scala.Boolean = {
        false
      }

      override def prepareOutput(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                 queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                                 state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                 resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                 operatorExecutionEvent: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): TopMapperOperator.this.PreTopOutput = {
        null
      }
    }

    class PreTopOutput(preTopped: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext]],
                       sink: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext]]]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput {
      override def produce(): scala.Unit = {
        null
      }
    }

  }

  class TopReduceOperator(argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.Operator with org.neo4j.cypher.internal.runtime.pipelined.operators.ReduceOperatorState[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, org.neo4j.cypher.internal.runtime.pipelined.operators.TopOperator.TopTable] {
    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def createState(argumentStateCreator: org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator,
                             stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                             queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                             state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                             resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.operators.ReduceOperatorState[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, org.neo4j.cypher.internal.runtime.pipelined.operators.TopOperator.TopTable] = {
      null
    }

    override def nextTasks(queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                           state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                           input: org.neo4j.cypher.internal.runtime.pipelined.operators.TopOperator.TopTable,
                           resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithAccumulator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, org.neo4j.cypher.internal.runtime.pipelined.operators.TopOperator.TopTable]] = {
      null
    }

    class OTask(override val accumulator: org.neo4j.cypher.internal.runtime.pipelined.operators.TopOperator.TopTable) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithAccumulator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, org.neo4j.cypher.internal.runtime.pipelined.operators.TopOperator.TopTable] {
      var sortedInputPerArgument: java.util.Iterator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] = {
        null
      }

      override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
        null
      }

      override def toString(): String = {
        null
      }

      override def operate(outputRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                           context: org.neo4j.cypher.internal.runtime.QueryContext,
                           state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                           resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
        null
      }

      override def canContinue: scala.Boolean = {
        false
      }
    }

  }

}

object TopOperator extends scala.AnyRef with scala.Serializable {

  abstract class TopTable() extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] {
    final def topRows(): java.util.Iterator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] = {
      null
    }

    def deallocateMemory(): scala.Unit

    protected def getTopRows: java.util.Iterator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext]
  }

  class Factory(memoryTracker: org.neo4j.cypher.internal.runtime.QueryMemoryTracker,
                comparator: java.util.Comparator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext],
                limit: scala.Int) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateFactory[TopOperator.TopTable] {
    override def newStandardArgumentState(argumentRowId: scala.Long,
                                          argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                          argumentRowIdsForReducers: scala.Array[scala.Long]): TopOperator.TopTable = {
      null
    }

    override def newConcurrentArgumentState(argumentRowId: scala.Long,
                                            argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                            argumentRowIdsForReducers: scala.Array[scala.Long]): TopOperator.TopTable = {
      null
    }
  }

  case class ZeroTable(override val argumentRowId: scala.Long,
                       override val argumentRowIdsForReducers: scala.Array[scala.Long]) extends TopOperator.TopTable with scala.Product with scala.Serializable {
    override def update(data: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
      null
    }

    override def deallocateMemory(): scala.Unit = {
      null
    }

    protected override def getTopRows: java.util.Iterator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] = {
      null
    }
  }

  class StandardTopTable(override val argumentRowId: scala.Long,
                         override val argumentRowIdsForReducers: scala.Array[scala.Long],
                         memoryTracker: org.neo4j.cypher.internal.runtime.QueryMemoryTracker,
                         comparator: java.util.Comparator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext],
                         limit: scala.Int) extends TopOperator.TopTable {
    override def update(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
      null
    }

    override def deallocateMemory(): scala.Unit = {
      null
    }

    protected override def getTopRows: java.util.Iterator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] = {
      null
    }
  }

  class ConcurrentTopTable(override val argumentRowId: scala.Long,
                           override val argumentRowIdsForReducers: scala.Array[scala.Long],
                           comparator: java.util.Comparator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext],
                           limit: scala.Int) extends TopOperator.TopTable {
    override def update(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
      null
    }

    override def deallocateMemory(): scala.Unit = {
      null
    }

    protected override def getTopRows: java.util.Iterator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] = {
      null
    }
  }

}
