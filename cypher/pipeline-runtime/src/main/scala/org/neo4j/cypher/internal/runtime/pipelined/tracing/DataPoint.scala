/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.tracing

case class DataPoint(val id: scala.Long,
                     val upstreamIds: scala.Seq[scala.Long],
                     val queryId: scala.Int,
                     val schedulingThreadId: scala.Long,
                     val scheduledTime: scala.Long,
                     val executionThreadId: scala.Long,
                     val startTime: scala.Long,
                     val stopTime: scala.Long,
                     val workId: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity) extends scala.AnyRef with scala.Product with scala.Serializable {
  def withTimeZero(t0: scala.Long): org.neo4j.cypher.internal.runtime.pipelined.tracing.DataPoint = {
    null
  }
}

trait DataPointFlusher extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.tracing.DataPointWriter with java.io.Closeable {
  def flush(): scala.Unit
}

class DataPointSchedulerTracer(dataPointWriter: org.neo4j.cypher.internal.runtime.pipelined.tracing.DataPointWriter) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer {
  override def traceQuery(): org.neo4j.cypher.internal.runtime.pipelined.tracing.QueryExecutionTracer = {
    null
  }

  case class QueryTracer(val queryId: scala.Int) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.tracing.QueryExecutionTracer with scala.Product with scala.Serializable {
    override def scheduleWorkUnit(workId: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                                  upstreamWorkUnits: scala.Seq[org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent]): org.neo4j.cypher.internal.runtime.pipelined.tracing.ScheduledWorkUnitEvent = {
      null
    }

    override def stopQuery(): scala.Unit = {
      null
    }
  }

  case class ScheduledWorkUnit(val workUnitId: scala.Long,
                               val upstreamWorkUnitIds: scala.Seq[scala.Long],
                               val queryId: scala.Int,
                               val scheduledTime: scala.Long,
                               val schedulingThreadId: scala.Long,
                               val workId: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.tracing.ScheduledWorkUnitEvent with scala.Product with scala.Serializable {
    override def start(): org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent = {
      null
    }
  }

  case class WorkUnit(override val id: scala.Long,
                      val upstreamIds: scala.Seq[scala.Long],
                      val queryId: scala.Int,
                      val schedulingThreadId: scala.Long,
                      val scheduledTime: scala.Long,
                      val executionThreadId: scala.Long,
                      val startTime: scala.Long,
                      val workId: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent with scala.Product with scala.Serializable {
    override def stop(): scala.Unit = {
      null
    }
  }

}

trait DataPointWriter extends scala.AnyRef {
  def write(dataPoint: org.neo4j.cypher.internal.runtime.pipelined.tracing.DataPoint): scala.Unit
}