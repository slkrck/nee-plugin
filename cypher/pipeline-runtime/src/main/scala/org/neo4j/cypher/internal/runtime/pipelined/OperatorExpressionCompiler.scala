/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined

class OperatorExpressionCompiler(slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                                 val inputSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                                 readOnly: scala.Boolean,
                                 codeGenerationMode: org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode,
                                 namer: org.neo4j.cypher.internal.runtime.compiled.expressions.VariableNamer) extends org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler(slots, readOnly, codeGenerationMode, namer) {
  final override def getLongAt(offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  final override def getRefAt(offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  final override def setLongAt(offset: scala.Int, value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  final override def setRefAt(offset: scala.Int, value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def copyFromInput(nLongs: scala.Int, nRefs: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  final def doCopyFromWithExecutionContext(context: org.neo4j.codegen.api.IntermediateRepresentation,
                                           input: org.neo4j.codegen.api.IntermediateRepresentation,
                                           nLongs: scala.Int,
                                           nRefs: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def getCachedPropertyAt(property: org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedProperty,
                                   getFromStore: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def setCachedPropertyAt(offset: scala.Int,
                                   value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def writeLocalsToSlots(): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def beginScope(scopeId: String): scala.Unit = {
    null
  }

  def endInitializationScope(mergeIntoParentScope: scala.Boolean = {
    false
  }): org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler.ScopeContinuationState = {
    null
  }

  def endScope(mergeIntoParentScope: scala.Boolean = {
    false
  }): org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler.ScopeLocalsState = {
    null
  }

  def getAllLocalsForLongSlots: scala.Seq[scala.Tuple2[scala.Int, String]] = {
    null
  }

  def getAllLocalsForRefSlots: scala.Seq[scala.Tuple2[scala.Int, String]] = {
    null
  }

  def getAllLocalsForCachedProperties: scala.Seq[scala.Tuple2[scala.Int, String]] = {
    null
  }

  protected def didInitializeCachedPropertyFromStore(): scala.Unit = {
    null
  }

  protected def didInitializeCachedPropertyFromContext(): scala.Unit = {
    null
  }

  protected def didLoadLocalCachedProperty(): scala.Unit = {
    null
  }

  protected override def isLabelSetOnNode(labelToken: org.neo4j.codegen.api.IntermediateRepresentation,
                                          offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def getNodeProperty(propertyToken: org.neo4j.codegen.api.IntermediateRepresentation,
                                         offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def getRelationshipProperty(propertyToken: org.neo4j.codegen.api.IntermediateRepresentation,
                                                 offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def getProperty(key: String,
                                     container: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
}

object OperatorExpressionCompiler extends scala.AnyRef {
  type FOREACH_LOCAL_FUN = scala.Function3[scala.Int, String, scala.Boolean, scala.Unit]

  case class LocalVariableSlotMapper(val scopeId: String, val slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration)
                                    (private val longSlotToLocal: scala.Array[String] = {
                                      null
                                    }, private val longSlotToLocalModified: scala.Array[scala.Boolean] = {
                                      null
                                    }, private val longSlotToLocalInitialized: scala.Array[scala.Boolean] = {
                                      null
                                    }, private val refSlotToLocal: scala.Array[String] = {
                                      null
                                    }, private val refSlotToLocalModified: scala.Array[scala.Boolean] = {
                                      null
                                    }, private val refSlotToLocalInitialized: scala.Array[scala.Boolean] = {
                                      null
                                    }, private val cachedProperties: scala.Array[String] = {
                                      null
                                    }) extends scala.AnyRef with scala.Product with scala.Serializable {
    val foreachLocalForLongSlot: scala.Function1[OperatorExpressionCompiler.FOREACH_LOCAL_FUN, scala.Unit] = {
      null
    }
    val foreachLocalForRefSlot: scala.Function1[OperatorExpressionCompiler.FOREACH_LOCAL_FUN, scala.Unit] = {
      null
    }

    def copy(): OperatorExpressionCompiler.LocalVariableSlotMapper = {
      null
    }

    def addLocalForLongSlot(offset: scala.Int): String = {
      null
    }

    def addLocalForRefSlot(offset: scala.Int): String = {
      null
    }

    def addCachedProperty(offset: scala.Int): String = {
      null
    }

    def markModifiedLocalForLongSlot(offset: scala.Int): scala.Unit = {
      null
    }

    def markModifiedLocalForRefSlot(offset: scala.Int): scala.Unit = {
      null
    }

    def markInitializedLocalForLongSlot(offset: scala.Int): scala.Unit = {
      null
    }

    def markInitializedLocalForRefSlot(offset: scala.Int): scala.Unit = {
      null
    }

    def getLocalForLongSlot(offset: scala.Int): String = {
      null
    }

    def getLocalForRefSlot(offset: scala.Int): String = {
      null
    }

    def foreachCachedProperty(f: scala.Function2[scala.Int, String, scala.Unit]): scala.Unit = {
      null
    }

    def merge(other: OperatorExpressionCompiler.LocalVariableSlotMapper): scala.Unit = {
      null
    }

    def genScopeLocalsState(codeGen: org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler,
                            inputContext: org.neo4j.codegen.api.IntermediateRepresentation): OperatorExpressionCompiler.ScopeLocalsState = {
      null
    }

    def genScopeContinuationState(codeGen: org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler,
                                  inputContext: org.neo4j.codegen.api.IntermediateRepresentation): OperatorExpressionCompiler.ScopeContinuationState = {
      null
    }
  }

  case class ScopeLocalsState(val locals: scala.Seq[org.neo4j.codegen.api.LocalVariable],
                              val declarations: scala.Seq[org.neo4j.codegen.api.IntermediateRepresentation],
                              val assignments: scala.Seq[org.neo4j.codegen.api.IntermediateRepresentation]) extends scala.AnyRef with scala.Product with scala.Serializable {
    def isEmpty: scala.Boolean = {
      false
    }

    def nonsEmpty: scala.Boolean = {
      false
    }
  }

  case class ScopeContinuationState(val fields: scala.Seq[org.neo4j.codegen.api.Field],
                                    val saveStateIR: org.neo4j.codegen.api.IntermediateRepresentation,
                                    val restoreStateIR: org.neo4j.codegen.api.IntermediateRepresentation,
                                    val declarations: scala.Seq[org.neo4j.codegen.api.IntermediateRepresentation],
                                    val assignments: scala.Seq[org.neo4j.codegen.api.IntermediateRepresentation]) extends scala.AnyRef with scala.Product with scala.Serializable {
    def isEmpty: scala.Boolean = {
      false
    }

    def nonEmpty: scala.Boolean = {
      false
    }
  }

  case class LocalsForSlots(val operatorExpressionCompiler: org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler) extends scala.AnyRef with scala.Product with scala.Serializable {
    def beginScope(scopeId: String): scala.Unit = {
      null
    }

    def endScope(mergeIntoParentScope: scala.Boolean): OperatorExpressionCompiler.ScopeLocalsState = {
      null
    }

    def endInitializationScope(mergeIntoParentScope: scala.Boolean): OperatorExpressionCompiler.ScopeContinuationState = {
      null
    }

    def mergeAllScopesCopy(): OperatorExpressionCompiler.LocalVariableSlotMapper = {
      null
    }

    def addLocalForLongSlot(offset: scala.Int): String = {
      null
    }

    def addLocalForRefSlot(offset: scala.Int): String = {
      null
    }

    def addCachedProperty(offset: scala.Int): String = {
      null
    }

    def markModifiedLocalForLongSlot(offset: scala.Int): scala.Unit = {
      null
    }

    def markModifiedLocalForRefSlot(offset: scala.Int): scala.Unit = {
      null
    }

    def markInitializedLocalForLongSlot(offset: scala.Int): scala.Unit = {
      null
    }

    def markInitializedLocalForRefSlot(offset: scala.Int): scala.Unit = {
      null
    }

    def getLocalForLongSlot(offset: scala.Int): String = {
      null
    }

    def getLocalForRefSlot(offset: scala.Int): String = {
      null
    }

    def foreachLocalForLongSlot(f: OperatorExpressionCompiler.FOREACH_LOCAL_FUN): scala.Unit = {
      null
    }

    def foreachLocalForRefSlot(f: OperatorExpressionCompiler.FOREACH_LOCAL_FUN): scala.Unit = {
      null
    }

    def foreachCachedProperty(f: scala.Function2[scala.Int, String, scala.Unit]): scala.Unit = {
      null
    }
  }

}
