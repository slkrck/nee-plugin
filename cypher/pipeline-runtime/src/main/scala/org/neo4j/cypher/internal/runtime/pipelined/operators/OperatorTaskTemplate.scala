/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

trait OperatorTaskTemplate extends scala.AnyRef {
  def inner: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate

  def id: org.neo4j.cypher.internal.v4_0.util.attribution.Id

  final def map[T](f: scala.Function1[org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate, T]): scala.List[T] = {
    null
  }

  final def flatMap[T](f: scala.Function1[org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate, scala.Seq[T]]): scala.Seq[T] = {
    null
  }

  def genClassDeclaration(packageName: String,
                          className: String,
                          staticFields: scala.Seq[org.neo4j.codegen.api.StaticField]): org.neo4j.codegen.api.ClassDeclaration[org.neo4j.cypher.internal.runtime.pipelined.operators.CompiledTask] = {
    null
  }

  def genInit: org.neo4j.codegen.api.IntermediateRepresentation

  def executionEventField: org.neo4j.codegen.api.InstanceField = {
    null
  }

  def genProfileEventField: scala.Option[org.neo4j.codegen.api.Field] = {
    null
  }

  def genInitializeProfileEvents: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def genSetExecutionEvent(event: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation

  def genCloseProfileEvents: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  final def genOperateWithExpressions: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def genOperateEnter: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def genOperateExit: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def genOutputBuffer: scala.Option[org.neo4j.codegen.api.IntermediateRepresentation] = {
    null
  }

  def genExpressions: scala.Seq[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression]

  def genFields: scala.Seq[org.neo4j.codegen.api.Field]

  def genLocalVariables: scala.Seq[org.neo4j.codegen.api.LocalVariable]

  def genCanContinue: scala.Option[org.neo4j.codegen.api.IntermediateRepresentation]

  def genCloseCursors: org.neo4j.codegen.api.IntermediateRepresentation

  protected def scopeId: String = {
    null
  }

  protected def codeGen: org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler

  protected def doIfInnerCantContinue(op: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected def genOperate: org.neo4j.codegen.api.IntermediateRepresentation

  protected def genProduce: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected def genCreateState: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
}

object OperatorTaskTemplate extends scala.AnyRef {
  def empty(withId: org.neo4j.cypher.internal.v4_0.util.attribution.Id): org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate = {
    null
  }
}
