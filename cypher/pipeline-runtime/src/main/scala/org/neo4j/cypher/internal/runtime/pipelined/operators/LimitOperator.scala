/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

class LimitOperator(argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                    val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                    countExpression: org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.MiddleOperator {
  override def createTask(argumentStateCreator: org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator,
                          stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                          queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                          state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                          resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask = {
    null
  }

  class LimitOperatorTask(argumentStateMap: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap[org.neo4j.cypher.internal.runtime.pipelined.operators.LimitOperator.LimitState]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask {
    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def operate(output: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                         context: org.neo4j.cypher.internal.runtime.QueryContext,
                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }

    override def setExecutionEvent(event: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit = {
      null
    }
  }

  class FilterState(var countLeft: scala.Long) extends scala.AnyRef {
    def next(): scala.Boolean = {
      false
    }
  }

}

object LimitOperator extends scala.AnyRef {
  def evaluateCountValue(queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                         countExpression: org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression): scala.Long = {
    0
  }

  def evaluateCountValue(countValue: org.neo4j.values.AnyValue): scala.Long = {
    0
  }

  abstract class LimitState() extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.WorkCanceller {
    def reserve(wanted: scala.Long): scala.Long
  }

  class LimitStateFactory(count: scala.Long) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateFactory[LimitOperator.LimitState] {
    override def newStandardArgumentState(argumentRowId: scala.Long,
                                          argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                          argumentRowIdsForReducers: scala.Array[scala.Long]): LimitOperator.LimitState = {
      null
    }

    override def newConcurrentArgumentState(argumentRowId: scala.Long,
                                            argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                            argumentRowIdsForReducers: scala.Array[scala.Long]): LimitOperator.LimitState = {
      null
    }
  }

  class StandardLimitState(override val argumentRowId: scala.Long,
                           countTotal: scala.Long,
                           override val argumentRowIdsForReducers: scala.Array[scala.Long]) extends LimitOperator.LimitState {
    override def reserve(wanted: scala.Long): scala.Long = {
      0
    }

    override def isCancelled: scala.Boolean = {
      false
    }

    override def toString(): String = {
      null
    }
  }

  class ConcurrentLimitState(override val argumentRowId: scala.Long,
                             countTotal: scala.Long,
                             override val argumentRowIdsForReducers: scala.Array[scala.Long]) extends LimitOperator.LimitState {
    def reserve(wanted: scala.Long): scala.Long = {
      0
    }

    override def isCancelled: scala.Boolean = {
      false
    }

    override def toString(): String = {
      null
    }
  }

}
