/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

class CartesianProductOperator(val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                               lhsArgumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                               rhsArgumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                               slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                               argumentSize: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.Operator with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorState {
  override def createState(argumentStateCreator: org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator,
                           stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                           queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                           state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                           resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorState = {
    null
  }

  override def nextTasks(context: org.neo4j.cypher.internal.runtime.QueryContext,
                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                         operatorInput: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorInput,
                         parallelism: scala.Int,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                         argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps): scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithAccumulator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, org.neo4j.cypher.internal.runtime.pipelined.operators.CartesianProductOperator.LHSMorsel]] = {
    null
  }

  class OTask(override val accumulator: org.neo4j.cypher.internal.runtime.pipelined.operators.CartesianProductOperator.LHSMorsel,
              rhsRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext) extends org.neo4j.cypher.internal.runtime.pipelined.operators.InputLoopTask with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorselAndAccumulator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, org.neo4j.cypher.internal.runtime.pipelined.operators.CartesianProductOperator.LHSMorsel] {
    override val inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
      null
    }

    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def toString(): String = {
      null
    }

    protected override def initializeInnerLoop(context: org.neo4j.cypher.internal.runtime.QueryContext,
                                               state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                               resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                               initExecutionContext: org.neo4j.cypher.internal.runtime.ExecutionContext): scala.Boolean = {
      false
    }

    protected override def innerLoop(outputRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                     context: org.neo4j.cypher.internal.runtime.QueryContext,
                                     state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState): scala.Unit = {
      null
    }

    protected override def closeInnerLoop(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }
  }

}

object CartesianProductOperator extends scala.AnyRef {

  class LHSMorsel(override val argumentRowId: scala.Long,
                  val lhsMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                  override val argumentRowIdsForReducers: scala.Array[scala.Long]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] {
    override def update(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
      null
    }

    override def toString(): String = {
      null
    }
  }

  object LHSMorsel extends scala.AnyRef {

    class Factory(stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateFactory[CartesianProductOperator.LHSMorsel] {
      override def newStandardArgumentState(argumentRowId: scala.Long,
                                            argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                            argumentRowIdsForReducers: scala.Array[scala.Long]): CartesianProductOperator.LHSMorsel = {
        null
      }

      override def newConcurrentArgumentState(argumentRowId: scala.Long,
                                              argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                              argumentRowIdsForReducers: scala.Array[scala.Long]): CartesianProductOperator.LHSMorsel = {
        null
      }

      override def completeOnConstruction: scala.Boolean = {
        false
      }
    }

  }

}
