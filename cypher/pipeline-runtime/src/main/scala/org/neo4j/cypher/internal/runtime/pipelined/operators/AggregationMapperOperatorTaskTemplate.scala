/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

class AggregationMapperOperatorTaskTemplate(val inner: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate,
                                            override val id: org.neo4j.cypher.internal.v4_0.util.attribution.Id,
                                            argumentSlotOffset: scala.Int,
                                            aggregators: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator],
                                            outputBufferId: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                            aggregationExpressionsCreator: scala.Function0[scala.Array[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression]],
                                            groupingKeyExpressionCreator: scala.Function0[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression],
                                            aggregationExpressions: scala.Array[org.neo4j.cypher.internal.v4_0.expressions.Expression])
                                           (protected val codeGen: org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate {
  type AggMap = java.util.LinkedHashMap[org.neo4j.values.AnyValue, scala.Array[scala.Any]]
  type AggOut = scala.collection.mutable.ArrayBuffer[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[AggregationMapperOperatorTaskTemplate.this.AggMap]]

  override def toString(): String = {
    null
  }

  override def genInit: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def genOperate: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def genOutputBuffer: scala.Option[org.neo4j.codegen.api.IntermediateRepresentation] = {
    null
  }

  override def genFields: scala.Seq[org.neo4j.codegen.api.Field] = {
    null
  }

  override def genLocalVariables: scala.Seq[org.neo4j.codegen.api.LocalVariable] = {
    null
  }

  override def genExpressions: scala.Seq[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression] = {
    null
  }

  override def genCanContinue: scala.Option[org.neo4j.codegen.api.IntermediateRepresentation] = {
    null
  }

  override def genCloseCursors: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def genSetExecutionEvent(event: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def genCreateState: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def genProduce: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
}

object AggregationMapperOperatorTaskTemplate extends scala.AnyRef {
  def createAggregators(aggregators: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator]): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def createUpdaters(aggregators: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator],
                     aggregatorsVar: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
}
