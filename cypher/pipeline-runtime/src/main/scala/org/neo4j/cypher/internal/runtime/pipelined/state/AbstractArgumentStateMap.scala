/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.state

abstract class AbstractArgumentStateMap[STATE <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentState, CONTROLLER <: org.neo4j.cypher.internal.runtime.pipelined.state.AbstractArgumentStateMap.StateController[STATE]]() extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMapWithArgumentIdCounter[STATE] with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMapWithoutArgumentIdCounter[STATE] {
  protected val controllers: java.util.Map[scala.Long, CONTROLLER]
  protected var lastCompletedArgumentId: scala.Long

  override def update(argumentRowId: scala.Long, onState: scala.Function1[STATE, scala.Unit]): scala.Unit = {
    null
  }

  override def clearAll(f: scala.Function1[STATE, scala.Unit]): scala.Unit = {
    null
  }

  override def filter[U](readingRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                         onArgument: scala.Function2[STATE, scala.Long, U],
                         onRow: scala.Function2[U, org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, scala.Boolean]): scala.Unit = {
    null
  }

  override def takeOneCompleted(): STATE = {
    null.asInstanceOf[STATE]
  }

  override def takeNextIfCompletedOrElsePeek(): org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateWithCompleted[STATE] = {
    null
  }

  override def nextArgumentStateIsCompletedOr(statePredicate: scala.Function1[STATE, scala.Boolean]): scala.Boolean = {
    false
  }

  override def peekNext(): STATE = {
    null.asInstanceOf[STATE]
  }

  override def peekCompleted(): scala.Iterator[STATE] = {
    null
  }

  override def peek(argumentId: scala.Long): STATE = {
    null.asInstanceOf[STATE]
  }

  override def hasCompleted: scala.Boolean = {
    false
  }

  override def hasCompleted(argument: scala.Long): scala.Boolean = {
    false
  }

  override def remove(argument: scala.Long): scala.Boolean = {
    false
  }

  override def initiate(argument: scala.Long,
                        argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                        argumentRowIdsForReducers: scala.Array[scala.Long]): scala.Unit = {
    null
  }

  override def increment(argument: scala.Long): scala.Unit = {
    null
  }

  override def decrement(argument: scala.Long): STATE = {
    null.asInstanceOf[STATE]
  }

  override def toString(): String = {
    null
  }

  protected def newStateController(argument: scala.Long,
                                   argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                   argumentRowIdsForReducers: scala.Array[scala.Long]): CONTROLLER
}

object AbstractArgumentStateMap extends scala.AnyRef {

  trait StateController[STATE <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentState] extends scala.AnyRef {
    def state: STATE

    def increment(): scala.Long

    def decrement(): scala.Long

    def isZero: scala.Boolean

    def take(): scala.Boolean

    def tryTake(): scala.Boolean
  }

  class ImmutableStateController[STATE <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentState](override val state: STATE) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.AbstractArgumentStateMap.StateController[STATE] {
    override def increment(): scala.Long = {
      0
    }

    override def decrement(): scala.Long = {
      0
    }

    override def tryTake(): scala.Boolean = {
      false
    }

    override def take(): scala.Boolean = {
      false
    }

    override def isZero: scala.Boolean = {
      false
    }

    override def toString(): String = {
      null
    }
  }

}
