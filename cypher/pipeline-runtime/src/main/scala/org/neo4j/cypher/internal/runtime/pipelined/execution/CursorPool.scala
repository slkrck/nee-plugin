/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.execution

class CursorPool[CURSOR <: org.neo4j.internal.kernel.api.Cursor](cursorFactory: scala.Function0[CURSOR]) extends java.lang.Object with java.lang.AutoCloseable {
  def setKernelTracer(tracer: org.neo4j.internal.kernel.api.KernelReadTracer): scala.Unit = {
    null
  }

  def allocateAndTrace(): CURSOR = {
    null.asInstanceOf[CURSOR]
  }

  def allocate(): CURSOR = {
    null.asInstanceOf[CURSOR]
  }

  def free(cursor: CURSOR): scala.Unit = {
    null.asInstanceOf[CURSOR]
  }

  override def close(): scala.Unit = {
    null
  }

  def getLiveCount: scala.Long = {
    0
  }
}

object CursorPool extends scala.AnyRef {
  def apply[CURSOR <: org.neo4j.internal.kernel.api.Cursor](cursorFactory: scala.Function0[CURSOR]): org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPool[CURSOR] = {
    null
  }
}

class CursorPools(cursorFactory: org.neo4j.internal.kernel.api.CursorFactory) extends java.lang.Object with org.neo4j.internal.kernel.api.CursorFactory with java.lang.AutoCloseable {
  val nodeCursorPool: org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPool[org.neo4j.internal.kernel.api.NodeCursor] = {
    null
  }
  val relationshipGroupCursorPool: org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPool[org.neo4j.internal.kernel.api.RelationshipGroupCursor] = {
    null
  }
  val relationshipTraversalCursorPool: org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPool[org.neo4j.internal.kernel.api.RelationshipTraversalCursor] = {
    null
  }
  val relationshipScanCursorPool: org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPool[org.neo4j.internal.kernel.api.RelationshipScanCursor] = {
    null
  }
  val nodeValueIndexCursorPool: org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPool[org.neo4j.internal.kernel.api.NodeValueIndexCursor] = {
    null
  }
  val nodeLabelIndexCursorPool: org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPool[org.neo4j.internal.kernel.api.NodeLabelIndexCursor] = {
    null
  }

  def setKernelTracer(tracer: org.neo4j.internal.kernel.api.KernelReadTracer): scala.Unit = {
    null
  }

  override def close(): scala.Unit = {
    null
  }

  def collectLiveCounts(liveCounts: org.neo4j.cypher.internal.runtime.pipelined.execution.LiveCounts): scala.Unit = {
    null
  }

  override def allocateNodeCursor(): org.neo4j.internal.kernel.api.NodeCursor = {
    null
  }

  override def allocateFullAccessNodeCursor(): org.neo4j.internal.kernel.api.NodeCursor = {
    null
  }

  override def allocateRelationshipScanCursor(): org.neo4j.internal.kernel.api.RelationshipScanCursor = {
    null
  }

  override def allocateFullAccessRelationshipScanCursor(): org.neo4j.internal.kernel.api.RelationshipScanCursor = {
    null
  }

  override def allocateRelationshipTraversalCursor(): org.neo4j.internal.kernel.api.RelationshipTraversalCursor = {
    null
  }

  override def allocatePropertyCursor(): org.neo4j.internal.kernel.api.PropertyCursor = {
    null
  }

  override def allocateFullAccessPropertyCursor(): org.neo4j.internal.kernel.api.PropertyCursor = {
    null
  }

  override def allocateRelationshipGroupCursor(): org.neo4j.internal.kernel.api.RelationshipGroupCursor = {
    null
  }

  override def allocateNodeValueIndexCursor(): org.neo4j.internal.kernel.api.NodeValueIndexCursor = {
    null
  }

  override def allocateNodeLabelIndexCursor(): org.neo4j.internal.kernel.api.NodeLabelIndexCursor = {
    null
  }

  override def allocateRelationshipIndexCursor(): org.neo4j.internal.kernel.api.RelationshipIndexCursor = {
    null
  }
}
