/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

abstract class VarExpandCursor(val fromNode: scala.Long,
                               val targetToNode: scala.Long,
                               nodeCursor: org.neo4j.internal.kernel.api.NodeCursor,
                               projectBackwards: scala.Boolean,
                               relTypes: scala.Array[scala.Int],
                               minLength: scala.Int,
                               maxLength: scala.Int,
                               read: org.neo4j.internal.kernel.api.Read,
                               executionContext: org.neo4j.cypher.internal.runtime.ExecutionContext,
                               dbAccess: org.neo4j.cypher.internal.runtime.DbAccess,
                               params: scala.Array[org.neo4j.values.AnyValue],
                               cursors: org.neo4j.cypher.internal.runtime.ExpressionCursors,
                               expressionVariables: scala.Array[org.neo4j.values.AnyValue]) extends scala.AnyRef {
  def enterWorkUnit(cursorPools: org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPools): scala.Unit = {
    null
  }

  def next(): scala.Boolean = {
    false
  }

  def toNode: scala.Long = {
    0
  }

  def relationships: org.neo4j.values.virtual.ListValue = {
    null
  }

  def setTracer(event: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit = {
    null
  }

  def free(cursorPools: org.neo4j.cypher.internal.runtime.pipelined.execution.CursorPools): scala.Unit = {
    null
  }

  protected def selectionCursor(groupCursor: org.neo4j.internal.kernel.api.RelationshipGroupCursor,
                                traversalCursor: org.neo4j.internal.kernel.api.RelationshipTraversalCursor,
                                node: org.neo4j.internal.kernel.api.NodeCursor,
                                types: scala.Array[scala.Int]): org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor

  protected def satisfyPredicates(executionContext: org.neo4j.cypher.internal.runtime.ExecutionContext,
                                  dbAccess: org.neo4j.cypher.internal.runtime.DbAccess,
                                  params: scala.Array[org.neo4j.values.AnyValue],
                                  cursors: org.neo4j.cypher.internal.runtime.ExpressionCursors,
                                  expressionVariables: scala.Array[org.neo4j.values.AnyValue],
                                  selectionCursor: org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor): scala.Boolean
}

object VarExpandCursor extends scala.AnyRef {
  def apply(direction: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
            fromNode: scala.Long,
            targetToNode: scala.Long,
            nodeCursor: org.neo4j.internal.kernel.api.NodeCursor,
            projectBackwards: scala.Boolean,
            relTypes: scala.Array[scala.Int],
            minLength: scala.Int,
            maxLength: scala.Int,
            read: org.neo4j.internal.kernel.api.Read,
            dbAccess: org.neo4j.cypher.internal.runtime.DbAccess,
            nodePredicate: org.neo4j.cypher.internal.runtime.pipelined.operators.VarExpandPredicate[scala.Long],
            relationshipPredicate: org.neo4j.cypher.internal.runtime.pipelined.operators.VarExpandPredicate[org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor]): org.neo4j.cypher.internal.runtime.pipelined.operators.VarExpandCursor = {
    null
  }

  def relationshipFromCursor(dbAccess: org.neo4j.cypher.internal.runtime.DbAccess,
                             cursor: org.neo4j.internal.kernel.api.helpers.RelationshipSelectionCursor): org.neo4j.values.virtual.RelationshipValue = {
    null
  }
}
