/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

case class MorselArgumentStateBufferOutputState(val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                                                val sink: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext]]],
                                                val argumentSlotOffset: scala.Int,
                                                val trackTime: scala.Boolean) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState with scala.Product with scala.Serializable {
  override def prepareOutput(outputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                             context: org.neo4j.cypher.internal.runtime.QueryContext,
                             state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                             resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                             operatorExecutionEvent: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput = {
    null
  }
}
