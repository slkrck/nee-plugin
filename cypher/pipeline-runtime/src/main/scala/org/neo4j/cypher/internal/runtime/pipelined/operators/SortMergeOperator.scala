/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

class SortMergeOperator(val argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                        val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                        orderBy: scala.Seq[org.neo4j.cypher.internal.runtime.slotted.ColumnOrder],
                        argumentSlotOffset: scala.Int) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.Operator with org.neo4j.cypher.internal.runtime.pipelined.operators.ReduceOperatorState[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer] {
  override def toString(): String = {
    null
  }

  override def createState(argumentStateCreator: org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator,
                           stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                           queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                           state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                           resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.operators.ReduceOperatorState[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer] = {
    null
  }

  override def nextTasks(queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                         input: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithAccumulator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer]] = {
    null
  }

  class OTask(override val accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithAccumulator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer] {
    var sortedInputPerArgument: java.util.PriorityQueue[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] = {
      null
    }

    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def toString(): String = {
      null
    }

    override def operate(outputRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                         context: org.neo4j.cypher.internal.runtime.QueryContext,
                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }

    override def canContinue: scala.Boolean = {
      false
    }
  }

}
