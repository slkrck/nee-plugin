/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.state

class TheExecutionState(executionGraphDefinition: org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition,
                        pipelines: scala.Seq[org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline],
                        stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                        workerWaker: org.neo4j.cypher.internal.runtime.pipelined.execution.WorkerWaker,
                        queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                        queryState: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                        initializationResources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                        tracker: org.neo4j.cypher.internal.runtime.pipelined.state.QueryCompletionTracker) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.ExecutionState {
  override val argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps = {
    null
  }
  override val pipelineStates: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.PipelineState] = {
    null
  }

  final override def createArgumentStateMap[S <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentState](argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                                                                                                                   factory: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateFactory[S]): org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap[S] = {
    null
  }

  override def initializeState(): scala.Unit = {
    null
  }

  override def getSink[T <: scala.AnyRef](fromPipeline: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                          bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[T] = {
    null
  }

  override def putMorsel(fromPipeline: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                         bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId,
                         output: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def takeMorsel(bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer = {
    null
  }

  override def takeAccumulator[DATA <: scala.AnyRef, ACC <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[DATA]](bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): ACC = {
    null.asInstanceOf[ACC]
  }

  override def takeAccumulatorAndMorsel[DATA <: scala.AnyRef, ACC <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[DATA]](bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatorAndMorsel[DATA, ACC] = {
    null
  }

  override def takeData[DATA <: scala.AnyRef](bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): DATA = {
    null.asInstanceOf[DATA]
  }

  override def closeWorkUnit(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Unit = {
    null
  }

  override def closeMorselTask(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                               inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def closeData[DATA <: scala.AnyRef](pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline, data: DATA): scala.Unit = {
    null
  }

  override def closeAccumulatorTask(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                                    accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Unit = {
    null
  }

  override def closeMorselAndAccumulatorTask(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                                             inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                             accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Unit = {
    null
  }

  override def filterCancelledArguments(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                                        inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Boolean = {
    false
  }

  override def filterCancelledArguments(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                                        accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Boolean = {
    false
  }

  override def filterCancelledArguments(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                                        inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                        accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Boolean = {
    false
  }

  override def putContinuation(task: org.neo4j.cypher.internal.runtime.pipelined.PipelineTask,
                               wakeUp: scala.Boolean,
                               resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
    null
  }

  override def canPut(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Boolean = {
    false
  }

  override def takeContinuation(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): org.neo4j.cypher.internal.runtime.pipelined.PipelineTask = {
    null
  }

  override def tryLock(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Boolean = {
    false
  }

  override def unlock(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Unit = {
    null
  }

  override def canContinueOrTake(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Boolean = {
    false
  }

  override def failQuery(throwable: scala.Throwable,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                         failedPipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): scala.Unit = {
    null
  }

  override def cancelQuery(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
    null
  }

  override def scheduleCancelQuery(): scala.Unit = {
    null
  }

  override def cleanUpTask(): org.neo4j.cypher.internal.runtime.pipelined.CleanUpTask = {
    null
  }

  override def hasEnded: scala.Boolean = {
    false
  }

  override def prettyString(pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline): String = {
    null
  }

  override def toString(): String = {
    null
  }
}
