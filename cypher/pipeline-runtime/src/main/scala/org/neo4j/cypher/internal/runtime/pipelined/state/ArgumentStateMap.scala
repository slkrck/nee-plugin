/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.state

trait ArgumentStateMap[S <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentState] extends scala.AnyRef {
  def clearAll(f: scala.Function1[S, scala.Unit]): scala.Unit

  def update(argumentRowId: scala.Long, onState: scala.Function1[S, scala.Unit]): scala.Unit

  def filter[FILTER_STATE](morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                           onArgument: scala.Function2[S, scala.Long, FILTER_STATE],
                           onRow: scala.Function2[FILTER_STATE, org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, scala.Boolean]): scala.Unit

  def peekCompleted(): scala.Iterator[S]

  def peek(argumentId: scala.Long): S

  def hasCompleted: scala.Boolean

  def hasCompleted(argument: scala.Long): scala.Boolean

  def remove(argument: scala.Long): scala.Boolean

  def initiate(argument: scala.Long,
               argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
               argumentRowIdsForReducers: scala.Array[scala.Long]): scala.Unit

  def increment(argument: scala.Long): scala.Unit

  def decrement(argument: scala.Long): S

  def argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId

  def argumentSlotOffset: scala.Int
}

object ArgumentStateMap extends scala.AnyRef {

  def filter(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
             predicate: scala.Function1[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, scala.Boolean]): scala.Unit = {
    null
  }

  def filter[FILTER_STATE](argumentSlotOffset: scala.Int,
                           morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                           onArgument: scala.Function2[scala.Long, scala.Long, FILTER_STATE],
                           onRow: scala.Function2[FILTER_STATE, org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, scala.Boolean]): scala.Unit = {
    null
  }

  def map[T](argumentSlotOffset: scala.Int,
             morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
             f: scala.Function1[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, T]): scala.IndexedSeq[ArgumentStateMap.PerArgument[T]] = {
    null
  }

  def foreach[T](argumentSlotOffset: scala.Int,
                 morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                 f: scala.Function2[scala.Long, org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, scala.Unit]): scala.Unit = {
    null
  }

  trait ArgumentState extends scala.AnyRef {
    def argumentRowId: scala.Long

    def argumentRowIdsForReducers: scala.Array[scala.Long]
  }

  trait WorkCanceller extends scala.AnyRef with ArgumentStateMap.ArgumentState {
    def isCancelled: scala.Boolean
  }

  trait MorselAccumulator[DATA <: scala.AnyRef] extends scala.AnyRef with ArgumentStateMap.ArgumentState {
    def update(data: DATA): scala.Unit
  }

  trait ArgumentStateFactory[S <: ArgumentStateMap.ArgumentState] extends scala.AnyRef {
    def newStandardArgumentState(argumentRowId: scala.Long,
                                 argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                 argumentRowIdsForReducers: scala.Array[scala.Long]): S

    def newConcurrentArgumentState(argumentRowId: scala.Long,
                                   argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                   argumentRowIdsForReducers: scala.Array[scala.Long]): S

    def completeOnConstruction: scala.Boolean = {
      false
    }
  }

  trait ArgumentStateMaps extends scala.AnyRef {
    def apply(argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId): org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap[_ <: ArgumentStateMap.ArgumentState]

    def applyByIntId(argumentStateMapId: scala.Int): org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap[_ <: ArgumentStateMap.ArgumentState] = {
      null
    }
  }

  case class ArgumentStateWithCompleted[S <: ArgumentStateMap.ArgumentState](val argumentState: S,
                                                                             val isCompleted: scala.Boolean) extends scala.AnyRef with scala.Product with scala.Serializable {
  }

  case class PerArgument[T](val argumentRowId: scala.Long, val value: T) extends scala.AnyRef with scala.Product with scala.Serializable {
  }

}
