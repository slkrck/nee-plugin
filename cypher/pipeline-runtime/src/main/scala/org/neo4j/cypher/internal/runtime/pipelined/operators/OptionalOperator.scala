/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

class OptionalOperator(val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                       argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                       argumentSlotOffset: scala.Int,
                       nullableSlots: scala.Seq[org.neo4j.cypher.internal.physicalplanning.Slot],
                       slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                       argumentSize: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.Operator {
  override def createState(argumentStateCreator: org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator,
                           stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                           queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                           state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                           resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorState = {
    null
  }

  class OptionalOperatorState() extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorState {
    override def nextTasks(context: org.neo4j.cypher.internal.runtime.QueryContext,
                           state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                           operatorInput: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorInput,
                           parallelism: scala.Int,
                           resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                           argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps): scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTask] = {
      null
    }
  }

  class OTask(val morselData: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MorselData) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OptionalOperatorTask {
    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def operate(output: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                         context: org.neo4j.cypher.internal.runtime.QueryContext,
                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }

    override def canContinue: scala.Boolean = {
      false
    }

    override def filterCancelledArguments(operatorCloser: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser): scala.Boolean = {
      false
    }

    override def producingWorkUnitEvent: org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent = {
      null
    }

    override def setExecutionEvent(event: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit = {
      null
    }

    protected override def closeCursors(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }

    protected override def closeInput(operatorCloser: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser): scala.Unit = {
      null
    }
  }

}

trait OptionalOperatorTask extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTask {
  def morselData: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MorselData

  override def estimatedHeapUsage: scala.Long = {
    0
  }
}
