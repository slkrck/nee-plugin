/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.state.buffers

class OptionalMorselBuffer(id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                           tracker: org.neo4j.cypher.internal.runtime.pipelined.state.QueryCompletionTracker,
                           downstreamArgumentReducers: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatingBuffer],
                           override val argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps,
                           val argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId) extends org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentCountUpdater with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatingBuffer with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext]]] with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ClosingSource[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MorselData] with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.SinkByOrigin with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.DataHolder {
  override val argumentSlotOffset: scala.Int = {
    0
  }

  override def sinkFor[T <: scala.AnyRef](fromPipeline: org.neo4j.cypher.internal.physicalplanning.PipelineId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[T] = {
    null
  }

  override def take(): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MorselData = {
    null
  }

  override def canPut: scala.Boolean = {
    false
  }

  override def put(data: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext]]): scala.Unit = {
    null
  }

  override def hasData: scala.Boolean = {
    false
  }

  override def initiate(argumentRowId: scala.Long, argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def increment(argumentRowId: scala.Long): scala.Unit = {
    null
  }

  override def decrement(argumentRowId: scala.Long): scala.Unit = {
    null
  }

  override def clearAll(): scala.Unit = {
    null
  }

  override def close(data: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MorselData): scala.Unit = {
    null
  }

  def filterCancelledArguments(accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Boolean = {
    false
  }

  override def toString(): String = {
    null
  }
}
