/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

case class AggregationOperator(val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                               val aggregations: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator],
                               val groupings: org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression) extends scala.AnyRef with scala.Product with scala.Serializable {
  type AggPreMap = java.util.LinkedHashMap[AggregationOperator.this.groupings.KeyType, scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater]]

  def mapper(argumentSlotOffset: scala.Int,
             outputBufferId: org.neo4j.cypher.internal.physicalplanning.BufferId,
             expressionValues: scala.Array[org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression]): AggregationOperator.this.AggregationMapperOperator = {
    null
  }

  def reducer(argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
              reducerOutputSlots: scala.Array[scala.Int]): AggregationOperator.this.AggregationReduceOperator = {
    null
  }

  abstract class AggregatingAccumulator() extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[AggregationOperator.this.AggPreMap] {
    def result(): java.util.Iterator[java.util.Map.Entry[AggregationOperator.this.groupings.KeyType, scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Reducer]]]
  }

  class AggregationMapperOperator(argumentSlotOffset: scala.Int,
                                  outputBufferId: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                  expressionValues: scala.Array[org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperator {
    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def outputBuffer: scala.Option[org.neo4j.cypher.internal.physicalplanning.BufferId] = {
      null
    }

    override def createState(executionState: org.neo4j.cypher.internal.runtime.pipelined.ExecutionState,
                             pipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId): org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState = {
      null
    }

    class State(sink: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[AggregationOperator.this.AggPreMap]]]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState {
      override def trackTime: scala.Boolean = {
        false
      }

      override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
        null
      }

      override def prepareOutput(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                 context: org.neo4j.cypher.internal.runtime.QueryContext,
                                 state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                 resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                 operatorExecutionEvent: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): AggregationMapperOperator.this.PreAggregatedOutput = {
        null
      }
    }

    class PreAggregatedOutput(preAggregated: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[AggregationOperator.this.AggPreMap]],
                              sink: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.PerArgument[AggregationOperator.this.AggPreMap]]]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput {
      override def produce(): scala.Unit = {
        null
      }
    }

  }

  class AggregationReduceOperator(val argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                  reducerOutputSlots: scala.Array[scala.Int]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.Operator with org.neo4j.cypher.internal.runtime.pipelined.operators.ReduceOperatorState[AggregationOperator.this.AggPreMap, AggregationOperator.this.AggregatingAccumulator] {
    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def createState(argumentStateCreator: org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator,
                             stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                             queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                             state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                             resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.operators.ReduceOperatorState[AggregationOperator.this.AggPreMap, AggregationOperator.this.AggregatingAccumulator] = {
      null
    }

    override def nextTasks(queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                           state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                           input: AggregationOperator.this.AggregatingAccumulator,
                           resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithAccumulator[AggregationOperator.this.AggPreMap, AggregationOperator.this.AggregatingAccumulator]] = {
      null
    }

    class OTask(override val accumulator: AggregationOperator.this.AggregatingAccumulator) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithAccumulator[AggregationOperator.this.AggPreMap, AggregationOperator.this.AggregatingAccumulator] {
      override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
        null
      }

      override def operate(outputRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                           context: org.neo4j.cypher.internal.runtime.QueryContext,
                           state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                           resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
        null
      }

      override def canContinue: scala.Boolean = {
        false
      }
    }

  }

  class StandardAggregatingAccumulator(override val argumentRowId: scala.Long,
                                       aggregators: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator],
                                       override val argumentRowIdsForReducers: scala.Array[scala.Long],
                                       memoryTracker: org.neo4j.cypher.internal.runtime.QueryMemoryTracker) extends AggregationOperator.this.AggregatingAccumulator {
    val reducerMap: java.util.LinkedHashMap[AggregationOperator.this.groupings.KeyType, scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Reducer]] = {
      null
    }

    override def update(data: AggregationOperator.this.AggPreMap): scala.Unit = {
      null
    }

    def result(): java.util.Iterator[java.util.Map.Entry[AggregationOperator.this.groupings.KeyType, scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Reducer]]] = {
      null
    }
  }

  class ConcurrentAggregatingAccumulator(override val argumentRowId: scala.Long,
                                         aggregators: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator],
                                         override val argumentRowIdsForReducers: scala.Array[scala.Long]) extends AggregationOperator.this.AggregatingAccumulator {
    val reducerMap: java.util.concurrent.ConcurrentHashMap[AggregationOperator.this.groupings.KeyType, scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Reducer]] = {
      null
    }

    override def update(data: AggregationOperator.this.AggPreMap): scala.Unit = {
      null
    }

    def result(): java.util.Iterator[java.util.Map.Entry[AggregationOperator.this.groupings.KeyType, scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Reducer]]] = {
      null
    }
  }

  object AggregatingAccumulator extends scala.AnyRef {

    class Factory(aggregators: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator],
                  memoryTracker: org.neo4j.cypher.internal.runtime.QueryMemoryTracker) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateFactory[AggregationOperator.this.AggregatingAccumulator] {
      override def newStandardArgumentState(argumentRowId: scala.Long,
                                            argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                            argumentRowIdsForReducers: scala.Array[scala.Long]): AggregationOperator.this.AggregatingAccumulator = {
        null
      }

      override def newConcurrentArgumentState(argumentRowId: scala.Long,
                                              argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                              argumentRowIdsForReducers: scala.Array[scala.Long]): AggregationOperator.this.AggregatingAccumulator = {
        null
      }
    }

  }

}
