/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

class ManyQueriesExactNodeIndexSeekTaskTemplate(override val inner: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate,
                                                id: org.neo4j.cypher.internal.v4_0.util.attribution.Id,
                                                innermost: org.neo4j.cypher.internal.runtime.pipelined.operators.DelegateOperatorTaskTemplate,
                                                nodeVarName: String,
                                                offset: scala.Int,
                                                property: org.neo4j.cypher.internal.physicalplanning.SlottedIndexedProperty,
                                                generatePredicate: scala.Function0[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression],
                                                queryIndexId: scala.Int,
                                                order: org.neo4j.internal.schema.IndexOrder,
                                                argumentSize: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size)
                                               (codeGen: org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler) extends org.neo4j.cypher.internal.runtime.pipelined.operators.InputLoopTaskTemplate(inner, id, innermost, codeGen) {
  override def genMoreFields: scala.Seq[org.neo4j.codegen.api.Field] = {
    null
  }

  override def genLocalVariables: scala.Seq[org.neo4j.codegen.api.LocalVariable] = {
    null
  }

  override def genExpressions: scala.Seq[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression] = {
    null
  }

  override def genSetExecutionEvent(event: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def genInitializeInnerLoop: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def genInnerLoop: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def genCloseInnerLoop: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
}

object ManyQueriesExactNodeIndexSeekTaskTemplate extends scala.AnyRef {
  val nextMethod: org.neo4j.codegen.api.Method = {
    null
  }

  def next(index: org.neo4j.internal.kernel.api.IndexReadSession,
           cursor: org.neo4j.internal.kernel.api.NodeValueIndexCursor,
           queries: org.neo4j.cypher.internal.runtime.pipelined.operators.ExactPredicateIterator,
           read: org.neo4j.internal.kernel.api.Read): scala.Boolean = {
    false
  }

  def unorderedQueryIterator(propertyKey: scala.Int,
                             list: org.neo4j.values.virtual.ListValue): org.neo4j.cypher.internal.runtime.pipelined.operators.ExactPredicateIterator = {
    null
  }

  def ascendingQueryIterator(propertyKey: scala.Int,
                             list: org.neo4j.values.virtual.ListValue): org.neo4j.cypher.internal.runtime.pipelined.operators.ExactPredicateIterator = {
    null
  }

  def descendingQueryIterator(propertyKey: scala.Int,
                              list: org.neo4j.values.virtual.ListValue): org.neo4j.cypher.internal.runtime.pipelined.operators.ExactPredicateIterator = {
    null
  }
}
