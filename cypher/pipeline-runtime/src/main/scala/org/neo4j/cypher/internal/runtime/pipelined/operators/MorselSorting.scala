/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

object MorselSorting extends scala.AnyRef {
  def createComparator(orderBy: scala.Seq[org.neo4j.cypher.internal.runtime.slotted.ColumnOrder]): java.util.Comparator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] = {
    null
  }

  def compareMorselIndexesByColumnOrder(row: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext)
                                       (order: org.neo4j.cypher.internal.runtime.slotted.ColumnOrder): java.util.Comparator[java.lang.Integer] = {
    null
  }

  def createMorselIndexesArray(row: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Array[java.lang.Integer] = {
    null
  }

  def createSortedMorselData(inputRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                             outputToInputIndexes: scala.Array[java.lang.Integer]): scala.Unit = {
    null
  }

  def createMorselComparator(order: org.neo4j.cypher.internal.runtime.slotted.ColumnOrder): java.util.Comparator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] = {
    null
  }
}
