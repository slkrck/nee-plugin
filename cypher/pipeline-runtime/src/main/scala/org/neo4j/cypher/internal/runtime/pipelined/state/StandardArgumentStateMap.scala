/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.state

class StandardArgumentStateMap[STATE <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentState](val argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                                                                                                          val argumentSlotOffset: scala.Int,
                                                                                                                          factory: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateFactory[STATE]) extends org.neo4j.cypher.internal.runtime.pipelined.state.AbstractArgumentStateMap[STATE, org.neo4j.cypher.internal.runtime.pipelined.state.AbstractArgumentStateMap.StateController[STATE]] {
  protected override val controllers: java.util.LinkedHashMap[scala.Long, org.neo4j.cypher.internal.runtime.pipelined.state.AbstractArgumentStateMap.StateController[STATE]] = {
    null
  }
  protected override var lastCompletedArgumentId: scala.Long = {
    0
  }

  protected override def newStateController(argument: scala.Long,
                                            argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                            argumentRowIdsForReducers: scala.Array[scala.Long]): org.neo4j.cypher.internal.runtime.pipelined.state.AbstractArgumentStateMap.StateController[STATE] = {
    null
  }
}

object StandardArgumentStateMap extends scala.AnyRef {

  private[state] class StandardStateController[STATE <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentState](override val state: STATE) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.AbstractArgumentStateMap.StateController[STATE] {
    override def isZero: scala.Boolean = {
      false
    }

    override def increment(): scala.Long = {
      0
    }

    override def decrement(): scala.Long = {
      0
    }

    override def tryTake(): scala.Boolean = {
      false
    }

    override def take(): scala.Boolean = {
      false
    }

    override def toString(): String = {
      null
    }
  }

}
