/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined

case class ExecutablePipeline(val id: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                              val lhs: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                              val rhs: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                              val start: org.neo4j.cypher.internal.runtime.pipelined.operators.Operator,
                              val middleOperators: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.operators.MiddleOperator],
                              val serial: scala.Boolean,
                              val slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                              val inputBuffer: org.neo4j.cypher.internal.physicalplanning.BufferDefinition,
                              val outputOperator: org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperator,
                              val needsMorsel: scala.Boolean = {
                                false
                              },
                              val needsFilteringMorsel: scala.Boolean = {
                                false
                              }) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity with scala.Product with scala.Serializable {
  override val workId: org.neo4j.cypher.internal.v4_0.util.attribution.Id = {
    new org.neo4j.cypher.internal.v4_0.util.attribution.Id(0)
  }
  override val workDescription: String = {
    null
  }

  def isFused: scala.Boolean = {
    false
  }

  def createState(executionState: org.neo4j.cypher.internal.runtime.pipelined.ExecutionState,
                  queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                  queryState: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                  resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                  stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory): PipelineState = {
    null.asInstanceOf[org.neo4j.cypher.internal.runtime.pipelined.PipelineState]
  }

  override def toString(): String = {
    null
  }
}

class PipelineState(val pipeline: org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline,
                    startState: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorState,
                    middleTasks: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask],
                    outputOperatorStateCreator: scala.Function1[org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask, org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState],
                    executionState: org.neo4j.cypher.internal.runtime.pipelined.ExecutionState) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorInput with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorCloser {
  def nextTask(context: org.neo4j.cypher.internal.runtime.QueryContext,
               state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
               resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.SchedulingResult[org.neo4j.cypher.internal.runtime.pipelined.PipelineTask] = {
    null
  }

  def allocateMorsel(producingWorkUnitEvent: org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent,
                     state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState): org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
    null
  }

  def putContinuation(task: org.neo4j.cypher.internal.runtime.pipelined.PipelineTask,
                      wakeUp: scala.Boolean,
                      resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
    null
  }

  override def takeMorsel(): org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer = {
    null
  }

  override def takeAccumulator[DATA <: scala.AnyRef, ACC <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[DATA]](): ACC = {
    return this.executionState.takeAccumulator(this.pipeline.inputBuffer.id)
  }

  override def takeAccumulatorAndMorsel[DATA <: scala.AnyRef, ACC <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[DATA]](): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatorAndMorsel[DATA, ACC] = {
    null
  }

  override def takeData[DATA <: scala.AnyRef](): DATA = return this.executionState.takeData(this.pipeline.inputBuffer.id)

  override def closeMorsel(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = this.executionState.closeMorselTask(this.pipeline, morsel)

  override def closeData[DATA <: scala.AnyRef](data: DATA): scala.Unit = {
    this.executionState.closeData(this.pipeline, data)
  }

  override def closeAccumulator(accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Unit = {
    null
  }

  override def closeMorselAndAccumulatorTask(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                             accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Unit = {
    null
  }

  override def filterCancelledArguments(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Boolean = {
    false
  }

  override def filterCancelledArguments(accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Boolean = {
    false
  }

  override def filterCancelledArguments(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                        accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Boolean = {
    false
  }
}

case class PipelineTask(val startTask: org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTask,
                        val middleTasks: scala.Array[org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask],
                        val outputOperatorState: org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState,
                        val queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                        val state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                        val pipelineState: org.neo4j.cypher.internal.runtime.pipelined.PipelineState) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.Task[org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources] with org.neo4j.cypher.internal.runtime.WithHeapUsageEstimation with scala.Product with scala.Serializable {
  override def executeWorkUnit(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                               workUnitEvent: org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent,
                               queryProfiler: org.neo4j.cypher.internal.profiling.QueryProfiler): org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput = {
    null
  }

  def filterCancelledArguments(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Boolean = {
    false
  }

  def close(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
    null
  }

  override def workId: org.neo4j.cypher.internal.v4_0.util.attribution.Id = {
    new org.neo4j.cypher.internal.v4_0.util.attribution.Id(0)
  }

  override def workDescription: String = {
    null
  }

  override def canContinue: scala.Boolean = {
    false
  }

  override def estimatedHeapUsage: scala.Long = {
    0
  }
}

