/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.state.buffers

class Buffers(numBuffers: scala.Int,
              tracker: org.neo4j.cypher.internal.runtime.pipelined.state.QueryCompletionTracker,
              argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps,
              stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory) extends scala.AnyRef {
  def sink[T <: scala.AnyRef](fromPipeline: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                              bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[T] = {
    null
  }

  def source[S <: scala.AnyRef](bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Source[S] = {
    null
  }

  def hasData(bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): scala.Boolean = {
    false
  }

  def morselBuffer(bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MorselBuffer = {
    null
  }

  def applyBuffer(bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MorselApplyBuffer = {
    null
  }

  def closingSource[S <: scala.AnyRef](bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ClosingSource[S] = {
    null
  }

  def argumentStateBuffer(bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.MorselArgumentStateBuffer[_, _] = {
    null
  }

  def lhsAccumulatingRhsStreamingBuffer(bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.LHSAccumulatingRHSStreamingBuffer[_, _] = {
    null
  }

  def clearAll(): scala.Unit = {
    null
  }

  def assertAllEmpty(): scala.Unit = {
    null
  }

  private[state] def constructBuffer(bufferDefinition: org.neo4j.cypher.internal.physicalplanning.BufferDefinition): scala.Unit = {
    null
  }
}

object Buffers extends scala.AnyRef {

  trait SinkByOrigin extends scala.AnyRef {
    def sinkFor[T <: scala.AnyRef](fromPipeline: org.neo4j.cypher.internal.physicalplanning.PipelineId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[T]
  }

  trait DataHolder extends scala.AnyRef {
    def clearAll(): scala.Unit
  }

  trait AccumulatingBuffer extends scala.AnyRef {
    def argumentSlotOffset: scala.Int

    def initiate(argumentRowId: scala.Long, argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit

    def increment(argumentRowId: scala.Long): scala.Unit

    def decrement(argumentRowId: scala.Long): scala.Unit
  }

  case class AccumulatorAndMorsel[DATA <: scala.AnyRef, ACC <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[DATA]](val acc: ACC,
                                                                                                                                                           val morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext) extends scala.AnyRef with scala.Product with scala.Serializable {
  }

}
