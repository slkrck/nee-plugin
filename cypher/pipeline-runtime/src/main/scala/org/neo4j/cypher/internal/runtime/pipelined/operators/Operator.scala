/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

trait Operator extends scala.AnyRef with org.neo4j.cypher.internal.runtime.scheduling.HasWorkIdentity {
  def createState(argumentStateCreator: org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator,
                  stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                  queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                  state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                  resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorState
}

trait OperatorInput extends scala.AnyRef {
  def takeMorsel(): org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer

  def takeAccumulator[DATA <: scala.AnyRef, ACC <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[DATA]](): ACC

  def takeAccumulatorAndMorsel[DATA <: scala.AnyRef, ACC <: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[DATA]](): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatorAndMorsel[DATA, ACC]

  def takeData[DATA <: scala.AnyRef](): DATA
}

trait OperatorCloser extends scala.AnyRef {
  def closeMorsel(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit

  def closeData[DATA <: scala.AnyRef](data: DATA): scala.Unit

  def closeAccumulator(accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Unit

  def closeMorselAndAccumulatorTask(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                    accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Unit

  def filterCancelledArguments(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Boolean

  def filterCancelledArguments(accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Boolean

  def filterCancelledArguments(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                               accumulator: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[_]): scala.Boolean
}

trait OperatorState extends scala.AnyRef {
  def nextTasks(context: org.neo4j.cypher.internal.runtime.QueryContext,
                state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                operatorInput: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorInput,
                parallelism: scala.Int,
                resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps): scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTask]
}

trait OperatorTask extends scala.AnyRef with org.neo4j.cypher.internal.runtime.scheduling.HasWorkIdentity {
  def operateWithProfile(output: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                         context: org.neo4j.cypher.internal.runtime.QueryContext,
                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                         queryProfiler: org.neo4j.cypher.internal.profiling.QueryProfiler): scala.Unit = {
    null
  }

  def setExecutionEvent(event: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit

  def operate(output: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
              context: org.neo4j.cypher.internal.runtime.QueryContext,
              state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
              resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit
}


