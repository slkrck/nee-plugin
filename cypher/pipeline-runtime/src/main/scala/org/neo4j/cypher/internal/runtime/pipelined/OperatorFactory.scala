/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined

class OperatorFactory(val executionGraphDefinition: org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition,
                      val converters: org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverters,
                      val readOnly: scala.Boolean,
                      val indexRegistrator: org.neo4j.cypher.internal.runtime.QueryIndexRegistrator,
                      semanticTable: org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable,
                      val interpretedPipesFallbackPolicy: org.neo4j.cypher.internal.runtime.pipelined.InterpretedPipesFallbackPolicy,
                      val slottedPipeBuilder: scala.Option[org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeMapper]) extends scala.AnyRef {
  def create(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
             inputBuffer: org.neo4j.cypher.internal.physicalplanning.BufferDefinition): org.neo4j.cypher.internal.runtime.pipelined.operators.Operator = {
    null
  }

  def createMiddleOperators(middlePlans: scala.Seq[org.neo4j.cypher.internal.logical.plans.LogicalPlan],
                            headOperator: org.neo4j.cypher.internal.runtime.pipelined.operators.Operator): scala.Array[org.neo4j.cypher.internal.runtime.pipelined.operators.MiddleOperator] = {
    null
  }

  def createProduceResults(plan: org.neo4j.cypher.internal.logical.plans.ProduceResult): org.neo4j.cypher.internal.runtime.pipelined.operators.ProduceResultOperator = {
    null
  }

  def createOutput(outputDefinition: org.neo4j.cypher.internal.physicalplanning.OutputDefinition,
                   nextPipelineFused: scala.Boolean): org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperator = {
    null
  }

  protected def createMiddleOrUpdateSlottedPipeChain(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
                                                     maybeSlottedPipeOperatorToChainOnTo: scala.Option[org.neo4j.cypher.internal.runtime.pipelined.operators.SlottedPipeOperator]): scala.Option[org.neo4j.cypher.internal.runtime.pipelined.operators.MiddleOperator] = {
    null
  }

  protected def createSlottedPipeHeadOperator(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan): org.neo4j.cypher.internal.runtime.pipelined.operators.Operator = {
    null
  }

  protected def createSlottedPipeMiddleOperator(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
                                                maybeSlottedPipeOperatorToChainOnTo: scala.Option[org.neo4j.cypher.internal.runtime.pipelined.operators.SlottedPipeOperator]): scala.Option[org.neo4j.cypher.internal.runtime.pipelined.operators.MiddleOperator] = {
    null
  }
}
