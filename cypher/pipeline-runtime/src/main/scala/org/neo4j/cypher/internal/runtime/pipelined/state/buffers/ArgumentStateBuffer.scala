/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.state.buffers

class ArgumentStateBuffer(override val argumentRowId: scala.Long,
                          inner: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffer[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext],
                          override val argumentRowIdsForReducers: scala.Array[scala.Long]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.MorselAccumulator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffer[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] {
  override def update(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def put(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def canPut: scala.Boolean = {
    false
  }

  override def hasData: scala.Boolean = {
    false
  }

  override def take(): org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
    null
  }

  override def foreach(f: scala.Function1[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext, scala.Unit]): scala.Unit = {
    null
  }

  override def iterator: java.util.Iterator[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] = {
    null
  }

  override def toString(): String = {
    null
  }
}

object ArgumentStateBuffer extends scala.AnyRef {

  class Factory(stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateFactory[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer] {
    override def newStandardArgumentState(argumentRowId: scala.Long,
                                          argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                          argumentRowIdsForReducers: scala.Array[scala.Long]): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer = {
      null
    }

    override def newConcurrentArgumentState(argumentRowId: scala.Long,
                                            argumentMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                            argumentRowIdsForReducers: scala.Array[scala.Long]): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.ArgumentStateBuffer = {
      null
    }
  }

}
