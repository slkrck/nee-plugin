/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.state

abstract class ArgumentCountUpdater() extends scala.AnyRef {
  def argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps

  protected def initiateArgumentStatesHere(argumentStates: scala.IndexedSeq[org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId],
                                           argumentRowId: scala.Long,
                                           morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  protected def initiateArgumentReducersHere(accumulatingBuffers: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatingBuffer],
                                             argumentRowId: scala.Long,
                                             morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  protected def forAllArgumentReducersAndGetArgumentRowIds(accumulatingBuffers: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatingBuffer],
                                                           morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                                           fun: scala.Function2[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatingBuffer, scala.Long, scala.Unit]): scala.Array[scala.Long] = {
    null
  }

  protected def forAllArgumentReducers(accumulatingBuffers: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatingBuffer],
                                       argumentRowIds: scala.Array[scala.Long],
                                       fun: scala.Function2[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatingBuffer, scala.Long, scala.Unit]): scala.Unit = {
    null
  }

  protected def incrementArgumentCounts(accumulatingBuffers: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatingBuffer],
                                        morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  protected def decrementArgumentCounts(accumulatingBuffers: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatingBuffer],
                                        morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  protected def decrementArgumentCounts(accumulatingBuffers: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatingBuffer],
                                        argumentRowIds: scala.IndexedSeq[scala.Long]): scala.Unit = {
    null
  }
}
