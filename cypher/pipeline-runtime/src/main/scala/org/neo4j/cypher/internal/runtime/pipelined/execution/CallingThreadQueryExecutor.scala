/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.execution

class CallingThreadQueryExecutor(cursors: org.neo4j.internal.kernel.api.CursorFactory) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.execution.QueryExecutor with org.neo4j.cypher.internal.runtime.pipelined.execution.WorkerWaker {
  override def wakeOne(): scala.Unit = {
    null
  }

  override def assertAllReleased(): scala.Unit = {
    null
  }

  override def execute[E <: scala.Exception](executablePipelines: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline],
                                             executionGraphDefinition: org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition,
                                             inputDataStream: org.neo4j.cypher.internal.runtime.InputDataStream,
                                             queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                                             params: scala.Array[org.neo4j.values.AnyValue],
                                             schedulerTracer: org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer,
                                             queryIndexes: scala.Array[org.neo4j.internal.kernel.api.IndexReadSession],
                                             nExpressionSlots: scala.Int,
                                             prePopulateResults: scala.Boolean,
                                             subscriber: org.neo4j.kernel.impl.query.QuerySubscriber,
                                             doProfile: scala.Boolean,
                                             morselSize: scala.Int,
                                             memoryTracking: org.neo4j.cypher.internal.runtime.MemoryTracking,
                                             executionGraphSchedulingPolicy: org.neo4j.cypher.internal.runtime.pipelined.execution.ExecutionGraphSchedulingPolicy): org.neo4j.cypher.internal.runtime.pipelined.execution.ProfiledQuerySubscription = {
    null
  }
}
