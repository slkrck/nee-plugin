/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

abstract class SlottedPipeOperator(val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentityMutableDescription,
                                   initialPipe: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe) extends scala.AnyRef {
  def pipe: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe = {
    null
  }

  def setPipe(newPipe: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe): scala.Unit = {
    null
  }
}

object SlottedPipeOperator extends scala.AnyRef {
  def createFeedPipeQueryState(inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                               queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                               morselQueryState: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                               resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                               memoryTracker: org.neo4j.cypher.internal.runtime.QueryMemoryTracker): org.neo4j.cypher.internal.runtime.pipelined.operators.FeedPipeQueryState = {
    null
  }

  def updateProfileEvent(profileEvent: org.neo4j.cypher.internal.profiling.OperatorProfileEvent,
                         profileInformation: org.neo4j.cypher.internal.runtime.interpreted.profiler.InterpretedProfileInformation): scala.Unit = {
    null
  }
}

class SlottedPipeHeadOperator(workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentityMutableDescription,
                              initialPipe: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe) extends org.neo4j.cypher.internal.runtime.pipelined.operators.SlottedPipeOperator(workIdentity, initialPipe) with org.neo4j.cypher.internal.runtime.pipelined.operators.Operator {
  override def toString(): String = {
    null
  }

  override def createState(argumentStateCreator: org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator,
                           stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                           queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                           state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                           resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorState = {
    null
  }

  class OTask(val inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
              val feedPipeQueryState: org.neo4j.cypher.internal.runtime.pipelined.operators.FeedPipeQueryState) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorsel with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorWithInterpretedDBHitsProfiling {
    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def toString(): String = {
      null
    }

    override def operate(outputRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                         context: org.neo4j.cypher.internal.runtime.QueryContext,
                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }

    override def canContinue: scala.Boolean = {
      false
    }

    override def setExecutionEvent(event: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit = {
      null
    }

    protected override def closeCursors(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }
  }

}

class SlottedPipeMiddleOperator(workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentityMutableDescription,
                                val initialPipe: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe) extends org.neo4j.cypher.internal.runtime.pipelined.operators.SlottedPipeOperator(workIdentity, initialPipe) with org.neo4j.cypher.internal.runtime.pipelined.operators.MiddleOperator {
  override def toString(): String = {
    null
  }

  override def createTask(argumentStateCreator: org.neo4j.cypher.internal.runtime.pipelined.ArgumentStateMapCreator,
                          stateFactory: org.neo4j.cypher.internal.runtime.pipelined.state.StateFactory,
                          queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                          state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                          resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask = {
    null
  }

  class OMiddleTask(val feedPipeQueryState: org.neo4j.cypher.internal.runtime.pipelined.operators.FeedPipeQueryState) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTask with org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorWithInterpretedDBHitsProfiling {
    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def toString(): String = {
      null
    }

    override def operate(outputRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                         context: org.neo4j.cypher.internal.runtime.QueryContext,
                         state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                         resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }

    override def setExecutionEvent(event: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit = {
      null
    }
  }

}