/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.execution

class MorselExecutionContext(final val morsel: Morsel,
                             final val slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                             final val maxNumberOfRows: scala.Int,
                             private[execution] var currentRow: scala.Int = {
                               0
                             },
                             private[execution] var startRow: scala.Int = {
                               0
                             },
                             private[execution] var endRow: scala.Int = {
                               0
                             },
                             final val producingWorkUnitEvent: org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent = {
                               null
                             }) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.ExecutionContext with org.neo4j.cypher.internal.runtime.slotted.SlottedCompatible {
  protected final val longsPerRow: scala.Int = {
    0
  }
  protected final val refsPerRow: scala.Int = {
    0
  }

  def attach(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  def detach(): org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
    null
  }

  def shallowCopy(): org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
    null
  }

  @scala.inline
  def numberOfRows: scala.Int = {
    0
  }

  @scala.inline
  def moveToNextRow(): scala.Unit = {
    null
  }

  @scala.inline
  def getValidRows: scala.Int = {
    0
  }

  @scala.inline
  def getFirstRow: scala.Int = {
    0
  }

  @scala.inline
  def getLastRow: scala.Int = {
    0
  }

  @scala.inline
  def getCurrentRow: scala.Int = {
    0
  }

  @scala.inline
  def setCurrentRow(row: scala.Int): scala.Unit = {
    null
  }

  @scala.inline
  def getLongsPerRow: scala.Int = {
    0
  }

  @scala.inline
  def getRefsPerRow: scala.Int = {
    0
  }

  @scala.inline
  def resetToFirstRow(): scala.Unit = {
    null
  }

  @scala.inline
  def resetToBeforeFirstRow(): scala.Unit = {
    null
  }

  @scala.inline
  def setToAfterLastRow(): scala.Unit = {
    null
  }

  @scala.inline
  def isValidRow: scala.Boolean = {
    false
  }

  @scala.inline
  def hasNextRow: scala.Boolean = {
    false
  }

  @scala.inline
  def hasData: scala.Boolean = {
    false
  }

  @scala.inline
  def isEmpty: scala.Boolean = {
    false
  }

  @scala.inline
  def finishedWriting(): scala.Unit = {
    null
  }

  def finishedWritingUsing(otherContext: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  def view(start: scala.Int, end: scala.Int): org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
    null
  }

  def compactRowsFrom(input: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def copyTo(target: org.neo4j.cypher.internal.runtime.ExecutionContext, sourceLongOffset: scala.Int = {
    0
  }, sourceRefOffset: scala.Int = {
    0
  }, targetLongOffset: scala.Int = {
    0
  }, targetRefOffset: scala.Int = {
    0
  }): scala.Unit = {
    null
  }

  override def copyFrom(input: org.neo4j.cypher.internal.runtime.ExecutionContext, nLongs: scala.Int, nRefs: scala.Int): scala.Unit = {
    null
  }

  override def copyToSlottedExecutionContext(other: org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext,
                                             nLongs: scala.Int,
                                             nRefs: scala.Int): scala.Unit = {
    null
  }

  override def toString(): String = {
    null
  }

  def prettyCurrentRow: String = {
    null
  }

  def prettyString: scala.Seq[String] = {
    null
  }

  def copyFrom(input: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def setLongAt(offset: scala.Int, value: scala.Long): scala.Unit = {
    null
  }

  override def getLongAt(offset: scala.Int): scala.Long = {
    0
  }

  def getLongAt(row: scala.Int, offset: scala.Int): scala.Long = {
    0
  }

  def getArgumentAt(offset: scala.Int): scala.Long = {
    0
  }

  def setArgumentAt(offset: scala.Int, argument: scala.Long): scala.Unit = {
    null
  }

  override def setRefAt(offset: scala.Int, value: org.neo4j.values.AnyValue): scala.Unit = {
    null
  }

  override def getRefAt(offset: scala.Int): org.neo4j.values.AnyValue = {
    null
  }

  override def getByName(name: String): org.neo4j.values.AnyValue = {
    null
  }

  override def containsName(name: String): scala.Boolean = {
    false
  }

  override def numberOfColumns: scala.Int = {
    0
  }

  override def set(newEntries: scala.Seq[scala.Tuple2[String, org.neo4j.values.AnyValue]]): scala.Unit = {
    null
  }

  override def set(key1: String, value1: org.neo4j.values.AnyValue): scala.Unit = {
    null
  }

  override def set(key1: String,
                   value1: org.neo4j.values.AnyValue,
                   key2: String,
                   value2: org.neo4j.values.AnyValue): scala.Unit = {
    null
  }

  override def set(key1: String,
                   value1: org.neo4j.values.AnyValue,
                   key2: String,
                   value2: org.neo4j.values.AnyValue,
                   key3: String,
                   value3: org.neo4j.values.AnyValue): scala.Unit = {
    null
  }

  override def mergeWith(other: org.neo4j.cypher.internal.runtime.ExecutionContext, entityById: org.neo4j.cypher.internal.runtime.EntityById): scala.Unit = {
    null
  }

  override def createClone(): org.neo4j.cypher.internal.runtime.ExecutionContext = {
    null
  }

  override def estimatedHeapUsage: scala.Long = {
    0
  }

  override def copyWith(key1: String, value1: org.neo4j.values.AnyValue): org.neo4j.cypher.internal.runtime.ExecutionContext = {
    null
  }

  override def copyWith(key1: String,
                        value1: org.neo4j.values.AnyValue,
                        key2: String,
                        value2: org.neo4j.values.AnyValue): org.neo4j.cypher.internal.runtime.ExecutionContext = {
    null
  }

  override def copyWith(key1: String,
                        value1: org.neo4j.values.AnyValue,
                        key2: String,
                        value2: org.neo4j.values.AnyValue,
                        key3: String,
                        value3: org.neo4j.values.AnyValue): org.neo4j.cypher.internal.runtime.ExecutionContext = {
    null
  }

  override def copyWith(newEntries: scala.Seq[scala.Tuple2[String, org.neo4j.values.AnyValue]]): org.neo4j.cypher.internal.runtime.ExecutionContext = {
    null
  }

  override def boundEntities(materializeNode: scala.Function1[scala.Long, org.neo4j.values.AnyValue],
                             materializeRelationship: scala.Function1[scala.Long, org.neo4j.values.AnyValue]): _root_.scala.Predef.Map[String, org.neo4j.values.AnyValue] = {
    null
  }

  override def isNull(key: String): scala.Boolean = {
    false
  }

  override def setCachedProperty(key: org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty, value: org.neo4j.values.storable.Value): scala.Unit = {
    null
  }

  override def setCachedPropertyAt(offset: scala.Int, value: org.neo4j.values.storable.Value): scala.Unit = {
    null
  }

  override def getCachedProperty(key: org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty): org.neo4j.values.storable.Value = {
    null
  }

  override def getCachedPropertyAt(offset: scala.Int): org.neo4j.values.storable.Value = {
    null
  }

  override def invalidateCachedNodeProperties(node: scala.Long): scala.Unit = {
    null
  }

  override def invalidateCachedRelationshipProperties(rel: scala.Long): scala.Unit = {
    null
  }

  override def setLinenumber(file: String, line: scala.Long, last: scala.Boolean = {
    false
  }): scala.Unit = {
    null
  }

  override def getLinenumber: scala.Option[org.neo4j.cypher.internal.runtime.ResourceLinenumber] = {
    null
  }

  override def setLinenumber(line: scala.Option[org.neo4j.cypher.internal.runtime.ResourceLinenumber]): scala.Unit = {
    null
  }

  protected def addPrettyRowMarker(sb: scala.collection.mutable.StringBuilder, row: scala.Int): scala.Unit = {
    null
  }

  private[execution] def longsAtCurrentRow: scala.Int = {
    0
  }

  private[execution] def refsAtCurrentRow: scala.Int = {
    0
  }
}

object MorselExecutionContext extends scala.AnyRef {
  val empty: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
    null
  }

  def apply(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.Morsel,
            slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
            maxNumberOfRows: scala.Int): org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
    null
  }

  def createInitialRow(): org.neo4j.cypher.internal.runtime.pipelined.execution.FilteringMorselExecutionContext = {
    null
  }
}

class FilteringMorselExecutionContext(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.Morsel,
                                      slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                                      maxNumberOfRows: scala.Int,
                                      initialCurrentRow: scala.Int,
                                      initialStartRow: scala.Int,
                                      initialEndRow: scala.Int,
                                      producingWorkUnitEvent: org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent = {
                                        null
                                      },
                                      initialCancelledRows: java.util.BitSet = {
                                        null
                                      }) extends org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext(morsel, slots, maxNumberOfRows, initialCurrentRow, initialStartRow, initialEndRow, producingWorkUnitEvent) {
  def cropCancelledRows(): scala.Unit = {
    null
  }

  def isCancelled(row: scala.Int): scala.Boolean = {
    false
  }

  def numberOfCancelledRows: scala.Int = {
    0
  }

  def hasCancelledRows: scala.Boolean = {
    false
  }

  def cancelRow(row: scala.Int): scala.Unit = {
    null
  }

  def cancelCurrentRow(): scala.Unit = {
    null
  }

  def cancelRows(cancelledRows: java.util.BitSet): scala.Unit = {
    null
  }

  override def shallowCopy(): org.neo4j.cypher.internal.runtime.pipelined.execution.FilteringMorselExecutionContext = {
    null
  }

  override def moveToNextRow(): scala.Unit = {
    null
  }

  @scala.inline
  override def getValidRows: scala.Int = {
    0
  }

  override def getFirstRow: scala.Int = {
    0
  }

  override def getLastRow: scala.Int = {
    0
  }

  override def resetToFirstRow(): scala.Unit = {
    null
  }

  override def resetToBeforeFirstRow(): scala.Unit = {
    null
  }

  override def setToAfterLastRow(): scala.Unit = {
    null
  }

  override def isValidRow: scala.Boolean = {
    false
  }

  override def hasNextRow: scala.Boolean = {
    false
  }

  override def view(start: scala.Int, end: scala.Int): org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
    null
  }

  override def compactRowsFrom(input: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def finishedWritingUsing(otherContext: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def toString(): String = {
    null
  }

  override def prettyCurrentRow: String = {
    null
  }

  @scala.inline
  final def moveToNextRawRow(): scala.Unit = {
    null
  }

  @scala.inline
  final def moveToRawRow(row: scala.Int): scala.Unit = {
    null
  }

  @scala.inline
  final def resetToFirstRawRow(): scala.Unit = {
    null
  }

  @scala.inline
  final def isValidRawRow: scala.Boolean = {
    false
  }

  protected override def addPrettyRowMarker(sb: scala.collection.mutable.StringBuilder, row: scala.Int): scala.Unit = {
    null
  }
}

object FilteringMorselExecutionContext extends scala.AnyRef {
  def apply(source: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): org.neo4j.cypher.internal.runtime.pipelined.execution.FilteringMorselExecutionContext = {
    null
  }
}
