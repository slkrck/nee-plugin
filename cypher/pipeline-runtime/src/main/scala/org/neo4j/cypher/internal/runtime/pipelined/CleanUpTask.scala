/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined

class CleanUpTask(executionState: org.neo4j.cypher.internal.runtime.pipelined.ExecutionState) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.Task[org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources] {
  override def executeWorkUnit(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                               workUnitEvent: org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent,
                               queryProfiler: org.neo4j.cypher.internal.profiling.QueryProfiler): org.neo4j.cypher.internal.runtime.pipelined.operators.PreparedOutput = {
    null
  }

  override def canContinue: scala.Boolean = {
    false
  }

  override def workId: org.neo4j.cypher.internal.v4_0.util.attribution.Id = {
    org.neo4j.cypher.internal.v4_0.util.attribution.Id(0)
  }

  override def workDescription: String = {
    ""
  }
}
