/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.aggregators

case object AvgAggregator extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.aggregators.Aggregator with scala.Product with scala.Serializable {
  override def newUpdater: org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater = {
    null
  }

  override def newStandardReducer(memoryTracker: org.neo4j.cypher.internal.runtime.QueryMemoryTracker): org.neo4j.cypher.internal.runtime.pipelined.aggregators.Reducer = {
    null
  }

  override def newConcurrentReducer: org.neo4j.cypher.internal.runtime.pipelined.aggregators.Reducer = {
    return new AvgAggregator.AvgConcurrentReducer
  }

  abstract class AvgStandardBase() extends scala.AnyRef {
    protected def failMix(): CypherTypeException = {
      throw new CypherTypeException("avg() cannot mix number and duration")
    }

    protected def failType(value: org.neo4j.values.AnyValue): CypherTypeException = {
      throw new CypherTypeException(new StringBuilder(64).append("avg() can only handle numerical values, duration, and null. Got ").append(value).toString)
    }
  }

  class AvgUpdater() extends AvgAggregator.AvgStandardBase with org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater {
    private[AvgAggregator] var seenNumber: scala.Boolean = {
      false
    }
    private[AvgAggregator] var seenDuration: scala.Boolean = {
      false
    }
    private[AvgAggregator] var count: scala.Long = {
      0
    }
    private[AvgAggregator] var avgNumber: org.neo4j.values.storable.NumberValue = {
      null
    }
    private[AvgAggregator] var sumMonths: scala.Long = {
      0
    }
    private[AvgAggregator] var sumDays: scala.Long = {
      0
    }
    private[AvgAggregator] var sumSeconds: scala.Long = {
      0
    }
    private[AvgAggregator] var sumNanos: scala.Long = {
      0
    }

    override def update(value: org.neo4j.values.AnyValue): scala.Unit = {
      value
    }
  }

  class AvgStandardReducer() extends AvgAggregator.AvgStandardBase with org.neo4j.cypher.internal.runtime.pipelined.aggregators.Reducer {
    private[AvgAggregator] var seenNumber: scala.Boolean = {
      false
    }
    private[AvgAggregator] var seenDuration: scala.Boolean = {
      false
    }
    private[AvgAggregator] var count: scala.Long = {
      0
    }
    private[AvgAggregator] var avgNumber: org.neo4j.values.storable.NumberValue = {
      null
    }
    private[AvgAggregator] var monthsRunningAvg: scala.Double = {
      0
    }
    private[AvgAggregator] var daysRunningAvg: scala.Double = {
      0
    }
    private[AvgAggregator] var secondsRunningAvg: scala.Double = {
      0
    }
    private[AvgAggregator] var nanosRunningAvg: scala.Double = {
      0
    }

    override def update(updater: org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater): scala.Unit = {
      null
    }

    override def result: org.neo4j.values.AnyValue = {
      null
    }
  }

  class AvgConcurrentReducer() extends AvgAggregator.AvgStandardBase with org.neo4j.cypher.internal.runtime.pipelined.aggregators.Reducer {

    override def update(updater: org.neo4j.cypher.internal.runtime.pipelined.aggregators.Updater): scala.Unit = {
      null
    }

    override def result: org.neo4j.values.AnyValue = {
      null
    }

    case class AvgNumber(val count: scala.Long = {
      0
    }, val avgNumber: org.neo4j.values.storable.NumberValue = {
      null
    }) extends scala.AnyRef with scala.Product with scala.Serializable {
    }

    case class AvgDuration(val count: scala.Long = {
      0
    }, val monthsRunningAvg: scala.Double = {
      0
    }, val daysRunningAvg: scala.Double = {
      0
    }, val secondsRunningAvg: scala.Double = {
      0
    }, val nanosRunningAvg: scala.Double = {
      0
    }) extends scala.AnyRef with scala.Product with scala.Serializable {
    }

  }

}
