/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

class UnwindOperator(val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity,
                     collection: org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression,
                     offset: scala.Int) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.StreamingOperator with org.neo4j.cypher.internal.runtime.ListSupport {
  protected override def nextTasks(context: org.neo4j.cypher.internal.runtime.QueryContext,
                                   state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                   inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer,
                                   parallelism: scala.Int,
                                   resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                   argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps): scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorsel] = {
    null
  }

  class OTask(val inputMorsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext) extends org.neo4j.cypher.internal.runtime.pipelined.operators.InputLoopTask {
    override def workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
      null
    }

    override def setExecutionEvent(event: org.neo4j.cypher.internal.profiling.OperatorProfileEvent): scala.Unit = {
      null
    }

    override def canContinue: scala.Boolean = {
      false
    }

    protected override def initializeInnerLoop(context: org.neo4j.cypher.internal.runtime.QueryContext,
                                               state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                               resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                               initExecutionContext: org.neo4j.cypher.internal.runtime.ExecutionContext): scala.Boolean = {
      false
    }

    protected override def innerLoop(outputRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                                     context: org.neo4j.cypher.internal.runtime.QueryContext,
                                     state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState): scala.Unit = {
      null
    }

    protected override def closeInnerLoop(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
      null
    }
  }

}

class UnwindOperatorTaskTemplate(inner: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate,
                                 id: org.neo4j.cypher.internal.v4_0.util.attribution.Id,
                                 innermost: org.neo4j.cypher.internal.runtime.pipelined.operators.DelegateOperatorTaskTemplate,
                                 isHead: scala.Boolean,
                                 rawListExpression: org.neo4j.cypher.internal.v4_0.expressions.Expression,
                                 offset: scala.Int)
                                (codeGen: org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler) extends org.neo4j.cypher.internal.runtime.pipelined.operators.InputLoopTaskTemplate(inner, id, innermost, codeGen) {
  final override def scopeId: String = {
    null
  }

  override def genMoreFields: scala.Seq[org.neo4j.codegen.api.Field] = {
    null
  }

  override def genLocalVariables: scala.Seq[org.neo4j.codegen.api.LocalVariable] = {
    null
  }

  override def genSetExecutionEvent(event: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def genExpressions: scala.Seq[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression] = {
    null
  }

  protected override def genInitializeInnerLoop: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def genInnerLoop: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def genCloseInnerLoop: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
}