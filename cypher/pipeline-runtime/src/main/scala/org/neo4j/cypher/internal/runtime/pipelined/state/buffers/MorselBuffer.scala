/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.state.buffers

class MorselBuffer(id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                   tracker: org.neo4j.cypher.internal.runtime.pipelined.state.QueryCompletionTracker,
                   downstreamArgumentReducers: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.AccumulatingBuffer],
                   workCancellers: scala.IndexedSeq[org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId],
                   override val argumentStateMaps: org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentStateMap.ArgumentStateMaps,
                   inner: org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffer[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext]) extends org.neo4j.cypher.internal.runtime.pipelined.state.ArgumentCountUpdater with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext] with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Source[org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer] with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.SinkByOrigin with org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Buffers.DataHolder {
  override def sinkFor[T <: scala.AnyRef](fromPipeline: org.neo4j.cypher.internal.physicalplanning.PipelineId): org.neo4j.cypher.internal.runtime.pipelined.state.buffers.Sink[T] = {
    null
  }

  override def put(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def canPut: scala.Boolean = {
    false
  }

  def putInDelegate(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def hasData: scala.Boolean = {
    false
  }

  override def take(): org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer = {
    null
  }

  override def clearAll(): scala.Unit = {
    null
  }

  def filterCancelledArguments(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Boolean = {
    false
  }

  def close(morsel: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext): scala.Unit = {
    null
  }

  override def toString(): String = {
    null
  }

  class Parallelizer(original: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.state.MorselParallelizer {
    override def originalForClosing: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
      null
    }

    override def nextCopy: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext = {
      null
    }
  }

}

object MorselBuffer extends scala.AnyRef {
}
