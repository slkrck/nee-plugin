/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

abstract class InputLoopTask() extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorsel {
  final override def operate(outputRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                             context: org.neo4j.cypher.internal.runtime.QueryContext,
                             state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                             resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
    null
  }

  override def canContinue: scala.Boolean = {
    false
  }

  protected def initializeInnerLoop(context: org.neo4j.cypher.internal.runtime.QueryContext,
                                    state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                                    resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources,
                                    initExecutionContext: org.neo4j.cypher.internal.runtime.ExecutionContext): scala.Boolean

  protected def innerLoop(outputRow: org.neo4j.cypher.internal.runtime.pipelined.execution.MorselExecutionContext,
                          context: org.neo4j.cypher.internal.runtime.QueryContext,
                          state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState): scala.Unit

  protected def closeInnerLoop(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit

  protected def enterOperate(context: org.neo4j.cypher.internal.runtime.QueryContext,
                             state: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryState,
                             resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
    null
  }

  protected def exitOperate(): scala.Unit = {
    null
  }

  protected override def closeCursors(resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
    null
  }
}

abstract class InputLoopTaskTemplate(override val inner: org.neo4j.cypher.internal.runtime.pipelined.operators.OperatorTaskTemplate,
                                     override val id: org.neo4j.cypher.internal.v4_0.util.attribution.Id,
                                     innermost: org.neo4j.cypher.internal.runtime.pipelined.operators.DelegateOperatorTaskTemplate,
                                     protected val codeGen: org.neo4j.cypher.internal.runtime.pipelined.OperatorExpressionCompiler,
                                     override val isHead: scala.Boolean = {
                                       false
                                     }) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.ContinuableOperatorTaskWithMorselTemplate {
  protected val canContinue: org.neo4j.codegen.api.InstanceField = {
    null
  }
  protected val innerLoop: org.neo4j.codegen.api.InstanceField = {
    null
  }

  final override def genFields: scala.Seq[org.neo4j.codegen.api.Field] = {
    null
  }

  protected final override def genOperateHead: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected final override def genOperateMiddle: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def genMoreFields: scala.Seq[org.neo4j.codegen.api.Field]

  override def genCanContinue: scala.Option[org.neo4j.codegen.api.IntermediateRepresentation] = {
    null
  }

  override def genCloseCursors: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def genInit: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  override def genClearStateOnCancelledRow: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def scopeId: String = {
    null
  }

  protected def genInitializeInnerLoop: org.neo4j.codegen.api.IntermediateRepresentation

  protected def genInnerLoop: org.neo4j.codegen.api.IntermediateRepresentation

  protected def genCloseInnerLoop: org.neo4j.codegen.api.IntermediateRepresentation

  protected def endInnerLoop: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
}
