/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

case class MorselBufferOutputOperator(val bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                      val nextPipelineHeadPlanId: org.neo4j.cypher.internal.v4_0.util.attribution.Id,
                                      val nextPipelineFused: scala.Boolean) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperator with scala.Product with scala.Serializable {
  override val workIdentity: org.neo4j.cypher.internal.runtime.scheduling.WorkIdentity = {
    null
  }

  override def outputBuffer: scala.Option[org.neo4j.cypher.internal.physicalplanning.BufferId] = {
    null
  }

  override def createState(executionState: org.neo4j.cypher.internal.runtime.pipelined.ExecutionState,
                           pipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId): org.neo4j.cypher.internal.runtime.pipelined.operators.OutputOperatorState = {
    null
  }
}
