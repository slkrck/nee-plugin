/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined.operators

object OperatorCodeGenHelperTemplates extends scala.AnyRef {

  val UNINITIALIZED_LONG_SLOT_VALUE: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val UNINITIALIZED_REF_SLOT_VALUE: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val DATA_READ_CONSTRUCTOR_PARAMETER: org.neo4j.codegen.api.Parameter = {
    null
  }
  val INPUT_MORSEL_CONSTRUCTOR_PARAMETER: org.neo4j.codegen.api.Parameter = {
    null
  }
  val ARGUMENT_STATE_MAPS_CONSTRUCTOR_PARAMETER: org.neo4j.codegen.api.Parameter = {
    null
  }
  val QUERY_RESOURCE_PARAMETER: org.neo4j.codegen.api.Parameter = {
    null
  }
  val OPERATOR_CLOSER_PARAMETER: org.neo4j.codegen.api.Parameter = {
    null
  }
  val WORK_IDENTITY_STATIC_FIELD_NAME: java.lang.String = {
    null
  }
  val DATA_READ: org.neo4j.codegen.api.InstanceField = {
    null
  }
  val INPUT_MORSEL: org.neo4j.codegen.api.InstanceField = {
    null
  }
  val QUERY_PROFILER: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val QUERY_STATE: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val QUERY_RESOURCES: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val CURSOR_POOL_V: org.neo4j.codegen.api.LocalVariable = {
    null
  }
  val CURSOR_POOL: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val OUTPUT_ROW: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val OUTPUT_ROW_MOVE_TO_NEXT: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val DB_ACCESS: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val PARAMS: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val EXPRESSION_CURSORS: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val EXPRESSION_VARIABLES: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val EXECUTION_STATE: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val PIPELINE_ID: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val SUBSCRIBER: org.neo4j.codegen.api.LocalVariable = {
    null
  }
  val SUBSCRIPTION: org.neo4j.codegen.api.LocalVariable = {
    null
  }
  val DEMAND: org.neo4j.codegen.api.LocalVariable = {
    null
  }
  val SERVED: org.neo4j.codegen.api.LocalVariable = {
    null
  }
  val HAS_DEMAND: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val PRE_POPULATE_RESULTS_V: org.neo4j.codegen.api.LocalVariable = {
    null
  }
  val PRE_POPULATE_RESULTS: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val ALLOCATE_NODE_CURSOR: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val ALLOCATE_NODE_LABEL_CURSOR: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val ALLOCATE_NODE_INDEX_CURSOR: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val ALLOCATE_GROUP_CURSOR: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val ALLOCATE_TRAVERSAL_CURSOR: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val ALLOCATE_REL_SCAN_CURSOR: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val OUTER_LOOP_LABEL_NAME: String = {
    null
  }
  val INPUT_ROW_IS_VALID: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val OUTPUT_ROW_IS_VALID: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val OUTPUT_ROW_FINISHED_WRITING: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val INPUT_ROW_MOVE_TO_NEXT: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val UPDATE_DEMAND: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val OUTPUT_COUNTER: org.neo4j.codegen.api.LocalVariable = {
    null
  }
  val UPDATE_OUTPUT_COUNTER: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val HAS_REMAINING_OUTPUT: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val NO_TOKEN: org.neo4j.codegen.api.GetStatic = {
    null
  }
  val SET_TRACER: org.neo4j.codegen.api.Method = {
    null
  }
  val NO_KERNEL_TRACER: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val NO_OPERATOR_PROFILE_EVENT: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val CALL_CAN_CONTINUE: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def allocateCursor(cursorPools: OperatorCodeGenHelperTemplates.CursorPoolsType): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def allNodeScan(cursor: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def nodeLabelScan(label: org.neo4j.codegen.api.IntermediateRepresentation,
                    cursor: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def nodeIndexScan(indexReadSession: org.neo4j.codegen.api.IntermediateRepresentation,
                    cursor: org.neo4j.codegen.api.IntermediateRepresentation,
                    order: org.neo4j.internal.schema.IndexOrder,
                    needsValues: scala.Boolean): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def nodeIndexSeek(indexReadSession: org.neo4j.codegen.api.IntermediateRepresentation,
                    cursor: org.neo4j.codegen.api.IntermediateRepresentation,
                    query: org.neo4j.codegen.api.IntermediateRepresentation,
                    order: org.neo4j.internal.schema.IndexOrder,
                    needsValues: scala.Boolean): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def indexOrder(indexOrder: org.neo4j.internal.schema.IndexOrder): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def exactSeek(prop: scala.Int, expression: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def lessThanSeek(prop: scala.Int,
                   inclusive: scala.Boolean,
                   expression: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def greaterThanSeek(prop: scala.Int,
                      inclusive: scala.Boolean,
                      expression: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def rangeBetweenSeek(prop: scala.Int,
                       fromInclusive: scala.Boolean,
                       fromExpression: org.neo4j.codegen.api.IntermediateRepresentation,
                       toInclusive: scala.Boolean,
                       toExpression: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def stringContainsScan(prop: scala.Int, expression: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def stringEndsWithScan(prop: scala.Int, expression: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def singleNode(node: org.neo4j.codegen.api.IntermediateRepresentation,
                 cursor: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def singleRelationship(relationship: org.neo4j.codegen.api.IntermediateRepresentation,
                         cursor: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def allocateAndTraceCursor(cursorField: org.neo4j.codegen.api.InstanceField,
                             executionEventField: org.neo4j.codegen.api.InstanceField,
                             allocate: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def freeCursor[CURSOR](cursor: org.neo4j.codegen.api.IntermediateRepresentation, cursorPools: OperatorCodeGenHelperTemplates.CursorPoolsType)
                        (implicit out: _root_.scala.Predef.Manifest[CURSOR]): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def cursorNext[CURSOR](cursor: org.neo4j.codegen.api.IntermediateRepresentation)
                        (implicit out: _root_.scala.Predef.Manifest[CURSOR]): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def nodeLabelId(labelName: String): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def relationshipTypeId(typeName: String): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def profilingCursorNext[CURSOR](cursor: org.neo4j.codegen.api.IntermediateRepresentation, id: org.neo4j.cypher.internal.v4_0.util.attribution.Id)
                                 (implicit out: _root_.scala.Predef.Manifest[CURSOR]): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def profileRow(id: org.neo4j.cypher.internal.v4_0.util.attribution.Id): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def profileRow(id: org.neo4j.cypher.internal.v4_0.util.attribution.Id,
                 hasRow: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def profileRows(id: org.neo4j.cypher.internal.v4_0.util.attribution.Id, nRows: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def profileRows(id: org.neo4j.cypher.internal.v4_0.util.attribution.Id,
                  nRows: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def closeEvent(id: org.neo4j.cypher.internal.v4_0.util.attribution.Id): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def dbHit(event: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def dbHits(event: org.neo4j.codegen.api.IntermediateRepresentation,
             nHits: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def onNode(event: org.neo4j.codegen.api.IntermediateRepresentation,
             node: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def indexReadSession(offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def asStorableValue(in: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def asListValue(in: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  sealed trait CursorPoolsType extends scala.AnyRef {
    def name: String
  }

  case object NodeCursorPool extends scala.AnyRef with OperatorCodeGenHelperTemplates.CursorPoolsType with scala.Product with scala.Serializable {
    override def name: String = {
      null
    }
  }

  case object NodeLabelIndexCursorPool extends scala.AnyRef with OperatorCodeGenHelperTemplates.CursorPoolsType with scala.Product with scala.Serializable {
    override def name: String = {
      null
    }
  }

  case object NodeValueIndexCursorPool extends scala.AnyRef with OperatorCodeGenHelperTemplates.CursorPoolsType with scala.Product with scala.Serializable {
    override def name: String = {
      null
    }
  }

  case object GroupCursorPool extends scala.AnyRef with OperatorCodeGenHelperTemplates.CursorPoolsType with scala.Product with scala.Serializable {
    override def name: String = {
      null
    }
  }

  case object TraversalCursorPool extends scala.AnyRef with OperatorCodeGenHelperTemplates.CursorPoolsType with scala.Product with scala.Serializable {
    override def name: String = {
      null
    }
  }

  case object RelScanCursorPool extends scala.AnyRef with OperatorCodeGenHelperTemplates.CursorPoolsType with scala.Product with scala.Serializable {
    override def name: String = {
      null
    }
  }

}
