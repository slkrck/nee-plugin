/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.pipelined

class Worker(val workerId: scala.Int,
             queryManager: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryManager,
             val sleeper: org.neo4j.cypher.internal.runtime.pipelined.Sleeper) extends java.lang.Object with java.lang.Runnable {
  def reset(): scala.Unit = {
    null
  }

  def stop(): scala.Unit = {
    null
  }

  def isSleeping: scala.Boolean = {
    false
  }

  override def run(): scala.Unit = {
    null
  }

  def workOnQuery(executingQuery: org.neo4j.cypher.internal.runtime.pipelined.execution.ExecutingQuery,
                  resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Boolean = {
    false
  }

  def assertIsNotActive(): scala.Unit = {
    null
  }

  override def toString(): String = {
    null
  }

  protected[pipelined] def executeTask(executingQuery: org.neo4j.cypher.internal.runtime.pipelined.execution.ExecutingQuery,
                                       task: org.neo4j.cypher.internal.runtime.pipelined.PipelineTask,
                                       resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): scala.Unit = {
    null
  }

  protected[pipelined] def scheduleNextTask(executingQuery: org.neo4j.cypher.internal.runtime.pipelined.execution.ExecutingQuery,
                                            resources: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources): org.neo4j.cypher.internal.runtime.pipelined.SchedulingResult[org.neo4j.cypher.internal.runtime.pipelined.Task[org.neo4j.cypher.internal.runtime.pipelined.execution.QueryResources]] = {
    null
  }
}

object Worker extends scala.AnyRef {
  val NO_WORK: scala.Seq[org.neo4j.cypher.internal.runtime.pipelined.tracing.WorkUnitEvent] = {
    null
  }

  def WORKING_THOUGH_RELEASED(worker: org.neo4j.cypher.internal.runtime.pipelined.Worker): String = {
    null
  }
}
