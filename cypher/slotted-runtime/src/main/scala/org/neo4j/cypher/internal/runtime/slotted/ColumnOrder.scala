/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.slotted

sealed trait ColumnOrder extends scala.AnyRef {
  def slot: org.neo4j.cypher.internal.physicalplanning.Slot

  def compareValues(a: org.neo4j.values.AnyValue, b: org.neo4j.values.AnyValue): scala.Int

  def compareLongs(a: scala.Long, b: scala.Long): scala.Int

  def compareNullableLongs(a: scala.Long, b: scala.Long): scala.Int
}

case class Ascending(val slot: org.neo4j.cypher.internal.physicalplanning.Slot) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.slotted.ColumnOrder with scala.Product with scala.Serializable {
  override def compareValues(a: org.neo4j.values.AnyValue, b: org.neo4j.values.AnyValue): scala.Int = {
    0
  }

  override def compareLongs(a: scala.Long, b: scala.Long): scala.Int = {
    0
  }

  override def compareNullableLongs(a: scala.Long, b: scala.Long): scala.Int = {
    0
  }
}

case class Descending(val slot: org.neo4j.cypher.internal.physicalplanning.Slot) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.slotted.ColumnOrder with scala.Product with scala.Serializable {
  override def compareValues(a: org.neo4j.values.AnyValue, b: org.neo4j.values.AnyValue): scala.Int = {
    0
  }

  override def compareLongs(a: scala.Long, b: scala.Long): scala.Int = {
    0
  }

  override def compareNullableLongs(a: scala.Long, b: scala.Long): scala.Int = {
    0
  }
}
