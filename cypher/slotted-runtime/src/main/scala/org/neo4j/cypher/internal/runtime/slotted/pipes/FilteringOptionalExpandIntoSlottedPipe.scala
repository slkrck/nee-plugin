/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.slotted.pipes

case class FilteringOptionalExpandIntoSlottedPipe(val source: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe,
                                                  val fromSlot: org.neo4j.cypher.internal.physicalplanning.Slot,
                                                  val relOffset: scala.Int,
                                                  val toSlot: org.neo4j.cypher.internal.physicalplanning.Slot,
                                                  val dir: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                                                  val lazyTypes: org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes,
                                                  val slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                                                  val predicate: org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression)
                                                 (val id: org.neo4j.cypher.internal.v4_0.util.attribution.Id) extends org.neo4j.cypher.internal.runtime.slotted.pipes.OptionalExpandIntoSlottedPipe(source, fromSlot, relOffset, toSlot, dir, lazyTypes, slots) with scala.Product with scala.Serializable {
  override def findMatchIterator(inputRow: org.neo4j.cypher.internal.runtime.ExecutionContext,
                                 state: org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState,
                                 relationships: org.eclipse.collections.api.iterator.LongIterator): scala.Iterator[org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext] = {
    null
  }
}
