/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.slotted.aggregation

class SlottedGroupingAggTable(slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                              groupingColumns: org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression,
                              aggregations: _root_.scala.Predef.Map[scala.Int, org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.AggregationExpression],
                              state: org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTable {
  override def clear(): scala.Unit = {
    null
  }

  override def processRow(row: org.neo4j.cypher.internal.runtime.ExecutionContext): scala.Unit = {
    null
  }

  override def result(): scala.Iterator[org.neo4j.cypher.internal.runtime.ExecutionContext] = {
    null
  }
}

object SlottedGroupingAggTable extends scala.AnyRef {

  case class Factory(val slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                     val groupingColumns: org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression,
                     val aggregations: _root_.scala.Predef.Map[scala.Int, org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.AggregationExpression]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTableFactory with scala.Product with scala.Serializable {
    override def table(state: org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState,
                       executionContextFactory: org.neo4j.cypher.internal.runtime.interpreted.pipes.ExecutionContextFactory): org.neo4j.cypher.internal.runtime.interpreted.pipes.AggregationPipe.AggregationTable = {
      null
    }

    override def registerOwningPipe(pipe: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe): scala.Unit = {
      null
    }
  }

}
