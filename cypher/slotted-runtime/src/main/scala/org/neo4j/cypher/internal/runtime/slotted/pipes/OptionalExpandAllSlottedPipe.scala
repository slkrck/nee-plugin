/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.slotted.pipes

abstract class OptionalExpandAllSlottedPipe(source: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe,
                                            fromSlot: org.neo4j.cypher.internal.physicalplanning.Slot,
                                            relOffset: scala.Int,
                                            toOffset: scala.Int,
                                            dir: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                                            types: org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes,
                                            slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration) extends org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeWithSource(source) with org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe {
  def filter(iterator: scala.Iterator[org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext],
             state: org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState): scala.Iterator[org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext]

  protected def internalCreateResults(input: scala.Iterator[org.neo4j.cypher.internal.runtime.ExecutionContext],
                                      state: org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState): scala.Iterator[org.neo4j.cypher.internal.runtime.ExecutionContext] = {
    null
  }
}

object OptionalExpandAllSlottedPipe extends scala.AnyRef {
  def apply(source: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe,
            fromSlot: org.neo4j.cypher.internal.physicalplanning.Slot,
            relOffset: scala.Int,
            toOffset: scala.Int,
            dir: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
            types: org.neo4j.cypher.internal.runtime.interpreted.pipes.RelationshipTypes,
            slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
            maybePredicate: scala.Option[org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression])
           (id: org.neo4j.cypher.internal.v4_0.util.attribution.Id = {
             org.neo4j.cypher.internal.v4_0.util.attribution.Id(0)
           }): org.neo4j.cypher.internal.runtime.slotted.pipes.OptionalExpandAllSlottedPipe = {
    null
  }
}
