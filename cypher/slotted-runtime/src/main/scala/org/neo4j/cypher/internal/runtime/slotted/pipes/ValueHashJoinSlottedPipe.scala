/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.slotted.pipes

case class ValueHashJoinSlottedPipe(val leftSide: org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression,
                                    val rightSide: org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression,
                                    val left: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe,
                                    val right: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe,
                                    val slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                                    val longOffset: scala.Int,
                                    val refsOffset: scala.Int,
                                    val argumentSize: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size)
                                   (val id: org.neo4j.cypher.internal.v4_0.util.attribution.Id = {
                                     org.neo4j.cypher.internal.v4_0.util.attribution.Id(0)
                                   }) extends org.neo4j.cypher.internal.runtime.slotted.pipes.AbstractHashJoinPipe[org.neo4j.values.AnyValue, org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression](left, right, slots) with scala.Product with scala.Serializable {
  override def computeKey(context: org.neo4j.cypher.internal.runtime.ExecutionContext,
                          keyColumns: org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression,
                          queryState: org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState): scala.Option[org.neo4j.values.AnyValue] = {
    null
  }

  override def copyDataFromRhs(newRow: org.neo4j.cypher.internal.runtime.slotted.SlottedExecutionContext,
                               rhs: org.neo4j.cypher.internal.runtime.ExecutionContext): scala.Unit = {
    null
  }
}
