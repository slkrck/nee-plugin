/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.slotted

case class ArrayResultExecutionContext(val resultArray: scala.Array[org.neo4j.values.AnyValue],
                                       val columnIndexMap: scala.collection.Map[String, scala.Int],
                                       val factory: org.neo4j.cypher.internal.runtime.slotted.ArrayResultExecutionContextFactory) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.ExecutionContext with org.neo4j.cypher.result.QueryResult.Record with scala.Product with scala.Serializable {
  override def release(): scala.Unit = {
    null
  }

  override def fields(): scala.Array[org.neo4j.values.AnyValue] = {
    null
  }

  override def getByName(key: String): org.neo4j.values.AnyValue = {
    null
  }

  override def numberOfColumns: scala.Int = {
    0
  }

  override def containsName(key: String): scala.Boolean = {
    false
  }

  override def copyTo(target: org.neo4j.cypher.internal.runtime.ExecutionContext, sourceLongOffset: scala.Int = {
    0
  }, sourceRefOffset: scala.Int = {
    0
  }, targetLongOffset: scala.Int = {
    0
  }, targetRefOffset: scala.Int = {
    0
  }): scala.Unit = {
    null
  }

  override def copyFrom(input: org.neo4j.cypher.internal.runtime.ExecutionContext, nLongs: scala.Int, nRefs: scala.Int): scala.Unit = {
    null
  }

  override def setLongAt(offset: scala.Int, value: scala.Long): scala.Unit = {
    null
  }

  override def getLongAt(offset: scala.Int): scala.Long = {
    0
  }

  override def setRefAt(offset: scala.Int, value: org.neo4j.values.AnyValue): scala.Unit = {
    null
  }

  override def getRefAt(offset: scala.Int): org.neo4j.values.AnyValue = {
    null
  }

  override def set(newEntries: scala.Seq[scala.Tuple2[String, org.neo4j.values.AnyValue]]): scala.Unit = {
    null
  }

  override def set(key: String, value: org.neo4j.values.AnyValue): scala.Unit = {
    null
  }

  override def set(key1: String,
                   value1: org.neo4j.values.AnyValue,
                   key2: String,
                   value2: org.neo4j.values.AnyValue): scala.Unit = {
    null
  }

  override def set(key1: String,
                   value1: org.neo4j.values.AnyValue,
                   key2: String,
                   value2: org.neo4j.values.AnyValue,
                   key3: String,
                   value3: org.neo4j.values.AnyValue): scala.Unit = {
    null
  }

  override def mergeWith(other: org.neo4j.cypher.internal.runtime.ExecutionContext, entityById: org.neo4j.cypher.internal.runtime.EntityById): scala.Unit = {
    null
  }

  override def createClone(): org.neo4j.cypher.internal.runtime.ExecutionContext = {
    null
  }

  override def copyWith(key: String, value: org.neo4j.values.AnyValue): org.neo4j.cypher.internal.runtime.ExecutionContext = {
    null
  }

  override def copyWith(key1: String,
                        value1: org.neo4j.values.AnyValue,
                        key2: String,
                        value2: org.neo4j.values.AnyValue): org.neo4j.cypher.internal.runtime.ExecutionContext = {
    null
  }

  override def copyWith(key1: String,
                        value1: org.neo4j.values.AnyValue,
                        key2: String,
                        value2: org.neo4j.values.AnyValue,
                        key3: String,
                        value3: org.neo4j.values.AnyValue): org.neo4j.cypher.internal.runtime.ExecutionContext = {
    null
  }

  override def copyWith(newEntries: scala.Seq[scala.Tuple2[String, org.neo4j.values.AnyValue]]): org.neo4j.cypher.internal.runtime.ExecutionContext = {
    null
  }

  override def boundEntities(materializeNode: scala.Function1[scala.Long, org.neo4j.values.AnyValue],
                             materializeRelationship: scala.Function1[scala.Long, org.neo4j.values.AnyValue]): _root_.scala.Predef.Map[String, org.neo4j.values.AnyValue] = {
    null
  }

  override def isNull(key: String): scala.Boolean = {
    false
  }

  override def setCachedProperty(key: org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty, value: org.neo4j.values.storable.Value): scala.Unit = {
    null
  }

  override def setCachedPropertyAt(offset: scala.Int, value: org.neo4j.values.storable.Value): scala.Unit = {
    null
  }

  override def getCachedProperty(key: org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty): org.neo4j.values.storable.Value = {
    null
  }

  override def getCachedPropertyAt(offset: scala.Int): org.neo4j.values.storable.Value = {
    null
  }

  override def invalidateCachedNodeProperties(node: scala.Long): scala.Unit = {
    null
  }

  override def invalidateCachedRelationshipProperties(rel: scala.Long): scala.Unit = {
    null
  }

  override def setLinenumber(file: String, line: scala.Long, last: scala.Boolean = {
    false
  }): scala.Unit = {
    null
  }

  override def getLinenumber: scala.Option[org.neo4j.cypher.internal.runtime.ResourceLinenumber] = {
    null
  }

  override def setLinenumber(line: scala.Option[org.neo4j.cypher.internal.runtime.ResourceLinenumber]): scala.Unit = {
    null
  }

  override def estimatedHeapUsage: scala.Long = {
    0
  }
}
