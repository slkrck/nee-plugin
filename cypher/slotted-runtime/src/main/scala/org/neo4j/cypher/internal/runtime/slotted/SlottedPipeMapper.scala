/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.slotted

class SlottedPipeMapper(fallback: org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeMapper,
                        expressionConverters: org.neo4j.cypher.internal.runtime.interpreted.commands.convert.ExpressionConverters,
                        physicalPlan: org.neo4j.cypher.internal.physicalplanning.PhysicalPlan,
                        readOnly: scala.Boolean,
                        indexRegistrator: org.neo4j.cypher.internal.runtime.QueryIndexRegistrator)
                       (implicit semanticTable: org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.interpreted.pipes.PipeMapper {
  override def onLeaf(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan): org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe = {
    null
  }

  override def onOneChildPlan(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
                              source: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe): org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe = {
    null
  }

  override def onTwoChildPlan(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
                              lhs: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe,
                              rhs: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe): org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe = {
    null
  }
}

object SlottedPipeMapper extends scala.AnyRef {
  def createProjectionsForResult(columns: scala.Seq[String],
                                 slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration): scala.Seq[scala.Tuple2[String, org.neo4j.cypher.internal.runtime.interpreted.commands.expressions.Expression]] = {
    null
  }

  def computeUnionMapping(in: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                          out: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration): org.neo4j.cypher.internal.runtime.slotted.pipes.UnionSlottedPipe.RowMapping = {
    null
  }

  def translateColumnOrder(slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                           s: org.neo4j.cypher.internal.logical.plans.ColumnOrder): org.neo4j.cypher.internal.runtime.slotted.ColumnOrder = {
    null
  }
}
