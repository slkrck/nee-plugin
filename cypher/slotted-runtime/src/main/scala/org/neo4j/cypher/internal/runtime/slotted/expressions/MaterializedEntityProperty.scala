/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.slotted.expressions


case class MaterializedEntityProperty(mapExpr: Expression, propertyKey: KeyToken) extends Expression
  with SlottedExpression {

  val property: Property = Property(mapExpr, propertyKey)


  override def apply(ctx: ExecutionContext, state: QueryState): AnyValue = {
    val n4: AnyValue = mapExpr.apply(ctx, state)
    var n3: AnyValue = null
    var n5: NodeValue = null
    var n6: RelationshipValue = null;
    if (n4.isInstanceOf[NodeValue]) {
      n5 = n4.asInstanceOf[NodeValue]
      n3 = n5.properties.get(propertyKey.name)
    } else if (n4.isInstanceOf[RelationshipValue]) {
      n6 = n4.asInstanceOf[RelationshipValue]
      n3 = n6.properties.get(propertyKey.name)
    } else {
      n3 = property.apply(ctx, state)
    }

    return n3


  }

  override def children: Seq[AstNode[_]] = {

    Seq.empty

  }
}
