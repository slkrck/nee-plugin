/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.slotted.pipes

case class SlottedGroupingExpression(val sortedGroupingExpression: scala.Array[org.neo4j.cypher.internal.runtime.slotted.pipes.SlotExpression]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression with scala.Product with scala.Serializable {
  override type KeyType = org.neo4j.values.virtual.ListValue

  override def registerOwningPipe(pipe: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe): scala.Unit = {
    null
  }

  override def computeGroupingKey(context: org.neo4j.cypher.internal.runtime.ExecutionContext,
                                  state: org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState): org.neo4j.values.virtual.ListValue = {
    null
  }

  override def computeOrderedGroupingKey(groupingKey: org.neo4j.values.virtual.ListValue): org.neo4j.values.AnyValue = {
    null
  }

  override def getGroupingKey(context: org.neo4j.cypher.internal.runtime.ExecutionContext): org.neo4j.values.virtual.ListValue = {
    null
  }

  override def isEmpty: scala.Boolean = {
    false
  }

  override def project(context: org.neo4j.cypher.internal.runtime.ExecutionContext, groupingKey: org.neo4j.values.virtual.ListValue): scala.Unit = {
    null
  }
}

case class SlottedGroupingExpression1(val groupingExpression: org.neo4j.cypher.internal.runtime.slotted.pipes.SlotExpression) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression with scala.Product with scala.Serializable {
  override type KeyType = org.neo4j.values.AnyValue

  override def registerOwningPipe(pipe: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe): scala.Unit = {
    null
  }

  override def computeGroupingKey(context: org.neo4j.cypher.internal.runtime.ExecutionContext,
                                  state: org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState): org.neo4j.values.AnyValue = {
    null
  }

  override def computeOrderedGroupingKey(groupingKey: org.neo4j.values.AnyValue): org.neo4j.values.AnyValue = {
    null
  }

  override def getGroupingKey(context: org.neo4j.cypher.internal.runtime.ExecutionContext): org.neo4j.values.AnyValue = {
    null
  }

  override def isEmpty: scala.Boolean = {
    false
  }

  override def project(context: org.neo4j.cypher.internal.runtime.ExecutionContext, groupingKey: org.neo4j.values.AnyValue): scala.Unit = {
    null
  }
}

case class SlottedGroupingExpression2(val groupingExpression1: org.neo4j.cypher.internal.runtime.slotted.pipes.SlotExpression,
                                      val groupingExpression2: org.neo4j.cypher.internal.runtime.slotted.pipes.SlotExpression) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression with scala.Product with scala.Serializable {
  override type KeyType = org.neo4j.values.virtual.ListValue

  override def registerOwningPipe(pipe: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe): scala.Unit = {
    null
  }

  override def computeGroupingKey(context: org.neo4j.cypher.internal.runtime.ExecutionContext,
                                  state: org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState): org.neo4j.values.virtual.ListValue = {
    null
  }

  override def computeOrderedGroupingKey(groupingKey: org.neo4j.values.virtual.ListValue): org.neo4j.values.AnyValue = {
    null
  }

  override def getGroupingKey(context: org.neo4j.cypher.internal.runtime.ExecutionContext): org.neo4j.values.virtual.ListValue = {
    null
  }

  override def isEmpty: scala.Boolean = {
    false
  }

  override def project(context: org.neo4j.cypher.internal.runtime.ExecutionContext, groupingKey: org.neo4j.values.virtual.ListValue): scala.Unit = {
    null
  }
}

case class SlottedGroupingExpression3(val groupingExpression1: org.neo4j.cypher.internal.runtime.slotted.pipes.SlotExpression,
                                      val groupingExpression2: org.neo4j.cypher.internal.runtime.slotted.pipes.SlotExpression,
                                      val groupingExpression3: org.neo4j.cypher.internal.runtime.slotted.pipes.SlotExpression) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.interpreted.GroupingExpression with scala.Product with scala.Serializable {
  override type KeyType = org.neo4j.values.virtual.ListValue

  override def registerOwningPipe(pipe: org.neo4j.cypher.internal.runtime.interpreted.pipes.Pipe): scala.Unit = {
    null
  }

  override def computeGroupingKey(context: org.neo4j.cypher.internal.runtime.ExecutionContext,
                                  state: org.neo4j.cypher.internal.runtime.interpreted.pipes.QueryState): org.neo4j.values.virtual.ListValue = {
    null
  }

  override def getGroupingKey(context: org.neo4j.cypher.internal.runtime.ExecutionContext): org.neo4j.values.virtual.ListValue = {
    null
  }

  override def computeOrderedGroupingKey(groupingKey: org.neo4j.values.virtual.ListValue): org.neo4j.values.AnyValue = {
    null
  }

  override def isEmpty: scala.Boolean = {
    false
  }

  override def project(context: org.neo4j.cypher.internal.runtime.ExecutionContext, groupingKey: org.neo4j.values.virtual.ListValue): scala.Unit = {
    null
  }
}

