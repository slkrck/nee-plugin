/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package org.neo4j.cypher.internal.runtime.compiled.expressions;

import org.neo4j.cypher.internal.runtime.DbAccess;
import org.neo4j.cypher.internal.runtime.ExecutionContext;
import org.neo4j.cypher.internal.runtime.KernelAPISupport;
import org.neo4j.exceptions.CypherTypeException;
import org.neo4j.exceptions.ParameterWrongTypeException;
import org.neo4j.internal.kernel.api.IndexQuery;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.BooleanValue;
import org.neo4j.values.storable.Value;
import org.neo4j.values.storable.ValueGroup;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.VirtualNodeValue;

public final class CompiledHelpers {

  private static final ValueGroup[] RANGE_SEEKABLE_VALUE_GROUPS;

  static {
    RANGE_SEEKABLE_VALUE_GROUPS = KernelAPISupport.RANGE_SEEKABLE_VALUE_GROUPS();
  }

  private CompiledHelpers() {
    throw new UnsupportedOperationException("do not instantiate");
  }

  public static Value assertBooleanOrNoValue(AnyValue value) {
    if (value != Values.NO_VALUE && !(value instanceof BooleanValue)) {
      throw new CypherTypeException(
          String.format("Don't know how to treat a predicate: %s", value.toString()), null);
    } else {
      return (Value) value;
    }
  }

  public static AnyValue nodeOrNoValue(ExecutionContext context, DbAccess dbAccess, int offset) {
    long nodeId = context.getLongAt(offset);
    return nodeId == -1L ? Values.NO_VALUE : dbAccess.nodeById(nodeId);
  }

  public static AnyValue nodeOrNoValue(DbAccess dbAccess, long nodeId) {
    return nodeId == -1L ? Values.NO_VALUE : dbAccess.nodeById(nodeId);
  }

  public static AnyValue relationshipOrNoValue(ExecutionContext context, DbAccess dbAccess,
      int offset) {
    long relationshipId = context.getLongAt(offset);
    return relationshipId == -1L ? Values.NO_VALUE : dbAccess.relationshipById(relationshipId);
  }

  public static boolean possibleRangePredicate(IndexQuery query) {
    ValueGroup valueGroup = query.valueGroup();
    ValueGroup[] n2 = RANGE_SEEKABLE_VALUE_GROUPS;
    int n3 = n2.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      ValueGroup rangeSeekableValueGroup = n2[n4];
      if (valueGroup == rangeSeekableValueGroup) {
        return true;
      }
    }

    return false;
  }

  public static long nodeFromAnyValue(AnyValue value) {
    if (value instanceof VirtualNodeValue) {
      return ((VirtualNodeValue) value).id();
    } else {
      throw new ParameterWrongTypeException(
          String.format("Expected to find a node but found %s instead", value));
    }
  }

  public static long nodeIdOrNullFromAnyValue(AnyValue value) {
    return value instanceof VirtualNodeValue ? ((VirtualNodeValue) value).id() : -1L;
  }
}
