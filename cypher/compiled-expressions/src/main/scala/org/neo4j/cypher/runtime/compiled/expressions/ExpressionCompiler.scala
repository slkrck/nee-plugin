/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.compiled.expressions

abstract class ExpressionCompiler(val slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                                  val readOnly: scala.Boolean,
                                  val codeGenerationMode: org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode,
                                  val namer: org.neo4j.cypher.internal.runtime.compiled.expressions.VariableNamer = {
                                    null
                                  }) extends scala.AnyRef {
  def compileExpression(e: org.neo4j.cypher.internal.v4_0.expressions.Expression): scala.Option[org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledExpression] = {
    null
  }

  def compileProjection(projections: _root_.scala.Predef.Map[String, org.neo4j.cypher.internal.v4_0.expressions.Expression]): scala.Option[org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledProjection] = {
    null
  }

  def compileGrouping(orderedGroupings: scala.Function1[org.neo4j.cypher.internal.physicalplanning.SlotConfiguration, scala.Seq[scala.Tuple3[String, org.neo4j.cypher.internal.v4_0.expressions.Expression, scala.Boolean]]]): scala.Option[org.neo4j.cypher.internal.runtime.compiled.expressions.CompiledGroupingExpression] = {
    null
  }

  def intermediateCompileGroupingKey(orderedGroupings: scala.Seq[org.neo4j.cypher.internal.v4_0.expressions.Expression]): scala.Option[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression] = {
    null
  }

  def intermediateCompileExpression(expression: org.neo4j.cypher.internal.v4_0.expressions.Expression): scala.Option[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression] = {
    null
  }

  def compileFunction(c: org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation): scala.Option[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression] = {
    null
  }

  def intermediateCompileProjection(projections: _root_.scala.Predef.Map[String, org.neo4j.cypher.internal.v4_0.expressions.Expression]): scala.Option[org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression] = {
    null
  }

  def getArgumentAt(offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  final def getLongFromExecutionContext(offset: scala.Int, context: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  final def getRefFromExecutionContext(offset: scala.Int, context: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  final def getCachedPropertyFromExecutionContext(offset: scala.Int, context: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  final def setRefInExecutionContext(offset: scala.Int,
                                     value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  final def setLongInExecutionContext(offset: scala.Int,
                                      value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected def getLongAt(offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation

  protected def getRefAt(offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation

  protected def setRefAt(offset: scala.Int, value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation

  protected def setLongAt(offset: scala.Int, value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation

  protected def setCachedPropertyAt(offset: scala.Int,
                                    value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation

  protected def getCachedPropertyAt(property: org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedProperty,
                                    getFromStore: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation

  protected def isLabelSetOnNode(labelToken: org.neo4j.codegen.api.IntermediateRepresentation,
                                 offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation

  protected def getNodeProperty(propertyToken: org.neo4j.codegen.api.IntermediateRepresentation,
                                offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation

  protected def getRelationshipProperty(propertyToken: org.neo4j.codegen.api.IntermediateRepresentation,
                                        offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation

  protected def getProperty(key: String,
                            container: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation

  protected final def setCachedPropertyInExecutionContext(offset: scala.Int,
                                                          value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
}

object ExpressionCompiler extends scala.AnyRef {
  val DB_ACCESS: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val NODE_CURSOR: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val vNODE_CURSOR: org.neo4j.codegen.api.LocalVariable = {
    null
  }
  val RELATIONSHIP_CURSOR: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val vRELATIONSHIP_CURSOR: org.neo4j.codegen.api.LocalVariable = {
    null
  }
  val PROPERTY_CURSOR: org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
  val vPROPERTY_CURSOR: org.neo4j.codegen.api.LocalVariable = {
    null
  }
  val NODE_PROPERTY: org.neo4j.codegen.api.Method = {
    null
  }
  val RELATIONSHIP_PROPERTY: org.neo4j.codegen.api.Method = {
    null
  }

  def defaultGenerator(slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                       readOnly: scala.Boolean,
                       codeGenerationMode: org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode,
                       namer: org.neo4j.cypher.internal.runtime.compiled.expressions.VariableNamer = {
                         null
                       }): org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler = {
    null
  }

  def noValueOr(expressions: org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression*)
               (onNotNull: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def nullCheck(expressions: org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression*)
               (onNull: org.neo4j.codegen.api.IntermediateRepresentation = {
                 null
               })
               (onNotNull: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  def nullCheckIfRequired(expression: org.neo4j.cypher.internal.runtime.compiled.expressions.IntermediateExpression,
                          onNull: org.neo4j.codegen.api.IntermediateRepresentation = {
                            null
                          }): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
}

class DefaultExpressionCompiler(slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                                readOnly: scala.Boolean,
                                codeGenerationMode: org.neo4j.codegen.api.CodeGeneration.CodeGenerationMode,
                                namer: org.neo4j.cypher.internal.runtime.compiled.expressions.VariableNamer) extends org.neo4j.cypher.internal.runtime.compiled.expressions.ExpressionCompiler(slots, readOnly, codeGenerationMode, namer) {
  protected override def getLongAt(offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def getRefAt(offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def setRefAt(offset: scala.Int,
                                  value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def setLongAt(offset: scala.Int,
                                   value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def setCachedPropertyAt(offset: scala.Int,
                                             value: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def getCachedPropertyAt(property: org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedProperty,
                                             getFromStore: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def isLabelSetOnNode(labelToken: org.neo4j.codegen.api.IntermediateRepresentation,
                                          offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def getNodeProperty(propertyToken: org.neo4j.codegen.api.IntermediateRepresentation,
                                         offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def getRelationshipProperty(propertyToken: org.neo4j.codegen.api.IntermediateRepresentation,
                                                 offset: scala.Int): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }

  protected override def getProperty(key: String,
                                     container: org.neo4j.codegen.api.IntermediateRepresentation): org.neo4j.codegen.api.IntermediateRepresentation = {
    null
  }
}