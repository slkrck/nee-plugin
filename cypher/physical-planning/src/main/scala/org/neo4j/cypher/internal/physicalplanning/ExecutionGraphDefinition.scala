/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.physicalplanning

case class ExecutionGraphDefinition(val physicalPlan: org.neo4j.cypher.internal.physicalplanning.PhysicalPlan,
                                    val buffers: scala.IndexedSeq[org.neo4j.cypher.internal.physicalplanning.BufferDefinition],
                                    val argumentStateMaps: scala.IndexedSeq[org.neo4j.cypher.internal.physicalplanning.ArgumentStateDefinition],
                                    val pipelines: scala.IndexedSeq[org.neo4j.cypher.internal.physicalplanning.PipelineDefinition],
                                    val applyRhsPlans: _root_.scala.Predef.Map[scala.Int, scala.Int]) extends scala.AnyRef with scala.Product with scala.Serializable {
  def findArgumentStateMapForPlan(planId: org.neo4j.cypher.internal.v4_0.util.attribution.Id): org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId = {
    null.asInstanceOf[ArgumentStateMapId]
  }
}

object ExecutionGraphDefinition extends scala.AnyRef with scala.Serializable {
  val NO_ARGUMENT_STATE_MAPS: scala.Array[org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId] = {
    null
  }
  val NO_BUFFERS: scala.Array[org.neo4j.cypher.internal.physicalplanning.BufferId] = {
    null
  }
}
