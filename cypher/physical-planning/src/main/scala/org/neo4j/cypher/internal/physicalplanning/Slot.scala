/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.physicalplanning

sealed trait Slot extends scala.AnyRef {
  def offset: scala.Int

  def nullable: scala.Boolean

  def typ: org.neo4j.cypher.internal.v4_0.util.symbols.CypherType

  def isTypeCompatibleWith(other: org.neo4j.cypher.internal.physicalplanning.Slot): scala.Boolean

  def isLongSlot: scala.Boolean

  def asNullable: org.neo4j.cypher.internal.physicalplanning.Slot
}

object SlotAllocation extends scala.AnyRef {

  val INITIAL_SLOT_CONFIGURATION: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration = {
    null
  }

  def allocateSlots(lp: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
                    semanticTable: org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable,
                    breakingPolicy: org.neo4j.cypher.internal.physicalplanning.PipelineBreakingPolicy,
                    availableExpressionVariables: org.neo4j.cypher.internal.runtime.expressionVariableAllocation.AvailableExpressionVariables,
                    allocateArgumentSlots: scala.Boolean = {
                      false
                    }): SlotAllocation.SlotMetaData = {
    null
  }

  private[physicalplanning] def NO_ARGUMENT(allocateArgumentSlots: scala.Boolean): SlotAllocation.SlotsAndArgument = {
    null
  }

  case class SlotsAndArgument(val slotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                              val argumentSize: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size,
                              val argumentPlan: org.neo4j.cypher.internal.v4_0.util.attribution.Id) extends scala.AnyRef with scala.Product with scala.Serializable {
  }

  case class SlotMetaData(val slotConfigurations: org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes.SlotConfigurations,
                          val argumentSizes: org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes.ArgumentSizes,
                          val applyPlans: org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes.ApplyPlans,
                          val nestedPlanArgumentConfigurations: org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes.NestedPlanArgumentConfigurations) extends scala.AnyRef with scala.Product with scala.Serializable {
  }

}

class SlotAllocationFailed(str: String) extends org.neo4j.exceptions.InternalException(str) {
}

sealed trait SlotWithAliases extends scala.AnyRef {
  def slot: org.neo4j.cypher.internal.physicalplanning.Slot

  def aliases: _root_.scala.Predef.Set[String]

  protected def makeString: String = {
    null
  }
}

case class LongSlot(val offset: scala.Int,
                    val nullable: scala.Boolean,
                    val typ: org.neo4j.cypher.internal.v4_0.util.symbols.CypherType) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.Slot with scala.Product with scala.Serializable {
  override def isTypeCompatibleWith(other: org.neo4j.cypher.internal.physicalplanning.Slot): scala.Boolean = {
    false
  }

  override def isLongSlot: scala.Boolean = {
    false
  }

  override def asNullable: org.neo4j.cypher.internal.physicalplanning.LongSlot = {
    null
  }
}

case class LongSlotWithAliases(val slot: org.neo4j.cypher.internal.physicalplanning.LongSlot,
                               val aliases: _root_.scala.Predef.Set[String]) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.SlotWithAliases with scala.Product with scala.Serializable {
  override def toString(): String = {
    null
  }
}

case class RefSlot(val offset: scala.Int,
                   val nullable: scala.Boolean,
                   val typ: org.neo4j.cypher.internal.v4_0.util.symbols.CypherType) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.Slot with scala.Product with scala.Serializable {
  override def isTypeCompatibleWith(other: org.neo4j.cypher.internal.physicalplanning.Slot): scala.Boolean = {
    false
  }

  override def isLongSlot: scala.Boolean = {
    false
  }

  override def asNullable: org.neo4j.cypher.internal.physicalplanning.RefSlot = {
    null
  }
}

case class RefSlotWithAliases(val slot: org.neo4j.cypher.internal.physicalplanning.RefSlot,
                              val aliases: _root_.scala.Predef.Set[String]) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.SlotWithAliases with scala.Product with scala.Serializable {
  override def toString(): String = {
    null
  }
}