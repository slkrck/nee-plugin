/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.physicalplanning.ast

trait SlottedCachedProperty extends org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty with org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty with org.neo4j.cypher.internal.runtime.ast.RuntimeExpression {
  def offset: scala.Int

  def offsetIsForLongSlot: scala.Boolean

  def cachedPropertyOffset: scala.Int

  override def originalEntityName: String = {
    null
  }
}

case class SlottedCachedPropertyWithoutPropertyToken(val entityName: String,
                                                     val propertyKey: org.neo4j.cypher.internal.v4_0.expressions.PropertyKeyName,
                                                     val offset: scala.Int,
                                                     val offsetIsForLongSlot: scala.Boolean,
                                                     val propKey: String,
                                                     val cachedPropertyOffset: scala.Int,
                                                     val entityType: org.neo4j.cypher.internal.v4_0.expressions.EntityType,
                                                     val nullable: scala.Boolean) extends org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty with org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedProperty with scala.Product with scala.Serializable {
}

case class SlottedCachedPropertyWithPropertyToken(val entityName: String,
                                                  val propertyKey: org.neo4j.cypher.internal.v4_0.expressions.PropertyKeyName,
                                                  val offset: scala.Int,
                                                  val offsetIsForLongSlot: scala.Boolean,
                                                  val propToken: scala.Int,
                                                  val cachedPropertyOffset: scala.Int,
                                                  val entityType: org.neo4j.cypher.internal.v4_0.expressions.EntityType,
                                                  val nullable: scala.Boolean) extends org.neo4j.cypher.internal.v4_0.expressions.LogicalProperty with org.neo4j.cypher.internal.physicalplanning.ast.SlottedCachedProperty with scala.Product with scala.Serializable {
}
