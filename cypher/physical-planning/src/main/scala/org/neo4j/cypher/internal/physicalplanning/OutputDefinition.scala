/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.physicalplanning

sealed trait OutputDefinition extends scala.AnyRef {
}

case class MorselArgumentStateBufferOutput(val id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                           val argumentSlotOffset: scala.Int,
                                           val nextPipelineHeadPlanId: org.neo4j.cypher.internal.v4_0.util.attribution.Id) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.OutputDefinition with scala.Product with scala.Serializable {
}

case class MorselBufferOutput(val id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                              val nextPipelineHeadPlanId: org.neo4j.cypher.internal.v4_0.util.attribution.Id) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.OutputDefinition with scala.Product with scala.Serializable {
}

case class ProduceResultOutput(val plan: org.neo4j.cypher.internal.logical.plans.ProduceResult) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.OutputDefinition with scala.Product with scala.Serializable {
}

case class ReduceOutput(val bufferId: org.neo4j.cypher.internal.physicalplanning.BufferId,
                        val plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.OutputDefinition with scala.Product with scala.Serializable {
}

case object NoOutput extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.OutputDefinition with scala.Product with scala.Serializable {
}

