/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.physicalplanning

sealed trait OperatorFusionPolicy extends scala.AnyRef {
  def fusionEnabled: scala.Boolean

  def fusionOverPipelineEnabled: scala.Boolean

  def canFuse(lp: org.neo4j.cypher.internal.logical.plans.LogicalPlan): scala.Boolean

  def canFuseOverPipeline(lp: org.neo4j.cypher.internal.logical.plans.LogicalPlan): scala.Boolean
}

object OperatorFusionPolicy extends scala.AnyRef {
  def apply(fusionEnabled: scala.Boolean, fusionOverPipelinesEnabled: scala.Boolean): org.neo4j.cypher.internal.physicalplanning.OperatorFusionPolicy = {
    null
  }

  case object OPERATOR_FUSION_DISABLED extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.OperatorFusionPolicy with scala.Product with scala.Serializable {
    override def canFuse(lp: org.neo4j.cypher.internal.logical.plans.LogicalPlan): scala.Boolean = {
      false
    }

    override def canFuseOverPipeline(lp: org.neo4j.cypher.internal.logical.plans.LogicalPlan): scala.Boolean = {
      false
    }

    override def fusionEnabled: scala.Boolean = {
      false
    }

    override def fusionOverPipelineEnabled: scala.Boolean = {
      false
    }
  }

  case object OPERATOR_FUSION_OVER_PIPELINES extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.OperatorFusionPolicy with scala.Product with scala.Serializable {
    override def fusionEnabled: scala.Boolean = {
      false
    }

    override def fusionOverPipelineEnabled: scala.Boolean = {
      false
    }

    override def canFuse(lp: org.neo4j.cypher.internal.logical.plans.LogicalPlan): scala.Boolean = {
      false
    }

    override def canFuseOverPipeline(lp: org.neo4j.cypher.internal.logical.plans.LogicalPlan): scala.Boolean = {
      false
    }
  }

  case object OPERATOR_FUSION_WITHIN_PIPELINE extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.OperatorFusionPolicy with scala.Product with scala.Serializable {
    override def fusionEnabled: scala.Boolean = {
      false
    }

    override def fusionOverPipelineEnabled: scala.Boolean = {
      false
    }

    override def canFuse(lp: org.neo4j.cypher.internal.logical.plans.LogicalPlan): scala.Boolean = {
      false
    }

    override def canFuseOverPipeline(lp: org.neo4j.cypher.internal.logical.plans.LogicalPlan): scala.Boolean = {
      false
    }
  }

}
