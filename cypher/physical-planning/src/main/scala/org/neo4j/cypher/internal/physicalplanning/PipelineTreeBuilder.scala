/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.physicalplanning

class PipelineTreeBuilder(breakingPolicy: org.neo4j.cypher.internal.physicalplanning.PipelineBreakingPolicy,
                          operatorFusionPolicy: org.neo4j.cypher.internal.physicalplanning.OperatorFusionPolicy,
                          stateDefinition: org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.ExecutionStateDefinitionBuild,
                          slotConfigurations: org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes.SlotConfigurations,
                          argumentSizes: org.neo4j.cypher.internal.physicalplanning.PhysicalPlanningAttributes.ArgumentSizes) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.TreeBuilder[org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.PipelineDefinitionBuild, org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.ApplyBufferDefinitionBuild] {
  private[physicalplanning] val pipelines: scala.collection.mutable.ArrayBuffer[org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.PipelineDefinitionBuild] = {
    null
  }
  private[physicalplanning] val applyRhsPlans: scala.collection.mutable.HashMap[scala.Int, scala.Int] = {
    null
  }

  protected override def validatePlan(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan): scala.Unit = {
    null
  }

  protected override def initialArgument(leftLeaf: org.neo4j.cypher.internal.logical.plans.LogicalPlan): org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.ApplyBufferDefinitionBuild = {
    null
  }

  protected override def onLeaf(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
                                argument: org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.ApplyBufferDefinitionBuild): org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.PipelineDefinitionBuild = {
    null
  }

  protected override def onOneChildPlan(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
                                        source: org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.PipelineDefinitionBuild,
                                        argument: org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.ApplyBufferDefinitionBuild): org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.PipelineDefinitionBuild = {
    null
  }

  protected override def onTwoChildPlanComingFromLeft(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
                                                      lhs: org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.PipelineDefinitionBuild,
                                                      argument: org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.ApplyBufferDefinitionBuild): org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.ApplyBufferDefinitionBuild = {
    null
  }

  protected override def onTwoChildPlanComingFromRight(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
                                                       lhs: org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.PipelineDefinitionBuild,
                                                       rhs: org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.PipelineDefinitionBuild,
                                                       argument: org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.ApplyBufferDefinitionBuild): org.neo4j.cypher.internal.physicalplanning.PipelineTreeBuilder.PipelineDefinitionBuild = {
    null
  }
}

object PipelineTreeBuilder extends scala.AnyRef {

  val NO_PIPELINE_BUILD: PipelineTreeBuilder.PipelineDefinitionBuild = {
    null
  }

  sealed trait DownstreamStateOperator extends scala.AnyRef {
  }

  abstract class BufferDefinitionBuild(val id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                       val bufferConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration) extends scala.AnyRef {
    val downstreamStates: scala.collection.mutable.ArrayBuffer[PipelineTreeBuilder.DownstreamStateOperator] = {
      null
    }
  }

  class PipelineDefinitionBuild(val id: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                val headPlan: org.neo4j.cypher.internal.logical.plans.LogicalPlan) extends scala.AnyRef {
    val fusedPlans: scala.collection.mutable.ArrayBuffer[org.neo4j.cypher.internal.logical.plans.LogicalPlan] = {
      null
    }
    val middlePlans: scala.collection.mutable.ArrayBuffer[org.neo4j.cypher.internal.logical.plans.LogicalPlan] = {
      null
    }
    var lhs: org.neo4j.cypher.internal.physicalplanning.PipelineId = {
      null.asInstanceOf[PipelineId]
    }
    var rhs: org.neo4j.cypher.internal.physicalplanning.PipelineId = {
      null.asInstanceOf[PipelineId]
    }
    var inputBuffer: PipelineTreeBuilder.BufferDefinitionBuild = {
      null
    }
    var outputDefinition: org.neo4j.cypher.internal.physicalplanning.OutputDefinition = {
      null
    }
    var serial: scala.Boolean = {
      false
    }
  }

  case class DownstreamReduce(val id: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId) extends scala.AnyRef with PipelineTreeBuilder.DownstreamStateOperator with scala.Product with scala.Serializable {
  }

  case class DownstreamState(val id: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId) extends scala.AnyRef with PipelineTreeBuilder.DownstreamStateOperator with scala.Product with scala.Serializable {
  }

  case class DownstreamWorkCanceller(val id: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId) extends scala.AnyRef with PipelineTreeBuilder.DownstreamStateOperator with scala.Product with scala.Serializable {
  }

  case class ArgumentStateDefinitionBuild(val id: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                          val planId: org.neo4j.cypher.internal.v4_0.util.attribution.Id,
                                          val argumentSlotOffset: scala.Int,
                                          val counts: scala.Boolean) extends scala.AnyRef with scala.Product with scala.Serializable {
  }

  class MorselBufferDefinitionBuild(id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                    val producingPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                    bufferConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration) extends PipelineTreeBuilder.BufferDefinitionBuild(id, bufferConfiguration) {
  }

  class OptionalMorselBufferDefinitionBuild(id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                            val producingPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                            val argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                            val argumentSlotOffset: scala.Int,
                                            bufferConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration) extends PipelineTreeBuilder.BufferDefinitionBuild(id, bufferConfiguration) {
  }

  class DelegateBufferDefinitionBuild(id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                      val applyBuffer: PipelineTreeBuilder.ApplyBufferDefinitionBuild,
                                      bufferConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration) extends PipelineTreeBuilder.BufferDefinitionBuild(id, bufferConfiguration) {
  }

  class ApplyBufferDefinitionBuild(id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                   producingPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                   val argumentSlotOffset: scala.Int,
                                   bufferSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration) extends PipelineTreeBuilder.MorselBufferDefinitionBuild(id, producingPipelineId, bufferSlotConfiguration) {
    val reducersOnRHS: scala.collection.mutable.ArrayBuffer[PipelineTreeBuilder.ArgumentStateDefinitionBuild] = {
      null
    }
    val delegates: scala.collection.mutable.ArrayBuffer[org.neo4j.cypher.internal.physicalplanning.BufferId] = {
      null
    }
  }

  class AttachBufferDefinitionBuild(id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                    inputSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                                    val outputSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                                    val argumentSlotOffset: scala.Int,
                                    val argumentSize: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size) extends PipelineTreeBuilder.BufferDefinitionBuild(id, inputSlotConfiguration) {
    var applyBuffer: PipelineTreeBuilder.ApplyBufferDefinitionBuild = {
      null
    }
  }

  class ArgumentStateBufferDefinitionBuild(id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                           producingPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                           val argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                           bufferSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration) extends PipelineTreeBuilder.MorselBufferDefinitionBuild(id, producingPipelineId, bufferSlotConfiguration) {
  }

  class LHSAccumulatingRHSStreamingBufferDefinitionBuild(id: org.neo4j.cypher.internal.physicalplanning.BufferId,
                                                         val lhsPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                                         val rhsPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                                         val lhsArgumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                                         val rhsArgumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                                         bufferSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration) extends PipelineTreeBuilder.BufferDefinitionBuild(id, bufferSlotConfiguration) {
  }

  class ExecutionStateDefinitionBuild(val physicalPlan: org.neo4j.cypher.internal.physicalplanning.PhysicalPlan) extends scala.AnyRef {
    val buffers: scala.collection.mutable.ArrayBuffer[PipelineTreeBuilder.BufferDefinitionBuild] = {
      null
    }
    val argumentStateMaps: scala.collection.mutable.ArrayBuffer[PipelineTreeBuilder.ArgumentStateDefinitionBuild] = {
      null
    }
    var initBuffer: PipelineTreeBuilder.ApplyBufferDefinitionBuild = {
      null
    }

    def newArgumentStateMap(planId: org.neo4j.cypher.internal.v4_0.util.attribution.Id,
                            argumentSlotOffset: scala.Int,
                            counts: scala.Boolean): PipelineTreeBuilder.ArgumentStateDefinitionBuild = {
      null
    }

    def newBuffer(producingPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                  bufferSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration): PipelineTreeBuilder.MorselBufferDefinitionBuild = {
      null
    }

    def newOptionalBuffer(producingPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                          argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                          argumentSlotOffset: scala.Int,
                          bufferSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration): PipelineTreeBuilder.OptionalMorselBufferDefinitionBuild = {
      null
    }

    def newDelegateBuffer(applyBufferDefinition: PipelineTreeBuilder.ApplyBufferDefinitionBuild,
                          bufferSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration): PipelineTreeBuilder.DelegateBufferDefinitionBuild = {
      null
    }

    def newApplyBuffer(producingPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                       argumentSlotOffset: scala.Int,
                       bufferSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration): PipelineTreeBuilder.ApplyBufferDefinitionBuild = {
      null
    }

    def newAttachBuffer(producingPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                        inputSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                        postAttachSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                        outerArgumentSlotOffset: scala.Int,
                        outerArgumentSize: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size): PipelineTreeBuilder.AttachBufferDefinitionBuild = {
      null
    }

    def newArgumentStateBuffer(producingPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                               argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                               bufferSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration): PipelineTreeBuilder.ArgumentStateBufferDefinitionBuild = {
      null
    }

    def newLhsAccumulatingRhsStreamingBuffer(lhsProducingPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                             rhsProducingPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                             lhsargumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                             rhsargumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                             bufferSlotConfiguration: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration): PipelineTreeBuilder.LHSAccumulatingRHSStreamingBufferDefinitionBuild = {
      null
    }
  }

}
