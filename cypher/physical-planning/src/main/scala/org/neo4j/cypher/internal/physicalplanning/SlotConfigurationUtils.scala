/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.physicalplanning

object SlotConfigurationUtils extends scala.AnyRef {
  val PRIMITIVE_NULL: scala.Long = {
    0
  }
  val NO_ENTITY_FUNCTION: java.util.function.ToLongFunction[org.neo4j.cypher.internal.runtime.ExecutionContext] = {
    null
  }

  def makeGetValueFromSlotFunctionFor(slot: org.neo4j.cypher.internal.physicalplanning.Slot): scala.Function1[org.neo4j.cypher.internal.runtime.ExecutionContext, org.neo4j.values.AnyValue] = {
    null
  }

  def makeGetPrimitiveFromSlotFunctionFor(slot: org.neo4j.cypher.internal.physicalplanning.Slot,
                                          returnType: org.neo4j.cypher.internal.v4_0.util.symbols.CypherType,
                                          throwOfTypeError: scala.Boolean = {
                                            false
                                          }): java.util.function.ToLongFunction[org.neo4j.cypher.internal.runtime.ExecutionContext] = {
    null
  }

  def makeGetPrimitiveNodeFromSlotFunctionFor(slot: org.neo4j.cypher.internal.physicalplanning.Slot, throwOnTypeError: scala.Boolean = {
    false
  }): java.util.function.ToLongFunction[org.neo4j.cypher.internal.runtime.ExecutionContext] = {
    null
  }

  def makeGetPrimitiveRelationshipFromSlotFunctionFor(slot: org.neo4j.cypher.internal.physicalplanning.Slot, throwOfTypeError: scala.Boolean = {
    false
  }): java.util.function.ToLongFunction[org.neo4j.cypher.internal.runtime.ExecutionContext] = {
    null
  }

  def makeSetValueInSlotFunctionFor(slot: org.neo4j.cypher.internal.physicalplanning.Slot): scala.Function2[org.neo4j.cypher.internal.runtime.ExecutionContext, org.neo4j.values.AnyValue, scala.Unit] = {
    null
  }

  def makeSetPrimitiveInSlotFunctionFor(slot: org.neo4j.cypher.internal.physicalplanning.Slot,
                                        valueType: org.neo4j.cypher.internal.v4_0.util.symbols.CypherType): scala.Function3[org.neo4j.cypher.internal.runtime.ExecutionContext, scala.Long, org.neo4j.cypher.internal.runtime.EntityById, scala.Unit] = {
    null
  }

  def makeSetPrimitiveNodeInSlotFunctionFor(slot: org.neo4j.cypher.internal.physicalplanning.Slot): scala.Function3[org.neo4j.cypher.internal.runtime.ExecutionContext, scala.Long, org.neo4j.cypher.internal.runtime.EntityById, scala.Unit] = {
    null
  }

  def makeSetPrimitiveRelationshipInSlotFunctionFor(slot: org.neo4j.cypher.internal.physicalplanning.Slot): scala.Function3[org.neo4j.cypher.internal.runtime.ExecutionContext, scala.Long, org.neo4j.cypher.internal.runtime.EntityById, scala.Unit] = {
    null
  }

  def generateSlotAccessorFunctions(slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration): scala.Unit = {
    null
  }
}
