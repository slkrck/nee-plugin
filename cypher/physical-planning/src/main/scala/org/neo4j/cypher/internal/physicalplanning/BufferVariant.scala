/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.physicalplanning

sealed trait BufferVariant extends scala.AnyRef {
}

case class ApplyBufferVariant(val argumentSlotOffset: scala.Int,
                              val reducersOnRHSReversed: scala.Array[org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId],
                              val delegates: scala.Array[org.neo4j.cypher.internal.physicalplanning.BufferId]) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.BufferVariant with scala.Product with scala.Serializable {
  override def canEqual(that: scala.Any): scala.Boolean = {
    false
  }

  override def equals(obj: scala.Any): scala.Boolean = {
    false
  }

  override def hashCode(): scala.Int = {
    0
  }

  override def toString(): String = {
    null
  }
}

case class ArgumentStateBufferVariant(val argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.BufferVariant with scala.Product with scala.Serializable {
}

case class AttachBufferVariant(val applyBuffer: org.neo4j.cypher.internal.physicalplanning.BufferDefinition,
                               val outputSlots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration,
                               val argumentSlotOffset: scala.Int,
                               val argumentSize: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.BufferVariant with scala.Product with scala.Serializable {
}

case class LHSAccumulatingRHSStreamingBufferVariant(val lhsPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                                    val rhsPipelineId: org.neo4j.cypher.internal.physicalplanning.PipelineId,
                                                    val lhsArgumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId,
                                                    val rhsArgumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.BufferVariant with scala.Product with scala.Serializable {
}

case class OptionalBufferVariant(val argumentStateMapId: org.neo4j.cypher.internal.physicalplanning.ArgumentStateMapId) extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.BufferVariant with scala.Product with scala.Serializable {
}

case object RegularBufferVariant extends scala.AnyRef with org.neo4j.cypher.internal.physicalplanning.BufferVariant with scala.Product with scala.Serializable {
}
