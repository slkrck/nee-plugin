/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.physicalplanning

class SlotConfiguration(private val slots: scala.collection.mutable.Map[String, org.neo4j.cypher.internal.physicalplanning.Slot],
                        private val cachedProperties: scala.collection.mutable.Map[org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty, org.neo4j.cypher.internal.physicalplanning.RefSlot],
                        private val applyPlans: scala.collection.mutable.Map[org.neo4j.cypher.internal.v4_0.util.attribution.Id, scala.Int],
                        var numberOfLongs: scala.Int,
                        var numberOfReferences: scala.Int) extends scala.AnyRef {
  def size(): org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size = {
    null
  }

  def addAlias(newKey: String, existingKey: String): org.neo4j.cypher.internal.physicalplanning.SlotConfiguration = {
    null
  }

  def isAlias(key: String): scala.Boolean = {
    false
  }

  def apply(key: String): org.neo4j.cypher.internal.physicalplanning.Slot = {
    null
  }

  def nameOfLongSlot(offset: scala.Int): scala.Option[String] = {
    null
  }

  def filterSlots[U](onVariable: scala.Function1[scala.Tuple2[String, org.neo4j.cypher.internal.physicalplanning.Slot], scala.Boolean],
                     onCachedProperty: scala.Function1[scala.Tuple2[org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty, org.neo4j.cypher.internal.physicalplanning.RefSlot], scala.Boolean]): scala.Iterable[org.neo4j.cypher.internal.physicalplanning.Slot] = {
    null
  }

  def get(key: String): scala.Option[org.neo4j.cypher.internal.physicalplanning.Slot] = {
    null
  }

  def add(key: String, slot: org.neo4j.cypher.internal.physicalplanning.Slot): scala.Unit = {
    null
  }

  def copy(): org.neo4j.cypher.internal.physicalplanning.SlotConfiguration = {
    null
  }

  def emptyUnderSameApply(): org.neo4j.cypher.internal.physicalplanning.SlotConfiguration = {
    null
  }

  def newLong(key: String,
              nullable: scala.Boolean,
              typ: org.neo4j.cypher.internal.v4_0.util.symbols.CypherType): org.neo4j.cypher.internal.physicalplanning.SlotConfiguration = {
    null
  }

  def newArgument(applyPlanId: org.neo4j.cypher.internal.v4_0.util.attribution.Id): org.neo4j.cypher.internal.physicalplanning.SlotConfiguration = {
    null
  }

  def newReference(key: String,
                   nullable: scala.Boolean,
                   typ: org.neo4j.cypher.internal.v4_0.util.symbols.CypherType): org.neo4j.cypher.internal.physicalplanning.SlotConfiguration = {
    null
  }

  def newCachedProperty(key: org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty, shouldDuplicate: scala.Boolean = {
    false
  }): org.neo4j.cypher.internal.physicalplanning.SlotConfiguration = {
    null
  }

  def getReferenceOffsetFor(name: String): scala.Int = {
    0
  }

  def getLongOffsetFor(name: String): scala.Int = {
    0
  }

  def getLongSlotFor(name: String): org.neo4j.cypher.internal.physicalplanning.Slot = {
    null
  }

  def getArgumentLongOffsetFor(applyPlanId: org.neo4j.cypher.internal.v4_0.util.attribution.Id): scala.Int = {
    0
  }

  def getCachedPropertyOffsetFor(key: org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty): scala.Int = {
    0
  }

  def updateAccessorFunctions(key: String,
                              getter: scala.Function1[org.neo4j.cypher.internal.runtime.ExecutionContext, org.neo4j.values.AnyValue],
                              setter: scala.Function2[org.neo4j.cypher.internal.runtime.ExecutionContext, org.neo4j.values.AnyValue, scala.Unit],
                              primitiveNodeSetter: scala.Option[scala.Function3[org.neo4j.cypher.internal.runtime.ExecutionContext, scala.Long, org.neo4j.cypher.internal.runtime.EntityById, scala.Unit]],
                              primitiveRelationshipSetter: scala.Option[scala.Function3[org.neo4j.cypher.internal.runtime.ExecutionContext, scala.Long, org.neo4j.cypher.internal.runtime.EntityById, scala.Unit]]): scala.Unit = {
    null
  }

  def getter(key: String): scala.Function1[org.neo4j.cypher.internal.runtime.ExecutionContext, org.neo4j.values.AnyValue] = {
    null
  }

  def setter(key: String): scala.Function2[org.neo4j.cypher.internal.runtime.ExecutionContext, org.neo4j.values.AnyValue, scala.Unit] = {
    null
  }

  def maybeGetter(key: String): scala.Option[scala.Function1[org.neo4j.cypher.internal.runtime.ExecutionContext, org.neo4j.values.AnyValue]] = {
    null
  }

  def maybeSetter(key: String): scala.Option[scala.Function2[org.neo4j.cypher.internal.runtime.ExecutionContext, org.neo4j.values.AnyValue, scala.Unit]] = {
    null
  }

  def maybePrimitiveNodeSetter(key: String): scala.Option[scala.Function3[org.neo4j.cypher.internal.runtime.ExecutionContext, scala.Long, org.neo4j.cypher.internal.runtime.EntityById, scala.Unit]] = {
    null
  }

  def maybePrimitiveRelationshipSetter(key: String): scala.Option[scala.Function3[org.neo4j.cypher.internal.runtime.ExecutionContext, scala.Long, org.neo4j.cypher.internal.runtime.EntityById, scala.Unit]] = {
    null
  }

  def foreachSlot[U](onVariable: scala.Function1[scala.Tuple2[String, org.neo4j.cypher.internal.physicalplanning.Slot], U],
                     onCachedProperty: scala.Function1[scala.Tuple2[org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty, org.neo4j.cypher.internal.physicalplanning.RefSlot], scala.Unit]): scala.Unit = {
    null
  }

  def foreachSlotOrdered(onVariable: scala.Function2[String, org.neo4j.cypher.internal.physicalplanning.Slot, scala.Unit],
                         onCachedProperty: scala.Function1[org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty, scala.Unit],
                         onApplyPlan: scala.Function1[org.neo4j.cypher.internal.v4_0.util.attribution.Id, scala.Unit] = {
                           null
                         },
                         skipFirst: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration.Size = {
                           null
                         }): scala.Unit = {
    null
  }

  def foreachCachedSlot[U](onCachedProperty: scala.Function1[scala.Tuple2[org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty, org.neo4j.cypher.internal.physicalplanning.RefSlot], scala.Unit]): scala.Unit = {
    null
  }

  def mapSlot[U](onVariable: scala.Function1[scala.Tuple2[String, org.neo4j.cypher.internal.physicalplanning.Slot], U],
                 onCachedProperty: scala.Function1[scala.Tuple2[org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty, org.neo4j.cypher.internal.physicalplanning.RefSlot], U]): scala.Iterable[U] = {
    null
  }

  def canEqual(other: scala.Any): scala.Boolean = {
    false
  }

  override def equals(other: scala.Any): scala.Boolean = {
    false
  }

  override def hashCode(): scala.Int = {
    0
  }

  override def toString(): String = {
    null
  }

  def getLongSlots: scala.collection.immutable.IndexedSeq[org.neo4j.cypher.internal.physicalplanning.SlotWithAliases] = {
    null
  }

  def getRefSlots: scala.collection.immutable.IndexedSeq[org.neo4j.cypher.internal.physicalplanning.SlotWithAliases] = {
    null
  }

  def getCachedPropertySlots: scala.collection.immutable.IndexedSeq[org.neo4j.cypher.internal.physicalplanning.SlotWithAliases] = {
    null
  }

  def hasCachedPropertySlot(key: org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty): scala.Boolean = {
    false
  }

  def getCachedPropertySlot(key: org.neo4j.cypher.internal.v4_0.expressions.ASTCachedProperty): scala.Option[org.neo4j.cypher.internal.physicalplanning.RefSlot] = {
    null
  }

  object SlotWithAliasesOrdering extends java.lang.Object with scala.Ordering[org.neo4j.cypher.internal.physicalplanning.SlotWithAliases] {
    def compare(x: org.neo4j.cypher.internal.physicalplanning.SlotWithAliases, y: org.neo4j.cypher.internal.physicalplanning.SlotWithAliases): scala.Int = {
      0
    }
  }

  object SlotOrdering extends java.lang.Object with scala.Ordering[org.neo4j.cypher.internal.physicalplanning.Slot] {
    def compare(x: org.neo4j.cypher.internal.physicalplanning.Slot, y: org.neo4j.cypher.internal.physicalplanning.Slot): scala.Int = {
      0
    }
  }

}

object SlotConfiguration extends scala.AnyRef {
  def empty: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration = {
    null
  }

  def apply(slots: _root_.scala.Predef.Map[String, org.neo4j.cypher.internal.physicalplanning.Slot],
            numberOfLongs: scala.Int,
            numberOfReferences: scala.Int): org.neo4j.cypher.internal.physicalplanning.SlotConfiguration = {
    null
  }

  def isLongSlot(slot: org.neo4j.cypher.internal.physicalplanning.Slot): scala.Boolean = {
    false
  }

  final def isRefSlotAndNotAlias(slots: org.neo4j.cypher.internal.physicalplanning.SlotConfiguration, k: String): scala.Boolean = {
    false
  }

  case class Size(val nLongs: scala.Int, val nReferences: scala.Int) extends scala.AnyRef with scala.Product with scala.Serializable {
  }

  object Size extends scala.AnyRef with scala.Serializable {
    val zero: SlotConfiguration.Size = {
      null
    }
  }

}
