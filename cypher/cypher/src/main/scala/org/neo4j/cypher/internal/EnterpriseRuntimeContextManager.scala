/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal

case class EnterpriseRuntimeContextManager(val codeStructure: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructure[org.neo4j.cypher.internal.executionplan.GeneratedQuery],
                                           val log: org.neo4j.logging.Log,
                                           val config: org.neo4j.cypher.internal.CypherRuntimeConfiguration,
                                           val runtimeEnvironment: org.neo4j.cypher.internal.RuntimeEnvironment) extends scala.AnyRef with org.neo4j.cypher.internal.RuntimeContextManager[org.neo4j.cypher.internal.EnterpriseRuntimeContext] with scala.Product with scala.Serializable {
  override def create(tokenContext: org.neo4j.cypher.internal.planner.spi.TokenContext,
                      schemaRead: org.neo4j.internal.kernel.api.SchemaRead,
                      clock: java.time.Clock,
                      debugOptions: _root_.scala.Predef.Set[String],
                      compileExpressions: scala.Boolean,
                      materializedEntitiesMode: scala.Boolean,
                      operatorEngine: org.neo4j.cypher.CypherOperatorEngineOption,
                      interpretedPipesFallback: org.neo4j.cypher.CypherInterpretedPipesFallbackOption): org.neo4j.cypher.internal.EnterpriseRuntimeContext = {
    null
  }

  override def assertAllReleased(): scala.Unit = {
    null
  }
}
