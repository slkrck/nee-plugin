/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation

class DynamicCount(val opName: String,
                   variable: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable,
                   expression: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression,
                   groupingKey: scala.Iterable[scala.Tuple2[String, org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression]],
                   distinct: scala.Boolean) extends org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.BaseAggregateExpression(expression, distinct) {
  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def update[E](structure: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                        (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  def distinctCondition[E](value: E,
                           valueType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                           structure: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                          (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit])
                          (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def continuation(instruction: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction): org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction = {
    null
  }
}
