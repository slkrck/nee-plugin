/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.spi.codegen

class GeneratedMethodStructure(val fields: org.neo4j.cypher.internal.spi.codegen.Fields,
                               val generator: org.neo4j.codegen.CodeBlock,
                               aux: org.neo4j.cypher.internal.spi.codegen.AuxGenerator,
                               tracing: scala.Boolean = {
                                 false
                               },
                               events: scala.List[String] = {
                                 null
                               },
                               locals: scala.collection.mutable.Map[String, org.neo4j.codegen.LocalVariable] = {
                                 null
                               })
                              (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression] {
  override def nodeFromNodeValueIndexCursor(targetVar: String, iterVar: String): scala.Unit = {
    null
  }

  override def nodeFromNodeCursor(targetVar: String, iterVar: String): scala.Unit = {
    null
  }

  override def nodeFromNodeLabelIndexCursor(targetVar: String, iterVar: String): scala.Unit = {
    null
  }

  override def nextRelationshipAndNode(toNodeVar: String,
                                       iterVar: String,
                                       direction: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                                       fromNodeVar: String,
                                       relVar: String): scala.Unit = {
    null
  }

  override def nextRelationship(cursorName: String,
                                ignored: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                                relVar: String): scala.Unit = {
    null
  }

  override def allNodesScan(cursorName: String): scala.Unit = {
    null
  }

  override def labelScan(cursorName: String, labelIdVar: String): scala.Unit = {
    null
  }

  override def lookupLabelId(labelIdVar: String, labelName: String): scala.Unit = {
    null
  }

  override def lookupLabelIdE(labelName: String): org.neo4j.codegen.Expression = {
    null
  }

  override def lookupRelationshipTypeId(typeIdVar: String, typeName: String): scala.Unit = {
    null
  }

  override def lookupRelationshipTypeIdE(typeName: String): org.neo4j.codegen.Expression = {
    null
  }

  override def advanceNodeCursor(cursorName: String): org.neo4j.codegen.Expression = {
    null
  }

  override def closeNodeCursor(cursorName: String): scala.Unit = {
    null
  }

  override def advanceNodeLabelIndexCursor(cursorName: String): org.neo4j.codegen.Expression = {
    null
  }

  override def closeNodeLabelIndexCursor(cursorName: String): scala.Unit = {
    null
  }

  override def advanceRelationshipSelectionCursor(cursorName: String): org.neo4j.codegen.Expression = {
    null
  }

  override def closeRelationshipSelectionCursor(cursorName: String): scala.Unit = {
    null
  }

  override def advanceNodeValueIndexCursor(cursorName: String): org.neo4j.codegen.Expression = {
    null
  }

  override def closeNodeValueIndexCursor(cursorName: String): scala.Unit = {
    null
  }

  override def whileLoop(test: org.neo4j.codegen.Expression)
                        (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def forEach(varName: String,
                       codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                       iterable: org.neo4j.codegen.Expression)
                      (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def ifStatement(test: org.neo4j.codegen.Expression)
                          (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  def throwException(exception: org.neo4j.codegen.Expression): scala.Unit = {
    null
  }

  override def ifNotStatement(test: org.neo4j.codegen.Expression)
                             (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def ifNonNullStatement(test: org.neo4j.codegen.Expression,
                                  codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType)
                                 (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def ternaryOperator(test: org.neo4j.codegen.Expression,
                               onTrue: org.neo4j.codegen.Expression,
                               onFalse: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def returnSuccessfully(): scala.Unit = {
    null
  }

  override def declareCounter(name: String,
                              initialValue: org.neo4j.codegen.Expression,
                              errorOnFloatingPoint: String): scala.Unit = {
    null
  }

  override def decrementInteger(name: String): scala.Unit = {
    null
  }

  override def incrementInteger(name: String): scala.Unit = {
    null
  }

  override def incrementInteger(name: String, value: org.neo4j.codegen.Expression): scala.Unit = {
    null
  }

  override def checkInteger(name: String,
                            comparator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.Comparator,
                            value: scala.Long): org.neo4j.codegen.Expression = {
    null
  }

  override def setInRow(column: scala.Int, value: org.neo4j.codegen.Expression): scala.Unit = {
    null
  }

  override def toMaterializedAnyValue(expression: org.neo4j.codegen.Expression,
                                      codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def toAnyValue(expression: org.neo4j.codegen.Expression,
                          codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def visitorAccept(): scala.Unit = {
    null
  }

  override def materializeNode(nodeIdVar: String,
                               codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def node(nodeIdVar: String,
                    codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def nullablePrimitive(varName: String,
                                 codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                 onSuccess: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def nullableReference(varName: String,
                                 codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                 onSuccess: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def materializeRelationship(relIdVar: String,
                                       codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def relationship(relIdVar: String,
                            codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def materializeAny(expression: org.neo4j.codegen.Expression,
                              codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def trace[V](planStepId: String, maybeSuffix: scala.Option[String] = {
    null
  })(block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], V]): V = {
    null.asInstanceOf[V]
  }

  override def incrementDbHits(): scala.Unit = {
    null
  }

  override def incrementRows(): scala.Unit = {
    null
  }

  override def expectParameter(key: String,
                               variableName: String,
                               codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit = {
    null
  }

  override def mapGetExpression(map: org.neo4j.codegen.Expression, key: String): org.neo4j.codegen.Expression = {
    null
  }

  override def constantExpression(value: scala.AnyRef): org.neo4j.codegen.Expression = {
    null
  }

  override def constantValueExpression(value: scala.AnyRef,
                                       codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def notExpression(value: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def threeValuedNotExpression(value: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def threeValuedEqualsExpression(lhs: org.neo4j.codegen.Expression, rhs: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def threeValuedPrimitiveEqualsExpression(lhs: org.neo4j.codegen.Expression,
                                                    rhs: org.neo4j.codegen.Expression,
                                                    codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def equalityExpression(lhs: org.neo4j.codegen.Expression,
                                  rhs: org.neo4j.codegen.Expression,
                                  codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def primitiveEquals(lhs: org.neo4j.codegen.Expression, rhs: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def orExpression(lhs: org.neo4j.codegen.Expression, rhs: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def threeValuedOrExpression(lhs: org.neo4j.codegen.Expression, rhs: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def markAsNull(varName: String,
                          codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit = {
    null
  }

  override def isNull(varName: String,
                      codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def isNull(expr: org.neo4j.codegen.Expression,
                      codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def notNull(expr: org.neo4j.codegen.Expression,
                       codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def notNull(varName: String,
                       codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def ifNullThenNoValue(expr: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def box(expression: org.neo4j.codegen.Expression,
                   codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def unbox(expression: org.neo4j.codegen.Expression,
                     codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def toFloat(expression: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def nodeGetRelationshipsWithDirection(iterVar: String,
                                                 nodeVar: String,
                                                 nodeVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                                 direction: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection): scala.Unit = {
    null
  }

  override def nodeGetRelationshipsWithDirectionAndTypes(iterVar: String,
                                                         nodeVar: String,
                                                         nodeVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                                         direction: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                                                         typeVars: scala.Seq[String]): scala.Unit = {
    null
  }

  override def connectingRelationships(iterVar: String,
                                       fromNode: String,
                                       fromNodeType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                       direction: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                                       toNode: String,
                                       toNodeType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit = {
    null
  }

  override def connectingRelationships(iterVar: String,
                                       fromNode: String,
                                       fromNodeType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                       direction: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                                       typeVars: scala.Seq[String],
                                       toNode: String,
                                       toNodeType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit = {
    null
  }

  override def loadVariable(varName: String): org.neo4j.codegen.Expression = {
    null
  }

  override def multiplyPrimitive(lhs: org.neo4j.codegen.Expression, rhs: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def addExpression(lhs: org.neo4j.codegen.Expression, rhs: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def subtractExpression(lhs: org.neo4j.codegen.Expression, rhs: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def multiplyExpression(lhs: org.neo4j.codegen.Expression, rhs: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def divideExpression(lhs: org.neo4j.codegen.Expression, rhs: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def modulusExpression(lhs: org.neo4j.codegen.Expression, rhs: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def powExpression(lhs: org.neo4j.codegen.Expression, rhs: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def asList(values: scala.Seq[org.neo4j.codegen.Expression]): org.neo4j.codegen.Expression = {
    null
  }

  override def asAnyValueList(values: scala.Seq[org.neo4j.codegen.Expression]): org.neo4j.codegen.Expression = {
    null
  }

  override def asPrimitiveStream(publicTypeList: org.neo4j.codegen.Expression,
                                 codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def asPrimitiveStream(values: scala.Seq[org.neo4j.codegen.Expression],
                                 codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def declarePrimitiveIterator(name: String,
                                        iterableCodeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit = {
    null
  }

  override def primitiveIteratorFrom(iterable: org.neo4j.codegen.Expression,
                                     iterableCodeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def primitiveIteratorNext(iterator: org.neo4j.codegen.Expression,
                                     iterableCodeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def primitiveIteratorHasNext(iterator: org.neo4j.codegen.Expression,
                                        iterableCodeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def declareIterator(name: String): scala.Unit = {
    null
  }

  override def declareIterator(name: String,
                               elementCodeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit = {
    null
  }

  override def iteratorFrom(iterable: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def iteratorNext(iterator: org.neo4j.codegen.Expression,
                            codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  override def iteratorHasNext(iterator: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def toSet(value: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def newDistinctSet(name: String,
                              codeGenTypes: scala.Iterable[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType]): scala.Unit = {
    null
  }

  override def distinctSetIfNotContains(name: String,
                                        structure: _root_.scala.Predef.Map[String, scala.Tuple2[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType, org.neo4j.codegen.Expression]])
                                       (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def distinctSetIterate(name: String,
                                  keyTupleDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.HashableTupleDescriptor)
                                 (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def newUniqueAggregationKey(varName: String,
                                       structure: _root_.scala.Predef.Map[String, scala.Tuple2[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType, org.neo4j.codegen.Expression]]): scala.Unit = {
    null
  }

  override def newAggregationMap(name: String,
                                 keyTypes: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType]): scala.Unit = {
    null
  }

  override def newMapOfSets(name: String,
                            keyTypes: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType],
                            elementType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit = {
    null
  }

  override def allocateSortTable(name: String,
                                 tableDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor,
                                 count: org.neo4j.codegen.Expression): scala.Unit = {
    null
  }

  override def sortTableAdd(name: String,
                            tableDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor,
                            value: org.neo4j.codegen.Expression): scala.Unit = {
    null
  }

  override def sortTableSort(name: String,
                             tableDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor): scala.Unit = {
    null
  }

  override def sortTableIterate(tableName: String,
                                tableDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor,
                                varNameToField: _root_.scala.Predef.Map[String, String])
                               (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def aggregationMapGet(mapName: String,
                                 valueVarName: String,
                                 key: _root_.scala.Predef.Map[String, scala.Tuple2[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType, org.neo4j.codegen.Expression]],
                                 keyVar: String): scala.Unit = {
    null
  }

  override def checkDistinct(name: String,
                             key: _root_.scala.Predef.Map[String, scala.Tuple2[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType, org.neo4j.codegen.Expression]],
                             keyVar: String,
                             value: org.neo4j.codegen.Expression,
                             valueType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType)
                            (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def aggregationMapPut(name: String,
                                 key: _root_.scala.Predef.Map[String, scala.Tuple2[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType, org.neo4j.codegen.Expression]],
                                 keyVar: String,
                                 value: org.neo4j.codegen.Expression): scala.Unit = {
    null
  }

  override def aggregationMapIterate(name: String,
                                     keyTupleDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.HashableTupleDescriptor,
                                     valueVar: String)
                                    (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def asMap(map: _root_.scala.Predef.Map[String, org.neo4j.codegen.Expression]): org.neo4j.codegen.Expression = {
    null
  }

  override def invokeMethod(resultType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType,
                            resultVar: String,
                            methodName: String)
                           (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def allocateProbeTable(tableVar: String,
                                  tableType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType): scala.Unit = {
    null
  }

  override def updateProbeTableCount(tableVar: String,
                                     tableType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CountingJoinTableType,
                                     keyVars: scala.Seq[String]): scala.Unit = {
    null
  }

  override def probe(tableVar: String,
                     tableType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType,
                     keyVars: scala.Seq[String])
                    (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def putField(tupleDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.TupleDescriptor,
                        value: org.neo4j.codegen.Expression,
                        fieldName: String,
                        localVar: String): scala.Unit = {
    null
  }

  override def updateProbeTable(tupleDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.TupleDescriptor,
                                tableVar: String,
                                tableType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.RecordingJoinTableType,
                                keyVars: scala.Seq[String],
                                element: org.neo4j.codegen.Expression): scala.Unit = {
    null
  }

  override def declareProperty(propertyVar: String): scala.Unit = {
    null
  }

  override def declareAndInitialize(varName: String,
                                    codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit = {
    null
  }

  override def declare(varName: String,
                       codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit = {
    null
  }

  override def assign(varName: String,
                      codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                      expression: org.neo4j.codegen.Expression): scala.Unit = {
    null
  }

  override def hasLabel(nodeVar: String,
                        labelVar: String,
                        predVar: String): org.neo4j.codegen.Expression = {
    null
  }

  override def relType(relVar: String, typeVar: String): scala.Unit = {
    null
  }

  override def localVariable(variable: String,
                             e: org.neo4j.codegen.Expression,
                             codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit = {
    null
  }

  override def declareFlag(name: String, initialValue: scala.Boolean): scala.Unit = {
    null
  }

  override def updateFlag(name: String, newValue: scala.Boolean): scala.Unit = {
    null
  }

  override def declarePredicate(name: String): scala.Unit = {
    null
  }

  override def nodeGetPropertyForVar(nodeVar: String,
                                     nodeVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                     propIdVar: String,
                                     propValueVar: String): scala.Unit = {
    null
  }

  override def nodeGetPropertyById(nodeVar: String,
                                   nodeVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                   propId: scala.Int,
                                   propValueVar: String): scala.Unit = {
    null
  }

  override def nodeIdSeek(nodeIdVar: String,
                          expression: org.neo4j.codegen.Expression,
                          codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType)
                         (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[org.neo4j.codegen.Expression], scala.Unit]): scala.Unit = {
    null
  }

  override def relationshipGetPropertyForVar(relIdVar: String,
                                             relVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                             propIdVar: String,
                                             propValueVar: String): scala.Unit = {
    null
  }

  override def relationshipGetPropertyById(relIdVar: String,
                                           relVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                           propId: scala.Int,
                                           propValueVar: String): scala.Unit = {
    null
  }

  override def lookupPropertyKey(propName: String, propIdVar: String): scala.Unit = {
    null
  }

  override def newIndexReference(referenceVar: String,
                                 labelVar: String,
                                 propKeyVar: String): scala.Unit = {
    null
  }

  override def indexSeek(cursorName: String,
                         indexReference: String,
                         value: org.neo4j.codegen.Expression,
                         codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit = {
    null
  }

  def token(t: scala.Int): org.neo4j.codegen.Expression = {
    null
  }

  def wildCardToken: org.neo4j.codegen.Expression = {
    null
  }

  override def nodeCountFromCountStore(expression: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def relCountFromCountStore(start: org.neo4j.codegen.Expression,
                                      end: org.neo4j.codegen.Expression,
                                      types: org.neo4j.codegen.Expression*): org.neo4j.codegen.Expression = {
    null
  }

  override def coerceToBoolean(propertyExpression: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  override def newTableValue(targetVar: String,
                             tupleDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.TupleDescriptor): org.neo4j.codegen.Expression = {
    null
  }

  override def noValue(): org.neo4j.codegen.Expression = {
    null
  }
}

object GeneratedMethodStructure extends scala.AnyRef {
  type CompletableFinalizer = scala.Function1[scala.Boolean, scala.Function1[org.neo4j.codegen.CodeBlock, scala.Unit]]
}
