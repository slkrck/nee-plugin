/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.spi.codegen

object Methods extends scala.AnyRef {
  val countingTableIncrement: org.neo4j.codegen.MethodReference = {
    null
  }
  val countingTableCompositeKeyPut: org.neo4j.codegen.MethodReference = {
    null
  }
  val countingTableGet: org.neo4j.codegen.MethodReference = {
    null
  }
  val countingTableCompositeKeyGet: org.neo4j.codegen.MethodReference = {
    null
  }
  val compositeKey: org.neo4j.codegen.MethodReference = {
    null
  }
  val hasNextLong: org.neo4j.codegen.MethodReference = {
    null
  }
  val hasMoreRelationship: org.neo4j.codegen.MethodReference = {
    null
  }
  val createMap: org.neo4j.codegen.MethodReference = {
    null
  }
  val createAnyValueMap: org.neo4j.codegen.MethodReference = {
    null
  }
  val format: org.neo4j.codegen.MethodReference = {
    null
  }
  val relationshipVisit: org.neo4j.codegen.MethodReference = {
    null
  }
  val getRelationship: org.neo4j.codegen.MethodReference = {
    null
  }
  val startNode: org.neo4j.codegen.MethodReference = {
    null
  }
  val endNode: org.neo4j.codegen.MethodReference = {
    null
  }
  val typeOf: org.neo4j.codegen.MethodReference = {
    null
  }
  val allConnectingRelationships: org.neo4j.codegen.MethodReference = {
    null
  }
  val connectingRelationships: org.neo4j.codegen.MethodReference = {
    null
  }
  val mathAdd: org.neo4j.codegen.MethodReference = {
    null
  }
  val mathSub: org.neo4j.codegen.MethodReference = {
    null
  }
  val mathMul: org.neo4j.codegen.MethodReference = {
    null
  }
  val mathPow: org.neo4j.codegen.MethodReference = {
    null
  }
  val mathDiv: org.neo4j.codegen.MethodReference = {
    null
  }
  val mathMod: org.neo4j.codegen.MethodReference = {
    null
  }
  val mathCastToInt: org.neo4j.codegen.MethodReference = {
    null
  }
  val mathCastToLong: org.neo4j.codegen.MethodReference = {
    null
  }
  val mathCastToLongOrFail: org.neo4j.codegen.MethodReference = {
    null
  }
  val mapGet: org.neo4j.codegen.MethodReference = {
    null
  }
  val mapContains: org.neo4j.codegen.MethodReference = {
    null
  }
  val setContains: org.neo4j.codegen.MethodReference = {
    null
  }
  val setAdd: org.neo4j.codegen.MethodReference = {
    null
  }
  val listAdd: org.neo4j.codegen.MethodReference = {
    null
  }
  val labelGetForName: org.neo4j.codegen.MethodReference = {
    null
  }
  val propertyKeyGetForName: org.neo4j.codegen.MethodReference = {
    null
  }
  val coerceToPredicate: org.neo4j.codegen.MethodReference = {
    null
  }
  val ternaryEquals: org.neo4j.codegen.MethodReference = {
    null
  }
  val equals: org.neo4j.codegen.MethodReference = {
    null
  }
  val or: org.neo4j.codegen.MethodReference = {
    null
  }
  val not: org.neo4j.codegen.MethodReference = {
    null
  }
  val relationshipTypeGetForName: org.neo4j.codegen.MethodReference = {
    null
  }
  val relationshipTypeGetName: org.neo4j.codegen.MethodReference = {
    null
  }
  val nodeExists: org.neo4j.codegen.MethodReference = {
    null
  }
  val countsForNode: org.neo4j.codegen.MethodReference = {
    null
  }
  val countsForRel: org.neo4j.codegen.MethodReference = {
    null
  }
  val nextLong: org.neo4j.codegen.MethodReference = {
    null
  }
  val fetchNextRelationship: org.neo4j.codegen.MethodReference = {
    null
  }
  val newNodeEntityById: org.neo4j.codegen.MethodReference = {
    null
  }
  val newRelationshipEntityById: org.neo4j.codegen.MethodReference = {
    null
  }
  val materializeAnyResult: org.neo4j.codegen.MethodReference = {
    null
  }
  val materializeAnyValueResult: org.neo4j.codegen.MethodReference = {
    null
  }
  val materializeNodeValue: org.neo4j.codegen.MethodReference = {
    null
  }
  val materializeRelationshipValue: org.neo4j.codegen.MethodReference = {
    null
  }
  val nodeId: org.neo4j.codegen.MethodReference = {
    null
  }
  val relId: org.neo4j.codegen.MethodReference = {
    null
  }
  val set: org.neo4j.codegen.MethodReference = {
    null
  }
  val visit: org.neo4j.codegen.MethodReference = {
    null
  }
  val executeOperator: org.neo4j.codegen.MethodReference = {
    null
  }
  val dbHit: org.neo4j.codegen.MethodReference = {
    null
  }
  val row: org.neo4j.codegen.MethodReference = {
    null
  }
  val unboxInteger: org.neo4j.codegen.MethodReference = {
    null
  }
  val unboxBoolean: org.neo4j.codegen.MethodReference = {
    null
  }
  val unboxLong: org.neo4j.codegen.MethodReference = {
    null
  }
  val unboxDouble: org.neo4j.codegen.MethodReference = {
    null
  }
  val unboxNode: org.neo4j.codegen.MethodReference = {
    null
  }
  val unboxRel: org.neo4j.codegen.MethodReference = {
    null
  }
  val reboxValue: org.neo4j.codegen.MethodReference = {
    null
  }
}
