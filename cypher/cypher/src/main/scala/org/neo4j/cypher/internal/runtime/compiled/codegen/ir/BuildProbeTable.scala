/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.compiled.codegen.ir

sealed trait BuildProbeTable extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction {
  protected val name: String

  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  def joinData: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.JoinData

  def tableType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType

  protected override def children: scala.collection.Seq[scala.Nothing] = {
    null
  }
}

object BuildProbeTable extends scala.AnyRef {
  def apply(id: String,
            name: String,
            nodes: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.compiled.codegen.Variable],
            valueSymbols: _root_.scala.Predef.Map[String, org.neo4j.cypher.internal.runtime.compiled.codegen.Variable])
           (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): org.neo4j.cypher.internal.runtime.compiled.codegen.ir.BuildProbeTable = {
    null
  }
}

case class BuildCountingProbeTable(val id: String,
                                   val name: String,
                                   val nodes: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.compiled.codegen.Variable]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.BuildProbeTable with scala.Product with scala.Serializable {
  override val tableType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CountingJoinTableType = {
    null
  }

  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def joinData: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.JoinData = {
    null
  }

  protected override def operatorId: scala.collection.immutable.Set[String] = {
    null
  }
}

case class BuildRecordingProbeTable(val id: String,
                                    val name: String,
                                    val nodes: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.compiled.codegen.Variable],
                                    val valueSymbols: _root_.scala.Predef.Map[String, org.neo4j.cypher.internal.runtime.compiled.codegen.Variable])
                                   (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.BuildProbeTable with scala.Product with scala.Serializable {
  override val tableType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.RecordingJoinTableType = {
    null
  }
  val joinData: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.JoinData = {
    null
  }

  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit ignored: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  protected override def operatorId: scala.collection.immutable.Set[String] = {
    null
  }
}
