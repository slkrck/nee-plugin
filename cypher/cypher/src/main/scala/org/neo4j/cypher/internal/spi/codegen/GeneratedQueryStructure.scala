/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.spi.codegen

object GeneratedQueryStructure extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructure[org.neo4j.cypher.internal.executionplan.GeneratedQuery] {

  override def generateQuery(className: String,
                             columns: scala.Seq[String],
                             operatorIds: _root_.scala.Predef.Map[String, org.neo4j.cypher.internal.v4_0.util.attribution.Id],
                             conf: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenConfiguration)
                            (methodStructure: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[_], scala.Unit])
                            (implicit codeGenContext: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): GeneratedQueryStructure.GeneratedQueryStructureResult = {
    null
  }

  def method[O <: scala.AnyRef, R](name: String, params: org.neo4j.codegen.TypeReference*)
                                  (implicit owner: _root_.scala.Predef.Manifest[O],
                                   returns: _root_.scala.Predef.Manifest[R]): org.neo4j.codegen.MethodReference = {
    null
  }

  def staticField[O <: scala.AnyRef, R](name: String)
                                       (implicit owner: _root_.scala.Predef.Manifest[O],
                                        fieldType: _root_.scala.Predef.Manifest[R]): org.neo4j.codegen.FieldReference = {
    null
  }

  def param[T <: scala.AnyRef](name: String)(implicit manifest: _root_.scala.Predef.Manifest[T]): org.neo4j.codegen.Parameter = {
    null
  }

  def typeRef[T](implicit manifest: _root_.scala.Predef.Manifest[T]): org.neo4j.codegen.TypeReference = {
    null
  }

  def typeReference(manifest: _root_.scala.Predef.Manifest[_]): org.neo4j.codegen.TypeReference = {
    null
  }

  def lowerType(cType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.TypeReference = {
    null
  }

  def lowerTypeScalarSubset(cType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.TypeReference = {
    null
  }

  def nullValue(cType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): org.neo4j.codegen.Expression = {
    null
  }

  case class GeneratedQueryStructureResult(val query: org.neo4j.cypher.internal.executionplan.GeneratedQuery,
                                           val code: scala.Seq[org.neo4j.cypher.internal.plandescription.Argument]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructureResult[org.neo4j.cypher.internal.executionplan.GeneratedQuery] with scala.Product with scala.Serializable {
  }

}
