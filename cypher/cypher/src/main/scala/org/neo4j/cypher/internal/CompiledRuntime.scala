/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal

object CompiledRuntime extends scala.AnyRef with org.neo4j.cypher.internal.CypherRuntime[org.neo4j.cypher.internal.EnterpriseRuntimeContext] {
  override def name: String = {
    null
  }

  @scala.throws[org.neo4j.exceptions.CantCompileQueryException]
  override def compileToExecutable(query: org.neo4j.cypher.internal.LogicalQuery,
                                   context: org.neo4j.cypher.internal.EnterpriseRuntimeContext,
                                   securityContext: org.neo4j.internal.kernel.api.security.SecurityContext = {
                                     null
                                   }): org.neo4j.cypher.internal.ExecutionPlan = {
    null
  }

  class CompiledExecutionPlan(val compiled: org.neo4j.cypher.internal.runtime.compiled.CompiledPlan) extends org.neo4j.cypher.internal.ExecutionPlan {
    override val runtimeName: org.neo4j.cypher.internal.RuntimeName = {
      null
    }

    override def run(queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                     executionMode: org.neo4j.cypher.internal.runtime.ExecutionMode,
                     params: org.neo4j.values.virtual.MapValue,
                     prePopulateResults: scala.Boolean,
                     input: org.neo4j.cypher.internal.runtime.InputDataStream,
                     subscriber: org.neo4j.kernel.impl.query.QuerySubscriber): org.neo4j.cypher.result.RuntimeResult = {
      null
    }

    override def metadata: scala.Seq[org.neo4j.cypher.internal.plandescription.Argument] = {
      null
    }

    override def notifications: _root_.scala.Predef.Set[org.neo4j.cypher.internal.v4_0.util.InternalNotification] = {
      null
    }
  }

}
