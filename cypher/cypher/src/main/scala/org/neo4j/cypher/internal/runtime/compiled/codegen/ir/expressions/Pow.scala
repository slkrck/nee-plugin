/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions

case class Pow(val lhs: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression,
               val rhs: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.BinaryOperator with scala.Product with scala.Serializable {
  override def nullable(implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Boolean = {
    false
  }

  override def name: String = {
    null
  }

  override def codeGenType(implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CypherCodeGenType = {
    null
  }

  protected override def generator[E](structure: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                                     (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Function2[E, E, E] = {
    null
  }
}
