/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.compiled.codegen

class CodeGenerator(val structure: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructure[org.neo4j.cypher.internal.executionplan.GeneratedQuery],
                    clock: java.time.Clock,
                    conf: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenConfiguration = {
                      null
                    }) extends scala.AnyRef {
  def generate(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
               tokenContext: org.neo4j.cypher.internal.planner.spi.TokenContext,
               semanticTable: org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable,
               readOnly: scala.Boolean,
               cardinalities: org.neo4j.cypher.internal.planner.spi.PlanningAttributes.Cardinalities,
               originalReturnColumns: scala.Seq[String]): org.neo4j.cypher.internal.runtime.compiled.CompiledPlan = {
    null
  }
}

object CodeGenerator extends scala.AnyRef {
  type SourceSink = scala.Option[scala.Function2[String, String, scala.Unit]]

  def generateCode[T](structure: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructure[T])
                     (instructions: scala.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction],
                      operatorIds: _root_.scala.Predef.Map[String, org.neo4j.cypher.internal.v4_0.util.attribution.Id],
                      columns: scala.Seq[String],
                      conf: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenConfiguration)
                     (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CodeStructureResult[T] = {
    null
  }
}
