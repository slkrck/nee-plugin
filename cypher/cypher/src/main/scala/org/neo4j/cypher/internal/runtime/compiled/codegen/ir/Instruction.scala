/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.compiled.codegen.ir

trait Instruction extends scala.AnyRef {
  def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
             (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
             (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit

  final def allOperatorIds: _root_.scala.Predef.Set[String] = {
    null
  }

  protected def children: scala.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction]

  protected def operatorId: _root_.scala.Predef.Set[String] = {
    null
  }
}

object Instruction extends scala.AnyRef {
  val empty: scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction = {
    null
  }
}

case class AggregationInstruction(val opName: String,
                                  val aggregationFunctions: scala.Iterable[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.aggregation.AggregateExpression]) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction with scala.Product with scala.Serializable {
  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  protected override def children: scala.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction] = {
    null
  }

  protected override def operatorId: scala.collection.immutable.Set[String] = {
    null
  }
}

case class ApplyInstruction(val id: String,
                            val instruction: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction with scala.Product with scala.Serializable {
  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def children: scala.collection.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction] = {
    null
  }

  protected override def operatorId: scala.collection.immutable.Set[String] = {
    null
  }
}

case class CheckingInstruction(val inner: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction,
                               val yieldedFlagVar: String) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction with scala.Product with scala.Serializable {
  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def children: scala.collection.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction] = {
    null
  }
}

case class NodeCountFromCountStoreInstruction(val opName: String,
                                              val variable: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable,
                                              val labels: scala.List[scala.Option[scala.Tuple2[scala.Option[scala.Int], String]]],
                                              val inner: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction with scala.Product with scala.Serializable {
  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def operatorId: _root_.scala.Predef.Set[String] = {
    null
  }

  override def children: scala.collection.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction] = {
    null
  }

  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }
}

case class CartesianProductInstruction(val id: String,
                                       val instruction: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction with scala.Product with scala.Serializable {
  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def children: scala.collection.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction] = {
    null
  }

  protected override def operatorId: scala.collection.immutable.Set[String] = {
    null
  }
}

case class NullingInstruction(val loop: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction,
                              val yieldedFlagVar: String,
                              val alternativeAction: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction,
                              val nullableVars: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable*) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction with scala.Product with scala.Serializable {
  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def children: scala.collection.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction] = {
    null
  }
}

case class SelectionInstruction(val id: String,
                                val instruction: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction with scala.Product with scala.Serializable {
  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def children: scala.collection.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction] = {
    null
  }

  protected override def operatorId: scala.collection.immutable.Set[String] = {
    null
  }
}

case class RelationshipCountFromCountStoreInstruction(val opName: String,
                                                      val variable: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable,
                                                      val startLabel: scala.Option[scala.Tuple2[scala.Option[scala.Int], String]],
                                                      val relTypes: scala.Seq[scala.Tuple2[scala.Option[scala.Int], String]],
                                                      val endLabel: scala.Option[scala.Tuple2[scala.Option[scala.Int], String]],
                                                      val inner: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction with scala.Product with scala.Serializable {
  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def operatorId: _root_.scala.Predef.Set[String] = {
    null
  }

  override def children: scala.collection.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction] = {
    null
  }

  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }
}

case class SkipInstruction(val opName: String,
                           val variableName: String,
                           val action: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction,
                           val numberToSkip: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction with scala.Product with scala.Serializable {
  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  protected override def children: scala.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction] = {
    null
  }

  protected override def operatorId: scala.collection.immutable.Set[String] = {
    null
  }
}

case class SortInstruction(val opName: String,
                           val sortTableInfo: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.SortTableInfo) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction with scala.Product with scala.Serializable {
  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def body[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  protected override def children: scala.Seq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.Instruction] = {
    null
  }

  protected override def operatorId: scala.collection.immutable.Set[String] = {
    null
  }
}