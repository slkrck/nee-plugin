/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.compiled.codegen.spi

trait MethodStructure[E] extends scala.AnyRef {
  def localVariable(variable: String,
                    e: E,
                    codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit

  def declareFlag(name: String, initialValue: scala.Boolean): scala.Unit

  def updateFlag(name: String, newValue: scala.Boolean): scala.Unit

  def declarePredicate(name: String): scala.Unit

  def assign(varName: String,
             codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
             value: E): scala.Unit

  def assign(v: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable, value: E): scala.Unit = {
    null
  }

  def declareAndInitialize(varName: String,
                           codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit

  def declare(varName: String, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit

  def declareProperty(name: String): scala.Unit

  def declareCounter(name: String, initialValue: E, errorOnFloatingPoint: String): scala.Unit

  def putField(tupleDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.TupleDescriptor,
               value: E,
               fieldName: String,
               localVar: String): scala.Unit

  def updateProbeTable(tupleDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.TupleDescriptor,
                       tableVar: String,
                       tableType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.RecordingJoinTableType,
                       keyVars: scala.Seq[String],
                       element: E): scala.Unit

  def probe(tableVar: String,
            tableType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType,
            keyVars: scala.Seq[String])
           (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def updateProbeTableCount(tableVar: String,
                            tableType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.CountingJoinTableType,
                            keyVar: scala.Seq[String]): scala.Unit

  def allocateProbeTable(tableVar: String, tableType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType): scala.Unit

  def invokeMethod(resultType: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.JoinTableType,
                   resultVar: String,
                   methodName: String)
                  (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def coerceToBoolean(propertyExpression: E): E

  def incrementInteger(name: String): scala.Unit

  def decrementInteger(name: String): scala.Unit

  def incrementInteger(name: String, value: E): scala.Unit

  def checkInteger(variableName: String,
                   comparator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.Comparator,
                   value: scala.Long): E

  def newTableValue(targetVar: String, tupleDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.TupleDescriptor): E

  def noValue(): E

  def constantExpression(value: scala.AnyRef): E

  def constantValueExpression(value: scala.AnyRef, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def constantPrimitiveExpression(value: scala.AnyVal): E = {
    null.asInstanceOf[E]
  }

  def asMap(map: _root_.scala.Predef.Map[String, E]): E

  def asList(values: scala.Seq[E]): E

  def asAnyValueList(values: scala.Seq[E]): E

  def asPrimitiveStream(values: E, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def asPrimitiveStream(values: scala.Seq[E], codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def declarePrimitiveIterator(name: String,
                               iterableCodeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit

  def primitiveIteratorFrom(iterable: E, iterableCodeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def primitiveIteratorNext(iterator: E, iterableCodeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def primitiveIteratorHasNext(iterator: E, iterableCodeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def declareIterator(name: String): scala.Unit

  def declareIterator(name: String, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit

  def iteratorFrom(iterable: E): E

  def iteratorNext(iterator: E, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def iteratorHasNext(iterator: E): E

  def toSet(value: E): E

  def newDistinctSet(name: String,
                     codeGenTypes: scala.Iterable[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType]): scala.Unit

  def distinctSetIfNotContains(name: String,
                               structure: _root_.scala.Predef.Map[String, scala.Tuple2[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType, E]])
                              (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def distinctSetIterate(name: String, key: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.HashableTupleDescriptor)
                        (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def newUniqueAggregationKey(varName: String,
                              structure: _root_.scala.Predef.Map[String, scala.Tuple2[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType, E]]): scala.Unit

  def newAggregationMap(name: String,
                        keyTypes: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType]): scala.Unit

  def aggregationMapGet(name: String,
                        varName: String,
                        key: _root_.scala.Predef.Map[String, scala.Tuple2[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType, E]],
                        keyVar: String): scala.Unit

  def aggregationMapPut(name: String,
                        key: _root_.scala.Predef.Map[String, scala.Tuple2[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType, E]],
                        keyVar: String,
                        value: E): scala.Unit

  def aggregationMapIterate(name: String,
                            key: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.HashableTupleDescriptor,
                            valueVar: String)
                           (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def newMapOfSets(name: String,
                   keyTypes: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType],
                   elementType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit

  def checkDistinct(name: String,
                    key: _root_.scala.Predef.Map[String, scala.Tuple2[org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType, E]],
                    keyVar: String,
                    value: E,
                    valueType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType)
                   (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def allocateSortTable(name: String,
                        tableDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor,
                        count: E): scala.Unit

  def sortTableAdd(name: String,
                   tableDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor,
                   value: E): scala.Unit

  def sortTableSort(name: String, tableDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor): scala.Unit

  def sortTableIterate(name: String,
                       tableDescriptor: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.SortTableDescriptor,
                       varNameToField: _root_.scala.Predef.Map[String, String])
                      (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def loadVariable(varName: String): E

  def multiplyPrimitive(lhs: E, rhs: E): E

  def addExpression(lhs: E, rhs: E): E

  def subtractExpression(lhs: E, rhs: E): E

  def multiplyExpression(lhs: E, rhs: E): E

  def divideExpression(lhs: E, rhs: E): E

  def modulusExpression(lhs: E, rhs: E): E

  def powExpression(lhs: E, rhs: E): E

  def threeValuedNotExpression(value: E): E

  def notExpression(value: E): E

  def threeValuedEqualsExpression(lhs: E, rhs: E): E

  def threeValuedPrimitiveEqualsExpression(lhs: E, rhs: E, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def equalityExpression(lhs: E, rhs: E, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def primitiveEquals(lhs: E, rhs: E): E

  def orExpression(lhs: E, rhs: E): E

  def threeValuedOrExpression(lhs: E, rhs: E): E

  def markAsNull(varName: String, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit

  def nullablePrimitive(varName: String,
                        codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                        onSuccess: E): E

  def nullableReference(varName: String,
                        codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                        onSuccess: E): E

  def isNull(expr: E, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def isNull(name: String, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def notNull(expr: E, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def notNull(name: String, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def ifNullThenNoValue(expr: E): E

  def box(expression: E, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def unbox(expression: E, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def toFloat(expression: E): E

  def expectParameter(key: String,
                      variableName: String,
                      codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit

  def mapGetExpression(map: E, key: String): E

  def trace[V](planStepId: String, maybeSuffix: scala.Option[String] = {
    null
  })(block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], V]): V

  def incrementDbHits(): scala.Unit

  def incrementRows(): scala.Unit

  def labelScan(iterVar: String, labelIdVar: String): scala.Unit

  def hasLabel(nodeVar: String, labelVar: String, predVar: String): E

  def allNodesScan(iterVar: String): scala.Unit

  def lookupLabelId(labelIdVar: String, labelName: String): scala.Unit

  def lookupLabelIdE(labelName: String): E

  def lookupRelationshipTypeId(typeIdVar: String, typeName: String): scala.Unit

  def lookupRelationshipTypeIdE(typeName: String): E

  def nodeGetRelationshipsWithDirection(iterVar: String,
                                        nodeVar: String,
                                        nodeVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                        direction: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection): scala.Unit

  def nodeGetRelationshipsWithDirectionAndTypes(iterVar: String,
                                                nodeVar: String,
                                                nodeVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                                direction: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                                                typeVars: scala.Seq[String]): scala.Unit

  def connectingRelationships(iterVar: String,
                              fromNode: String,
                              fromNodeType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                              dir: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                              toNode: String,
                              toNodeType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit

  def connectingRelationships(iterVar: String,
                              fromNode: String,
                              fromNodeType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                              dir: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                              types: scala.Seq[String],
                              toNode: String,
                              toNodeType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit

  def nodeFromNodeValueIndexCursor(targetVar: String, iterVar: String): scala.Unit

  def nodeFromNodeCursor(targetVar: String, iterVar: String): scala.Unit

  def nodeFromNodeLabelIndexCursor(targetVar: String, iterVar: String): scala.Unit

  def nextRelationshipAndNode(toNodeVar: String,
                              iterVar: String,
                              direction: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                              fromNodeVar: String,
                              relVar: String): scala.Unit

  def nextRelationship(iterVar: String,
                       direction: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                       relVar: String): scala.Unit

  def advanceNodeCursor(cursorName: String): E

  def closeNodeCursor(cursorName: String): scala.Unit

  def advanceNodeLabelIndexCursor(cursorName: String): E

  def closeNodeLabelIndexCursor(cursorName: String): scala.Unit

  def advanceRelationshipSelectionCursor(cursorName: String): E

  def closeRelationshipSelectionCursor(cursorName: String): scala.Unit

  def advanceNodeValueIndexCursor(cursorName: String): E

  def closeNodeValueIndexCursor(cursorName: String): scala.Unit

  def nodeGetPropertyById(nodeVar: String,
                          nodeVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                          propId: scala.Int,
                          propValueVar: String): scala.Unit

  def nodeGetPropertyForVar(nodeVar: String,
                            nodeVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                            propIdVar: String,
                            propValueVar: String): scala.Unit

  def nodeIdSeek(nodeIdVar: String,
                 expression: E,
                 codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType)
                (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def relationshipGetPropertyById(relIdVar: String,
                                  relVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                  propId: scala.Int,
                                  propValueVar: String): scala.Unit

  def relationshipGetPropertyForVar(relIdVar: String,
                                    relVarType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType,
                                    propIdVar: String,
                                    propValueVar: String): scala.Unit

  def lookupPropertyKey(propName: String, propVar: String): scala.Unit

  def indexSeek(iterVar: String,
                descriptorVar: String,
                value: E,
                codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): scala.Unit

  def relType(relIdVar: String, typeVar: String): scala.Unit

  def newIndexReference(descriptorVar: String, labelVar: String, propKeyVar: String): scala.Unit

  def nodeCountFromCountStore(expression: E): E

  def relCountFromCountStore(start: E, end: E, types: E*): E

  def token(t: scala.Int): E

  def wildCardToken: E

  def whileLoop(test: E)(block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def forEach(varName: String, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType, iterable: E)
             (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def ifStatement(test: E)(block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def ifNotStatement(test: E)(block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def ifNonNullStatement(test: E, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType)
                        (block: scala.Function1[org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], scala.Unit]): scala.Unit

  def ternaryOperator(test: E, onTrue: E, onFalse: E): E

  def returnSuccessfully(): scala.Unit

  def throwException(exception: org.neo4j.codegen.Expression): scala.Unit

  def materializeNode(nodeIdVar: String, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def node(nodeIdVar: String, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def materializeRelationship(relIdVar: String,
                              codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def relationship(relIdVar: String, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def materializeAny(expression: E, codeGenType: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def visitorAccept(): scala.Unit

  def setInRow(column: scala.Int, value: E): scala.Unit

  def toAnyValue(e: E, t: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E

  def toMaterializedAnyValue(e: E, t: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenType): E
}
