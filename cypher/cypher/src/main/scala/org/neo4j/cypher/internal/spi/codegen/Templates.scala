/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.spi.codegen

object Templates extends scala.AnyRef {
  val newCountingMap: org.neo4j.codegen.Expression = {
    null
  }
  val noValue: org.neo4j.codegen.Expression = {
    null
  }
  val incoming: org.neo4j.codegen.Expression = {
    null
  }
  val outgoing: org.neo4j.codegen.Expression = {
    null
  }
  val both: org.neo4j.codegen.Expression = {
    null
  }
  val newResultRow: org.neo4j.codegen.Expression = {
    null
  }
  val newRelationshipDataExtractor: org.neo4j.codegen.Expression = {
    null
  }
  val valueComparator: org.neo4j.codegen.Expression = {
    null
  }
  val anyValueComparator: org.neo4j.codegen.Expression = {
    null
  }
  val FIELD_NAMES: org.neo4j.codegen.MethodTemplate = {
    null
  }

  def createNewInstance(valueType: org.neo4j.codegen.TypeReference,
                        args: scala.Tuple2[org.neo4j.codegen.TypeReference, org.neo4j.codegen.Expression]*): org.neo4j.codegen.Expression = {
    null
  }

  def createNewNodeReference(expression: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  def createNewRelationshipReference(expression: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  def createNewNodeValueFromPrimitive(proxySpi: org.neo4j.codegen.Expression, expression: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  def createNewRelationshipValueFromPrimitive(proxySpi: org.neo4j.codegen.Expression,
                                              expression: org.neo4j.codegen.Expression): org.neo4j.codegen.Expression = {
    null
  }

  def asList[T](values: scala.Seq[org.neo4j.codegen.Expression])(implicit manifest: _root_.scala.Predef.Manifest[T]): org.neo4j.codegen.Expression = {
    null
  }

  def asAnyValueList(values: scala.Seq[org.neo4j.codegen.Expression]): org.neo4j.codegen.Expression = {
    null
  }

  def asPrimitiveNodeStream(values: scala.Seq[org.neo4j.codegen.Expression]): org.neo4j.codegen.Expression = {
    null
  }

  def asPrimitiveRelationshipStream(values: scala.Seq[org.neo4j.codegen.Expression]): org.neo4j.codegen.Expression = {
    null
  }

  def asLongStream(values: scala.Seq[org.neo4j.codegen.Expression]): org.neo4j.codegen.Expression = {
    null
  }

  def asDoubleStream(values: scala.Seq[org.neo4j.codegen.Expression]): org.neo4j.codegen.Expression = {
    null
  }

  def asIntStream(values: scala.Seq[org.neo4j.codegen.Expression]): org.neo4j.codegen.Expression = {
    null
  }

  def handleEntityNotFound[V](generate: org.neo4j.codegen.CodeBlock,
                              fields: org.neo4j.cypher.internal.spi.codegen.Fields,
                              namer: org.neo4j.cypher.internal.runtime.compiled.codegen.Namer)
                             (happyPath: scala.Function1[org.neo4j.codegen.CodeBlock, V])
                             (onFailure: scala.Function1[org.neo4j.codegen.CodeBlock, V]): V = {
    null.asInstanceOf[V]
  }

  def handleKernelExceptions[V](generate: org.neo4j.codegen.CodeBlock,
                                fields: org.neo4j.cypher.internal.spi.codegen.Fields,
                                namer: org.neo4j.cypher.internal.runtime.compiled.codegen.Namer)(block: scala.Function1[org.neo4j.codegen.CodeBlock, V]): V = {
    null.asInstanceOf[V]
  }

  def tryCatch(generate: org.neo4j.codegen.CodeBlock)
              (tryBlock: scala.Function1[org.neo4j.codegen.CodeBlock, scala.Unit])
              (exception: org.neo4j.codegen.Parameter)
              (catchBlock: scala.Function1[org.neo4j.codegen.CodeBlock, scala.Unit]): scala.Unit = {
    null
  }

  def constructor(classHandle: org.neo4j.codegen.ClassHandle): org.neo4j.codegen.MethodTemplate = {
    null
  }

  def getOrLoadCursors(clazz: org.neo4j.codegen.ClassGenerator, fields: org.neo4j.cypher.internal.spi.codegen.Fields): scala.Unit = {
    null
  }

  def getOrLoadDataRead(clazz: org.neo4j.codegen.ClassGenerator, fields: org.neo4j.cypher.internal.spi.codegen.Fields): scala.Unit = {
    null
  }

  def getOrLoadTokenRead(clazz: org.neo4j.codegen.ClassGenerator, fields: org.neo4j.cypher.internal.spi.codegen.Fields): scala.Unit = {
    null
  }

  def getOrLoadSchemaRead(clazz: org.neo4j.codegen.ClassGenerator, fields: org.neo4j.cypher.internal.spi.codegen.Fields): scala.Unit = {
    null
  }

  def nodeCursor(clazz: org.neo4j.codegen.ClassGenerator, fields: org.neo4j.cypher.internal.spi.codegen.Fields): scala.Unit = {
    null
  }

  def relationshipScanCursor(clazz: org.neo4j.codegen.ClassGenerator, fields: org.neo4j.cypher.internal.spi.codegen.Fields): scala.Unit = {
    null
  }

  def closeCursors(clazz: org.neo4j.codegen.ClassGenerator, fields: org.neo4j.cypher.internal.spi.codegen.Fields): scala.Unit = {
    null
  }

  def propertyCursor(clazz: org.neo4j.codegen.ClassGenerator, fields: org.neo4j.cypher.internal.spi.codegen.Fields): scala.Unit = {
    null
  }
}
