/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.compiled.codegen

class CodeGenContext(val semanticTable: org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticTable,
                     lookup: _root_.scala.Predef.Map[String, scala.Int],
                     val namer: org.neo4j.cypher.internal.runtime.compiled.codegen.Namer = {
                       null
                     }) extends scala.AnyRef {
  val operatorIds: scala.collection.mutable.Map[String, org.neo4j.cypher.internal.v4_0.util.attribution.Id] = {
    null
  }

  def addVariable(queryVariable: String, variable: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable): scala.Unit = {
    null
  }

  def numberOfColumns(): scala.Int = {
    0
  }

  def nameToIndex(name: String): scala.Int = {
    0
  }

  def updateVariable(queryVariable: String, variable: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable): scala.Unit = {
    null
  }

  def getVariable(queryVariable: String): org.neo4j.cypher.internal.runtime.compiled.codegen.Variable = {
    null
  }

  def hasVariable(queryVariable: String): scala.Boolean = {
    false
  }

  def isProjectedVariable(queryVariable: String): scala.Boolean = {
    false
  }

  def variableQueryVariables(): _root_.scala.Predef.Set[String] = {
    null
  }

  def addProjectedVariable(queryVariable: String, variable: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable): scala.Unit = {
    null
  }

  def retainProjectedVariables(queryVariablesToRetain: _root_.scala.Predef.Set[String]): scala.Unit = {
    null
  }

  def getProjectedVariables: _root_.scala.Predef.Map[String, org.neo4j.cypher.internal.runtime.compiled.codegen.Variable] = {
    null
  }

  def addProbeTable(plan: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenPlan,
                    codeThunk: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.JoinData): scala.Unit = {
    null
  }

  def getProbeTable(plan: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenPlan): org.neo4j.cypher.internal.runtime.compiled.codegen.ir.JoinData = {
    null
  }

  def pushParent(plan: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenPlan): scala.Unit = {
    null
  }

  def popParent(): org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenPlan = {
    null
  }

  def registerOperator(plan: org.neo4j.cypher.internal.logical.plans.LogicalPlan): String = {
    null
  }
}
