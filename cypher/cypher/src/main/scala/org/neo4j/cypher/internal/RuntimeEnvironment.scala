/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal

class RuntimeEnvironment(config: org.neo4j.cypher.internal.CypherRuntimeConfiguration,
                         pipelinedQueryExecutor: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryExecutor,
                         parallelQueryExecutor: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryExecutor,
                         val tracer: org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer,
                         val cursors: org.neo4j.internal.kernel.api.CursorFactory) extends scala.AnyRef {
  def getQueryExecutor(parallelExecution: scala.Boolean): org.neo4j.cypher.internal.runtime.pipelined.execution.QueryExecutor = {
    null
  }
}

object RuntimeEnvironment extends scala.AnyRef {
  def of(config: org.neo4j.cypher.internal.CypherRuntimeConfiguration,
         jobScheduler: org.neo4j.scheduler.JobScheduler,
         cursors: org.neo4j.internal.kernel.api.CursorFactory,
         lifeSupport: org.neo4j.kernel.lifecycle.LifeSupport,
         workerManager: org.neo4j.cypher.internal.runtime.pipelined.WorkerManagement): org.neo4j.cypher.internal.RuntimeEnvironment = {
    null
  }

  def createTracer(config: org.neo4j.cypher.internal.CypherRuntimeConfiguration,
                   jobScheduler: org.neo4j.scheduler.JobScheduler,
                   lifeSupport: org.neo4j.kernel.lifecycle.LifeSupport): org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer = {
    null
  }
}
