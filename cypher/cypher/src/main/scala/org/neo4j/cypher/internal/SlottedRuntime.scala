/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal

object SlottedRuntime extends scala.AnyRef with org.neo4j.cypher.internal.CypherRuntime[org.neo4j.cypher.internal.EnterpriseRuntimeContext] with org.neo4j.cypher.internal.DebugPrettyPrinter {
  override val PRINT_QUERY_TEXT: scala.Boolean = {
    false
  }
  override val PRINT_LOGICAL_PLAN: scala.Boolean = {
    false
  }
  override val PRINT_REWRITTEN_LOGICAL_PLAN: scala.Boolean = {
    false
  }
  override val PRINT_PIPELINE_INFO: scala.Boolean = {
    false
  }
  override val PRINT_FAILURE_STACK_TRACE: scala.Boolean = {
    false
  }
  val ENABLE_DEBUG_PRINTS: scala.Boolean = {
    false
  }
  val PRINT_PLAN_INFO_EARLY: scala.Boolean = {
    false
  }

  override def name: String = {
    null
  }

  @scala.throws[org.neo4j.exceptions.CantCompileQueryException]
  override def compileToExecutable(query: org.neo4j.cypher.internal.LogicalQuery,
                                   context: org.neo4j.cypher.internal.EnterpriseRuntimeContext,
                                   securityContext: org.neo4j.internal.kernel.api.security.SecurityContext = {
                                     null
                                   }): org.neo4j.cypher.internal.ExecutionPlan = {
    null
  }
}
