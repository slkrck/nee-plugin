/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.compiled.codegen.ir.functions

sealed trait CodeGenFunction1 extends scala.AnyRef {
  def apply(arg: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression): org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression
}

case object IdCodeGenFunction extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.functions.CodeGenFunction1 with scala.Product with scala.Serializable {
  override def apply(arg: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression): org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression = {
    null
  }
}

object functionConverter extends scala.AnyRef {
  def apply(fcn: org.neo4j.cypher.internal.v4_0.expressions.FunctionInvocation,
            callback: scala.Function1[org.neo4j.cypher.internal.v4_0.expressions.Expression, org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression])
           (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression = {
    null
  }
}

case object TypeCodeGenFunction extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.functions.CodeGenFunction1 with scala.Product with scala.Serializable {
  override def apply(arg: org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression): org.neo4j.cypher.internal.runtime.compiled.codegen.ir.expressions.CodeGenExpression = {
    null
  }
}
