/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal.runtime.compiled.codegen.ir

case class ExpandAllLoopDataGenerator(val opName: String,
                                      val fromVar: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable,
                                      val dir: org.neo4j.cypher.internal.v4_0.expressions.SemanticDirection,
                                      val types: _root_.scala.Predef.Map[String, String],
                                      val toVar: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable,
                                      val relVar: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable) extends scala.AnyRef with org.neo4j.cypher.internal.runtime.compiled.codegen.ir.LoopDataGenerator with scala.Product with scala.Serializable {
  override def init[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                      (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def produceLoopData[E](cursorName: String, generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                                 (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def getNext[E](nextVar: org.neo4j.cypher.internal.runtime.compiled.codegen.Variable,
                          cursorName: String,
                          generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E])
                         (implicit context: org.neo4j.cypher.internal.runtime.compiled.codegen.CodeGenContext): scala.Unit = {
    null
  }

  override def checkNext[E](generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E], cursorName: String): E = {
    null.asInstanceOf[E]
  }

  override def close[E](cursorName: String,
                        generator: org.neo4j.cypher.internal.runtime.compiled.codegen.spi.MethodStructure[E]): scala.Unit = {
    null
  }
}
