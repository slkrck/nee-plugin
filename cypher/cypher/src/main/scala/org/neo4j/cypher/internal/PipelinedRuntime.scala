/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package org.neo4j.cypher.internal

class PipelinedRuntime private(parallelExecution: scala.Boolean,
                               override val name: String) extends scala.AnyRef with org.neo4j.cypher.internal.CypherRuntime[org.neo4j.cypher.internal.EnterpriseRuntimeContext] with org.neo4j.cypher.internal.DebugPrettyPrinter {
  override val PRINT_QUERY_TEXT: scala.Boolean = {
    false
  }
  override val PRINT_LOGICAL_PLAN: scala.Boolean = {
    false
  }
  override val PRINT_REWRITTEN_LOGICAL_PLAN: scala.Boolean = {
    false
  }
  override val PRINT_PIPELINE_INFO: scala.Boolean = {
    false
  }
  override val PRINT_FAILURE_STACK_TRACE: scala.Boolean = {
    false
  }
  val ENABLE_DEBUG_PRINTS: scala.Boolean = {
    false
  }
  val PRINT_PLAN_INFO_EARLY: scala.Boolean = {
    false
  }

  override def compileToExecutable(query: org.neo4j.cypher.internal.LogicalQuery,
                                   context: org.neo4j.cypher.internal.EnterpriseRuntimeContext,
                                   securityContext: org.neo4j.internal.kernel.api.security.SecurityContext = {
                                     null
                                   }): org.neo4j.cypher.internal.ExecutionPlan = {
    null
  }

  class PipelinedExecutionPlan(executablePipelines: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline],
                               executionGraphDefinition: org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition,
                               queryIndexes: org.neo4j.cypher.internal.runtime.QueryIndexes,
                               nExpressionSlots: scala.Int,
                               logicalPlan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
                               parameterMapping: org.neo4j.cypher.internal.runtime.ParameterMapping,
                               fieldNames: scala.Array[String],
                               queryExecutor: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryExecutor,
                               schedulerTracer: org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer,
                               batchSize: scala.Int,
                               memoryTrackingController: org.neo4j.cypher.internal.runtime.MemoryTrackingController,
                               maybeThreadSafeExecutionResources: scala.Option[scala.Tuple2[org.neo4j.internal.kernel.api.CursorFactory, org.neo4j.cypher.internal.ResourceManagerFactory]],
                               override val metadata: scala.Seq[org.neo4j.cypher.internal.plandescription.Argument],
                               warnings: _root_.scala.Predef.Set[org.neo4j.cypher.internal.v4_0.util.InternalNotification],
                               executionGraphSchedulingPolicy: org.neo4j.cypher.internal.runtime.pipelined.execution.ExecutionGraphSchedulingPolicy) extends org.neo4j.cypher.internal.ExecutionPlan {
    override def run(queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                     executionMode: org.neo4j.cypher.internal.runtime.ExecutionMode,
                     params: org.neo4j.values.virtual.MapValue,
                     prePopulateResults: scala.Boolean,
                     inputDataStream: org.neo4j.cypher.internal.runtime.InputDataStream,
                     subscriber: org.neo4j.kernel.impl.query.QuerySubscriber): org.neo4j.cypher.result.RuntimeResult = {
      null
    }

    override def runtimeName: org.neo4j.cypher.internal.RuntimeName = {
      null
    }

    override def notifications: _root_.scala.Predef.Set[org.neo4j.cypher.internal.v4_0.util.InternalNotification] = {
      null
    }

    override def threadSafeExecutionResources(): scala.Option[scala.Tuple2[org.neo4j.internal.kernel.api.CursorFactory, org.neo4j.cypher.internal.ResourceManagerFactory]] = {
      null
    }
  }

  class PipelinedRuntimeResult(executablePipelines: scala.IndexedSeq[org.neo4j.cypher.internal.runtime.pipelined.ExecutablePipeline],
                               executionGraphDefinition: org.neo4j.cypher.internal.physicalplanning.ExecutionGraphDefinition,
                               queryIndexes: scala.Array[org.neo4j.internal.kernel.api.IndexReadSession],
                               nExpressionSlots: scala.Int,
                               prePopulateResults: scala.Boolean,
                               inputDataStream: org.neo4j.cypher.internal.runtime.InputDataStream,
                               logicalPlan: org.neo4j.cypher.internal.logical.plans.LogicalPlan,
                               queryContext: org.neo4j.cypher.internal.runtime.QueryContext,
                               params: scala.Array[org.neo4j.values.AnyValue],
                               override val fieldNames: scala.Array[String],
                               queryExecutor: org.neo4j.cypher.internal.runtime.pipelined.execution.QueryExecutor,
                               schedulerTracer: org.neo4j.cypher.internal.runtime.pipelined.tracing.SchedulerTracer,
                               subscriber: org.neo4j.kernel.impl.query.QuerySubscriber,
                               doProfile: scala.Boolean,
                               batchSize: scala.Int,
                               memoryTracking: org.neo4j.cypher.internal.runtime.MemoryTracking,
                               executionGraphSchedulingPolicy: org.neo4j.cypher.internal.runtime.pipelined.execution.ExecutionGraphSchedulingPolicy) extends java.lang.Object with org.neo4j.cypher.result.RuntimeResult {
    override def queryStatistics(): org.neo4j.cypher.internal.runtime.QueryStatistics = {
      null
    }

    override def totalAllocatedMemory(): java.util.Optional[java.lang.Long] = {
      null
    }

    override def consumptionState(): org.neo4j.cypher.result.RuntimeResult.ConsumptionState = {
      null
    }

    override def close(): scala.Unit = {
      null
    }

    override def queryProfile(): org.neo4j.cypher.result.QueryProfile = {
      null
    }

    override def request(numberOfRecords: scala.Long): scala.Unit = {
      null
    }

    override def cancel(): scala.Unit = {
      null
    }

    override def await(): scala.Boolean = {
      false
    }
  }

}

object PipelinedRuntime extends scala.AnyRef {
  val PIPELINED: org.neo4j.cypher.internal.PipelinedRuntime = {
    null
  }
  val PARALLEL: org.neo4j.cypher.internal.PipelinedRuntime = {
    null
  }
  val CODE_GEN_FAILED_MESSAGE: java.lang.String = {
    null
  }
}
