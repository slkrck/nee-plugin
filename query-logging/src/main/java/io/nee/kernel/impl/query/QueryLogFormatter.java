/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.query;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import org.neo4j.internal.helpers.Strings;
import org.neo4j.kernel.api.query.QuerySnapshot;
import org.neo4j.values.AnyValue;
import org.neo4j.values.utils.PrettyPrinter;
import org.neo4j.values.virtual.MapValue;

class QueryLogFormatter {

  private QueryLogFormatter() {
  }

  static void formatPageDetails(StringBuilder result, QuerySnapshot query) {
    result.append(query.pageHits()).append(" page hits, ");
    result.append(query.pageFaults()).append(" page faults - ");
  }

  static void formatAllocatedBytes(StringBuilder result, QuerySnapshot query) {
    Optional<Long> bytes = query.allocatedBytes();
    bytes.ifPresent((x) ->
    {
      result.append(x).append(" B - ");
    });
  }

  static void formatDetailedTime(StringBuilder result, QuerySnapshot query) {
    result.append("(planning: ")
        .append(TimeUnit.MICROSECONDS.toMillis(query.compilationTimeMicros()));
    Long cpuTime = query.cpuTimeMicros();
    if (cpuTime != null) {
      result.append(", cpu: ").append(TimeUnit.MICROSECONDS.toMillis(cpuTime));
    }

    result.append(", waiting: ").append(TimeUnit.MICROSECONDS.toMillis(query.waitTimeMicros()));
    result.append(") - ");
  }

  static void formatMapValue(StringBuilder result, MapValue params) {
    result.append('{');
    if (params != null) {
      String[] sep = new String[]{""};
      params.foreach((key, value) ->
      {
        result.append(sep[0]).append(key).append(": ");
        result.append(formatAnyValue(value));
        sep[0] = ", ";
      });
    }

    result.append("}");
  }

  private static String formatAnyValue(AnyValue value) {
    PrettyPrinter printer = new PrettyPrinter("'");
    value.writeTo(printer);
    return printer.value();
  }

  static void formatMap(StringBuilder result, Map<String, Object> params) {
    formatMap(result, params, Collections.emptySet());
  }

  private static void formatMap(StringBuilder result, Map<String, Object> params,
      Collection<String> obfuscate) {
    result.append('{');
    if (params != null) {
      String sep = "";

      for (Iterator n4 = params.entrySet().iterator(); n4.hasNext(); sep = ", ") {
        Entry<String, Object> entry = (Entry) n4.next();
        result.append(sep).append(entry.getKey()).append(": ");
        if (obfuscate.contains(entry.getKey())) {
          result.append("******");
        } else {
          formatValue(result, entry.getValue());
        }
      }
    }

    result.append("}");
  }

  private static void formatValue(StringBuilder result, Object value) {
    if (value instanceof Map) {
      formatMap(result, (Map) value, Collections.emptySet());
    } else if (value instanceof String) {
      result.append('\'').append(value).append('\'');
    } else {
      result.append(Strings.prettyPrint(value));
    }
  }
}
