/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.query;

import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.extension.ExtensionFactory;
import org.neo4j.kernel.extension.ExtensionType;
import org.neo4j.kernel.extension.context.ExtensionContext;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;

public class QueryLoggerExtension extends ExtensionFactory<QueryLoggerExtension.Dependencies> {

  public QueryLoggerExtension() {
    super(ExtensionType.GLOBAL, "query-logging");
  }

  public Lifecycle newInstance(ExtensionContext context,
      QueryLoggerExtension.Dependencies dependencies) {
    final FileSystemAbstraction fileSystem = dependencies.fileSystem();
    final Config config = dependencies.config();
    final Monitors monitoring = dependencies.monitoring();
    final LogService logService = dependencies.logger();
    final JobScheduler jobScheduler = dependencies.jobScheduler();
    return new LifecycleAdapter() {
      DynamicLoggingQueryExecutionMonitor logger;

      public void init() {
        Log debugLog = logService.getInternalLog(DynamicLoggingQueryExecutionMonitor.class);
        this.logger = new DynamicLoggingQueryExecutionMonitor(config, fileSystem, jobScheduler,
            debugLog);
        this.logger.init();
        monitoring.addMonitorListener(this.logger);
      }

      public void shutdown() {
        this.logger.shutdown();
      }
    };
  }

  public interface Dependencies {

    FileSystemAbstraction fileSystem();

    Config config();

    Monitors monitoring();

    LogService logger();

    JobScheduler jobScheduler();
  }
}
