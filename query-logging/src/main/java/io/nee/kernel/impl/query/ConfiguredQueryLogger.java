/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.query;

import java.util.concurrent.TimeUnit;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.GraphDatabaseSettings.LogQueryLevel;
import org.neo4j.kernel.api.query.ExecutingQuery;
import org.neo4j.kernel.api.query.QuerySnapshot;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;

class ConfiguredQueryLogger implements QueryLogger {

  private final Log log;
  private final long thresholdMillis;
  private final boolean logQueryParameters;
  private final boolean logDetailedTime;
  private final boolean logAllocatedBytes;
  private final boolean logPageDetails;
  private final boolean logRuntime;
  private final boolean verboseLogging;

  ConfiguredQueryLogger(Log log, Config config) {
    this.log = log;
    this.thresholdMillis = config.get(GraphDatabaseSettings.log_queries_threshold).toMillis();
    this.logQueryParameters = config
        .get(GraphDatabaseSettings.log_queries_parameter_logging_enabled);
    this.logDetailedTime = config
        .get(GraphDatabaseSettings.log_queries_detailed_time_logging_enabled);
    this.logAllocatedBytes = config
        .get(GraphDatabaseSettings.log_queries_allocation_logging_enabled);
    this.logPageDetails = config.get(GraphDatabaseSettings.log_queries_page_detail_logging_enabled);
    this.logRuntime = config.get(GraphDatabaseSettings.log_queries_runtime_logging_enabled);
    this.verboseLogging = config.get(GraphDatabaseSettings.log_queries) == LogQueryLevel.VERBOSE;
  }

  public void start(ExecutingQuery query) {
    if (this.verboseLogging) {
      Log n10000 = this.log;
      String n10001 = this.logEntry(query.snapshot());
      n10000.info("Query started: " + n10001);
    }
  }

  public void failure(ExecutingQuery query, Throwable failure) {
    this.log.error(this.logEntry(query.snapshot()), failure);
  }

  public void failure(ExecutingQuery query, String reason) {
    this.log.error(this.logEntry(query.snapshot(), reason));
  }

  public void success(ExecutingQuery query) {
    if (TimeUnit.NANOSECONDS.toMillis(query.elapsedNanos()) >= this.thresholdMillis
        || this.verboseLogging) {
      this.log.info(this.logEntry(query.snapshot()));
    }
  }

  private String logEntry(QuerySnapshot query, String reason) {
    String n10000 = this.logEntry(query);
    return n10000 + " - " + reason.split(System.getProperty("line.separator"))[0];
  }

  private String logEntry(QuerySnapshot query) {
    String sourceString = query.clientConnection().asConnectionDetails();
    String username = query.username();
    NamedDatabaseId namedDatabaseId = query.databaseId();
    String queryText = query.queryText();
    StringBuilder result = new StringBuilder();
    if (this.verboseLogging) {
      result.append("id:").append(query.id()).append(" - ");
    }

    result.append(TimeUnit.MICROSECONDS.toMillis(query.elapsedTimeMicros())).append(" ms: ");
    if (this.logDetailedTime) {
      QueryLogFormatter.formatDetailedTime(result, query);
    }

    if (this.logAllocatedBytes) {
      QueryLogFormatter.formatAllocatedBytes(result, query);
    }

    if (this.logPageDetails) {
      QueryLogFormatter.formatPageDetails(result, query);
    }

    result.append(sourceString).append("\t").append(namedDatabaseId.name()).append(" - ")
        .append(username).append(" - ").append(queryText);
    if (this.logQueryParameters) {
      QueryLogFormatter.formatMapValue(result.append(" - "), query.queryParameters());
    }

    if (this.logRuntime) {
      result.append(" - runtime=").append(query.runtime());
    }

    QueryLogFormatter.formatMap(result.append(" - "), query.transactionAnnotationData());
    return result.toString();
  }
}
