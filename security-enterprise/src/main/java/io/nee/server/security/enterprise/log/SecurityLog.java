/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.log;

import io.nee.server.security.enterprise.configuration.SecuritySettings;
import java.io.File;
import java.io.IOException;
import java.time.ZoneId;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.internal.helpers.Strings;
import org.neo4j.internal.kernel.api.security.AuthSubject;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.FormattedLog;
import org.neo4j.logging.FormattedLog.Builder;
import org.neo4j.logging.Log;
import org.neo4j.logging.Logger;
import org.neo4j.logging.RotatingFileOutputStreamSupplier;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public class SecurityLog extends LifecycleAdapter implements Log {

  private final Log inner;
  private RotatingFileOutputStreamSupplier rotatingSupplier;

  public SecurityLog(Config config, FileSystemAbstraction fileSystem, Executor executor)
      throws IOException {
    ZoneId logTimeZoneId = config.get(GraphDatabaseSettings.db_timezone).getZoneId();
    File logFile = config.get(SecuritySettings.security_log_filename).toFile();
    Builder builder = FormattedLog.withZoneId(logTimeZoneId);
    this.rotatingSupplier =
        new RotatingFileOutputStreamSupplier(fileSystem, logFile,
            config.get(SecuritySettings.store_security_log_rotation_threshold),
            config.get(SecuritySettings.store_security_log_rotation_delay).toMillis(),
            config.get(SecuritySettings.store_security_log_max_archives), executor);
    FormattedLog formattedLog = builder.toOutputStream(this.rotatingSupplier);
    formattedLog.setLevel(config.get(SecuritySettings.security_log_level));
    this.inner = formattedLog;
  }

  public SecurityLog(Log log) {
    this.inner = log;
  }

  private static String withSubject(AuthSubject subject, String msg) {
    String n10000 = Strings.escape(subject.username());
    return "[" + n10000 + "]: " + msg;
  }

  public static SecurityLog create(Config config, FileSystemAbstraction fileSystem,
      JobScheduler jobScheduler) throws IOException {
    return new SecurityLog(config, fileSystem, jobScheduler.executor(Group.LOG_ROTATION));
  }

  public boolean isDebugEnabled() {
    return this.inner.isDebugEnabled();
  }

  public Logger debugLogger() {
    return this.inner.debugLogger();
  }

  public void debug(String message) {
    this.inner.debug(message);
  }

  public void debug(String message, Throwable throwable) {
    this.inner.debug(message, throwable);
  }

  public void debug(String format, Object... arguments) {
    this.inner.debug(format, arguments);
  }

  public void debug(AuthSubject subject, String format, Object... arguments) {
    this.inner.debug(withSubject(subject, format), arguments);
  }

  public Logger infoLogger() {
    return this.inner.infoLogger();
  }

  public void info(String message) {
    this.inner.info(message);
  }

  public void info(String message, Throwable throwable) {
    this.inner.info(message, throwable);
  }

  public void info(String format, Object... arguments) {
    this.inner.info(format, arguments);
  }

  public void info(AuthSubject subject, String format, Object... arguments) {
    this.inner.info(withSubject(subject, format), arguments);
  }

  public void info(AuthSubject subject, String format) {
    this.inner.info(withSubject(subject, format));
  }

  public Logger warnLogger() {
    return this.inner.warnLogger();
  }

  public void warn(String message) {
    this.inner.warn(message);
  }

  public void warn(String message, Throwable throwable) {
    this.inner.warn(message, throwable);
  }

  public void warn(String format, Object... arguments) {
    this.inner.warn(format, arguments);
  }

  public void warn(AuthSubject subject, String format, Object... arguments) {
    this.inner.warn(withSubject(subject, format), arguments);
  }

  public Logger errorLogger() {
    return this.inner.errorLogger();
  }

  public void error(String message) {
    this.inner.error(message);
  }

  public void error(String message, Throwable throwable) {
    this.inner.error(message, throwable);
  }

  public void error(String format, Object... arguments) {
    this.inner.error(format, arguments);
  }

  public void error(AuthSubject subject, String format, Object... arguments) {
    this.inner.error(withSubject(subject, format), arguments);
  }

  public void bulk(Consumer<Log> consumer) {
    this.inner.bulk(consumer);
  }

  public void shutdown() throws Exception {
    if (this.rotatingSupplier != null) {
      this.rotatingSupplier.close();
      this.rotatingSupplier = null;
    }
  }
}
