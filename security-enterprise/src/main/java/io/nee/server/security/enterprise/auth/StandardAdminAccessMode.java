/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import io.nee.kernel.enterprise.api.security.AdminAccessMode;
import java.util.HashSet;
import java.util.Iterator;
import org.neo4j.internal.kernel.api.security.AdminActionOnResource;
import org.neo4j.internal.kernel.api.security.AdminActionOnResource.DatabaseScope;
import org.neo4j.internal.kernel.api.security.PrivilegeAction;

public class StandardAdminAccessMode implements AdminAccessMode {

  private final HashSet<AdminActionOnResource> whitelist;
  private final HashSet<AdminActionOnResource> blacklist;

  StandardAdminAccessMode(HashSet<AdminActionOnResource> whitelist,
      HashSet<AdminActionOnResource> blacklist) {
    this.whitelist = whitelist;
    this.blacklist = blacklist;
  }

  public static boolean matches(HashSet<AdminActionOnResource> actions,
      AdminActionOnResource action) {
    Iterator n2 = actions.iterator();

    AdminActionOnResource rule;
    do {
      if (!n2.hasNext()) {
        return false;
      }

      rule = (AdminActionOnResource) n2.next();
    }
    while (!rule.matches(action));

    return true;
  }

  public boolean allows(AdminActionOnResource action) {
    return matches(this.whitelist, action) && !matches(this.blacklist, action);
  }

  public static class Builder {

    HashSet<AdminActionOnResource> whitelist = new HashSet();
    HashSet<AdminActionOnResource> blacklist = new HashSet();

    public StandardAdminAccessMode.Builder full() {
      PrivilegeAction[] n1 = PrivilegeAction.values();
      int n2 = n1.length;

      for (int n3 = 0; n3 < n2; ++n3) {
        PrivilegeAction a = n1[n3];
        this.whitelist.add(new AdminActionOnResource(a, DatabaseScope.ALL));
      }

      return this;
    }

    public StandardAdminAccessMode.Builder allow(AdminActionOnResource action) {
      this.whitelist.add(action);
      return this;
    }

    public StandardAdminAccessMode.Builder deny(AdminActionOnResource action) {
      this.blacklist.add(action);
      return this;
    }

    public StandardAdminAccessMode build() {
      return new StandardAdminAccessMode(this.whitelist, this.blacklist);
    }
  }
}
