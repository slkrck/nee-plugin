/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

public class LabelSegment implements Segment {

  public static final LabelSegment ALL = new LabelSegment(null);
  private final String label;

  public LabelSegment(String label) {
    this.label = label;
  }

  public String getLabel() {
    return this.label;
  }

  public int hashCode() {
    return this.label.hashCode();
  }

  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj instanceof LabelSegment) {
      LabelSegment other = (LabelSegment) obj;
      if (this.label == null) {
        return other.label == null;
      } else {
        return this.label.equals(other.getLabel());
      }
    } else {
      return false;
    }
  }

  public String toString() {
    return this.label == null ? "All labels" : "label: '" + this.label + "'";
  }
}
