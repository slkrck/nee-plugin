/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.neo4j.internal.helpers.collection.MapUtil;
import org.neo4j.kernel.api.exceptions.InvalidArgumentsException;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.server.security.auth.ListSnapshot;
import org.neo4j.server.security.auth.exception.ConcurrentModificationException;

public abstract class AbstractRoleRepository extends LifecycleAdapter implements RoleRepository {

  private final Map<String, RoleRecord> rolesByName = new ConcurrentHashMap();
  private final Map<String, SortedSet<String>> rolesByUsername = new ConcurrentHashMap();
  private final Pattern roleNamePattern = Pattern.compile("^[a-zA-Z0-9_]+$");
  protected volatile List<RoleRecord> roles = new ArrayList();
  protected AtomicLong lastLoaded = new AtomicLong(0L);

  public void clear() {
    this.roles.clear();
    this.rolesByName.clear();
    this.rolesByUsername.clear();
  }

  public RoleRecord getRoleByName(String roleName) {
    return roleName == null ? null : this.rolesByName.get(roleName);
  }

  public Set<String> getRoleNamesByUsername(String username) {
    Set<String> roleNames = this.rolesByUsername.get(username);
    return roleNames != null ? roleNames : Collections.emptySet();
  }

  public void create(RoleRecord role) throws InvalidArgumentsException, IOException {
    this.assertValidRoleName(role.name());
    synchronized (this) {
      Iterator n3 = this.roles.iterator();

      RoleRecord other;
      do {
        if (!n3.hasNext()) {
          this.roles.add(role);
          this.persistRoles();
          this.rolesByName.put(role.name(), role);
          this.populateUserMap(role);
          return;
        }

        other = (RoleRecord) n3.next();
      }
      while (!other.name().equals(role.name()));

      throw new InvalidArgumentsException(
          "The specified role '" + role.name() + "' already exists.");
    }
  }

  public void setRoles(ListSnapshot<RoleRecord> rolesSnapshot) throws InvalidArgumentsException {
    Iterator n2 = rolesSnapshot.values().iterator();

    while (n2.hasNext()) {
      RoleRecord role = (RoleRecord) n2.next();
      this.assertValidRoleName(role.name());
    }

    synchronized (this) {
      this.roles.clear();
      this.roles.addAll(rolesSnapshot.values());
      this.lastLoaded.set(rolesSnapshot.timestamp());
      MapUtil.trimToList(this.rolesByName, this.roles, RoleRecord::name);
      MapUtil.trimToFlattenedList(this.rolesByUsername, this.roles, (r) ->
      {
        return r.users().stream();
      });
      Iterator n7 = this.roles.iterator();

      while (n7.hasNext()) {
        RoleRecord role = (RoleRecord) n7.next();
        this.rolesByName.put(role.name(), role);
        this.populateUserMap(role);
      }
    }
  }

  public void update(RoleRecord existingRole, RoleRecord updatedRole)
      throws ConcurrentModificationException, IOException {
    if (!existingRole.name().equals(updatedRole.name())) {
      String n10002 = existingRole.name();
      throw new IllegalArgumentException(
          "The attempt to update the role from '" + n10002 + "' to '" + updatedRole.name()
              + "' failed. Changing a roles name is not allowed.");
    } else {
      synchronized (this) {
        List<RoleRecord> newRoles = new ArrayList();
        boolean foundRole = false;
        Iterator n6 = this.roles.iterator();

        while (n6.hasNext()) {
          RoleRecord other = (RoleRecord) n6.next();
          if (other.equals(existingRole)) {
            foundRole = true;
            newRoles.add(updatedRole);
          } else {
            newRoles.add(other);
          }
        }

        if (!foundRole) {
          throw new ConcurrentModificationException();
        } else {
          this.roles = newRoles;
          this.persistRoles();
          this.rolesByName.put(updatedRole.name(), updatedRole);
          this.removeFromUserMap(existingRole);
          this.populateUserMap(updatedRole);
        }
      }
    }
  }

  public synchronized boolean delete(RoleRecord role) throws IOException {
    boolean foundRole = false;
    List<RoleRecord> newRoles = new ArrayList();
    Iterator n4 = this.roles.iterator();

    while (n4.hasNext()) {
      RoleRecord other = (RoleRecord) n4.next();
      if (other.name().equals(role.name())) {
        foundRole = true;
      } else {
        newRoles.add(other);
      }
    }

    if (foundRole) {
      this.roles = newRoles;
      this.persistRoles();
      this.rolesByName.remove(role.name());
    }

    this.removeFromUserMap(role);
    return foundRole;
  }

  public synchronized int numberOfRoles() {
    return this.roles.size();
  }

  public void assertValidRoleName(String name) throws InvalidArgumentsException {
    if (name != null && !name.isEmpty()) {
      if (!this.roleNamePattern.matcher(name).matches()) {
        throw new InvalidArgumentsException("Role name '" + name
            + "' contains illegal characters. Use simple ascii characters and numbers.");
      }
    } else {
      throw new InvalidArgumentsException("The provided role name is empty.");
    }
  }

  public synchronized void removeUserFromAllRoles(String username)
      throws ConcurrentModificationException, IOException {
    Set<String> roles = this.rolesByUsername.get(username);
    if (roles != null) {
      List<String> rolesToRemoveFrom = new ArrayList(roles);
      Iterator n4 = rolesToRemoveFrom.iterator();

      while (n4.hasNext()) {
        String roleName = (String) n4.next();
        RoleRecord role = this.rolesByName.get(roleName);
        RoleRecord newRole = role.augment().withoutUser(username).build();
        this.update(role, newRole);
      }
    }
  }

  public synchronized Set<String> getAllRoleNames() {
    return this.roles.stream().map(RoleRecord::name).collect(Collectors.toSet());
  }

  public void purge() throws IOException {
    this.clear();
  }

  public void markAsMigrated() throws IOException {
    this.clear();
  }

  protected abstract void persistRoles() throws IOException;

  protected abstract ListSnapshot<RoleRecord> readPersistedRoles() throws IOException;

  protected void populateUserMap(RoleRecord role) {
    Iterator n2 = role.users().iterator();

    while (n2.hasNext()) {
      String username = (String) n2.next();
      SortedSet<String> memberOfRoles = this.rolesByUsername.computeIfAbsent(username, (k) ->
      {
        return new ConcurrentSkipListSet();
      });
      memberOfRoles.add(role.name());
    }
  }

  protected void removeFromUserMap(RoleRecord role) {
    Iterator n2 = role.users().iterator();

    while (n2.hasNext()) {
      String username = (String) n2.next();
      SortedSet<String> memberOfRoles = this.rolesByUsername.get(username);
      if (memberOfRoles != null) {
        memberOfRoles.remove(role.name());
      }
    }
  }
}
