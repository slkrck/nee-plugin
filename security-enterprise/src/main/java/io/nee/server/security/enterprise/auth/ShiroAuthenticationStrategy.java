/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import java.util.Collection;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.pam.AbstractAuthenticationStrategy;
import org.apache.shiro.realm.Realm;
import org.neo4j.server.security.auth.ShiroAuthenticationInfo;

public class ShiroAuthenticationStrategy extends AbstractAuthenticationStrategy {

  public AuthenticationInfo beforeAllAttempts(Collection<? extends Realm> realms,
      AuthenticationToken token) throws AuthenticationException {
    return new ShiroAuthenticationInfo();
  }

  public AuthenticationInfo afterAttempt(Realm realm, AuthenticationToken token,
      AuthenticationInfo singleRealmInfo, AuthenticationInfo aggregateInfo,
      Throwable t) throws AuthenticationException {
    AuthenticationInfo info = super.afterAttempt(realm, token, singleRealmInfo, aggregateInfo, t);
    if (t != null && info instanceof ShiroAuthenticationInfo) {
      ((ShiroAuthenticationInfo) info).addThrowable(t);
    }

    return info;
  }
}
