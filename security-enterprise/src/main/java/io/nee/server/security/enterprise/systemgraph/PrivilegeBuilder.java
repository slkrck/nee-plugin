/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.systemgraph;

import io.nee.server.security.enterprise.auth.DatabaseSegment;
import io.nee.server.security.enterprise.auth.LabelSegment;
import io.nee.server.security.enterprise.auth.RelTypeSegment;
import io.nee.server.security.enterprise.auth.Resource;
import io.nee.server.security.enterprise.auth.ResourcePrivilege;
import io.nee.server.security.enterprise.auth.Segment;
import java.util.NoSuchElementException;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.internal.helpers.collection.Iterables;
import org.neo4j.internal.kernel.api.security.PrivilegeAction;
import org.neo4j.kernel.api.exceptions.InvalidArgumentsException;

class PrivilegeBuilder {

  private final ResourcePrivilege.GrantOrDeny privilegeType;
  private final PrivilegeAction action;
  private Segment segment;
  private Resource resource;
  private String dbName = "";
  private boolean allDatabases;

  PrivilegeBuilder(ResourcePrivilege.GrantOrDeny privilegeType, String action) {
    this.privilegeType = privilegeType;
    this.action = PrivilegeAction.valueOf(action.toUpperCase());
  }

  PrivilegeBuilder forAllDatabases() {
    this.allDatabases = true;
    return this;
  }

  PrivilegeBuilder forDatabase(String database) {
    this.dbName = database;
    return this;
  }

  PrivilegeBuilder withinScope(Node qualifierNode) {
    Label qualifierType;
    try {
      qualifierType = Iterables.single(qualifierNode.getLabels());
    } catch (NoSuchElementException n7) {
      throw new IllegalStateException(
          "Privilege segments require qualifier nodes with exactly one label. " + n7.getMessage());
    }

    String n3 = qualifierType.name();
    byte n4 = -1;
    switch (n3.hashCode()) {
      case -1940868337:
        if (n3.equals("RelationshipQualifierAll")) {
          n4 = 4;
        }
        break;
      case -1930519157:
        if (n3.equals("LabelQualifierAll")) {
          n4 = 2;
        }
        break;
      case -619202225:
        if (n3.equals("DatabaseQualifier")) {
          n4 = 0;
        }
        break;
      case 608332082:
        if (n3.equals("RelationshipQualifier")) {
          n4 = 3;
        }
        break;
      case 1506078774:
        if (n3.equals("LabelQualifier")) {
          n4 = 1;
        }
    }

    switch (n4) {
      case 0:
        this.segment = DatabaseSegment.ALL;
        break;
      case 1:
        String label = qualifierNode.getProperty("label").toString();
        this.segment = new LabelSegment(label);
        break;
      case 2:
        this.segment = LabelSegment.ALL;
        break;
      case 3:
        String relType = qualifierNode.getProperty("label").toString();
        this.segment = new RelTypeSegment(relType);
        break;
      case 4:
        this.segment = RelTypeSegment.ALL;
        break;
      default:
        throw new IllegalArgumentException(
            "Unknown privilege qualifier type: " + qualifierType.name());
    }

    return this;
  }

  PrivilegeBuilder onResource(Node resourceNode) throws InvalidArgumentsException {
    String type = resourceNode.getProperty("type").toString();
    Resource.Type resourceType = this.asResourceType(type);
    switch (resourceType) {
      case DATABASE:
        this.resource = new Resource.DatabaseResource();
        break;
      case GRAPH:
        this.resource = new Resource.GraphResource();
        break;
      case PROPERTY:
        String propertyKey = resourceNode.getProperty("arg1").toString();
        this.resource = new Resource.PropertyResource(propertyKey);
        break;
      case ALL_PROPERTIES:
        this.resource = new Resource.AllPropertiesResource();
        break;
      case PROCEDURE:
        String namespace = resourceNode.getProperty("arg1").toString();
        String procedureName = resourceNode.getProperty("arg2").toString();
        this.resource = new Resource.ProcedureResource(namespace, procedureName);
        break;
      default:
        throw new IllegalArgumentException("Unknown resourceType: " + resourceType);
    }

    return this;
  }

  private Resource.Type asResourceType(String typeString) throws InvalidArgumentsException {
    try {
      return Resource.Type.valueOf(typeString.toUpperCase());
    } catch (IllegalArgumentException n3) {
      throw new InvalidArgumentsException(
          String.format("Found not valid resource (%s) in the system graph.", typeString));
    }
  }

  ResourcePrivilege build() throws InvalidArgumentsException {
    return this.allDatabases ? new ResourcePrivilege(this.privilegeType, this.action, this.resource,
        this.segment)
        : new ResourcePrivilege(this.privilegeType, this.action, this.resource, this.segment,
            this.dbName);
  }
}
