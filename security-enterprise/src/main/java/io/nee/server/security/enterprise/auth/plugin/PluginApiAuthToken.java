/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth.plugin;

import io.nee.server.security.enterprise.auth.plugin.api.AuthToken;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import org.neo4j.kernel.api.security.exception.InvalidAuthTokenException;

public class PluginApiAuthToken implements AuthToken {

  private final String principal;
  private final char[] credentials;
  private final Map<String, Object> parameters;

  private PluginApiAuthToken(String principal, char[] credentials, Map<String, Object> parameters) {
    this.principal = principal;
    this.credentials = credentials;
    this.parameters = parameters;
  }

  public static PluginApiAuthToken of(String principal, char[] credentials,
      Map<String, Object> parameters) {
    return new PluginApiAuthToken(principal, credentials, parameters);
  }

  public static PluginApiAuthToken createFromMap(Map<String, Object> authTokenMap)
      throws InvalidAuthTokenException {
    String scheme = org.neo4j.kernel.api.security.AuthToken.safeCast("scheme", authTokenMap);
    String principal = org.neo4j.kernel.api.security.AuthToken.safeCast("principal", authTokenMap);
    byte[] credentials = null;
    if (scheme.equals("basic")) {
      credentials = org.neo4j.kernel.api.security.AuthToken
          .safeCastCredentials("credentials", authTokenMap);
    } else {
      Object credentialsObject = authTokenMap.get("credentials");
      if (credentialsObject instanceof byte[]) {
        credentials = (byte[]) credentialsObject;
      }
    }

    Map<String, Object> parameters = org.neo4j.kernel.api.security.AuthToken
        .safeCastMap("parameters", authTokenMap);
    return of(principal,
        credentials != null ? StandardCharsets.UTF_8.decode(ByteBuffer.wrap(credentials)).array()
            : null, parameters);
  }

  public String principal() {
    return this.principal;
  }

  public char[] credentials() {
    return this.credentials;
  }

  public Map<String, Object> parameters() {
    return this.parameters;
  }

  void clearCredentials() {
    if (this.credentials != null) {
      Arrays.fill(this.credentials, '\u0000');
    }
  }
}
