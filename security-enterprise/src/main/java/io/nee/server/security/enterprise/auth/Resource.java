/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import org.neo4j.internal.kernel.api.security.PrivilegeAction;
import org.neo4j.kernel.api.exceptions.InvalidArgumentsException;

public interface Resource {

  void assertValidCombination(PrivilegeAction n1) throws InvalidArgumentsException;

  default String getArg1() {
    return "";
  }

  default String getArg2() {
    return "";
  }

  Resource.Type type();

  enum Type {
    ALL_PROPERTIES,
    PROPERTY,
    GRAPH,
    DATABASE,
    PROCEDURE;

    public String toString() {
      return super.toString().toLowerCase();
    }
  }

  class ProcedureResource implements Resource {

    private final String nameSpace;
    private final String procedure;

    public ProcedureResource(String nameSpace, String procedure) {
      this.nameSpace = nameSpace;
      this.procedure = procedure;
    }

    public void assertValidCombination(PrivilegeAction action) throws InvalidArgumentsException {
      if (!action.equals(PrivilegeAction.EXECUTE)) {
        throw new InvalidArgumentsException(String
            .format("Procedure resource cannot be combined with action '%s'", action.toString()));
      }
    }

    public String getArg1() {
      return this.nameSpace == null ? "" : this.nameSpace;
    }

    public String getArg2() {
      return this.procedure == null ? "" : this.procedure;
    }

    public Resource.Type type() {
      return Resource.Type.PROCEDURE;
    }

    public String toString() {
      return "library " + this.nameSpace + " procedure " + this.procedure;
    }

    public int hashCode() {
      int hash = this.nameSpace == null ? 0 : this.nameSpace.hashCode();
      return hash + 31 * (this.procedure == null ? 0 : this.procedure.hashCode());
    }

    public boolean equals(Object obj) {
      if (obj == this) {
        return true;
      } else if (!(obj instanceof Resource.ProcedureResource)) {
        return false;
      } else {
        Resource.ProcedureResource other = (Resource.ProcedureResource) obj;
        boolean equals = other.nameSpace == null && this.nameSpace == null
            || other.nameSpace != null && other.nameSpace.equals(this.nameSpace);
        return equals && (other.procedure == null && this.procedure == null
            || other.procedure != null && other.procedure.equals(this.procedure));
      }
    }
  }

  class PropertyResource implements Resource {

    private final String property;

    public PropertyResource(String property) {
      this.property = property;
    }

    public void assertValidCombination(PrivilegeAction action) throws InvalidArgumentsException {
      if (!action.equals(PrivilegeAction.WRITE) && !action.equals(PrivilegeAction.READ)) {
        throw new InvalidArgumentsException(String
            .format("Property resource cannot be combined with action `%s`", action.toString()));
      }
    }

    public String getArg1() {
      return this.property == null ? "" : this.property;
    }

    public Resource.Type type() {
      return Resource.Type.PROPERTY;
    }

    public String toString() {
      return "property " + this.property;
    }

    public int hashCode() {
      return this.property == null ? 0
          : this.property.hashCode() + 31 * Resource.Type.PROPERTY.hashCode();
    }

    public boolean equals(Object obj) {
      if (obj == this) {
        return true;
      } else if (!(obj instanceof Resource.PropertyResource)) {
        return false;
      } else {
        Resource.PropertyResource other = (Resource.PropertyResource) obj;
        return this.property == null && other.property == null
            || this.property != null && this.property.equals(other.property);
      }
    }
  }

  class AllPropertiesResource implements Resource {

    public void assertValidCombination(PrivilegeAction action) throws InvalidArgumentsException {
      if (!action.equals(PrivilegeAction.WRITE) && !action.equals(PrivilegeAction.READ)) {
        throw new InvalidArgumentsException(String
            .format("Property resource cannot be combined with action `%s`", action.toString()));
      }
    }

    public Resource.Type type() {
      return Resource.Type.ALL_PROPERTIES;
    }

    public String toString() {
      return "all properties ";
    }

    public int hashCode() {
      return Resource.Type.ALL_PROPERTIES.hashCode();
    }

    public boolean equals(Object obj) {
      return obj == this || obj instanceof Resource.AllPropertiesResource;
    }
  }

  class DatabaseResource implements Resource {

    public void assertValidCombination(PrivilegeAction action) throws InvalidArgumentsException {
      if (!action.equals(PrivilegeAction.ACCESS) && !PrivilegeAction.SCHEMA.satisfies(action)
          && !PrivilegeAction.TOKEN.satisfies(action) &&
          !PrivilegeAction.ADMIN.satisfies(action)) {
        throw new InvalidArgumentsException(String
            .format("Database resource cannot be combined with action '%s'", action.toString()));
      }
    }

    public Resource.Type type() {
      return Resource.Type.DATABASE;
    }

    public String toString() {
      return "database";
    }

    public int hashCode() {
      return Resource.Type.DATABASE.hashCode();
    }

    public boolean equals(Object obj) {
      return obj instanceof Resource.DatabaseResource;
    }
  }

  class GraphResource implements Resource {

    public void assertValidCombination(PrivilegeAction action) throws InvalidArgumentsException {
      if (!action.equals(PrivilegeAction.WRITE) && !action.equals(PrivilegeAction.READ) && !action
          .equals(PrivilegeAction.TRAVERSE)) {
        throw new InvalidArgumentsException(
            String.format("Graph resource cannot be combined with action '%s'", action.toString()));
      }
    }

    public Resource.Type type() {
      return Resource.Type.GRAPH;
    }

    public String toString() {
      return "graph";
    }

    public int hashCode() {
      return Resource.Type.GRAPH.hashCode();
    }

    public boolean equals(Object obj) {
      return obj instanceof Resource.GraphResource;
    }
  }
}
