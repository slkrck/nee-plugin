/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import io.nee.kernel.enterprise.api.security.AdminAccessMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import org.eclipse.collections.api.map.primitive.IntObjectMap;
import org.eclipse.collections.api.map.primitive.MutableIntObjectMap;
import org.eclipse.collections.api.set.primitive.IntSet;
import org.eclipse.collections.api.set.primitive.MutableIntSet;
import org.eclipse.collections.impl.factory.primitive.IntObjectMaps;
import org.eclipse.collections.impl.factory.primitive.IntSets;
import org.neo4j.graphdb.security.AuthorizationViolationException;
import org.neo4j.internal.kernel.api.LabelSet;
import org.neo4j.internal.kernel.api.security.AccessMode;
import org.neo4j.internal.kernel.api.security.AdminActionOnResource;
import org.neo4j.internal.kernel.api.security.AdminActionOnResource.DatabaseScope;
import org.neo4j.internal.kernel.api.security.LoginContext.IdLookup;
import org.neo4j.internal.kernel.api.security.PrivilegeAction;

class StandardAccessMode implements AccessMode {

  private final boolean allowsAccess;
  private final boolean allowsReads;
  private final boolean allowsWrites;
  private final boolean allowsTokenCreates;
  private final boolean allowsSchemaWrites;
  private final boolean passwordChangeRequired;
  private final Set<String> roles;
  private final boolean allowsTraverseAllLabels;
  private final boolean allowsTraverseAllRelTypes;
  private final IntSet whitelistTraverseLabels;
  private final IntSet whitelistTraverseRelTypes;
  private final boolean disallowsTraverseAllLabels;
  private final boolean disallowsTraverseAllRelTypes;
  private final IntSet blacklistTraverseLabels;
  private final IntSet blacklistTraverseRelTypes;
  private final boolean allowsReadAllPropertiesAllLabels;
  private final boolean allowsReadAllPropertiesAllRelTypes;
  private final IntSet whitelistedLabelsForAllProperties;
  private final IntSet whitelistedRelTypesForAllProperties;
  private final IntSet whitelistedNodePropertiesForAllLabels;
  private final IntSet whitelistedNodePropertiesForSomeLabel;
  private final IntSet whitelistedRelationshipPropertiesForAllTypes;
  private final IntSet whitelistedRelationshipPropertiesForSomeType;
  private final IntObjectMap<IntSet> whitelistedLabelsForProperty;
  private final IntObjectMap<IntSet> whitelistedRelTypesForProperty;
  private final boolean disallowsReadAllPropertiesAllLabels;
  private final boolean disallowsReadAllPropertiesAllRelTypes;
  private final IntSet blacklistedNodeProperties;
  private final IntSet blacklistedRelationshipProperties;
  private final IntSet blacklistedLabelsForAllProperties;
  private final IntSet blacklistedRelTypesForAllProperties;
  private final IntObjectMap<IntSet> blacklistedLabelsForProperty;
  private final IntObjectMap<IntSet> blacklistedRelTypesForProperty;
  private final AdminAccessMode adminAccessMode;
  private final DatabaseScope database;

  private StandardAccessMode(boolean allowsAccess, boolean allowsReads, boolean allowsWrites,
      boolean allowsTokenCreates, boolean allowsSchemaWrites,
      boolean passwordChangeRequired, Set<String> roles, boolean allowsTraverseAllLabels,
      boolean allowsTraverseAllRelTypes,
      IntSet whitelistTraverseLabels, IntSet whitelistTraverseRelTypes,
      boolean disallowsTraverseAllLabels,
      boolean disallowsTraverseAllRelTypes,
      IntSet blacklistTraverseLabels, IntSet blacklistTraverseRelTypes,
      boolean allowsReadAllPropertiesAllLabels,
      boolean allowsReadAllPropertiesAllRelTypes, IntSet whitelistedLabelsForAllProperties,
      IntSet whitelistedRelTypesForAllProperties,
      IntSet whitelistedNodePropertiesForAllLabels,
      IntSet whitelistedRelationshipPropertiesForAllTypes,
      IntObjectMap<IntSet> whitelistedLabelsForProperty,
      IntObjectMap<IntSet> whitelistedRelTypesForProperty,
      boolean disallowsReadAllPropertiesAllLabels,
      boolean disallowsReadAllPropertiesAllRelTypes, IntSet blacklistedLabelsForAllProperties,
      IntSet blacklistedRelTypesForAllProperties,
      IntSet blacklistedNodeProperties, IntSet blacklistedRelationshipProperties,
      IntObjectMap<IntSet> blacklistedLabelsForProperty,
      IntObjectMap<IntSet> blacklistedRelTypesForProperty, AdminAccessMode adminAccessMode,
      String database) {
    this.allowsAccess = allowsAccess;
    this.allowsReads = allowsReads;
    this.allowsWrites = allowsWrites;
    this.allowsTokenCreates = allowsTokenCreates;
    this.allowsSchemaWrites = allowsSchemaWrites;
    this.passwordChangeRequired = passwordChangeRequired;
    this.roles = roles;
    this.allowsTraverseAllLabels = allowsTraverseAllLabels;
    this.allowsTraverseAllRelTypes = allowsTraverseAllRelTypes;
    this.whitelistTraverseLabels = whitelistTraverseLabels;
    this.whitelistTraverseRelTypes = whitelistTraverseRelTypes;
    this.disallowsTraverseAllLabels = disallowsTraverseAllLabels;
    this.disallowsTraverseAllRelTypes = disallowsTraverseAllRelTypes;
    this.blacklistTraverseLabels = blacklistTraverseLabels;
    this.blacklistTraverseRelTypes = blacklistTraverseRelTypes;
    this.allowsReadAllPropertiesAllLabels = allowsReadAllPropertiesAllLabels;
    this.allowsReadAllPropertiesAllRelTypes = allowsReadAllPropertiesAllRelTypes;
    this.whitelistedLabelsForAllProperties = whitelistedLabelsForAllProperties;
    this.whitelistedRelTypesForAllProperties = whitelistedRelTypesForAllProperties;
    this.whitelistedNodePropertiesForAllLabels = whitelistedNodePropertiesForAllLabels;
    this.whitelistedRelationshipPropertiesForAllTypes = whitelistedRelationshipPropertiesForAllTypes;
    this.whitelistedLabelsForProperty = whitelistedLabelsForProperty;
    this.whitelistedRelTypesForProperty = whitelistedRelTypesForProperty;
    this.disallowsReadAllPropertiesAllLabels = disallowsReadAllPropertiesAllLabels;
    this.disallowsReadAllPropertiesAllRelTypes = disallowsReadAllPropertiesAllRelTypes;
    this.blacklistedLabelsForAllProperties = blacklistedLabelsForAllProperties;
    this.blacklistedRelTypesForAllProperties = blacklistedRelTypesForAllProperties;
    this.blacklistedNodeProperties = blacklistedNodeProperties;
    this.blacklistedRelationshipProperties = blacklistedRelationshipProperties;
    this.blacklistedLabelsForProperty = blacklistedLabelsForProperty;
    this.blacklistedRelTypesForProperty = blacklistedRelTypesForProperty;
    this.adminAccessMode = adminAccessMode;
    this.database = new DatabaseScope(database);
    this.whitelistedNodePropertiesForSomeLabel = whitelistedLabelsForProperty.keySet()
        .select((key) ->
        {
          return !whitelistedLabelsForProperty
              .get(key).isEmpty();
        });
    this.whitelistedRelationshipPropertiesForSomeType = whitelistedRelTypesForProperty.keySet()
        .select((key) ->
        {
          return !whitelistedRelTypesForProperty
              .get(key).isEmpty();
        });
  }

  public boolean allowsWrites() {
    return this.allowsWrites;
  }

  public boolean allowsTokenCreates(PrivilegeAction action) {
    return this.allowsTokenCreates && this.adminAccessMode
        .allows(new AdminActionOnResource(action, this.database));
  }

  public boolean allowsSchemaWrites() {
    return this.allowsSchemaWrites;
  }

  public boolean allowsSchemaWrites(PrivilegeAction action) {
    return this.allowsSchemaWrites && this.adminAccessMode
        .allows(new AdminActionOnResource(action, this.database));
  }

  public boolean allowsTraverseAllLabels() {
    return this.allowsTraverseAllLabels && !this.disallowsTraverseAllLabels
        && this.blacklistTraverseLabels.isEmpty();
  }

  public boolean allowsTraverseAllNodesWithLabel(long label) {
    if (!this.disallowsTraverseAllLabels && !this.blacklistTraverseLabels.notEmpty()) {
      return this.allowsTraverseAllLabels || this.whitelistTraverseLabels.contains((int) label);
    } else {
      return false;
    }
  }

  public boolean disallowsTraverseLabel(long label) {
    return this.disallowsTraverseAllLabels || this.blacklistTraverseLabels.contains((int) label);
  }

  public boolean allowsTraverseNode(long... labels) {
    if (this.allowsTraverseAllLabels()) {
      return true;
    } else if (this.disallowsTraverseAllLabels || labels.length == 1 && labels[0] == -1L) {
      return false;
    } else {
      boolean allowedTraverseAnyLabel = false;
      long[] n3 = labels;
      int n4 = labels.length;

      for (int n5 = 0; n5 < n4; ++n5) {
        long labelAsLong = n3[n5];
        int label = (int) labelAsLong;
        if (this.blacklistTraverseLabels.contains(label)) {
          return false;
        }

        if (this.whitelistTraverseLabels.contains(label)) {
          allowedTraverseAnyLabel = true;
        }
      }

      return allowedTraverseAnyLabel || this.allowsTraverseAllLabels;
    }
  }

  public boolean allowsTraverseAllRelTypes() {
    return this.allowsTraverseAllRelTypes && !this.disallowsTraverseAllRelTypes
        && this.blacklistTraverseRelTypes.isEmpty();
  }

  public boolean allowsTraverseRelType(int relType) {
    if (this.allowsTraverseAllRelTypes()) {
      return true;
    } else if (relType != -1 && !this.disallowsTraverseAllRelTypes
        && !this.blacklistTraverseRelTypes.contains(relType)) {
      return this.allowsTraverseAllRelTypes || this.whitelistTraverseRelTypes.contains(relType);
    } else {
      return false;
    }
  }

  public boolean allowsReadPropertyAllLabels(int propertyKey) {
    return (this.allowsReadAllPropertiesAllLabels || this.whitelistedNodePropertiesForAllLabels
        .contains(propertyKey)) &&
        !this.disallowsReadPropertyForSomeLabel(propertyKey);
  }

  public boolean disallowsReadPropertyForSomeLabel(int propertyKey) {
    return this.disallowsReadAllPropertiesAllLabels || this.blacklistedNodeProperties
        .contains(propertyKey) ||
        !this.blacklistedLabelsForAllProperties.isEmpty()
        || this.blacklistedLabelsForProperty.get(propertyKey) != null;
  }

  private boolean disallowsReadPropertyForAllLabels(int propertyKey) {
    return this.disallowsReadAllPropertiesAllLabels || this.blacklistedNodeProperties
        .contains(propertyKey);
  }

  public boolean allowsReadNodeProperty(Supplier<LabelSet> labelSupplier, int propertyKey) {
    if (this.allowsReadPropertyAllLabels(propertyKey)) {
      return true;
    } else if (this.disallowsReadPropertyForAllLabels(propertyKey)) {
      return false;
    } else {
      LabelSet labelSet = labelSupplier.get();
      IntSet whiteListed = this.whitelistedLabelsForProperty.get(propertyKey);
      IntSet blackListed = this.blacklistedLabelsForProperty.get(propertyKey);
      boolean allowed = false;
      long[] n7 = labelSet.all();
      int n8 = n7.length;

      for (int n9 = 0; n9 < n8; ++n9) {
        long labelAsLong = n7[n9];
        int label = (int) labelAsLong;
        if (whiteListed != null && whiteListed.contains(label)) {
          allowed = true;
        }

        if (blackListed != null && blackListed.contains(label)) {
          return false;
        }

        if (this.whitelistedLabelsForAllProperties.contains(label)) {
          allowed = true;
        }

        if (this.blacklistedLabelsForAllProperties.contains(label)) {
          return false;
        }
      }

      return allowed || this.allowsReadAllPropertiesAllLabels
          || this.whitelistedNodePropertiesForAllLabels.contains(propertyKey);
    }
  }

  public boolean allowsReadPropertyAllRelTypes(int propertyKey) {
    return (this.allowsReadAllPropertiesAllRelTypes
        || this.whitelistedRelationshipPropertiesForAllTypes.contains(propertyKey)) &&
        !this.disallowsReadPropertyForSomeRelType(propertyKey);
  }

  private boolean disallowsReadPropertyForSomeRelType(int propertyKey) {
    return this.disallowsReadAllPropertiesAllRelTypes || this.blacklistedRelationshipProperties
        .contains(propertyKey) ||
        !this.blacklistedRelTypesForAllProperties.isEmpty()
        || this.blacklistedRelTypesForProperty.get(propertyKey) != null;
  }

  private boolean disallowsReadPropertyForAllRelTypes(int propertyKey) {
    return this.disallowsReadAllPropertiesAllRelTypes || this.blacklistedRelationshipProperties
        .contains(propertyKey);
  }

  public boolean allowsReadRelationshipProperty(IntSupplier relType, int propertyKey) {
    if (this.allowsReadPropertyAllRelTypes(propertyKey)) {
      return true;
    } else if (this.disallowsReadPropertyForAllRelTypes(propertyKey)) {
      return false;
    } else {
      IntSet whitelisted = this.whitelistedRelTypesForProperty.get(propertyKey);
      IntSet blacklisted = this.blacklistedRelTypesForProperty.get(propertyKey);
      boolean allowed = whitelisted != null && whitelisted.contains(relType.getAsInt()) ||
          this.whitelistedRelTypesForAllProperties.contains(relType.getAsInt())
          || this.allowsReadAllPropertiesAllRelTypes ||
          this.whitelistedRelationshipPropertiesForAllTypes.contains(propertyKey);
      boolean disallowedRelType = blacklisted != null && blacklisted.contains(relType.getAsInt()) ||
          this.blacklistedRelTypesForAllProperties.contains(relType.getAsInt());
      return allowed && !disallowedRelType;
    }
  }

  public boolean allowsSeePropertyKeyToken(int propertyKey) {
    boolean disabledForNodes =
        this.disallowsReadAllPropertiesAllLabels || this.blacklistedNodeProperties
            .contains(propertyKey) ||
            !this.allowPropertyReadOnSomeNode(propertyKey);
    boolean disabledForRels =
        this.disallowsReadAllPropertiesAllRelTypes || this.blacklistedRelationshipProperties
            .contains(propertyKey) ||
            !this.allowsPropertyReadOnSomeRelType(propertyKey);
    return !disabledForNodes || !disabledForRels;
  }

  private boolean allowPropertyReadOnSomeNode(int propertyKey) {
    return this.allowsReadAllPropertiesAllLabels || this.whitelistedNodePropertiesForAllLabels
        .contains(propertyKey) ||
        this.whitelistedNodePropertiesForSomeLabel.contains(propertyKey)
        || this.whitelistedLabelsForAllProperties.notEmpty();
  }

  private boolean allowsPropertyReadOnSomeRelType(int propertyKey) {
    return this.allowsReadAllPropertiesAllRelTypes
        || this.whitelistedRelationshipPropertiesForAllTypes.contains(propertyKey) ||
        this.whitelistedRelationshipPropertiesForSomeType.contains(propertyKey)
        || this.whitelistedRelTypesForAllProperties.notEmpty();
  }

  public boolean allowsProcedureWith(String[] roleNames) {
    String[] n2 = roleNames;
    int n3 = roleNames.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      String roleName = n2[n4];
      if (this.roles.contains(roleName)) {
        return true;
      }
    }

    return false;
  }

  public AuthorizationViolationException onViolation(String msg) {
    return this.passwordChangeRequired ? Static.CREDENTIALS_EXPIRED
        .onViolation("Permission denied.") : new AuthorizationViolationException(msg);
  }

  public String name() {
    Set<String> sortedRoles = new TreeSet(this.roles);
    return this.roles.isEmpty() ? "no roles" : "roles [" + String.join(",", sortedRoles) + "]";
  }

  boolean allowsAccess() {
    return this.allowsAccess;
  }

  public Set<String> roles() {
    return this.roles;
  }

  AdminAccessMode getAdminAccessMode() {
    return this.adminAccessMode;
  }

  static class Builder {

    private final boolean isAuthenticated;
    private final boolean passwordChangeRequired;
    private final Set<String> roles;
    private final IdLookup resolver;
    private final String database;
    private final Map<ResourcePrivilege.GrantOrDeny, Boolean> anyAccess = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, Boolean> anyRead = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, Boolean> anyWrite = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, Boolean> traverseAllLabels = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, Boolean> traverseAllRelTypes = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, MutableIntSet> traverseLabels = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, MutableIntSet> traverseRelTypes = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, Boolean> readAllPropertiesAllLabels = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, Boolean> readAllPropertiesAllRelTypes = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, MutableIntSet> nodeSegmentForAllProperties = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, MutableIntSet> relationshipSegmentForAllProperties = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, MutableIntSet> nodeProperties = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, MutableIntSet> relationshipProperties = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, MutableIntObjectMap<IntSet>> nodeSegmentForProperty = new HashMap();
    private final Map<ResourcePrivilege.GrantOrDeny, MutableIntObjectMap<IntSet>> relationshipSegmentForProperty = new HashMap();
    private final StandardAdminAccessMode.Builder adminModeBuilder = new StandardAdminAccessMode.Builder();
    private boolean token;
    private boolean schema;

    Builder(boolean isAuthenticated, boolean passwordChangeRequired, Set<String> roles,
        IdLookup resolver, String database) {
      this.isAuthenticated = isAuthenticated;
      this.passwordChangeRequired = passwordChangeRequired;
      this.roles = roles;
      this.resolver = resolver;
      this.database = database;
      ResourcePrivilege.GrantOrDeny[] n6 = ResourcePrivilege.GrantOrDeny.values();
      int n7 = n6.length;

      for (int n8 = 0; n8 < n7; ++n8) {
        ResourcePrivilege.GrantOrDeny privilegeType = n6[n8];
        this.traverseLabels.put(privilegeType, IntSets.mutable.empty());
        this.traverseRelTypes.put(privilegeType, IntSets.mutable.empty());
        this.nodeSegmentForAllProperties.put(privilegeType, IntSets.mutable.empty());
        this.relationshipSegmentForAllProperties.put(privilegeType, IntSets.mutable.empty());
        this.nodeProperties.put(privilegeType, IntSets.mutable.empty());
        this.relationshipProperties.put(privilegeType, IntSets.mutable.empty());
        this.nodeSegmentForProperty.put(privilegeType, IntObjectMaps.mutable.empty());
        this.relationshipSegmentForProperty.put(privilegeType, IntObjectMaps.mutable.empty());
      }
    }

    StandardAccessMode build() {
      return new StandardAccessMode(this.isAuthenticated && this.anyAccess
          .getOrDefault(ResourcePrivilege.GrantOrDeny.GRANT, false) &&
          !this.anyAccess.getOrDefault(ResourcePrivilege.GrantOrDeny.DENY, false),
          this.isAuthenticated && this.anyRead
              .getOrDefault(ResourcePrivilege.GrantOrDeny.GRANT, false),
          this.isAuthenticated && this.anyWrite
              .getOrDefault(ResourcePrivilege.GrantOrDeny.GRANT, false) &&
              !this.anyWrite.getOrDefault(ResourcePrivilege.GrantOrDeny.DENY, false),
          this.isAuthenticated && this.token,
          this.isAuthenticated && this.schema, this.passwordChangeRequired, this.roles,
          this.traverseAllLabels.getOrDefault(ResourcePrivilege.GrantOrDeny.GRANT, false),
          this.traverseAllRelTypes.getOrDefault(ResourcePrivilege.GrantOrDeny.GRANT, false),
          this.traverseLabels.get(ResourcePrivilege.GrantOrDeny.GRANT),
          this.traverseRelTypes.get(ResourcePrivilege.GrantOrDeny.GRANT),
          this.traverseAllLabels.getOrDefault(ResourcePrivilege.GrantOrDeny.DENY, false),
          this.traverseAllRelTypes.getOrDefault(ResourcePrivilege.GrantOrDeny.DENY, false),
          this.traverseLabels.get(ResourcePrivilege.GrantOrDeny.DENY),
          this.traverseRelTypes.get(ResourcePrivilege.GrantOrDeny.DENY),
          this.readAllPropertiesAllLabels.getOrDefault(ResourcePrivilege.GrantOrDeny.GRANT, false),
          this.readAllPropertiesAllRelTypes
              .getOrDefault(ResourcePrivilege.GrantOrDeny.GRANT, false),
          this.nodeSegmentForAllProperties.get(ResourcePrivilege.GrantOrDeny.GRANT),
          this.relationshipSegmentForAllProperties.get(ResourcePrivilege.GrantOrDeny.GRANT),
          this.nodeProperties.get(ResourcePrivilege.GrantOrDeny.GRANT),
          this.relationshipProperties.get(ResourcePrivilege.GrantOrDeny.GRANT),
          this.nodeSegmentForProperty.get(ResourcePrivilege.GrantOrDeny.GRANT),
          this.relationshipSegmentForProperty.get(ResourcePrivilege.GrantOrDeny.GRANT),
          this.readAllPropertiesAllLabels.getOrDefault(ResourcePrivilege.GrantOrDeny.DENY, false),
          this.readAllPropertiesAllRelTypes.getOrDefault(ResourcePrivilege.GrantOrDeny.DENY, false),
          this.nodeSegmentForAllProperties.get(ResourcePrivilege.GrantOrDeny.DENY),
          this.relationshipSegmentForAllProperties.get(ResourcePrivilege.GrantOrDeny.DENY),
          this.nodeProperties.get(ResourcePrivilege.GrantOrDeny.DENY),
          this.relationshipProperties.get(ResourcePrivilege.GrantOrDeny.DENY),
          this.nodeSegmentForProperty.get(ResourcePrivilege.GrantOrDeny.DENY),
          this.relationshipSegmentForProperty.get(ResourcePrivilege.GrantOrDeny.DENY),
          this.adminModeBuilder.build(),
          this.database);
    }

    StandardAccessMode.Builder withAccess() {
      this.anyAccess.put(ResourcePrivilege.GrantOrDeny.GRANT, true);
      return this;
    }

    StandardAccessMode.Builder addPrivilege(ResourcePrivilege privilege) {
      Resource resource = privilege.getResource();
      Segment segment = privilege.getSegment();
      ResourcePrivilege.GrantOrDeny privilegeType = privilege.getPrivilegeType();
      PrivilegeAction action = privilege.getAction();
      switch (action) {
        case ACCESS:
          this.anyAccess.put(privilegeType, true);
          break;
        case TRAVERSE:
          this.anyRead.put(privilegeType, true);
          if (segment instanceof LabelSegment) {
            if (segment.equals(LabelSegment.ALL)) {
              this.traverseAllLabels.put(privilegeType, true);
            } else {
              this.addLabel(this.traverseLabels.get(privilegeType), (LabelSegment) segment);
            }
          } else {
            if (!(segment instanceof RelTypeSegment)) {
              throw new IllegalStateException(
                  "Unsupported segment qualifier for traverse privilege: " + segment.getClass()
                      .getSimpleName());
            }

            if (segment.equals(RelTypeSegment.ALL)) {
              this.traverseAllRelTypes.put(privilegeType, true);
            } else {
              this.addRelType(this.traverseRelTypes.get(privilegeType), (RelTypeSegment) segment);
            }
          }
          break;
        case READ:
          this.anyRead.put(privilegeType, true);
          switch (resource.type()) {
            case GRAPH:
              this.readAllPropertiesAllLabels.put(privilegeType, true);
              this.readAllPropertiesAllRelTypes.put(privilegeType, true);
              return this;
            case PROPERTY:
              int propertyId = this.resolvePropertyId(resource.getArg1());
              if (propertyId != -1) {
                if (segment instanceof LabelSegment) {
                  if (segment.equals(LabelSegment.ALL)) {
                    this.nodeProperties.get(privilegeType).add(propertyId);
                  } else {
                    this.addLabel(this.nodeSegmentForProperty.get(privilegeType),
                        (LabelSegment) segment, propertyId);
                  }

                  return this;
                } else {
                  if (!(segment instanceof RelTypeSegment)) {
                    throw new IllegalStateException(
                        "Unsupported segment qualifier for read privilege: " + segment.getClass()
                            .getSimpleName());
                  }

                  if (segment.equals(RelTypeSegment.ALL)) {
                    this.relationshipProperties.get(privilegeType).add(propertyId);
                  } else {
                    this.addRelType(this.relationshipSegmentForProperty.get(privilegeType),
                        (RelTypeSegment) segment,
                        propertyId);
                  }

                  return this;
                }
              }

              return this;
            case ALL_PROPERTIES:
              if (segment instanceof LabelSegment) {
                if (segment.equals(LabelSegment.ALL)) {
                  this.readAllPropertiesAllLabels.put(privilegeType, true);
                } else {
                  this.addLabel(this.nodeSegmentForAllProperties.get(privilegeType),
                      (LabelSegment) segment);
                }

                return this;
              } else {
                if (!(segment instanceof RelTypeSegment)) {
                  throw new IllegalStateException(
                      "Unsupported segment qualifier for read privilege: " + segment.getClass()
                          .getSimpleName());
                }

                if (segment.equals(RelTypeSegment.ALL)) {
                  this.readAllPropertiesAllRelTypes.put(privilegeType, true);
                } else {
                  this.addRelType(this.relationshipSegmentForAllProperties.get(privilegeType),
                      (RelTypeSegment) segment);
                }

                return this;
              }
            default:
              return this;
          }
        case WRITE:
          switch (resource.type()) {
            case GRAPH:
            case PROPERTY:
            case ALL_PROPERTIES:
              this.anyWrite.put(privilegeType, true);
              return this;
            default:
              return this;
          }
        default:
          if (PrivilegeAction.TOKEN.satisfies(action)) {
            this.token = true;
            this.addPrivilegeAction(privilege);
          } else if (PrivilegeAction.SCHEMA.satisfies(action)) {
            this.schema = true;
            this.addPrivilegeAction(privilege);
          } else if (PrivilegeAction.ADMIN.satisfies(action)) {
            this.addPrivilegeAction(privilege);
          }
      }

      return this;
    }

    private void addPrivilegeAction(ResourcePrivilege privilege) {
      DatabaseScope dbScope =
          privilege.isAllDatabases() ? DatabaseScope.ALL : new DatabaseScope(privilege.getDbName());
      AdminActionOnResource adminAction = new AdminActionOnResource(privilege.getAction(), dbScope);
      if (privilege.getPrivilegeType().isGrant()) {
        this.adminModeBuilder.allow(adminAction);
      } else {
        this.adminModeBuilder.deny(adminAction);
      }
    }

    private int resolveLabelId(String label) {
      assert !label.isEmpty();

      return this.resolver.getLabelId(label);
    }

    private int resolveRelTypeId(String relType) {
      assert !relType.isEmpty();

      return this.resolver.getRelTypeId(relType);
    }

    private int resolvePropertyId(String property) {
      assert !property.isEmpty();

      return this.resolver.getPropertyKeyId(property);
    }

    private void addLabel(MutableIntObjectMap<IntSet> map, LabelSegment segment, int propertyId) {
      MutableIntSet setForProperty = (MutableIntSet) map
          .getIfAbsentPut(propertyId, IntSets.mutable.empty());
      this.addLabel(setForProperty, segment);
    }

    private void addLabel(MutableIntSet whiteOrBlacklist, LabelSegment segment) {
      int labelId = this.resolveLabelId(segment.getLabel());
      if (labelId != -1) {
        whiteOrBlacklist.add(labelId);
      }
    }

    private void addRelType(MutableIntObjectMap<IntSet> map, RelTypeSegment segment,
        int propertyId) {
      MutableIntSet setForProperty = (MutableIntSet) map
          .getIfAbsentPut(propertyId, IntSets.mutable.empty());
      this.addRelType(setForProperty, segment);
    }

    private void addRelType(MutableIntSet whiteOrBlacklist, RelTypeSegment segment) {
      int relTypeId = this.resolveRelTypeId(segment.getRelType());
      if (relTypeId != -1) {
        whiteOrBlacklist.add(relTypeId);
      }
    }
  }
}
