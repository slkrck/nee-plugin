/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth.plugin;

import io.nee.server.security.enterprise.auth.RealmLifecycle;
import io.nee.server.security.enterprise.auth.ShiroAuthorizationInfoProvider;
import io.nee.server.security.enterprise.auth.plugin.api.AuthProviderOperations;
import io.nee.server.security.enterprise.auth.plugin.api.AuthorizationExpiredException;
import io.nee.server.security.enterprise.auth.plugin.spi.AuthInfo;
import io.nee.server.security.enterprise.auth.plugin.spi.AuthPlugin;
import io.nee.server.security.enterprise.auth.plugin.spi.AuthenticationPlugin;
import io.nee.server.security.enterprise.auth.plugin.spi.AuthorizationPlugin;
import io.nee.server.security.enterprise.auth.plugin.spi.CustomCacheableAuthenticationInfo;
import io.nee.server.security.enterprise.log.SecurityLog;
import java.nio.file.Path;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.cypher.internal.security.SecureHasher;
import org.neo4j.kernel.api.security.exception.InvalidAuthTokenException;
import org.neo4j.kernel.internal.Version;
import org.neo4j.logging.Log;
import org.neo4j.server.security.auth.ShiroAuthToken;

public class PluginRealm extends AuthorizingRealm implements RealmLifecycle,
    ShiroAuthorizationInfoProvider {

  private final Config config;
  private final Log log;
  private final Clock clock;
  private final SecureHasher secureHasher;
  private final AuthProviderOperations authProviderOperations;
  private AuthenticationPlugin authenticationPlugin;
  private AuthorizationPlugin authorizationPlugin;
  private AuthPlugin authPlugin;

  public PluginRealm(Config config, SecurityLog securityLog, Clock clock,
      SecureHasher secureHasher) {
    this.authProviderOperations = new PluginRealm.PluginRealmOperations();
    this.config = config;
    this.clock = clock;
    this.secureHasher = secureHasher;
    this.log = securityLog;
    this.setCredentialsMatcher(new PluginRealm.CredentialsMatcher());
    this.setAuthenticationCachingEnabled(false);
    this.setAuthorizationCachingEnabled(true);
  }

  public PluginRealm(AuthenticationPlugin authenticationPlugin,
      AuthorizationPlugin authorizationPlugin, Config config, SecurityLog securityLog, Clock clock,
      SecureHasher secureHasher) {
    this(config, securityLog, clock, secureHasher);
    this.authenticationPlugin = authenticationPlugin;
    this.authorizationPlugin = authorizationPlugin;
    this.resolvePluginName();
  }

  public PluginRealm(AuthPlugin authPlugin, Config config, SecurityLog securityLog, Clock clock,
      SecureHasher secureHasher) {
    this(config, securityLog, clock, secureHasher);
    this.authPlugin = authPlugin;
    this.resolvePluginName();
  }

  private static CustomCacheableAuthenticationInfo.CredentialsMatcher getCustomCredentialsMatcherIfPresent(
      AuthenticationInfo info) {
    return info instanceof CustomCredentialsMatcherSupplier
        ? ((CustomCredentialsMatcherSupplier) info).getCredentialsMatcher() : null;
  }

  private void resolvePluginName() {
    String pluginName = null;
    if (this.authPlugin != null) {
      pluginName = this.authPlugin.name();
    } else if (this.authenticationPlugin != null) {
      pluginName = this.authenticationPlugin.name();
    } else if (this.authorizationPlugin != null) {
      pluginName = this.authorizationPlugin.name();
    }

    if (pluginName != null && !pluginName.isEmpty()) {
      this.setName("plugin-" + pluginName);
    }
  }

  private Collection<AuthorizationPlugin.PrincipalAndProvider> getPrincipalAndProviderCollection(
      PrincipalCollection principalCollection) {
    Collection<AuthorizationPlugin.PrincipalAndProvider> principalAndProviderCollection = new ArrayList();
    Iterator n3 = principalCollection.getRealmNames().iterator();

    while (n3.hasNext()) {
      String realm = (String) n3.next();
      Iterator n5 = principalCollection.fromRealm(realm).iterator();

      while (n5.hasNext()) {
        Object principal = n5.next();
        principalAndProviderCollection
            .add(new AuthorizationPlugin.PrincipalAndProvider(principal, realm));
      }
    }

    return principalAndProviderCollection;
  }

  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    if (this.authorizationPlugin != null) {
      io.nee.server.security.enterprise.auth.plugin.spi.AuthorizationInfo authorizationInfo;
      try {
        authorizationInfo = this.authorizationPlugin
            .authorize(this.getPrincipalAndProviderCollection(principals));
      } catch (AuthorizationExpiredException n4) {
        throw new org.neo4j.graphdb.security.AuthorizationExpiredException(
            "Plugin '" + this.getName() + "' authorization info expired: " + n4.getMessage(), n4);
      }

      if (authorizationInfo != null) {
        return PluginAuthorizationInfo.create(authorizationInfo);
      }
    } else if (this.authPlugin != null && !principals.fromRealm(this.getName()).isEmpty()) {
      throw new org.neo4j.graphdb.security.AuthorizationExpiredException(
          "Plugin '" + this.getName() + "' authorization info expired.");
    }

    return null;
  }

  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token)
      throws AuthenticationException {
    if (token instanceof ShiroAuthToken) {
      try {
        PluginApiAuthToken pluginAuthToken = PluginApiAuthToken
            .createFromMap(((ShiroAuthToken) token).getAuthTokenMap());

        PluginAuthInfo n5;
        try {
          if (this.authPlugin == null) {
            if (this.authenticationPlugin == null) {
              return null;
            }

            io.nee.server.security.enterprise.auth.plugin.spi.AuthenticationInfo authenticationInfo =
                this.authenticationPlugin.authenticate(pluginAuthToken);
            if (authenticationInfo == null) {
              return null;
            }

            PluginAuthenticationInfo n12 = PluginAuthenticationInfo
                .createCacheable(authenticationInfo, this.getName(), this.secureHasher);
            return n12;
          }

          AuthInfo authInfo = this.authPlugin.authenticateAndAuthorize(pluginAuthToken);
          if (authInfo == null) {
            return null;
          }

          PluginAuthInfo pluginAuthInfo = PluginAuthInfo
              .createCacheable(authInfo, this.getName(), this.secureHasher);
          this.cacheAuthorizationInfo(pluginAuthInfo);
          n5 = pluginAuthInfo;
        } finally {
          pluginAuthToken.clearCredentials();
        }

        return n5;
      } catch (InvalidAuthTokenException | io.nee.server.security.enterprise.auth.plugin.api.AuthenticationException n10) {
        throw new AuthenticationException(n10.getMessage(), n10.getCause());
      }
    } else {
      return null;
    }
  }

  private void cacheAuthorizationInfo(PluginAuthInfo authInfo) {
    Cache<Object, AuthorizationInfo> authorizationCache = this.getAuthorizationCache();
    Object key = this.getAuthorizationCacheKey(authInfo.getPrincipals());
    authorizationCache.put(key, authInfo);
  }

  public boolean canAuthenticate() {
    return this.authPlugin != null || this.authenticationPlugin != null;
  }

  public boolean canAuthorize() {
    return this.authPlugin != null || this.authorizationPlugin != null;
  }

  public AuthorizationInfo getAuthorizationInfoSnapshot(PrincipalCollection principalCollection) {
    return this.getAuthorizationInfo(principalCollection);
  }

  protected Object getAuthorizationCacheKey(PrincipalCollection principals) {
    return this.getAvailablePrincipal(principals);
  }

  protected Object getAuthenticationCacheKey(AuthenticationToken token) {
    return token != null ? token.getPrincipal() : null;
  }

  public boolean supports(AuthenticationToken token) {
    return this.supportsSchemeAndRealm(token);
  }

  private boolean supportsSchemeAndRealm(AuthenticationToken token) {
    if (token instanceof ShiroAuthToken) {
      ShiroAuthToken shiroAuthToken = (ShiroAuthToken) token;
      return shiroAuthToken.supportsRealm(this.getName());
    } else {
      return false;
    }
  }

  public void initialize() throws Exception {
    if (this.authenticationPlugin != null) {
      this.authenticationPlugin.initialize(this.authProviderOperations);
    }

    if (this.authorizationPlugin != null && this.authorizationPlugin != this.authenticationPlugin) {
      this.authorizationPlugin.initialize(this.authProviderOperations);
    }

    if (this.authPlugin != null) {
      this.authPlugin.initialize(this.authProviderOperations);
    }
  }

  public void start() throws Exception {
    if (this.authenticationPlugin != null) {
      this.authenticationPlugin.start();
    }

    if (this.authorizationPlugin != null && this.authorizationPlugin != this.authenticationPlugin) {
      this.authorizationPlugin.start();
    }

    if (this.authPlugin != null) {
      this.authPlugin.start();
    }
  }

  public void stop() throws Exception {
    if (this.authenticationPlugin != null) {
      this.authenticationPlugin.stop();
    }

    if (this.authorizationPlugin != null && this.authorizationPlugin != this.authenticationPlugin) {
      this.authorizationPlugin.stop();
    }

    if (this.authPlugin != null) {
      this.authPlugin.stop();
    }
  }

  public void shutdown() {
    if (this.authenticationPlugin != null) {
      this.authenticationPlugin.shutdown();
    }

    if (this.authorizationPlugin != null && this.authorizationPlugin != this.authenticationPlugin) {
      this.authorizationPlugin.shutdown();
    }

    if (this.authPlugin != null) {
      this.authPlugin.shutdown();
    }
  }

  private class PluginRealmOperations implements AuthProviderOperations {

    private final AuthProviderOperations.Log innerLog = new AuthProviderOperations.Log() {
      private String withPluginName(String msg) {
        String n10000 = PluginRealm.this.getName();
        return "{" + n10000 + "} " + msg;
      }

      public void debug(String message) {
        PluginRealm.this.log.debug(this.withPluginName(message));
      }

      public void info(String message) {
        PluginRealm.this.log.info(this.withPluginName(message));
      }

      public void warn(String message) {
        PluginRealm.this.log.warn(this.withPluginName(message));
      }

      public void error(String message) {
        PluginRealm.this.log.error(this.withPluginName(message));
      }

      public boolean isDebugEnabled() {
        return PluginRealm.this.log.isDebugEnabled();
      }
    };

    public Path neo4jHome() {
      return PluginRealm.this.config.get(GraphDatabaseSettings.neo4j_home).toFile()
          .getAbsoluteFile().toPath();
    }

    public Optional<Path> neo4jConfigFile() {
      return Optional.empty();
    }

    public String neo4jVersion() {
      return Version.getNeo4jVersion();
    }

    public Clock clock() {
      return PluginRealm.this.clock;
    }

    public AuthProviderOperations.Log log() {
      return this.innerLog;
    }

    public void setAuthenticationCachingEnabled(boolean authenticationCachingEnabled) {
      PluginRealm.this.setAuthenticationCachingEnabled(authenticationCachingEnabled);
    }

    public void setAuthorizationCachingEnabled(boolean authorizationCachingEnabled) {
      PluginRealm.this.setAuthorizationCachingEnabled(authorizationCachingEnabled);
    }
  }

  private class CredentialsMatcher implements org.apache.shiro.authc.credential.CredentialsMatcher {

    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
      CustomCacheableAuthenticationInfo.CredentialsMatcher customCredentialsMatcher = PluginRealm
          .getCustomCredentialsMatcherIfPresent(info);
      if (customCredentialsMatcher != null) {
        Map authToken = ((ShiroAuthToken) token).getAuthTokenMap();

        try {
          PluginApiAuthToken pluginApiAuthToken = PluginApiAuthToken.createFromMap(authToken);
          boolean n17 = false;

          boolean n6;
          try {
            n17 = true;
            n6 = customCredentialsMatcher.doCredentialsMatch(pluginApiAuthToken);
            n17 = false;
          } finally {
            if (n17) {
              char[] credentialsx = pluginApiAuthToken.credentials();
              if (credentialsx != null) {
                Arrays.fill(credentialsx, '\u0000');
              }
            }
          }

          char[] credentials = pluginApiAuthToken.credentials();
          if (credentials != null) {
            Arrays.fill(credentials, '\u0000');
          }

          return n6;
        } catch (InvalidAuthTokenException n20) {
          throw new AuthenticationException(n20.getMessage());
        }
      } else if (info.getCredentials() != null) {
        PluginShiroAuthToken pluginShiroAuthToken = PluginShiroAuthToken.of(token);

        boolean n5;
        try {
          n5 = PluginRealm.this.secureHasher.getHashedCredentialsMatcher()
              .doCredentialsMatch(pluginShiroAuthToken, info);
        } finally {
          pluginShiroAuthToken.clearCredentials();
        }

        return n5;
      } else if (PluginRealm.this.isAuthenticationCachingEnabled()) {
        PluginRealm.this.log.error(
            "Authentication caching is enabled in plugin %s but it does not return cacheable credentials. This configuration is not secure.",
            PluginRealm.this.getName());
        return false;
      } else {
        return true;
      }
    }
  }
}
