/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.support.DelegatingSubject;
import org.neo4j.internal.kernel.api.security.AuthenticationResult;
import org.neo4j.server.security.auth.ShiroAuthenticationInfo;

public class ShiroSubject extends DelegatingSubject {

  private AuthenticationResult authenticationResult;
  private ShiroAuthenticationInfo authenticationInfo;

  public ShiroSubject(SecurityManager securityManager, AuthenticationResult authenticationResult) {
    super(securityManager);
    this.authenticationResult = authenticationResult;
  }

  public ShiroSubject(PrincipalCollection principals, boolean authenticated, String host,
      Session session, boolean sessionCreationEnabled,
      SecurityManager securityManager, AuthenticationResult authenticationResult,
      ShiroAuthenticationInfo authenticationInfo) {
    super(principals, authenticated, host, session, sessionCreationEnabled, securityManager);
    this.authenticationResult = authenticationResult;
    this.authenticationInfo = authenticationInfo;
  }

  public AuthenticationResult getAuthenticationResult() {
    return this.authenticationResult;
  }

  void setAuthenticationResult(AuthenticationResult authenticationResult) {
    this.authenticationResult = authenticationResult;
  }

  public ShiroAuthenticationInfo getAuthenticationInfo() {
    return this.authenticationInfo;
  }

  public void clearAuthenticationInfo() {
    this.authenticationInfo = null;
  }
}
