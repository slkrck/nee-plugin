/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.systemgraph;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import io.nee.server.security.enterprise.auth.RealmLifecycle;
import io.nee.server.security.enterprise.auth.ResourcePrivilege;
import io.nee.server.security.enterprise.auth.ShiroAuthorizationInfoProvider;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Stream;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.neo4j.cypher.internal.security.SecureHasher;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.internal.helpers.collection.Iterables;
import org.neo4j.kernel.api.exceptions.InvalidArgumentsException;
import org.neo4j.server.security.auth.AuthenticationStrategy;
import org.neo4j.server.security.systemgraph.BasicSystemGraphRealm;
import org.neo4j.server.security.systemgraph.SecurityGraphInitializer;

public class SystemGraphRealm extends BasicSystemGraphRealm implements RealmLifecycle,
    ShiroAuthorizationInfoProvider {

  private final boolean authorizationEnabled;
  private final Cache<String, Set<ResourcePrivilege>> privilegeCache;

  public SystemGraphRealm(SecurityGraphInitializer systemGraphInitializer,
      DatabaseManager<?> databaseManager, SecureHasher secureHasher,
      AuthenticationStrategy authenticationStrategy, boolean authenticationEnabled,
      boolean authorizationEnabled) {
    super(systemGraphInitializer, databaseManager, secureHasher, authenticationStrategy,
        authenticationEnabled);
    this.setName("native");
    this.authorizationEnabled = authorizationEnabled;
    Caffeine<Object, Object> builder =
        Caffeine.newBuilder().maximumSize(10000L).executor(ForkJoinPool.commonPool())
            .expireAfterAccess(Duration.ofHours(1L));
    this.privilegeCache = builder.build();
    this.setAuthorizationCachingEnabled(true);
  }

  public void initialize() {
  }

  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    if (!this.authorizationEnabled) {
      return null;
    } else {
      String username = (String) this.getAvailablePrincipal(principals);
      if (username == null) {
        return null;
      } else {
        boolean existingUser = false;
        boolean passwordChangeRequired = false;
        boolean suspended = false;
        TreeSet roleNames = new TreeSet();

        try {
          Transaction tx = this.getSystemDb().beginTx();

          try {
            Node userNode = tx.findNode(Label.label("User"), "name", username);
            if (userNode != null) {
              existingUser = true;
              passwordChangeRequired = (Boolean) userNode.getProperty("passwordChangeRequired");
              suspended = (Boolean) userNode.getProperty("suspended");
              Iterable<Relationship> rels =
                  userNode
                      .getRelationships(Direction.OUTGOING, RelationshipType.withName("HAS_ROLE"));
              rels.forEach((rel) ->
              {
                roleNames.add(rel.getEndNode().getProperty("name"));
              });
            }

            tx.commit();
          } catch (Throwable n11) {
            if (tx != null) {
              try {
                tx.close();
              } catch (Throwable n10) {
                n11.addSuppressed(n10);
              }
            }

            throw n11;
          }

          if (tx != null) {
            tx.close();
          }
        } catch (NotFoundException n12) {
          return null;
        }

        if (!existingUser) {
          return null;
        } else {
          return !passwordChangeRequired && !suspended ? new SimpleAuthorizationInfo(roleNames)
              : new SimpleAuthorizationInfo();
        }
      }
    }
  }

  protected Object getAuthorizationCacheKey(PrincipalCollection principals) {
    return this.getAvailablePrincipal(principals);
  }

  public AuthorizationInfo getAuthorizationInfoSnapshot(PrincipalCollection principalCollection) {
    return this.getAuthorizationInfo(principalCollection);
  }

  public Set<ResourcePrivilege> getPrivilegesForRoles(Set<String> roles) {
    Map<String, Set<ResourcePrivilege>> resultsPerRole = new HashMap();
    boolean lookupPrivileges = false;
    Iterator n4 = roles.iterator();

    Set privs;
    while (n4.hasNext()) {
      String role = (String) n4.next();
      privs = this.privilegeCache.getIfPresent(role);
      if (privs == null) {
        lookupPrivileges = true;
      } else {
        resultsPerRole.put(role, privs);
      }
    }

    if (lookupPrivileges) {
      Transaction tx = this.getSystemDb().beginTx();

      try {
        Stream<Node> roleStream = tx.findNodes(Label.label("Role")).stream().filter((roleNode) ->
        {
          return this.rolesForUserContainsRole(roles, roleNode);
        });
        roleStream.forEach((roleNode) ->
        {
          try {
            String roleName = (String) roleNode.getProperty("name");
            Set<ResourcePrivilege> rolePrivileges = resultsPerRole
                .computeIfAbsent(roleName, (role) ->
                {
                  return new HashSet();
                });
            roleNode.getRelationships(Direction.OUTGOING).forEach((relToPriv) ->
            {
              try {
                Node privilege = relToPriv.getEndNode();
                String grantOrDeny = relToPriv.getType().name();
                String action =
                    (String) privilege.getProperty("action");
                Node resourceNode = Iterables
                    .single(privilege.getRelationships(
                        Direction.OUTGOING,
                        RelationshipType.withName(
                            "APPLIES_TO")))
                    .getEndNode();
                Node segmentNode = Iterables
                    .single(privilege.getRelationships(
                        Direction.OUTGOING,
                        RelationshipType.withName(
                            "SCOPE")))
                    .getEndNode();
                Node dbNode = Iterables.single(
                    segmentNode.getRelationships(
                        Direction.OUTGOING,
                        RelationshipType.withName(
                            "FOR")))
                    .getEndNode();
                String dbName =
                    (String) dbNode.getProperty("name");
                Node qualifierNode = Iterables
                    .single(segmentNode.getRelationships(
                        Direction.OUTGOING,
                        RelationshipType.withName(
                            "QUALIFIED")))
                    .getEndNode();
                ResourcePrivilege.GrantOrDeny privilegeType =
                    ResourcePrivilege.GrantOrDeny
                        .fromRelType(grantOrDeny);
                PrivilegeBuilder privilegeBuilder =
                    new PrivilegeBuilder(privilegeType,
                        action);
                privilegeBuilder.withinScope(qualifierNode)
                    .onResource(resourceNode);
                String dbLabel = Iterables
                    .single(dbNode.getLabels()).name();
                byte n14 = -1;
                switch (dbLabel.hashCode()) {
                  case -1810636282:
                    if (dbLabel.equals("DatabaseAll")) {
                      n14 = 1;
                    }
                    break;
                  case -721472364:
                    if (dbLabel.equals("DeletedDatabase")) {
                      n14 = 2;
                    }
                    break;
                  case 1854109083:
                    if (dbLabel.equals("Database")) {
                      n14 = 0;
                    }
                }

                switch (n14) {
                  case 0:
                    privilegeBuilder.forDatabase(dbName);
                    break;
                  case 1:
                    privilegeBuilder.forAllDatabases();
                    break;
                  case 2:
                    return;
                  default:
                    throw new IllegalStateException(
                        "Cannot have database node without either 'Database' or 'DatabaseAll' labels: "
                            +
                            dbLabel);
                }

                rolePrivileges.add(privilegeBuilder.build());
              } catch (InvalidArgumentsException n15) {
                throw new IllegalStateException(
                    "Failed to authorize", n15);
              }
            });
          } catch (NotFoundException nfe) {
          }
        });
        tx.commit();
      } catch (Throwable n8) {
        if (tx != null) {
          try {
            tx.close();
          } catch (Throwable n7) {
            n8.addSuppressed(n7);
          }
        }

        throw n8;
      }

      if (tx != null) {
        tx.close();
      }
    }

    if (!resultsPerRole.isEmpty()) {
      this.privilegeCache.putAll(resultsPerRole);
    }

    Set<ResourcePrivilege> combined = new HashSet();
    Iterator n12 = resultsPerRole.values().iterator();

    while (n12.hasNext()) {
      privs = (Set) n12.next();
      combined.addAll(privs);
    }

    return combined;
  }

  private boolean rolesForUserContainsRole(Set<String> roles, Node roleNode) {
    try {
      return roles.contains(roleNode.getProperty("name").toString());
    } catch (NotFoundException n4) {
      return false;
    }
  }

  public void clearCacheForRoles() {
    this.privilegeCache.invalidateAll();
  }
}
