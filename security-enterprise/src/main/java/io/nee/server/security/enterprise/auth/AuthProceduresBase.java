/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import io.nee.kernel.enterprise.api.security.EnterpriseSecurityContext;
import io.nee.server.security.enterprise.log.SecurityLog;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.neo4j.graphdb.Transaction;
import org.neo4j.internal.kernel.api.security.AuthenticationResult;
import org.neo4j.kernel.internal.GraphDatabaseAPI;
import org.neo4j.procedure.Context;

public class AuthProceduresBase {

  @Context
  public EnterpriseSecurityContext securityContext;
  @Context
  public GraphDatabaseAPI graph;
  @Context
  public Transaction transaction;
  @Context
  public SecurityLog securityLog;

  protected AuthProceduresBase.UserResult userResultForSubject() {
    String username = this.securityContext.subject().username();
    boolean changeReq = this.securityContext.subject().getAuthenticationResult()
        .equals(AuthenticationResult.PASSWORD_CHANGE_REQUIRED);
    return new AuthProceduresBase.UserResult(username, this.securityContext.roles(), changeReq,
        false);
  }

  public static class RoleResult {

    public final String role;
    public final List<String> users;

    RoleResult(String role, Set<String> users) {
      this.role = role;
      this.users = new ArrayList();
      this.users.addAll(users);
    }
  }

  public static class UserResult {

    public final String username;
    public final List<String> roles;
    public final List<String> flags;

    UserResult(String username, Collection<String> roles, boolean changeRequired,
        boolean suspended) {
      this.username = username;
      this.roles = new ArrayList();
      this.roles.addAll(roles);
      this.flags = new ArrayList();
      if (changeRequired) {
        this.flags.add("password_change_required");
      }

      if (suspended) {
        this.flags.add("is_suspended");
      }
    }
  }

  public static class StringResult {

    public final String value;

    StringResult(String value) {
      this.value = value;
    }
  }
}
