/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise;

import com.github.benmanes.caffeine.cache.Ticker;
import io.nee.dbms.ReplicatedDatabaseEventService;
import io.nee.kernel.enterprise.api.security.EnterpriseAuthManager;
import io.nee.kernel.enterprise.api.security.EnterpriseSecurityContext;
import io.nee.kernel.impl.enterprise.configuration.EnterpriseEditionSettings;
import io.nee.server.security.enterprise.auth.FileRoleRepository;
import io.nee.server.security.enterprise.auth.LdapRealm;
import io.nee.server.security.enterprise.auth.MultiRealmAuthManager;
import io.nee.server.security.enterprise.auth.RoleRepository;
import io.nee.server.security.enterprise.auth.SecurityProcedures;
import io.nee.server.security.enterprise.auth.ShiroCaffeineCache;
import io.nee.server.security.enterprise.auth.UserManagementProcedures;
import io.nee.server.security.enterprise.auth.plugin.PluginRealm;
import io.nee.server.security.enterprise.auth.plugin.spi.AuthPlugin;
import io.nee.server.security.enterprise.auth.plugin.spi.AuthenticationPlugin;
import io.nee.server.security.enterprise.auth.plugin.spi.AuthorizationPlugin;
import io.nee.server.security.enterprise.configuration.SecuritySettings;
import io.nee.server.security.enterprise.log.SecurityLog;
import io.nee.server.security.enterprise.systemgraph.EnterpriseSecurityGraphInitializer;
import io.nee.server.security.enterprise.systemgraph.SystemGraphRealm;
import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.realm.Realm;
import org.neo4j.collection.Dependencies;
import org.neo4j.common.DependencySatisfier;
import org.neo4j.configuration.Config;
import org.neo4j.cypher.internal.security.SecureHasher;
import org.neo4j.dbms.DatabaseManagementSystemSettings;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.dbms.database.SystemGraphInitializer;
import org.neo4j.graphdb.factory.module.DatabaseInitializer;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import org.neo4j.kernel.api.security.AuthManager;
import org.neo4j.kernel.api.security.SecurityModule;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.internal.event.GlobalTransactionEventListeners;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.server.security.auth.CommunitySecurityModule;
import org.neo4j.server.security.auth.FileUserRepository;
import org.neo4j.server.security.auth.UserRepository;
import org.neo4j.server.security.systemgraph.SecurityGraphInitializer;
import org.neo4j.service.Services;
import org.neo4j.time.Clocks;

public class EnterpriseSecurityModule extends SecurityModule {

  private static final String ROLE_STORE_FILENAME = "roles";
  private static final String DEFAULT_ADMIN_STORE_FILENAME = "admin.ini";
  private final Config config;
  private final GlobalProcedures globalProcedures;
  private final JobScheduler scheduler;
  private final LogProvider logProvider;
  private final Log log;
  private final FileSystemAbstraction fileSystem;
  private final DependencySatisfier dependencySatisfier;
  private final GlobalTransactionEventListeners transactionEventListeners;
  private DatabaseManager<?> databaseManager;
  private boolean isClustered;
  private SystemGraphInitializer systemGraphInitializer;
  private EnterpriseAuthManager authManager;
  private EnterpriseSecurityModule.SecurityConfig securityConfig;
  private SecureHasher secureHasher;

  public EnterpriseSecurityModule(LogProvider logProvider, Config config,
      GlobalProcedures procedures, JobScheduler scheduler,
      FileSystemAbstraction fileSystem, DependencySatisfier dependencySatisfier,
      GlobalTransactionEventListeners transactionEventListeners) {
    this.logProvider = logProvider;
    this.config = config;
    this.globalProcedures = procedures;
    this.scheduler = scheduler;
    this.fileSystem = fileSystem;
    this.dependencySatisfier = dependencySatisfier;
    this.transactionEventListeners = transactionEventListeners;
    this.log = logProvider.getLog(this.getClass());
  }

  private static List<Realm> selectOrderedActiveRealms(List<String> configuredRealms,
      List<Realm> availableRealms) {
    List<Realm> orderedActiveRealms = new ArrayList(configuredRealms.size());
    Iterator n3 = configuredRealms.iterator();

    while (true) {
      while (n3.hasNext()) {
        String configuredRealmName = (String) n3.next();
        Iterator n5 = availableRealms.iterator();

        while (n5.hasNext()) {
          Realm realm = (Realm) n5.next();
          if (configuredRealmName.equals(realm.getName())) {
            orderedActiveRealms.add(realm);
            break;
          }
        }
      }

      return orderedActiveRealms;
    }
  }

  private static CacheManager createCacheManager(Config config) {
    long ttl = config.get(SecuritySettings.auth_cache_ttl).toMillis();
    boolean useTTL = config.get(SecuritySettings.auth_cache_use_ttl);
    int maxCapacity = config.get(SecuritySettings.auth_cache_max_capacity);
    return new ShiroCaffeineCache.Manager(Ticker.systemTicker(), ttl, maxCapacity, useTTL);
  }

  private static List<PluginRealm> createPluginRealms(Config config, SecurityLog securityLog,
      SecureHasher secureHasher,
      EnterpriseSecurityModule.SecurityConfig securityConfig) {
    List<PluginRealm> availablePluginRealms = new ArrayList();
    Set<Class> excludedClasses = new HashSet();
    Iterator n6;
    PluginRealm pluginRealm;
    if (securityConfig.pluginAuthentication && securityConfig.pluginAuthorization) {
      n6 = Services.loadAll(AuthPlugin.class).iterator();

      while (n6.hasNext()) {
        AuthPlugin plugin = (AuthPlugin) n6.next();
        pluginRealm = new PluginRealm(plugin, config, securityLog, Clocks.systemClock(),
            secureHasher);
        availablePluginRealms.add(pluginRealm);
      }
    }

    if (securityConfig.pluginAuthentication) {
      for (n6 = Services.loadAll(AuthenticationPlugin.class).iterator(); n6.hasNext();
          availablePluginRealms.add(pluginRealm)) {
        AuthenticationPlugin plugin = (AuthenticationPlugin) n6.next();
        if (securityConfig.pluginAuthorization && plugin instanceof AuthorizationPlugin) {
          pluginRealm = new PluginRealm(plugin, (AuthorizationPlugin) plugin, config, securityLog,
              Clocks.systemClock(), secureHasher);
          excludedClasses.add(plugin.getClass());
        } else {
          pluginRealm = new PluginRealm(plugin, null, config, securityLog, Clocks.systemClock(),
              secureHasher);
        }
      }
    }

    if (securityConfig.pluginAuthorization) {
      n6 = Services.loadAll(AuthorizationPlugin.class).iterator();

      while (n6.hasNext()) {
        AuthorizationPlugin plugin = (AuthorizationPlugin) n6.next();
        if (!excludedClasses.contains(plugin.getClass())) {
          availablePluginRealms.add(
              new PluginRealm(null, plugin, config, securityLog, Clocks.systemClock(),
                  secureHasher));
        }
      }
    }

    n6 = securityConfig.pluginAuthProviders.iterator();

    while (n6.hasNext()) {
      String pluginRealmName = (String) n6.next();
      if (availablePluginRealms.stream().noneMatch((r) ->
      {
        return r.getName().equals(pluginRealmName);
      })) {
        throw illegalConfiguration(
            String.format("Failed to load auth plugin '%s'.", pluginRealmName));
      }
    }

    List<PluginRealm> realms = availablePluginRealms.stream().filter((realm) ->
    {
      return securityConfig.pluginAuthProviders.contains(realm.getName());
    }).collect(Collectors.toList());
    boolean missingAuthenticatingRealm =
        securityConfig.onlyPluginAuthentication() && realms.stream()
            .noneMatch(PluginRealm::canAuthenticate);
    boolean missingAuthorizingRealm = securityConfig.onlyPluginAuthorization() && realms.stream()
        .noneMatch(PluginRealm::canAuthorize);
    if (!missingAuthenticatingRealm && !missingAuthorizingRealm) {
      return realms;
    } else {
      String missingProvider =
          missingAuthenticatingRealm && missingAuthorizingRealm ? "authentication or authorization"
              : (missingAuthenticatingRealm ? "authentication" : "authorization");
      throw illegalConfiguration(String
          .format("No plugin %s provider loaded even though required by configuration.",
              missingProvider));
    }
  }

  private static RoleRepository getRoleRepository(Config config, LogProvider logProvider,
      FileSystemAbstraction fileSystem) {
    return new FileRoleRepository(fileSystem, getRoleRepositoryFile(config), logProvider);
  }

  private static UserRepository getDefaultAdminRepository(Config config, LogProvider logProvider,
      FileSystemAbstraction fileSystem) {
    return new FileUserRepository(fileSystem, getDefaultAdminRepositoryFile(config), logProvider);
  }

  private static File getRoleRepositoryFile(Config config) {
    return new File(config.get(DatabaseManagementSystemSettings.auth_store_directory).toFile(),
        "roles");
  }

  private static File getDefaultAdminRepositoryFile(Config config) {
    return new File(config.get(DatabaseManagementSystemSettings.auth_store_directory).toFile(),
        "admin.ini");
  }

  private static IllegalArgumentException illegalConfiguration(String message) {
    return new IllegalArgumentException("Illegal configuration: " + message);
  }

  static List<String> mergeAuthenticationAndAuthorization(List<String> authenticationProviders,
      List<String> authorizationProviders) {
    Deque<String> authorizationDeque = new ArrayDeque(authorizationProviders);
    List<String> authProviders = new ArrayList();
    Iterator n4 = authenticationProviders.iterator();

    while (true) {
      while (n4.hasNext()) {
        String authenticationProvider = (String) n4.next();
        if (authProviders.contains(authenticationProvider)) {
          throw illegalConfiguration(
              "The relative order of authentication providers and authorization providers must match.");
        }

        if (!authorizationDeque.contains(authenticationProvider)) {
          authProviders.add(authenticationProvider);
        } else {
          while (!authorizationDeque.isEmpty()) {
            String top = authorizationDeque.pop();
            authProviders.add(top);
            if (authenticationProvider.equals(top)) {
              break;
            }
          }
        }
      }

      authProviders.addAll(authorizationDeque);
      return authProviders;
    }
  }

  public void setup() {
    this.secureHasher = new SecureHasher();
    Dependencies platformDependencies = (Dependencies) this.dependencySatisfier;
    this.databaseManager = (DatabaseManager) platformDependencies
        .resolveDependency(DatabaseManager.class);
    this.systemGraphInitializer = platformDependencies
        .resolveDependency(SystemGraphInitializer.class);
    this.isClustered =
        this.config.get(EnterpriseEditionSettings.mode) == EnterpriseEditionSettings.Mode.CORE ||
            this.config.get(EnterpriseEditionSettings.mode)
                == EnterpriseEditionSettings.Mode.READ_REPLICA;
    SecurityLog securityLog = this.createSecurityLog();
    this.life.add(securityLog);
    this.authManager = this.newAuthManager(securityLog);
    this.life.add(this.dependencySatisfier.satisfyDependency(this.authManager));
    AuthCacheClearingDatabaseEventListener databaseEventListener = new AuthCacheClearingDatabaseEventListener(
        this.authManager);
    if (this.isClustered) {
      ReplicatedDatabaseEventService replicatedDatabaseEventService =
          platformDependencies.resolveDependency(ReplicatedDatabaseEventService.class);
      replicatedDatabaseEventService
          .registerListener(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID, databaseEventListener);
    } else {
      this.transactionEventListeners
          .registerTransactionEventListener("system", databaseEventListener);
    }

    this.globalProcedures.registerComponent(SecurityLog.class, (ctx) ->
    {
      return securityLog;
    }, false);
    this.globalProcedures.registerComponent(EnterpriseAuthManager.class, (ctx) ->
    {
      return this.authManager;
    }, false);
    this.globalProcedures.registerComponent(EnterpriseSecurityContext.class, (ctx) ->
    {
      return this.asEnterpriseEdition(ctx.securityContext());
    }, true);
    if (this.securityConfig.nativeAuthEnabled) {
      if (this.config.get(SecuritySettings.authentication_providers).size() <= 1 &&
          this.config.get(SecuritySettings.authorization_providers).size() <= 1) {
        this.registerProcedure(this.globalProcedures, this.log, UserManagementProcedures.class,
            null);
      } else {
        this.registerProcedure(this.globalProcedures, this.log, UserManagementProcedures.class,
            "%s only applies to native users.");
      }
    }

    this.registerProcedure(this.globalProcedures, this.log, SecurityProcedures.class, null);
  }

  private SecurityLog createSecurityLog() {
    try {
      return SecurityLog.create(this.config, this.fileSystem, this.scheduler);
    } catch (IOException | SecurityException n3) {
      String message = "Unable to create security log.";
      this.log.error(message, n3);
      throw new RuntimeException(message, n3);
    }
  }

  public AuthManager authManager() {
    return this.authManager;
  }

  public DatabaseInitializer getDatabaseInitializer() {
    return (database) ->
    {
      Log log = this.logProvider.getLog(this.getClass());
      EnterpriseSecurityGraphInitializer initializer = this.createSecurityInitializer(log);

      try {
        initializer.initializeSecurityGraph(database);
      } catch (Throwable n5) {
        throw new RuntimeException(n5);
      }
    };
  }

  private EnterpriseSecurityContext asEnterpriseEdition(SecurityContext securityContext) {
    if (securityContext instanceof EnterpriseSecurityContext) {
      return (EnterpriseSecurityContext) securityContext;
    } else {
      String n10002 = EnterpriseSecurityContext.class.getName();
      throw new RuntimeException(
          "Expected " + n10002 + ", got " + securityContext.getClass().getName());
    }
  }

  EnterpriseAuthManager newAuthManager(SecurityLog securityLog) {
    this.securityConfig = this.getValidatedSecurityConfig(this.config);
    List<Realm> realms = new ArrayList(this.securityConfig.authProviders.size() + 1);
    SecureHasher secureHasher = new SecureHasher();
    SystemGraphRealm internalRealm = this.createSystemGraphRealm(this.config, securityLog);
    realms.add(internalRealm);
    if (this.securityConfig.hasLdapProvider) {
      realms.add(
          new LdapRealm(this.config, securityLog, secureHasher,
              this.securityConfig.ldapAuthentication, this.securityConfig.ldapAuthorization));
    }

    if (!this.securityConfig.pluginAuthProviders.isEmpty()) {
      realms
          .addAll(createPluginRealms(this.config, securityLog, secureHasher, this.securityConfig));
    }

    List<Realm> orderedActiveRealms = selectOrderedActiveRealms(this.securityConfig.authProviders,
        realms);
    if (orderedActiveRealms.isEmpty()) {
      throw illegalConfiguration("No valid auth provider is active.");
    } else {
      return new MultiRealmAuthManager(internalRealm, orderedActiveRealms,
          createCacheManager(this.config), securityLog,
          this.config.get(SecuritySettings.security_log_successful_authentication));
    }
  }

  private EnterpriseSecurityModule.SecurityConfig getValidatedSecurityConfig(Config config) {
    EnterpriseSecurityModule.SecurityConfig securityConfig = new EnterpriseSecurityModule.SecurityConfig(
        config);
    securityConfig.validate();
    return securityConfig;
  }

  private EnterpriseSecurityGraphInitializer createSecurityInitializer(Log log) {
    UserRepository migrationUserRepository = CommunitySecurityModule
        .getUserRepository(this.config, this.logProvider, this.fileSystem);
    RoleRepository migrationRoleRepository = getRoleRepository(this.config, this.logProvider,
        this.fileSystem);
    UserRepository initialUserRepository = CommunitySecurityModule
        .getInitialUserRepository(this.config, this.logProvider, this.fileSystem);
    UserRepository defaultAdminRepository = getDefaultAdminRepository(this.config, this.logProvider,
        this.fileSystem);
    return new EnterpriseSecurityGraphInitializer(this.databaseManager, this.systemGraphInitializer,
        log, migrationUserRepository, migrationRoleRepository,
        initialUserRepository, defaultAdminRepository, this.secureHasher);
  }

  private SystemGraphRealm createSystemGraphRealm(Config config, SecurityLog securityLog) {
    SecurityGraphInitializer securityGraphInitializer =
        this.isClustered ? SecurityGraphInitializer.NO_OP
            : this.createSecurityInitializer(securityLog);
    return new SystemGraphRealm(securityGraphInitializer, this.databaseManager, this.secureHasher,
        CommunitySecurityModule.createAuthenticationStrategy(config),
        this.securityConfig.nativeAuthentication,
        this.securityConfig.nativeAuthorization);
  }

  protected static class SecurityConfig {

    final List<String> authProviders;
    final Set<String> pluginAuthProviders;
    final List<String> pluginAuthenticationProviders;
    final List<String> pluginAuthorizationProviders;
    final boolean nativeAuthentication;
    final boolean nativeAuthorization;
    final boolean ldapAuthentication;
    final boolean ldapAuthorization;
    final boolean pluginAuthentication;
    final boolean pluginAuthorization;
    final boolean nativeAuthEnabled;
    private final boolean propertyAuthorization;
    private final String propertyAuthMapping;
    boolean hasNativeProvider;
    boolean hasLdapProvider;

    SecurityConfig(Config config) {
      List<String> authenticationProviders = new ArrayList(
          config.get(SecuritySettings.authentication_providers));
      List<String> authorizationProviders = new ArrayList(
          config.get(SecuritySettings.authorization_providers));
      this.authProviders = EnterpriseSecurityModule
          .mergeAuthenticationAndAuthorization(authenticationProviders, authorizationProviders);
      this.hasNativeProvider =
          authenticationProviders.contains("native") || authorizationProviders.contains("native");
      this.hasLdapProvider =
          authenticationProviders.contains("ldap") || authorizationProviders.contains("ldap");
      this.pluginAuthenticationProviders = authenticationProviders.stream().filter((r) ->
      {
        return r.startsWith("plugin-");
      }).collect(Collectors.toList());
      this.pluginAuthorizationProviders = authorizationProviders.stream().filter((r) ->
      {
        return r.startsWith("plugin-");
      }).collect(Collectors.toList());
      this.pluginAuthProviders = new HashSet();
      this.pluginAuthProviders.addAll(this.pluginAuthenticationProviders);
      this.pluginAuthProviders.addAll(this.pluginAuthorizationProviders);
      this.nativeAuthentication = authenticationProviders.contains("native");
      this.nativeAuthorization = authorizationProviders.contains("native");
      this.nativeAuthEnabled = this.nativeAuthentication || this.nativeAuthorization;
      this.ldapAuthentication = authenticationProviders.contains("ldap");
      this.ldapAuthorization = authorizationProviders.contains("ldap");
      this.pluginAuthentication = !this.pluginAuthenticationProviders.isEmpty();
      this.pluginAuthorization = !this.pluginAuthorizationProviders.isEmpty();
      this.propertyAuthorization = config
          .get(SecuritySettings.property_level_authorization_enabled);
      this.propertyAuthMapping = config
          .get(SecuritySettings.property_level_authorization_permissions);
    }

    protected void validate() {
      if (!this.nativeAuthentication && !this.ldapAuthentication && !this.pluginAuthentication) {
        throw EnterpriseSecurityModule.illegalConfiguration("No authentication provider found.");
      } else if (!this.nativeAuthorization && !this.ldapAuthorization
          && !this.pluginAuthorization) {
        throw EnterpriseSecurityModule.illegalConfiguration("No authorization provider found.");
      } else if (this.propertyAuthorization || this.propertyAuthMapping != null) {
        throw EnterpriseSecurityModule.illegalConfiguration(
            "Property level blacklisting through configuration setting has been replaced by privilege management on roles, e.g. 'DENY READ {property} ON GRAPH * ELEMENTS * TO role'.");
      }
    }

    boolean onlyPluginAuthentication() {
      return !this.nativeAuthentication && !this.ldapAuthentication && this.pluginAuthentication;
    }

    boolean onlyPluginAuthorization() {
      return !this.nativeAuthorization && !this.ldapAuthorization && this.pluginAuthorization;
    }
  }
}
