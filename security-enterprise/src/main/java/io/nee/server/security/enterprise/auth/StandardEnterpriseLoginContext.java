/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import io.nee.kernel.enterprise.api.security.EnterpriseLoginContext;
import io.nee.kernel.enterprise.api.security.EnterpriseSecurityContext;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.shiro.authz.AuthorizationInfo;
import org.neo4j.graphdb.security.AuthorizationViolationException;
import org.neo4j.internal.kernel.api.security.AuthSubject;
import org.neo4j.internal.kernel.api.security.AuthenticationResult;
import org.neo4j.kernel.api.exceptions.Status.Security;

public class StandardEnterpriseLoginContext implements EnterpriseLoginContext {

  private final MultiRealmAuthManager authManager;
  private final ShiroSubject shiroSubject;
  private final StandardEnterpriseLoginContext.NeoShiroSubject neoShiroSubject;

  StandardEnterpriseLoginContext(MultiRealmAuthManager authManager, ShiroSubject shiroSubject) {
    this.authManager = authManager;
    this.shiroSubject = shiroSubject;
    this.neoShiroSubject = new StandardEnterpriseLoginContext.NeoShiroSubject();
  }

  private static String buildMessageFromThrowables(String baseMessage, List<Throwable> throwables) {
    if (throwables == null) {
      return baseMessage;
    } else {
      StringBuilder sb = new StringBuilder(baseMessage);
      Iterator n3 = throwables.iterator();

      while (n3.hasNext()) {
        Throwable t = (Throwable) n3.next();
        if (t.getMessage() != null) {
          sb.append(" (");
          sb.append(t.getMessage());
          sb.append(")");
        }

        Throwable cause = t.getCause();
        if (cause != null && cause.getMessage() != null) {
          sb.append(" (");
          sb.append(cause.getMessage());
          sb.append(")");
        }

        Throwable causeCause = cause != null ? cause.getCause() : null;
        if (causeCause != null && causeCause.getMessage() != null) {
          sb.append(" (");
          sb.append(causeCause.getMessage());
          sb.append(")");
        }
      }

      return sb.toString();
    }
  }

  public AuthSubject subject() {
    return this.neoShiroSubject;
  }

  private StandardAccessMode mode(IdLookup resolver, String dbName) {
    boolean isAuthenticated = this.shiroSubject.isAuthenticated();
    boolean passwordChangeRequired = this.shiroSubject.getAuthenticationResult()
        == AuthenticationResult.PASSWORD_CHANGE_REQUIRED;
    Set<String> roles = this.queryForRoleNames();
    StandardAccessMode.Builder accessModeBuilder = new StandardAccessMode.Builder(isAuthenticated,
        passwordChangeRequired, roles, resolver, dbName);
    Set<ResourcePrivilege> privileges = this.authManager.getPermissions(roles);
    Iterator n8 = privileges.iterator();

    while (n8.hasNext()) {
      ResourcePrivilege privilege = (ResourcePrivilege) n8.next();
      if (privilege.appliesTo(dbName)) {
        accessModeBuilder.addPrivilege(privilege);
      }
    }

    if (dbName.equals("system")) {
      accessModeBuilder.withAccess();
    }

    StandardAccessMode mode = accessModeBuilder.build();
    if (!mode.allowsAccess()) {
      throw mode.onViolation(
          String.format("Database access is not allowed for user '%s' with roles %s.",
              this.neoShiroSubject.username(), roles.toString()));
    } else {
      return mode;
    }
  }

  public EnterpriseSecurityContext authorize(IdLookup idLookup, String dbName) {
    if (!this.shiroSubject.isAuthenticated()) {
      throw new AuthorizationViolationException("Permission denied.", Security.Unauthorized);
    } else {
      StandardAccessMode mode = this.mode(idLookup, dbName);
      return new EnterpriseSecurityContext(this.neoShiroSubject, mode, mode.roles(),
          mode.getAdminAccessMode());
    }
  }

  public Set<String> roles() {
    return this.queryForRoleNames();
  }

  private Set<String> queryForRoleNames() {
    Collection<AuthorizationInfo> authorizationInfo = this.authManager
        .getAuthorizationInfo(this.shiroSubject.getPrincipals());
    return authorizationInfo.stream().flatMap((authInfo) ->
    {
      Collection<String> roles = authInfo.getRoles();
      return roles == null ? Stream.empty() : roles.stream();
    }).collect(Collectors.toSet());
  }

  class NeoShiroSubject implements AuthSubject {

    public String username() {
      Object principal = StandardEnterpriseLoginContext.this.shiroSubject.getPrincipal();
      return principal != null ? principal.toString() : "";
    }

    public void logout() {
      StandardEnterpriseLoginContext.this.shiroSubject.logout();
    }

    public AuthenticationResult getAuthenticationResult() {
      return StandardEnterpriseLoginContext.this.shiroSubject.getAuthenticationResult();
    }

    public void setPasswordChangeNoLongerRequired() {
      if (this.getAuthenticationResult() == AuthenticationResult.PASSWORD_CHANGE_REQUIRED) {
        StandardEnterpriseLoginContext.this.shiroSubject
            .setAuthenticationResult(AuthenticationResult.SUCCESS);
      }
    }

    public boolean hasUsername(String username) {
      Object principal = StandardEnterpriseLoginContext.this.shiroSubject.getPrincipal();
      return username != null && username.equals(principal);
    }

    String getAuthenticationFailureMessage() {
      String message = "";
      List<Throwable> throwables = StandardEnterpriseLoginContext.this.shiroSubject
          .getAuthenticationInfo().getThrowables();
      switch (StandardEnterpriseLoginContext.this.shiroSubject.getAuthenticationResult()) {
        case FAILURE:
          message = StandardEnterpriseLoginContext
              .buildMessageFromThrowables("invalid principal or credentials", throwables);
          break;
        case TOO_MANY_ATTEMPTS:
          message = StandardEnterpriseLoginContext
              .buildMessageFromThrowables("too many failed attempts", throwables);
          break;
        case PASSWORD_CHANGE_REQUIRED:
          message = StandardEnterpriseLoginContext
              .buildMessageFromThrowables("password change required", throwables);
      }

      return message;
    }

    void clearAuthenticationInfo() {
      StandardEnterpriseLoginContext.this.shiroSubject.clearAuthenticationInfo();
    }
  }
}
