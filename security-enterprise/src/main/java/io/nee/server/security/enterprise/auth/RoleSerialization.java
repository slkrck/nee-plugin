/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import java.util.SortedSet;
import java.util.TreeSet;
import org.neo4j.cypher.internal.security.FormatException;
import org.neo4j.server.security.auth.FileRepositorySerializer;

public class RoleSerialization extends FileRepositorySerializer<RoleRecord> {

  private static final String roleSeparator = ":";
  private static final String userSeparator = ",";

  protected String serialize(RoleRecord role) {
    return String.join(":", role.name(), String.join(",", role.users()));
  }

  protected RoleRecord deserializeRecord(String line, int lineNumber) throws FormatException {
    String[] parts = line.split(":", -1);
    if (parts.length != 2) {
      throw new FormatException(String.format("wrong number of line fields [line %d]", lineNumber));
    } else {
      return (new RoleRecord.Builder()).withName(parts[0])
          .withUsers(this.deserializeUsers(parts[1])).build();
    }
  }

  private SortedSet<String> deserializeUsers(String part) {
    String[] splits = part.split(",", -1);
    SortedSet<String> users = new TreeSet();
    String[] n4 = splits;
    int n5 = splits.length;

    for (int n6 = 0; n6 < n5; ++n6) {
      String user = n4[n6];
      if (!user.trim().isEmpty()) {
        users.add(user);
      }
    }

    return users;
  }
}
