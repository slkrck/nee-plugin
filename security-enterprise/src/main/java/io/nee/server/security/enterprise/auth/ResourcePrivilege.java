/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import org.neo4j.internal.kernel.api.security.PrivilegeAction;
import org.neo4j.kernel.api.exceptions.InvalidArgumentsException;

public class ResourcePrivilege {

  private final ResourcePrivilege.GrantOrDeny privilegeType;
  private final PrivilegeAction action;
  private final Resource resource;
  private final Segment segment;
  private final String dbName;
  private final boolean allDatabases;

  public ResourcePrivilege(ResourcePrivilege.GrantOrDeny privilegeType, PrivilegeAction action,
      Resource resource, Segment segment)
      throws InvalidArgumentsException {
    this.privilegeType = privilegeType;
    this.action = action;
    this.resource = resource;
    this.segment = segment;
    this.dbName = "";
    this.allDatabases = true;
    resource.assertValidCombination(action);
  }

  public ResourcePrivilege(ResourcePrivilege.GrantOrDeny privilegeType, PrivilegeAction action,
      Resource resource, Segment segment, String dbName)
      throws InvalidArgumentsException {
    this.privilegeType = privilegeType;
    this.action = action;
    this.resource = resource;
    this.segment = segment;
    this.dbName = dbName;
    this.allDatabases = false;
    resource.assertValidCombination(action);
  }

  boolean appliesTo(String database) {
    if (database.equals("system") && PrivilegeAction.ADMIN.satisfies(this.action)) {
      return true;
    } else {
      return this.allDatabases || database.equals(this.dbName);
    }
  }

  ResourcePrivilege.GrantOrDeny getPrivilegeType() {
    return this.privilegeType;
  }

  public Resource getResource() {
    return this.resource;
  }

  public PrivilegeAction getAction() {
    return this.action;
  }

  public Segment getSegment() {
    return this.segment;
  }

  public String getDbName() {
    return this.dbName;
  }

  public boolean isAllDatabases() {
    return this.allDatabases;
  }

  public String toString() {
    return String
        .format("(%s, %s, %s, %s)", this.privilegeType.prefix, this.getAction(), this.getResource(),
            this.getSegment());
  }

  public int hashCode() {
    return this.action.hashCode() + 31 * this.resource.hashCode();
  }

  public boolean equals(Object obj) {
    if (!(obj instanceof ResourcePrivilege)) {
      return false;
    } else {
      ResourcePrivilege other = (ResourcePrivilege) obj;
      return other.action.equals(this.action) && other.resource.equals(this.resource)
          && other.segment.equals(this.segment) &&
          other.dbName.equals(this.dbName) && other.privilegeType == this.privilegeType
          && other.allDatabases == this.allDatabases;
    }
  }

  public enum GrantOrDeny {
    GRANT("GRANTED"),
    DENY("DENIED");

    public final String name = super.toString().toLowerCase();
    public final String prefix = super.toString().toUpperCase();
    public final String relType;

    GrantOrDeny(String relType) {
      this.relType = relType;
    }

    public static ResourcePrivilege.GrantOrDeny fromRelType(String relType) {
      ResourcePrivilege.GrantOrDeny[] n1 = values();
      int n2 = n1.length;

      for (int n3 = 0; n3 < n2; ++n3) {
        ResourcePrivilege.GrantOrDeny grantOrDeny = n1[n3];
        if (grantOrDeny.relType.equals(relType)) {
          return grantOrDeny;
        }
      }

      throw new IllegalArgumentException("Unknown privilege type: " + relType);
    }

    public boolean isGrant() {
      return this == GRANT;
    }

    public boolean isDeny() {
      return this == DENY;
    }

    public String toString() {
      return this.name;
    }
  }
}
