/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.neo4j.kernel.api.exceptions.InvalidArgumentsException;
import org.neo4j.kernel.impl.security.User;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.server.security.auth.ListSnapshot;
import org.neo4j.server.security.auth.exception.ConcurrentModificationException;

public interface RoleRepository extends Lifecycle {

  static boolean validate(List<User> users, List<RoleRecord> roles) {
    Set<String> usernamesInRoles = roles.stream().flatMap((rr) ->
    {
      return rr.users().stream();
    }).collect(Collectors.toSet());
    Set<String> usernameInUsers = users.stream().map(User::name).collect(Collectors.toSet());
    return usernameInUsers.containsAll(usernamesInRoles);
  }

  RoleRecord getRoleByName(String n1);

  Set<String> getRoleNamesByUsername(String n1);

  void clear();

  void create(RoleRecord n1) throws InvalidArgumentsException, IOException;

  void setRoles(ListSnapshot<RoleRecord> n1) throws InvalidArgumentsException;

  void update(RoleRecord n1, RoleRecord n2) throws ConcurrentModificationException, IOException;

  boolean delete(RoleRecord n1) throws IOException;

  int numberOfRoles();

  void assertValidRoleName(String n1) throws InvalidArgumentsException;

  void removeUserFromAllRoles(String n1) throws ConcurrentModificationException, IOException;

  Set<String> getAllRoleNames();

  ListSnapshot<RoleRecord> getPersistedSnapshot() throws IOException;

  void purge() throws IOException;

  void markAsMigrated() throws IOException;
}
