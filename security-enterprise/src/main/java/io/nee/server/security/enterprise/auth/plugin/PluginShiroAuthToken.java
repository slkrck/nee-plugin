/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth.plugin;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import org.apache.shiro.authc.AuthenticationToken;
import org.neo4j.server.security.auth.ShiroAuthToken;

public class PluginShiroAuthToken extends ShiroAuthToken {

  private final char[] credentials;

  private PluginShiroAuthToken(Map<String, Object> authTokenMap) {
    super(authTokenMap);
    byte[] credentialsBytes = (byte[]) super.getCredentials();
    this.credentials =
        credentialsBytes != null ? StandardCharsets.UTF_8.decode(ByteBuffer.wrap(credentialsBytes))
            .array() : null;
  }

  public static PluginShiroAuthToken of(ShiroAuthToken shiroAuthToken) {
    return new PluginShiroAuthToken(shiroAuthToken.getAuthTokenMap());
  }

  public static PluginShiroAuthToken of(AuthenticationToken authenticationToken) {
    ShiroAuthToken shiroAuthToken = (ShiroAuthToken) authenticationToken;
    return of(shiroAuthToken);
  }

  public Object getCredentials() {
    return this.credentials;
  }

  void clearCredentials() {
    if (this.credentials != null) {
      Arrays.fill(this.credentials, '\u0000');
    }
  }
}
