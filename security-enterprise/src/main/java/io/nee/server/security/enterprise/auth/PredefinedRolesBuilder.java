/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.shiro.authz.SimpleRole;
import org.apache.shiro.authz.permission.WildcardPermission;

public class PredefinedRolesBuilder implements RolesBuilder {

  public static final WildcardPermission SYSTEM = new WildcardPermission("system:*");
  public static final WildcardPermission SCHEMA = new WildcardPermission("database:*:*:schema");
  public static final WildcardPermission TOKEN = new WildcardPermission("database:*:*:token");
  public static final WildcardPermission WRITE = new WildcardPermission("database:*:write:graph");
  public static final WildcardPermission READ = new WildcardPermission("database:*:read:graph");
  public static final WildcardPermission ACCESS = new WildcardPermission("database:*:access:graph");
  public static final Map<String, SimpleRole> roles;
  private static final Map<String, SimpleRole> innerRoles = staticBuildRoles();

  static {
    roles = Collections.unmodifiableMap(innerRoles);
  }

  private static Map<String, SimpleRole> staticBuildRoles() {
    Map<String, SimpleRole> roles = new ConcurrentHashMap(4);
    SimpleRole admin = new SimpleRole("admin");
    admin.add(SYSTEM);
    admin.add(SCHEMA);
    admin.add(TOKEN);
    admin.add(WRITE);
    admin.add(READ);
    admin.add(ACCESS);
    roles.put("admin", admin);
    SimpleRole architect = new SimpleRole("architect");
    architect.add(SCHEMA);
    architect.add(TOKEN);
    architect.add(WRITE);
    architect.add(READ);
    architect.add(ACCESS);
    roles.put("architect", architect);
    SimpleRole publisher = new SimpleRole("publisher");
    publisher.add(TOKEN);
    publisher.add(WRITE);
    publisher.add(READ);
    publisher.add(ACCESS);
    roles.put("publisher", publisher);
    SimpleRole editor = new SimpleRole("editor");
    editor.add(WRITE);
    editor.add(READ);
    editor.add(ACCESS);
    roles.put("editor", editor);
    SimpleRole reader = new SimpleRole("reader");
    reader.add(READ);
    reader.add(ACCESS);
    roles.put("reader", reader);
    return roles;
  }

  public Map<String, SimpleRole> buildRoles() {
    return roles;
  }
}
