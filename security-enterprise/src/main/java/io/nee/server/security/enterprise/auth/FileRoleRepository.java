/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import java.io.File;
import java.io.IOException;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import org.neo4j.cypher.internal.security.FormatException;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.server.security.auth.FileRepository;
import org.neo4j.server.security.auth.ListSnapshot;

public class FileRoleRepository extends AbstractRoleRepository implements FileRepository {

  private final File roleFile;
  private final Log log;
  private final RoleSerialization serialization = new RoleSerialization();
  private final FileSystemAbstraction fileSystem;

  public FileRoleRepository(FileSystemAbstraction fileSystem, File file, LogProvider logProvider) {
    this.roleFile = file;
    this.log = logProvider.getLog(this.getClass());
    this.fileSystem = fileSystem;
  }

  public void start() throws Exception {
    this.clear();
    FileRepository.assertNotMigrated(this.roleFile, this.fileSystem, this.log);
    ListSnapshot<RoleRecord> onDiskRoles = this.readPersistedRoles();
    if (onDiskRoles != null) {
      this.setRoles(onDiskRoles);
    }
  }

  protected ListSnapshot<RoleRecord> readPersistedRoles() throws IOException {
    if (this.fileSystem.fileExists(this.roleFile)) {
      long readTime;
      List readRoles;
      try {
        readTime = this.fileSystem.lastModifiedTime(this.roleFile);
        readRoles = this.serialization.loadRecordsFromFile(this.fileSystem, this.roleFile);
      } catch (FormatException n5) {
        this.log.error("Failed to read role file \"%s\" (%s)", this.roleFile.getAbsolutePath(),
            n5.getMessage());
        throw new IllegalStateException("Failed to read role file '" + this.roleFile + "'.");
      }

      return new ListSnapshot(readTime, readRoles, true);
    } else {
      return null;
    }
  }

  protected void persistRoles() throws IOException {
    this.serialization.saveRecordsToFile(this.fileSystem, this.roleFile, this.roles);
  }

  public ListSnapshot<RoleRecord> getPersistedSnapshot() throws IOException {
    if (this.lastLoaded.get() < this.fileSystem.lastModifiedTime(this.roleFile)) {
      return this.readPersistedRoles();
    } else {
      synchronized (this) {
        return new ListSnapshot(this.lastLoaded.get(), new ArrayList(this.roles), false);
      }
    }
  }

  public void purge() throws IOException {
    super.purge();
    if (!this.fileSystem.deleteFile(this.roleFile)) {
      throw new IOException("Failed to delete file '" + this.roleFile.getAbsolutePath() + "'");
    }
  }

  public void markAsMigrated() throws IOException {
    super.markAsMigrated();
    File destinationFile = FileRepository.getMigratedFile(this.roleFile);
    this.fileSystem.renameFile(this.roleFile, destinationFile, StandardCopyOption.REPLACE_EXISTING,
        StandardCopyOption.COPY_ATTRIBUTES);
  }
}
