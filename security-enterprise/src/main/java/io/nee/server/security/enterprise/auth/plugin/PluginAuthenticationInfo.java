/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth.plugin;

import io.nee.server.security.enterprise.auth.plugin.spi.AuthenticationInfo;
import io.nee.server.security.enterprise.auth.plugin.spi.CacheableAuthenticationInfo;
import io.nee.server.security.enterprise.auth.plugin.spi.CustomCacheableAuthenticationInfo;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.neo4j.cypher.internal.security.SecureHasher;
import org.neo4j.internal.kernel.api.security.AuthenticationResult;
import org.neo4j.server.security.auth.ShiroAuthenticationInfo;

class PluginAuthenticationInfo extends ShiroAuthenticationInfo implements
    CustomCredentialsMatcherSupplier {

  private CustomCacheableAuthenticationInfo.CredentialsMatcher credentialsMatcher;

  private PluginAuthenticationInfo(Object principal, String realmName,
      CustomCacheableAuthenticationInfo.CredentialsMatcher credentialsMatcher) {
    super(principal, realmName, AuthenticationResult.SUCCESS);
    this.credentialsMatcher = credentialsMatcher;
  }

  private PluginAuthenticationInfo(Object principal, Object hashedCredentials,
      ByteSource credentialsSalt, String realmName) {
    super(principal, hashedCredentials, credentialsSalt, realmName, AuthenticationResult.SUCCESS);
  }

  private static PluginAuthenticationInfo create(AuthenticationInfo authenticationInfo,
      String realmName) {
    return new PluginAuthenticationInfo(authenticationInfo.principal(), realmName, null);
  }

  private static PluginAuthenticationInfo create(AuthenticationInfo authenticationInfo,
      SimpleHash hashedCredentials, String realmName) {
    return new PluginAuthenticationInfo(authenticationInfo.principal(),
        hashedCredentials.getBytes(), hashedCredentials.getSalt(), realmName);
  }

  public static PluginAuthenticationInfo createCacheable(AuthenticationInfo authenticationInfo,
      String realmName, SecureHasher secureHasher) {
    if (authenticationInfo instanceof CustomCacheableAuthenticationInfo) {
      CustomCacheableAuthenticationInfo info = (CustomCacheableAuthenticationInfo) authenticationInfo;
      return new PluginAuthenticationInfo(authenticationInfo.principal(), realmName,
          info.credentialsMatcher());
    } else if (authenticationInfo instanceof CacheableAuthenticationInfo) {
      byte[] credentials = ((CacheableAuthenticationInfo) authenticationInfo).credentials();
      SimpleHash hashedCredentials = secureHasher.hash(credentials);
      return create(authenticationInfo, hashedCredentials, realmName);
    } else {
      return create(authenticationInfo, realmName);
    }
  }

  public CustomCacheableAuthenticationInfo.CredentialsMatcher getCredentialsMatcher() {
    return this.credentialsMatcher;
  }
}
