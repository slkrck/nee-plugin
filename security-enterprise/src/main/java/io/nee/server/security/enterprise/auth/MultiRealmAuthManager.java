/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import io.nee.kernel.enterprise.api.security.EnterpriseAuthManager;
import io.nee.kernel.enterprise.api.security.EnterpriseLoginContext;
import io.nee.server.security.enterprise.log.SecurityLog;
import io.nee.server.security.enterprise.systemgraph.SystemGraphRealm;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.authc.pam.UnsupportedTokenException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.DefaultSessionStorageEvaluator;
import org.apache.shiro.mgt.DefaultSubjectDAO;
import org.apache.shiro.mgt.SubjectDAO;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.realm.CachingRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.Initializable;
import org.neo4j.graphdb.security.AuthProviderFailedException;
import org.neo4j.graphdb.security.AuthProviderTimeoutException;
import org.neo4j.internal.helpers.Strings;
import org.neo4j.internal.kernel.api.security.AuthenticationResult;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.kernel.api.security.AuthToken;
import org.neo4j.kernel.api.security.exception.InvalidAuthTokenException;
import org.neo4j.server.security.auth.ShiroAuthToken;

public class MultiRealmAuthManager implements EnterpriseAuthManager {

  private final SystemGraphRealm systemGraphRealm;
  private final Collection<Realm> realms;
  private final DefaultSecurityManager securityManager;
  private final CacheManager cacheManager;
  private final SecurityLog securityLog;
  private final boolean logSuccessfulLogin;

  public MultiRealmAuthManager(SystemGraphRealm systemGraphRealm, Collection<Realm> realms,
      CacheManager cacheManager, SecurityLog securityLog,
      boolean logSuccessfulLogin) {
    this.systemGraphRealm = systemGraphRealm;
    this.realms = realms;
    this.cacheManager = cacheManager;
    this.securityManager = new DefaultSecurityManager(realms);
    this.securityLog = securityLog;
    this.logSuccessfulLogin = logSuccessfulLogin;
    this.securityManager.setSubjectFactory(new ShiroSubjectFactory());
    ((ModularRealmAuthenticator) this.securityManager.getAuthenticator())
        .setAuthenticationStrategy(new ShiroAuthenticationStrategy());
    this.securityManager.setSubjectDAO(this.createSubjectDAO());
  }

  private SubjectDAO createSubjectDAO() {
    DefaultSubjectDAO subjectDAO = new DefaultSubjectDAO();
    DefaultSessionStorageEvaluator sessionStorageEvaluator = new DefaultSessionStorageEvaluator();
    sessionStorageEvaluator.setSessionStorageEnabled(false);
    subjectDAO.setSessionStorageEvaluator(sessionStorageEvaluator);
    return subjectDAO;
  }

  public EnterpriseLoginContext login(Map<String, Object> authToken)
      throws InvalidAuthTokenException {
    StandardEnterpriseLoginContext n17;
    try {
      ShiroAuthToken token = new ShiroAuthToken(authToken);
      this.assertValidScheme(token);

      StandardEnterpriseLoginContext securityContext;
      Throwable cause;
      try {
        securityContext = new StandardEnterpriseLoginContext(this,
            (ShiroSubject) this.securityManager.login(null, token));
        AuthenticationResult authenticationResult = securityContext.subject()
            .getAuthenticationResult();
        if (authenticationResult == AuthenticationResult.SUCCESS) {
          if (this.logSuccessfulLogin) {
            this.securityLog.info(securityContext.subject(), "logged in");
          }
        } else if (authenticationResult == AuthenticationResult.PASSWORD_CHANGE_REQUIRED) {
          this.securityLog.info(securityContext.subject(), "logged in (password change required)");
        } else {
          String errorMessage = ((StandardEnterpriseLoginContext.NeoShiroSubject) securityContext
              .subject()).getAuthenticationFailureMessage();
          this.securityLog
              .error("[%s]: failed to log in: %s", Strings.escape(token.getPrincipal().toString()),
                  errorMessage);
        }

        ((StandardEnterpriseLoginContext.NeoShiroSubject) securityContext.subject())
            .clearAuthenticationInfo();
      } catch (UnsupportedTokenException n13) {
        this.securityLog.error("Unknown user failed to log in: %s", n13.getMessage());
        cause = n13.getCause();
        if (cause instanceof InvalidAuthTokenException) {
          String n10002 = cause.getMessage();
          throw new InvalidAuthTokenException(n10002 + ": " + token);
        }

        throw AuthToken.invalidToken(": " + token);
      } catch (ExcessiveAttemptsException n14) {
        securityContext = new StandardEnterpriseLoginContext(this,
            new ShiroSubject(this.securityManager, AuthenticationResult.TOO_MANY_ATTEMPTS));
        this.securityLog.error("[%s]: failed to log in: too many failed attempts",
            Strings.escape(token.getPrincipal().toString()));
      } catch (AuthenticationException n15) {
        if (n15.getCause() != null && n15.getCause() instanceof AuthProviderTimeoutException) {
          cause = n15.getCause().getCause();
          this.securityLog.error("[%s]: failed to log in: auth server timeout%s",
              Strings.escape(token.getPrincipal().toString()),
              cause != null && cause.getMessage() != null ? " (" + cause.getMessage() + ")" : "");
          throw new AuthProviderTimeoutException(n15.getCause().getMessage(), n15.getCause());
        }

        if (n15.getCause() != null && n15.getCause() instanceof AuthProviderFailedException) {
          cause = n15.getCause().getCause();
          this.securityLog.error("[%s]: failed to log in: auth server connection refused%s",
              Strings.escape(token.getPrincipal().toString()),
              cause != null && cause.getMessage() != null ? " (" + cause.getMessage() + ")" : "");
          throw new AuthProviderFailedException(n15.getCause().getMessage(), n15.getCause());
        }

        securityContext = new StandardEnterpriseLoginContext(this,
            new ShiroSubject(this.securityManager, AuthenticationResult.FAILURE));
        cause = n15.getCause();
        Throwable causeCause = n15.getCause() != null ? n15.getCause().getCause() : null;
        String errorMessage = String.format("invalid principal or credentials%s%s",
            cause != null && cause.getMessage() != null ? " (" + cause.getMessage() + ")" : "",
            causeCause != null && causeCause.getMessage() != null ? " (" + causeCause.getMessage()
                + ")" : "");
        this.securityLog
            .error("[%s]: failed to log in: %s", Strings.escape(token.getPrincipal().toString()),
                errorMessage);
      }

      n17 = securityContext;
    } finally {
      AuthToken.clearCredentials(authToken);
    }

    return n17;
  }

  public void log(String message, SecurityContext securityContext) {
    this.securityLog.info(securityContext.subject(), message);
  }

  private void assertValidScheme(ShiroAuthToken token) throws InvalidAuthTokenException {
    String scheme = token.getSchemeSilently();
    if (scheme == null) {
      throw AuthToken.invalidToken("missing key `scheme`: " + token);
    } else if (scheme.equals("none")) {
      throw AuthToken.invalidToken("scheme='none' only allowed when auth is disabled: " + token);
    }
  }

  public void init() throws Exception {
    boolean initUserManager = true;
    Iterator n2 = this.realms.iterator();

    while (n2.hasNext()) {
      Realm realm = (Realm) n2.next();
      if (this.systemGraphRealm == realm) {
        initUserManager = false;
      }

      if (realm instanceof Initializable) {
        ((Initializable) realm).init();
      }

      if (realm instanceof CachingRealm) {
        ((CachingRealm) realm).setCacheManager(this.cacheManager);
      }

      if (realm instanceof RealmLifecycle) {
        ((RealmLifecycle) realm).initialize();
      }
    }

    if (initUserManager) {
      this.systemGraphRealm.init();
      this.systemGraphRealm.setCacheManager(this.cacheManager);
      this.systemGraphRealm.initialize();
    }
  }

  public void start() throws Exception {
    boolean startUserManager = true;
    Iterator n2 = this.realms.iterator();

    while (n2.hasNext()) {
      Realm realm = (Realm) n2.next();
      if (this.systemGraphRealm == realm) {
        startUserManager = false;
      }

      if (realm instanceof RealmLifecycle) {
        ((RealmLifecycle) realm).start();
      }
    }

    if (startUserManager) {
      this.systemGraphRealm.start();
    }
  }

  public void stop() throws Exception {
    Iterator n1 = this.realms.iterator();

    while (n1.hasNext()) {
      Realm realm = (Realm) n1.next();
      if (realm instanceof RealmLifecycle) {
        ((RealmLifecycle) realm).stop();
      }
    }
  }

  public void shutdown() throws Exception {
    Iterator n1 = this.realms.iterator();

    while (n1.hasNext()) {
      Realm realm = (Realm) n1.next();
      if (realm instanceof CachingRealm) {
        ((CachingRealm) realm).setCacheManager(null);
      }

      if (realm instanceof RealmLifecycle) {
        ((RealmLifecycle) realm).shutdown();
      }
    }
  }

  public void clearAuthCache() {
    Iterator n1 = this.realms.iterator();

    while (n1.hasNext()) {
      Realm realm = (Realm) n1.next();
      Cache cache;
      if (realm instanceof AuthenticatingRealm) {
        cache = ((AuthenticatingRealm) realm).getAuthenticationCache();
        if (cache != null) {
          cache.clear();
        }
      }

      if (realm instanceof AuthorizingRealm) {
        cache = ((AuthorizingRealm) realm).getAuthorizationCache();
        if (cache != null) {
          cache.clear();
        }
      }
    }

    this.systemGraphRealm.clearCacheForRoles();
  }

  Collection<AuthorizationInfo> getAuthorizationInfo(PrincipalCollection principalCollection) {
    List<AuthorizationInfo> infoList = new ArrayList(1);
    Iterator n3 = this.realms.iterator();

    while (n3.hasNext()) {
      Realm realm = (Realm) n3.next();
      if (realm instanceof ShiroAuthorizationInfoProvider) {
        AuthorizationInfo info = ((ShiroAuthorizationInfoProvider) realm)
            .getAuthorizationInfoSnapshot(principalCollection);
        if (info != null) {
          infoList.add(info);
        }
      }
    }

    return infoList;
  }

  Set<ResourcePrivilege> getPermissions(Set<String> roles) {
    return this.systemGraphRealm.getPrivilegesForRoles(roles);
  }
}
