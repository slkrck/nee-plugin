/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

public class RelTypeSegment implements Segment {

  public static final RelTypeSegment ALL = new RelTypeSegment(null);
  private final String relType;

  public RelTypeSegment(String relType) {
    this.relType = relType;
  }

  public String getRelType() {
    return this.relType;
  }

  public int hashCode() {
    return this.relType.hashCode();
  }

  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj instanceof RelTypeSegment) {
      RelTypeSegment other = (RelTypeSegment) obj;
      if (this.relType == null) {
        return other.relType == null;
      } else {
        return this.relType.equals(other.getRelType());
      }
    } else {
      return false;
    }
  }

  public String toString() {
    return this.relType == null ? "All relationships" : "relType: '" + this.relType + "'";
  }
}
