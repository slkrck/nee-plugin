/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth.plugin;

import io.nee.server.security.enterprise.auth.plugin.spi.AuthInfo;
import io.nee.server.security.enterprise.auth.plugin.spi.CacheableAuthInfo;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.neo4j.cypher.internal.security.SecureHasher;
import org.neo4j.internal.kernel.api.security.AuthenticationResult;
import org.neo4j.server.security.auth.ShiroAuthenticationInfo;

public class PluginAuthInfo extends ShiroAuthenticationInfo implements AuthorizationInfo {

  Set<String> roles;

  private PluginAuthInfo(Object principal, String realmName, Set<String> roles) {
    super(principal, realmName, AuthenticationResult.SUCCESS);
    this.roles = roles;
  }

  private PluginAuthInfo(Object principal, Object hashedCredentials, ByteSource credentialsSalt,
      String realmName, Set<String> roles) {
    super(principal, hashedCredentials, credentialsSalt, realmName, AuthenticationResult.SUCCESS);
    this.roles = roles;
  }

  private PluginAuthInfo(AuthInfo authInfo, SimpleHash hashedCredentials, String realmName) {
    this(authInfo.principal(), hashedCredentials.getBytes(), hashedCredentials.getSalt(), realmName,
        new HashSet(authInfo.roles()));
  }

  public static PluginAuthInfo create(AuthInfo authInfo, String realmName) {
    return new PluginAuthInfo(authInfo.principal(), realmName, new HashSet(authInfo.roles()));
  }

  public static PluginAuthInfo createCacheable(AuthInfo authInfo, String realmName,
      SecureHasher secureHasher) {
    if (authInfo instanceof CacheableAuthInfo) {
      byte[] credentials = ((CacheableAuthInfo) authInfo).credentials();
      SimpleHash hashedCredentials = secureHasher.hash(credentials);
      return new PluginAuthInfo(authInfo, hashedCredentials, realmName);
    } else {
      return new PluginAuthInfo(authInfo.principal(), realmName, new HashSet(authInfo.roles()));
    }
  }

  public Collection<String> getRoles() {
    return this.roles;
  }

  public Collection<String> getStringPermissions() {
    return Collections.emptyList();
  }

  public Collection<Permission> getObjectPermissions() {
    return Collections.emptyList();
  }
}
