/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Ticker;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

public class ShiroCaffeineCache<K, V> implements Cache<K, V> {

  private final com.github.benmanes.caffeine.cache.Cache<K, V> caffCache;

  ShiroCaffeineCache(Ticker ticker, long ttl, int maxCapacity, boolean useTTL) {
    this(ticker, ForkJoinPool.commonPool(), ttl, maxCapacity, useTTL);
  }

  ShiroCaffeineCache(Ticker ticker, Executor maintenanceExecutor, long ttl, int maxCapacity,
      boolean useTTL) {
    Caffeine<Object, Object> builder = Caffeine.newBuilder().maximumSize(maxCapacity)
        .executor(maintenanceExecutor);
    if (useTTL) {
      if (ttl <= 0L) {
        throw new IllegalArgumentException("TTL must be larger than zero.");
      }

      builder.ticker(ticker).expireAfterWrite(ttl, TimeUnit.MILLISECONDS);
    }

    this.caffCache = builder.build();
  }

  public V get(K key) throws CacheException {
    return this.caffCache.getIfPresent(key);
  }

  public V put(K key, V value) throws CacheException {
    return this.caffCache.asMap().put(key, value);
  }

  public V remove(K key) throws CacheException {
    return this.caffCache.asMap().remove(key);
  }

  public void clear() throws CacheException {
    this.caffCache.invalidateAll();
  }

  public int size() {
    return this.caffCache.asMap().size();
  }

  public Set<K> keys() {
    return this.caffCache.asMap().keySet();
  }

  public Collection<V> values() {
    return this.caffCache.asMap().values();
  }

  private static class NullCache<K, V> implements Cache<K, V> {

    public V get(K key) throws CacheException {
      return null;
    }

    public V put(K key, V value) throws CacheException {
      return null;
    }

    public V remove(K key) throws CacheException {
      return null;
    }

    public void clear() throws CacheException {
    }

    public int size() {
      return 0;
    }

    public Set<K> keys() {
      return Collections.emptySet();
    }

    public Collection<V> values() {
      return Collections.emptySet();
    }
  }

  public static class Manager implements CacheManager {

    private final Map<String, Cache<?, ?>> caches;
    private final Ticker ticker;
    private final long ttl;
    private final int maxCapacity;
    private final boolean useTTL;

    public Manager(Ticker ticker, long ttl, int maxCapacity, boolean useTTL) {
      this.ticker = ticker;
      this.ttl = ttl;
      this.maxCapacity = maxCapacity;
      this.useTTL = useTTL;
      this.caches = new HashMap();
    }

    public <K, V> Cache<K, V> getCache(String s) throws CacheException {
      return (Cache) this.caches.computeIfAbsent(s, (ignored) ->
      {
        return this.useTTL && this.ttl <= 0L ? new NullCache()
            : new ShiroCaffeineCache(this.ticker, this.ttl, this.maxCapacity, this.useTTL);
      });
    }
  }
}
