/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.security.enterprise.auth;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

public class RoleRecord {

  private final String name;
  private final SortedSet<String> users;

  public RoleRecord(String name, SortedSet<String> users) {
    this.name = name;
    this.users = users;
  }

  public RoleRecord(String name, String... users) {
    this.name = name;
    this.users = new TreeSet();
    this.users.addAll(Arrays.asList(users));
  }

  public String name() {
    return this.name;
  }

  public SortedSet<String> users() {
    return this.users;
  }

  public RoleRecord.Builder augment() {
    return new RoleRecord.Builder(this);
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      RoleRecord role = (RoleRecord) o;
      if (this.name != null) {
        if (this.name.equals(role.name)) {
          return this.users != null ? this.users.equals(role.users) : role.users == null;
        }
      } else if (role.name == null) {
        return this.users != null ? this.users.equals(role.users) : role.users == null;
      }

      return false;
    } else {
      return false;
    }
  }

  public int hashCode() {
    int result = this.name != null ? this.name.hashCode() : 0;
    result = 31 * result + (this.users != null ? this.users.hashCode() : 0);
    return result;
  }

  public String toString() {
    return "Role{name='" + this.name + "', users='" + this.users + "'}";
  }

  public static class Builder {

    private String name;
    private SortedSet<String> users = new TreeSet();

    public Builder() {
    }

    public Builder(RoleRecord base) {
      this.name = base.name;
      this.users = new TreeSet(base.users);
    }

    public RoleRecord.Builder withName(String name) {
      this.name = name;
      return this;
    }

    public RoleRecord.Builder withUsers(SortedSet<String> users) {
      this.users = users;
      return this;
    }

    public RoleRecord.Builder withUser(String user) {
      this.users.add(user);
      return this;
    }

    public RoleRecord.Builder withoutUser(String user) {
      this.users.remove(user);
      return this;
    }

    public RoleRecord build() {
      return new RoleRecord(this.name, this.users);
    }
  }
}
