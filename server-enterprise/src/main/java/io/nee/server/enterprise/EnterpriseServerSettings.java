/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.enterprise;

import java.time.Duration;
import org.neo4j.configuration.Description;
import org.neo4j.configuration.SettingImpl;
import org.neo4j.configuration.SettingValueParsers;
import org.neo4j.configuration.SettingsDeclaration;
import org.neo4j.graphdb.config.Setting;

@Description("Settings available in the Enterprise server")
public class EnterpriseServerSettings implements SettingsDeclaration {

  @Description("Configure the Neo4j Browser to time out logged in users after this idle period. Setting this to 0 indicates no limit.")
  public static final Setting<Duration> browser_credentialTimeout;
  @Description("Configure the Neo4j Browser to store or not store user credentials.")
  public static final Setting<Boolean> browser_retainConnectionCredentials;
  @Description("Configure the policy for outgoing Neo4j Browser connections.")
  public static final Setting<Boolean> browser_allowOutgoingBrowserConnections;

  static {
    browser_credentialTimeout = SettingImpl
        .newBuilder("browser.credential_timeout", SettingValueParsers.DURATION, Duration.ZERO)
        .build();
    browser_retainConnectionCredentials = SettingImpl
        .newBuilder("browser.retain_connection_credentials", SettingValueParsers.BOOL, true)
        .build();
    browser_allowOutgoingBrowserConnections = SettingImpl
        .newBuilder("browser.allow_outgoing_connections", SettingValueParsers.BOOL, true).build();
  }
}
