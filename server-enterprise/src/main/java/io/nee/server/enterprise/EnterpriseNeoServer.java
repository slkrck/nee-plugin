/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.enterprise;

import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.kernel.impl.enterprise.configuration.MetricsSettings;
import io.nee.metrics.source.server.ServerThreadView;
import io.nee.metrics.source.server.ServerThreadViewSetter;
import io.nee.server.database.EnterpriseGraphFactory;
import io.nee.server.enterprise.modules.EnterpriseAuthorizationModule;
import io.nee.server.rest.DatabaseRoleInfoServerModule;
import io.nee.server.rest.EnterpriseDiscoverableURIs;
import io.nee.server.rest.LegacyManagementModule;
import io.nee.server.rest.causalclustering.CausalClusteringService;
import io.nee.server.rest.causalclustering.LegacyCausalClusteringRedirectService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import org.eclipse.jetty.util.thread.ThreadPool;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.exceptions.UnsatisfiedDependencyException;
import org.neo4j.graphdb.facade.ExternalDependencies;
import org.neo4j.logging.Log;
import org.neo4j.server.CommunityNeoServer;
import org.neo4j.server.database.GraphFactory;
import org.neo4j.server.modules.AuthorizationModule;
import org.neo4j.server.modules.DBMSModule;
import org.neo4j.server.modules.ServerModule;
import org.neo4j.server.rest.discovery.DiscoverableURIs;
import org.neo4j.server.web.Jetty9WebServer;
import org.neo4j.server.web.WebServer;

public class EnterpriseNeoServer extends CommunityNeoServer {

  public EnterpriseNeoServer(Config config, ExternalDependencies dependencies) {
    super(config, new EnterpriseGraphFactory(), dependencies);
  }

  public EnterpriseNeoServer(Config config, GraphFactory graphFactory,
      ExternalDependencies dependencies) {
    super(config, graphFactory, dependencies);
  }

  protected WebServer createWebServer() {
    Jetty9WebServer webServer = (Jetty9WebServer) super.createWebServer();
    webServer.setJettyCreatedCallback((jetty) ->
    {
      if (this.getConfig().get(MetricsSettings.metricsEnabled)) {
        final ThreadPool threadPool = jetty.getThreadPool();

        assert threadPool != null;

        try {
          ServerThreadViewSetter setter =
              this.getSystemDatabaseDependencyResolver()
                  .resolveDependency(ServerThreadViewSetter.class);
          setter.set(new ServerThreadView() {
            public int allThreads() {
              return threadPool.getThreads();
            }

            public int idleThreads() {
              return threadPool.getIdleThreads();
            }
          });
        } catch (UnsatisfiedDependencyException n5) {
          Log log = this.userLogProvider.getLog(this.getClass());
          log.warn("Metrics dependencies not found.", n5);
        }
      }
    });
    return webServer;
  }

  protected AuthorizationModule createAuthorizationModule() {
    return new EnterpriseAuthorizationModule(this.webServer, this.authManagerSupplier,
        this.userLogProvider, this.getConfig(), this.getUriWhitelist());
  }

  protected DBMSModule createDBMSModule() {
    Supplier<DiscoverableURIs> discoverableURIs = () ->
    {
      return EnterpriseDiscoverableURIs.enterpriseDiscoverableURIs(this.getConfig(),
          this.getSystemDatabaseDependencyResolver()
              .resolveDependency(ConnectorPortRegister.class));
    };
    return new DBMSModule(this.webServer, this.getConfig(), discoverableURIs, this.userLogProvider);
  }

  protected Iterable<ServerModule> createServerModules() {
    List<ServerModule> modules = new ArrayList();
    modules.add(new DatabaseRoleInfoServerModule(this.webServer, this.getConfig()));
    modules.add(new LegacyManagementModule(this.webServer, this.getConfig()));
    Iterable<ServerModule> n10000 = super.createServerModules();
    Objects.requireNonNull(modules);
    n10000.forEach(modules::add);
    return modules;
  }

  protected List<Pattern> getUriWhitelist() {
    ArrayList<Pattern> result = new ArrayList(super.getUriWhitelist());
    if (!this.getConfig().get(CausalClusteringSettings.status_auth_enabled)) {
      result.add(CausalClusteringService.databaseClusterUriPattern(this.getConfig()));
      result.add(
          LegacyCausalClusteringRedirectService.databaseLegacyClusterUriPattern(this.getConfig()));
    }

    return result;
  }
}
