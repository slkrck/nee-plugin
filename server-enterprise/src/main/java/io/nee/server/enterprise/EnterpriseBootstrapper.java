/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.enterprise;

import io.nee.causalclustering.core.CausalClusterConfigurationValidator;
import io.nee.server.database.EnterpriseGraphFactory;
import java.util.ArrayList;
import java.util.List;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GroupSettingValidator;
import org.neo4j.graphdb.facade.GraphDatabaseDependencies;
import org.neo4j.server.CommunityBootstrapper;
import org.neo4j.server.NeoServer;
import org.neo4j.server.database.GraphFactory;

public class EnterpriseBootstrapper extends CommunityBootstrapper {

  protected GraphFactory createGraphFactory(Config config) {
    return new EnterpriseGraphFactory();
  }

  protected NeoServer createNeoServer(GraphFactory graphFactory, Config config,
      GraphDatabaseDependencies dependencies) {
    return new EnterpriseNeoServer(config, graphFactory, dependencies);
  }

  protected List<Class<? extends GroupSettingValidator>> configurationValidators() {
    List<Class<? extends GroupSettingValidator>> validators = new ArrayList(
        super.configurationValidators());
    validators.add(CausalClusterConfigurationValidator.class);
    return validators;
  }
}
