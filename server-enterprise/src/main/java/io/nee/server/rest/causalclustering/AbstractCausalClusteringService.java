/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.rest.causalclustering;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.neo4j.dbms.DatabaseStateService;
import org.neo4j.server.database.DatabaseService;
import org.neo4j.server.rest.repr.OutputFormat;

public abstract class AbstractCausalClusteringService implements ClusterService {

  static final String AVAILABLE = "available";
  static final String WRITABLE = "writable";
  static final String READ_ONLY = "read-only";
  static final String STATUS = "status";
  private final CausalClusteringStatus status;

  AbstractCausalClusteringService(OutputFormat output, DatabaseStateService dbStateService,
      DatabaseService dbService, String databaseName) {
    this.status = this.createStatus(output, dbStateService, dbService, databaseName);
  }

  private CausalClusteringStatus createStatus(OutputFormat output,
      DatabaseStateService dbStateService, DatabaseService dbService, String databaseName) {
    try {
      return CausalClusteringStatusFactory
          .build(output, dbStateService, dbService, databaseName, this);
    } catch (Exception n6) {
      return new FixedResponse(
          Response.status(Status.INTERNAL_SERVER_ERROR).type(MediaType.TEXT_PLAIN_TYPE)
              .entity(n6.toString()).build());
    }
  }

  @GET
  public Response discover() {
    return this.status.discover();
  }

  @GET
  @Path("writable")
  public Response isWritable() {
    return this.status.writable();
  }

  @GET
  @Path("read-only")
  public Response isReadOnly() {
    return this.status.readonly();
  }

  @GET
  @Path("available")
  public Response isAvailable() {
    return this.status.available();
  }

  @GET
  @Path("status")
  public Response status() {
    return this.status.description();
  }
}
