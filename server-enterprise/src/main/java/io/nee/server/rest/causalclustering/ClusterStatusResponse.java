/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.rest.causalclustering;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.nee.causalclustering.identity.MemberId;
import java.time.Duration;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@JsonSerialize
public class ClusterStatusResponse {

  private final boolean isCore;
  private final long lastAppliedRaftIndex;
  private final boolean isParticipatingInRaftGroup;
  private final Collection<String> votingMembers;
  private final boolean isHealthy;
  private final String memberId;
  private final String leader;
  private final Long millisSinceLastLeaderMessage;
  private final Double raftCommandsPerSecond;
  private final boolean isDiscoveryHealthy;

  ClusterStatusResponse(long lastAppliedRaftIndex, boolean isParticipatingInRaftGroup,
      Collection<MemberId> votingMembers, boolean isHealthy,
      MemberId memberId, MemberId leader, Duration millisSinceLastLeaderMessage,
      Double raftCommandsPerSecond, boolean isCore,
      boolean isDiscoveryHealthy) {
    this.lastAppliedRaftIndex = lastAppliedRaftIndex;
    this.isParticipatingInRaftGroup = isParticipatingInRaftGroup;
    this.votingMembers = votingMembers.stream().map((member) ->
    {
      return member.getUuid().toString();
    }).sorted().collect(Collectors.toList());
    this.isHealthy = isHealthy;
    this.memberId = memberId.getUuid().toString();
    this.leader = Optional.ofNullable(leader).map(MemberId::getUuid).map(UUID::toString)
        .orElse(null);
    this.millisSinceLastLeaderMessage = Optional.ofNullable(millisSinceLastLeaderMessage)
        .map(Duration::toMillis).orElse(null);
    this.raftCommandsPerSecond = raftCommandsPerSecond;
    this.isCore = isCore;
    this.isDiscoveryHealthy = isDiscoveryHealthy;
  }

  public long getLastAppliedRaftIndex() {
    return this.lastAppliedRaftIndex;
  }

  public boolean isParticipatingInRaftGroup() {
    return this.isParticipatingInRaftGroup;
  }

  public Collection<String> getVotingMembers() {
    return this.votingMembers;
  }

  public boolean isHealthy() {
    return this.isHealthy;
  }

  public boolean isDiscoveryHealthy() {
    return this.isDiscoveryHealthy;
  }

  public String getMemberId() {
    return this.memberId;
  }

  public String getLeader() {
    return this.leader;
  }

  public Long getMillisSinceLastLeaderMessage() {
    return this.millisSinceLastLeaderMessage;
  }

  public Double getRaftCommandsPerSecond() {
    return this.raftCommandsPerSecond;
  }

  public boolean isCore() {
    return this.isCore;
  }
}
