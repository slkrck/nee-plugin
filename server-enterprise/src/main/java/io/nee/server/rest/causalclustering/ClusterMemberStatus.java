/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.rest.causalclustering;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.nee.causalclustering.identity.MemberId;
import java.io.IOException;
import java.time.Duration;
import java.util.Collection;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.neo4j.kernel.internal.GraphDatabaseAPI;
import org.neo4j.server.rest.repr.OutputFormat;

abstract class ClusterMemberStatus implements CausalClusteringStatus {

  protected final OutputFormat output;
  protected final GraphDatabaseAPI db;
  private final ClusterService clusterService;

  ClusterMemberStatus(OutputFormat output, GraphDatabaseAPI db, ClusterService clusterService) {
    this.output = output;
    this.db = db;
    this.clusterService = clusterService;
  }

  public final Response discover() {
    return this.output.ok(new CausalClusteringDiscovery(
        this.clusterService.relativeClusterPath(this.db.databaseName())));
  }

  Response statusResponse(long lastAppliedRaftIndex, boolean isParticipatingInRaftGroup,
      Collection<MemberId> votingMembers, boolean isHealthy,
      MemberId memberId, MemberId leader, Duration millisSinceLastLeaderMessage,
      Double raftCommandsPerSecond, boolean isCore,
      boolean discoveryHealthy) {
    ObjectMapper objectMapper = new ObjectMapper();

    String jsonObject;
    try {
      jsonObject = objectMapper.writeValueAsString(
          new ClusterStatusResponse(lastAppliedRaftIndex, isParticipatingInRaftGroup, votingMembers,
              isHealthy, memberId, leader,
              millisSinceLastLeaderMessage, raftCommandsPerSecond, isCore, discoveryHealthy));
    } catch (IOException n15) {
      throw new RuntimeException(n15);
    }

    return Response.status(Status.OK).type("application/json").entity(jsonObject).build();
  }

  Response positiveResponse() {
    return this.plainTextResponse(Status.OK, "true");
  }

  Response negativeResponse() {
    return this.plainTextResponse(Status.NOT_FOUND, "false");
  }

  private Response plainTextResponse(Status status, String entityBody) {
    return Response.status(status).type(MediaType.TEXT_PLAIN_TYPE).entity(entityBody).build();
  }
}
