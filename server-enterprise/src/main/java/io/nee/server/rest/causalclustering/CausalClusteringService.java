/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.rest.causalclustering;

import java.util.regex.Pattern;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.DatabaseStateService;
import org.neo4j.server.configuration.ServerSettings;
import org.neo4j.server.database.DatabaseService;
import org.neo4j.server.rest.repr.OutputFormat;

@Path("/{databaseName}/cluster")
public class CausalClusteringService extends AbstractCausalClusteringService {

  public static final String NAME = "cluster";
  static final String DB_CLUSTER_PATH = "/{databaseName}/cluster";
  private static final String CLUSTER_PATH = "/cluster";
  private static final String DB_NAME = "databaseName";

  public CausalClusteringService(@Context OutputFormat output,
      @Context DatabaseStateService databaseStateService, @Context DatabaseService dbService,
      @PathParam("databaseName") String databaseName) {
    super(output, databaseStateService, dbService, databaseName);
  }

  public static Pattern databaseClusterUriPattern(Config config) {
    return Pattern.compile(config.get(ServerSettings.db_api_path).getPath() + "/[^/]*/cluster.*");
  }

  public static String absoluteDatabaseClusterPath(Config config) {
    return config.get(ServerSettings.db_api_path).getPath() + "/{databaseName}/cluster";
  }

  public String relativeClusterPath(String databaseName) {
    return databaseName + "/cluster";
  }
}
