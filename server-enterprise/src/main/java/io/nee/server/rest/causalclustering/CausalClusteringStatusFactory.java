/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.rest.causalclustering;

import io.nee.dbms.EnterpriseOperatorState;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.neo4j.dbms.DatabaseStateService;
import org.neo4j.dbms.api.DatabaseNotFoundException;
import org.neo4j.kernel.internal.GraphDatabaseAPI;
import org.neo4j.server.database.DatabaseService;
import org.neo4j.server.rest.repr.OutputFormat;

public class CausalClusteringStatusFactory {

  public static CausalClusteringStatus build(OutputFormat output,
      DatabaseStateService dbStateService, DatabaseService dbService, String databaseName,
      ClusterService clusterService) {
    GraphDatabaseAPI db = findDb(dbService, databaseName);
    if (db == null) {
      return new FixedResponse(Status.NOT_FOUND);
    } else {
      EnterpriseOperatorState operatorState = (EnterpriseOperatorState) dbStateService
          .stateOfDatabase(db.databaseId());
      return handleDbState(output, databaseName, clusterService, db, operatorState);
    }
  }

  private static CausalClusteringStatus handleDbState(OutputFormat output, String databaseName,
      ClusterService clusterService, GraphDatabaseAPI db,
      EnterpriseOperatorState operatorState) {
    switch (operatorState) {
      case INITIAL:
        return new FixedResponse(
            Response.status(Status.SERVICE_UNAVAILABLE).header("Retry-After", 60)
                .type(MediaType.TEXT_PLAIN_TYPE).entity(
                "Database " + databaseName + " is " + operatorState.description()).build());
      case STOPPED:
      case DROPPED:
      case UNKNOWN:
        return new FixedResponse(
            Response.status(Status.SERVICE_UNAVAILABLE)
                .entity("Database " + databaseName + " is " + operatorState.description()).build());
      default:
        return createStatus(output, clusterService, db);
    }
  }

  private static CausalClusteringStatus createStatus(OutputFormat output,
      ClusterService clusterService, GraphDatabaseAPI db) {
    switch (db.databaseInfo()) {
      case CORE:
        return new CoreStatus(output, db, clusterService);
      case READ_REPLICA:
        return new ReadReplicaStatus(output, db, clusterService);
      default:
        return new FixedResponse(Status.FORBIDDEN);
    }
  }

  private static GraphDatabaseAPI findDb(DatabaseService dbService, String databaseName) {
    try {
      return dbService.getDatabase(databaseName);
    } catch (DatabaseNotFoundException n3) {
      return null;
    }
  }
}
