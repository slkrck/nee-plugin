/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.rest.causalclustering;

import io.nee.causalclustering.core.state.machines.CommandIndexTracker;
import io.nee.causalclustering.discovery.RoleInfo;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.monitoring.ThroughputMonitor;
import java.time.Duration;
import java.util.Collection;
import javax.ws.rs.core.Response;
import org.neo4j.common.DependencyResolver;
import org.neo4j.kernel.internal.GraphDatabaseAPI;
import org.neo4j.monitoring.DatabaseHealth;
import org.neo4j.monitoring.Health;
import org.neo4j.server.rest.repr.OutputFormat;

class ReadReplicaStatus extends ClusterMemberStatus {

  private final ThroughputMonitor throughputMonitor;
  private final TopologyService topologyService;
  private final Health dbHealth;
  private final CommandIndexTracker commandIndexTracker;

  ReadReplicaStatus(OutputFormat output, GraphDatabaseAPI databaseAPI,
      ClusterService clusterService) {
    super(output, databaseAPI, clusterService);
    DependencyResolver dependencyResolver = databaseAPI.getDependencyResolver();
    this.commandIndexTracker = dependencyResolver.resolveDependency(CommandIndexTracker.class);
    this.topologyService = dependencyResolver.resolveDependency(TopologyService.class);
    this.dbHealth = dependencyResolver.resolveDependency(DatabaseHealth.class);
    this.throughputMonitor = dependencyResolver.resolveDependency(ThroughputMonitor.class);
  }

  public Response available() {
    return this.positiveResponse();
  }

  public Response readonly() {
    return this.positiveResponse();
  }

  public Response writable() {
    return this.negativeResponse();
  }

  public Response description() {
    Collection<MemberId> votingMembers = this.topologyService.allCoreServers().keySet();
    boolean isHealthy = this.dbHealth.isHealthy();
    boolean isDiscoveryHealthy = this.topologyService.isHealthy();
    MemberId myId = this.topologyService.memberId();
    MemberId leaderId = votingMembers.stream().filter((memberId) ->
    {
      return this.topologyService.lookupRole(this.db.databaseId(), memberId) ==
          RoleInfo.LEADER;
    }).findFirst().orElse(null);
    long lastAppliedRaftIndex = this.commandIndexTracker.getAppliedCommandIndex();
    Duration millisSinceLastLeaderMessage = null;
    Double raftCommandsPerSecond = this.throughputMonitor.throughput().orElse(null);
    return this
        .statusResponse(lastAppliedRaftIndex, false, votingMembers, isHealthy, myId, leaderId,
            millisSinceLastLeaderMessage,
            raftCommandsPerSecond, false, isDiscoveryHealthy);
  }
}
