/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.rest;

import io.nee.kernel.impl.enterprise.configuration.EnterpriseEditionSettings;
import io.nee.server.rest.causalclustering.CausalClusteringService;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.server.rest.discovery.CommunityDiscoverableURIs;
import org.neo4j.server.rest.discovery.DiscoverableURIs;
import org.neo4j.server.rest.discovery.DiscoverableURIs.Builder;

public class EnterpriseDiscoverableURIs {

  public static DiscoverableURIs enterpriseDiscoverableURIs(Config config,
      ConnectorPortRegister portRegister) {
    Builder discoverableURIsBuilder = CommunityDiscoverableURIs
        .communityDiscoverableURIsBuilder(config, portRegister);
    EnterpriseEditionSettings.Mode mode = config.get(EnterpriseEditionSettings.mode);
    if (mode == EnterpriseEditionSettings.Mode.CORE
        || mode == EnterpriseEditionSettings.Mode.READ_REPLICA) {
      discoverableURIsBuilder
          .addEndpoint("cluster", CausalClusteringService.absoluteDatabaseClusterPath(config));
    }

    return discoverableURIsBuilder.build();
  }
}
