/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.rest.causalclustering;

import io.nee.causalclustering.core.consensus.DurationSinceLastMessageMonitor;
import io.nee.causalclustering.core.consensus.NoLeaderFoundException;
import io.nee.causalclustering.core.consensus.RaftMachine;
import io.nee.causalclustering.core.consensus.membership.RaftMembershipManager;
import io.nee.causalclustering.core.consensus.roles.Role;
import io.nee.causalclustering.core.consensus.roles.RoleProvider;
import io.nee.causalclustering.core.state.machines.CommandIndexTracker;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.monitoring.ThroughputMonitor;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.core.Response;
import org.neo4j.common.DependencyResolver;
import org.neo4j.kernel.internal.GraphDatabaseAPI;
import org.neo4j.monitoring.DatabaseHealth;
import org.neo4j.monitoring.Health;
import org.neo4j.server.rest.repr.OutputFormat;

class CoreStatus extends ClusterMemberStatus {

  private final RaftMembershipManager raftMembershipManager;
  private final Health databaseHealth;
  private final TopologyService topologyService;
  private final DurationSinceLastMessageMonitor raftMessageTimerResetMonitor;
  private final RaftMachine raftMachine;
  private final CommandIndexTracker commandIndexTracker;
  private final ThroughputMonitor throughputMonitor;
  private final RoleProvider roleProvider;

  CoreStatus(OutputFormat output, GraphDatabaseAPI databaseAPI, ClusterService clusterService) {
    super(output, databaseAPI, clusterService);
    DependencyResolver dependencyResolver = databaseAPI.getDependencyResolver();
    this.raftMembershipManager = dependencyResolver.resolveDependency(RaftMembershipManager.class);
    this.databaseHealth = dependencyResolver.resolveDependency(DatabaseHealth.class);
    this.topologyService = dependencyResolver.resolveDependency(TopologyService.class);
    this.raftMachine = dependencyResolver.resolveDependency(RaftMachine.class);
    this.raftMessageTimerResetMonitor = dependencyResolver
        .resolveDependency(DurationSinceLastMessageMonitor.class);
    this.commandIndexTracker = dependencyResolver.resolveDependency(CommandIndexTracker.class);
    this.throughputMonitor = dependencyResolver.resolveDependency(ThroughputMonitor.class);
    this.roleProvider = dependencyResolver.resolveDependency(RoleProvider.class);
  }

  public Response available() {
    return this.positiveResponse();
  }

  public Response readonly() {
    Role role = this.roleProvider.currentRole();
    return Role.FOLLOWER != role && Role.CANDIDATE != role ? this.negativeResponse()
        : this.positiveResponse();
  }

  public Response writable() {
    return this.roleProvider.currentRole() == Role.LEADER ? this.positiveResponse()
        : this.negativeResponse();
  }

  public Response description() {
    MemberId myId = this.topologyService.memberId();
    MemberId leaderId = this.getLeader();
    List<MemberId> votingMembers = new ArrayList(this.raftMembershipManager.votingMembers());
    boolean participatingInRaftGroup = votingMembers.contains(myId) && Objects.nonNull(leaderId);
    long lastAppliedRaftIndex = this.commandIndexTracker.getAppliedCommandIndex();
    Duration millisSinceLastLeaderMessage;
    if (Objects.equals(myId, leaderId)) {
      millisSinceLastLeaderMessage = Duration.ofMillis(0L);
    } else {
      millisSinceLastLeaderMessage = this.raftMessageTimerResetMonitor.durationSinceLastMessage();
    }

    Double raftCommandsPerSecond = this.throughputMonitor.throughput().orElse(null);
    return this.statusResponse(lastAppliedRaftIndex, participatingInRaftGroup, votingMembers,
        this.databaseHealth.isHealthy(), myId, leaderId,
        millisSinceLastLeaderMessage, raftCommandsPerSecond, true,
        this.topologyService.isHealthy());
  }

  private MemberId getLeader() {
    try {
      return this.raftMachine.getLeaderInfo().memberId();
    } catch (NoLeaderFoundException n2) {
      return null;
    }
  }
}
