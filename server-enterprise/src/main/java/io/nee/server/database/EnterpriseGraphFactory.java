/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.server.database;

import io.nee.causalclustering.core.CoreEditionModule;
import io.nee.causalclustering.core.CoreGraphDatabase;
import io.nee.causalclustering.discovery.akka.AkkaDiscoveryServiceFactory;
import io.nee.causalclustering.readreplica.ReadReplicaEditionModule;
import io.nee.causalclustering.readreplica.ReadReplicaGraphDatabase;
import io.nee.enterprise.edition.EnterpriseEditionModule;
import io.nee.kernel.impl.enterprise.configuration.EnterpriseEditionSettings;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.graphdb.facade.DatabaseManagementServiceFactory;
import org.neo4j.graphdb.facade.ExternalDependencies;
import org.neo4j.kernel.impl.factory.DatabaseInfo;
import org.neo4j.server.database.GraphFactory;

public class EnterpriseGraphFactory implements GraphFactory {

  private static AkkaDiscoveryServiceFactory newDiscoveryServiceFactory() {
    return new AkkaDiscoveryServiceFactory();
  }

  public DatabaseManagementService newDatabaseManagementService(Config config,
      ExternalDependencies dependencies) {
    EnterpriseEditionSettings.Mode mode = config.get(EnterpriseEditionSettings.mode);
    switch (mode) {
      case CORE:
        return (new CoreGraphDatabase(config, dependencies, newDiscoveryServiceFactory(),
            CoreEditionModule::new)).getManagementService();
      case READ_REPLICA:
        return (new ReadReplicaGraphDatabase(config, dependencies, newDiscoveryServiceFactory(),
            ReadReplicaEditionModule::new)).getManagementService();
      default:
        return (new DatabaseManagementServiceFactory(DatabaseInfo.ENTERPRISE,
            EnterpriseEditionModule::new)).build(config, dependencies);
    }
  }
}
