/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.backup;

import io.nee.backup.impl.BackupExecutionException;
import io.nee.backup.impl.ConsistencyCheckExecutionException;
import io.nee.backup.impl.OnlineBackupContext;
import io.nee.backup.impl.OnlineBackupExecutor;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Objects;
import org.neo4j.annotations.api.PublicApi;
import org.neo4j.configuration.Config;
import org.neo4j.internal.helpers.progress.ProgressMonitorFactory;
import org.neo4j.logging.FormattedLogProvider;
import org.neo4j.logging.LogProvider;
import org.neo4j.util.Preconditions;

@PublicApi
public class OnlineBackup {

  private final String hostnameOrIp;
  private final int port;
  private final Config config;
  private boolean fallbackToFullBackup = true;
  private boolean consistencyCheck = true;
  private OutputStream outputStream;

  private OnlineBackup(String hostnameOrIp, int port) {
    this.outputStream = System.out;
    this.hostnameOrIp = hostnameOrIp;
    this.port = port;
    this.config = Config.defaults();
  }

  public static OnlineBackup from(String hostnameOrIp) {
    return from(hostnameOrIp, 6362);
  }

  public static OnlineBackup from(String hostnameOrIp, int port) {
    return new OnlineBackup(Objects.requireNonNull(hostnameOrIp, "hostnameOrIp"),
        requireValidPort(port));
  }

  private static int requireValidPort(int port) {
    Preconditions.checkArgument(port > 0 && port <= 65535,
        "Port is expected to be positive and less than or equal to 65535 but was: " + port);
    return port;
  }

  public OnlineBackup withFallbackToFullBackup(boolean fallbackToFullBackup) {
    this.fallbackToFullBackup = fallbackToFullBackup;
    return this;
  }

  public OnlineBackup withConsistencyCheck(boolean consistencyCheck) {
    this.consistencyCheck = consistencyCheck;
    return this;
  }

  public OnlineBackup withOutputStream(OutputStream outputStream) {
    this.outputStream = Objects.requireNonNull(outputStream, "outputStream");
    return this;
  }

  public OnlineBackup.Result backup(String databaseName, Path targetDirectory) {
    Objects.requireNonNull(databaseName, "databaseName");
    Objects.requireNonNull(targetDirectory, "targetDirectory");
    FormattedLogProvider logProvider = FormattedLogProvider.toOutputStream(this.outputStream);

    try {
      this.executeBackup(databaseName, targetDirectory, logProvider);
      return new OnlineBackup.Result(this.consistencyCheck);
    } catch (BackupExecutionException n5) {
      throw new RuntimeException("Backup failed", n5);
    } catch (ConsistencyCheckExecutionException n6) {
      logProvider.getLog(this.getClass()).error("Consistency check failed", n6);
      return new OnlineBackup.Result(false);
    }
  }

  private void executeBackup(String databaseName, Path targetDirectory, LogProvider logProvider)
      throws BackupExecutionException, ConsistencyCheckExecutionException {
    OnlineBackupExecutor backupExecutor = OnlineBackupExecutor.builder()
        .withUserLogProvider(logProvider).withProgressMonitorFactory(
            ProgressMonitorFactory.textual(this.outputStream)).build();
    OnlineBackupContext context = OnlineBackupContext.builder()
        .withAddress(this.hostnameOrIp, this.port).withConfig(this.config).withConsistencyCheck(
            this.consistencyCheck).withFallbackToFullBackup(this.fallbackToFullBackup)
        .withBackupDirectory(targetDirectory).withReportsDirectory(
            targetDirectory).withDatabaseName(databaseName).build();
    backupExecutor.executeBackup(context);
  }

  public static class Result {

    private final boolean consistent;

    private Result(boolean consistent) {
      this.consistent = consistent;
    }

    public boolean isConsistent() {
      return this.consistent;
    }
  }
}
