/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.backup.impl;

import io.nee.causalclustering.catchup.storecopy.DatabaseIdDownloadFailedException;
import io.nee.causalclustering.catchup.storecopy.StoreCopyFailedException;
import io.nee.causalclustering.catchup.storecopy.StoreFiles;
import io.nee.causalclustering.catchup.storecopy.StoreIdDownloadFailedException;
import java.io.IOException;
import java.util.Objects;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StoreId;

class DefaultBackupStrategy extends LifecycleAdapter implements BackupStrategy {

  private final BackupDelegator backupDelegator;
  private final Log log;
  private final StoreFiles storeFiles;

  DefaultBackupStrategy(BackupDelegator backupDelegator, LogProvider logProvider,
      StoreFiles storeFiles) {
    this.backupDelegator = backupDelegator;
    this.log = logProvider.getLog(DefaultBackupStrategy.class);
    this.storeFiles = storeFiles;
  }

  public void performFullBackup(DatabaseLayout targetDbLayout, SocketAddress address,
      String databaseName) throws BackupExecutionException {
    DefaultBackupStrategy.BackupInfo backupInfo = this
        .prepareForBackup(targetDbLayout, address, databaseName);
    if (backupInfo.localStoreId != null) {
      throw new BackupExecutionException(new StoreIdDownloadFailedException(
          String.format(
              "Cannot perform a full backup onto preexisting backup. Remote store id was %s but local is %s",
              backupInfo.remoteStoreId,
              backupInfo.localStoreId)));
    } else {
      try {
        this.backupDelegator
            .copy(backupInfo.remoteAddress, backupInfo.remoteStoreId, backupInfo.namedDatabaseId,
                targetDbLayout);
      } catch (StoreCopyFailedException n6) {
        throw new BackupExecutionException(n6);
      }
    }
  }

  public void performIncrementalBackup(DatabaseLayout targetDbLayout, SocketAddress address,
      String databaseName) throws BackupExecutionException {
    DefaultBackupStrategy.BackupInfo backupInfo = this
        .prepareForBackup(targetDbLayout, address, databaseName);
    if (!Objects.equals(backupInfo.localStoreId, backupInfo.remoteStoreId)) {
      throw new BackupExecutionException(new StoreIdDownloadFailedException(
          String.format("Remote store id was %s but local is %s", backupInfo.remoteStoreId,
              backupInfo.localStoreId)));
    } else {
      this.catchup(backupInfo.remoteAddress, backupInfo.remoteStoreId, backupInfo.namedDatabaseId,
          targetDbLayout);
    }
  }

  public void start() {
    this.backupDelegator.start();
  }

  public void stop() {
    this.backupDelegator.stop();
  }

  private DefaultBackupStrategy.BackupInfo prepareForBackup(DatabaseLayout databaseLayout,
      SocketAddress address, String databaseName)
      throws BackupExecutionException {
    try {
      this.log.info("Remote backup address is " + address);
      NamedDatabaseId namedDatabaseId = this.backupDelegator.fetchDatabaseId(address, databaseName);
      this.log.info("Database id is " + namedDatabaseId);
      StoreId remoteStoreId = this.backupDelegator.fetchStoreId(address, namedDatabaseId);
      this.log.info("Remote store id is " + remoteStoreId);
      StoreId localStoreId = this.readLocalStoreId(databaseLayout);
      this.log.info("Local store id is " + remoteStoreId);
      return new DefaultBackupStrategy.BackupInfo(address, remoteStoreId, localStoreId,
          namedDatabaseId);
    } catch (DatabaseIdDownloadFailedException | StoreIdDownloadFailedException n7) {
      throw new BackupExecutionException(n7);
    }
  }

  private StoreId readLocalStoreId(DatabaseLayout databaseLayout) {
    try {
      return this.storeFiles.isEmpty(databaseLayout) ? null
          : this.storeFiles.readStoreId(databaseLayout);
    } catch (IOException n3) {
      this.log.warn("Unable to read store ID from metadata store in " + databaseLayout, n3);
      return null;
    }
  }

  private void catchup(SocketAddress fromAddress, StoreId storeId, NamedDatabaseId namedDatabaseId,
      DatabaseLayout databaseLayout)
      throws BackupExecutionException {
    try {
      this.backupDelegator.tryCatchingUp(fromAddress, storeId, namedDatabaseId, databaseLayout);
    } catch (StoreCopyFailedException n6) {
      throw new BackupExecutionException(n6);
    }
  }

  private static class BackupInfo {

    final SocketAddress remoteAddress;
    final StoreId remoteStoreId;
    final StoreId localStoreId;
    final NamedDatabaseId namedDatabaseId;

    BackupInfo(SocketAddress remoteAddress, StoreId remoteStoreId, StoreId localStoreId,
        NamedDatabaseId namedDatabaseId) {
      this.remoteAddress = remoteAddress;
      this.remoteStoreId = remoteStoreId;
      this.localStoreId = localStoreId;
      this.namedDatabaseId = namedDatabaseId;
    }
  }
}
