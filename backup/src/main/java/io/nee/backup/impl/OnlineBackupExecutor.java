/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.backup.impl;

import io.nee.causalclustering.catchup.storecopy.StoreCopyClientMonitor;
import io.nee.causalclustering.catchup.storecopy.StoreFiles;
import io.nee.com.storecopy.FileMoveProvider;
import java.nio.file.Path;
import org.neo4j.consistency.ConsistencyCheckService;
import org.neo4j.internal.helpers.progress.ProgressMonitorFactory;
import org.neo4j.io.fs.DefaultFileSystemAbstraction;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StorageEngineFactory;

public class OnlineBackupExecutor {

  private final FileSystemAbstraction fs;
  private final LogProvider userLogProvider;
  private final LogProvider internalLogProvider;
  private final ProgressMonitorFactory progressMonitorFactory;
  private final Monitors monitors;
  private final BackupSupportingClassesFactory backupSupportingClassesFactory;
  private final ConsistencyCheckService consistencyCheckService = new ConsistencyCheckService();

  private OnlineBackupExecutor(OnlineBackupExecutor.Builder builder) {
    this.fs = builder.fs;
    this.userLogProvider = builder.userLogProvider;
    this.internalLogProvider = builder.internalLogProvider;
    this.progressMonitorFactory = builder.progressMonitorFactory;
    this.monitors = builder.monitors;
    this.backupSupportingClassesFactory = builder.supportingClassesFactory;
  }

  public static OnlineBackupExecutor buildDefault() {
    return builder().build();
  }

  public static OnlineBackupExecutor.Builder builder() {
    return new OnlineBackupExecutor.Builder();
  }

  public void executeBackup(OnlineBackupContext context)
      throws BackupExecutionException, ConsistencyCheckExecutionException {
    this.verify(context);
    BackupSupportingClasses supportingClasses = this.backupSupportingClassesFactory
        .createSupportingClasses(context);

    try {
      StoreCopyClientMonitor backupStoreCopyMonitor = new BackupOutputMonitor(this.userLogProvider);
      this.monitors.addMonitorListener(backupStoreCopyMonitor);
      PageCache pageCache = supportingClasses.getPageCache();
      StoreFiles storeFiles = new StoreFiles(this.fs, pageCache);
      BackupCopyService copyService = new BackupCopyService(this.fs, new FileMoveProvider(this.fs),
          storeFiles, this.internalLogProvider);
      BackupStrategy strategy = new DefaultBackupStrategy(supportingClasses.getBackupDelegator(),
          this.internalLogProvider, storeFiles);
      BackupStrategyWrapper wrapper =
          new BackupStrategyWrapper(strategy, copyService, this.fs, pageCache, this.userLogProvider,
              this.internalLogProvider);
      BackupStrategyCoordinator coordinator =
          new BackupStrategyCoordinator(this.fs, this.consistencyCheckService,
              this.internalLogProvider, this.progressMonitorFactory, wrapper);
      coordinator.performBackup(context);
    } catch (Throwable n11) {
      if (supportingClasses != null) {
        try {
          supportingClasses.close();
        } catch (Throwable n10) {
          n11.addSuppressed(n10);
        }
      }

      throw n11;
    }

    if (supportingClasses != null) {
      supportingClasses.close();
    }
  }

  private void verify(OnlineBackupContext context) throws BackupExecutionException {
    this.checkDestination(context.getDatabaseBackupDir().getParent());
    this.checkDestination(context.getReportDir());
  }

  private void checkDestination(Path path) throws BackupExecutionException {
    if (!this.fs.isDirectory(path.toFile())) {
      throw new BackupExecutionException(String.format("Directory '%s' does not exist.", path));
    }
  }

  public static final class Builder {

    private FileSystemAbstraction fs = new DefaultFileSystemAbstraction();
    private LogProvider userLogProvider = NullLogProvider.getInstance();
    private LogProvider internalLogProvider = NullLogProvider.getInstance();
    private ProgressMonitorFactory progressMonitorFactory;
    private Monitors monitors;
    private BackupSupportingClassesFactory supportingClassesFactory;
    private StorageEngineFactory storageEngineFactory;

    private Builder() {
      this.progressMonitorFactory = ProgressMonitorFactory.NONE;
      this.monitors = new Monitors();
      this.storageEngineFactory = StorageEngineFactory.selectStorageEngine();
    }

    public OnlineBackupExecutor.Builder withFileSystem(FileSystemAbstraction fs) {
      this.fs = fs;
      return this;
    }

    public OnlineBackupExecutor.Builder withUserLogProvider(LogProvider userLogProvider) {
      this.userLogProvider = userLogProvider;
      return this;
    }

    public OnlineBackupExecutor.Builder withInternalLogProvider(LogProvider internalLogProvider) {
      this.internalLogProvider = internalLogProvider;
      return this;
    }

    public OnlineBackupExecutor.Builder withProgressMonitorFactory(
        ProgressMonitorFactory progressMonitorFactory) {
      this.progressMonitorFactory = progressMonitorFactory;
      return this;
    }

    public OnlineBackupExecutor.Builder withMonitors(Monitors monitors) {
      this.monitors = monitors;
      return this;
    }

    public OnlineBackupExecutor.Builder withSupportingClassesFactory(
        BackupSupportingClassesFactory supportingClassesFactory) {
      this.supportingClassesFactory = supportingClassesFactory;
      return this;
    }

    public OnlineBackupExecutor.Builder withStorageEngineFactory(
        StorageEngineFactory storageEngineFactory) {
      this.storageEngineFactory = storageEngineFactory;
      return this;
    }

    public OnlineBackupExecutor build() {
      if (this.supportingClassesFactory == null) {
        this.supportingClassesFactory =
            new BackupSupportingClassesFactory(this.storageEngineFactory, this.fs,
                this.internalLogProvider, this.monitors);
      }

      return new OnlineBackupExecutor(this);
    }
  }
}
