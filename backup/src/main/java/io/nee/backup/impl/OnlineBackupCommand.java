/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.backup.impl;

import io.nee.kernel.impl.enterprise.configuration.OnlineBackupSettings;
import java.io.IOException;
import java.nio.file.Path;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.neo4j.cli.AbstractCommand;
import org.neo4j.cli.CommandFailedException;
import org.neo4j.cli.Converters.DatabaseNameConverter;
import org.neo4j.cli.ExecutionContext;
import org.neo4j.commandline.Util;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.ConfigUtils;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.SettingValueParsers;
import org.neo4j.configuration.helpers.NormalizedDatabaseName;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.consistency.ConsistencyCheckOptions;
import org.neo4j.internal.helpers.progress.ProgressMonitorFactory;
import org.neo4j.logging.Level;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;
import picocli.CommandLine.Command;
import picocli.CommandLine.Help.Visibility;
import picocli.CommandLine.Mixin;
import picocli.CommandLine.Option;

@Command(name = "backup", header = {
    "Perform an online backup from a running Neo4j enterprise server."}, description = {
    "Perform an online backup from a running Neo4j enterprise server. Neo4j's backup service must have been configured on the server beforehand.%n%nAll consistency checks except 'cc-graph' can be quite expensive so it may be useful to turn them off for very large databases. Increasing the heap size can also be a good idea. See 'neo4j-admin help' for details.%n%nFor more information see: https://neo4j.com/docs/operations-manual/4.0/backup/"})
public class OnlineBackupCommand extends AbstractCommand {

  private static final int STATUS_CONSISTENCY_CHECK_ERROR = 2;
  private static final int STATUS_CONSISTENCY_CHECK_INCONSISTENT = 3;
  @Option(names = {"--backup-dir"}, paramLabel = "<path>", required = true, description = {
      "Directory to place backup in."})
  private Path backupDir;
  @Option(names = {
      "--from"}, paramLabel = "<host:port>", defaultValue = "localhost:6362", description = {
      "Host and port of Neo4j."})
  private String from;
  @Option(names = {"--database"}, defaultValue = "neo4j", description = {
      "Name of the remote database to backup."}, converter = {
      DatabaseNameConverter.class})
  private NormalizedDatabaseName database;
  @Option(names = {
      "--fallback-to-full"}, paramLabel = "<true/false>", defaultValue = "true", showDefaultValue = Visibility.ALWAYS, description = {
      "If an incremental backup fails backup will move the old backup to <name>.err.<N> and fallback to a full."})
  private boolean fallbackToFull;
  @Option(names = {"--pagecache"}, paramLabel = "<size>", defaultValue = "8m", description = {
      "The size of the page cache to use for the backup process."})
  private String pagecacheMemory;
  @Option(names = {
      "--check-consistency"}, paramLabel = "<true/false>", defaultValue = "true", showDefaultValue = Visibility.ALWAYS, description = {
      "If a consistency check should be made."})
  private boolean checkConsistency;
  @Mixin
  private ConsistencyCheckOptions consistencyCheckOptions;
  @Option(names = {"--additional-config"}, paramLabel = "<path>", description = {
      "Configuration file to supply additional configuration in."})
  private Path additionalConfig;

  public OnlineBackupCommand(ExecutionContext ctx) {
    super(ctx);
  }

  private static Path requireExisting(Path p) {
    try {
      return p.toRealPath();
    } catch (IOException n2) {
      throw new CommandFailedException(String.format("Path '%s' does not exist.", p), n2);
    }
  }

  protected void execute() {
    this.backupDir = requireExisting(this.backupDir);
    SocketAddress address = SettingValueParsers.SOCKET_ADDRESS.parse(this.from);
    Path configFile = this.ctx.confDir().resolve("neo4j.conf");
    Config config = this.buildConfig(configFile, this.additionalConfig, this.backupDir);
    OnlineBackupContext onlineBackupContext = OnlineBackupContext.builder()
        .withBackupDirectory(requireExisting(this.backupDir)).withReportsDirectory(
            this.consistencyCheckOptions.getReportDir()).withAddress(address)
        .withDatabaseName(this.database.name()).withConfig(
            config).withFallbackToFullBackup(this.fallbackToFull)
        .withConsistencyCheck(this.checkConsistency).withConsistencyCheckGraph(
            this.consistencyCheckOptions.isCheckGraph()).withConsistencyCheckIndexes(
            this.consistencyCheckOptions.isCheckIndexes()).withConsistencyCheckIndexStructure(
            this.consistencyCheckOptions.isCheckIndexStructure())
        .withConsistencyCheckPropertyOwners(
            this.consistencyCheckOptions.isCheckPropertyOwners())
        .withConsistencyCheckLabelScanStore(
            this.consistencyCheckOptions.isCheckLabelScanStore()).build();
    LogProvider userLogProvider = Util.configuredLogProvider(config, this.ctx.out());
    LogProvider internalLogProvider =
        this.verbose ? userLogProvider : NullLogProvider.getInstance();
    OnlineBackupExecutor backupExecutor =
        OnlineBackupExecutor.builder().withFileSystem(this.ctx.fs())
            .withInternalLogProvider(internalLogProvider).withUserLogProvider(
            userLogProvider)
            .withProgressMonitorFactory(ProgressMonitorFactory.textual(this.ctx.err())).build();

    try {
      backupExecutor.executeBackup(onlineBackupContext);
    } catch (ConsistencyCheckExecutionException n10) {
      int exitCode = n10.consistencyCheckFailedToExecute() ? 2 : 3;
      throw new CommandFailedException(n10.getMessage(), n10, exitCode);
    } catch (Exception n11) {
      throw new CommandFailedException(
          "Execution of backup failed. " + ExceptionUtils.getRootCause(n11).getMessage(), n11);
    }

    this.ctx.out().println("Backup complete.");
  }

  private Config buildConfig(Path configFile, Path additionalConfigFile, Path backupDirectory) {
    Config cfg = Config.newBuilder().fromFileNoThrow(configFile.toFile())
        .fromFileNoThrow(additionalConfigFile).set(GraphDatabaseSettings.neo4j_home,
            backupDirectory)
        .set(GraphDatabaseSettings.pagecache_memory, this.pagecacheMemory)
        .set(GraphDatabaseSettings.pagecache_warmup_enabled,
            false)
        .set(OnlineBackupSettings.online_backup_enabled, false).build();
    ConfigUtils.disableAllConnectors(cfg);
    if (this.verbose) {
      cfg.set(GraphDatabaseSettings.store_internal_log_level, Level.DEBUG);
    }

    return cfg;
  }
}
