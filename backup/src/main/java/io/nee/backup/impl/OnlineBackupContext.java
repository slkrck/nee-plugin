/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.backup.impl;

import io.nee.kernel.impl.enterprise.configuration.OnlineBackupSettings;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.consistency.checking.full.ConsistencyFlags;

public class OnlineBackupContext {

  private final SocketAddress address;
  private final String databaseName;
  private final Path databaseBackupDir;
  private final Path reportDir;
  private final boolean fallbackToFullBackup;
  private final boolean consistencyCheck;
  private final ConsistencyFlags consistencyFlags;
  private final Config config;

  private OnlineBackupContext(SocketAddress address, String databaseName, Path databaseBackupDir,
      Path reportDir, boolean fallbackToFullBackup,
      boolean consistencyCheck, ConsistencyFlags consistencyFlags, Config config) {
    this.address = address;
    this.databaseName = databaseName;
    this.databaseBackupDir = databaseBackupDir;
    this.reportDir = reportDir;
    this.fallbackToFullBackup = fallbackToFullBackup;
    this.consistencyCheck = consistencyCheck;
    this.consistencyFlags = consistencyFlags;
    this.config = config;
  }

  public static OnlineBackupContext.Builder builder() {
    return new OnlineBackupContext.Builder();
  }

  public SocketAddress getAddress() {
    return this.address;
  }

  public String getDatabaseName() {
    return this.databaseName;
  }

  public Path getDatabaseBackupDir() {
    return this.databaseBackupDir;
  }

  public boolean fallbackToFullBackupEnabled() {
    return this.fallbackToFullBackup;
  }

  public boolean consistencyCheckEnabled() {
    return this.consistencyCheck;
  }

  public Path getReportDir() {
    return this.reportDir;
  }

  Config getConfig() {
    return this.config;
  }

  ConsistencyFlags getConsistencyFlags() {
    return this.consistencyFlags;
  }

  public static final class Builder {

    private SocketAddress address;
    private String databaseName;
    private Path backupDirectory;
    private Path reportsDirectory;
    private boolean fallbackToFullBackup = true;
    private Config config;
    private boolean consistencyCheck = true;
    private boolean consistencyCheckGraph = true;
    private boolean consistencyCheckIndexes = true;
    private boolean consistencyCheckIndexStructure = true;
    private boolean consistencyCheckLabelScanStore = true;
    private boolean consistencyCheckPropertyOwners;

    private Builder() {
    }

    public OnlineBackupContext.Builder withAddress(String hostname, int port) {
      return this.withAddress(new SocketAddress(hostname, port));
    }

    public OnlineBackupContext.Builder withAddress(SocketAddress address) {
      this.address = address;
      return this;
    }

    public OnlineBackupContext.Builder withDatabaseName(String databaseName) {
      this.databaseName = databaseName;
      return this;
    }

    public OnlineBackupContext.Builder withBackupDirectory(Path backupDirectory) {
      this.backupDirectory = backupDirectory;
      return this;
    }

    public OnlineBackupContext.Builder withReportsDirectory(Path reportsDirectory) {
      this.reportsDirectory = reportsDirectory;
      return this;
    }

    public OnlineBackupContext.Builder withFallbackToFullBackup(boolean fallbackToFullBackup) {
      this.fallbackToFullBackup = fallbackToFullBackup;
      return this;
    }

    public OnlineBackupContext.Builder withConfig(Config config) {
      this.config = config;
      return this;
    }

    public OnlineBackupContext.Builder withConsistencyCheck(boolean consistencyCheck) {
      this.consistencyCheck = consistencyCheck;
      return this;
    }

    public OnlineBackupContext.Builder withConsistencyCheckGraph(Boolean consistencyCheckGraph) {
      this.consistencyCheckGraph = consistencyCheckGraph;
      return this;
    }

    public OnlineBackupContext.Builder withConsistencyCheckIndexes(
        Boolean consistencyCheckIndexes) {
      this.consistencyCheckIndexes = consistencyCheckIndexes;
      return this;
    }

    public OnlineBackupContext.Builder withConsistencyCheckIndexStructure(
        Boolean consistencyCheckIndexStructure) {
      this.consistencyCheckIndexStructure = consistencyCheckIndexStructure;
      return this;
    }

    public OnlineBackupContext.Builder withConsistencyCheckLabelScanStore(
        Boolean consistencyCheckLabelScanStore) {
      this.consistencyCheckLabelScanStore = consistencyCheckLabelScanStore;
      return this;
    }

    public OnlineBackupContext.Builder withConsistencyCheckPropertyOwners(
        Boolean consistencyCheckPropertyOwners) {
      this.consistencyCheckPropertyOwners = consistencyCheckPropertyOwners;
      return this;
    }

    public OnlineBackupContext build() {
      if (this.config == null) {
        this.config = Config.defaults();
      }

      if (this.databaseName == null) {
        this.databaseName = this.config.get(GraphDatabaseSettings.default_database);
      }

      if (this.backupDirectory == null) {
        this.backupDirectory = Paths.get(".");
      }

      if (this.reportsDirectory == null) {
        this.reportsDirectory = Paths.get(".");
      }

      SocketAddress address = this.buildAddress();
      Path databaseBackupDirectory = this.backupDirectory.resolve(this.databaseName);
      ConsistencyFlags consistencyFlags = this.buildConsistencyFlags();
      return new OnlineBackupContext(address, this.databaseName, databaseBackupDirectory,
          this.reportsDirectory, this.fallbackToFullBackup,
          this.consistencyCheck, consistencyFlags, this.config);
    }

    private SocketAddress buildAddress() {
      if (this.address == null) {
        SocketAddress defaultListenAddress = this.config
            .get(OnlineBackupSettings.online_backup_listen_address);
        this.address = new SocketAddress(defaultListenAddress.getHostname(),
            defaultListenAddress.getPort());
      }

      return this.address;
    }

    private ConsistencyFlags buildConsistencyFlags() {
      return new ConsistencyFlags(this.consistencyCheckGraph, this.consistencyCheckIndexes,
          this.consistencyCheckIndexStructure,
          this.consistencyCheckLabelScanStore, this.consistencyCheckPropertyOwners);
    }
  }
}
