/*
 * Copyright (c) 2002-2018 "Neo4j,"
 * Neo4j Sweden AB [http://neo4j.com]
 *
 * This file is part of Neo4j Enterprise Edition. The included source
 * code can be redistributed and/or modified under the terms of the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3
 * (http://www.fsf.org/licensing/licenses/agpl-3.0.html)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * Neo4j object code can be licensed independently from the source
 * under separate terms from the AGPL. Inquiries can be directed to:
 * licensing@neo4j.com
 *
 * More information is also available at:
 * https://neo4j.com/licensing/
 */
package io.nee.backup.impl;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.catchup.CatchupClientFactory;
import io.nee.causalclustering.catchup.CatchupResponseAdaptor;
import io.nee.causalclustering.catchup.storecopy.DatabaseIdDownloadFailedException;
import io.nee.causalclustering.catchup.storecopy.RemoteStore;
import io.nee.causalclustering.catchup.storecopy.StoreCopyClient;
import io.nee.causalclustering.catchup.storecopy.StoreCopyFailedException;
import io.nee.causalclustering.catchup.storecopy.StoreIdDownloadFailedException;
import io.nee.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponse;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.kernel.database.DatabaseIdFactory;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StoreId;

class BackupDelegator extends LifecycleAdapter {

  private final RemoteStore remoteStore;
  private final StoreCopyClient storeCopyClient;
  private final CatchupClientFactory catchUpClient;
  private final LogProvider logProvider;

  BackupDelegator(RemoteStore remoteStore, StoreCopyClient storeCopyClient,
      CatchupClientFactory catchUpClient, LogProvider logProvider) {
    this.remoteStore = remoteStore;
    this.storeCopyClient = storeCopyClient;
    this.catchUpClient = catchUpClient;
    this.logProvider = logProvider;
  }

  void copy(SocketAddress fromAddress, StoreId expectedStoreId, NamedDatabaseId namedDatabaseId,
      DatabaseLayout databaseLayout)
      throws StoreCopyFailedException {
    remoteStore.copy(new CatchupAddressProvider.SingleAddressProvider(fromAddress), expectedStoreId,
        databaseLayout, true);
  }

  void tryCatchingUp(SocketAddress fromAddress, StoreId expectedStoreId,
      NamedDatabaseId namedDatabaseId, DatabaseLayout databaseLayout)
      throws StoreCopyFailedException {
    try {
      remoteStore.tryCatchingUp(new CatchupAddressProvider.SingleAddressProvider(fromAddress),
          expectedStoreId, databaseLayout, true, true);
    } catch (IOException e) {
      throw new StoreCopyFailedException(e);
    }
  }

  public void start() {
    this.catchUpClient.start();
  }

  public void stop() {
    this.catchUpClient.stop();
  }

  public StoreId fetchStoreId(SocketAddress fromAddress, NamedDatabaseId namedDatabaseId)
      throws StoreIdDownloadFailedException {
    return this.storeCopyClient.fetchStoreId(fromAddress);
  }

  public NamedDatabaseId fetchDatabaseId(SocketAddress fromAddress, final String databaseName)
      throws DatabaseIdDownloadFailedException {
    CatchupResponseAdaptor copyWithDatabaseId = new CatchupResponseAdaptor<NamedDatabaseId>() {
      public void onGetDatabaseIdResponse(CompletableFuture<NamedDatabaseId> signal,
          GetDatabaseIdResponse response) {
        signal.complete(DatabaseIdFactory.from(databaseName, response.databaseId().uuid()));
      }
    };

    try {
      return (NamedDatabaseId) this.catchUpClient.getClient(fromAddress,
          this.logProvider.getLog(this.getClass())).v3((client) ->
      {
        return client
            .getDatabaseId(databaseName);
      })
          .withResponseHandler(copyWithDatabaseId).request();
    } catch (Exception n5) {
      throw new DatabaseIdDownloadFailedException(n5);
    }
  }
}
