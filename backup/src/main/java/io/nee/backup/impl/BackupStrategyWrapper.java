/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.backup.impl;

import java.io.IOException;
import java.nio.file.Path;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.lifecycle.Lifespan;
import org.neo4j.kernel.recovery.Recovery;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.util.VisibleForTesting;

class BackupStrategyWrapper {

  private final BackupStrategy backupStrategy;
  private final BackupCopyService backupCopyService;
  private final Log userLog;
  private final Log debugLog;
  private final FileSystemAbstraction fs;
  private final PageCache pageCache;

  BackupStrategyWrapper(BackupStrategy backupStrategy, BackupCopyService backupCopyService,
      FileSystemAbstraction fs, PageCache pageCache,
      LogProvider userLogProvider, LogProvider logProvider) {
    this.backupStrategy = backupStrategy;
    this.backupCopyService = backupCopyService;
    this.fs = fs;
    this.pageCache = pageCache;
    this.userLog = userLogProvider.getLog(this.getClass());
    this.debugLog = logProvider.getLog(this.getClass());
  }

  void doBackup(OnlineBackupContext onlineBackupContext) throws BackupExecutionException {
    Lifespan ignore = new Lifespan(this.backupStrategy);

    try {
      this.performBackupWithoutLifecycle(onlineBackupContext);
    } catch (Throwable n6) {
      try {
        ignore.close();
      } catch (Throwable n5) {
        n6.addSuppressed(n5);
      }

      throw n6;
    }

    ignore.close();
  }

  private void performBackupWithoutLifecycle(OnlineBackupContext onlineBackupContext)
      throws BackupExecutionException {
    Path backupLocation = onlineBackupContext.getDatabaseBackupDir();
    SocketAddress address = onlineBackupContext.getAddress();
    Config config = onlineBackupContext.getConfig();
    DatabaseLayout backupLayout = DatabaseLayout.ofFlat(backupLocation.toFile());
    boolean previousBackupExists = this.backupCopyService.backupExists(backupLayout);
    boolean fallbackToFull = onlineBackupContext.fallbackToFullBackupEnabled();
    if (previousBackupExists) {
      this.debugLog.info("Previous backup found, trying incremental backup.");
      if (this.tryIncrementalBackup(backupLayout, config, address, fallbackToFull,
          onlineBackupContext.getDatabaseName())) {
        return;
      }
    }

    if (previousBackupExists && fallbackToFull) {
      this.debugLog.info("Incremental backup failed, a new full backup will be performed.");
      this.fullBackupWithTemporaryFolderResolutions(onlineBackupContext,
          onlineBackupContext.getDatabaseName());
    } else {
      if (previousBackupExists) {
        throw new BackupExecutionException(
            "Incremental backup failed but fallback to full backup is disallowed by configuration");
      }

      this.debugLog.info("Previous backup not found, a new full backup will be performed.");
      this.fullBackupWithTemporaryFolderResolutions(onlineBackupContext,
          onlineBackupContext.getDatabaseName());
    }
  }

  private boolean tryIncrementalBackup(DatabaseLayout backupLayout, Config config,
      SocketAddress address, boolean fallbackToFullAllowed,
      String databaseName) throws BackupExecutionException {
    try {
      this.backupStrategy.performIncrementalBackup(backupLayout, address, databaseName);
      this.performRecovery(config, backupLayout);
      return true;
    } catch (Exception n7) {
      if (fallbackToFullAllowed) {
        this.debugLog.warn("Incremental backup failed", n7);
        return false;
      } else {
        throw n7;
      }
    }
  }

  private void fullBackupWithTemporaryFolderResolutions(OnlineBackupContext onlineBackupContext,
      String databaseName) throws BackupExecutionException {
    Path userSpecifiedBackupLocation = onlineBackupContext.getDatabaseBackupDir();
    Path temporaryFullBackupLocation = this.backupCopyService
        .findAnAvailableLocationForNewFullBackup(userSpecifiedBackupLocation);
    boolean backupToATemporaryLocation = !userSpecifiedBackupLocation
        .equals(temporaryFullBackupLocation);
    if (backupToATemporaryLocation) {
      this.userLog.info(
          "Full backup will be first performed to a temporary directory '%s' because the specified directory '%s' already exists and is not empty",
          temporaryFullBackupLocation, userSpecifiedBackupLocation);
    }

    SocketAddress address = onlineBackupContext.getAddress();
    DatabaseLayout backupLayout = DatabaseLayout.ofFlat(temporaryFullBackupLocation.toFile());
    this.backupStrategy.performFullBackup(backupLayout, address, databaseName);
    this.performRecovery(onlineBackupContext.getConfig(), backupLayout);
    if (backupToATemporaryLocation) {
      this.renameTemporaryBackupToExpected(temporaryFullBackupLocation,
          userSpecifiedBackupLocation);
    }
  }

  @VisibleForTesting
  void performRecovery(Config config, DatabaseLayout backupLayout) throws BackupExecutionException {
    try {
      Recovery.performRecovery(this.fs, this.pageCache, config, backupLayout);
    } catch (IOException n4) {
      throw new BackupExecutionException(n4);
    }
  }

  private void renameTemporaryBackupToExpected(Path temporaryFullBackupLocation,
      Path userSpecifiedBackupLocation) throws BackupExecutionException {
    try {
      Path newBackupLocationForPreExistingBackup = this.backupCopyService
          .findNewBackupLocationForBrokenExisting(userSpecifiedBackupLocation);
      this.userLog
          .info("Moving pre-existing directory '%s' that does not contain a valid backup to '%s'",
              userSpecifiedBackupLocation, newBackupLocationForPreExistingBackup);
      this.backupCopyService
          .moveBackupLocation(userSpecifiedBackupLocation, newBackupLocationForPreExistingBackup);
      this.userLog.info("Moving temporary backup directory '%s' to the specified directory '%s'",
          temporaryFullBackupLocation, userSpecifiedBackupLocation);
      this.backupCopyService
          .moveBackupLocation(temporaryFullBackupLocation, userSpecifiedBackupLocation);
      this.backupCopyService
          .deletePreExistingBrokenBackupIfPossible(newBackupLocationForPreExistingBackup,
              userSpecifiedBackupLocation);
    } catch (IOException n4) {
      throw new BackupExecutionException(n4);
    }
  }
}
