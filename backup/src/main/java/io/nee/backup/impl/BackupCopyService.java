/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.backup.impl;

import io.nee.causalclustering.catchup.storecopy.StoreFiles;
import io.nee.com.storecopy.FileMoveAction;
import io.nee.com.storecopy.FileMoveProvider;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.FileSystemUtils;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StoreId;

class BackupCopyService {

  private static final int MAX_OLD_BACKUPS = 1000;
  private final FileSystemAbstraction fs;
  private final FileMoveProvider fileMoveProvider;
  private final StoreFiles storeFiles;
  private final Log log;

  /**
   * @param fs
   * @param fileMoveProvider
   * @param storeFiles
   * @param logProvider
   */
  BackupCopyService(FileSystemAbstraction fs, FileMoveProvider fileMoveProvider,
      StoreFiles storeFiles, LogProvider logProvider) {
    this.fs = fs;
    this.fileMoveProvider = fileMoveProvider;
    this.storeFiles = storeFiles;
    this.log = logProvider.getLog(this.getClass());
  }

  /**
   * @param file
   * @return
   */
  private static Supplier<RuntimeException> noFreeBackupLocation(Path file) {
    return () ->
    {
      return new RuntimeException(
          String.format(
              "Unable to find a free backup location for the provided %s. %d possible locations were already taken.",
              file, 1000));
    };
  }

  /**
   * @param originalBackupDirectory
   * @param pattern
   * @return
   */
  private static Stream<Path> availableAlternativeNames(Path originalBackupDirectory,
      String pattern) {
    return IntStream.range(0, 1000).mapToObj((iteration) ->
    {
      return alteredBackupDirectoryName(pattern, originalBackupDirectory, iteration);
    });
  }

  /**
   * @param pattern
   * @param directory
   * @param iteration
   * @return
   */
  private static Path alteredBackupDirectoryName(String pattern, Path directory, int iteration) {
    Path directoryName = directory.getFileName();
    return directory.resolveSibling(String.format(pattern, directoryName, iteration));
  }

  /**
   * @param oldLocation
   * @param newLocation
   * @throws IOException
   */
  void moveBackupLocation(Path oldLocation, Path newLocation) throws IOException {
    try {
      File source = oldLocation.toFile();
      File target = newLocation.toFile();
      Iterator moves = this.fileMoveProvider.traverseForMoving(source).iterator();

      while (moves.hasNext()) {
        ((FileMoveAction) moves.next()).move(target);
      }

      this.fs.deleteRecursively(oldLocation.toFile());
    } catch (IOException n6) {
      throw new IOException(
          "Failed to rename backup directory from " + oldLocation + " to " + newLocation, n6);
    }
  }

  /**
   * @param preExistingBrokenBackupDir
   * @param newSuccessfulBackupDir
   * @throws IOException
   */
  void deletePreExistingBrokenBackupIfPossible(Path preExistingBrokenBackupDir,
      Path newSuccessfulBackupDir) throws IOException {
    DatabaseLayout preExistingBrokenBackupLayout = DatabaseLayout
        .ofFlat(preExistingBrokenBackupDir.toFile());
    DatabaseLayout newSuccessfulBackupLayout = DatabaseLayout
        .ofFlat(newSuccessfulBackupDir.toFile());

    StoreId preExistingBrokenBackupStoreId;
    try {
      preExistingBrokenBackupStoreId = this.storeFiles.readStoreId(preExistingBrokenBackupLayout);
    } catch (IOException n9) {
      this.log.warn(
          "Unable to read store ID from the pre-existing invalid backup. It will not be deleted",
          n9);
      return;
    }

    StoreId newSuccessfulBackupStoreId;
    try {
      newSuccessfulBackupStoreId = this.storeFiles.readStoreId(newSuccessfulBackupLayout);
    } catch (IOException n8) {
      throw new IOException("Unable to read store ID from the new successful backup", n8);
    }

    if (newSuccessfulBackupStoreId.equals(preExistingBrokenBackupStoreId)) {
      this.log.info(
          "Deleting the pre-existing invalid backup because its store ID is the same as in the new successful backup %s",
          newSuccessfulBackupStoreId);
      this.fs.deleteRecursively(preExistingBrokenBackupDir.toFile());
    } else {
      this.log.info(
          "Pre-existing invalid backup can't be deleted because its store ID %s is not the same as in the new successful backup %s",
          preExistingBrokenBackupStoreId, newSuccessfulBackupStoreId);
    }
  }

  /**
   * @param databaseLayout
   * @return
   */
  boolean backupExists(DatabaseLayout databaseLayout) {
    return this.fs.fileExists(databaseLayout.metadataStore());
  }

  /**
   * @param existingBackup
   * @return
   */
  Path findNewBackupLocationForBrokenExisting(Path existingBackup) {
    return this.findAnAvailableBackupLocation(existingBackup, "%s.err.%d");
  }

  /**
   * @param desiredBackupLocation
   * @return
   */
  Path findAnAvailableLocationForNewFullBackup(Path desiredBackupLocation) {
    return this.findAnAvailableBackupLocation(desiredBackupLocation, "%s.temp.%d");
  }

  /**
   * @param file
   * @param pattern
   * @return
   */
  private Path findAnAvailableBackupLocation(Path file, String pattern) {
    return FileSystemUtils.isEmptyOrNonExistingDirectory(this.fs, file.toFile()) ? file
        : availableAlternativeNames(file, pattern).filter((f) ->
        {
          return FileSystemUtils
              .isEmptyOrNonExistingDirectory(
                  this.fs,
                  f.toFile());
        })
            .findFirst()
            .orElseThrow(
                noFreeBackupLocation(
                    file));
  }
}
