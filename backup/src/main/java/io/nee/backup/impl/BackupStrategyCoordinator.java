/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.backup.impl;

import java.io.File;
import org.neo4j.consistency.ConsistencyCheckService;
import org.neo4j.consistency.ConsistencyCheckService.Result;
import org.neo4j.consistency.checking.full.ConsistencyFlags;
import org.neo4j.internal.helpers.progress.ProgressMonitorFactory;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.logging.LogProvider;

class BackupStrategyCoordinator {

  private final FileSystemAbstraction fs;
  private final ConsistencyCheckService consistencyCheckService;
  private final LogProvider logProvider;
  private final ProgressMonitorFactory progressMonitorFactory;
  private final BackupStrategyWrapper strategy;

  BackupStrategyCoordinator(FileSystemAbstraction fs,
      ConsistencyCheckService consistencyCheckService, LogProvider logProvider,
      ProgressMonitorFactory progressMonitorFactory, BackupStrategyWrapper strategy) {
    this.fs = fs;
    this.consistencyCheckService = consistencyCheckService;
    this.logProvider = logProvider;
    this.progressMonitorFactory = progressMonitorFactory;
    this.strategy = strategy;
  }

  public void performBackup(OnlineBackupContext context)
      throws BackupExecutionException, ConsistencyCheckExecutionException {
    File destination = context.getDatabaseBackupDir().toFile();
    ConsistencyFlags consistencyFlags = context.getConsistencyFlags();
    this.strategy.doBackup(context);
    if (context.consistencyCheckEnabled()) {
      this.performConsistencyCheck(context, consistencyFlags, DatabaseLayout.ofFlat(destination));
    }
  }

  private void performConsistencyCheck(OnlineBackupContext context,
      ConsistencyFlags consistencyFlags, DatabaseLayout layout)
      throws ConsistencyCheckExecutionException {
    Result ccResult;
    try {
      ccResult =
          this.consistencyCheckService
              .runFullConsistencyCheck(layout, context.getConfig(), this.progressMonitorFactory,
                  this.logProvider, this.fs,
                  false, context.getReportDir().toFile(), consistencyFlags);
    } catch (Exception n6) {
      throw new ConsistencyCheckExecutionException("Failed to do consistency check on the backup",
          n6, true);
    }

    if (!ccResult.isSuccessful()) {
      throw new ConsistencyCheckExecutionException(
          String.format("Inconsistencies found. See '%s' for details.", ccResult.reportFile()),
          false);
    }
  }
}
