/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.backup.impl;

import io.nee.causalclustering.catchup.storecopy.StoreCopyClientMonitor;
import java.util.concurrent.TimeUnit;
import org.neo4j.internal.helpers.Format;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.time.Clocks;
import org.neo4j.time.Stopwatch;
import org.neo4j.time.SystemNanoClock;

class BackupOutputMonitor implements StoreCopyClientMonitor {

  private final Log log;
  private final SystemNanoClock clock;
  private Stopwatch startTime;
  private Stopwatch partStartTime;

  BackupOutputMonitor(LogProvider logProvider) {
    this(logProvider, Clocks.nanoClock());
  }

  BackupOutputMonitor(LogProvider logProvider, SystemNanoClock clock) {
    this.log = logProvider.getLog(this.getClass());
    this.clock = clock;
  }

  public void start() {
    this.startTime = this.clock.startStopWatch();
  }

  public void startReceivingStoreFiles() {
    this.log.info("Start receiving store files");
    this.notePartStartTime();
  }

  public void finishReceivingStoreFiles() {
    this.log.info("Finish receiving store files, took %s", this.durationSincePartStartTime());
  }

  public void startReceivingStoreFile(String file) {
    this.log.info("Start receiving store file %s", file);
  }

  public void finishReceivingStoreFile(String file) {
    this.log.info("Finish receiving store file %s", file);
  }

  public void startReceivingTransactions(long startTxId) {
    this.log.info("Start receiving transactions from %d", startTxId);
    this.notePartStartTime();
  }

  public void finishReceivingTransactions(long endTxId) {
    this.log.info("Finish receiving transactions at %d, took %s", endTxId,
        this.durationSincePartStartTime());
  }

  public void startRecoveringStore() {
    this.log.info("Start recovering store");
    this.notePartStartTime();
  }

  public void finishRecoveringStore() {
    this.log.info("Finish recovering store, took %s", this.durationSincePartStartTime());
  }

  public void startReceivingIndexSnapshots() {
    this.log.info("Start receiving index snapshots");
    this.notePartStartTime();
  }

  public void startReceivingIndexSnapshot(long indexId) {
    this.log.info("Start receiving index snapshot id %d", indexId);
  }

  public void finishReceivingIndexSnapshot(long indexId) {
    this.log.info("Finished receiving index snapshot id %d", indexId);
  }

  public void finishReceivingIndexSnapshots() {
    this.log.info("Finished receiving index snapshots, took %s", this.durationSincePartStartTime());
  }

  public void finish() {
    this.log
        .info("Finished, took %s", Format.duration(this.startTime.elapsed(TimeUnit.MILLISECONDS)));
  }

  private void notePartStartTime() {
    this.partStartTime = this.clock.startStopWatch();
  }

  private String durationSincePartStartTime() {
    return Format.duration(this.partStartTime.elapsed(TimeUnit.MILLISECONDS));
  }
}
