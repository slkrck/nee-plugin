/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.backup.impl;

import java.util.Collection;
import java.util.Objects;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.impl.api.CloseableResourceManager;
import org.neo4j.storageengine.api.StorageEngineFactory;

class BackupSupportingClasses implements AutoCloseable {

  private final BackupDelegator backupDelegator;
  private final CloseableResourceManager closeableResourceManager;
  private final PageCache pageCache;
  private final StorageEngineFactory storageEngineFactory;

  BackupSupportingClasses(BackupDelegator backupDelegator, PageCache pageCache,
      Collection<AutoCloseable> closeables) {
    this.backupDelegator = backupDelegator;
    this.pageCache = pageCache;
    this.closeableResourceManager = new CloseableResourceManager();
    this.storageEngineFactory = StorageEngineFactory.selectStorageEngine();
    CloseableResourceManager n10001 = this.closeableResourceManager;
    Objects.requireNonNull(n10001);
    closeables.forEach(n10001::registerCloseableResource);
  }

  public BackupDelegator getBackupDelegator() {
    return this.backupDelegator;
  }

  public PageCache getPageCache() {
    return this.pageCache;
  }

  public StorageEngineFactory getStorageEngineFactory() {
    return this.storageEngineFactory;
  }

  public void close() {
    this.closeableResourceManager.closeAllCloseableResources();
  }
}
