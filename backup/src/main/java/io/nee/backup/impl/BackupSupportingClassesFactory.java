/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.backup.impl;

import io.nee.causalclustering.catchup.CatchupClientBuilder;
import io.nee.causalclustering.catchup.CatchupClientFactory;
import io.nee.causalclustering.catchup.storecopy.RemoteStore;
import io.nee.causalclustering.catchup.storecopy.StoreCopyClient;
import io.nee.causalclustering.catchup.tx.TransactionLogCatchUpFactory;
import io.nee.causalclustering.catchup.tx.TxPullClient;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.core.SupportedProtocolCreator;
import io.nee.causalclustering.net.BootstrapConfiguration;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import java.time.Clock;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.ssl.SslPolicyScope;
import org.neo4j.internal.helpers.ExponentialBackoffStrategy;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.impl.pagecache.ConfigurableStandalonePageCacheFactory;
import org.neo4j.kernel.impl.scheduler.JobSchedulerFactory;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.ssl.SslPolicy;
import org.neo4j.ssl.config.SslPolicyLoader;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.util.VisibleForTesting;

public class BackupSupportingClassesFactory {

  private final LogProvider logProvider;
  private final Clock clock;
  private final Monitors monitors;
  private final FileSystemAbstraction fileSystemAbstraction;
  private final TransactionLogCatchUpFactory transactionLogCatchUpFactory;
  private final StorageEngineFactory storageEngineFactory;

  public BackupSupportingClassesFactory(StorageEngineFactory storageEngineFactory,
      FileSystemAbstraction fileSystemAbstraction, LogProvider logProvider,
      Monitors monitors) {
    this.logProvider = logProvider;
    this.clock = Clock.systemUTC();
    this.monitors = monitors;
    this.fileSystemAbstraction = fileSystemAbstraction;
    this.transactionLogCatchUpFactory = new TransactionLogCatchUpFactory();
    this.storageEngineFactory = storageEngineFactory;
  }

  private static BackupDelegator backupDelegator(RemoteStore remoteStore,
      StoreCopyClient storeCopyClient, CatchupClientFactory catchUpClient,
      LogProvider logProvider) {
    return new BackupDelegator(remoteStore, storeCopyClient, catchUpClient, logProvider);
  }

  private static PageCache createPageCache(FileSystemAbstraction fileSystemAbstraction,
      Config config, JobScheduler jobScheduler) {
    return ConfigurableStandalonePageCacheFactory
        .createPageCache(fileSystemAbstraction, config, jobScheduler);
  }

  BackupSupportingClasses createSupportingClasses(OnlineBackupContext context) {
    JobScheduler jobScheduler = JobSchedulerFactory.createInitialisedScheduler();
    PageCache pageCache = createPageCache(this.fileSystemAbstraction, context.getConfig(),
        jobScheduler);
    return new BackupSupportingClasses(
        this.backupDelegatorFromConfig(pageCache, context, jobScheduler), pageCache,
        Arrays.asList(pageCache, jobScheduler));
  }

  private BackupDelegator backupDelegatorFromConfig(PageCache pageCache,
      OnlineBackupContext onlineBackupContext, JobScheduler jobScheduler) {
    Config config = onlineBackupContext.getConfig();
    CatchupClientFactory catchUpClient = this.catchUpClient(onlineBackupContext, jobScheduler);

    TxPullClient txPullClient = new TxPullClient(catchUpClient, null, this.monitors,
        this.logProvider);

    ExponentialBackoffStrategy backOffStrategy =
        new ExponentialBackoffStrategy(1L,
            config.get(CausalClusteringSettings.store_copy_backoff_max_wait).toMillis(),
            TimeUnit.MILLISECONDS);
    StoreCopyClient storeCopyClient = new StoreCopyClient(catchUpClient, null, monitors,
        this.logProvider, backOffStrategy);

    RemoteStore remoteStore = new RemoteStore(this.logProvider, this.fileSystemAbstraction,
        pageCache, storeCopyClient,
        txPullClient, this.transactionLogCatchUpFactory, config, this.monitors,
        this.storageEngineFactory,
        null); // TODO: Where to get namedDatabaseId

    return backupDelegator(remoteStore, storeCopyClient, catchUpClient, this.logProvider);
  }

  @VisibleForTesting
  protected NettyPipelineBuilderFactory createPipelineBuilderFactory(SslPolicy sslPolicy) {
    return new NettyPipelineBuilderFactory(sslPolicy);
  }

  private CatchupClientFactory catchUpClient(OnlineBackupContext onlineBackupContext,
      JobScheduler jobScheduler) {
    Config config = onlineBackupContext.getConfig();
    SupportedProtocolCreator supportedProtocolCreator = new SupportedProtocolCreator(config,
        this.logProvider);
    ApplicationSupportedProtocols supportedCatchupProtocols = supportedProtocolCreator
        .getSupportedCatchupProtocolsFromConfiguration();
    SslPolicy sslPolicy = this.loadSslPolicy(config);
    NettyPipelineBuilderFactory pipelineBuilderFactory = this
        .createPipelineBuilderFactory(sslPolicy);
    return CatchupClientBuilder.builder().catchupProtocols(supportedCatchupProtocols)
        .modifierProtocols(
            supportedProtocolCreator.createSupportedModifierProtocols())
        .pipelineBuilder(pipelineBuilderFactory).inactivityTimeout(
            config.get(CausalClusteringSettings.catch_up_client_inactivity_timeout))
        .scheduler(jobScheduler).bootstrapConfig(
            BootstrapConfiguration.clientConfig(config))
        .handShakeTimeout(config.get(CausalClusteringSettings.handshake_timeout)).clock(
            this.clock).debugLogProvider(this.logProvider).userLogProvider(this.logProvider)
        .build();
  }

  private SslPolicy loadSslPolicy(Config config) {
    SslPolicyLoader sslPolicyLoader = SslPolicyLoader.create(config, this.logProvider);
    return sslPolicyLoader.hasPolicyForSource(SslPolicyScope.BACKUP) ? sslPolicyLoader
        .getPolicy(SslPolicyScope.BACKUP) : null;
  }
}
