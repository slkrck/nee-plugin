/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.com.storecopy;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;

public interface FileMoveAction {

  static FileMoveAction copyViaFileSystem(final File file, File basePath) {
    final Path base = basePath.toPath();
    return new FileMoveAction() {
      public void move(File toDir, CopyOption... copyOptions) throws IOException {
        Path originalPath = file.toPath();
        Path relativePath = base.relativize(originalPath);
        Path resolvedPath = toDir.toPath().resolve(relativePath);
        if (!Files.isSymbolicLink(resolvedPath.getParent())) {
          Files.createDirectories(resolvedPath.getParent());
        }

        Files.copy(originalPath, resolvedPath, copyOptions);
      }

      public File file() {
        return file;
      }
    };
  }

  static FileMoveAction moveViaFileSystem(final File sourceFile, final File sourceDirectory) {
    return new FileMoveAction() {
      public void move(File toDir, CopyOption... copyOptions) throws IOException {
        FileMoveAction.copyViaFileSystem(sourceFile, sourceDirectory).move(toDir, copyOptions);
        if (!sourceFile.delete()) {
          throw new IOException("Unable to delete source file after copying " + sourceFile);
        }
      }

      public File file() {
        return sourceFile;
      }
    };
  }

  void move(File n1, CopyOption... n2) throws IOException;

  File file();
}
