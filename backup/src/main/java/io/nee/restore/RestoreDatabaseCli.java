/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.restore;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import org.neo4j.cli.AbstractCommand;
import org.neo4j.cli.Converters.DatabaseNameConverter;
import org.neo4j.cli.ExecutionContext;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.ConfigUtils;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.helpers.NormalizedDatabaseName;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "restore", description = {"Restore a backed up database."})
public class RestoreDatabaseCli extends AbstractCommand {

  @Option(names = {"--from"}, paramLabel = "<path>", required = true, description = {
      "Path to backup to restore from."})
  private File from;
  @Option(names = {"--database"}, description = {
      "Name of the database to restore."}, defaultValue = "neo4j", converter = {
      DatabaseNameConverter.class})
  private NormalizedDatabaseName database;
  @Option(names = {"--force"}, arity = "0", description = {
      "If an existing database should be replaced."})
  private boolean force;

  public RestoreDatabaseCli(ExecutionContext ctx) {
    super(ctx);
  }

  private static Config loadNeo4jConfig(Path homeDir, Path configDir) {
    Config cfg = Config.newBuilder().fromFile(configDir.resolve("neo4j.conf").toFile())
        .set(GraphDatabaseSettings.neo4j_home, homeDir).build();
    ConfigUtils.disableAllConnectors(cfg);
    return cfg;
  }

  public void execute() throws IOException {
    Config config = loadNeo4jConfig(this.ctx.homeDir(), this.ctx.confDir());
    RestoreDatabaseCommand restoreDatabaseCommand = new RestoreDatabaseCommand(this.ctx.fs(),
        this.from, config, this.database.name(), this.force);
    restoreDatabaseCommand.execute();
  }
}
