/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.restore;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import org.neo4j.cli.CommandFailedException;
import org.neo4j.commandline.Util;
import org.neo4j.commandline.dbms.CannotWriteException;
import org.neo4j.commandline.dbms.LockChecker;
import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.layout.Neo4jLayout;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.kernel.impl.transaction.log.files.LogFilesBuilder;
import org.neo4j.kernel.impl.util.Validators;
import org.neo4j.kernel.internal.locker.FileLockException;

public class RestoreDatabaseCommand {

  private final FileSystemAbstraction fs;
  private final File fromDatabasePath;
  private final DatabaseLayout targetDatabaseLayout;
  private final boolean forceOverwrite;

  public RestoreDatabaseCommand(FileSystemAbstraction fs, File fromDatabasePath, Config config,
      String databaseName, boolean forceOverwrite) {
    this.fs = fs;
    this.fromDatabasePath = fromDatabasePath;
    this.forceOverwrite = forceOverwrite;
    this.targetDatabaseLayout = buildTargetDatabaseLayout(databaseName, config);
  }

  private static DatabaseLayout buildTargetDatabaseLayout(String databaseName, Config config) {
    return Neo4jLayout.of(config).databaseLayout(databaseName);
  }

  public void execute() throws IOException {
    if (!this.fs.fileExists(this.fromDatabasePath)) {
      throw new IllegalArgumentException(
          String.format("Source directory does not exist [%s]", this.fromDatabasePath));
    } else {
      try {
        Validators.CONTAINS_EXISTING_DATABASE.validate(this.fromDatabasePath);
      } catch (IllegalArgumentException n5) {
        throw new IllegalArgumentException(
            String.format("Source directory is not a database backup [%s]", this.fromDatabasePath));
      }

      if (this.fs.fileExists(this.targetDatabaseLayout.databaseDirectory())
          && !this.forceOverwrite) {
        throw new IllegalArgumentException(String
            .format("Database with name [%s] already exists at %s",
                this.targetDatabaseLayout.getDatabaseName(),
                this.targetDatabaseLayout.databaseDirectory()));
      } else {
        this.fs.mkdirs(this.targetDatabaseLayout.databaseDirectory());

        try {
          Closeable ignored = LockChecker.checkDatabaseLock(this.targetDatabaseLayout);

          try {
            this.cleanTargetDirectories();
            this.restoreDatabaseFiles();
          } catch (Throwable n6) {
            if (ignored != null) {
              try {
                ignored.close();
              } catch (Throwable n4) {
                n6.addSuppressed(n4);
              }
            }

            throw n6;
          }

          if (ignored != null) {
            ignored.close();
          }
        } catch (FileLockException n7) {
          throw new CommandFailedException(
              "The database is in use. Stop database '" + this.targetDatabaseLayout
                  .getDatabaseName() + "' and try again.", n7);
        } catch (CannotWriteException n8) {
          throw new CommandFailedException("You do not have permission to restore database.", n8);
        }
      }
    }
  }

  private void cleanTargetDirectories() throws IOException {
    File databaseDirectory = this.targetDatabaseLayout.databaseDirectory();
    File transactionLogsDirectory = this.targetDatabaseLayout.getTransactionLogsDirectory();
    File databaseLockFile = this.targetDatabaseLayout.databaseLockFile();
    File[] filesToRemove = this.fs.listFiles(databaseDirectory, (dir, name) ->
    {
      return !name.equals(databaseLockFile.getName());
    });
    if (filesToRemove != null) {
      File[] n5 = filesToRemove;
      int n6 = filesToRemove.length;

      for (int n7 = 0; n7 < n6; ++n7) {
        File file = n5[n7];
        this.fs.deleteRecursively(file);
      }
    }

    if (!Util.isSameOrChildFile(databaseDirectory, transactionLogsDirectory)) {
      this.fs.deleteRecursively(transactionLogsDirectory);
    }
  }

  private void restoreDatabaseFiles() throws IOException {
    File[] databaseFiles = this.fs.listFiles(this.fromDatabasePath);
    LogFiles transactionLogFiles = LogFilesBuilder
        .logFilesBasedOnlyBuilder(this.fromDatabasePath, this.fs).build();
    if (databaseFiles != null) {
      File databaseDirectory = this.targetDatabaseLayout.databaseDirectory();
      File transactionLogsDirectory = this.targetDatabaseLayout.getTransactionLogsDirectory();
      File databaseLockFile = this.targetDatabaseLayout.databaseLockFile();
      File[] n6 = databaseFiles;
      int n7 = databaseFiles.length;

      for (int n8 = 0; n8 < n7; ++n8) {
        File file = n6[n8];
        File targetDirectory;
        if (file.isDirectory()) {
          targetDirectory = new File(databaseDirectory, file.getName());
          this.fs.mkdirs(targetDirectory);
          this.fs.copyRecursively(file, targetDirectory);
        } else {
          targetDirectory =
              transactionLogFiles.isLogFile(file) ? transactionLogsDirectory : databaseDirectory;
          File targetFile = new File(targetDirectory, file.getName());
          if (!databaseLockFile.equals(targetFile)) {
            this.fs.copyToDirectory(file, targetDirectory);
          }
        }
      }
    }
  }
}
