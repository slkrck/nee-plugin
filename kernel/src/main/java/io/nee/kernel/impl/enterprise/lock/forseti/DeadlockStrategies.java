/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.enterprise.lock.forseti;

import org.neo4j.util.FeatureToggles;

public enum DeadlockStrategies implements ForsetiLockManager.DeadlockResolutionStrategy {
  ABORT_YOUNG {
    public boolean shouldAbort(ForsetiClient clientThatsAsking,
        ForsetiClient clientWereDeadlockedWith) {
      if (DeadlockStrategies.isSameClient(clientThatsAsking, clientWereDeadlockedWith)) {
        return true;
      } else {
        long ourCount = clientThatsAsking.activeLockCount();
        long otherCount = clientWereDeadlockedWith.activeLockCount();
        if (ourCount > otherCount) {
          return false;
        } else if (otherCount > ourCount) {
          return true;
        } else {
          return clientThatsAsking.id() >= clientWereDeadlockedWith.id();
        }
      }
    }
  },
  ABORT_OLD {
    public boolean shouldAbort(ForsetiClient clientThatsAsking,
        ForsetiClient clientWereDeadlockedWith) {
      if (DeadlockStrategies.isSameClient(clientThatsAsking, clientWereDeadlockedWith)) {
        return true;
      } else {
        return !ABORT_YOUNG.shouldAbort(clientThatsAsking, clientWereDeadlockedWith);
      }
    }
  },
  ABORT_SHORT_WAIT_LIST {
    public boolean shouldAbort(ForsetiClient clientThatsAsking,
        ForsetiClient clientWereDeadlockedWith) {
      if (DeadlockStrategies.isSameClient(clientThatsAsking, clientWereDeadlockedWith)) {
        return true;
      } else {
        int ourCount = clientThatsAsking.waitListSize();
        int otherCount = clientWereDeadlockedWith.waitListSize();
        if (ourCount > otherCount) {
          return false;
        } else if (otherCount > ourCount) {
          return true;
        } else {
          return clientThatsAsking.id() > clientWereDeadlockedWith.id();
        }
      }
    }
  },
  ABORT_LONG_WAIT_LIST {
    public boolean shouldAbort(ForsetiClient clientThatsAsking,
        ForsetiClient clientWereDeadlockedWith) {
      if (DeadlockStrategies.isSameClient(clientThatsAsking, clientWereDeadlockedWith)) {
        return true;
      } else {
        return !ABORT_SHORT_WAIT_LIST.shouldAbort(clientThatsAsking, clientWereDeadlockedWith);
      }
    }
  };

  public static final ForsetiLockManager.DeadlockResolutionStrategy DEFAULT =
      FeatureToggles.flag(DeadlockStrategies.class, "strategy", ABORT_YOUNG);

  private static boolean isSameClient(ForsetiClient a, ForsetiClient b) {
    return a.id() == b.id();
  }

  public abstract boolean shouldAbort(ForsetiClient n1, ForsetiClient n2);
}
