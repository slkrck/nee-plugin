/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.enterprise;

import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.NodeLabelIndexCursor;
import org.neo4j.internal.kernel.api.PropertyCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.internal.kernel.api.exceptions.schema.ConstraintValidationException;
import org.neo4j.internal.kernel.api.exceptions.schema.ConstraintValidationException.Phase;
import org.neo4j.internal.kernel.api.exceptions.schema.CreateConstraintFailureException;
import org.neo4j.internal.schema.ConstraintDescriptor;
import org.neo4j.internal.schema.LabelSchemaDescriptor;
import org.neo4j.internal.schema.RelationTypeSchemaDescriptor;
import org.neo4j.internal.schema.constraints.NodeKeyConstraintDescriptor;
import org.neo4j.kernel.api.exceptions.schema.NodePropertyExistenceException;
import org.neo4j.kernel.api.exceptions.schema.RelationshipPropertyExistenceException;
import org.neo4j.kernel.impl.constraints.StandardConstraintSemantics;
import org.neo4j.storageengine.api.StorageReader;
import org.neo4j.storageengine.api.txstate.ReadableTransactionState;
import org.neo4j.storageengine.api.txstate.TxStateVisitor;

public class EnterpriseConstraintSemantics extends StandardConstraintSemantics {

  public EnterpriseConstraintSemantics() {
    super(2);
  }

  public String getName() {
    return "enterpriseConstraints";
  }

  protected ConstraintDescriptor readNonStandardConstraint(ConstraintDescriptor constraint,
      String errorMessage) {
    if (!constraint.enforcesPropertyExistence()) {
      throw new IllegalStateException("Unsupported constraint type: " + constraint);
    } else {
      return constraint;
    }
  }

  public ConstraintDescriptor createNodeKeyConstraintRule(long ruleId,
      NodeKeyConstraintDescriptor descriptor, long indexId) {
    return this.accessor.createNodeKeyConstraintRule(ruleId, descriptor, indexId);
  }

  public ConstraintDescriptor createExistenceConstraint(long ruleId,
      ConstraintDescriptor descriptor) {
    return this.accessor.createExistenceConstraint(ruleId, descriptor);
  }

  public void validateNodePropertyExistenceConstraint(NodeLabelIndexCursor allNodes,
      NodeCursor nodeCursor, PropertyCursor propertyCursor,
      LabelSchemaDescriptor descriptor) throws CreateConstraintFailureException {
    while (allNodes.next()) {
      allNodes.node(nodeCursor);

      while (nodeCursor.next()) {
        int[] n5 = descriptor.getPropertyIds();
        int n6 = n5.length;

        for (int n7 = 0; n7 < n6; ++n7) {
          int propertyKey = n5[n7];
          nodeCursor.properties(propertyCursor);
          if (!this.hasProperty(propertyCursor, propertyKey)) {
            throw this.createConstraintFailure(
                new NodePropertyExistenceException(descriptor, Phase.VERIFICATION,
                    nodeCursor.nodeReference()));
          }
        }
      }
    }
  }

  public void validateNodeKeyConstraint(NodeLabelIndexCursor allNodes, NodeCursor nodeCursor,
      PropertyCursor propertyCursor,
      LabelSchemaDescriptor descriptor) throws CreateConstraintFailureException {
    this.validateNodePropertyExistenceConstraint(allNodes, nodeCursor, propertyCursor, descriptor);
  }

  private boolean hasProperty(PropertyCursor propertyCursor, int property) {
    while (true) {
      if (propertyCursor.next()) {
        if (propertyCursor.propertyKey() != property) {
          continue;
        }

        return true;
      }

      return false;
    }
  }

  public void validateRelationshipPropertyExistenceConstraint(
      RelationshipScanCursor relationshipCursor, PropertyCursor propertyCursor,
      RelationTypeSchemaDescriptor descriptor) throws CreateConstraintFailureException {
    label22:
    while (true) {
      if (relationshipCursor.next()) {
        relationshipCursor.properties(propertyCursor);
        int[] n4 = descriptor.getPropertyIds();
        int n5 = n4.length;
        int n6 = 0;

        while (true) {
          if (n6 >= n5) {
            continue label22;
          }

          int propertyKey = n4[n6];
          if (relationshipCursor.type() == descriptor.getRelTypeId() && !this
              .hasProperty(propertyCursor, propertyKey)) {
            throw this.createConstraintFailure(
                new RelationshipPropertyExistenceException(descriptor, Phase.VERIFICATION,
                    relationshipCursor.relationshipReference()));
          }

          ++n6;
        }
      }

      return;
    }
  }

  private CreateConstraintFailureException createConstraintFailure(
      ConstraintValidationException it) {
    return new CreateConstraintFailureException(it.constraint(), it);
  }

  public TxStateVisitor decorateTxStateVisitor(StorageReader storageReader, Read read,
      CursorFactory cursorFactory, ReadableTransactionState txState,
      TxStateVisitor visitor) {
    return !txState.hasDataChanges() ? visitor
        : PropertyExistenceEnforcer.getOrCreatePropertyExistenceEnforcerFrom(storageReader)
            .decorate(visitor, read,
                cursorFactory);
  }
}
