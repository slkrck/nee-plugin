/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.enterprise.lock.forseti;

import java.time.Clock;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import java.util.function.IntFunction;
import java.util.stream.Stream;
import org.eclipse.collections.api.block.procedure.primitive.LongProcedure;
import org.eclipse.collections.api.iterator.IntIterator;
import org.eclipse.collections.api.map.primitive.LongIntMap;
import org.eclipse.collections.api.map.primitive.MutableLongIntMap;
import org.eclipse.collections.impl.map.mutable.primitive.LongIntHashMap;
import org.neo4j.collection.pool.Pool;
import org.neo4j.graphdb.TransactionFailureException;
import org.neo4j.internal.unsafe.UnsafeUtil;
import org.neo4j.kernel.DeadlockDetectedException;
import org.neo4j.kernel.impl.api.LeaseClient;
import org.neo4j.kernel.impl.locking.ActiveLock;
import org.neo4j.kernel.impl.locking.ActiveLock.Factory;
import org.neo4j.kernel.impl.locking.LockAcquisitionTimeoutException;
import org.neo4j.kernel.impl.locking.LockClientStateHolder;
import org.neo4j.kernel.impl.locking.LockClientStoppedException;
import org.neo4j.kernel.impl.locking.Locks.Client;
import org.neo4j.kernel.impl.util.collection.SimpleBitSet;
import org.neo4j.lock.AcquireLockTimeoutException;
import org.neo4j.lock.LockTracer;
import org.neo4j.lock.LockWaitEvent;
import org.neo4j.lock.ResourceType;
import org.neo4j.lock.ResourceTypes;
import org.neo4j.lock.WaitStrategy;

//import org.eclipse.collections.impl.map.mutable.primitive.AbstractMutableIntValuesMap.SentinelValues;

public class ForsetiClient implements Client {

  private final int clientId;
  private final ConcurrentMap<Long, ForsetiLockManager.Lock>[] lockMaps;
  private final WaitStrategy[] waitStrategies;
  private final ForsetiLockManager.DeadlockResolutionStrategy deadlockResolutionStrategy;
  private final Pool<ForsetiClient> clientPool;
  private final IntFunction<ForsetiClient> clientById;
  private final MutableLongIntMap[] sharedLockCounts;
  private final MutableLongIntMap[] exclusiveLockCounts;
  private final long lockAcquisitionTimeoutMillis;
  private final Clock clock;
  private final SimpleBitSet waitList = new SimpleBitSet(64);
  private final LockClientStateHolder stateHolder = new LockClientStateHolder();
  private final ExclusiveLock myExclusiveLock = new ExclusiveLock(this);
  private final ForsetiClient.ReleaseExclusiveLocksAndClearSharedVisitor releaseExclusiveAndClearSharedVisitor =
      new ForsetiClient.ReleaseExclusiveLocksAndClearSharedVisitor();
  private final ForsetiClient.ReleaseSharedDontCheckExclusiveVisitor releaseSharedDontCheckExclusiveVisitor =
      new ForsetiClient.ReleaseSharedDontCheckExclusiveVisitor();
  private long waitListCheckPoint;
  private volatile boolean hasLocks;
  private volatile ForsetiLockManager.Lock waitingForLock;

  public ForsetiClient(int id, ConcurrentMap<Long, ForsetiLockManager.Lock>[] lockMaps,
      WaitStrategy[] waitStrategies, Pool<ForsetiClient> clientPool,
      ForsetiLockManager.DeadlockResolutionStrategy deadlockResolutionStrategy,
      IntFunction<ForsetiClient> clientById,
      long lockAcquisitionTimeoutMillis,
      Clock clock) {
    this.clientId = id;
    this.lockMaps = lockMaps;
    this.waitStrategies = waitStrategies;
    this.deadlockResolutionStrategy = deadlockResolutionStrategy;
    this.clientPool = clientPool;
    this.clientById = clientById;
    this.sharedLockCounts = new MutableLongIntMap[lockMaps.length];
    this.exclusiveLockCounts = new MutableLongIntMap[lockMaps.length];
    this.lockAcquisitionTimeoutMillis = lockAcquisitionTimeoutMillis;
    this.clock = clock;

    for (int i = 0; i < this.sharedLockCounts.length; ++i) {
      this.sharedLockCounts[i] = new ForsetiClient.CountableLongIntHashMap();
      this.exclusiveLockCounts[i] = new ForsetiClient.CountableLongIntHashMap();
    }
  }

  private static void collectActiveLocks(LongIntMap[] counts, List<ActiveLock> locks,
      Factory activeLock) {
    for (int typeId = 0; typeId < counts.length; ++typeId) {
      LongIntMap lockCounts = counts[typeId];
      if (lockCounts != null) {
        ResourceType resourceType = ResourceTypes.fromId(typeId);
        lockCounts.forEachKeyValue((resourceId, count) ->
        {
          locks.add(activeLock.create(resourceType, resourceId));
        });
      }
    }
  }

  public void reset() {
    this.stateHolder.reset();
  }

  public void initialize(LeaseClient leaseClient) {
  }

  public void acquireShared(LockTracer tracer, ResourceType resourceType, long... resourceIds)
      throws AcquireLockTimeoutException {
    this.hasLocks = true;
    this.stateHolder.incrementActiveClients(this);
    LockWaitEvent waitEvent = null;

    try {
      ConcurrentMap<Long, ForsetiLockManager.Lock> lockMap = this.lockMaps[resourceType.typeId()];
      MutableLongIntMap heldShareLocks = this.sharedLockCounts[resourceType.typeId()];
      MutableLongIntMap heldExclusiveLocks = this.exclusiveLockCounts[resourceType.typeId()];
      long[] n8 = resourceIds;
      int n9 = resourceIds.length;

      for (int n10 = 0; n10 < n9; ++n10) {
        long resourceId = n8[n10];
        int heldCount = heldShareLocks.getIfAbsent(resourceId, -1);
        if (heldCount != -1) {
          heldShareLocks.put(resourceId, Math.incrementExact(heldCount));
        } else if (heldExclusiveLocks.containsKey(resourceId)) {
          heldShareLocks.put(resourceId, 1);
        } else {
          int tries = 0;
          SharedLock mySharedLock = null;
          long waitStartMillis = this.clock.millis();

          while (true) {
            this.assertValid(waitStartMillis, resourceType, resourceId);
            ForsetiLockManager.Lock existingLock = lockMap.get(resourceId);
            if (existingLock == null) {
              if (mySharedLock == null) {
                mySharedLock = new SharedLock(this);
              }

              if (lockMap.putIfAbsent(resourceId, mySharedLock) == null) {
                break;
              }
            } else {
              if (existingLock instanceof SharedLock) {
                if (((SharedLock) existingLock).acquire(this)) {
                  break;
                }
              } else if (!(existingLock instanceof ExclusiveLock)) {
                throw new UnsupportedOperationException("Unknown lock type: " + existingLock);
              }

              if (waitEvent == null) {
                waitEvent = tracer.waitForLock(false, resourceType, resourceId);
              }

              this.waitFor(existingLock, resourceType, resourceId, false, tries++);
            }
          }

          heldShareLocks.put(resourceId, 1);
        }
      }
    } finally {
      if (waitEvent != null) {
        waitEvent.close();
      }

      this.clearWaitList();
      this.waitingForLock = null;
      this.stateHolder.decrementActiveClients();
    }
  }

  public void acquireExclusive(LockTracer tracer, ResourceType resourceType, long... resourceIds)
      throws AcquireLockTimeoutException {
    this.hasLocks = true;
    this.stateHolder.incrementActiveClients(this);
    LockWaitEvent waitEvent = null;

    try {
      ConcurrentMap<Long, ForsetiLockManager.Lock> lockMap = this.lockMaps[resourceType.typeId()];
      MutableLongIntMap heldLocks = this.exclusiveLockCounts[resourceType.typeId()];
      long[] n7 = resourceIds;
      int n8 = resourceIds.length;

      for (int n9 = 0; n9 < n8; ++n9) {
        long resourceId = n7[n9];
        int heldCount = heldLocks.getIfAbsent(resourceId, -1);
        if (heldCount != -1) {
          heldLocks.put(resourceId, Math.incrementExact(heldCount));
        } else {
          int tries = 0;

          ForsetiLockManager.Lock existingLock;
          for (long waitStartMillis = this.clock.millis();
              (existingLock = lockMap.putIfAbsent(resourceId, this.myExclusiveLock)) != null;
              this.waitFor(existingLock, resourceType, resourceId, true, tries++)) {
            this.assertValid(waitStartMillis, resourceType, resourceId);
            if (tries > 50 && existingLock instanceof SharedLock) {
              SharedLock sharedLock = (SharedLock) existingLock;
              if (this
                  .tryUpgradeSharedToExclusive(tracer, waitEvent, resourceType, lockMap, resourceId,
                      sharedLock, waitStartMillis)) {
                break;
              }
            }

            if (waitEvent == null) {
              waitEvent = tracer.waitForLock(true, resourceType, resourceId);
            }
          }

          heldLocks.put(resourceId, 1);
        }
      }
    } finally {
      if (waitEvent != null) {
        waitEvent.close();
      }

      this.clearWaitList();
      this.waitingForLock = null;
      this.stateHolder.decrementActiveClients();
    }
  }

  public boolean tryExclusiveLock(ResourceType resourceType, long resourceId) {
    this.hasLocks = true;
    this.stateHolder.incrementActiveClients(this);

    boolean n8;
    try {
      ConcurrentMap<Long, ForsetiLockManager.Lock> lockMap = this.lockMaps[resourceType.typeId()];
      MutableLongIntMap heldLocks = this.exclusiveLockCounts[resourceType.typeId()];
      int heldCount = heldLocks.getIfAbsent(resourceId, -1);
      if (heldCount != -1) {
        heldLocks.put(resourceId, Math.incrementExact(heldCount));
        boolean n13 = true;
        return n13;
      }

      ForsetiLockManager.Lock lock;
      if ((lock = lockMap.putIfAbsent(resourceId, this.myExclusiveLock)) != null) {
        if (lock instanceof SharedLock && this.sharedLockCounts[resourceType.typeId()]
            .containsKey(resourceId)) {
          SharedLock sharedLock = (SharedLock) lock;
          if (sharedLock.tryAcquireUpdateLock(this)) {
            boolean n9;
            if (sharedLock.numberOfHolders() == 1) {
              heldLocks.put(resourceId, 1);
              n9 = true;
              return n9;
            }

            sharedLock.releaseUpdateLock();
            n9 = false;
            return n9;
          }
        }

        n8 = false;
        return n8;
      }

      heldLocks.put(resourceId, 1);
      n8 = true;
    } finally {
      this.stateHolder.decrementActiveClients();
    }

    return n8;
  }

  public boolean trySharedLock(ResourceType resourceType, long resourceId) {
    this.hasLocks = true;
    this.stateHolder.incrementActiveClients(this);

    boolean n8;
    try {
      ConcurrentMap<Long, ForsetiLockManager.Lock> lockMap = this.lockMaps[resourceType.typeId()];
      MutableLongIntMap heldShareLocks = this.sharedLockCounts[resourceType.typeId()];
      MutableLongIntMap heldExclusiveLocks = this.exclusiveLockCounts[resourceType.typeId()];
      int heldCount = heldShareLocks.getIfAbsent(resourceId, -1);
      if (heldCount == -1) {
        if (heldExclusiveLocks.containsKey(resourceId)) {
          heldShareLocks.put(resourceId, 1);
          n8 = true;
          return n8;
        }

        long waitStartMillis = this.clock.millis();

        while (true) {
          this.assertValid(waitStartMillis, resourceType, resourceId);
          ForsetiLockManager.Lock existingLock = lockMap.get(resourceId);
          if (existingLock == null) {
            if (lockMap.putIfAbsent(resourceId, new SharedLock(this)) == null) {
              break;
            }
          } else {
            boolean n11;
            if (!(existingLock instanceof SharedLock)) {
              if (existingLock instanceof ExclusiveLock) {
                n11 = false;
                return n11;
              }

              throw new UnsupportedOperationException("Unknown lock type: " + existingLock);
            }

            if (((SharedLock) existingLock).acquire(this)) {
              break;
            }

            if (((SharedLock) existingLock).isUpdateLock()) {
              n11 = false;
              return n11;
            }
          }
        }

        heldShareLocks.put(resourceId, 1);
        boolean n16 = true;
        return n16;
      }

      heldShareLocks.put(resourceId, Math.incrementExact(heldCount));
      n8 = true;
    } finally {
      this.stateHolder.decrementActiveClients();
    }

    return n8;
  }

  public boolean reEnterShared(ResourceType resourceType, long resourceId) {
    this.stateHolder.incrementActiveClients(this);

    boolean n7;
    try {
      MutableLongIntMap heldShareLocks = this.sharedLockCounts[resourceType.typeId()];
      MutableLongIntMap heldExclusiveLocks = this.exclusiveLockCounts[resourceType.typeId()];
      int heldCount = heldShareLocks.getIfAbsent(resourceId, -1);
      if (heldCount == -1) {
        if (heldExclusiveLocks.containsKey(resourceId)) {
          heldShareLocks.put(resourceId, 1);
          n7 = true;
          return n7;
        }

        n7 = false;
        return n7;
      }

      heldShareLocks.put(resourceId, Math.incrementExact(heldCount));
      n7 = true;
    } finally {
      this.stateHolder.decrementActiveClients();
    }

    return n7;
  }

  public boolean reEnterExclusive(ResourceType resourceType, long resourceId) {
    this.stateHolder.incrementActiveClients(this);

    boolean n6;
    try {
      MutableLongIntMap heldLocks = this.exclusiveLockCounts[resourceType.typeId()];
      int heldCount = heldLocks.getIfAbsent(resourceId, -1);
      if (heldCount == -1) {
        n6 = false;
        return n6;
      }

      heldLocks.put(resourceId, Math.incrementExact(heldCount));
      n6 = true;
    } finally {
      this.stateHolder.decrementActiveClients();
    }

    return n6;
  }

  public void releaseShared(ResourceType resourceType, long... resourceIds) {
    this.stateHolder.incrementActiveClients(this);

    try {
      MutableLongIntMap sharedLocks = this.sharedLockCounts[resourceType.typeId()];
      MutableLongIntMap exclusiveLocks = this.exclusiveLockCounts[resourceType.typeId()];
      ConcurrentMap<Long, ForsetiLockManager.Lock> resourceTypeLocks = this.lockMaps[resourceType
          .typeId()];
      long[] n6 = resourceIds;
      int n7 = resourceIds.length;

      for (int n8 = 0; n8 < n7; ++n8) {
        long resourceId = n6[n8];
        if (!this.releaseLocalLock(resourceType, resourceId, sharedLocks) && !exclusiveLocks
            .containsKey(resourceId)) {
          this.releaseGlobalLock(resourceTypeLocks, resourceId);
        }
      }
    } finally {
      this.stateHolder.decrementActiveClients();
    }
  }

  public void releaseExclusive(ResourceType resourceType, long... resourceIds) {
    this.stateHolder.incrementActiveClients(this);

    try {
      ConcurrentMap<Long, ForsetiLockManager.Lock> resourceTypeLocks = this.lockMaps[resourceType
          .typeId()];
      MutableLongIntMap exclusiveLocks = this.exclusiveLockCounts[resourceType.typeId()];
      MutableLongIntMap sharedLocks = this.sharedLockCounts[resourceType.typeId()];
      long[] n6 = resourceIds;
      int n7 = resourceIds.length;

      for (int n8 = 0; n8 < n7; ++n8) {
        long resourceId = n6[n8];
        if (!this.releaseLocalLock(resourceType, resourceId, exclusiveLocks)) {
          if (sharedLocks.containsKey(resourceId)) {
            ForsetiLockManager.Lock lock = resourceTypeLocks.get(resourceId);
            SharedLock sharedLock;
            if (lock instanceof SharedLock) {
              sharedLock = (SharedLock) lock;
              if (!sharedLock.isUpdateLock()) {
                throw new IllegalStateException(
                    "Incorrect state of exclusive lock. Lock should be updated to exclusive before attempt to release it. Lock: "
                        + this);
              }

              sharedLock.releaseUpdateLock();
            } else {
              sharedLock = new SharedLock(this);
              resourceTypeLocks.put(resourceId, sharedLock);
            }
          } else {
            this.releaseGlobalLock(resourceTypeLocks, resourceId);
          }
        }
      }
    } finally {
      this.stateHolder.decrementActiveClients();
    }
  }

  private void releaseAllClientLocks() {
    for (int i = 0; i < this.exclusiveLockCounts.length; ++i) {
      MutableLongIntMap exclusiveLocks = this.exclusiveLockCounts[i];
      MutableLongIntMap sharedLocks = this.sharedLockCounts[i];
      int size;
      if (exclusiveLocks != null) {
        size = exclusiveLocks.size();
        if (size > 0) {
          exclusiveLocks.forEachKey(
              this.releaseExclusiveAndClearSharedVisitor.initialize(sharedLocks, this.lockMaps[i]));
          if (size <= 32) {
            exclusiveLocks.clear();
          } else {
            this.exclusiveLockCounts[i] = new LongIntHashMap();
          }
        }
      }

      if (sharedLocks != null) {
        size = sharedLocks.size();
        if (size > 0) {
          sharedLocks
              .forEachKey(this.releaseSharedDontCheckExclusiveVisitor.initialize(this.lockMaps[i]));
          if (size <= 32) {
            sharedLocks.clear();
          } else {
            this.sharedLockCounts[i] = new LongIntHashMap();
          }
        }
      }
    }
  }

  public void prepare() {
    this.stateHolder.prepare(this);
  }

  public void stop() {
    if (this.stateHolder.stopClient()) {
      this.waitForAllClientsToLeave();
      this.releaseAllLocks();
    }
  }

  private void waitForAllClientsToLeave() {
    while (this.stateHolder.hasActiveClients()) {
      try {
        Thread.sleep(10L);
      } catch (InterruptedException n2) {
        Thread.interrupted();
      }
    }
  }

  public void close() {
    this.stateHolder.closeClient();
    this.waitForAllClientsToLeave();
    this.releaseAllLocks();
    this.clientPool.release(this);
  }

  private void releaseAllLocks() {
    if (this.hasLocks) {
      this.releaseAllClientLocks();
      this.clearWaitList();
      this.hasLocks = false;
    }
  }

  public int getLockSessionId() {
    return this.clientId;
  }

  public Stream<ActiveLock> activeLocks() {
    List<ActiveLock> locks = new ArrayList();
    collectActiveLocks(this.exclusiveLockCounts, locks, Factory.EXCLUSIVE_LOCK);
    collectActiveLocks(this.sharedLockCounts, locks, Factory.SHARED_LOCK);
    return locks.stream();
  }

  public long activeLockCount() {
    return this.countLocks(this.exclusiveLockCounts) + this.countLocks(this.sharedLockCounts);
  }

  private long countLocks(LongIntMap[] lockCounts) {
    long count = 0L;
    LongIntMap[] n4 = lockCounts;
    int n5 = lockCounts.length;

    for (int n6 = 0; n6 < n5; ++n6) {
      LongIntMap lockCount = n4[n6];
      if (lockCount != null) {
        count += lockCount.size();
      }
    }

    return count;
  }

  int waitListSize() {
    return this.waitList.size();
  }

  void copyWaitListTo(SimpleBitSet other) {
    other.put(this.waitList);
  }

  boolean isWaitingFor(int clientId) {
    return clientId != this.clientId && this.waitList.contains(clientId);
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ForsetiClient that = (ForsetiClient) o;
      return this.clientId == that.clientId;
    } else {
      return false;
    }
  }

  public int hashCode() {
    return this.clientId;
  }

  public String toString() {
    return String.format("ForsetiClient[%d]", this.clientId);
  }

  private void releaseGlobalLock(ConcurrentMap<Long, ForsetiLockManager.Lock> lockMap,
      long resourceId) {
    ForsetiLockManager.Lock lock = lockMap.get(resourceId);
    if (lock instanceof ExclusiveLock) {
      lockMap.remove(resourceId);
    } else if (lock instanceof SharedLock && ((SharedLock) lock).release(this)) {
      ((SharedLock) lock).cleanUpdateHolder();
      lockMap.remove(resourceId);
    }
  }

  private boolean releaseLocalLock(ResourceType type, long resourceId,
      MutableLongIntMap localLocks) {
    int lockCount = localLocks.removeKeyIfAbsent(resourceId, -1);
    if (lockCount == -1) {
      throw new IllegalStateException(
          this + " cannot release lock that it does not hold: " + type + "[" + resourceId + "].");
    } else if (lockCount > 1) {
      localLocks.put(resourceId, lockCount - 1);
      return true;
    } else {
      return false;
    }
  }

  private boolean tryUpgradeSharedToExclusive(LockTracer tracer, LockWaitEvent waitEvent,
      ResourceType resourceType,
      ConcurrentMap<Long, ForsetiLockManager.Lock> lockMap, long resourceId, SharedLock sharedLock,
      long waitStartMillis)
      throws AcquireLockTimeoutException {
    int tries = 0;
    boolean holdsSharedLock = this.sharedLockCounts[resourceType.typeId()].containsKey(resourceId);
    if (!holdsSharedLock) {
      if (!sharedLock.acquire(this)) {
        return false;
      } else {
        try {
          if (this
              .tryUpgradeToExclusiveWithShareLockHeld(tracer, waitEvent, resourceType, resourceId,
                  sharedLock, tries, waitStartMillis)) {
            return true;
          } else {
            this.releaseGlobalLock(lockMap, resourceId);
            return false;
          }
        } catch (Throwable n13) {
          this.releaseGlobalLock(lockMap, resourceId);
          throw n13;
        }
      }
    } else {
      return this
          .tryUpgradeToExclusiveWithShareLockHeld(tracer, waitEvent, resourceType, resourceId,
              sharedLock, tries, waitStartMillis);
    }
  }

  private boolean tryUpgradeToExclusiveWithShareLockHeld(LockTracer tracer,
      LockWaitEvent priorEvent, ResourceType resourceType, long resourceId,
      SharedLock sharedLock, int tries, long waitStartMillis) throws AcquireLockTimeoutException {
    if (!sharedLock.tryAcquireUpdateLock(this)) {
      return false;
    } else {
      LockWaitEvent waitEvent = null;

      boolean n11;
      try {
        for (; sharedLock.numberOfHolders() > 1;
            this.waitFor(sharedLock, resourceType, resourceId, true, tries++)) {
          this.assertValid(waitStartMillis, resourceType, resourceId);
          if (waitEvent == null && priorEvent == null) {
            waitEvent = tracer.waitForLock(true, resourceType, resourceId);
          }
        }

        n11 = true;
      } catch (Throwable n15) {
        sharedLock.releaseUpdateLock();
        if (!(n15 instanceof DeadlockDetectedException)
            && !(n15 instanceof LockClientStoppedException)) {
          throw new TransactionFailureException(
              "Failed to upgrade shared lock to exclusive: " + sharedLock, n15);
        }

        throw (RuntimeException) n15;
      } finally {
        if (waitEvent != null) {
          waitEvent.close();
        }

        this.clearWaitList();
        this.waitingForLock = null;
      }

      return n11;
    }
  }

  private void clearWaitList() {
    this.waitListCheckPoint = this.waitList
        .checkPointAndPut(this.waitListCheckPoint, this.clientId);
  }

  private void waitFor(ForsetiLockManager.Lock lock, ResourceType type, long resourceId,
      boolean exclusive, int tries) {
    this.waitingForLock = lock;
    this.clearAndCopyWaitList(lock);
    this.waitStrategies[type.typeId()].apply(tries);
    int b = lock.detectDeadlock(this.id());
    if (b != -1 && this.deadlockResolutionStrategy.shouldAbort(this, this.clientById.apply(b))) {
      UnsafeUtil.loadFence();
      String message = this + " can't acquire " + lock + " on " + type + "(" + resourceId
          + "), because holders of that lock are waiting for " + this +
          ".\n Wait list:" + lock.describeWaitList();
      if (lock.detectDeadlock(this.id()) != -1 && this.isDeadlockReal(lock, tries)) {
        throw new DeadlockDetectedException(message);
      }
    }
  }

  private void clearAndCopyWaitList(ForsetiLockManager.Lock lock) {
    this.clearWaitList();
    lock.copyHolderWaitListsInto(this.waitList);
  }

  private boolean isDeadlockReal(ForsetiLockManager.Lock lock, int tries) {
    Set<ForsetiLockManager.Lock> waitedUpon = new HashSet();
    Set<ForsetiClient> owners = new HashSet();
    Set<ForsetiLockManager.Lock> nextWaitedUpon = new HashSet();
    Set<ForsetiClient> nextOwners = new HashSet();
    lock.collectOwners(owners);

    do {
      waitedUpon.addAll(nextWaitedUpon);
      this.collectNextOwners(waitedUpon, owners, nextWaitedUpon, nextOwners);
      if (nextOwners.contains(this) && tries > 20) {
        nextOwners.clear();
        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(10L));
        this.collectNextOwners(waitedUpon, owners, nextWaitedUpon, nextOwners);
        if (nextOwners.contains(this)) {
          return true;
        }
      }

      owners.clear();
      Set<ForsetiClient> ownersTmp = owners;
      owners = nextOwners;
      nextOwners = ownersTmp;
    }
    while (!nextWaitedUpon.isEmpty());

    return false;
  }

  private void collectNextOwners(Set<ForsetiLockManager.Lock> waitedUpon, Set<ForsetiClient> owners,
      Set<ForsetiLockManager.Lock> nextWaitedUpon,
      Set<ForsetiClient> nextOwners) {
    nextWaitedUpon.clear();
    Iterator n5 = owners.iterator();

    while (n5.hasNext()) {
      ForsetiClient owner = (ForsetiClient) n5.next();
      ForsetiLockManager.Lock waitingForLock = owner.waitingForLock;
      if (waitingForLock != null && !waitedUpon.contains(waitingForLock)) {
        nextWaitedUpon.add(waitingForLock);
      }
    }

    n5 = nextWaitedUpon.iterator();

    while (n5.hasNext()) {
      ForsetiLockManager.Lock lck = (ForsetiLockManager.Lock) n5.next();
      lck.collectOwners(nextOwners);
    }
  }

  String describeWaitList() {
    StringBuilder sb = new StringBuilder(String.format("%nClient[%d] waits for [", this.id()));
    IntIterator iter = this.waitList.iterator();
    boolean first = true;

    while (iter.hasNext()) {
      int next = iter.next();
      if (next != this.clientId) {
        sb.append(!first ? "," : "").append(next);
        first = false;
      }
    }

    sb.append("]");
    return sb.toString();
  }

  public int id() {
    return this.clientId;
  }

  private void assertValid(long waitStartMillis, ResourceType resourceType, long resourceId) {
    this.assertNotStopped();
    this.assertNotExpired(waitStartMillis, resourceType, resourceId);
  }

  private void assertNotStopped() {
    if (this.stateHolder.isStopped()) {
      throw new LockClientStoppedException(this);
    }
  }

  private void assertNotExpired(long waitStartMillis, ResourceType resourceType, long resourceId) {
    if (this.lockAcquisitionTimeoutMillis > 0L
        && this.lockAcquisitionTimeoutMillis + waitStartMillis < this.clock.millis()) {
      throw new LockAcquisitionTimeoutException(resourceType, resourceId,
          this.lockAcquisitionTimeoutMillis);
    }
  }

  private static class CountableLongIntHashMap extends LongIntHashMap {

    CountableLongIntHashMap() {
    }

    public int size() {
      SentinelValues sentinelValues = this.getSentinelValues();
      return this.getOccupiedWithData() + (sentinelValues == null ? 0 : sentinelValues.size());
    }
  }

  private class ReleaseExclusiveLocksAndClearSharedVisitor implements LongProcedure {

    private MutableLongIntMap sharedLockCounts;
    private ConcurrentMap<Long, ForsetiLockManager.Lock> lockMap;

    private LongProcedure initialize(MutableLongIntMap sharedLockCounts,
        ConcurrentMap<Long, ForsetiLockManager.Lock> lockMap) {
      this.sharedLockCounts = sharedLockCounts;
      this.lockMap = lockMap;
      return this;
    }

    public void value(long resourceId) {
      ForsetiClient.this.releaseGlobalLock(this.lockMap, resourceId);
      if (this.sharedLockCounts != null) {
        this.sharedLockCounts.remove(resourceId);
      }
    }
  }

  private class ReleaseSharedDontCheckExclusiveVisitor implements LongProcedure {

    private ConcurrentMap<Long, ForsetiLockManager.Lock> lockMap;

    private LongProcedure initialize(ConcurrentMap<Long, ForsetiLockManager.Lock> lockMap) {
      this.lockMap = lockMap;
      return this;
    }

    public void value(long resourceId) {
      ForsetiClient.this.releaseGlobalLock(this.lockMap, resourceId);
    }
  }
}
