/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.net;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.neo4j.kernel.api.net.NetworkConnectionIdGenerator;
import org.neo4j.kernel.api.net.NetworkConnectionTracker;
import org.neo4j.kernel.api.net.TrackedNetworkConnection;

public class DefaultNetworkConnectionTracker implements NetworkConnectionTracker {

  private final NetworkConnectionIdGenerator idGenerator = new NetworkConnectionIdGenerator();
  private final Map<String, TrackedNetworkConnection> connectionsById = new ConcurrentHashMap();

  public String newConnectionId(String connector) {
    return this.idGenerator.newConnectionId(connector);
  }

  public void add(TrackedNetworkConnection connection) {
    TrackedNetworkConnection previousConnection = this.connectionsById
        .put(connection.id(), connection);
    if (previousConnection != null) {
      throw new IllegalArgumentException(
          "Attempt to register a connection with an existing id " + connection.id()
              + ". Existing connection: " + previousConnection +
              ", new connection: " + connection);
    }
  }

  public void remove(TrackedNetworkConnection connection) {
    this.connectionsById.remove(connection.id());
  }

  public TrackedNetworkConnection get(String id) {
    return this.connectionsById.get(id);
  }

  public List<TrackedNetworkConnection> activeConnections() {
    return new ArrayList(this.connectionsById.values());
  }
}
