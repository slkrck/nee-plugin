/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.pagecache;

import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.io.pagecache.PagedFile;

class ParallelPageLoader implements PageLoader {

  private final PagedFile file;
  private final Executor executor;
  private final PageCache pageCache;
  private final AtomicLong received;
  private final AtomicLong processed;

  ParallelPageLoader(PagedFile file, Executor executor, PageCache pageCache) {
    this.file = file;
    this.executor = executor;
    this.pageCache = pageCache;
    this.received = new AtomicLong();
    this.processed = new AtomicLong();
  }

  public void load(long pageId) {
    this.received.getAndIncrement();
    this.executor.execute(() ->
    {
      try {
        PageCursor cursor = this.file.io(pageId, 1);

        try {
          cursor.next();
        } catch (Throwable n12) {
          if (cursor != null) {
            try {
              cursor.close();
            } catch (Throwable n11) {
              n12.addSuppressed(n11);
            }
          }

          throw n12;
        }

        if (cursor != null) {
          cursor.close();
        }
      } catch (IOException n13) {
      } finally {
        this.pageCache.reportEvents();
        this.processed.getAndIncrement();
      }
    });
  }

  public void close() {
    while (this.processed.get() < this.received.get()) {
      Thread.yield();
    }
  }
}
