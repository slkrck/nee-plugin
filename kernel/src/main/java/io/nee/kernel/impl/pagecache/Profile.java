/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.pagecache;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.neo4j.dbms.archive.CompressionFormat;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.FileUtils;
import org.neo4j.io.pagecache.PagedFile;

final class Profile implements Comparable<Profile> {

  static final String PROFILE_DIR = "profiles";
  private final File baseDir;
  private final File profileFile;
  private final File pagedFile;
  private final long profileSequenceId;

  private Profile(File baseDir, File profileFile, File pagedFile, long profileSequenceId) {
    Objects.requireNonNull(profileFile);
    Objects.requireNonNull(pagedFile);
    Objects.requireNonNull(baseDir);
    this.baseDir = baseDir;
    this.profileFile = profileFile;
    this.pagedFile = pagedFile;
    this.profileSequenceId = profileSequenceId;
  }

  static Profile first(File databaseDirectory, File file) {
    long profileSequenceId = 0L;
    return new Profile(databaseDirectory, profileName(databaseDirectory, file, profileSequenceId),
        file, profileSequenceId);
  }

  private static File profileName(File baseDirectory, File mappedFile, long count) {
    File profileDirectory = new File(baseDirectory, "profiles");
    File profileFileDir = FileUtils.pathToFileAfterMove(baseDirectory, profileDirectory, mappedFile)
        .getParentFile();
    String name = mappedFile.getName();
    return new File(profileFileDir, name + "." + count + ".cacheprof");
  }

  static Predicate<Profile> relevantTo(PagedFile pagedFile) {
    return (p) ->
    {
      return p.pagedFile.equals(pagedFile.file());
    };
  }

  static Stream<Profile> parseProfileName(Path basePath, Path profilePath, Path mappedFilePath) {
    String name = profilePath.getFileName().toString();
    if (!name.endsWith(".cacheprof")) {
      return Stream.empty();
    } else {
      int lastDot = name.lastIndexOf(46);
      int secondLastDot = name.lastIndexOf(46, lastDot - 1);
      String countStr = name.substring(secondLastDot + 1, lastDot);

      try {
        long sequenceId = Long.parseLong(countStr, 10);
        String targetMappedFileName = name.substring(0, secondLastDot);
        return targetMappedFileName.equals(mappedFilePath.getFileName().toString()) ? Stream.of(
            new Profile(basePath.toFile(), profilePath.toFile(), mappedFilePath.toFile(),
                sequenceId)) : Stream.empty();
      } catch (NumberFormatException n10) {
        return Stream.empty();
      }
    }
  }

  public int compareTo(Profile that) {
    int compare = this.pagedFile.compareTo(that.pagedFile);
    return compare == 0 ? Long.compare(this.profileSequenceId, that.profileSequenceId) : compare;
  }

  public boolean equals(Object o) {
    if (o instanceof Profile) {
      Profile profile = (Profile) o;
      return this.profileFile.equals(profile.profileFile);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return this.profileFile.hashCode();
  }

  public String toString() {
    return "Profile(" + this.profileSequenceId + " for " + this.pagedFile + ")";
  }

  File file() {
    return this.profileFile;
  }

  void delete(FileSystemAbstraction fs) {
    fs.deleteFile(this.profileFile);
  }

  InputStream read(FileSystemAbstraction fs) throws IOException {
    try {
      return CompressionFormat.decompress(() ->
      {
        return fs.openAsInputStream(this.profileFile);
      });
    } catch (IOException n3) {
      throw new IOException("Exception when building decompressor.", n3);
    }
  }

  OutputStream write(FileSystemAbstraction fs) throws IOException {
    fs.mkdirs(this.profileFile.getParentFile());

    try {
      return CompressionFormat.compress(() ->
      {
        return fs.openAsOutputStream(this.profileFile, false);
      }, CompressionFormat.GZIP);
    } catch (IOException n3) {
      throw new IOException("Exception when building compressor.", n3);
    }
  }

  Profile next() {
    long next = this.profileSequenceId + 1L;
    return new Profile(this.baseDir, profileName(this.baseDir, this.pagedFile, next),
        this.pagedFile, next);
  }
}
