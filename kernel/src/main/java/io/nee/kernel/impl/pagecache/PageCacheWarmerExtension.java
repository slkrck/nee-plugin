/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.pagecache;

import io.nee.kernel.impl.pagecache.monitor.PageCacheWarmerMonitor;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.availability.DatabaseAvailabilityGuard;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.impl.transaction.state.DatabaseFileListing;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.scheduler.JobScheduler;

class PageCacheWarmerExtension extends LifecycleAdapter {

  private final DatabaseAvailabilityGuard databaseAvailabilityGuard;
  private final Database database;
  private final Config config;
  private final PageCacheWarmer pageCacheWarmer;
  private final WarmupAvailabilityListener availabilityListener;
  private volatile boolean started;

  PageCacheWarmerExtension(JobScheduler scheduler,
      DatabaseAvailabilityGuard databaseAvailabilityGuard, PageCache pageCache,
      FileSystemAbstraction fs,
      Database database, Log log, PageCacheWarmerMonitor monitor, Config config) {
    this.databaseAvailabilityGuard = databaseAvailabilityGuard;
    this.database = database;
    this.config = config;
    this.pageCacheWarmer = new PageCacheWarmer(fs, pageCache, scheduler,
        database.getDatabaseLayout().databaseDirectory(), config, log);
    this.availabilityListener = new WarmupAvailabilityListener(scheduler, this.pageCacheWarmer,
        config, log, monitor, database.getNamedDatabaseId());
  }

  public void start() {
    if (this.config.get(GraphDatabaseSettings.pagecache_warmup_enabled)) {
      this.pageCacheWarmer.start();
      this.databaseAvailabilityGuard.addListener(this.availabilityListener);
      this.getNeoStoreFileListing().registerStoreFileProvider(this.pageCacheWarmer);
      this.started = true;
    }
  }

  public void stop() throws Exception {
    if (this.started) {
      this.databaseAvailabilityGuard.removeListener(this.availabilityListener);
      this.availabilityListener.unavailable();
      this.pageCacheWarmer.stop();
      this.started = false;
    }
  }

  private DatabaseFileListing getNeoStoreFileListing() {
    return this.database.getDependencyResolver().resolveDependency(DatabaseFileListing.class);
  }
}
