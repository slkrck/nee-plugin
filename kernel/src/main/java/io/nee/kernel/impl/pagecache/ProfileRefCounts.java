/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.pagecache;

import java.util.HashMap;
import java.util.Map;

class ProfileRefCounts {

  private final Map<Profile, ProfileRefCounts.Counter> bag = new HashMap();

  synchronized void incrementRefCounts(Profile[] profiles) {
    Profile[] n2 = profiles;
    int n3 = profiles.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      Profile profile = n2[n4];
      this.bag.computeIfAbsent(profile, (p) ->
      {
        return new Counter();
      }).increment();
    }
  }

  synchronized void decrementRefCounts(Profile[] profiles) {
    Profile[] n2 = profiles;
    int n3 = profiles.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      Profile profile = n2[n4];
      this.bag.computeIfPresent(profile, (p, i) ->
      {
        return i.decrementAndGet() == 0 ? null : i;
      });
    }
  }

  synchronized boolean contains(Profile profile) {
    return this.bag.containsKey(profile);
  }

  private static class Counter {

    private int count;

    void increment() {
      ++this.count;
    }

    int decrementAndGet() {
      return --this.count;
    }
  }
}
