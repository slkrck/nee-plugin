/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.enterprise.configuration;

import org.neo4j.configuration.Description;
import org.neo4j.configuration.SettingImpl;
import org.neo4j.configuration.SettingValueParsers;
import org.neo4j.configuration.SettingsDeclaration;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.graphdb.config.Setting;

public class OnlineBackupSettings implements SettingsDeclaration {

  public static final String DEFAULT_BACKUP_HOST = "localhost";
  public static final int DEFAULT_BACKUP_PORT = 6362;
  @Description("Enable support for running online backups.")
  public static final Setting<Boolean> online_backup_enabled;
  @Description("Network interface and port for the backup server to listen on.")
  public static final Setting<SocketAddress> online_backup_listen_address;

  static {
    online_backup_enabled = SettingImpl
        .newBuilder("dbms.backup.enabled", SettingValueParsers.BOOL, true).build();
    online_backup_listen_address =
        SettingImpl.newBuilder("dbms.backup.listen_address", SettingValueParsers.SOCKET_ADDRESS,
            new SocketAddress("127.0.0.1", 6362)).build();
  }
}
