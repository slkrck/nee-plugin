/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.pagecache;

import io.nee.kernel.impl.pagecache.monitor.PageCacheWarmerMonitor;
import java.util.concurrent.TimeUnit;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.kernel.availability.AvailabilityListener;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobHandle;
import org.neo4j.scheduler.JobScheduler;

class WarmupAvailabilityListener implements AvailabilityListener {

  private final JobScheduler scheduler;
  private final PageCacheWarmer pageCacheWarmer;
  private final Config config;
  private final Log log;
  private final PageCacheWarmerMonitor monitor;
  private final NamedDatabaseId namedDatabaseId;
  private volatile boolean available;
  private JobHandle jobHandle;

  WarmupAvailabilityListener(JobScheduler scheduler, PageCacheWarmer pageCacheWarmer, Config config,
      Log log, PageCacheWarmerMonitor monitor,
      NamedDatabaseId namedDatabaseId) {
    this.scheduler = scheduler;
    this.pageCacheWarmer = pageCacheWarmer;
    this.config = config;
    this.log = log;
    this.monitor = monitor;
    this.namedDatabaseId = namedDatabaseId;
  }

  public synchronized void available() {
    this.available = true;
    this.jobHandle = this.scheduler.schedule(Group.FILE_IO_HELPER, this::startWarmup);
  }

  private void startWarmup() {
    if (this.available) {
      try {
        this.monitor.warmupStarted(this.namedDatabaseId);
        this.pageCacheWarmer.reheat().ifPresent((loadedPages) ->
        {
          this.monitor.warmupCompleted(this.namedDatabaseId, loadedPages);
        });
      } catch (Exception n2) {
        this.log.debug(
            "Active page cache warmup failed, so it may take longer for the cache to be populated with hot data.",
            n2);
      }

      this.scheduleProfile();
    }
  }

  private synchronized void scheduleProfile() {
    if (this.available) {
      long frequencyMillis = this.config
          .get(GraphDatabaseSettings.pagecache_warmup_profiling_interval).toMillis();
      this.jobHandle = this.scheduler
          .scheduleRecurring(Group.FILE_IO_HELPER, this::doProfile, frequencyMillis,
              TimeUnit.MILLISECONDS);
    }
  }

  private void doProfile() {
    try {
      this.pageCacheWarmer.profile().ifPresent((pages) ->
      {
        this.monitor.profileCompleted(this.namedDatabaseId, pages);
      });
    } catch (Exception n2) {
      this.log.debug(
          "Page cache profiling failed, so no new profile of what data is hot or not was produced. This may reduce the effectiveness of a future page cache warmup process.",
          n2);
    }
  }

  public synchronized void unavailable() {
    this.available = false;
    if (this.jobHandle != null) {
      this.jobHandle.cancel();
      this.jobHandle = null;
    }
  }
}
