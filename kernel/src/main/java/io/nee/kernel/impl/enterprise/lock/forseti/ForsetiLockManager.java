/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.enterprise.lock.forseti;

import java.time.Clock;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import org.neo4j.collection.pool.LinkedQueuePool;
import org.neo4j.collection.pool.Pool;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.kernel.impl.locking.Locks;
import org.neo4j.kernel.impl.util.collection.SimpleBitSet;
import org.neo4j.lock.ResourceType;
import org.neo4j.lock.WaitStrategy;

public class ForsetiLockManager implements Locks {

  private final ConcurrentMap<Long, ForsetiLockManager.Lock>[] lockMaps;
  private final ResourceType[] resourceTypes;
  private final Pool<ForsetiClient> clientPool;
  private volatile boolean closed;

  public ForsetiLockManager(Config config, Clock clock, ResourceType... resourceTypes) {
    int maxResourceId = this.findMaxResourceId(resourceTypes);
    this.lockMaps = new ConcurrentMap[maxResourceId];
    this.resourceTypes = new ResourceType[maxResourceId];
    WaitStrategy[] waitStrategies = new WaitStrategy[maxResourceId];
    ResourceType[] n6 = resourceTypes;
    int n7 = resourceTypes.length;

    for (int n8 = 0; n8 < n7; ++n8) {
      ResourceType type = n6[n8];
      this.lockMaps[type.typeId()] = new ConcurrentHashMap(16, 0.6F, 512);
      waitStrategies[type.typeId()] = type.waitStrategy();
      this.resourceTypes[type.typeId()] = type;
    }

    this.clientPool = new ForsetiLockManager.ForsetiClientFlyweightPool(config, clock,
        this.lockMaps, waitStrategies);
  }

  public Client newClient() {
    if (this.closed) {
      throw new IllegalStateException(this + " already closed");
    } else {
      ForsetiClient forsetiClient = this.clientPool.acquire();
      forsetiClient.reset();
      return forsetiClient;
    }
  }

  public void accept(Visitor out) {
    for (int i = 0; i < this.lockMaps.length; ++i) {
      if (this.lockMaps[i] != null) {
        ResourceType type = this.resourceTypes[i];
        Iterator n4 = this.lockMaps[i].entrySet().iterator();

        while (n4.hasNext()) {
          Entry<Long, ForsetiLockManager.Lock> entry = (Entry) n4.next();
          ForsetiLockManager.Lock lock = entry.getValue();
          out.visit(type, entry.getKey(), lock.describeWaitList(), 0L,
              System.identityHashCode(lock));
        }
      }
    }
  }

  private int findMaxResourceId(ResourceType[] resourceTypes) {
    int max = 0;
    ResourceType[] n3 = resourceTypes;
    int n4 = resourceTypes.length;

    for (int n5 = 0; n5 < n4; ++n5) {
      ResourceType resourceType = n3[n5];
      max = Math.max(resourceType.typeId(), max);
    }

    return max + 1;
  }

  public void close() {
    this.closed = true;
  }

  interface DeadlockResolutionStrategy {

    boolean shouldAbort(ForsetiClient n1, ForsetiClient n2);
  }

  interface Lock {

    void copyHolderWaitListsInto(SimpleBitSet n1);

    int detectDeadlock(int n1);

    String describeWaitList();

    void collectOwners(Set<ForsetiClient> n1);
  }

  private static class ForsetiClientFlyweightPool extends LinkedQueuePool<ForsetiClient> {

    private final AtomicInteger clientIds = new AtomicInteger(0);
    private final Queue<Integer> unusedIds = new ConcurrentLinkedQueue();
    private final ConcurrentMap<Integer, ForsetiClient> clientsById = new ConcurrentHashMap();
    private final Config config;
    private final Clock clock;
    private final ConcurrentMap<Long, ForsetiLockManager.Lock>[] lockMaps;
    private final WaitStrategy[] waitStrategies;
    private final ForsetiLockManager.DeadlockResolutionStrategy deadlockResolutionStrategy;

    ForsetiClientFlyweightPool(Config config, Clock clock,
        ConcurrentMap<Long, ForsetiLockManager.Lock>[] lockMaps, WaitStrategy[] waitStrategies) {
      super(128, null);
      this.deadlockResolutionStrategy = DeadlockStrategies.DEFAULT;
      this.config = config;
      this.clock = clock;
      this.lockMaps = lockMaps;
      this.waitStrategies = waitStrategies;
    }

    protected ForsetiClient create() {
      Integer id = this.unusedIds.poll();
      if (id == null) {
        id = this.clientIds.getAndIncrement();
      }

      long lockAcquisitionTimeoutMillis = this.config
          .get(GraphDatabaseSettings.lock_acquisition_timeout).toMillis();
      int n10002 = id;
      ConcurrentMap[] n10003 = this.lockMaps;
      WaitStrategy[] n10004 = this.waitStrategies;
      ForsetiLockManager.DeadlockResolutionStrategy n10006 = this.deadlockResolutionStrategy;
      ConcurrentMap<Integer, ForsetiClient> clientsById = this.clientsById;
      Objects.requireNonNull(clientsById);
      ForsetiClient client =
          new ForsetiClient(n10002, n10003, n10004, this, n10006, clientsById::get,
              lockAcquisitionTimeoutMillis, this.clock);
      this.clientsById.put(id, client);
      return client;
    }

    protected void dispose(ForsetiClient resource) {
      super.dispose(resource);
      this.clientsById.remove(resource.id());
      if (resource.id() < 1024) {
        this.unusedIds.offer(resource.id());
      }
    }
  }
}
