/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.enterprise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import org.eclipse.collections.api.IntIterable;
import org.eclipse.collections.api.iterator.MutableLongIterator;
import org.eclipse.collections.api.map.primitive.MutableLongObjectMap;
import org.eclipse.collections.api.set.primitive.IntSet;
import org.eclipse.collections.api.set.primitive.LongSet;
import org.eclipse.collections.api.set.primitive.MutableIntSet;
import org.eclipse.collections.impl.map.mutable.primitive.LongObjectHashMap;
import org.eclipse.collections.impl.set.mutable.primitive.IntHashSet;
import org.neo4j.collection.PrimitiveArrays;
import org.neo4j.internal.kernel.api.CursorFactory;
import org.neo4j.internal.kernel.api.LabelSet;
import org.neo4j.internal.kernel.api.NodeCursor;
import org.neo4j.internal.kernel.api.PropertyCursor;
import org.neo4j.internal.kernel.api.Read;
import org.neo4j.internal.kernel.api.RelationshipScanCursor;
import org.neo4j.internal.kernel.api.exceptions.schema.ConstraintValidationException;
import org.neo4j.internal.kernel.api.exceptions.schema.ConstraintValidationException.Phase;
import org.neo4j.internal.schema.ConstraintDescriptor;
import org.neo4j.internal.schema.LabelSchemaDescriptor;
import org.neo4j.internal.schema.RelationTypeSchemaDescriptor;
import org.neo4j.internal.schema.SchemaDescriptor;
import org.neo4j.internal.schema.SchemaProcessor;
import org.neo4j.io.IOUtils;
import org.neo4j.kernel.api.exceptions.schema.NodePropertyExistenceException;
import org.neo4j.kernel.api.exceptions.schema.RelationshipPropertyExistenceException;
import org.neo4j.storageengine.api.StorageProperty;
import org.neo4j.storageengine.api.StorageReader;
import org.neo4j.storageengine.api.txstate.TxStateVisitor;
import org.neo4j.storageengine.api.txstate.TxStateVisitor.Delegator;

class PropertyExistenceEnforcer {

  private static final PropertyExistenceEnforcer NO_CONSTRAINTS = new PropertyExistenceEnforcer(
      Collections.emptyList(), Collections.emptyList()) {
    TxStateVisitor decorate(TxStateVisitor visitor, Read read, CursorFactory cursorFactory) {
      return visitor;
    }
  };
  private static final Function<StorageReader, PropertyExistenceEnforcer> FACTORY = (storageReader) ->
  {
    final List<LabelSchemaDescriptor> nodes = new ArrayList();
    final List<RelationTypeSchemaDescriptor> relationships = new ArrayList();
    Iterator constraints = storageReader.constraintsGetAll();

    while (constraints.hasNext()) {
      ConstraintDescriptor constraint = (ConstraintDescriptor) constraints.next();
      if (constraint.enforcesPropertyExistence()) {
        constraint.schema().processWith(new SchemaProcessor() {
          public void processSpecific(LabelSchemaDescriptor schema) {
            nodes.add(schema);
          }

          public void processSpecific(RelationTypeSchemaDescriptor schema) {
            relationships.add(schema);
          }

          public void processSpecific(SchemaDescriptor schema) {
            throw new UnsupportedOperationException(
                "General SchemaDescriptor cannot support constraints");
          }
        });
      }
    }

    if (nodes.isEmpty() && relationships.isEmpty()) {
      return NO_CONSTRAINTS;
    } else {
      return new PropertyExistenceEnforcer(nodes, relationships);
    }
  };
  private final List<LabelSchemaDescriptor> nodeConstraints;
  private final List<RelationTypeSchemaDescriptor> relationshipConstraints;
  private final MutableLongObjectMap<int[]> mandatoryNodePropertiesByLabel = new LongObjectHashMap();
  private final MutableLongObjectMap<int[]> mandatoryRelationshipPropertiesByType = new LongObjectHashMap();

  private PropertyExistenceEnforcer(List<LabelSchemaDescriptor> nodes,
      List<RelationTypeSchemaDescriptor> rels) {
    this.nodeConstraints = nodes;
    this.relationshipConstraints = rels;
    Iterator n3 = nodes.iterator();

    while (n3.hasNext()) {
      LabelSchemaDescriptor constraint = (LabelSchemaDescriptor) n3.next();
      update(this.mandatoryNodePropertiesByLabel, constraint.getLabelId(),
          copyAndSortPropertyIds(constraint.getPropertyIds()));
    }

    n3 = rels.iterator();

    while (n3.hasNext()) {
      RelationTypeSchemaDescriptor constraint = (RelationTypeSchemaDescriptor) n3.next();
      update(this.mandatoryRelationshipPropertiesByType, constraint.getRelTypeId(),
          copyAndSortPropertyIds(constraint.getPropertyIds()));
    }
  }

  static PropertyExistenceEnforcer getOrCreatePropertyExistenceEnforcerFrom(
      StorageReader storageReader) {
    return storageReader.getOrCreateSchemaDependantState(PropertyExistenceEnforcer.class, FACTORY);
  }

  private static void update(MutableLongObjectMap<int[]> map, int key, int[] sortedValues) {
    int[] current = map.get(key);
    if (current != null) {
      sortedValues = PrimitiveArrays.union(current, sortedValues);
    }

    map.put(key, sortedValues);
  }

  private static int[] copyAndSortPropertyIds(int[] propertyIds) {
    int[] values = new int[propertyIds.length];
    System.arraycopy(propertyIds, 0, values, 0, propertyIds.length);
    Arrays.sort(values);
    return values;
  }

  TxStateVisitor decorate(TxStateVisitor visitor, Read read, CursorFactory cursorFactory) {
    return new PropertyExistenceEnforcer.Decorator(visitor, read, cursorFactory);
  }

  private void validateNodeProperties(long id, LabelSet labelIds, IntSet propertyKeyIds)
      throws NodePropertyExistenceException {
    int numberOfLabels = labelIds.numberOfLabels();
    long label;
    if (numberOfLabels > this.mandatoryNodePropertiesByLabel.size()) {
      MutableLongIterator labels = this.mandatoryNodePropertiesByLabel.keySet().longIterator();

      while (labels.hasNext()) {
        label = labels.next();
        if (labelIds.contains(Math.toIntExact(label))) {
          this.validateNodeProperties(id, label, this.mandatoryNodePropertiesByLabel.get(label),
              propertyKeyIds);
        }
      }
    } else {
      for (int i = 0; i < numberOfLabels; ++i) {
        label = labelIds.label(i);
        int[] keys = this.mandatoryNodePropertiesByLabel.get(label);
        if (keys != null) {
          this.validateNodeProperties(id, label, keys, propertyKeyIds);
        }
      }
    }
  }

  private void validateNodeProperties(long id, long label, int[] requiredKeys,
      IntSet propertyKeyIds) throws NodePropertyExistenceException {
    int[] n7 = requiredKeys;
    int n8 = requiredKeys.length;

    for (int n9 = 0; n9 < n8; ++n9) {
      int key = n7[n9];
      if (!propertyKeyIds.contains(key)) {
        this.failNode(id, label, key);
      }
    }
  }

  private void failNode(long id, long label, int propertyKey)
      throws NodePropertyExistenceException {
    Iterator n6 = this.nodeConstraints.iterator();

    LabelSchemaDescriptor constraint;
    do {
      if (!n6.hasNext()) {
        throw new IllegalStateException(String
            .format("Node constraint for label=%d, propertyKey=%d should exist.", label,
                propertyKey));
      }

      constraint = (LabelSchemaDescriptor) n6.next();
    }
    while ((long) constraint.getLabelId() != label || !this
        .contains(constraint.getPropertyIds(), propertyKey));

    throw new NodePropertyExistenceException(constraint, Phase.VALIDATION, id);
  }

  private void failRelationship(long id, int relationshipType, int propertyKey)
      throws RelationshipPropertyExistenceException {
    Iterator n5 = this.relationshipConstraints.iterator();

    RelationTypeSchemaDescriptor constraint;
    do {
      if (!n5.hasNext()) {
        throw new IllegalStateException(
            String.format(
                "Relationship constraint for relationshipType=%d, propertyKey=%d should exist.",
                relationshipType, propertyKey));
      }

      constraint = (RelationTypeSchemaDescriptor) n5.next();
    }
    while (constraint.getRelTypeId() != relationshipType || !this
        .contains(constraint.getPropertyIds(), propertyKey));

    throw new RelationshipPropertyExistenceException(constraint, Phase.VALIDATION, id);
  }

  private boolean contains(int[] list, int value) {
    int[] n3 = list;
    int n4 = list.length;

    for (int n5 = 0; n5 < n4; ++n5) {
      int x = n3[n5];
      if (value == x) {
        return true;
      }
    }

    return false;
  }

  private class Decorator extends Delegator {

    private final MutableIntSet propertyKeyIds = new IntHashSet();
    private final Read read;
    private final CursorFactory cursorFactory;
    private final NodeCursor nodeCursor;
    private final PropertyCursor propertyCursor;
    private final RelationshipScanCursor relationshipCursor;

    Decorator(TxStateVisitor next, Read read, CursorFactory cursorFactory) {
      super(next);
      this.read = read;
      this.cursorFactory = cursorFactory;
      this.nodeCursor = cursorFactory.allocateFullAccessNodeCursor();
      this.propertyCursor = cursorFactory.allocateFullAccessPropertyCursor();
      this.relationshipCursor = cursorFactory.allocateRelationshipScanCursor();
    }

    public void visitNodePropertyChanges(long id, Iterator<StorageProperty> added,
        Iterator<StorageProperty> changed, IntIterable removed)
        throws ConstraintValidationException {
      this.validateNode(id);
      super.visitNodePropertyChanges(id, added, changed, removed);
    }

    public void visitNodeLabelChanges(long id, LongSet added, LongSet removed)
        throws ConstraintValidationException {
      this.validateNode(id);
      super.visitNodeLabelChanges(id, added, removed);
    }

    public void visitCreatedRelationship(long id, int type, long startNode, long endNode)
        throws ConstraintValidationException {
      this.validateRelationship(id);
      super.visitCreatedRelationship(id, type, startNode, endNode);
    }

    public void visitRelPropertyChanges(long id, Iterator<StorageProperty> added,
        Iterator<StorageProperty> changed, IntIterable removed)
        throws ConstraintValidationException {
      this.validateRelationship(id);
      super.visitRelPropertyChanges(id, added, changed, removed);
    }

    public void close() {
      super.close();
      IOUtils.closeAllUnchecked(this.nodeCursor, this.relationshipCursor, this.propertyCursor);
    }

    private void validateNode(long nodeId) throws NodePropertyExistenceException {
      if (!PropertyExistenceEnforcer.this.mandatoryNodePropertiesByLabel.isEmpty()) {
        this.read.singleNode(nodeId, this.nodeCursor);
        if (!this.nodeCursor.next()) {
          throw new IllegalStateException(
              String.format("Node %d with changes should exist.", nodeId));
        } else {
          LabelSet labelIds = this.nodeCursor.labels();
          if (labelIds.numberOfLabels() != 0) {
            this.propertyKeyIds.clear();
            this.nodeCursor.properties(this.propertyCursor);

            while (this.propertyCursor.next()) {
              this.propertyKeyIds.add(this.propertyCursor.propertyKey());
            }

            PropertyExistenceEnforcer.this
                .validateNodeProperties(nodeId, labelIds, this.propertyKeyIds);
          }
        }
      }
    }

    private void validateRelationship(long id) throws RelationshipPropertyExistenceException {
      if (!PropertyExistenceEnforcer.this.mandatoryRelationshipPropertiesByType.isEmpty()) {
        this.read.singleRelationship(id, this.relationshipCursor);
        if (!this.relationshipCursor.next()) {
          throw new IllegalStateException(
              String.format("Relationship %d with changes should exist.", id));
        } else {
          int relationshipType = this.relationshipCursor.type();
          int[] required = PropertyExistenceEnforcer.this.mandatoryRelationshipPropertiesByType
              .get(relationshipType);
          if (required != null) {
            this.propertyKeyIds.clear();
            this.relationshipCursor.properties(this.propertyCursor);

            while (this.propertyCursor.next()) {
              this.propertyKeyIds.add(this.propertyCursor.propertyKey());
            }

            int[] n5 = required;
            int n6 = required.length;

            for (int n7 = 0; n7 < n6; ++n7) {
              int mandatory = n5[n7];
              if (!this.propertyKeyIds.contains(mandatory)) {
                PropertyExistenceEnforcer.this.failRelationship(id, relationshipType, mandatory);
              }
            }
          }
        }
      }
    }
  }
}
