/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.enterprise.lock.forseti;

import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import org.neo4j.kernel.impl.util.collection.SimpleBitSet;

class SharedLock implements ForsetiLockManager.Lock {

  private static final int UPDATE_LOCK_FLAG = Integer.MIN_VALUE;
  private static final int MAX_HOLDERS = 4680;
  private final AtomicInteger refCount = new AtomicInteger(1);
  private final AtomicReferenceArray<ForsetiClient>[] clientsHoldingThisLock = new AtomicReferenceArray[4];
  private ForsetiClient updateHolder;

  SharedLock(ForsetiClient client) {
    this.addClientHoldingLock(client);
  }

  public boolean acquire(ForsetiClient client) {
    if (!this.acquireReference()) {
      return false;
    } else if (!this.clientHoldsThisLock(client)) {
      return this.addClientHoldingLock(client);
    } else {
      this.releaseReference();
      return false;
    }
  }

  public boolean release(ForsetiClient client) {
    this.removeClientHoldingLock(client);
    return this.releaseReference();
  }

  public void copyHolderWaitListsInto(SimpleBitSet waitList) {
    AtomicReferenceArray[] n2 = this.clientsHoldingThisLock;
    int n3 = n2.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      AtomicReferenceArray<ForsetiClient> holders = n2[n4];

      for (int j = 0; holders != null && j < holders.length(); ++j) {
        ForsetiClient client = holders.get(j);
        if (client != null) {
          client.copyWaitListTo(waitList);
        }
      }
    }
  }

  public int detectDeadlock(int clientId) {
    AtomicReferenceArray[] n2 = this.clientsHoldingThisLock;
    int n3 = n2.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      AtomicReferenceArray<ForsetiClient> holders = n2[n4];

      for (int j = 0; holders != null && j < holders.length(); ++j) {
        ForsetiClient client = holders.get(j);
        if (client != null && client.isWaitingFor(clientId)) {
          return client.id();
        }
      }
    }

    return -1;
  }

  public boolean tryAcquireUpdateLock(ForsetiClient client) {
    while (true) {
      int refs = this.refCount.get();
      if (refs > 0) {
        if (!this.refCount.compareAndSet(refs, refs | Integer.MIN_VALUE)) {
          continue;
        }

        this.updateHolder = client;
        return true;
      }

      return false;
    }
  }

  public void releaseUpdateLock() {
    int refs;
    do {
      refs = this.refCount.get();
      this.cleanUpdateHolder();
    }
    while (!this.refCount.compareAndSet(refs, refs & Integer.MAX_VALUE));
  }

  public void cleanUpdateHolder() {
    this.updateHolder = null;
  }

  public int numberOfHolders() {
    return this.refCount.get() & Integer.MAX_VALUE;
  }

  public boolean isUpdateLock() {
    return (this.refCount.get() & Integer.MIN_VALUE) == Integer.MIN_VALUE;
  }

  public String describeWaitList() {
    StringBuilder sb = new StringBuilder("SharedLock[");
    AtomicReferenceArray[] n2 = this.clientsHoldingThisLock;
    int n3 = n2.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      AtomicReferenceArray<ForsetiClient> holders = n2[n4];
      boolean first = true;

      for (int j = 0; holders != null && j < holders.length(); ++j) {
        ForsetiClient current = holders.get(j);
        if (current != null) {
          sb.append(first ? "" : ", ").append(current.describeWaitList());
          first = false;
        }
      }
    }

    return sb.append("]").toString();
  }

  public void collectOwners(Set<ForsetiClient> owners) {
    AtomicReferenceArray[] n2 = this.clientsHoldingThisLock;
    int n3 = n2.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      AtomicReferenceArray<ForsetiClient> ownerArray = n2[n4];
      if (ownerArray != null) {
        int len = ownerArray.length();

        for (int i = 0; i < len; ++i) {
          ForsetiClient owner = ownerArray.get(i);
          if (owner != null) {
            owners.add(owner);
          }
        }
      }
    }
  }

  public String toString() {
    int n10000;
    if (this.isUpdateLock()) {
      n10000 = System.identityHashCode(this);
      return "UpdateLock{objectId=" + n10000 + ", refCount=" + (this.refCount.get()
          & Integer.MAX_VALUE) + ", holder=" + this.updateHolder + "}";
    } else {
      n10000 = System.identityHashCode(this);
      return "SharedLock{objectId=" + n10000 + ", refCount=" + this.refCount + "}";
    }
  }

  private void removeClientHoldingLock(ForsetiClient client) {
    AtomicReferenceArray[] n2 = this.clientsHoldingThisLock;
    int n3 = n2.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      AtomicReferenceArray<ForsetiClient> holders = n2[n4];
      if (holders == null) {
        break;
      }

      for (int j = 0; j < holders.length(); ++j) {
        ForsetiClient current = holders.get(j);
        if (current != null && current.equals(client)) {
          holders.set(j, null);
          return;
        }
      }
    }

    throw new IllegalStateException(
        client + " asked to be removed from holder list, but it does not hold " + this);
  }

  private boolean addClientHoldingLock(ForsetiClient client) {
    while (true) {
      for (int i = 0; i < this.clientsHoldingThisLock.length; ++i) {
        AtomicReferenceArray<ForsetiClient> holders = this.clientsHoldingThisLock[i];
        if (holders == null) {
          holders = this.addHolderArray(i);
        }

        for (int j = 0; j < holders.length(); ++j) {
          ForsetiClient c = holders.get(j);
          if (c == null && holders.compareAndSet(j, null, client)) {
            return true;
          }
        }
      }
    }
  }

  private boolean acquireReference() {
    while (true) {
      int refs = this.refCount.get();
      if (refs > 0 && refs < 4680) {
        if (!this.refCount.compareAndSet(refs, refs + 1)) {
          continue;
        }

        return true;
      }

      return false;
    }
  }

  private boolean releaseReference() {
    int refAndUpdateFlag;
    int newRefCount;
    do {
      refAndUpdateFlag = this.refCount.get();
      newRefCount = (refAndUpdateFlag & Integer.MAX_VALUE) - 1;
    }
    while (!this.refCount
        .compareAndSet(refAndUpdateFlag, newRefCount | refAndUpdateFlag & Integer.MIN_VALUE));

    return newRefCount == 0;
  }

  private synchronized AtomicReferenceArray<ForsetiClient> addHolderArray(int slot) {
    if (this.clientsHoldingThisLock[slot] == null) {
      this.clientsHoldingThisLock[slot] = new AtomicReferenceArray(
          (int) (8.0D * Math.pow(8.0D, slot)));
    }

    return this.clientsHoldingThisLock[slot];
  }

  private boolean clientHoldsThisLock(ForsetiClient client) {
    AtomicReferenceArray[] n2 = this.clientsHoldingThisLock;
    int n3 = n2.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      AtomicReferenceArray<ForsetiClient> holders = n2[n4];

      for (int j = 0; holders != null && j < holders.length(); ++j) {
        ForsetiClient current = holders.get(j);
        if (current != null && current.equals(client)) {
          return true;
        }
      }
    }

    return false;
  }
}
