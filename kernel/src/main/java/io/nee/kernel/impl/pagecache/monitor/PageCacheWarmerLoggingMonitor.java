/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.pagecache.monitor;

import java.util.concurrent.TimeUnit;
import org.neo4j.internal.helpers.Format;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.time.Stopwatch;

public class PageCacheWarmerLoggingMonitor extends PageCacheWarmerMonitorAdapter {

  private final Log log;
  private Stopwatch warmupStart;

  public PageCacheWarmerLoggingMonitor(Log log) {
    this.log = log;
  }

  public void warmupStarted(NamedDatabaseId namedDatabaseId) {
    this.warmupStart = Stopwatch.start();
    this.log.info("Page cache warmup started.");
  }

  public void warmupCompleted(NamedDatabaseId namedDatabaseId, long pagesLoaded) {
    this.log.info("Page cache warmup completed. %d pages loaded. Duration: %s.",
        pagesLoaded, Format.duration(this.warmupStart.elapsed(TimeUnit.MILLISECONDS)));
  }
}
