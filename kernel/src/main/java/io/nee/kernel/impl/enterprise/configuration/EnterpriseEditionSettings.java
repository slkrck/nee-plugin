/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.enterprise.configuration;

import java.util.List;
import org.neo4j.configuration.Description;
import org.neo4j.configuration.SettingConstraints;
import org.neo4j.configuration.SettingImpl;
import org.neo4j.configuration.SettingValueParsers;
import org.neo4j.configuration.SettingsDeclaration;
import org.neo4j.graphdb.config.Setting;

public class EnterpriseEditionSettings implements SettingsDeclaration {

  @Description("The maximum number of databases.")
  public static final Setting<Long> maxNumberOfDatabases;
  @Description("A list of setting name patterns (comma separated) that are allowed to be dynamically changed. The list may contain both full setting names, and partial names with the wildcard '*'. If this setting is left empty all dynamic settings updates will be blocked.")
  public static final Setting<List<String>> dynamic_setting_whitelist;
  @Description("Configure the operating mode of the database -- 'SINGLE' for stand-alone operation, 'CORE' for operating as a core member of a Causal Cluster, or 'READ_REPLICA' for operating as a read replica member of a Causal Cluster.")
  public static final Setting<EnterpriseEditionSettings.Mode> mode;

  static {
    maxNumberOfDatabases =
        SettingImpl.newBuilder("dbms.max_databases", SettingValueParsers.LONG, 100L)
            .addConstraint(SettingConstraints.min(2L)).build();
    dynamic_setting_whitelist =
        SettingImpl.newBuilder("dbms.dynamic.setting.whitelist",
            SettingValueParsers.listOf(SettingValueParsers.STRING), List.of("*")).build();
    mode = SettingImpl
        .newBuilder("dbms.mode", SettingValueParsers.ofEnum(EnterpriseEditionSettings.Mode.class),
            EnterpriseEditionSettings.Mode.SINGLE).build();
  }

  public enum Mode {
    SINGLE,
    CORE,
    READ_REPLICA
  }
}
