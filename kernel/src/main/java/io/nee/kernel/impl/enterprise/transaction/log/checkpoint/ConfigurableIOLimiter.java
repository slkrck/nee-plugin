/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.enterprise.transaction.log.checkpoint;

import java.io.Flushable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import java.util.function.ObjLongConsumer;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.io.pagecache.IOLimiter;
import org.neo4j.util.VisibleForTesting;

public class ConfigurableIOLimiter implements IOLimiter {

  private static final AtomicLongFieldUpdater<ConfigurableIOLimiter> stateUpdater = AtomicLongFieldUpdater
      .newUpdater(ConfigurableIOLimiter.class, "state");
  private static final int NO_LIMIT = 0;
  private static final int QUANTUM_MILLIS = 100;
  private static final int TIME_BITS = 32;
  private static final long TIME_MASK = 4294967295L;
  private static final int QUANTUMS_PER_SECOND;

  static {
    QUANTUMS_PER_SECOND = (int) (TimeUnit.SECONDS.toMillis(1L) / 100L);
  }

  private final ObjLongConsumer<Object> pauseNanos;
  private volatile long state;

  public ConfigurableIOLimiter(Config config) {
    this(config, LockSupport::parkNanos);
  }

  @VisibleForTesting
  ConfigurableIOLimiter(Config config, ObjLongConsumer<Object> pauseNanos) {
    this.pauseNanos = pauseNanos;
    Integer iops = config.get(GraphDatabaseSettings.check_point_iops_limit);
    this.updateConfiguration(iops);
    config.addListener(GraphDatabaseSettings.check_point_iops_limit, (prev, update) ->
    {
      this.updateConfiguration(update);
    });
  }

  private static long composeState(int disabledCounter, int iopq) {
    return (long) disabledCounter << 32 | (long) iopq;
  }

  private static int getIOPQ(long state) {
    return (int) (state & 4294967295L);
  }

  private static int getDisabledCounter(long state) {
    return (int) (state >>> 32);
  }

  private static long currentTimeMillis() {
    return TimeUnit.NANOSECONDS.toMillis(System.nanoTime());
  }

  private void updateConfiguration(Integer iops) {
    long oldState;
    long newState;
    int disabledCounter;
    if (iops != null && iops >= 1) {
      do {
        oldState = stateUpdater.get(this);
        disabledCounter = getDisabledCounter(oldState);
        disabledCounter &= -2;
        int iopq = iops / QUANTUMS_PER_SECOND;
        newState = composeState(disabledCounter, iopq);
      }
      while (!stateUpdater.compareAndSet(this, oldState, newState));
    } else {
      do {
        oldState = stateUpdater.get(this);
        disabledCounter = getDisabledCounter(oldState);
        disabledCounter |= 1;
        newState = composeState(disabledCounter, 0);
      }
      while (!stateUpdater.compareAndSet(this, oldState, newState));
    }
  }

  public long maybeLimitIO(long previousStamp, int recentlyCompletedIOs, Flushable flushable) {
    long state = stateUpdater.get(this);
    if (getDisabledCounter(state) > 0) {
      return 0L;
    } else {
      long now = currentTimeMillis() & 4294967295L;
      long then = previousStamp & 4294967295L;
      if (now - then > 100L) {
        return now + ((long) recentlyCompletedIOs << 32);
      } else {
        long ioSum = (previousStamp >> 32) + (long) recentlyCompletedIOs;
        if (ioSum >= (long) getIOPQ(state)) {
          long millisLeftInQuantum = Math.min(100L, 100L - (now - then));
          this.pauseNanos.accept(this, TimeUnit.MILLISECONDS.toNanos(millisLeftInQuantum));
          return currentTimeMillis() & 4294967295L;
        } else {
          return then + (ioSum << 32);
        }
      }
    }
  }

  public void disableLimit() {
    long currentState;
    long newState;
    do {
      currentState = stateUpdater.get(this);
      int disabledCounter = getDisabledCounter(currentState) + 2;
      newState = composeState(disabledCounter, getIOPQ(currentState));
    }
    while (!stateUpdater.compareAndSet(this, currentState, newState));
  }

  public void enableLimit() {
    long currentState;
    long newState;
    do {
      currentState = stateUpdater.get(this);
      int disabledCounter = getDisabledCounter(currentState) - 2;
      newState = composeState(disabledCounter, getIOPQ(currentState));
    }
    while (!stateUpdater.compareAndSet(this, currentState, newState));
  }

  public boolean isLimited() {
    return getDisabledCounter(this.state) == 0;
  }
}
