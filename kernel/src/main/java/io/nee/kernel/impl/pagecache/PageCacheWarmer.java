/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.pagecache;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.graphdb.Resource;
import org.neo4j.io.fs.FileHandle;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.io.pagecache.PagedFile;
import org.neo4j.io.pagecache.impl.FileIsNotMappedException;
import org.neo4j.kernel.impl.transaction.state.DatabaseFileListing.StoreFileProvider;
import org.neo4j.logging.Log;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobHandle;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.storageengine.api.StoreFileMetadata;

public class PageCacheWarmer implements StoreFileProvider {

  public static final String SUFFIX_CACHEPROF = ".cacheprof";
  private static final int IO_PARALLELISM = Runtime.getRuntime().availableProcessors();
  private final FileSystemAbstraction fs;
  private final PageCache pageCache;
  private final JobScheduler scheduler;
  private final File databaseDirectory;
  private final File profilesDirectory;
  private final Log log;
  private final ProfileRefCounts refCounts;
  private final Config config;
  private volatile boolean stopped;
  private ExecutorService executor;
  private PageLoaderFactory pageLoaderFactory;

  PageCacheWarmer(FileSystemAbstraction fs, PageCache pageCache, JobScheduler scheduler,
      File databaseDirectory, Config config, Log log) {
    this.fs = fs;
    this.pageCache = pageCache;
    this.scheduler = scheduler;
    this.databaseDirectory = databaseDirectory;
    this.profilesDirectory = new File(databaseDirectory, "profiles");
    this.log = log;
    this.refCounts = new ProfileRefCounts();
    this.config = config;
  }

  private static ExecutorService buildExecutorService(JobScheduler scheduler) {
    BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue(IO_PARALLELISM * 4);
    RejectedExecutionHandler rejectionPolicy = new CallerRunsPolicy();
    ThreadFactory threadFactory = scheduler.threadFactory(Group.FILE_IO_HELPER);
    return new ThreadPoolExecutor(0, IO_PARALLELISM, 10L, TimeUnit.SECONDS, workQueue,
        threadFactory, rejectionPolicy);
  }

  private static Stream<Profile> filterRelevant(Profile[] profiles, PagedFile pagedFile) {
    return Stream.of(profiles).filter(Profile.relevantTo(pagedFile));
  }

  public synchronized Resource addFilesTo(Collection<StoreFileMetadata> coll) throws IOException {
    if (this.stopped) {
      return Resource.EMPTY;
    } else {
      List<PagedFile> pagedFilesInDatabase = this.pageCache.listExistingMappings();
      Profile[] existingProfiles = this.findExistingProfiles(pagedFilesInDatabase);
      Profile[] n4 = existingProfiles;
      int n5 = existingProfiles.length;

      for (int n6 = 0; n6 < n5; ++n6) {
        Profile profile = n4[n6];
        coll.add(new StoreFileMetadata(profile.file(), 1, false));
      }

      this.refCounts.incrementRefCounts(existingProfiles);
      return () ->
      {
        this.refCounts.decrementRefCounts(existingProfiles);
      };
    }
  }

  public synchronized void start() {
    this.stopped = false;
    this.executor = buildExecutorService(this.scheduler);
    this.pageLoaderFactory = new PageLoaderFactory(this.executor, this.pageCache);
  }

  public void stop() {
    this.stopped = true;
    this.stopWarmer();
  }

  private synchronized void stopWarmer() {
    if (this.executor != null) {
      this.executor.shutdown();
    }
  }

  synchronized OptionalLong reheat() throws IOException {
    if (this.stopped) {
      return OptionalLong.empty();
    } else {
      return this.config.get(GraphDatabaseSettings.pagecache_warmup_prefetch) ? OptionalLong
          .of(this.loadEverything())
          : OptionalLong.of(this.loadEverythingFromProfile());
    }
  }

  private long loadEverything() {
    try {
      Pattern whitelist = Pattern
          .compile(this.config.get(GraphDatabaseSettings.pagecache_warmup_prefetch_whitelist));
      this.log.info("Warming up page cache by pre-fetching files matching regex: %s",
          whitelist.pattern());
      List<JobHandle> handles = new ArrayList();
      LongAdder totalPageCounter = new LongAdder();
      Iterator n4 = this.pageCache.listExistingMappings().iterator();

      while (n4.hasNext()) {
        PagedFile pagedFile = (PagedFile) n4.next();
        if (whitelist.matcher(pagedFile.file().toString()).find()) {
          handles.add(this.scheduler.schedule(Group.FILE_IO_HELPER, () ->
          {
            totalPageCounter.add(this.touchAllPages(pagedFile));
          }));
        }
      }

      n4 = handles.iterator();

      while (n4.hasNext()) {
        JobHandle handle = (JobHandle) n4.next();
        handle.waitTermination();
      }

      this.log.info("Warming of page cache completed");
      return totalPageCounter.sum();
    } catch (IOException n6) {
      throw new RuntimeException("Could not list existing mappings in page cache", n6);
    } catch (InterruptedException | ExecutionException n7) {
      throw new RuntimeException("Got interrupted while warming up page cache", n7);
    }
  }

  private long touchAllPages(PagedFile pagedFile) {
    this.log.debug("Pre-fetching %s", pagedFile.file().getName());

    try {
      PageCursor cursor = pagedFile.io(0L, 9);

      long n5;
      try {
        long pages = 0L;

        while (true) {
          if (this.stopped || !cursor.next()) {
            this.pageCache.reportEvents();
            n5 = pages;
            break;
          }

          ++pages;
        }
      } catch (Throwable n8) {
        if (cursor != null) {
          try {
            cursor.close();
          } catch (Throwable n7) {
            n8.addSuppressed(n7);
          }
        }

        throw n8;
      }

      if (cursor != null) {
        cursor.close();
      }

      return n5;
    } catch (IOException n9) {
      throw new RuntimeException("Could not prefetch all pages into page cache", n9);
    }
  }

  private long loadEverythingFromProfile() throws IOException {
    long pagesLoaded = 0L;
    List<PagedFile> pagedFilesInDatabase = this.pageCache.listExistingMappings();
    Profile[] existingProfiles = this.findExistingProfiles(pagedFilesInDatabase);
    Iterator n5 = pagedFilesInDatabase.iterator();

    while (n5.hasNext()) {
      PagedFile file = (PagedFile) n5.next();

      try {
        pagesLoaded += this.reheat(file, existingProfiles);
      } catch (FileIsNotMappedException n8) {
      }
    }

    return pagesLoaded;
  }

  public synchronized OptionalLong profile() throws IOException {
    if (this.stopped) {
      return OptionalLong.empty();
    } else {
      long pagesInMemory = 0L;
      List<PagedFile> pagedFilesInDatabase = this.pageCache.listExistingMappings();
      Profile[] existingProfiles = this.findExistingProfiles(pagedFilesInDatabase);
      Iterator n5 = pagedFilesInDatabase.iterator();

      while (n5.hasNext()) {
        PagedFile file = (PagedFile) n5.next();

        try {
          pagesInMemory += this.profile(file, existingProfiles);
        } catch (FileIsNotMappedException n8) {
        }

        if (this.stopped) {
          this.pageCache.reportEvents();
          return OptionalLong.empty();
        }
      }

      this.pageCache.reportEvents();
      return OptionalLong.of(pagesInMemory);
    }
  }

  private long reheat(PagedFile file, Profile[] existingProfiles) throws IOException {
    Optional<Profile> savedProfile =
        filterRelevant(existingProfiles, file).sorted(Comparator.reverseOrder())
            .filter(this::verifyChecksum).findFirst();
    if (savedProfile.isEmpty()) {
      return 0L;
    } else {
      long pagesLoaded = 0L;
      InputStream input = savedProfile.get().read(this.fs);

      long n12;
      label93:
      {
        try {
          label94:
          {
            PageLoader loader;
            label95:
            {
              loader = this.pageLoaderFactory.getLoader(file);

              try {
                long pageId = 0L;

                int b;
                while ((b = input.read()) != -1) {
                  for (int i = 0; i < 8; ++i) {
                    if (this.stopped) {
                      this.pageCache.reportEvents();
                      n12 = pagesLoaded;
                      break label95;
                    }

                    if ((b & 1) == 1) {
                      loader.load(pageId);
                      ++pagesLoaded;
                    }

                    b >>= 1;
                    ++pageId;
                  }
                }
              } catch (Throwable n16) {
                if (loader != null) {
                  try {
                    loader.close();
                  } catch (Throwable n15) {
                    n16.addSuppressed(n15);
                  }
                }

                throw n16;
              }

              if (loader != null) {
                loader.close();
              }
              break label94;
            }

            if (loader != null) {
              loader.close();
            }
            break label93;
          }
        } catch (Throwable n17) {
          if (input != null) {
            try {
              input.close();
            } catch (Throwable n14) {
              n17.addSuppressed(n14);
            }
          }

          throw n17;
        }

        if (input != null) {
          input.close();
        }

        this.pageCache.reportEvents();
        return pagesLoaded;
      }

      if (input != null) {
        input.close();
      }

      return n12;
    }
  }

  private boolean verifyChecksum(Profile profile) {
    try {
      InputStream input = profile.read(this.fs);

      int b;
      try {
        do {
          b = input.read();
        }
        while (b != -1);
      } catch (Throwable n6) {
        if (input != null) {
          try {
            input.close();
          } catch (Throwable n5) {
            n6.addSuppressed(n5);
          }
        }

        throw n6;
      }

      if (input != null) {
        input.close();
      }

      return true;
    } catch (IOException n7) {
      return false;
    }
  }

  private long profile(PagedFile file, Profile[] existingProfiles) throws IOException {
    long pagesInMemory = 0L;
    Profile nextProfile = filterRelevant(existingProfiles, file).max(Comparator.naturalOrder())
        .map(Profile::next).orElse(
            Profile.first(this.databaseDirectory, file.file()));
    OutputStream output = nextProfile.write(this.fs);

    try {
      PageCursor cursor = file.io(0L, 17);

      try {
        int stepper = 0;
        int b = 0;

        while (cursor.next()) {
          if (cursor.getCurrentPageId() != -1L) {
            ++pagesInMemory;
            b |= 1 << stepper;
          }

          ++stepper;
          if (stepper == 8) {
            output.write(b);
            b = 0;
            stepper = 0;
          }
        }

        output.write(b);
        output.flush();
      } catch (Throwable n12) {
        if (cursor != null) {
          try {
            cursor.close();
          } catch (Throwable n11) {
            n12.addSuppressed(n11);
          }
        }

        throw n12;
      }

      if (cursor != null) {
        cursor.close();
      }
    } catch (Throwable n13) {
      if (output != null) {
        try {
          output.close();
        } catch (Throwable n10) {
          n13.addSuppressed(n10);
        }
      }

      throw n13;
    }

    if (output != null) {
      output.close();
    }

    filterRelevant(existingProfiles, file).filter((profile) ->
    {
      return !this.refCounts.contains(profile);
    }).forEach((profile) ->
    {
      profile.delete(this.fs);
    });
    return pagesInMemory;
  }

  private Profile[] findExistingProfiles(List<PagedFile> pagedFilesInDatabase) throws IOException {
    List<Path> allProfilePaths =
        this.fs.streamFilesRecursive(this.profilesDirectory).map(FileHandle::getFile)
            .map(File::toPath).collect(Collectors.toList());
    return pagedFilesInDatabase.stream().map((pagedFile) ->
    {
      return pagedFile.file().toPath();
    }).flatMap((pagedFilePath) ->
    {
      return this.extractRelevantProfiles(allProfilePaths, pagedFilePath);
    }).toArray((n) ->
    {
      return new Profile[n];
    });
  }

  private Stream<? extends Profile> extractRelevantProfiles(List<Path> allProfilePaths,
      Path pagedFilePath) {
    Path databasePath = this.databaseDirectory.toPath();
    Path profilesPath = this.profilesDirectory.toPath();
    return allProfilePaths.stream().filter((profilePath) ->
    {
      return this.sameRelativePath(databasePath, pagedFilePath, profilesPath, profilePath);
    }).flatMap((profilePath) ->
    {
      return Profile.parseProfileName(databasePath, profilePath, pagedFilePath);
    });
  }

  private boolean sameRelativePath(Path databasePath, Path pagedFilePath, Path profilesPath,
      Path profilePath) {
    Path profileRelativeDir = profilesPath.relativize(profilePath).getParent();
    Path pagedFileRelativeDir = databasePath.relativize(pagedFilePath).getParent();
    if (profileRelativeDir != null) {
      return profileRelativeDir.equals(pagedFileRelativeDir);
    } else {
      return pagedFileRelativeDir == null;
    }
  }
}
