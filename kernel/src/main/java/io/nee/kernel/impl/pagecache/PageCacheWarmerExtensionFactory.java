/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.pagecache;

import io.nee.kernel.impl.pagecache.monitor.PageCacheWarmerLoggingMonitor;
import io.nee.kernel.impl.pagecache.monitor.PageCacheWarmerMonitor;
import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.availability.DatabaseAvailabilityGuard;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.extension.ExtensionFactory;
import org.neo4j.kernel.extension.ExtensionType;
import org.neo4j.kernel.extension.context.ExtensionContext;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;

public class PageCacheWarmerExtensionFactory extends
    ExtensionFactory<PageCacheWarmerExtensionFactory.Dependencies> {

  public PageCacheWarmerExtensionFactory() {
    super(ExtensionType.DATABASE, "pagecachewarmer");
  }

  public Lifecycle newInstance(ExtensionContext context,
      PageCacheWarmerExtensionFactory.Dependencies deps) {
    JobScheduler scheduler = deps.jobScheduler();
    DatabaseAvailabilityGuard databaseAvailabilityGuard = deps.availabilityGuard();
    PageCache pageCache = deps.pageCache();
    FileSystemAbstraction fs = deps.fileSystemAbstraction();
    LogService logService = deps.logService();
    Database database = deps.getDatabase();
    Log log = logService.getInternalLog(PageCacheWarmer.class);
    Monitors monitors = deps.monitors();
    PageCacheWarmerMonitor monitor = monitors
        .newMonitor(PageCacheWarmerMonitor.class, new String[0]);
    monitors.addMonitorListener(new PageCacheWarmerLoggingMonitor(log));
    Config config = deps.config();
    return new PageCacheWarmerExtension(scheduler, databaseAvailabilityGuard, pageCache, fs,
        database, log, monitor, config);
  }

  public interface Dependencies {

    JobScheduler jobScheduler();

    DatabaseAvailabilityGuard availabilityGuard();

    PageCache pageCache();

    FileSystemAbstraction fileSystemAbstraction();

    Database getDatabase();

    LogService logService();

    Monitors monitors();

    Config config();
  }
}
