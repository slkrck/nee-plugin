/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.enterprise.transaction.log.checkpoint;

import org.neo4j.kernel.impl.transaction.log.checkpoint.AbstractCheckPointThreshold;
import org.neo4j.kernel.impl.transaction.log.pruning.LogPruning;

public class VolumetricCheckPointThreshold extends AbstractCheckPointThreshold {

  private final LogPruning logPruning;

  VolumetricCheckPointThreshold(LogPruning logPruning) {
    super("volumetric checkpoint threshold, based on log pruning strategy");
    this.logPruning = logPruning;
  }

  protected String createCheckpointThresholdDescription(String description) {
    return description + " '" + this.logPruning.describeCurrentStrategy() + "'";
  }

  protected boolean thresholdReached(long lastCommittedTransactionId) {
    return this.logPruning.mightHaveLogsToPrune();
  }

  public void initialize(long transactionId) {
  }

  public void checkPointHappened(long transactionId) {
  }

  public long checkFrequencyMillis() {
    return DEFAULT_CHECKING_FREQUENCY_MILLIS;
  }
}
