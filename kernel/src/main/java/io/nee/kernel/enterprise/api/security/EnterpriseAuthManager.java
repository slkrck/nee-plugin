/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.enterprise.api.security;

import java.util.Map;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.kernel.api.security.AuthManager;
import org.neo4j.kernel.api.security.AuthToken;
import org.neo4j.kernel.api.security.exception.InvalidAuthTokenException;

public interface EnterpriseAuthManager extends AuthManager {

  EnterpriseAuthManager NO_AUTH = new EnterpriseAuthManager() {
    public EnterpriseLoginContext login(Map<String, Object> authToken) {
      AuthToken.clearCredentials(authToken);
      return EnterpriseLoginContext.AUTH_DISABLED;
    }

    public void init() {
    }

    public void start() {
    }

    public void stop() {
    }

    public void shutdown() {
    }

    public void clearAuthCache() {
    }

    public void log(String message, SecurityContext securityContext) {
    }
  };

  void clearAuthCache();

  EnterpriseLoginContext login(Map<String, Object> n1) throws InvalidAuthTokenException;
}
