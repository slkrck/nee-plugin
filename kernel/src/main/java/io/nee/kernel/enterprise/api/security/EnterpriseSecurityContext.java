/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.enterprise.api.security;

import java.util.Collections;
import java.util.Set;
import org.neo4j.internal.kernel.api.security.AccessMode;
import org.neo4j.internal.kernel.api.security.AccessMode.Static;
import org.neo4j.internal.kernel.api.security.AdminActionOnResource;
import org.neo4j.internal.kernel.api.security.AuthSubject;
import org.neo4j.internal.kernel.api.security.SecurityContext;

public class EnterpriseSecurityContext extends SecurityContext {

  public static final EnterpriseSecurityContext AUTH_DISABLED;

  static {
    AUTH_DISABLED = authDisabled(Static.FULL);
  }

  private final Set<String> roles;
  private final AdminAccessMode adminAccessMode;

  public EnterpriseSecurityContext(AuthSubject subject, AccessMode mode, Set<String> roles,
      AdminAccessMode adminAccessMode) {
    super(subject, mode);
    this.roles = roles;
    this.adminAccessMode = adminAccessMode;
  }

  private static EnterpriseSecurityContext authDisabled(AccessMode mode) {
    return new EnterpriseSecurityContext(AuthSubject.AUTH_DISABLED, mode, Collections.emptySet(),
        AdminAccessMode.FULL) {
      public EnterpriseSecurityContext withMode(AccessMode mode) {
        return EnterpriseSecurityContext.authDisabled(mode);
      }

      public String description() {
        return "AUTH_DISABLED with " + this.mode().name();
      }

      public String toString() {
        return this.defaultString("enterprise-auth-disabled");
      }
    };
  }

  public boolean isAdmin() {
    return this.adminAccessMode.allows(AdminActionOnResource.ALL);
  }

  public boolean allowsAdminAction(AdminActionOnResource action) {
    return this.adminAccessMode.allows(action);
  }

  public EnterpriseSecurityContext authorize(IdLookup idLookup, String dbName) {
    return this;
  }

  public EnterpriseSecurityContext withMode(AccessMode mode) {
    return new EnterpriseSecurityContext(this.subject, mode, this.roles, this.adminAccessMode);
  }

  public Set<String> roles() {
    return this.roles;
  }
}
