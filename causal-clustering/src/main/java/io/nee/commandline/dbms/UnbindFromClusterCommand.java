/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.commandline.dbms;

import io.nee.causalclustering.core.state.ClusterStateLayout;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import org.neo4j.cli.AbstractCommand;
import org.neo4j.cli.CommandFailedException;
import org.neo4j.cli.ExecutionContext;
import org.neo4j.commandline.dbms.LockChecker;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.io.layout.Neo4jLayout;
import org.neo4j.kernel.internal.locker.FileLockException;
import picocli.CommandLine.Command;

@Command(name = "unbind", header = {
    "Removes cluster state data for the specified database."}, description = {
    "Removes cluster state data for the specified database, so that the instance can rebind to a new or recovered cluster."})
class UnbindFromClusterCommand extends AbstractCommand {

  UnbindFromClusterCommand(ExecutionContext ctx) {
    super(ctx);
  }

  private static Config loadNeo4jConfig(Path homeDir, Path configDir) {
    return Config.newBuilder().fromFileNoThrow(configDir.resolve("neo4j.conf"))
        .set(GraphDatabaseSettings.neo4j_home, homeDir).build();
  }

  public void execute() {
    try {
      Config config = loadNeo4jConfig(this.ctx.homeDir(), this.ctx.confDir());
      Neo4jLayout neo4jLayout = Neo4jLayout.of(config);
      Closeable ignored = LockChecker.checkDbmsLock(neo4jLayout);

      try {
        File clusterStateDirectory =
            ClusterStateLayout.of(config.get(GraphDatabaseSettings.data_directory).toFile())
                .getClusterStateDirectory();
        if (this.ctx.fs().fileExists(clusterStateDirectory)) {
          this.deleteClusterStateIn(clusterStateDirectory);
        } else {
          this.ctx.err().println("This instance was not bound. No work performed.");
        }
      } catch (Throwable n7) {
        if (ignored != null) {
          try {
            ignored.close();
          } catch (Throwable n6) {
            n7.addSuppressed(n6);
          }
        }

        throw n7;
      }

      if (ignored != null) {
        ignored.close();
      }
    } catch (FileLockException n8) {
      throw new CommandFailedException("Database is currently locked. Please shutdown database.",
          n8);
    } catch (Exception n9) {
      throw new CommandFailedException(n9.getMessage(), n9);
    }
  }

  private void deleteClusterStateIn(File target)
      throws UnbindFromClusterCommand.UnbindFailureException {
    try {
      this.ctx.fs().deleteRecursively(target);
    } catch (IOException n3) {
      throw new UnbindFromClusterCommand.UnbindFailureException(n3);
    }
  }

  private static class UnbindFailureException extends Exception {

    UnbindFailureException(Exception e) {
      super(e);
    }
  }
}
