/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.replication;

import io.nee.causalclustering.core.state.StateMachineResult;
import java.util.Objects;

public class ReplicationResult {

  private final ReplicationResult.Outcome outcome;
  private final Throwable failure;
  private final StateMachineResult stateMachineResult;

  private ReplicationResult(ReplicationResult.Outcome outcome,
      StateMachineResult stateMachineResult) {
    Objects.requireNonNull(stateMachineResult, "Illegal result: " + stateMachineResult);
    this.outcome = outcome;
    this.failure = null;
    this.stateMachineResult = stateMachineResult;
  }

  private ReplicationResult(ReplicationResult.Outcome outcome, Throwable failure) {
    Objects.requireNonNull(failure, "Illegal failure: " + failure);
    this.outcome = outcome;
    this.stateMachineResult = null;
    this.failure = failure;
  }

  public static ReplicationResult notReplicated(Throwable failure) {
    return new ReplicationResult(ReplicationResult.Outcome.NOT_REPLICATED, failure);
  }

  public static ReplicationResult maybeReplicated(Throwable failure) {
    return new ReplicationResult(ReplicationResult.Outcome.MAYBE_REPLICATED, failure);
  }

  public static ReplicationResult applied(StateMachineResult stateMachineResult) {
    return new ReplicationResult(ReplicationResult.Outcome.APPLIED, stateMachineResult);
  }

  public ReplicationResult.Outcome outcome() {
    return this.outcome;
  }

  public Throwable failure() {
    return this.failure;
  }

  public StateMachineResult stateMachineResult() {
    return this.stateMachineResult;
  }

  public enum Outcome {
    NOT_REPLICATED,
    MAYBE_REPLICATED,
    APPLIED
  }
}
