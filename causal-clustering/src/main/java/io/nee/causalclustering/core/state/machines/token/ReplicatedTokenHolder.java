/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.token;

import io.nee.causalclustering.core.replication.ReplicationResult;
import io.nee.causalclustering.core.replication.Replicator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Supplier;
import org.neo4j.exceptions.KernelException;
import org.neo4j.graphdb.TransactionFailureException;
import org.neo4j.internal.id.IdGeneratorFactory;
import org.neo4j.internal.id.IdType;
import org.neo4j.kernel.api.txstate.TransactionState;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.api.state.TxState;
import org.neo4j.lock.ResourceLocker;
import org.neo4j.storageengine.api.CommandCreationContext;
import org.neo4j.storageengine.api.StorageCommand;
import org.neo4j.storageengine.api.StorageEngine;
import org.neo4j.storageengine.api.StorageReader;
import org.neo4j.storageengine.api.txstate.TxStateVisitor;
import org.neo4j.token.AbstractTokenHolderBase;
import org.neo4j.token.TokenRegistry;

public class ReplicatedTokenHolder extends AbstractTokenHolderBase {

  private final Replicator replicator;
  private final IdGeneratorFactory idGeneratorFactory;
  private final IdType tokenIdType;
  private final TokenType type;
  private final Supplier<StorageEngine> storageEngineSupplier;
  private final ReplicatedTokenCreator tokenCreator;
  private final DatabaseId databaseId;

  ReplicatedTokenHolder(NamedDatabaseId namedDatabaseId, TokenRegistry tokenRegistry,
      Replicator replicator, IdGeneratorFactory idGeneratorFactory,
      IdType tokenIdType, Supplier<StorageEngine> storageEngineSupplier, TokenType type,
      ReplicatedTokenCreator tokenCreator) {
    super(tokenRegistry);
    this.replicator = replicator;
    this.idGeneratorFactory = idGeneratorFactory;
    this.tokenIdType = tokenIdType;
    this.type = type;
    this.storageEngineSupplier = storageEngineSupplier;
    this.tokenCreator = tokenCreator;
    this.databaseId = namedDatabaseId.databaseId();
  }

  public void getOrCreateIds(String[] names, int[] ids) throws KernelException {
    for (int i = 0; i < names.length; ++i) {
      ids[i] = this.innerGetOrCreateId(names[i], false);
    }
  }

  public void getOrCreateInternalIds(String[] names, int[] ids) throws KernelException {
    for (int i = 0; i < names.length; ++i) {
      ids[i] = this.innerGetOrCreateId(names[i], true);
    }
  }

  protected int createToken(String tokenName, boolean internal) {
    ReplicatedTokenRequest tokenRequest = new ReplicatedTokenRequest(this.databaseId, this.type,
        tokenName, this.createCommands(tokenName, internal));
    ReplicationResult replicationResult = this.replicator.replicate(tokenRequest);
    if (replicationResult.outcome() != ReplicationResult.Outcome.APPLIED) {
      throw new TransactionFailureException("Could not replicate token for " + this.databaseId,
          replicationResult.failure());
    } else {
      try {
        return (Integer) replicationResult.stateMachineResult().consume();
      } catch (Exception n6) {
        throw new IllegalStateException(n6);
      }
    }
  }

  private byte[] createCommands(String tokenName, boolean internal) {
    StorageEngine storageEngine = this.storageEngineSupplier.get();
    Collection<StorageCommand> commands = new ArrayList();
    TransactionState txState = new TxState();
    int tokenId = Math.toIntExact(this.idGeneratorFactory.get(this.tokenIdType).nextId());
    this.tokenCreator.createToken(txState, tokenName, internal, tokenId);

    try {
      StorageReader reader = storageEngine.newReader();

      try {
        CommandCreationContext creationContext = storageEngine.newCommandCreationContext();

        try {
          storageEngine
              .createCommands(commands, txState, reader, creationContext, ResourceLocker.PREVENT,
                  Long.MAX_VALUE,
                  TxStateVisitor.NO_DECORATION);
        } catch (Throwable n13) {
          if (creationContext != null) {
            try {
              creationContext.close();
            } catch (Throwable n12) {
              n13.addSuppressed(n12);
            }
          }

          throw n13;
        }

        if (creationContext != null) {
          creationContext.close();
        }
      } catch (Throwable n14) {
        if (reader != null) {
          try {
            reader.close();
          } catch (Throwable n11) {
            n14.addSuppressed(n11);
          }
        }

        throw n14;
      }

      if (reader != null) {
        reader.close();
      }
    } catch (KernelException n15) {
      throw new RuntimeException(
          "Unable to create token '" + tokenName + "' for " + this.databaseId, n15);
    }

    return StorageCommandMarshal.commandsToBytes(commands);
  }
}
