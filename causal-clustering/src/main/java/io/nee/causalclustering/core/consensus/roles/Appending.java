/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.roles;

import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.consensus.outcome.AppendLogEntry;
import io.nee.causalclustering.core.consensus.outcome.BatchAppendLogEntries;
import io.nee.causalclustering.core.consensus.outcome.Outcome;
import io.nee.causalclustering.core.consensus.outcome.ShipCommand;
import io.nee.causalclustering.core.consensus.outcome.TruncateLogCommand;
import io.nee.causalclustering.core.consensus.state.ReadableRaftState;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import java.io.IOException;
import java.util.Collection;
import org.neo4j.logging.Log;

class Appending {

  private Appending() {
  }

  static void handleAppendEntriesRequest(ReadableRaftState state, Outcome outcome,
      RaftMessages.AppendEntries.Request request, Log log) throws IOException {
    RaftMessages.AppendEntries.Response appendResponse;
    if (request.leaderTerm() < state.term()) {
      appendResponse = new RaftMessages.AppendEntries.Response(state.myself(), state.term(), false,
          -1L, state.entryLog().appendIndex());
      outcome.addOutgoingMessage(new RaftMessages.Directed(request.from(), appendResponse));
    } else {
      outcome.setPreElection(false);
      outcome.setNextTerm(request.leaderTerm());
      outcome.setLeader(request.from());
      outcome.setLeaderCommit(request.leaderCommit());
      if (!Follower.logHistoryMatches(state, request.prevLogIndex(), request.prevLogTerm())) {
        assert request.prevLogIndex() > -1L && request.prevLogTerm() > -1L;

        appendResponse = new RaftMessages.AppendEntries.Response(state.myself(),
            request.leaderTerm(), false, -1L, state.entryLog().appendIndex());
        outcome.addOutgoingMessage(new RaftMessages.Directed(request.from(), appendResponse));
      } else {
        long baseIndex = request.prevLogIndex() + 1L;

        int offset;
        long logIndex;
        for (offset = 0; offset < request.entries().length; ++offset) {
          logIndex = baseIndex + (long) offset;
          long logTerm = state.entryLog().readEntryTerm(logIndex);
          if (logIndex > state.entryLog().appendIndex()) {
            break;
          }

          if (logIndex >= state.entryLog().prevIndex() && logTerm != request.entries()[offset]
              .term()) {
            if (logIndex <= state.commitIndex()) {
              throw new IllegalStateException(
                  String.format(
                      "Cannot truncate entry at index %d with term %d when commit index is at %d",
                      logIndex, logTerm,
                      state.commitIndex()));
            }

            outcome.addLogCommand(new TruncateLogCommand(logIndex));
            break;
          }
        }

        if (offset < request.entries().length) {
          outcome.addLogCommand(new BatchAppendLogEntries(baseIndex, offset, request.entries()));
        }

        Follower
            .commitToLogOnUpdate(state, request.prevLogIndex() + (long) request.entries().length,
                request.leaderCommit(), outcome);
        logIndex = request.prevLogIndex() + (long) request.entries().length;
        appendResponse =
            new RaftMessages.AppendEntries.Response(state.myself(), request.leaderTerm(), true,
                logIndex, logIndex);
        outcome.addOutgoingMessage(new RaftMessages.Directed(request.from(), appendResponse));
      }
    }
  }

  static void appendNewEntry(ReadableRaftState ctx, Outcome outcome, ReplicatedContent content)
      throws IOException {
    long prevLogIndex = ctx.entryLog().appendIndex();
    long prevLogTerm =
        prevLogIndex == -1L ? -1L
            : (prevLogIndex > ctx.lastLogIndexBeforeWeBecameLeader() ? ctx.term()
                : ctx.entryLog().readEntryTerm(prevLogIndex));
    RaftLogEntry newLogEntry = new RaftLogEntry(ctx.term(), content);
    outcome.addShipCommand(
        new ShipCommand.NewEntries(prevLogIndex, prevLogTerm, new RaftLogEntry[]{newLogEntry}));
    outcome.addLogCommand(new AppendLogEntry(prevLogIndex + 1L, newLogEntry));
  }

  static void appendNewEntries(ReadableRaftState ctx, Outcome outcome,
      Collection<ReplicatedContent> contents) throws IOException {
    long prevLogIndex = ctx.entryLog().appendIndex();
    long prevLogTerm =
        prevLogIndex == -1L ? -1L
            : (prevLogIndex > ctx.lastLogIndexBeforeWeBecameLeader() ? ctx.term()
                : ctx.entryLog().readEntryTerm(prevLogIndex));
    RaftLogEntry[] raftLogEntries = contents.stream().map((content) ->
    {
      return new RaftLogEntry(ctx.term(), content);
    }).toArray((n) ->
    {
      return new RaftLogEntry[n];
    });
    outcome.addShipCommand(new ShipCommand.NewEntries(prevLogIndex, prevLogTerm, raftLogEntries));
    outcome.addLogCommand(new BatchAppendLogEntries(prevLogIndex + 1L, 0, raftLogEntries));
  }
}
