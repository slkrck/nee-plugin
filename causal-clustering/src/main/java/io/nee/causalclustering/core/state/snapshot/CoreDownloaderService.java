/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.snapshot;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.core.state.CommandApplicationProcess;
import io.nee.causalclustering.core.state.CoreSnapshotService;
import io.nee.causalclustering.error_handling.DatabasePanicker;
import io.nee.dbms.DatabaseStartAborter;
import io.nee.dbms.ReplicatedDatabaseEventService;
import java.util.Optional;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobHandle;
import org.neo4j.scheduler.JobScheduler;

public class CoreDownloaderService extends LifecycleAdapter {

  private final JobScheduler jobScheduler;
  private final CoreDownloader downloader;
  private final CommandApplicationProcess applicationProcess;
  private final Log log;
  private final TimeoutStrategy backoffStrategy;
  private final DatabasePanicker panicker;
  private final Monitors monitors;
  private final DatabaseStartAborter databaseStartAborter;
  private final StoreDownloadContext context;
  private final CoreSnapshotService snapshotService;
  private final ReplicatedDatabaseEventService databaseEventService;
  private PersistentSnapshotDownloader currentJob;
  private JobHandle jobHandle;
  private boolean stopped;

  public CoreDownloaderService(JobScheduler jobScheduler, CoreDownloader downloader,
      StoreDownloadContext context, CoreSnapshotService snapshotService,
      ReplicatedDatabaseEventService databaseEventService,
      CommandApplicationProcess applicationProcess, LogProvider logProvider,
      TimeoutStrategy backoffStrategy, DatabasePanicker panicker, Monitors monitors,
      DatabaseStartAborter databaseStartAborter) {
    this.jobScheduler = jobScheduler;
    this.downloader = downloader;
    this.context = context;
    this.snapshotService = snapshotService;
    this.databaseEventService = databaseEventService;
    this.applicationProcess = applicationProcess;
    this.log = logProvider.getLog(this.getClass());
    this.backoffStrategy = backoffStrategy;
    this.panicker = panicker;
    this.monitors = monitors;
    this.databaseStartAborter = databaseStartAborter;
  }

  public synchronized Optional<JobHandle> scheduleDownload(CatchupAddressProvider addressProvider) {
    if (this.stopped) {
      return Optional.empty();
    } else if (this.currentJob != null && !this.currentJob.hasCompleted()) {
      return Optional.of(this.jobHandle);
    } else {
      this.currentJob = new PersistentSnapshotDownloader(addressProvider, this.applicationProcess,
          this.downloader, this.snapshotService,
          this.databaseEventService, this.context, this.log, this.backoffStrategy, this.panicker,
          this.monitors);
      this.jobHandle = this.jobScheduler.schedule(Group.DOWNLOAD_SNAPSHOT, this.currentJob);
      return Optional.of(this.jobHandle);
    }
  }

  public synchronized void start() throws Exception {
    this.databaseStartAborter
        .setAbortable(this.context.databaseId(), DatabaseStartAborter.PreventReason.STORE_COPY,
            false);
  }

  public synchronized void stop() throws Exception {
    this.stopped = true;
    if (this.currentJob != null) {
      this.currentJob.stop();
    }

    this.databaseStartAborter
        .setAbortable(this.context.databaseId(), DatabaseStartAborter.PreventReason.STORE_COPY,
            true);
  }

  public synchronized Optional<JobHandle> downloadJob() {
    return Optional.ofNullable(this.jobHandle);
  }
}
