/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.replication;

import io.nee.causalclustering.core.replication.session.GlobalSession;
import io.nee.causalclustering.core.replication.session.LocalOperationId;
import io.nee.causalclustering.core.state.StateMachineResult;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class ProgressTrackerImpl implements ProgressTracker {

  private final Map<LocalOperationId, Progress> tracker = new ConcurrentHashMap();
  private final GlobalSession myGlobalSession;

  public ProgressTrackerImpl(GlobalSession myGlobalSession) {
    this.myGlobalSession = myGlobalSession;
  }

  public Progress start(DistributedOperation operation) {
    assert operation.globalSession().equals(this.myGlobalSession);

    Progress progress = new Progress();
    this.tracker.put(operation.operationId(), progress);
    return progress;
  }

  public void trackReplication(DistributedOperation operation) {
    if (operation.globalSession().equals(this.myGlobalSession)) {
      Progress progress = this.tracker.get(operation.operationId());
      if (progress != null) {
        progress.setReplicated();
      }
    }
  }

  public void trackResult(DistributedOperation operation, StateMachineResult result) {
    Objects.requireNonNull(result, "Illegal result for operation: " + operation);
    if (operation.globalSession().equals(this.myGlobalSession)) {
      Progress progress = this.tracker.remove(operation.operationId());
      if (progress != null) {
        progress.registerResult(result);
      }
    }
  }

  public void abort(DistributedOperation operation) {
    this.tracker.remove(operation.operationId());
  }

  public void triggerReplicationEvent() {
    this.tracker.forEach((ignored, progress) ->
    {
      progress.triggerReplicationEvent();
    });
  }

  public int inProgressCount() {
    return this.tracker.size();
  }
}
