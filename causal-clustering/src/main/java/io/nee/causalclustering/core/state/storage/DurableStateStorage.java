/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.storage;

import io.nee.causalclustering.core.state.CoreStateFiles;
import io.nee.causalclustering.core.state.StateRecoveryManager;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.neo4j.io.ByteUnit;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.PhysicalFlushableChannel;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class DurableStateStorage<STATE> extends LifecycleAdapter implements StateStorage<STATE> {

  private final StateRecoveryManager<STATE> recoveryManager;
  private final Log log;
  private final File fileA;
  private final File fileB;
  private final FileSystemAbstraction fsa;
  private final CoreStateFiles<STATE> fileType;
  private final StateMarshal<STATE> marshal;
  private final int numberOfEntriesBeforeRotation;
  private STATE initialState;
  private int numberOfEntriesWrittenInActiveFile;
  private File currentStoreFile;
  private PhysicalFlushableChannel currentStoreChannel;

  public DurableStateStorage(FileSystemAbstraction fsa, File baseDir,
      CoreStateFiles<STATE> fileType, int numberOfEntriesBeforeRotation,
      LogProvider logProvider) {
    this.fsa = fsa;
    this.fileType = fileType;
    this.marshal = fileType.marshal();
    this.numberOfEntriesBeforeRotation = numberOfEntriesBeforeRotation;
    this.log = logProvider.getLog(this.getClass());
    this.recoveryManager = new StateRecoveryManager(fsa, this.marshal);
    this.fileA = new File(baseDir, fileType.name() + ".a");
    this.fileB = new File(baseDir, fileType.name() + ".b");
  }

  public boolean exists() {
    return this.fsa.fileExists(this.fileA) && this.fsa.fileExists(this.fileB);
  }

  private void create() throws IOException {
    this.ensureExists(this.fileA);
    this.ensureExists(this.fileB);
  }

  private void ensureExists(File file) throws IOException {
    if (!this.fsa.fileExists(file)) {
      this.fsa.mkdirs(file.getParentFile());
      PhysicalFlushableChannel channel = this.channelForFile(file);

      try {
        this.marshal.marshal(this.marshal.startState(), channel);
      } catch (Throwable n6) {
        if (channel != null) {
          try {
            channel.close();
          } catch (Throwable n5) {
            n6.addSuppressed(n5);
          }
        }

        throw n6;
      }

      if (channel != null) {
        channel.close();
      }
    }
  }

  private void recover() throws IOException {
    StateRecoveryManager.RecoveryStatus<STATE> recoveryStatus = this.recoveryManager
        .recover(this.fileA, this.fileB);
    this.currentStoreFile = recoveryStatus.activeFile();
    this.currentStoreChannel = this.resetStoreFile(this.currentStoreFile);
    this.initialState = recoveryStatus.recoveredState();
    this.log.info("%s state restored, up to ordinal %d", this.fileType,
        this.marshal.ordinal(this.initialState));
  }

  public STATE getInitialState() {
    assert this.initialState != null;

    return this.initialState;
  }

  public void init() throws IOException {
    this.create();
    this.recover();
  }

  public synchronized void shutdown() throws IOException {
    this.currentStoreChannel.close();
    this.currentStoreChannel = null;
  }

  public synchronized void writeState(STATE state) throws IOException {
    if (this.numberOfEntriesWrittenInActiveFile >= this.numberOfEntriesBeforeRotation) {
      this.switchStoreFile();
      this.numberOfEntriesWrittenInActiveFile = 0;
    }

    this.marshal.marshal(state, this.currentStoreChannel);
    this.currentStoreChannel.prepareForFlush().flush();
    ++this.numberOfEntriesWrittenInActiveFile;
  }

  public String toString() {
    return "DurableStateStorage{fileType=" + this.fileType + "}";
  }

  private void switchStoreFile() throws IOException {
    this.currentStoreChannel.close();
    if (this.currentStoreFile.equals(this.fileA)) {
      this.currentStoreChannel = this.resetStoreFile(this.fileB);
      this.currentStoreFile = this.fileB;
    } else {
      this.currentStoreChannel = this.resetStoreFile(this.fileA);
      this.currentStoreFile = this.fileA;
    }
  }

  private PhysicalFlushableChannel resetStoreFile(File nextStore) throws IOException {
    this.fsa.truncate(nextStore, 0L);
    return this.channelForFile(nextStore);
  }

  private PhysicalFlushableChannel channelForFile(File file) throws IOException {
    ByteBuffer buffer = ByteBuffer.allocate(Math.toIntExact(ByteUnit.kibiBytes(512L)));
    return new PhysicalFlushableChannel(this.fsa.write(file), buffer);
  }
}
