/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.membership;

import io.nee.causalclustering.core.consensus.roles.Role;
import io.nee.causalclustering.core.consensus.roles.follower.FollowerStates;
import io.nee.causalclustering.identity.MemberId;
import java.util.Set;

interface RaftMembershipStateMachineEventHandler {

  RaftMembershipStateMachineEventHandler onRole(Role n1);

  RaftMembershipStateMachineEventHandler onRaftGroupCommitted();

  RaftMembershipStateMachineEventHandler onFollowerStateChange(FollowerStates<MemberId> n1);

  RaftMembershipStateMachineEventHandler onMissingMember(MemberId n1);

  RaftMembershipStateMachineEventHandler onSuperfluousMember(MemberId n1);

  RaftMembershipStateMachineEventHandler onTargetChanged(Set<MemberId> n1);

  void onExit();

  void onEntry();

  abstract class Adapter implements RaftMembershipStateMachineEventHandler {

    public RaftMembershipStateMachineEventHandler onRole(Role role) {
      return this;
    }

    public RaftMembershipStateMachineEventHandler onRaftGroupCommitted() {
      return this;
    }

    public RaftMembershipStateMachineEventHandler onMissingMember(MemberId member) {
      return this;
    }

    public RaftMembershipStateMachineEventHandler onSuperfluousMember(MemberId member) {
      return this;
    }

    public RaftMembershipStateMachineEventHandler onFollowerStateChange(
        FollowerStates<MemberId> followerStates) {
      return this;
    }

    public RaftMembershipStateMachineEventHandler onTargetChanged(Set targetMembers) {
      return this;
    }

    public void onExit() {
    }

    public void onEntry() {
    }
  }
}
