/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.replication;

import io.nee.causalclustering.core.consensus.LeaderInfo;
import io.nee.causalclustering.core.consensus.LeaderListener;
import io.nee.causalclustering.core.consensus.LeaderLocator;
import io.nee.causalclustering.core.consensus.NoLeaderFoundException;
import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.replication.monitoring.ReplicationMonitor;
import io.nee.causalclustering.core.replication.session.LocalSessionPool;
import io.nee.causalclustering.core.replication.session.OperationContext;
import io.nee.causalclustering.core.state.StateMachineResult;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.Outbound;
import io.nee.dbms.database.ClusteredDatabaseContext;
import java.time.Duration;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;
import org.neo4j.kernel.availability.UnavailableException;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;

public class RaftReplicator implements Replicator, LeaderListener {

  private final NamedDatabaseId namedDatabaseId;
  private final MemberId me;
  private final Outbound<MemberId, RaftMessages.RaftMessage> outbound;
  private final ProgressTracker progressTracker;
  private final LocalSessionPool sessionPool;
  private final TimeoutStrategy progressTimeoutStrategy;
  private final Log log;
  private final DatabaseManager<ClusteredDatabaseContext> databaseManager;
  private final ReplicationMonitor replicationMonitor;
  private final long availabilityTimeoutMillis;
  private final LeaderProvider leaderProvider;

  public RaftReplicator(NamedDatabaseId namedDatabaseId, LeaderLocator leaderLocator, MemberId me,
      Outbound<MemberId, RaftMessages.RaftMessage> outbound,
      LocalSessionPool sessionPool, ProgressTracker progressTracker,
      TimeoutStrategy progressTimeoutStrategy,
      long availabilityTimeoutMillis,
      LogProvider logProvider, DatabaseManager<ClusteredDatabaseContext> databaseManager,
      Monitors monitors, Duration leaderAwaitDuration) {
    this.namedDatabaseId = namedDatabaseId;
    this.me = me;
    this.outbound = outbound;
    this.progressTracker = progressTracker;
    this.sessionPool = sessionPool;
    this.progressTimeoutStrategy = progressTimeoutStrategy;
    this.availabilityTimeoutMillis = availabilityTimeoutMillis;
    this.log = logProvider.getLog(this.getClass());
    this.databaseManager = databaseManager;
    this.replicationMonitor = monitors.newMonitor(ReplicationMonitor.class, new String[0]);
    this.leaderProvider = new LeaderProvider(leaderAwaitDuration);
    leaderLocator.registerListener(this);
  }

  public ReplicationResult replicate(ReplicatedContent command) {
    this.replicationMonitor.clientRequest();

    try {
      this.assertDatabaseAvailable();
    } catch (UnavailableException n11) {
      this.replicationMonitor.notReplicated();
      return ReplicationResult.notReplicated(n11);
    }

    MemberId leader;
    try {
      leader = this.leaderProvider.awaitLeaderOrThrow();
    } catch (InterruptedException n9) {
      Thread.currentThread().interrupt();
      this.replicationMonitor.notReplicated();
      return ReplicationResult.notReplicated(n9);
    } catch (NoLeaderFoundException n10) {
      this.replicationMonitor.notReplicated();
      return ReplicationResult.notReplicated(n10);
    }

    OperationContext session = this.sessionPool.acquireSession();
    DistributedOperation operation = new DistributedOperation(command, session.globalSession(),
        session.localOperationId());
    StateMachineResult stateMachineResult = null;
    Progress progress = this.progressTracker.start(operation);

    try {
      Timeout progressTimeout = this.progressTimeoutStrategy.newTimeout();
      ReplicationLogger logger = new ReplicationLogger(this.log);

      do {
        logger.newAttempt(operation, leader);
        if (this.tryReplicate(leader, operation, progress, progressTimeout)) {
          this.sessionPool.releaseSession(session);
          this.replicationMonitor.successfullyReplicated();
          logger.success(operation);
          progress.awaitResult();
          stateMachineResult = progress.result();
        } else {
          this.assertDatabaseAvailable();
          leader = this.leaderProvider.awaitLeaderOrThrow();
        }
      }
      while (stateMachineResult == null);
    } catch (Throwable n12) {
      if (n12 instanceof InterruptedException) {
        Thread.currentThread().interrupt();
      }

      this.progressTracker.abort(operation);
      this.replicationMonitor.maybeReplicated();
      return ReplicationResult.maybeReplicated(n12);
    }

    return ReplicationResult.applied(stateMachineResult);
  }

  private boolean tryReplicate(MemberId leader, DistributedOperation operation, Progress progress,
      Timeout replicationTimeout) throws InterruptedException {
    this.replicationMonitor.replicationAttempt();
    this.outbound.send(leader, new RaftMessages.NewEntry.Request(this.me, operation), true);
    progress.awaitReplication(replicationTimeout.getAndIncrement());
    return progress.isReplicated();
  }

  public void onLeaderSwitch(LeaderInfo leaderInfo) {
    this.progressTracker.triggerReplicationEvent();
    MemberId newLeader = leaderInfo.memberId();
    MemberId oldLeader = this.leaderProvider.currentLeader();
    if (newLeader == null && oldLeader != null) {
      this.log.info("Lost previous leader '%s'. Currently no available leader", oldLeader);
    } else if (newLeader != null && oldLeader == null) {
      this.log.info("A new leader has been detected: '%s'", newLeader);
    }

    this.leaderProvider.setLeader(newLeader);
  }

  private void assertDatabaseAvailable() throws UnavailableException {
    Database database = this.databaseManager.getDatabaseContext(this.namedDatabaseId)
        .map(DatabaseContext::database).orElseThrow(
            IllegalStateException::new);
    database.getDatabaseAvailabilityGuard().await(this.availabilityTimeoutMillis);
    if (!database.getDatabaseHealth().isHealthy()) {
      throw new UnavailableException("Database is not healthy.");
    }
  }
}
