/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.core.consensus.RaftMachine;
import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.outcome.ConsensusOutcome;
import io.nee.causalclustering.core.consensus.outcome.SnapshotRequirement;
import io.nee.causalclustering.core.state.snapshot.CoreDownloaderService;
import io.nee.causalclustering.error_handling.DatabasePanicker;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.LifecycleMessageHandler;
import java.util.Optional;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.JobHandle;

public class RaftMessageApplier implements
    LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> {

  private final Log log;
  private final RaftMachine raftMachine;
  private final CoreDownloaderService downloadService;
  private final CommandApplicationProcess applicationProcess;
  private final DatabasePanicker panicker;
  private final CatchupAddressProvider.LeaderOrUpstreamStrategyBasedAddressProvider catchupAddressProvider;
  private boolean stopped;

  public RaftMessageApplier(LogProvider logProvider, RaftMachine raftMachine,
      CoreDownloaderService downloadService,
      CommandApplicationProcess applicationProcess,
      CatchupAddressProvider.LeaderOrUpstreamStrategyBasedAddressProvider catchupAddressProvider,
      DatabasePanicker panicker) {
    this.log = logProvider.getLog(this.getClass());
    this.raftMachine = raftMachine;
    this.downloadService = downloadService;
    this.applicationProcess = applicationProcess;
    this.catchupAddressProvider = catchupAddressProvider;
    this.panicker = panicker;
  }

  public synchronized void handle(
      RaftMessages.ReceivedInstantRaftIdAwareMessage<?> wrappedMessage) {
    if (!this.stopped) {
      try {
        ConsensusOutcome outcome = this.raftMachine.handle(wrappedMessage.message());
        if (outcome.snapshotRequirement().isPresent()) {
          SnapshotRequirement snapshotRequirement = outcome.snapshotRequirement().get();
          this.log.info(String.format("Scheduling download because of %s", snapshotRequirement));
          Optional<JobHandle> downloadJob = this.downloadService
              .scheduleDownload(this.catchupAddressProvider);
          if (downloadJob.isPresent()) {
            downloadJob.get().waitTermination();
          }
        } else {
          this.notifyCommitted(outcome.getCommitIndex());
        }
      } catch (Throwable n5) {
        this.log.error("Error handling message", n5);
        this.panicker.panic(n5);
        this.stop();
      }
    }
  }

  public synchronized void start(RaftId raftId) {
    this.stopped = false;
  }

  public synchronized void stop() {
    this.stopped = true;
  }

  private void notifyCommitted(long commitIndex) {
    this.applicationProcess.notifyCommitted(commitIndex);
  }
}
