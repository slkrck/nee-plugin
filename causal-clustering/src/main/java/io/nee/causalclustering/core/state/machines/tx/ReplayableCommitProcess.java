/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.tx;

import java.util.concurrent.atomic.AtomicLong;
import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionToApply;
import org.neo4j.kernel.impl.transaction.tracing.CommitEvent;
import org.neo4j.storageengine.api.TransactionApplicationMode;

class ReplayableCommitProcess implements TransactionCommitProcess {

  private final AtomicLong lastLocalTxId = new AtomicLong(1L);
  private final TransactionCommitProcess localCommitProcess;
  private final TransactionCounter transactionCounter;

  ReplayableCommitProcess(TransactionCommitProcess localCommitProcess,
      TransactionCounter transactionCounter) {
    this.localCommitProcess = localCommitProcess;
    this.transactionCounter = transactionCounter;
  }

  public long commit(TransactionToApply batch, CommitEvent commitEvent,
      TransactionApplicationMode mode) throws TransactionFailureException {
    long txId = this.lastLocalTxId.incrementAndGet();
    return txId > this.transactionCounter.lastCommittedTransactionId() ? this.localCommitProcess
        .commit(batch, commitEvent, mode) : txId;
  }
}
