/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.protocol.Protocol;
import io.nee.causalclustering.protocol.application.ApplicationProtocolCategory;
import io.nee.causalclustering.protocol.application.ApplicationProtocolVersion;
import io.nee.causalclustering.protocol.application.ApplicationProtocols;
import io.nee.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import io.nee.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import io.nee.causalclustering.protocol.modifier.ModifierProtocolCategory;
import io.nee.causalclustering.protocol.modifier.ModifierProtocols;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.configuration.Config;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class SupportedProtocolCreator {

  private final Config config;
  private final Log log;

  public SupportedProtocolCreator(Config config, LogProvider logProvider) {
    this.config = config;
    this.log = logProvider.getLog(this.getClass());
  }

  public ApplicationSupportedProtocols getSupportedCatchupProtocolsFromConfiguration() {
    return this.getApplicationSupportedProtocols(
        this.config.get(CausalClusteringSettings.catchup_implementations),
        ApplicationProtocolCategory.CATCHUP);
  }

  public ApplicationSupportedProtocols getSupportedRaftProtocolsFromConfiguration() {
    return this.getApplicationSupportedProtocols(
        this.config.get(CausalClusteringSettings.raft_implementations),
        ApplicationProtocolCategory.RAFT);
  }

  private ApplicationSupportedProtocols getApplicationSupportedProtocols(
      List<ApplicationProtocolVersion> configVersions,
      ApplicationProtocolCategory category) {
    if (configVersions.isEmpty()) {
      return new ApplicationSupportedProtocols(category, Collections.emptyList());
    } else {
      List<ApplicationProtocolVersion> knownVersions = this
          .protocolsForConfig(category, configVersions, (version) ->
          {
            return ApplicationProtocols.find(category, version);
          });
      if (knownVersions.isEmpty()) {
        throw new IllegalArgumentException(
            String.format("None of configured %s implementations %s are known",
                category.canonicalName(), configVersions));
      } else {
        return new ApplicationSupportedProtocols(category, knownVersions);
      }
    }
  }

  public List<ModifierSupportedProtocols> createSupportedModifierProtocols() {
    ModifierSupportedProtocols supportedCompression = this.compressionProtocolVersions();
    return Stream.of(supportedCompression).filter((supportedProtocols) ->
    {
      return !supportedProtocols.versions().isEmpty();
    }).collect(Collectors.toList());
  }

  private ModifierSupportedProtocols compressionProtocolVersions() {
    List<String> implementations =
        this.protocolsForConfig(ModifierProtocolCategory.COMPRESSION,
            (List) this.config.get(CausalClusteringSettings.compression_implementations),
            (implementation) ->
            {
              return ModifierProtocols.find(ModifierProtocolCategory.COMPRESSION, implementation);
            });
    return new ModifierSupportedProtocols(ModifierProtocolCategory.COMPRESSION, implementations);
  }

  private <IMPL extends Comparable<IMPL>, T extends Protocol<IMPL>> List<IMPL> protocolsForConfig(
      Protocol.Category<T> category, List<IMPL> implementations,
      Function<IMPL, Optional<T>> finder) {
    return implementations.stream().map((impl) ->
    {
      return Pair.of(impl, finder.apply(impl));
    }).peek((protocolWithImplementation) ->
    {
      this.logUnknownProtocol(category, protocolWithImplementation);
    }).map(Pair::other).flatMap(Optional::stream).map(Protocol::implementation)
        .collect(Collectors.toList());
  }

  private <IMPL extends Comparable<IMPL>, T extends Protocol<IMPL>> void logUnknownProtocol(
      Protocol.Category<T> category,
      Pair<IMPL, Optional<T>> protocolWithImplementation) {
    if (protocolWithImplementation.other().isEmpty()) {
      this.log.warn("Configured %s protocol implementation %s unknown. Ignoring.", category,
          protocolWithImplementation.first());
    }
  }
}
