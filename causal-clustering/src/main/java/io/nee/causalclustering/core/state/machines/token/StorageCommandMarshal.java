/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.token;

import io.nee.causalclustering.messaging.BoundedNetworkWritableChannel;
import io.nee.causalclustering.messaging.NetworkReadableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryCommand;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryReader;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryWriter;
import org.neo4j.kernel.impl.transaction.log.entry.VersionAwareLogEntryReader;
import org.neo4j.storageengine.api.StorageCommand;

public class StorageCommandMarshal {

  public static byte[] commandsToBytes(Collection<StorageCommand> commands) {
    ByteBuf commandBuffer = Unpooled.buffer();
    BoundedNetworkWritableChannel channel = new BoundedNetworkWritableChannel(commandBuffer);

    try {
      (new LogEntryWriter(channel)).serialize(commands);
    } catch (IOException n4) {
      throw new IllegalStateException("This should not be possible since it is all in memory.");
    }

    byte[] commandsBytes = Arrays.copyOf(commandBuffer.array(), commandBuffer.writerIndex());
    commandBuffer.release();
    return commandsBytes;
  }

  static Collection<StorageCommand> bytesToCommands(byte[] commandBytes) {
    ByteBuf txBuffer = Unpooled.wrappedBuffer(commandBytes);
    NetworkReadableChannel channel = new NetworkReadableChannel(txBuffer);
    LogEntryReader reader = new VersionAwareLogEntryReader();
    LinkedList commands = new LinkedList();

    try {
      LogEntryCommand entryRead;
      while ((entryRead = (LogEntryCommand) reader.readLogEntry(channel)) != null) {
        commands.add(entryRead.getCommand());
      }

      return commands;
    } catch (IOException n7) {
      throw new IllegalStateException("This should not be possible since it is all in memory.");
    }
  }
}
