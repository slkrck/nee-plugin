/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.snapshot;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.catchup.CatchupComponentsRepository;
import io.nee.causalclustering.catchup.storecopy.DatabaseShutdownException;
import io.nee.causalclustering.catchup.storecopy.RemoteStore;
import io.nee.causalclustering.catchup.storecopy.StoreCopyFailedException;
import io.nee.causalclustering.catchup.storecopy.StoreCopyProcess;
import io.nee.causalclustering.catchup.storecopy.StoreIdDownloadFailedException;
import java.io.IOException;
import java.util.Optional;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StoreId;

public class StoreDownloader {

  private final CatchupComponentsRepository componentsRepo;
  private final Log log;

  public StoreDownloader(CatchupComponentsRepository componentsRepo, LogProvider logProvider) {
    this.componentsRepo = componentsRepo;
    this.log = logProvider.getLog(this.getClass());
  }

  boolean bringUpToDate(StoreDownloadContext context, SocketAddress primaryAddress,
      CatchupAddressProvider addressProvider)
      throws IOException, DatabaseShutdownException {
    CatchupComponentsRepository.CatchupComponents components = this
        .getCatchupComponents(context.databaseId());
    Optional<StoreId> validStoreId = this
        .validateStoreId(context, components.remoteStore(), primaryAddress);
    if (validStoreId.isEmpty()) {
      return false;
    } else {
      if (!context.isEmpty()) {
        if (this.tryCatchup(context, addressProvider, components.remoteStore())) {
          return true;
        }

        context.delete();
      }

      return this
          .replaceWithStore(validStoreId.get(), addressProvider, components.storeCopyProcess());
    }
  }

  private Optional<StoreId> validateStoreId(StoreDownloadContext context, RemoteStore remoteStore,
      SocketAddress address) {
    StoreId remoteStoreId;
    try {
      remoteStoreId = remoteStore.getStoreId(address);
    } catch (StoreIdDownloadFailedException n6) {
      this.log.warn("Store copy failed", n6);
      return Optional.empty();
    }

    if (!context.isEmpty() && !remoteStoreId.equals(context.storeId())) {
      this.log.error("Store copy failed due to store ID mismatch");
      return Optional.empty();
    } else {
      return Optional.ofNullable(remoteStoreId);
    }
  }

  private boolean tryCatchup(StoreDownloadContext context, CatchupAddressProvider addressProvider,
      RemoteStore remoteStore) throws IOException {
    try {
      remoteStore.tryCatchingUp(addressProvider, context.storeId(), context.databaseLayout(), false,
          false);
      return true;
    } catch (StoreCopyFailedException n5) {
      this.log.warn("Failed to catch up", n5);
      return false;
    }
  }

  private boolean replaceWithStore(StoreId remoteStoreId, CatchupAddressProvider addressProvider,
      StoreCopyProcess storeCopy)
      throws IOException, DatabaseShutdownException {
    try {
      storeCopy.replaceWithStoreFrom(addressProvider, remoteStoreId);
      return true;
    } catch (StoreCopyFailedException n5) {
      this.log.warn("Failed to copy and replace store", n5);
      return false;
    }
  }

  private CatchupComponentsRepository.CatchupComponents getCatchupComponents(
      NamedDatabaseId namedDatabaseId) {
    return this.componentsRepo.componentsFor(namedDatabaseId).orElseThrow(() ->
    {
      return new IllegalStateException(
          String.format(
              "There are no catchup components for the database %s.",
              namedDatabaseId));
    });
  }
}
