/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class InMemoryRaftLog implements RaftLog {

  private final Map<Long, RaftLogEntry> raftLog = new HashMap();
  private final long commitIndex = -1L;
  private long prevIndex = -1L;
  private long prevTerm = -1L;
  private long appendIndex = -1L;
  private long term = -1L;

  public synchronized long append(RaftLogEntry... entries) {
    long newAppendIndex = this.appendIndex;
    RaftLogEntry[] n4 = entries;
    int n5 = entries.length;

    for (int n6 = 0; n6 < n5; ++n6) {
      RaftLogEntry entry = n4[n6];
      newAppendIndex = this.appendSingle(entry);
    }

    return newAppendIndex;
  }

  private synchronized long appendSingle(RaftLogEntry logEntry) {
    Objects.requireNonNull(logEntry);
    if (logEntry.term() >= this.term) {
      this.term = logEntry.term();
      ++this.appendIndex;
      this.raftLog.put(this.appendIndex, logEntry);
      return this.appendIndex;
    } else {
      throw new IllegalStateException(
          String.format("Non-monotonic term %d for in entry %s in term %d", logEntry.term(),
              logEntry.toString(), this.term));
    }
  }

  public synchronized long prune(long safeIndex) {
    if (safeIndex > this.prevIndex) {
      long removeIndex = this.prevIndex + 1L;
      this.prevTerm = this.readEntryTerm(safeIndex);
      this.prevIndex = safeIndex;

      do {
        this.raftLog.remove(removeIndex);
        ++removeIndex;
      }
      while (removeIndex <= safeIndex);
    }

    return this.prevIndex;
  }

  public synchronized long appendIndex() {
    return this.appendIndex;
  }

  public synchronized long prevIndex() {
    return this.prevIndex;
  }

  public synchronized long readEntryTerm(long logIndex) {
    if (logIndex == this.prevIndex) {
      return this.prevTerm;
    } else {
      return logIndex >= this.prevIndex && logIndex <= this.appendIndex ? this.raftLog.get(logIndex)
          .term() : -1L;
    }
  }

  public synchronized void truncate(long fromIndex) {
    if (fromIndex <= this.commitIndex) {
      throw new IllegalArgumentException("cannot truncate before the commit index");
    } else if (fromIndex > this.appendIndex) {
      throw new IllegalArgumentException(
          "Cannot truncate at index " + fromIndex + " when append index is " + this.appendIndex);
    } else {
      if (fromIndex <= this.prevIndex) {
        this.prevIndex = -1L;
        this.prevTerm = -1L;
      }

      for (long i = this.appendIndex; i >= fromIndex; --i) {
        this.raftLog.remove(i);
      }

      if (this.appendIndex >= fromIndex) {
        this.appendIndex = fromIndex - 1L;
      }

      this.term = this.readEntryTerm(this.appendIndex);
    }
  }

  public synchronized RaftLogCursor getEntryCursor(final long fromIndex) {
    return new RaftLogCursor() {
      RaftLogEntry current;
      private long currentIndex = fromIndex - 1L;

      public boolean next() {
        ++this.currentIndex;
        synchronized (InMemoryRaftLog.this) {
          boolean hasNext = this.currentIndex <= InMemoryRaftLog.this.appendIndex;
          if (hasNext) {
            if (this.currentIndex <= InMemoryRaftLog.this.prevIndex
                || this.currentIndex > InMemoryRaftLog.this.appendIndex) {
              return false;
            }

            this.current = InMemoryRaftLog.this.raftLog.get(this.currentIndex);
          } else {
            this.current = null;
          }

          return hasNext;
        }
      }

      public void close() {
      }

      public long index() {
        return this.currentIndex;
      }

      public RaftLogEntry get() {
        return this.current;
      }
    };
  }

  public synchronized long skip(long index, long term) {
    if (index > this.appendIndex) {
      this.raftLog.clear();
      this.appendIndex = index;
      this.prevIndex = index;
      this.prevTerm = term;
    }

    return this.appendIndex;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      InMemoryRaftLog that = (InMemoryRaftLog) o;
      return Objects.equals(this.appendIndex, that.appendIndex) && Objects
          .equals(this.commitIndex, that.commitIndex) &&
          Objects.equals(this.term, that.term) && Objects.equals(this.raftLog, that.raftLog);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.raftLog, this.appendIndex, this.commitIndex, this.term);
  }
}
