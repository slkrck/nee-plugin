/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.vote;

import io.nee.causalclustering.core.state.storage.SafeStateMarshal;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.IOException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class VoteState {

  private MemberId votedFor;
  private long term = -1L;

  public VoteState() {
  }

  private VoteState(MemberId votedFor, long term) {
    this.term = term;
    this.votedFor = votedFor;
  }

  public MemberId votedFor() {
    return this.votedFor;
  }

  public boolean update(MemberId votedFor, long term) {
    if (this.termChanged(term)) {
      this.votedFor = votedFor;
      this.term = term;
      return true;
    } else {
      if (this.votedFor == null) {
        if (votedFor != null) {
          this.votedFor = votedFor;
          return true;
        }
      } else if (!this.votedFor.equals(votedFor)) {
        throw new IllegalArgumentException("Can only vote once per term.");
      }

      return false;
    }
  }

  private boolean termChanged(long term) {
    return term != this.term;
  }

  public long term() {
    return this.term;
  }

  public String toString() {
    return "VoteState{votedFor=" + this.votedFor + ", term=" + this.term + "}";
  }

  public static class Marshal extends SafeStateMarshal<VoteState> {

    private final ChannelMarshal<MemberId> memberMarshal;

    public Marshal() {
      this.memberMarshal = MemberId.Marshal.INSTANCE;
    }

    public void marshal(VoteState state, WritableChannel channel) throws IOException {
      channel.putLong(state.term);
      this.memberMarshal.marshal(state.votedFor(), channel);
    }

    public VoteState unmarshal0(ReadableChannel channel) throws IOException, EndOfStreamException {
      long term = channel.getLong();
      MemberId member = this.memberMarshal.unmarshal(channel);
      return new VoteState(member, term);
    }

    public VoteState startState() {
      return new VoteState();
    }

    public long ordinal(VoteState state) {
      return state.term();
    }
  }
}
