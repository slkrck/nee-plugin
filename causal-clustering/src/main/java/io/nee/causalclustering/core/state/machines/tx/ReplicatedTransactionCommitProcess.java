/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.tx;

import io.nee.causalclustering.core.replication.ReplicationResult;
import io.nee.causalclustering.core.replication.Replicator;
import io.nee.causalclustering.core.state.machines.lease.ClusterLeaseCoordinator;
import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.kernel.api.exceptions.Status.Cluster;
import org.neo4j.kernel.api.exceptions.Status.General;
import org.neo4j.kernel.api.exceptions.Status.Transaction;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionToApply;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;
import org.neo4j.kernel.impl.transaction.tracing.CommitEvent;
import org.neo4j.storageengine.api.TransactionApplicationMode;

public class ReplicatedTransactionCommitProcess implements TransactionCommitProcess {

  private final Replicator replicator;
  private final NamedDatabaseId namedDatabaseId;
  private final ClusterLeaseCoordinator leaseCoordinator;

  public ReplicatedTransactionCommitProcess(Replicator replicator, NamedDatabaseId namedDatabaseId,
      ClusterLeaseCoordinator leaseCoordinator) {
    this.replicator = replicator;
    this.namedDatabaseId = namedDatabaseId;
    this.leaseCoordinator = leaseCoordinator;
  }

  public long commit(TransactionToApply tx, CommitEvent commitEvent,
      TransactionApplicationMode mode) throws TransactionFailureException {
    TransactionRepresentation txRepresentation = tx.transactionRepresentation();
    int leaseId = txRepresentation.getLeaseId();
    if (leaseId != -1 && this.leaseCoordinator.isInvalid(leaseId)) {
      throw new TransactionFailureException(Transaction.LeaseExpired,
          "The lease has been invalidated");
    } else {
      TransactionRepresentationReplicatedTransaction transaction = ReplicatedTransaction
          .from(txRepresentation, this.namedDatabaseId);

      ReplicationResult replicationResult;
      try {
        replicationResult = this.replicator.replicate(transaction);
      } catch (Throwable n11) {
        this.leaseCoordinator.invalidateLease(leaseId);
        throw new TransactionFailureException(Cluster.ReplicationFailure, n11);
      }

      switch (replicationResult.outcome()) {
        case MAYBE_REPLICATED:
          this.leaseCoordinator.invalidateLease(leaseId);
        case NOT_REPLICATED:
          throw new TransactionFailureException(Cluster.ReplicationFailure,
              replicationResult.failure());
        case APPLIED:
          try {
            return (Long) replicationResult.stateMachineResult().consume();
          } catch (TransactionFailureException n9) {
            throw n9;
          } catch (Throwable n10) {
            throw new TransactionFailureException(General.UnknownError, "Unexpected exception",
                n10);
          }
        default:
          throw new TransactionFailureException(General.UnknownError,
              "Unexpected outcome: " + replicationResult.outcome());
      }
    }
  }
}
