/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.core.TempBootstrapDir;
import io.nee.causalclustering.core.consensus.membership.MembershipEntry;
import io.nee.causalclustering.core.replication.session.GlobalSessionTrackerState;
import io.nee.causalclustering.core.state.machines.lease.ReplicatedLeaseState;
import io.nee.causalclustering.core.state.machines.tx.LogIndexTxHeaderEncoding;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshot;
import io.nee.causalclustering.core.state.snapshot.RaftCoreState;
import io.nee.causalclustering.helper.TemporaryDatabase;
import io.nee.causalclustering.helper.TemporaryDatabaseFactory;
import io.nee.causalclustering.identity.MemberId;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.database.DatabasePageCache;
import org.neo4j.graphdb.factory.module.DatabaseInitializer;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.io.pagecache.tracing.cursor.context.EmptyVersionContextSupplier;
import org.neo4j.kernel.impl.store.MetaDataStore;
import org.neo4j.kernel.impl.transaction.log.FlushablePositionAwareChecksumChannel;
import org.neo4j.kernel.impl.transaction.log.LogPositionMarker;
import org.neo4j.kernel.impl.transaction.log.PhysicalTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.TransactionLogWriter;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryWriter;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.kernel.impl.transaction.log.files.LogFilesBuilder;
import org.neo4j.kernel.lifecycle.Lifespan;
import org.neo4j.kernel.recovery.Recovery;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.storageengine.api.StoreId;
import org.neo4j.storageengine.api.TransactionIdStore;
import org.neo4j.storageengine.api.TransactionMetaDataStore;

public class RaftBootstrapper {

  private static final long FIRST_INDEX = 0L;
  private static final long FIRST_TERM = 0L;
  private final BootstrapContext bootstrapContext;
  private final TemporaryDatabaseFactory tempDatabaseFactory;
  private final DatabaseInitializer databaseInitializer;
  private final PageCache pageCache;
  private final FileSystemAbstraction fs;
  private final Log log;
  private final StorageEngineFactory storageEngineFactory;
  private final Config config;
  private final BootstrapSaver bootstrapSaver;

  public RaftBootstrapper(BootstrapContext bootstrapContext,
      TemporaryDatabaseFactory tempDatabaseFactory, DatabaseInitializer databaseInitializer,
      PageCache pageCache, FileSystemAbstraction fs, LogProvider logProvider,
      StorageEngineFactory storageEngineFactory, Config config,
      BootstrapSaver bootstrapSaver) {
    this.bootstrapContext = bootstrapContext;
    this.tempDatabaseFactory = tempDatabaseFactory;
    this.databaseInitializer = databaseInitializer;
    this.pageCache = pageCache;
    this.fs = fs;
    this.log = logProvider.getLog(this.getClass());
    this.storageEngineFactory = storageEngineFactory;
    this.config = config;
    this.bootstrapSaver = bootstrapSaver;
  }

  public CoreSnapshot bootstrap(Set<MemberId> members) {
    return this.bootstrap(members, null);
  }

  public CoreSnapshot bootstrap(Set<MemberId> members, StoreId storeId) {
    try {
      Log n10000 = this.log;
      String n10001 = this.bootstrapContext.databaseId().name();
      n10000.info("Bootstrapping database " + n10001 + " for members " + members);
      if (this.isStorePresent()) {
        this.ensureRecoveredOrThrow(this.bootstrapContext, this.config);
        if (this.bootstrapContext.databaseId().isSystemDatabase()) {
          this.bootstrapExistingSystemDatabase();
        }
      } else {
        this.createStore(storeId);
      }

      this.appendNullTransactionLogEntryToSetRaftIndexToMinusOne(this.bootstrapContext);
      CoreSnapshot snapshot = this.buildCoreSnapshot(members);
      n10000 = this.log;
      n10001 = this.bootstrapContext.databaseId().name();
      n10000.info("Bootstrapping of the database " + n10001 + " completed " + snapshot);
      return snapshot;
    } catch (Exception n4) {
      throw new BootstrapException(this.bootstrapContext.databaseId(), n4);
    }
  }

  public void saveStore() throws IOException {
    DatabaseLayout databaseLayout = this.bootstrapContext.databaseLayout();
    this.bootstrapSaver.save(databaseLayout);
  }

  private void bootstrapExistingSystemDatabase() throws IOException {
    TempBootstrapDir.Resource bootstrapRootDir = TempBootstrapDir
        .cleanBeforeAndAfter(this.fs, this.bootstrapContext.databaseLayout());

    try {
      File tempDefaultDatabaseDir = new File(bootstrapRootDir.get(), "neo4j");
      this.fs.copyRecursively(this.bootstrapContext.databaseLayout().databaseDirectory(),
          tempDefaultDatabaseDir);
      this.fs.copyRecursively(this.bootstrapContext.databaseLayout().getTransactionLogsDirectory(),
          tempDefaultDatabaseDir);
      DatabaseLayout tempDatabaseLayout = this
          .initializeStoreUsingTempDatabase(bootstrapRootDir.get());
      this.bootstrapContext.replaceWith(tempDatabaseLayout.databaseDirectory());
    } catch (Throwable n5) {
      if (bootstrapRootDir != null) {
        try {
          bootstrapRootDir.close();
        } catch (Throwable n4) {
          n5.addSuppressed(n4);
        }
      }

      throw n5;
    }

    if (bootstrapRootDir != null) {
      bootstrapRootDir.close();
    }
  }

  private boolean isStorePresent() {
    return this.storageEngineFactory
        .storageExists(this.fs, this.bootstrapContext.databaseLayout(), this.pageCache);
  }

  private void createStore(StoreId storeId) throws IOException {
    TempBootstrapDir.Resource bootstrapRootDir = TempBootstrapDir
        .cleanBeforeAndAfter(this.fs, this.bootstrapContext.databaseLayout());

    try {
      String databaseName = this.bootstrapContext.databaseId().name();
      this.log.info(
          "Initializing the store for database " + databaseName + " using a temporary database in "
              + bootstrapRootDir);
      DatabaseLayout bootstrapDatabaseLayout = this
          .initializeStoreUsingTempDatabase(bootstrapRootDir.get());
      if (storeId != null) {
        this.log.info("Changing store ID of bootstrapped database to " + storeId);
        MetaDataStore.setStoreId(this.pageCache, bootstrapDatabaseLayout.metadataStore(), storeId,
            -559063315L, 0L);
      }

      this.log.info("Moving created store files from " + bootstrapDatabaseLayout + " to "
          + this.bootstrapContext.databaseLayout());
      this.bootstrapContext.replaceWith(bootstrapDatabaseLayout.databaseDirectory());
      this.bootstrapContext.removeTransactionLogs();
    } catch (Throwable n6) {
      if (bootstrapRootDir != null) {
        try {
          bootstrapRootDir.close();
        } catch (Throwable n5) {
          n6.addSuppressed(n5);
        }
      }

      throw n6;
    }

    if (bootstrapRootDir != null) {
      bootstrapRootDir.close();
    }
  }

  private DatabaseLayout initializeStoreUsingTempDatabase(File bootstrapRootDir) {
    TemporaryDatabase tempDatabase = this.tempDatabaseFactory
        .startTemporaryDatabase(bootstrapRootDir, this.config);

    DatabaseLayout databaseLayout;
    try {
      this.databaseInitializer.initialize(tempDatabase.graphDatabaseService());
      databaseLayout = tempDatabase.defaultDatabaseDirectory();
    } catch (Throwable n7) {
      if (tempDatabase != null) {
        try {
          tempDatabase.close();
        } catch (Throwable n6) {
          n7.addSuppressed(n6);
        }
      }

      throw n7;
    }

    if (tempDatabase != null) {
      tempDatabase.close();
    }

    return databaseLayout;
  }

  private void ensureRecoveredOrThrow(BootstrapContext bootstrapContext, Config config)
      throws Exception {
    if (Recovery.isRecoveryRequired(this.fs, bootstrapContext.databaseLayout(), config)) {
      String message = "Cannot bootstrap database " + bootstrapContext.databaseId().name() +
          ". Recovery is required. Please ensure that the store being seeded comes from a cleanly shutdown instance of Neo4j or a Neo4j backup";
      this.log.error(message);
      throw new IllegalStateException(message);
    }
  }

  private CoreSnapshot buildCoreSnapshot(Set<MemberId> members) {
    RaftCoreState raftCoreState = new RaftCoreState(new MembershipEntry(0L, members));
    GlobalSessionTrackerState sessionTrackerState = new GlobalSessionTrackerState();
    CoreSnapshot coreSnapshot = new CoreSnapshot(0L, 0L);
    coreSnapshot.add(CoreStateFiles.RAFT_CORE_STATE, raftCoreState);
    coreSnapshot.add(CoreStateFiles.SESSION_TRACKER, sessionTrackerState);
    coreSnapshot.add(CoreStateFiles.LEASE, ReplicatedLeaseState.INITIAL_LEASE_STATE);
    return coreSnapshot;
  }

  private void appendNullTransactionLogEntryToSetRaftIndexToMinusOne(
      BootstrapContext bootstrapContext) throws IOException {
    DatabaseLayout layout = bootstrapContext.databaseLayout();
    DatabasePageCache databasePageCache = new DatabasePageCache(this.pageCache,
        EmptyVersionContextSupplier.EMPTY);

    try {
      StoreId storeId = this.storageEngineFactory.storeId(layout, this.pageCache);
      TransactionIdStore readOnlyTransactionIdStore = this.storageEngineFactory
          .readOnlyTransactionIdStore(this.fs, layout, databasePageCache);
      LogFiles logFiles = LogFilesBuilder.activeFilesBuilder(layout, this.fs, databasePageCache)
          .withConfig(this.config).withStoreId(
              storeId).withLastCommittedTransactionIdSupplier(() ->
          {
            return readOnlyTransactionIdStore.getLastClosedTransactionId() - 1L;
          }).build();
      LogPositionMarker logPositionMarker = new LogPositionMarker();
      Lifespan ignored = new Lifespan(logFiles);

      long dummyTransactionId;
      try {
        FlushablePositionAwareChecksumChannel channel = logFiles.getLogFile().getWriter();
        TransactionLogWriter writer = new TransactionLogWriter(new LogEntryWriter(channel));
        long lastCommittedTransactionId = readOnlyTransactionIdStore
            .getLastCommittedTransactionId();
        PhysicalTransactionRepresentation tx = new PhysicalTransactionRepresentation(
            Collections.emptyList());
        byte[] txHeaderBytes = LogIndexTxHeaderEncoding.encodeLogIndexAsTxHeader(-1L);
        tx.setHeader(txHeaderBytes, -1L, lastCommittedTransactionId, -1L, -1);
        dummyTransactionId = lastCommittedTransactionId + 1L;
        channel.getCurrentPosition(logPositionMarker);
        writer.append(tx, dummyTransactionId, -559063315);
        channel.prepareForFlush().flush();
      } catch (Throwable n20) {
        try {
          ignored.close();
        } catch (Throwable n18) {
          n20.addSuppressed(n18);
        }

        throw n20;
      }

      ignored.close();
      TransactionMetaDataStore transactionMetaDataStore =
          this.storageEngineFactory
              .transactionMetaDataStore(this.fs, layout, this.config, databasePageCache);

      try {
        transactionMetaDataStore.setLastCommittedAndClosedTransactionId(dummyTransactionId, 0,
            System.currentTimeMillis(),
            logPositionMarker.getByteOffset(), logPositionMarker.getLogVersion());
      } catch (Throwable n21) {
        if (transactionMetaDataStore != null) {
          try {
            transactionMetaDataStore.close();
          } catch (Throwable n19) {
            n21.addSuppressed(n19);
          }
        }

        throw n21;
      }

      if (transactionMetaDataStore != null) {
        transactionMetaDataStore.close();
      }
    } catch (Throwable n22) {
      try {
        databasePageCache.close();
      } catch (Throwable n17) {
        n22.addSuppressed(n17);
      }

      throw n22;
    }

    databasePageCache.close();
  }
}
