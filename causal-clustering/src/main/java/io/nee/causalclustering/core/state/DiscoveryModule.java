/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.discovery.CoreTopologyService;
import io.nee.causalclustering.discovery.DiscoveryServiceFactory;
import io.nee.causalclustering.discovery.RemoteMembersResolver;
import io.nee.causalclustering.discovery.ResolutionResolverFactory;
import io.nee.causalclustering.discovery.RetryStrategy;
import io.nee.causalclustering.discovery.member.DiscoveryMemberFactory;
import io.nee.causalclustering.identity.MemberId;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.ssl.config.SslPolicyLoader;
import org.neo4j.time.SystemNanoClock;

public class DiscoveryModule {

  private final SystemNanoClock clock;
  private final JobScheduler jobScheduler;
  private final LogProvider userLog;
  private final LogProvider debugLog;
  private final CoreTopologyService topologyService;

  public DiscoveryModule(MemberId myself, DiscoveryServiceFactory discoveryServiceFactory,
      DiscoveryMemberFactory discoveryMemberFactory,
      GlobalModule globalModule, SslPolicyLoader sslPolicyLoader) {
    LifeSupport globalLife = globalModule.getGlobalLife();
    Config globalConfig = globalModule.getGlobalConfig();
    LogService logService = globalModule.getLogService();
    Dependencies globalDependencies = globalModule.getGlobalDependencies();
    this.clock = globalModule.getGlobalClock();
    this.jobScheduler = globalModule.getJobScheduler();
    Monitors globalMonitors = globalModule.getGlobalMonitors();
    this.debugLog = logService.getInternalLogProvider();
    this.userLog = logService.getUserLogProvider();
    this.topologyService =
        this.createDiscoveryService(myself, discoveryServiceFactory, discoveryMemberFactory,
            sslPolicyLoader, globalLife, globalConfig, logService,
            globalMonitors, globalDependencies);
  }

  private static RetryStrategy resolveStrategy(Config config) {
    long refreshPeriodMillis = config.get(CausalClusteringSettings.cluster_topology_refresh)
        .toMillis();
    int pollingFrequencyWithinRefreshWindow = 2;
    int numberOfRetries = pollingFrequencyWithinRefreshWindow + 1;
    long delayInMillis = refreshPeriodMillis / (long) pollingFrequencyWithinRefreshWindow;
    return new RetryStrategy(delayInMillis, numberOfRetries);
  }

  private CoreTopologyService createDiscoveryService(MemberId myself,
      DiscoveryServiceFactory discoveryServiceFactory,
      DiscoveryMemberFactory discoveryMemberFactory, SslPolicyLoader sslPolicyLoader,
      LifeSupport life,
      Config config, LogService logService,
      Monitors monitors, Dependencies dependencies) {
    RemoteMembersResolver remoteMembersResolver = ResolutionResolverFactory
        .chooseResolver(config, logService);
    CoreTopologyService topologyService =
        discoveryServiceFactory
            .coreTopologyService(config, myself, this.jobScheduler, this.debugLog, this.userLog,
                remoteMembersResolver,
                resolveStrategy(config), sslPolicyLoader, discoveryMemberFactory, monitors,
                this.clock);
    life.add(topologyService);
    dependencies.satisfyDependency(topologyService);
    return topologyService;
  }

  public CoreTopologyService topologyService() {
    return this.topologyService;
  }
}
