/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.core.consensus.log.RaftLogCursor;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.consensus.log.ReadableRaftLog;
import io.nee.causalclustering.core.consensus.log.cache.InFlightCache;
import java.io.IOException;

public class InFlightLogEntryReader implements AutoCloseable {

  private final ReadableRaftLog raftLog;
  private final InFlightCache inFlightCache;
  private final boolean pruneAfterRead;
  private RaftLogCursor cursor;
  private boolean useCache = true;

  public InFlightLogEntryReader(ReadableRaftLog raftLog, InFlightCache inFlightCache,
      boolean pruneAfterRead) {
    this.raftLog = raftLog;
    this.inFlightCache = inFlightCache;
    this.pruneAfterRead = pruneAfterRead;
  }

  public RaftLogEntry get(long logIndex) throws IOException {
    RaftLogEntry entry = null;
    if (this.useCache) {
      entry = this.inFlightCache.get(logIndex);
    } else {
      this.inFlightCache.reportSkippedCacheAccess();
    }

    if (entry == null) {
      this.useCache = false;
      entry = this.getUsingCursor(logIndex);
    }

    if (this.pruneAfterRead) {
      this.inFlightCache.prune(logIndex);
    }

    return entry;
  }

  private RaftLogEntry getUsingCursor(long logIndex) throws IOException {
    if (this.cursor == null) {
      this.cursor = this.raftLog.getEntryCursor(logIndex);
    }

    if (this.cursor.next()) {
      if (this.cursor.index() != logIndex) {
        throw new IllegalStateException(
            String.format("expected index %d but was %s", logIndex, this.cursor.index()));
      } else {
        return this.cursor.get();
      }
    } else {
      return null;
    }
  }

  public void close() throws IOException {
    if (this.cursor != null) {
      this.cursor.close();
    }
  }
}
