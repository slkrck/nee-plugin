/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.replication.session;

import java.util.ArrayDeque;
import java.util.Deque;

public class LocalSessionPool {

  private final Deque<LocalSession> sessionStack = new ArrayDeque();
  private final GlobalSession globalSession;
  private long nextLocalSessionId;

  public LocalSessionPool(GlobalSession globalSession) {
    this.globalSession = globalSession;
  }

  private LocalSession createSession() {
    return new LocalSession(this.nextLocalSessionId++);
  }

  public GlobalSession getGlobalSession() {
    return this.globalSession;
  }

  public synchronized OperationContext acquireSession() {
    LocalSession localSession =
        this.sessionStack.isEmpty() ? this.createSession() : this.sessionStack.pop();
    return new OperationContext(this.globalSession, localSession.nextOperationId(), localSession);
  }

  public synchronized void releaseSession(OperationContext operationContext) {
    this.sessionStack.push(operationContext.localSession());
  }

  public synchronized long openSessionCount() {
    return this.nextLocalSessionId - (long) this.sessionStack.size();
  }
}
