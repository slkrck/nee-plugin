/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus;

import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.ComposableMessageHandler;
import io.nee.causalclustering.messaging.LifecycleMessageHandler;
import java.util.function.LongSupplier;

public class LeaderAvailabilityHandler implements
    LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> {

  private final LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> delegateHandler;
  private final LeaderAvailabilityTimers leaderAvailabilityTimers;
  private final LeaderAvailabilityHandler.ShouldRenewElectionTimeout shouldRenewElectionTimeout;
  private final RaftMessageTimerResetMonitor raftMessageTimerResetMonitor;

  public LeaderAvailabilityHandler(
      LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> delegateHandler,
      LeaderAvailabilityTimers leaderAvailabilityTimers,
      RaftMessageTimerResetMonitor raftMessageTimerResetMonitor,
      LongSupplier term) {
    this.delegateHandler = delegateHandler;
    this.leaderAvailabilityTimers = leaderAvailabilityTimers;
    this.shouldRenewElectionTimeout = new LeaderAvailabilityHandler.ShouldRenewElectionTimeout(
        term);
    this.raftMessageTimerResetMonitor = raftMessageTimerResetMonitor;
  }

  public static ComposableMessageHandler composable(
      LeaderAvailabilityTimers leaderAvailabilityTimers,
      RaftMessageTimerResetMonitor raftMessageTimerResetMonitor, LongSupplier term) {
    return (delegate) ->
    {
      return new LeaderAvailabilityHandler(delegate, leaderAvailabilityTimers,
          raftMessageTimerResetMonitor, term);
    };
  }

  public synchronized void start(RaftId raftId) throws Exception {
    this.delegateHandler.start(raftId);
  }

  public synchronized void stop() throws Exception {
    this.delegateHandler.stop();
  }

  public void handle(RaftMessages.ReceivedInstantRaftIdAwareMessage<?> message) {
    this.handleTimeouts(message);
    this.delegateHandler.handle(message);
  }

  private void handleTimeouts(RaftMessages.ReceivedInstantRaftIdAwareMessage<?> message) {
    if (message.dispatch(this.shouldRenewElectionTimeout)) {
      this.raftMessageTimerResetMonitor.timerReset();
      this.leaderAvailabilityTimers.renewElection();
    }
  }

  private static class ShouldRenewElectionTimeout implements
      RaftMessages.Handler<Boolean, RuntimeException> {

    private final LongSupplier term;

    private ShouldRenewElectionTimeout(LongSupplier term) {
      this.term = term;
    }

    public Boolean handle(RaftMessages.AppendEntries.Request request) {
      return request.leaderTerm() >= this.term.getAsLong();
    }

    public Boolean handle(RaftMessages.Heartbeat heartbeat) {
      return heartbeat.leaderTerm() >= this.term.getAsLong();
    }

    public Boolean handle(RaftMessages.Vote.Request request) {
      return Boolean.FALSE;
    }

    public Boolean handle(RaftMessages.Vote.Response response) {
      return Boolean.FALSE;
    }

    public Boolean handle(RaftMessages.PreVote.Request request) {
      return Boolean.FALSE;
    }

    public Boolean handle(RaftMessages.PreVote.Response response) {
      return Boolean.FALSE;
    }

    public Boolean handle(RaftMessages.AppendEntries.Response response) {
      return Boolean.FALSE;
    }

    public Boolean handle(RaftMessages.LogCompactionInfo logCompactionInfo) {
      return Boolean.FALSE;
    }

    public Boolean handle(RaftMessages.HeartbeatResponse heartbeatResponse) {
      return Boolean.FALSE;
    }

    public Boolean handle(RaftMessages.Timeout.Election election) {
      return Boolean.FALSE;
    }

    public Boolean handle(RaftMessages.Timeout.Heartbeat heartbeat) {
      return Boolean.FALSE;
    }

    public Boolean handle(RaftMessages.NewEntry.Request request) {
      return Boolean.FALSE;
    }

    public Boolean handle(RaftMessages.NewEntry.BatchRequest batchRequest) {
      return Boolean.FALSE;
    }

    public Boolean handle(RaftMessages.PruneRequest pruneRequest) {
      return Boolean.FALSE;
    }
  }
}
