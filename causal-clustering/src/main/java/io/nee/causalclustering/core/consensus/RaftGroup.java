/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus;

import io.nee.causalclustering.common.RaftLogImplementation;
import io.nee.causalclustering.common.state.ClusterStateStorageFactory;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.core.consensus.log.InMemoryRaftLog;
import io.nee.causalclustering.core.consensus.log.MonitoredRaftLog;
import io.nee.causalclustering.core.consensus.log.RaftLog;
import io.nee.causalclustering.core.consensus.log.cache.InFlightCache;
import io.nee.causalclustering.core.consensus.log.cache.InFlightCacheFactory;
import io.nee.causalclustering.core.consensus.log.segmented.CoreLogPruningStrategy;
import io.nee.causalclustering.core.consensus.log.segmented.CoreLogPruningStrategyFactory;
import io.nee.causalclustering.core.consensus.log.segmented.SegmentedRaftLog;
import io.nee.causalclustering.core.consensus.membership.MemberIdSetBuilder;
import io.nee.causalclustering.core.consensus.membership.RaftMembershipManager;
import io.nee.causalclustering.core.consensus.membership.RaftMembershipState;
import io.nee.causalclustering.core.consensus.schedule.TimerService;
import io.nee.causalclustering.core.consensus.shipping.RaftLogShippingManager;
import io.nee.causalclustering.core.consensus.state.RaftState;
import io.nee.causalclustering.core.consensus.term.MonitoredTermStateStorage;
import io.nee.causalclustering.core.consensus.term.TermState;
import io.nee.causalclustering.core.consensus.vote.VoteState;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.core.replication.SendToMyself;
import io.nee.causalclustering.core.state.ClusterStateLayout;
import io.nee.causalclustering.core.state.storage.StateStorage;
import io.nee.causalclustering.discovery.CoreTopologyService;
import io.nee.causalclustering.discovery.RaftCoreTopologyConnector;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.Outbound;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import io.nee.causalclustering.messaging.marshalling.CoreReplicatedContentMarshalV2;
import java.io.File;
import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.DatabaseLogProvider;
import org.neo4j.logging.internal.DatabaseLogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.time.Clocks;
import org.neo4j.time.SystemNanoClock;

public class RaftGroup {

  private final MonitoredRaftLog raftLog;
  private final RaftMachine raftMachine;
  private final RaftMembershipManager raftMembershipManager;
  private final InFlightCache inFlightCache;
  private final LeaderAvailabilityTimers leaderAvailabilityTimers;

  RaftGroup(Config config, DatabaseLogService logService, FileSystemAbstraction fileSystem,
      JobScheduler jobScheduler, SystemNanoClock clock,
      MemberId myself, LifeSupport life, Monitors monitors, Dependencies dependencies,
      Outbound<MemberId, RaftMessages.RaftMessage> outbound,
      ClusterStateLayout clusterState, CoreTopologyService topologyService,
      ClusterStateStorageFactory storageFactory,
      NamedDatabaseId namedDatabaseId) {
    DatabaseLogProvider logProvider = logService.getInternalLogProvider();
    TimerService timerService = new TimerService(jobScheduler, logProvider);
    Map<Integer, ChannelMarshal<ReplicatedContent>> marshals = Map
        .of(2, new CoreReplicatedContentMarshalV2());
    RaftLog underlyingLog = createRaftLog(config, life, fileSystem, clusterState, marshals,
        logProvider, jobScheduler, namedDatabaseId);
    this.raftLog = new MonitoredRaftLog(underlyingLog, monitors);
    StateStorage<TermState> durableTermState = storageFactory
        .createRaftTermStorage(namedDatabaseId.name(), life, logProvider);
    StateStorage<TermState> termState = new MonitoredTermStateStorage(durableTermState, monitors);
    StateStorage<VoteState> voteState = storageFactory
        .createRaftVoteStorage(namedDatabaseId.name(), life, logProvider);
    StateStorage<RaftMembershipState> raftMembershipStorage = storageFactory
        .createRaftMembershipStorage(namedDatabaseId.name(), life, logProvider);
    this.leaderAvailabilityTimers = createElectionTiming(config, timerService, logProvider);
    SendToMyself leaderOnlyReplicator = new SendToMyself(myself, outbound);
    Integer minimumConsensusGroupSize = config
        .get(CausalClusteringSettings.minimum_core_cluster_size_at_runtime);
    MemberIdSetBuilder memberSetBuilder = new MemberIdSetBuilder();
    this.raftMembershipManager =
        new RaftMembershipManager(leaderOnlyReplicator, myself, memberSetBuilder, this.raftLog,
            logProvider, minimumConsensusGroupSize,
            this.leaderAvailabilityTimers.getElectionTimeoutMillis(), Clocks.systemClock(),
            config.get(CausalClusteringSettings.join_catch_up_timeout).toMillis(),
            raftMembershipStorage);
    dependencies.satisfyDependency(this.raftMembershipManager);
    life.add(this.raftMembershipManager);
    this.inFlightCache = InFlightCacheFactory.create(config, monitors);
    RaftLogShippingManager logShipping =
        new RaftLogShippingManager(outbound, logProvider, this.raftLog, timerService,
            Clocks.systemClock(), myself, this.raftMembershipManager,
            this.leaderAvailabilityTimers.getElectionTimeoutMillis(),
            config.get(CausalClusteringSettings.catchup_batch_size),
            config.get(CausalClusteringSettings.log_shipping_max_lag), this.inFlightCache);
    boolean supportsPreVoting = config.get(CausalClusteringSettings.enable_pre_voting);
    RaftState state =
        new RaftState(myself, termState, this.raftMembershipManager, this.raftLog, voteState,
            this.inFlightCache, logProvider, supportsPreVoting,
            config.get(CausalClusteringSettings.refuse_to_be_leader));
    RaftMessageTimerResetMonitor raftMessageTimerResetMonitor =
        monitors.newMonitor(RaftMessageTimerResetMonitor.class, new String[0]);
    RaftOutcomeApplier raftOutcomeApplier =
        new RaftOutcomeApplier(state, outbound, this.leaderAvailabilityTimers,
            raftMessageTimerResetMonitor, logShipping, this.raftMembershipManager,
            logProvider);
    this.raftMachine =
        new RaftMachine(myself, this.leaderAvailabilityTimers, logProvider,
            this.raftMembershipManager, this.inFlightCache, raftOutcomeApplier,
            state);
    DurationSinceLastMessageMonitor durationSinceLastMessageMonitor = new DurationSinceLastMessageMonitor(
        clock);
    monitors.addMonitorListener(durationSinceLastMessageMonitor);
    dependencies.satisfyDependency(durationSinceLastMessageMonitor);
    life.add(new RaftCoreTopologyConnector(topologyService, this.raftMachine, namedDatabaseId));
    life.add(logShipping);
  }

  private static LeaderAvailabilityTimers createElectionTiming(Config config,
      TimerService timerService, LogProvider logProvider) {
    Duration electionTimeout = config.get(CausalClusteringSettings.leader_election_timeout);
    return new LeaderAvailabilityTimers(electionTimeout, electionTimeout.dividedBy(3L),
        Clocks.systemClock(), timerService, logProvider);
  }

  private static RaftLog createRaftLog(Config config, LifeSupport life,
      FileSystemAbstraction fileSystem, ClusterStateLayout layout,
      Map<Integer, ChannelMarshal<ReplicatedContent>> marshalSelector, LogProvider logProvider,
      JobScheduler scheduler,
      NamedDatabaseId namedDatabaseId) {
    RaftLogImplementation raftLogImplementation = RaftLogImplementation
        .valueOf(config.get(CausalClusteringSettings.raft_log_implementation));
    switch (raftLogImplementation) {
      case IN_MEMORY:
        return new InMemoryRaftLog();
      case SEGMENTED:
        long rotateAtSize = config.get(CausalClusteringSettings.raft_log_rotation_size);
        int readerPoolSize = config.get(CausalClusteringSettings.raft_log_reader_pool_size);
        CoreLogPruningStrategy pruningStrategy =
            (new CoreLogPruningStrategyFactory(
                config.get(CausalClusteringSettings.raft_log_pruning_strategy), logProvider))
                .newInstance();
        File directory = layout.raftLogDirectory(namedDatabaseId.name());
        Objects.requireNonNull(marshalSelector);
        return life.add(
            new SegmentedRaftLog(fileSystem, directory, rotateAtSize, marshalSelector::get,
                logProvider, readerPoolSize, Clocks.systemClock(),
                scheduler, pruningStrategy));
      default:
        throw new IllegalStateException(
            "Unknown raft log implementation: " + raftLogImplementation);
    }
  }

  public RaftLog raftLog() {
    return this.raftLog;
  }

  public RaftMachine raftMachine() {
    return this.raftMachine;
  }

  public RaftMembershipManager raftMembershipManager() {
    return this.raftMembershipManager;
  }

  public InFlightCache inFlightCache() {
    return this.inFlightCache;
  }

  public LeaderAvailabilityTimers getLeaderAvailabilityTimers() {
    return this.leaderAvailabilityTimers;
  }
}
