/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus;

import java.util.concurrent.ThreadFactory;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ContinuousJob extends LifecycleAdapter {

  private final ContinuousJob.AbortableJob abortableJob;
  private final Log log;
  private final Thread thread;

  public ContinuousJob(ThreadFactory threadFactory, Runnable task, LogProvider logProvider,
      String threadNameSuffix) {
    this.abortableJob = new ContinuousJob.AbortableJob(task);
    this.thread = threadFactory.newThread(this.abortableJob);
    Thread n10000 = this.thread;
    String n10001 = this.thread.getName();
    n10000.setName(n10001 + "-" + threadNameSuffix);
    this.log = logProvider.getLog(this.getClass());
  }

  public void start() {
    this.abortableJob.keepRunning = true;
    this.thread.start();
  }

  public void stop() throws Exception {
    this.log.info("ContinuousJob " + this.thread.getName() + " stopping");
    this.abortableJob.keepRunning = false;
    this.thread.join();
  }

  private static class AbortableJob implements Runnable {

    private final Runnable task;
    private volatile boolean keepRunning;

    AbortableJob(Runnable task) {
      this.task = task;
    }

    public void run() {
      while (this.keepRunning) {
        this.task.run();
      }
    }
  }
}
