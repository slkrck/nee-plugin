/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.replication.session;

public class LocalOperationId {

  private final long localSessionId;
  private final long sequenceNumber;

  public LocalOperationId(long localSessionId, long sequenceNumber) {
    this.localSessionId = localSessionId;
    this.sequenceNumber = sequenceNumber;
  }

  public long localSessionId() {
    return this.localSessionId;
  }

  public long sequenceNumber() {
    return this.sequenceNumber;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      LocalOperationId that = (LocalOperationId) o;
      if (this.localSessionId != that.localSessionId) {
        return false;
      } else {
        return this.sequenceNumber == that.sequenceNumber;
      }
    } else {
      return false;
    }
  }

  public int hashCode() {
    int result = (int) (this.localSessionId ^ this.localSessionId >>> 32);
    result = 31 * result + (int) (this.sequenceNumber ^ this.sequenceNumber >>> 32);
    return result;
  }

  public String toString() {
    return String
        .format("LocalOperationId{localSessionId=%d, sequenceNumber=%d}", this.localSessionId,
            this.sequenceNumber);
  }
}
