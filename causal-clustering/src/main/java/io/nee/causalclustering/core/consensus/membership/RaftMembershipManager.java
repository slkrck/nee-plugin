/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.membership;

import io.nee.causalclustering.core.consensus.log.RaftLogCursor;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.consensus.log.ReadableRaftLog;
import io.nee.causalclustering.core.consensus.outcome.RaftLogCommand;
import io.nee.causalclustering.core.consensus.roles.Role;
import io.nee.causalclustering.core.consensus.roles.follower.FollowerStates;
import io.nee.causalclustering.core.replication.SendToMyself;
import io.nee.causalclustering.core.state.storage.StateStorage;
import io.nee.causalclustering.identity.MemberId;
import java.io.IOException;
import java.time.Clock;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.LongSupplier;
import org.neo4j.internal.helpers.collection.Iterables;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RaftMembershipManager extends LifecycleAdapter implements RaftMembership,
    RaftLogCommand.Handler {

  private final SendToMyself sendToMyself;
  private final MemberId myself;
  private final RaftMembers.Builder<MemberId> memberSetBuilder;
  private final ReadableRaftLog raftLog;
  private final Log log;
  private final StateStorage<RaftMembershipState> storage;
  private final int minimumConsensusGroupSize;
  private final RaftMembershipChanger membershipChanger;
  private final Set<RaftMembership.Listener> listeners = new HashSet();
  private final Set<MemberId> additionalReplicationMembers = new HashSet();
  private Set<MemberId> targetMembers;
  private LongSupplier recoverFromIndexSupplier;
  private RaftMembershipState state;
  private volatile Set<MemberId> votingMembers = Collections.unmodifiableSet(new HashSet());
  private volatile Set<MemberId> replicationMembers = Collections.unmodifiableSet(new HashSet());

  public RaftMembershipManager(SendToMyself sendToMyself, MemberId myself,
      RaftMembers.Builder<MemberId> memberSetBuilder, ReadableRaftLog raftLog,
      LogProvider logProvider, int minimumConsensusGroupSize, long electionTimeout, Clock clock,
      long catchupTimeout,
      StateStorage<RaftMembershipState> membershipStorage) {
    this.sendToMyself = sendToMyself;
    this.myself = myself;
    this.memberSetBuilder = memberSetBuilder;
    this.raftLog = raftLog;
    this.minimumConsensusGroupSize = minimumConsensusGroupSize;
    this.storage = membershipStorage;
    this.log = logProvider.getLog(this.getClass());
    this.membershipChanger = new RaftMembershipChanger(raftLog, clock, electionTimeout, logProvider,
        catchupTimeout, this);
  }

  public void setRecoverFromIndexSupplier(LongSupplier recoverFromIndexSupplier) {
    this.recoverFromIndexSupplier = recoverFromIndexSupplier;
  }

  public void start() throws IOException {
    this.state = this.storage.getInitialState();
    long recoverFromIndex = this.recoverFromIndexSupplier.getAsLong();
    this.log.info("Membership state before recovery: " + this.state);
    this.log.info("Recovering from: " + recoverFromIndex + " to: " + this.raftLog.appendIndex());
    RaftLogCursor cursor = this.raftLog.getEntryCursor(recoverFromIndex);

    try {
      while (cursor.next()) {
        this.append(cursor.index(), cursor.get());
      }
    } catch (Throwable n7) {
      if (cursor != null) {
        try {
          cursor.close();
        } catch (Throwable n6) {
          n7.addSuppressed(n6);
        }
      }

      throw n7;
    }

    if (cursor != null) {
      cursor.close();
    }

    this.log.info("Membership state after recovery: " + this.state);
    this.updateMemberSets();
  }

  public void setTargetMembershipSet(Set<MemberId> targetMembers) {
    boolean targetMembershipChanged = !targetMembers.equals(this.targetMembers);
    this.targetMembers = new HashSet(targetMembers);
    if (targetMembershipChanged) {
      this.log.info("Target membership: " + targetMembers);
    }

    this.membershipChanger.onTargetChanged(targetMembers);
    HashSet<MemberId> intersection = new HashSet(targetMembers);
    Set<MemberId> votingMembers = this.votingMembers();
    intersection.retainAll(votingMembers);
    int majority = votingMembers.size() / 2 + 1;
    if (intersection.size() < majority) {
      this.log.warn(
          "Target membership %s does not contain a majority of existing raft members %s. It is likely an operator removed too many core members too quickly. If not, this issue should be transient.",
          this.targetMembers, this.votingMembers);
    }

    this.checkForStartCondition();
  }

  private Set<MemberId> missingMembers() {
    if (this.targetMembers != null && this.votingMembers() != null) {
      Set<MemberId> missingMembers = new HashSet(this.targetMembers);
      missingMembers.removeAll(this.votingMembers());
      return missingMembers;
    } else {
      return Collections.emptySet();
    }
  }

  private void updateMemberSets() {
    this.votingMembers = Collections.unmodifiableSet(this.state.getLatest());
    HashSet<MemberId> newReplicationMembers = new HashSet(this.votingMembers);
    newReplicationMembers.addAll(this.additionalReplicationMembers);
    this.replicationMembers = Collections.unmodifiableSet(newReplicationMembers);
    this.listeners.forEach(RaftMembership.Listener::onMembershipChanged);
  }

  void addAdditionalReplicationMember(MemberId member) {
    this.additionalReplicationMembers.add(member);
    this.updateMemberSets();
  }

  void removeAdditionalReplicationMember(MemberId member) {
    this.additionalReplicationMembers.remove(member);
    this.updateMemberSets();
  }

  private boolean isSafeToRemoveMember() {
    Set<MemberId> votingMembers = this.votingMembers();
    boolean safeToRemoveMember =
        votingMembers != null && votingMembers.size() > this.minimumConsensusGroupSize;
    if (!safeToRemoveMember) {
      Set<MemberId> membersToRemove = this.superfluousMembers();
      this.log.info(
          "Not safe to remove %s %s because it would reduce the number of voting members below the expected cluster size of %d. Voting members: %s",
          membersToRemove.size() > 1 ? "members" : "member", membersToRemove,
          this.minimumConsensusGroupSize, votingMembers);
    }

    return safeToRemoveMember;
  }

  private Set<MemberId> superfluousMembers() {
    if (this.targetMembers != null && this.votingMembers() != null) {
      Set<MemberId> superfluousMembers = new HashSet(this.votingMembers());
      superfluousMembers.removeAll(this.targetMembers);
      superfluousMembers.remove(this.myself);
      return superfluousMembers;
    } else {
      return Collections.emptySet();
    }
  }

  private void checkForStartCondition() {
    if (this.missingMembers().size() > 0) {
      this.membershipChanger.onMissingMember(Iterables.first(this.missingMembers()));
    } else if (this.superfluousMembers().size() > 0 && this.isSafeToRemoveMember()) {
      this.membershipChanger.onSuperfluousMember(Iterables.first(this.superfluousMembers()));
    }
  }

  void doConsensus(Set<MemberId> newVotingMemberSet) {
    this.log.info("Getting consensus on new voting member set %s", newVotingMemberSet);
    this.sendToMyself.replicate(this.memberSetBuilder.build(newVotingMemberSet));
  }

  void stateChanged() {
    this.checkForStartCondition();
  }

  public void onFollowerStateChange(FollowerStates<MemberId> followerStates) {
    this.membershipChanger.onFollowerStateChange(followerStates);
  }

  public void onRole(Role role) {
    this.membershipChanger.onRole(role);
  }

  public Set<MemberId> votingMembers() {
    return this.votingMembers;
  }

  public Set<MemberId> replicationMembers() {
    return this.replicationMembers;
  }

  public void registerListener(RaftMembership.Listener listener) {
    this.listeners.add(listener);
  }

  boolean uncommittedMemberChangeInLog() {
    return this.state.uncommittedMemberChangeInLog();
  }

  public void processLog(long commitIndex, Collection<RaftLogCommand> logCommands)
      throws IOException {
    Iterator n4 = logCommands.iterator();

    while (n4.hasNext()) {
      RaftLogCommand logCommand = (RaftLogCommand) n4.next();
      logCommand.dispatch(this);
    }

    if (this.state.commit(commitIndex)) {
      this.membershipChanger.onRaftGroupCommitted();
      this.storage.writeState(this.state);
      this.updateMemberSets();
    }
  }

  public void append(long baseIndex, RaftLogEntry... entries) throws IOException {
    RaftLogEntry[] n4 = entries;
    int n5 = entries.length;

    for (int n6 = 0; n6 < n5; ++n6) {
      RaftLogEntry entry = n4[n6];
      if (entry.content() instanceof RaftMembers) {
        RaftMembers<MemberId> raftMembers = (RaftMembers) entry.content();
        if (this.state.uncommittedMemberChangeInLog()) {
          this.log.warn("Appending with uncommitted membership change in log");
        }

        if (this.state.append(baseIndex, new HashSet(raftMembers.getMembers()))) {
          this.log.info("Appending new member set %s", this.state);
          this.storage.writeState(this.state);
          this.updateMemberSets();
        } else {
          this.log.warn(
              "Appending member set was ignored. Current state: %s, Appended set: %s, Log index: %d%n",
              this.state, raftMembers, baseIndex);
        }
      }

      ++baseIndex;
    }
  }

  public void truncate(long fromIndex) throws IOException {
    if (this.state.truncate(fromIndex)) {
      this.storage.writeState(this.state);
      this.updateMemberSets();
    }
  }

  public void prune(long pruneIndex) {
  }

  public MembershipEntry getCommitted() {
    return this.state.committed();
  }

  public void install(MembershipEntry committed) throws IOException {
    this.state = new RaftMembershipState(committed.logIndex(), committed, null);
    this.storage.writeState(this.state);
    this.updateMemberSets();
  }
}
