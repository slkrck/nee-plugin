/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.roles.follower;

import java.util.HashMap;
import java.util.Map;

public class FollowerStates<MEMBER> {

  private final Map<MEMBER, FollowerState> states;

  public FollowerStates(FollowerStates<MEMBER> original, MEMBER updatedMember,
      FollowerState updatedState) {
    this.states = new HashMap(original.states);
    this.states.put(updatedMember, updatedState);
  }

  public FollowerStates() {
    this.states = new HashMap();
  }

  public FollowerState get(MEMBER member) {
    FollowerState result = this.states.get(member);
    if (result == null) {
      result = new FollowerState();
    }

    return result;
  }

  public String toString() {
    return String.format("FollowerStates%s", this.states);
  }

  public int size() {
    return this.states.size();
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      boolean n10000;
      label35:
      {
        FollowerStates that = (FollowerStates) o;
        if (this.states != null) {
          if (this.states.equals(that.states)) {
            break label35;
          }
        } else if (that.states == null) {
          break label35;
        }

        n10000 = false;
        return n10000;
      }

      n10000 = true;
      return n10000;
    } else {
      return false;
    }
  }

  public int hashCode() {
    return this.states != null ? this.states.hashCode() : 0;
  }

  public FollowerStates<MEMBER> onSuccessResponse(MEMBER member, long newMatchIndex) {
    return new FollowerStates(this, member, this.get(member).onSuccessResponse(newMatchIndex));
  }
}
