/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.shipping;

import io.nee.causalclustering.core.consensus.LeaderContext;
import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.log.ReadableRaftLog;
import io.nee.causalclustering.core.consensus.log.cache.InFlightCache;
import io.nee.causalclustering.core.consensus.membership.RaftMembership;
import io.nee.causalclustering.core.consensus.outcome.ShipCommand;
import io.nee.causalclustering.core.consensus.schedule.TimerService;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.Outbound;
import java.time.Clock;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.LogProvider;

public class RaftLogShippingManager extends LifecycleAdapter implements RaftMembership.Listener {

  private final Outbound<MemberId, RaftMessages.RaftMessage> outbound;
  private final LogProvider logProvider;
  private final ReadableRaftLog raftLog;
  private final Clock clock;
  private final MemberId myself;
  private final RaftMembership membership;
  private final long retryTimeMillis;
  private final int catchupBatchSize;
  private final int maxAllowedShippingLag;
  private final InFlightCache inFlightCache;
  private final Map<MemberId, RaftLogShipper> logShippers = new HashMap();
  private final TimerService timerService;
  private LeaderContext lastLeaderContext;
  private boolean running;
  private boolean stopped;

  public RaftLogShippingManager(Outbound<MemberId, RaftMessages.RaftMessage> outbound,
      LogProvider logProvider, ReadableRaftLog raftLog,
      TimerService timerService, Clock clock, MemberId myself, RaftMembership membership,
      long retryTimeMillis,
      int catchupBatchSize,
      int maxAllowedShippingLag, InFlightCache inFlightCache) {
    this.outbound = outbound;
    this.logProvider = logProvider;
    this.raftLog = raftLog;
    this.timerService = timerService;
    this.clock = clock;
    this.myself = myself;
    this.membership = membership;
    this.retryTimeMillis = retryTimeMillis;
    this.catchupBatchSize = catchupBatchSize;
    this.maxAllowedShippingLag = maxAllowedShippingLag;
    this.inFlightCache = inFlightCache;
    membership.registerListener(this);
  }

  public synchronized void pause() {
    this.running = false;
    this.logShippers.values().forEach(RaftLogShipper::stop);
    this.logShippers.clear();
  }

  public synchronized void resume(LeaderContext initialLeaderContext) {
    if (!this.stopped) {
      this.running = true;
      Iterator n2 = this.membership.replicationMembers().iterator();

      while (n2.hasNext()) {
        MemberId member = (MemberId) n2.next();
        this.ensureLogShipperRunning(member, initialLeaderContext);
      }

      this.lastLeaderContext = initialLeaderContext;
    }
  }

  public synchronized void stop() {
    this.pause();
    this.stopped = true;
  }

  private void ensureLogShipperRunning(MemberId member, LeaderContext leaderContext) {
    RaftLogShipper logShipper = this.logShippers.get(member);
    if (logShipper == null && !member.equals(this.myself)) {
      logShipper =
          new RaftLogShipper(this.outbound, this.logProvider, this.raftLog, this.clock,
              this.timerService, this.myself, member, leaderContext.term,
              leaderContext.commitIndex, this.retryTimeMillis, this.catchupBatchSize,
              this.maxAllowedShippingLag,
              this.inFlightCache);
      this.logShippers.put(member, logShipper);
      logShipper.start();
    }
  }

  public synchronized void handleCommands(Iterable<ShipCommand> shipCommands,
      LeaderContext leaderContext) {
    Iterator n3 = shipCommands.iterator();

    while (n3.hasNext()) {
      ShipCommand shipCommand = (ShipCommand) n3.next();
      Iterator n5 = this.logShippers.values().iterator();

      while (n5.hasNext()) {
        RaftLogShipper logShipper = (RaftLogShipper) n5.next();
        shipCommand.applyTo(logShipper, leaderContext);
      }
    }

    this.lastLeaderContext = leaderContext;
  }

  public synchronized void onMembershipChanged() {
    if (this.lastLeaderContext != null && this.running) {
      HashSet<MemberId> toBeRemoved = new HashSet(this.logShippers.keySet());
      toBeRemoved.removeAll(this.membership.replicationMembers());
      Iterator n2 = toBeRemoved.iterator();

      MemberId replicationMember;
      while (n2.hasNext()) {
        replicationMember = (MemberId) n2.next();
        RaftLogShipper logShipper = this.logShippers.remove(replicationMember);
        if (logShipper != null) {
          logShipper.stop();
        }
      }

      n2 = this.membership.replicationMembers().iterator();

      while (n2.hasNext()) {
        replicationMember = (MemberId) n2.next();
        this.ensureLogShipperRunning(replicationMember, this.lastLeaderContext);
      }
    }
  }

  public String toString() {
    return String
        .format("RaftLogShippingManager{logShippers=%s, myself=%s}", this.logShippers, this.myself);
  }
}
