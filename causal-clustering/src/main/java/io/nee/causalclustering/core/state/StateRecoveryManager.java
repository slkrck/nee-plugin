/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.core.state.storage.StateMarshal;
import io.nee.causalclustering.messaging.EndOfStreamException;
import java.io.File;
import java.io.IOException;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.ReadAheadChannel;
import org.neo4j.io.memory.BufferScope;

public class StateRecoveryManager<STATE> {

  protected final FileSystemAbstraction fileSystem;
  private final StateMarshal<STATE> marshal;

  public StateRecoveryManager(FileSystemAbstraction fileSystem, StateMarshal<STATE> marshal) {
    this.fileSystem = fileSystem;
    this.marshal = marshal;
  }

  public StateRecoveryManager.RecoveryStatus<STATE> recover(File fileA, File fileB)
      throws IOException {
    assert fileA != null && fileB != null;

    STATE a = this.readLastEntryFrom(fileA);
    STATE b = this.readLastEntryFrom(fileB);
    if (a == null && b == null) {
      throw new IllegalStateException("no recoverable state");
    } else if (a == null) {
      return new StateRecoveryManager.RecoveryStatus(fileA, b);
    } else if (b == null) {
      return new StateRecoveryManager.RecoveryStatus(fileB, a);
    } else {
      return this.marshal.ordinal(a) > this.marshal.ordinal(b)
          ? new StateRecoveryManager.RecoveryStatus(fileB, a)
          : new StateRecoveryManager.RecoveryStatus(fileA, b);
    }
  }

  private STATE readLastEntryFrom(File file) throws IOException {
    BufferScope bufferScope = new BufferScope(ReadAheadChannel.DEFAULT_READ_AHEAD_SIZE);

    Object n6;
    try {
      ReadAheadChannel channel = new ReadAheadChannel(this.fileSystem.read(file),
          bufferScope.buffer);

      try {
        Object result = null;

        while (true) {
          try {
            Object lastRead;
            if ((lastRead = this.marshal.unmarshal(channel)) != null) {
              result = lastRead;
              continue;
            }
          } catch (EndOfStreamException n9) {
          }

          n6 = result;
          break;
        }
      } catch (Throwable n10) {
        try {
          channel.close();
        } catch (Throwable n8) {
          n10.addSuppressed(n8);
        }

        throw n10;
      }

      channel.close();
    } catch (Throwable n11) {
      try {
        bufferScope.close();
      } catch (Throwable n7) {
        n11.addSuppressed(n7);
      }

      throw n11;
    }

    bufferScope.close();
    return (STATE) n6;
  }

  public static class RecoveryStatus<STATE> {

    private final File activeFile;
    private final STATE recoveredState;

    RecoveryStatus(File activeFile, STATE recoveredState) {
      this.activeFile = activeFile;
      this.recoveredState = recoveredState;
    }

    public STATE recoveredState() {
      return this.recoveredState;
    }

    public File activeFile() {
      return this.activeFile;
    }
  }
}
