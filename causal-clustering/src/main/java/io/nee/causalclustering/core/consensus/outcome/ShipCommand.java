/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.outcome;

import io.nee.causalclustering.core.consensus.LeaderContext;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.consensus.shipping.RaftLogShipper;
import java.util.Arrays;

public abstract class ShipCommand {

  public abstract void applyTo(RaftLogShipper n1, LeaderContext n2);

  public static class CommitUpdate extends ShipCommand {

    public void applyTo(RaftLogShipper raftLogShipper, LeaderContext leaderContext) {
      raftLogShipper.onCommitUpdate(leaderContext);
    }

    public String toString() {
      return "CommitUpdate{}";
    }
  }

  public static class NewEntries extends ShipCommand {

    private final long prevLogIndex;
    private final long prevLogTerm;
    private final RaftLogEntry[] newLogEntries;

    public NewEntries(long prevLogIndex, long prevLogTerm, RaftLogEntry[] newLogEntries) {
      this.prevLogIndex = prevLogIndex;
      this.prevLogTerm = prevLogTerm;
      this.newLogEntries = newLogEntries;
    }

    public void applyTo(RaftLogShipper raftLogShipper, LeaderContext leaderContext) {
      raftLogShipper
          .onNewEntries(this.prevLogIndex, this.prevLogTerm, this.newLogEntries, leaderContext);
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        ShipCommand.NewEntries newEntries = (ShipCommand.NewEntries) o;
        if (this.prevLogIndex != newEntries.prevLogIndex) {
          return false;
        } else {
          return this.prevLogTerm == newEntries.prevLogTerm && Arrays
              .equals(this.newLogEntries, newEntries.newLogEntries);
        }
      } else {
        return false;
      }
    }

    public int hashCode() {
      int result = (int) (this.prevLogIndex ^ this.prevLogIndex >>> 32);
      result = 31 * result + (int) (this.prevLogTerm ^ this.prevLogTerm >>> 32);
      result = 31 * result + Arrays.hashCode(this.newLogEntries);
      return result;
    }

    public String toString() {
      return String
          .format("NewEntry{prevLogIndex=%d, prevLogTerm=%d, newLogEntry=%s}", this.prevLogIndex,
              this.prevLogTerm,
              Arrays.toString(this.newLogEntries));
    }
  }

  public static class Match extends ShipCommand {

    private final long newMatchIndex;
    private final Object target;

    public Match(long newMatchIndex, Object target) {
      this.newMatchIndex = newMatchIndex;
      this.target = target;
    }

    public void applyTo(RaftLogShipper raftLogShipper, LeaderContext leaderContext) {
      if (raftLogShipper.identity().equals(this.target)) {
        raftLogShipper.onMatch(this.newMatchIndex, leaderContext);
      }
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        ShipCommand.Match match = (ShipCommand.Match) o;
        return this.newMatchIndex == match.newMatchIndex && this.target.equals(match.target);
      } else {
        return false;
      }
    }

    public int hashCode() {
      int result = (int) (this.newMatchIndex ^ this.newMatchIndex >>> 32);
      result = 31 * result + this.target.hashCode();
      return result;
    }

    public String toString() {
      return String.format("Match{newMatchIndex=%d, target=%s}", this.newMatchIndex, this.target);
    }
  }

  public static class Mismatch extends ShipCommand {

    private final long lastRemoteAppendIndex;
    private final Object target;

    public Mismatch(long lastRemoteAppendIndex, Object target) {
      this.lastRemoteAppendIndex = lastRemoteAppendIndex;
      this.target = target;
    }

    public void applyTo(RaftLogShipper raftLogShipper, LeaderContext leaderContext) {
      if (raftLogShipper.identity().equals(this.target)) {
        raftLogShipper.onMismatch(this.lastRemoteAppendIndex, leaderContext);
      }
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        ShipCommand.Mismatch mismatch = (ShipCommand.Mismatch) o;
        return this.lastRemoteAppendIndex == mismatch.lastRemoteAppendIndex && this.target
            .equals(mismatch.target);
      } else {
        return false;
      }
    }

    public int hashCode() {
      int result = (int) (this.lastRemoteAppendIndex ^ this.lastRemoteAppendIndex >>> 32);
      result = 31 * result + this.target.hashCode();
      return result;
    }

    public String toString() {
      return String
          .format("Mismatch{lastRemoteAppendIndex=%d, target=%s}", this.lastRemoteAppendIndex,
              this.target);
    }
  }
}
