/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.core.state.BootstrapSaver;
import io.nee.causalclustering.core.state.CoreSnapshotService;
import io.nee.causalclustering.core.state.snapshot.CoreDownloaderService;
import io.nee.causalclustering.core.state.storage.SimpleStorage;
import io.nee.causalclustering.identity.BoundState;
import io.nee.causalclustering.identity.RaftBinder;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.LifecycleMessageHandler;
import io.nee.dbms.ClusterInternalDbmsOperator;
import io.nee.dbms.DatabaseStartAborter;
import java.time.Duration;
import java.util.Optional;
import org.neo4j.kernel.database.Database;
import org.neo4j.scheduler.JobHandle;

class CoreBootstrap {

  private final Database kernelDatabase;
  private final RaftBinder raftBinder;
  private final LifecycleMessageHandler<?> raftMessageHandler;
  private final CoreSnapshotService snapshotService;
  private final CoreDownloaderService downloadService;
  private final ClusterInternalDbmsOperator clusterInternalOperator;
  private final DatabaseStartAborter databaseStartAborter;
  private final SimpleStorage<RaftId> raftIdStorage;
  private final BootstrapSaver bootstrapSaver;
  private final TempBootstrapDir tempBootstrapDir;

  CoreBootstrap(Database kernelDatabase, RaftBinder raftBinder,
      LifecycleMessageHandler<?> raftMessageHandler, CoreSnapshotService snapshotService,
      CoreDownloaderService downloadService, ClusterInternalDbmsOperator clusterInternalOperator,
      DatabaseStartAborter databaseStartAborter,
      SimpleStorage<RaftId> raftIdStorage, BootstrapSaver bootstrapSaver,
      TempBootstrapDir tempBootstrapDir) {
    this.kernelDatabase = kernelDatabase;
    this.raftBinder = raftBinder;
    this.raftMessageHandler = raftMessageHandler;
    this.snapshotService = snapshotService;
    this.downloadService = downloadService;
    this.clusterInternalOperator = clusterInternalOperator;
    this.databaseStartAborter = databaseStartAborter;
    this.raftIdStorage = raftIdStorage;
    this.bootstrapSaver = bootstrapSaver;
    this.tempBootstrapDir = tempBootstrapDir;
  }

  public void perform() throws Exception {
    ClusterInternalDbmsOperator.BootstrappingHandle bootstrapHandle = this.clusterInternalOperator
        .bootstrap(this.kernelDatabase.getNamedDatabaseId());

    try {
      this.bindAndStartMessageHandler();
    } finally {
      bootstrapHandle.release();
      this.databaseStartAborter.started(this.kernelDatabase.getNamedDatabaseId());
    }
  }

  private void bindAndStartMessageHandler() throws Exception {
    this.bootstrapSaver.restore(this.kernelDatabase.getDatabaseLayout());
    this.tempBootstrapDir.delete();
    BoundState boundState = this.raftBinder.bindToRaft(this.databaseStartAborter);
    if (boundState.snapshot().isPresent()) {
      this.snapshotService.installSnapshot(boundState.snapshot().get());
      this.raftMessageHandler.start(boundState.raftId());
    } else {
      this.raftMessageHandler.start(boundState.raftId());

      try {
        this.awaitState();
        this.bootstrapSaver.clean(this.kernelDatabase.getDatabaseLayout());
      } catch (Exception n3) {
        this.raftMessageHandler.stop();
        throw n3;
      }
    }

    if (this.raftIdStorage.exists()) {
      RaftId raftIdStore = this.raftIdStorage.readState();
      if (!raftIdStore.equals(boundState.raftId())) {
        throw new IllegalStateException(
            String.format("Exiting raft id '%s' is different from bound state '%s'.",
                raftIdStore.uuid(), boundState.raftId().uuid()));
      }
    } else {
      this.raftIdStorage.writeState(boundState.raftId());
    }
  }

  private void awaitState() throws Exception {
    Duration waitTime = Duration.ofSeconds(1L);
    this.snapshotService.awaitState(this.databaseStartAborter, waitTime);
    Optional<JobHandle> downloadJob = this.downloadService.downloadJob();
    if (downloadJob.isPresent()) {
      downloadJob.get().waitTermination();
    }
  }
}
