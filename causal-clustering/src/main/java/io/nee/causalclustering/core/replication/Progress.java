/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.replication;

import io.nee.causalclustering.core.state.StateMachineResult;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Progress {

  private final Semaphore replicationSignal = new Semaphore(0);
  private final Semaphore resultSignal = new Semaphore(0);
  private volatile boolean isReplicated;
  private volatile StateMachineResult result;

  public void triggerReplicationEvent() {
    this.replicationSignal.release();
  }

  public void setReplicated() {
    this.isReplicated = true;
    this.replicationSignal.release();
  }

  public void awaitReplication(long timeoutMillis) throws InterruptedException {
    if (!this.isReplicated) {
      this.replicationSignal.tryAcquire(timeoutMillis, TimeUnit.MILLISECONDS);
    }
  }

  public void awaitResult() throws InterruptedException {
    if (this.result == null) {
      this.resultSignal.acquire();
    }
  }

  public boolean isReplicated() {
    return this.isReplicated;
  }

  void registerResult(StateMachineResult result) {
    this.result = result;
    this.resultSignal.release();
  }

  public StateMachineResult result() {
    return this.result;
  }
}
