/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.snapshot;

import io.nee.causalclustering.core.state.CoreStateFiles;
import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.messaging.EndOfStreamException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class CoreSnapshot {

  private final long prevIndex;
  private final long prevTerm;
  private final Map<CoreStateFiles<?>, Object> snapshotCollection = new HashMap();

  public CoreSnapshot(long prevIndex, long prevTerm) {
    this.prevIndex = prevIndex;
    this.prevTerm = prevTerm;
  }

  public long prevIndex() {
    return this.prevIndex;
  }

  public long prevTerm() {
    return this.prevTerm;
  }

  public <T> void add(CoreStateFiles<T> type, T state) {
    this.snapshotCollection.put(type, state);
  }

  public <T> T get(CoreStateFiles<T> type) {
    return (T) this.snapshotCollection.get(type);
  }

  public int size() {
    return this.snapshotCollection.size();
  }

  public String toString() {
    return String
        .format("CoreSnapshot{prevIndex=%d, prevTerm=%d, snapshotCollection=%s}", this.prevIndex,
            this.prevTerm, this.snapshotCollection);
  }

  public static class Marshal extends SafeChannelMarshal<CoreSnapshot> {

    public void marshal(CoreSnapshot coreSnapshot, WritableChannel channel) throws IOException {
      channel.putLong(coreSnapshot.prevIndex);
      channel.putLong(coreSnapshot.prevTerm);
      channel.putInt(coreSnapshot.size());
      Iterator n3 = coreSnapshot.snapshotCollection.entrySet().iterator();

      while (n3.hasNext()) {
        Entry<CoreStateFiles<?>, Object> entry = (Entry) n3.next();
        CoreStateFiles type = entry.getKey();
        Object state = entry.getValue();
        channel.putInt(type.typeId());
        type.marshal().marshal(state, channel);
      }
    }

    public CoreSnapshot unmarshal0(ReadableChannel channel)
        throws IOException, EndOfStreamException {
      long prevIndex = channel.getLong();
      long prevTerm = channel.getLong();
      CoreSnapshot coreSnapshot = new CoreSnapshot(prevIndex, prevTerm);
      int snapshotCount = channel.getInt();

      for (int i = 0; i < snapshotCount; ++i) {
        int typeOrdinal = channel.getInt();
        CoreStateFiles type = CoreStateFiles.values().get(typeOrdinal);
        Object state = type.marshal().unmarshal(channel);
        coreSnapshot.add(type, state);
      }

      return coreSnapshot;
    }
  }
}
