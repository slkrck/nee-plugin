/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.replication.session;

import io.nee.causalclustering.identity.MemberId;
import java.util.UUID;

public class GlobalSession {

  private final UUID sessionId;
  private final MemberId owner;

  public GlobalSession(UUID sessionId, MemberId owner) {
    this.sessionId = sessionId;
    this.owner = owner;
  }

  public UUID sessionId() {
    return this.sessionId;
  }

  public MemberId owner() {
    return this.owner;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      GlobalSession that = (GlobalSession) o;
      return this.sessionId.equals(that.sessionId) && this.owner.equals(that.owner);
    } else {
      return false;
    }
  }

  public int hashCode() {
    int result = this.sessionId.hashCode();
    result = 31 * result + this.owner.hashCode();
    return result;
  }

  public String toString() {
    return String.format("GlobalSession{sessionId=%s, owner=%s}", this.sessionId, this.owner);
  }
}
