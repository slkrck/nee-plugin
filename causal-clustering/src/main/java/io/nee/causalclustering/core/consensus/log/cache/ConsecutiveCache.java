/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.cache;

import io.nee.collection.CircularBuffer;

class ConsecutiveCache<V> {

  private final CircularBuffer<V> circle;
  private long endIndex = -1L;

  ConsecutiveCache(int capacity) {
    this.circle = new CircularBuffer(capacity);
  }

  private long firstIndex() {
    return this.endIndex - (long) this.circle.size() + 1L;
  }

  void put(long idx, V e, V[] evictions) {
    if (idx < 0L) {
      throw new IllegalArgumentException(String.format("Index must be >= 0 (was %d)", idx));
    } else if (e == null) {
      throw new IllegalArgumentException("Null entries are not accepted");
    } else {
      if (idx == this.endIndex + 1L) {
        evictions[0] = this.circle.append(e);
        ++this.endIndex;
      } else {
        this.circle.clear(evictions);
        this.circle.append(e);
        this.endIndex = idx;
      }
    }
  }

  V get(long idx) {
    if (idx < 0L) {
      throw new IllegalArgumentException(String.format("Index must be >= 0 (was %d)", idx));
    } else {
      return idx <= this.endIndex && idx >= this.firstIndex() ? this.circle
          .read(Math.toIntExact(idx - this.firstIndex())) : null;
    }
  }

  public void clear(V[] evictions) {
    this.circle.clear(evictions);
  }

  public int size() {
    return this.circle.size();
  }

  public void prune(long upToIndex, V[] evictions) {
    long index = this.firstIndex();

    for (int i = 0; index <= Math.min(upToIndex, this.endIndex); ++index) {
      evictions[i] = this.circle.remove();

      assert evictions[i] != null;

      ++i;
    }
  }

  public V remove() {
    return this.circle.remove();
  }

  public void truncate(long fromIndex, V[] evictions) {
    if (fromIndex <= this.endIndex) {
      long index = Math.max(fromIndex, this.firstIndex());

      for (int n6 = 0; index <= this.endIndex; ++index) {
        evictions[n6++] = this.circle.removeHead();
      }

      this.endIndex = fromIndex - 1L;
    }
  }
}
