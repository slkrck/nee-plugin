/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.core.CoreState;
import io.nee.causalclustering.core.consensus.RaftMachine;
import io.nee.causalclustering.core.consensus.log.RaftLog;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshot;
import io.nee.dbms.DatabaseStartAborter;
import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.neo4j.dbms.database.DatabaseStartAbortedException;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.internal.CappedLogger;
import org.neo4j.logging.internal.LogService;

public class CoreSnapshotService {

  private static final String OPERATION_NAME = "snapshot request";
  private final CommandApplicationProcess applicationProcess;
  private final CoreState coreState;
  private final RaftLog raftLog;
  private final RaftMachine raftMachine;
  private final NamedDatabaseId namedDatabaseId;
  private final CappedLogger logger;

  public CoreSnapshotService(CommandApplicationProcess applicationProcess, RaftLog raftLog,
      CoreState coreState, RaftMachine raftMachine,
      NamedDatabaseId namedDatabaseId, LogService logService, Clock clock) {
    this.applicationProcess = applicationProcess;
    this.coreState = coreState;
    this.raftLog = raftLog;
    this.raftMachine = raftMachine;
    this.namedDatabaseId = namedDatabaseId;
    this.logger = (new CappedLogger(logService.getInternalLog(this.getClass())))
        .setTimeLimit(10L, TimeUnit.SECONDS, clock);
  }

  public synchronized CoreSnapshot snapshot() throws Exception {
    this.applicationProcess.pauseApplier("snapshot request");

    CoreSnapshot n6;
    try {
      long lastApplied = this.applicationProcess.lastApplied();
      long prevTerm = this.raftLog.readEntryTerm(lastApplied);
      CoreSnapshot coreSnapshot = new CoreSnapshot(lastApplied, prevTerm);
      this.coreState.augmentSnapshot(coreSnapshot);
      coreSnapshot.add(CoreStateFiles.RAFT_CORE_STATE, this.raftMachine.coreState());
      n6 = coreSnapshot;
    } finally {
      this.applicationProcess.resumeApplier("snapshot request");
    }

    return n6;
  }

  public synchronized void installSnapshot(CoreSnapshot coreSnapshot) throws IOException {
    long snapshotPrevIndex = coreSnapshot.prevIndex();
    this.raftLog.skip(snapshotPrevIndex, coreSnapshot.prevTerm());
    this.raftMachine.installCoreState(coreSnapshot.get(CoreStateFiles.RAFT_CORE_STATE));
    this.coreState.installSnapshot(coreSnapshot);
    this.coreState.flush(snapshotPrevIndex);
    this.applicationProcess.installSnapshot(coreSnapshot);
    this.notifyAll();
  }

  public synchronized void awaitState(DatabaseStartAborter startAborter, Duration waitTime)
      throws InterruptedException, DatabaseStartAbortedException {
    while (this.raftMachine.state().appendIndex() < 0L) {
      this.logger.info("Waiting for another raft group member to publish a core state snapshot");
      if (startAborter.shouldAbort(this.namedDatabaseId)) {
        throw new DatabaseStartAbortedException(this.namedDatabaseId);
      }

      this.wait(waitTime.toMillis());
    }
  }
}
