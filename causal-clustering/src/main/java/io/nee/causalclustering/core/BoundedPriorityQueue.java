/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

public class BoundedPriorityQueue<E> {

  private final BoundedPriorityQueue.Config config;
  private final Function<E, Long> sizeOf;
  private final BlockingQueue<BoundedPriorityQueue<E>.StableElement> queue;
  private final AtomicLong seqGen = new AtomicLong();
  private final AtomicInteger count = new AtomicInteger();
  private final AtomicLong bytes = new AtomicLong();

  BoundedPriorityQueue(BoundedPriorityQueue.Config config, Function<E, Long> sizeOf,
      java.util.Comparator<E> comparator) {
    this.config = config;
    this.sizeOf = sizeOf;
    this.queue = new PriorityBlockingQueue(config.maxCount,
        new BoundedPriorityQueue.Comparator(comparator));
  }

  public int count() {
    return this.count.get();
  }

  public long bytes() {
    return this.bytes.get();
  }

  public BoundedPriorityQueue.Result offer(E element) {
    int updatedCount = this.count.incrementAndGet();
    if (updatedCount > this.config.maxCount) {
      this.count.decrementAndGet();
      return BoundedPriorityQueue.Result.E_COUNT_EXCEEDED;
    } else {
      long elementBytes = this.sizeOf.apply(element);
      long updatedBytes = this.bytes.addAndGet(elementBytes);
      if (elementBytes != 0L && updatedCount > this.config.minCount
          && updatedBytes > this.config.maxBytes) {
        this.bytes.addAndGet(-elementBytes);
        this.count.decrementAndGet();
        return BoundedPriorityQueue.Result.E_SIZE_EXCEEDED;
      } else if (!this.queue.offer(new BoundedPriorityQueue.StableElement(element))) {
        throw new IllegalStateException();
      } else {
        return BoundedPriorityQueue.Result.OK;
      }
    }
  }

  private Optional<E> deduct(BoundedPriorityQueue<E>.StableElement element) {
    if (element == null) {
      return Optional.empty();
    } else {
      this.count.decrementAndGet();
      this.bytes.addAndGet(-this.sizeOf.apply(element.element));
      return Optional.of(element.element);
    }
  }

  public Optional<E> poll() {
    return this.deduct((BoundedPriorityQueue.StableElement) this.queue.poll());
  }

  public Optional<E> poll(int timeout, TimeUnit unit) throws InterruptedException {
    return this.deduct((BoundedPriorityQueue.StableElement) this.queue.poll(timeout, unit));
  }

  Optional<BoundedPriorityQueue.Removable<E>> peek() {
    return Optional.ofNullable((BoundedPriorityQueue.Removable) this.queue.peek());
  }

  public enum Result {
    OK,
    E_COUNT_EXCEEDED,
    E_SIZE_EXCEEDED
  }

  public interface Removable<E> {

    E get();

    boolean remove();

    default <T> BoundedPriorityQueue.Removable<T> map(final Function<E, T> fn) {
      return new BoundedPriorityQueue.Removable<T>() {
        public T get() {
          return fn.apply(Removable.this.get());
        }

        public boolean remove() {
          return Removable.this.remove();
        }
      };
    }
  }

  public static class Config {

    private final int minCount;
    private final int maxCount;
    private final long maxBytes;

    public Config(int maxCount, long maxBytes) {
      this(1, maxCount, maxBytes);
    }

    public Config(int minCount, int maxCount, long maxBytes) {
      this.minCount = minCount;
      this.maxCount = maxCount;
      this.maxBytes = maxBytes;
    }
  }

  class Comparator implements java.util.Comparator<BoundedPriorityQueue<E>.StableElement> {

    private final java.util.Comparator<E> comparator;

    Comparator(java.util.Comparator<E> comparator) {
      this.comparator = comparator;
    }

    public int compare(BoundedPriorityQueue<E>.StableElement o1,
        BoundedPriorityQueue<E>.StableElement o2) {
      int compare = this.comparator.compare(o1.element, o2.element);
      return compare != 0 ? compare : Long.compare(o1.seqNo, o2.seqNo);
    }
  }

  class StableElement implements BoundedPriorityQueue.Removable<E> {

    private final long seqNo;
    private final E element;

    StableElement(E element) {
      this.seqNo = BoundedPriorityQueue.this.seqGen.getAndIncrement();
      this.element = element;
    }

    public E get() {
      return this.element;
    }

    public boolean remove() {
      boolean removed = BoundedPriorityQueue.this.queue.remove(this);
      if (removed) {
        BoundedPriorityQueue.this.deduct(this);
      }

      return removed;
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        BoundedPriorityQueue<E>.StableElement that = (BoundedPriorityQueue.StableElement) o;
        return this.seqNo == that.seqNo;
      } else {
        return false;
      }
    }

    public int hashCode() {
      return Objects.hash(this.seqNo);
    }
  }
}
