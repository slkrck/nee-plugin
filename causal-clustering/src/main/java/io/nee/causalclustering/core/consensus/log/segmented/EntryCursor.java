/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import io.nee.causalclustering.core.consensus.log.EntryRecord;
import java.io.IOException;
import java.util.Optional;
import org.neo4j.cursor.CursorValue;
import org.neo4j.cursor.IOCursor;

class EntryCursor implements IOCursor<EntryRecord> {

  private final Segments segments;
  private final CursorValue<EntryRecord> currentRecord = new CursorValue();
  private IOCursor<EntryRecord> cursor;
  private OpenEndRangeMap.ValueRange<Long, SegmentFile> segmentRange;
  private long currentIndex;
  private long limit = Long.MAX_VALUE;

  EntryCursor(Segments segments, long logIndex) {
    this.segments = segments;
    this.currentIndex = logIndex - 1L;
  }

  public boolean next() throws IOException {
    ++this.currentIndex;
    if ((this.segmentRange == null || this.currentIndex >= this.limit) && !this.nextSegment()) {
      return false;
    } else if (this.cursor.next()) {
      this.currentRecord.set(this.cursor.get());
      return true;
    } else {
      this.currentRecord.invalidate();
      return false;
    }
  }

  private boolean nextSegment() throws IOException {
    this.segmentRange = this.segments.getForIndex(this.currentIndex);
    Optional<SegmentFile> optionalFile = this.segmentRange.value();
    if (!optionalFile.isPresent()) {
      this.currentRecord.invalidate();
      return false;
    } else {
      SegmentFile file = optionalFile.get();
      IOCursor oldCursor = this.cursor;

      try {
        this.cursor = file.getCursor(this.currentIndex);
      } catch (DisposedException n5) {
        this.currentRecord.invalidate();
        return false;
      }

      if (oldCursor != null) {
        try {
          oldCursor.close();
        } catch (Exception e) {
          e.printStackTrace();
          throw new IOException();
        }
      }

      this.limit = this.segmentRange.limit().orElse(Long.MAX_VALUE);
      return true;
    }
  }

  public void close() throws IOException {
    if (this.cursor != null) {
      this.cursor.close();
    }
  }

  public EntryRecord get() {
    return this.currentRecord.get();
  }
}
