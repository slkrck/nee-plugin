/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.common.ClusteredDatabase;
import io.nee.causalclustering.common.DatabaseTopologyNotifier;
import io.nee.causalclustering.core.consensus.RaftMachine;
import io.nee.causalclustering.core.state.CommandApplicationProcess;
import io.nee.causalclustering.core.state.snapshot.CoreDownloaderService;
import io.nee.causalclustering.messaging.LifecycleMessageHandler;
import java.util.Objects;
import org.neo4j.function.ThrowingAction;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.kernel.recovery.RecoveryFacade;

class CoreDatabase extends ClusteredDatabase {

  CoreDatabase(RaftMachine raftMachine, Database kernelDatabase,
      CommandApplicationProcess commandApplicationProcess,
      LifecycleMessageHandler<?> raftMessageHandler, CoreDownloaderService downloadService,
      RecoveryFacade recoveryFacade,
      Lifecycle clusterComponents,
      CorePanicHandlers panicHandler, CoreBootstrap bootstrap,
      DatabaseTopologyNotifier topologyNotifier) {
    this.addComponent(panicHandler);
    this.addComponent(clusterComponents);
    this.addComponent(LifecycleAdapter.onStart(() ->
    {
      recoveryFacade.recovery(kernelDatabase.getDatabaseLayout());
    }));
    this.addComponent(topologyNotifier);
    Objects.requireNonNull(bootstrap);
    this.addComponent(LifecycleAdapter.onStart(bootstrap::perform));
    this.addComponent(kernelDatabase);
    Objects.requireNonNull(commandApplicationProcess);
    ThrowingAction n10001 = commandApplicationProcess::start;
    Objects.requireNonNull(commandApplicationProcess);
    this.addComponent(LifecycleAdapter.simpleLife(n10001, commandApplicationProcess::stop));
    Objects.requireNonNull(raftMachine);
    this.addComponent(LifecycleAdapter.onStart(raftMachine::postRecoveryActions));
    Objects.requireNonNull(raftMessageHandler);
    this.addComponent(LifecycleAdapter.onStop(raftMessageHandler::stop));
    Objects.requireNonNull(raftMachine);
    this.addComponent(LifecycleAdapter.onStop(raftMachine::stopTimers));
    this.addComponent(downloadService);
  }
}
