/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.tx;

import io.nee.causalclustering.messaging.marshalling.ReplicatedContentHandler;
import io.netty.buffer.ByteBuf;
import io.netty.handler.stream.ChunkedInput;
import java.io.IOException;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;

public class TransactionRepresentationReplicatedTransaction extends ReplicatedTransaction {

  private final TransactionRepresentation tx;
  private final DatabaseId databaseId;

  public TransactionRepresentationReplicatedTransaction(TransactionRepresentation tx,
      DatabaseId databaseId) {
    super(databaseId);
    this.tx = tx;
    this.databaseId = databaseId;
  }

  public DatabaseId databaseId() {
    return this.databaseId;
  }

  public ChunkedInput<ByteBuf> encode() {
    return new ChunkedTransaction(this);
  }

  public TransactionRepresentation extract(TransactionRepresentationExtractor extractor) {
    return extractor.extract(this);
  }

  public TransactionRepresentation tx() {
    return this.tx;
  }

  public void dispatch(ReplicatedContentHandler contentHandler) throws IOException {
    contentHandler.handle(this);
  }

  public String toString() {
    return "TransactionRepresentationReplicatedTransaction{tx=" + this.tx + "}";
  }
}
