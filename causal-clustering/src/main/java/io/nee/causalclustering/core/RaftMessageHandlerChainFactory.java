/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.core.consensus.ContinuousJob;
import io.nee.causalclustering.core.consensus.LeaderAvailabilityHandler;
import io.nee.causalclustering.core.consensus.LeaderAvailabilityTimers;
import io.nee.causalclustering.core.consensus.RaftGroup;
import io.nee.causalclustering.core.consensus.RaftMachine;
import io.nee.causalclustering.core.consensus.RaftMessageMonitoringHandler;
import io.nee.causalclustering.core.consensus.RaftMessageTimerResetMonitor;
import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.state.CommandApplicationProcess;
import io.nee.causalclustering.core.state.RaftMessageApplier;
import io.nee.causalclustering.core.state.snapshot.CoreDownloaderService;
import io.nee.causalclustering.error_handling.DatabasePanicker;
import io.nee.causalclustering.messaging.ComposableMessageHandler;
import io.nee.causalclustering.messaging.LifecycleMessageHandler;
import java.time.Clock;
import java.util.Objects;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

class RaftMessageHandlerChainFactory {

  private final RaftMessageDispatcher raftMessageDispatcher;
  private final CatchupAddressProvider.LeaderOrUpstreamStrategyBasedAddressProvider catchupAddressProvider;
  private final DatabasePanicker panicker;
  private final JobScheduler jobScheduler;
  private final Clock clock;
  private final LogProvider logProvider;
  private final Monitors monitors;
  private final Config config;
  private final NamedDatabaseId namedDatabaseId;

  RaftMessageHandlerChainFactory(JobScheduler jobScheduler, Clock clock, LogProvider logProvider,
      Monitors monitors, Config config,
      RaftMessageDispatcher raftMessageDispatcher,
      CatchupAddressProvider.LeaderOrUpstreamStrategyBasedAddressProvider catchupAddressProvider,
      DatabasePanicker panicker, NamedDatabaseId namedDatabaseId) {
    this.jobScheduler = jobScheduler;
    this.clock = clock;
    this.logProvider = logProvider;
    this.monitors = monitors;
    this.config = config;
    this.raftMessageDispatcher = raftMessageDispatcher;
    this.catchupAddressProvider = catchupAddressProvider;
    this.panicker = panicker;
    this.namedDatabaseId = namedDatabaseId;
  }

  LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> createMessageHandlerChain(
      RaftGroup raftGroup,
      CoreDownloaderService downloaderService,
      CommandApplicationProcess commandApplicationProcess) {
    RaftMessageApplier messageApplier =
        new RaftMessageApplier(this.logProvider, raftGroup.raftMachine(), downloaderService,
            commandApplicationProcess, this.catchupAddressProvider,
            this.panicker);
    ComposableMessageHandler monitoringHandler = RaftMessageMonitoringHandler
        .composable(this.clock, this.monitors);
    ComposableMessageHandler batchingMessageHandler = this.createBatchingHandler();
    LeaderAvailabilityTimers n10000 = raftGroup.getLeaderAvailabilityTimers();
    RaftMessageTimerResetMonitor n10001 = this.monitors
        .newMonitor(RaftMessageTimerResetMonitor.class, new String[0]);
    RaftMachine n10002 = raftGroup.raftMachine();
    Objects.requireNonNull(n10002);
    ComposableMessageHandler leaderAvailabilityHandler = LeaderAvailabilityHandler
        .composable(n10000, n10001, n10002::term);
    ComposableMessageHandler clusterBindingHandler = ClusterBindingHandler
        .composable(this.raftMessageDispatcher, this.logProvider);
    return clusterBindingHandler.compose(leaderAvailabilityHandler).compose(batchingMessageHandler)
        .compose(
            monitoringHandler).apply(messageApplier);
  }

  private ComposableMessageHandler createBatchingHandler() {
    BoundedPriorityQueue.Config inQueueConfig = new BoundedPriorityQueue.Config(
        this.config.get(CausalClusteringSettings.raft_in_queue_size),
        this.config
            .get(CausalClusteringSettings.raft_in_queue_max_bytes));
    BatchingMessageHandler.Config batchConfig =
        new BatchingMessageHandler.Config(
            this.config.get(CausalClusteringSettings.raft_in_queue_max_batch),
            this.config.get(CausalClusteringSettings.raft_in_queue_max_batch_bytes));
    return BatchingMessageHandler
        .composable(inQueueConfig, batchConfig, this::jobFactory, this.logProvider);
  }

  private ContinuousJob jobFactory(Runnable runnable) {
    return new ContinuousJob(this.jobScheduler.threadFactory(Group.RAFT_BATCH_HANDLER), runnable,
        this.logProvider, this.namedDatabaseId.toString());
  }
}
