/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.SettingMigrator;
import org.neo4j.configuration.SettingMigrators;
import org.neo4j.logging.Level;
import org.neo4j.logging.Log;

public class CausalClusteringSettingsMigrator implements SettingMigrator {

  private static Level parseLevel(String value) {
    int levelInt = Integer.parseInt(value);
    if (levelInt == java.util.logging.Level.OFF.intValue()) {
      return Level.NONE;
    } else if (levelInt <= java.util.logging.Level.FINE.intValue()) {
      return Level.DEBUG;
    } else if (levelInt <= java.util.logging.Level.INFO.intValue()) {
      return Level.INFO;
    } else {
      return levelInt <= java.util.logging.Level.WARNING.intValue() ? Level.WARN : Level.ERROR;
    }
  }

  public void migrate(Map<String, String> values, Map<String, String> defaultValues, Log log) {
    this.migrateRoutingTtl(values, log);
    this.migrateDisableMiddlewareLogging(values, log);
    this.migrateMiddlewareLoggingLevelSetting(values, log);
    this.migrateMiddlewareLoggingLevelValue(values, log);
    this.migrateAdvertisedAddresses(values, defaultValues, log);
  }

  private void migrateRoutingTtl(Map<String, String> values, Log log) {
    SettingMigrators.migrateSettingNameChange(values, log, "causal_clustering.cluster_routing_ttl",
        GraphDatabaseSettings.routing_ttl);
  }

  private void migrateDisableMiddlewareLogging(Map<String, String> values, Log log) {
    String oldSetting = "causal_clustering.disable_middleware_logging";
    String newSetting = CausalClusteringSettings.middleware_logging_level.name();
    String value = values.remove(oldSetting);
    if (Objects.equals("true", value)) {
      log.warn("Use of deprecated setting %s. It is replaced by %s", oldSetting, newSetting);
      values.put(newSetting, Level.NONE.toString());
    }
  }

  private void migrateMiddlewareLoggingLevelSetting(Map<String, String> input, Log log) {
    String oldSetting = "causal_clustering.middleware_logging.level";
    String newSetting = CausalClusteringSettings.middleware_logging_level.name();
    String value = input.remove(oldSetting);
    if (StringUtils.isNotBlank(value) && NumberUtils.isParsable(value)) {
      log.warn("Use of deprecated setting %s. It is replaced by %s", oldSetting, newSetting);
      input.put(newSetting, value);
    }
  }

  private void migrateMiddlewareLoggingLevelValue(Map<String, String> values, Log log) {
    String setting = CausalClusteringSettings.middleware_logging_level.name();
    String value = values.get(setting);
    if (StringUtils.isNotBlank(value) && NumberUtils.isParsable(value)) {
      String level = parseLevel(value).toString();
      log.warn("Old value format in %s used. %s migrated to %s", setting, value, level);
      values.put(setting, level);
    }
  }

  private void migrateAdvertisedAddresses(Map<String, String> values,
      Map<String, String> defaultValues, Log log) {
    SettingMigrators.migrateAdvertisedAddressInheritanceChange(values, defaultValues, log,
        CausalClusteringSettings.transaction_listen_address.name(),
        CausalClusteringSettings.transaction_advertised_address.name());
    SettingMigrators.migrateAdvertisedAddressInheritanceChange(values, defaultValues, log,
        CausalClusteringSettings.raft_listen_address.name(),
        CausalClusteringSettings.raft_advertised_address.name());
    SettingMigrators.migrateAdvertisedAddressInheritanceChange(values, defaultValues, log,
        CausalClusteringSettings.discovery_listen_address.name(),
        CausalClusteringSettings.discovery_advertised_address.name());
  }
}
