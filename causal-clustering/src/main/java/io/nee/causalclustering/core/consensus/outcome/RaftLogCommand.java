/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.outcome;

import io.nee.causalclustering.core.consensus.log.RaftLog;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.consensus.log.cache.InFlightCache;
import java.io.IOException;
import org.neo4j.logging.Log;

public interface RaftLogCommand {

  void dispatch(RaftLogCommand.Handler n1) throws IOException;

  void applyTo(RaftLog n1, Log n2) throws IOException;

  void applyTo(InFlightCache n1, Log n2);

  interface Handler {

    void append(long n1, RaftLogEntry... n3) throws IOException;

    void truncate(long n1) throws IOException;

    void prune(long n1);
  }
}
