/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import io.nee.causalclustering.messaging.marshalling.CoreReplicatedContentMarshalV2;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Iterator;
import org.neo4j.cursor.IOCursor;
import org.neo4j.internal.helpers.Args;
import org.neo4j.io.fs.DefaultFileSystemAbstraction;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;
import org.neo4j.time.Clocks;

class DumpSegmentedRaftLog {

  private static final String TO_FILE = "tofile";
  private static final DumpSegmentedRaftLog.Printer SYSTEM_OUT_PRINTER = new DumpSegmentedRaftLog.Printer() {
    public PrintStream getFor(String file) {
      return System.out;
    }

    public void close() {
    }
  };
  private final FileSystemAbstraction fileSystem;
  private final ChannelMarshal<ReplicatedContent> marshal;

  private DumpSegmentedRaftLog(FileSystemAbstraction fileSystem,
      ChannelMarshal<ReplicatedContent> marshal) {
    this.fileSystem = fileSystem;
    this.marshal = marshal;
  }

  public static void main(String[] args) throws Exception {
    Args arguments = Args.withFlags(new String[]{"tofile"}).parse(args);
    DumpSegmentedRaftLog.Printer printer = getPrinter(arguments);

    try {
      Iterator n3 = arguments.orphans().iterator();

      while (n3.hasNext()) {
        String fileAsString = (String) n3.next();
        System.out.println("Reading file " + fileAsString);

        try {
          DefaultFileSystemAbstraction fileSystem = new DefaultFileSystemAbstraction();

          try {
            (new DumpSegmentedRaftLog(fileSystem, new CoreReplicatedContentMarshalV2()))
                .dump(fileAsString, printer.getFor(fileAsString));
          } catch (IOException n10) {
            try {
              fileSystem.close();
            } catch (Throwable n9) {
              n10.addSuppressed(n9);
            }

            throw n10;
          }

          fileSystem.close();
        } catch (DisposedException | DamagedLogStorageException | IOException n11) {
          n11.printStackTrace();
        }
      }
    } catch (IOException n12) {
      if (printer != null) {
        try {
          printer.close();
        } catch (Throwable n8) {
          n12.addSuppressed(n8);
        }
      }

      throw n12;
    }

    if (printer != null) {
      printer.close();
    }
  }

  private static DumpSegmentedRaftLog.Printer getPrinter(Args args) {
    boolean toFile = args.getBoolean("tofile", false, true);
    return toFile ? new FilePrinter() : SYSTEM_OUT_PRINTER;
  }

  private int dump(String filenameOrDirectory, PrintStream out) throws Exception {
    LogProvider logProvider = NullLogProvider.getInstance();
    int[] logsFound = new int[]{0};
    FileNames fileNames = new FileNames(new File(filenameOrDirectory));
    ReaderPool readerPool = new ReaderPool(0, logProvider, fileNames, this.fileSystem,
        Clocks.systemClock());
    RecoveryProtocol recoveryProtocol = new RecoveryProtocol(this.fileSystem, fileNames, readerPool,
        (ignored) ->
        {
          return this.marshal;
        }, logProvider);
    Segments segments = recoveryProtocol.run().segments;
    segments.visit((segment) ->
    {
      int n10002 = logsFound[0]++;
      out.println("=== " + segment.getFilename() + " ===");
      SegmentHeader header = segment.header();
      out.println(header.toString());

      try {
        IOCursor cursor = segment.getCursor(header.prevIndex() + 1L);

        try {
          while (cursor.next()) {
            out.println(cursor.get().toString());
          }
        } catch (Exception n8) {
          if (cursor != null) {
            try {
              cursor.close();
            } catch (Throwable n7) {
              n8.addSuppressed(n7);
            }
          }

          throw n8;
        }

        if (cursor != null) {
          cursor.close();
        }

        return false;
      } catch (Exception n9) {
        n9.printStackTrace();
        System.exit(-1);
        return true;
      }
    });
    return logsFound[0];
  }

  interface Printer extends AutoCloseable {

    PrintStream getFor(String n1) throws FileNotFoundException;

    void close();
  }

  private static class FilePrinter implements DumpSegmentedRaftLog.Printer {

    private File directory;
    private PrintStream out;

    public PrintStream getFor(String file) throws FileNotFoundException {
      File absoluteFile = (new File(file)).getAbsoluteFile();
      File dir = absoluteFile.isDirectory() ? absoluteFile : absoluteFile.getParentFile();
      if (!dir.equals(this.directory)) {
        this.close();
        File dumpFile = new File(dir, "dump-logical-log.txt");
        System.out.println("Redirecting the output to " + dumpFile.getPath());
        this.out = new PrintStream(dumpFile);
        this.directory = dir;
      }

      return this.out;
    }

    public void close() {
      if (this.out != null) {
        this.out.close();
      }
    }
  }
}
