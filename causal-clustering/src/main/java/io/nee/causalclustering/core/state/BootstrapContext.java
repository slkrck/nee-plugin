/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.catchup.storecopy.StoreFiles;
import java.io.File;
import java.io.IOException;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;

public class BootstrapContext {

  private final NamedDatabaseId namedDatabaseId;
  private final DatabaseLayout databaseLayout;
  private final StoreFiles storeFiles;
  private final LogFiles transactionLogs;

  public BootstrapContext(NamedDatabaseId namedDatabaseId, DatabaseLayout databaseLayout,
      StoreFiles storeFiles, LogFiles transactionLogs) {
    this.namedDatabaseId = namedDatabaseId;
    this.databaseLayout = databaseLayout;
    this.storeFiles = storeFiles;
    this.transactionLogs = transactionLogs;
  }

  NamedDatabaseId databaseId() {
    return this.namedDatabaseId;
  }

  DatabaseLayout databaseLayout() {
    return this.databaseLayout;
  }

  void replaceWith(File sourceDir) throws IOException {
    this.storeFiles.delete(this.databaseLayout, this.transactionLogs);
    this.storeFiles.moveTo(sourceDir, this.databaseLayout, this.transactionLogs);
  }

  void removeTransactionLogs() {
    this.storeFiles.delete(this.transactionLogs);
  }
}
