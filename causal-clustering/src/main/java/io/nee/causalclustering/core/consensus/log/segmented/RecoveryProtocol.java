/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import io.nee.causalclustering.core.consensus.log.EntryRecord;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.function.Function;
import org.neo4j.cursor.IOCursor;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.PhysicalFlushableChannel;
import org.neo4j.io.fs.ReadAheadChannel;
import org.neo4j.io.fs.StoreChannel;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class RecoveryProtocol {

  private static final SegmentHeader.Marshal headerMarshal = new SegmentHeader.Marshal();
  private final FileSystemAbstraction fileSystem;
  private final FileNames fileNames;
  private final Function<Integer, ChannelMarshal<ReplicatedContent>> marshalSelector;
  private final LogProvider logProvider;
  private final Log log;
  private final ReaderPool readerPool;

  RecoveryProtocol(FileSystemAbstraction fileSystem, FileNames fileNames, ReaderPool readerPool,
      Function<Integer, ChannelMarshal<ReplicatedContent>> marshalSelector,
      LogProvider logProvider) {
    this.fileSystem = fileSystem;
    this.fileNames = fileNames;
    this.readerPool = readerPool;
    this.marshalSelector = marshalSelector;
    this.logProvider = logProvider;
    this.log = logProvider.getLog(this.getClass());
  }

  private static SegmentHeader loadHeader(FileSystemAbstraction fileSystem, File file)
      throws IOException, EndOfStreamException {
    StoreChannel channel = fileSystem.read(file);

    SegmentHeader n4;
    try {
      ByteBuffer buffer = ByteBuffer.allocate(SegmentHeader.CURRENT_RECORD_OFFSET);
      n4 = headerMarshal.unmarshal(new ReadAheadChannel(channel, buffer));
    } catch (Throwable n6) {
      if (channel != null) {
        try {
          channel.close();
        } catch (Throwable n5) {
          n6.addSuppressed(n5);
        }
      }

      throw n6;
    }

    if (channel != null) {
      channel.close();
    }

    return n4;
  }

  private static void writeHeader(FileSystemAbstraction fileSystem, File file, SegmentHeader header)
      throws IOException {
    StoreChannel channel = fileSystem.write(file);

    try {
      channel.position(0L);
      ByteBuffer buffer = ByteBuffer.allocate(SegmentHeader.CURRENT_RECORD_OFFSET);
      PhysicalFlushableChannel writer = new PhysicalFlushableChannel(channel, buffer);
      headerMarshal.marshal(header, writer);
      writer.prepareForFlush().flush();
    } catch (Throwable n7) {
      if (channel != null) {
        try {
          channel.close();
        } catch (Throwable n6) {
          n7.addSuppressed(n6);
        }
      }

      throw n7;
    }

    if (channel != null) {
      channel.close();
    }
  }

  private static void checkSegmentNumberSequence(long fileNameSegmentNumber,
      long expectedSegmentNumber) throws DamagedLogStorageException {
    if (fileNameSegmentNumber != expectedSegmentNumber) {
      throw new DamagedLogStorageException(
          "Segment numbers not strictly monotonic. Expected: %d but found: %d",
          expectedSegmentNumber, fileNameSegmentNumber);
    }
  }

  private static void checkSegmentNumberMatches(long headerSegmentNumber,
      long fileNameSegmentNumber) throws DamagedLogStorageException {
    if (headerSegmentNumber != fileNameSegmentNumber) {
      throw new DamagedLogStorageException(
          "File segment number does not match header. Expected: %d but found: %d",
          headerSegmentNumber, fileNameSegmentNumber);
    }
  }

  State run() throws IOException, DamagedLogStorageException, DisposedException {
    State state = new State();
    SortedMap<Long, File> files = this.fileNames.getAllFiles(this.fileSystem, this.log);
    if (files.entrySet().isEmpty()) {
      state.segments =
          new Segments(this.fileSystem, this.fileNames, this.readerPool, Collections.emptyList(),
              this.marshalSelector, this.logProvider, -1L);
      state.segments.rotate(-1L, -1L, -1L);
      state.terms = new Terms(-1L, -1L);
      return state;
    } else {
      List<SegmentFile> segmentFiles = new ArrayList();
      SegmentFile segment = null;
      long expectedSegmentNumber = files.firstKey();
      boolean mustRecoverLastHeader = false;
      boolean skip = true;

      for (Iterator n9 = files.entrySet().iterator(); n9.hasNext(); ++expectedSegmentNumber) {
        Entry<Long, File> entry = (Entry) n9.next();
        long fileSegmentNumber = entry.getKey();
        File file = entry.getValue();
        checkSegmentNumberSequence(fileSegmentNumber, expectedSegmentNumber);

        SegmentHeader header;
        try {
          header = loadHeader(this.fileSystem, file);
          checkSegmentNumberMatches(header.segmentNumber(), fileSegmentNumber);
        } catch (EndOfStreamException n18) {
          if (files.lastKey() != fileSegmentNumber) {
            throw new DamagedLogStorageException(n18,
                "Intermediate file with incomplete or no header found: %s", file);
          }

          if (files.size() == 1) {
            throw new DamagedLogStorageException(n18,
                "Single file with incomplete or no header found: %s", file);
          }

          mustRecoverLastHeader = true;
          break;
        }

        segment = new SegmentFile(this.fileSystem, file, this.readerPool, fileSegmentNumber,
            this.marshalSelector.apply(header.formatVersion()), this.logProvider, header);
        segmentFiles.add(segment);
        if (segment.header().prevIndex() != segment.header().prevFileLastIndex()) {
          this.log.info(String
              .format("Skipping from index %d to %d.", segment.header().prevFileLastIndex(),
                  segment.header().prevIndex() + 1L));
          skip = true;
        }

        if (skip) {
          state.prevIndex = segment.header().prevIndex();
          state.prevTerm = segment.header().prevTerm();
          skip = false;
        }
      }

      assert segment != null;

      state.appendIndex = segment.header().prevIndex();
      state.terms = new Terms(segment.header().prevIndex(), segment.header().prevTerm());
      IOCursor cursor = segment.getCursor(segment.header().prevIndex() + 1L);

      try {
        while (cursor.next()) {
          EntryRecord entry = (EntryRecord) cursor.get();
          state.appendIndex = entry.logIndex();
          state.terms.append(state.appendIndex, entry.logEntry().term());
        }
      } catch (IOException n17) {
        if (cursor != null) {
          try {
            cursor.close();
          } catch (Throwable n16) {
            n17.addSuppressed(n16);
          }
        }

        throw n17;
      } catch (Exception e) {
        e.printStackTrace();
      }

      if (cursor != null) {

        try {
          cursor.close();
        } catch (Throwable e) {
          throw new IOException(e);
        }
      }

      if (mustRecoverLastHeader) {
        SegmentHeader header = new SegmentHeader(state.appendIndex, expectedSegmentNumber,
            state.appendIndex, state.terms.latest());
        this.log.warn("Recovering last file based on next-to-last file. " + header);
        File file = this.fileNames.getForSegment(expectedSegmentNumber);
        writeHeader(this.fileSystem, file, header);
        segment = new SegmentFile(this.fileSystem, file, this.readerPool, expectedSegmentNumber,
            this.marshalSelector.apply(header.formatVersion()), this.logProvider, header);
        segmentFiles.add(segment);
      }

      state.segments = new Segments(this.fileSystem, this.fileNames, this.readerPool, segmentFiles,
          this.marshalSelector, this.logProvider,
          segment.header().segmentNumber());
      return state;
    }
  }
}
