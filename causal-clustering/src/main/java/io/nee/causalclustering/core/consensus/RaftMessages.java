/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus;

import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.Message;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public interface RaftMessages {

  enum Type {
    VOTE_REQUEST,
    VOTE_RESPONSE,
    APPEND_ENTRIES_REQUEST,
    APPEND_ENTRIES_RESPONSE,
    HEARTBEAT,
    HEARTBEAT_RESPONSE,
    LOG_COMPACTION_INFO,
    ELECTION_TIMEOUT,
    HEARTBEAT_TIMEOUT,
    NEW_ENTRY_REQUEST,
    NEW_BATCH_REQUEST,
    PRUNE_REQUEST,
    PRE_VOTE_REQUEST,
    PRE_VOTE_RESPONSE
  }

  interface ReceivedInstantRaftIdAwareMessage<RM extends RaftMessages.RaftMessage>
      extends RaftMessages.ReceivedInstantAwareMessage<RM>, RaftMessages.RaftIdAwareMessage<RM> {

    static <RM extends RaftMessages.RaftMessage> RaftMessages.ReceivedInstantRaftIdAwareMessage<RM> of(
        Instant receivedAt, RaftId raftId, RM message) {
      return new RaftMessages.ReceivedInstantRaftIdAwareMessageImpl(receivedAt, raftId, message);
    }
  }

  interface ReceivedInstantAwareMessage<RM extends RaftMessages.RaftMessage> extends
      RaftMessages.EnrichedRaftMessage<RM> {

    static <RM extends RaftMessages.RaftMessage> RaftMessages.ReceivedInstantAwareMessage<RM> of(
        Instant receivedAt, RM message) {
      return new RaftMessages.ReceivedInstantAwareMessageImpl(receivedAt, message);
    }

    Instant receivedAt();
  }

  interface RaftIdAwareMessage<RM extends RaftMessages.RaftMessage> extends
      RaftMessages.EnrichedRaftMessage<RM> {

    static <RM extends RaftMessages.RaftMessage> RaftMessages.RaftIdAwareMessage<RM> of(
        RaftId raftId, RM message) {
      return new RaftMessages.RaftIdAwareMessageImpl(raftId, message);
    }

    RaftId raftId();
  }

  interface EnrichedRaftMessage<RM extends RaftMessages.RaftMessage> extends
      RaftMessages.RaftMessage {

    RM message();

    default MemberId from() {
      return this.message().from();
    }

    default RaftMessages.Type type() {
      return this.message().type();
    }

    default <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
      return this.message().dispatch(handler);
    }
  }

  interface NewEntry {

    class BatchRequest extends RaftMessages.BaseRaftMessage {

      private final List<ReplicatedContent> batch;

      public BatchRequest(List<ReplicatedContent> batch) {
        super(null, RaftMessages.Type.NEW_BATCH_REQUEST);
        this.batch = batch;
      }

      public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
        return handler.handle(this);
      }

      public boolean equals(Object o) {
        if (this == o) {
          return true;
        } else if (o != null && this.getClass() == o.getClass()) {
          if (!super.equals(o)) {
            return false;
          } else {
            RaftMessages.NewEntry.BatchRequest batchRequest = (RaftMessages.NewEntry.BatchRequest) o;
            return Objects.equals(this.batch, batchRequest.batch);
          }
        } else {
          return false;
        }
      }

      public int hashCode() {
        return Objects.hash(super.hashCode(), this.batch);
      }

      public String toString() {
        return "BatchRequest{batch=" + this.batch + "}";
      }

      public List<ReplicatedContent> contents() {
        return Collections.unmodifiableList(this.batch);
      }
    }

    class Request extends RaftMessages.BaseRaftMessage {

      private final ReplicatedContent content;

      public Request(MemberId from, ReplicatedContent content) {
        super(from, RaftMessages.Type.NEW_ENTRY_REQUEST);
        this.content = content;
      }

      public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
        return handler.handle(this);
      }

      public String toString() {
        return String.format("NewEntry.Request from %s {content=%s}", this.from, this.content);
      }

      public boolean equals(Object o) {
        if (this == o) {
          return true;
        } else if (o != null && this.getClass() == o.getClass()) {
          boolean n10000;
          label35:
          {
            RaftMessages.NewEntry.Request request = (RaftMessages.NewEntry.Request) o;
            if (this.content != null) {
              if (this.content.equals(request.content)) {
                break label35;
              }
            } else if (request.content == null) {
              break label35;
            }

            n10000 = false;
            return n10000;
          }

          n10000 = true;
          return n10000;
        } else {
          return false;
        }
      }

      public int hashCode() {
        return this.content != null ? this.content.hashCode() : 0;
      }

      public ReplicatedContent content() {
        return this.content;
      }
    }
  }

  interface Timeout {

    class Heartbeat extends RaftMessages.BaseRaftMessage {

      public Heartbeat(MemberId from) {
        super(from, RaftMessages.Type.HEARTBEAT_TIMEOUT);
      }

      public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
        return handler.handle(this);
      }

      public String toString() {
        return "Timeout.Heartbeat{}";
      }
    }

    class Election extends RaftMessages.BaseRaftMessage {

      public Election(MemberId from) {
        super(from, RaftMessages.Type.ELECTION_TIMEOUT);
      }

      public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
        return handler.handle(this);
      }

      public String toString() {
        return "Timeout.Election{}";
      }
    }
  }

  interface AppendEntries {

    class Response extends RaftMessages.BaseRaftMessage {

      private final long term;
      private final boolean success;
      private final long matchIndex;
      private final long appendIndex;

      public Response(MemberId from, long term, boolean success, long matchIndex,
          long appendIndex) {
        super(from, RaftMessages.Type.APPEND_ENTRIES_RESPONSE);
        this.term = term;
        this.success = success;
        this.matchIndex = matchIndex;
        this.appendIndex = appendIndex;
      }

      public long term() {
        return this.term;
      }

      public boolean success() {
        return this.success;
      }

      public long matchIndex() {
        return this.matchIndex;
      }

      public long appendIndex() {
        return this.appendIndex;
      }

      public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
        return handler.handle(this);
      }

      public boolean equals(Object o) {
        if (this == o) {
          return true;
        } else if (o != null && this.getClass() == o.getClass()) {
          if (!super.equals(o)) {
            return false;
          } else {
            RaftMessages.AppendEntries.Response response = (RaftMessages.AppendEntries.Response) o;
            return this.term == response.term && this.success == response.success
                && this.matchIndex == response.matchIndex &&
                this.appendIndex == response.appendIndex;
          }
        } else {
          return false;
        }
      }

      public int hashCode() {
        return Objects
            .hash(super.hashCode(), this.term, this.success, this.matchIndex, this.appendIndex);
      }

      public String toString() {
        return String.format(
            "AppendEntries.Response from %s {term=%d, success=%s, matchIndex=%d, appendIndex=%d}",
            this.from, this.term, this.success,
            this.matchIndex, this.appendIndex);
      }
    }

    class Request extends RaftMessages.BaseRaftMessage {

      private final long leaderTerm;
      private final long prevLogIndex;
      private final long prevLogTerm;
      private final RaftLogEntry[] entries;
      private final long leaderCommit;

      public Request(MemberId from, long leaderTerm, long prevLogIndex, long prevLogTerm,
          RaftLogEntry[] entries, long leaderCommit) {
        super(from, RaftMessages.Type.APPEND_ENTRIES_REQUEST);
        Objects.requireNonNull(entries);

        assert (prevLogIndex != -1L || prevLogTerm == -1L) && (prevLogTerm != -1L
            || prevLogIndex == -1L) :
            String.format("prevLogIndex was %d and prevLogTerm was %d", prevLogIndex, prevLogTerm);

        this.entries = entries;
        this.leaderTerm = leaderTerm;
        this.prevLogIndex = prevLogIndex;
        this.prevLogTerm = prevLogTerm;
        this.leaderCommit = leaderCommit;
      }

      public long leaderTerm() {
        return this.leaderTerm;
      }

      public long prevLogIndex() {
        return this.prevLogIndex;
      }

      public long prevLogTerm() {
        return this.prevLogTerm;
      }

      public RaftLogEntry[] entries() {
        return this.entries;
      }

      public long leaderCommit() {
        return this.leaderCommit;
      }

      public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
        return handler.handle(this);
      }

      public boolean equals(Object o) {
        if (this == o) {
          return true;
        } else if (o != null && this.getClass() == o.getClass()) {
          RaftMessages.AppendEntries.Request request = (RaftMessages.AppendEntries.Request) o;
          return Objects.equals(this.leaderTerm, request.leaderTerm) && Objects
              .equals(this.prevLogIndex, request.prevLogIndex) &&
              Objects.equals(this.prevLogTerm, request.prevLogTerm) && Objects
              .equals(this.leaderCommit, request.leaderCommit) &&
              Arrays.equals(this.entries, request.entries);
        } else {
          return false;
        }
      }

      public int hashCode() {
        return Objects.hash(this.leaderTerm, this.prevLogIndex, this.prevLogTerm,
            Arrays.hashCode(this.entries), this.leaderCommit);
      }

      public String toString() {
        return String.format(
            "AppendEntries.Request from %s {leaderTerm=%d, prevLogIndex=%d, prevLogTerm=%d, entry=%s, leaderCommit=%d}",
            this.from,
            this.leaderTerm, this.prevLogIndex, this.prevLogTerm, Arrays.toString(this.entries),
            this.leaderCommit);
      }
    }
  }

  interface PreVote {

    class Response extends RaftMessages.BaseRaftMessage implements RaftMessages.AnyVote.Response {

      private final long term;
      private final boolean voteGranted;

      public Response(MemberId from, long term, boolean voteGranted) {
        super(from, RaftMessages.Type.PRE_VOTE_RESPONSE);
        this.term = term;
        this.voteGranted = voteGranted;
      }

      public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
        return handler.handle(this);
      }

      public boolean equals(Object o) {
        if (this == o) {
          return true;
        } else if (o != null && this.getClass() == o.getClass()) {
          RaftMessages.PreVote.Response response = (RaftMessages.PreVote.Response) o;
          return this.term == response.term && this.voteGranted == response.voteGranted;
        } else {
          return false;
        }
      }

      public int hashCode() {
        int result = (int) this.term;
        result = 31 * result + (this.voteGranted ? 1 : 0);
        return result;
      }

      public String toString() {
        return String
            .format("PreVote.Response from %s {term=%d, voteGranted=%s}", this.from, this.term,
                this.voteGranted);
      }

      public long term() {
        return this.term;
      }

      public boolean voteGranted() {
        return this.voteGranted;
      }
    }

    class Request extends RaftMessages.BaseRaftMessage implements RaftMessages.AnyVote.Request {

      private final long term;
      private final MemberId candidate;
      private final long lastLogIndex;
      private final long lastLogTerm;

      public Request(MemberId from, long term, MemberId candidate, long lastLogIndex,
          long lastLogTerm) {
        super(from, RaftMessages.Type.PRE_VOTE_REQUEST);
        this.term = term;
        this.candidate = candidate;
        this.lastLogIndex = lastLogIndex;
        this.lastLogTerm = lastLogTerm;
      }

      public long term() {
        return this.term;
      }

      public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
        return handler.handle(this);
      }

      public boolean equals(Object o) {
        if (this == o) {
          return true;
        } else if (o != null && this.getClass() == o.getClass()) {
          RaftMessages.PreVote.Request request = (RaftMessages.PreVote.Request) o;
          return this.lastLogIndex == request.lastLogIndex
              && this.lastLogTerm == request.lastLogTerm && this.term == request.term &&
              this.candidate.equals(request.candidate);
        } else {
          return false;
        }
      }

      public int hashCode() {
        int result = (int) this.term;
        result = 31 * result + this.candidate.hashCode();
        result = 31 * result + (int) (this.lastLogIndex ^ this.lastLogIndex >>> 32);
        result = 31 * result + (int) (this.lastLogTerm ^ this.lastLogTerm >>> 32);
        return result;
      }

      public String toString() {
        return String.format(
            "PreVote.Request from %s {term=%d, candidate=%s, lastAppended=%d, lastLogTerm=%d}",
            this.from, this.term, this.candidate,
            this.lastLogIndex, this.lastLogTerm);
      }

      public long lastLogTerm() {
        return this.lastLogTerm;
      }

      public long lastLogIndex() {
        return this.lastLogIndex;
      }

      public MemberId candidate() {
        return this.candidate;
      }
    }
  }

  interface Vote {

    class Response extends RaftMessages.BaseRaftMessage implements RaftMessages.AnyVote.Response {

      private final long term;
      private final boolean voteGranted;

      public Response(MemberId from, long term, boolean voteGranted) {
        super(from, RaftMessages.Type.VOTE_RESPONSE);
        this.term = term;
        this.voteGranted = voteGranted;
      }

      public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
        return handler.handle(this);
      }

      public boolean equals(Object o) {
        if (this == o) {
          return true;
        } else if (o != null && this.getClass() == o.getClass()) {
          RaftMessages.Vote.Response response = (RaftMessages.Vote.Response) o;
          return this.term == response.term && this.voteGranted == response.voteGranted;
        } else {
          return false;
        }
      }

      public int hashCode() {
        int result = (int) this.term;
        result = 31 * result + (this.voteGranted ? 1 : 0);
        return result;
      }

      public String toString() {
        return String
            .format("Vote.Response from %s {term=%d, voteGranted=%s}", this.from, this.term,
                this.voteGranted);
      }

      public long term() {
        return this.term;
      }

      public boolean voteGranted() {
        return this.voteGranted;
      }
    }

    class Request extends RaftMessages.BaseRaftMessage implements RaftMessages.AnyVote.Request {

      private final long term;
      private final MemberId candidate;
      private final long lastLogIndex;
      private final long lastLogTerm;

      public Request(MemberId from, long term, MemberId candidate, long lastLogIndex,
          long lastLogTerm) {
        super(from, RaftMessages.Type.VOTE_REQUEST);
        this.term = term;
        this.candidate = candidate;
        this.lastLogIndex = lastLogIndex;
        this.lastLogTerm = lastLogTerm;
      }

      public long term() {
        return this.term;
      }

      public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
        return handler.handle(this);
      }

      public boolean equals(Object o) {
        if (this == o) {
          return true;
        } else if (o != null && this.getClass() == o.getClass()) {
          RaftMessages.Vote.Request request = (RaftMessages.Vote.Request) o;
          return this.lastLogIndex == request.lastLogIndex
              && this.lastLogTerm == request.lastLogTerm && this.term == request.term &&
              this.candidate.equals(request.candidate);
        } else {
          return false;
        }
      }

      public int hashCode() {
        int result = (int) this.term;
        result = 31 * result + this.candidate.hashCode();
        result = 31 * result + (int) (this.lastLogIndex ^ this.lastLogIndex >>> 32);
        result = 31 * result + (int) (this.lastLogTerm ^ this.lastLogTerm >>> 32);
        return result;
      }

      public String toString() {
        return String
            .format("Vote.Request from %s {term=%d, candidate=%s, lastAppended=%d, lastLogTerm=%d}",
                this.from, this.term, this.candidate,
                this.lastLogIndex, this.lastLogTerm);
      }

      public long lastLogTerm() {
        return this.lastLogTerm;
      }

      public long lastLogIndex() {
        return this.lastLogIndex;
      }

      public MemberId candidate() {
        return this.candidate;
      }
    }
  }

  interface AnyVote {

    interface Response {

      long term();

      boolean voteGranted();
    }

    interface Request {

      long term();

      long lastLogTerm();

      long lastLogIndex();

      MemberId candidate();
    }
  }

  interface RaftMessage extends Message {

    MemberId from();

    RaftMessages.Type type();

    <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> n1) throws E;
  }

  interface Handler<T, E extends Exception> {

    T handle(RaftMessages.Vote.Request n1) throws E;

    T handle(RaftMessages.Vote.Response n1) throws E;

    T handle(RaftMessages.PreVote.Request n1) throws E;

    T handle(RaftMessages.PreVote.Response n1) throws E;

    T handle(RaftMessages.AppendEntries.Request n1) throws E;

    T handle(RaftMessages.AppendEntries.Response n1) throws E;

    T handle(RaftMessages.Heartbeat n1) throws E;

    T handle(RaftMessages.HeartbeatResponse n1) throws E;

    T handle(RaftMessages.LogCompactionInfo n1) throws E;

    T handle(RaftMessages.Timeout.Election n1) throws E;

    T handle(RaftMessages.Timeout.Heartbeat n1) throws E;

    T handle(RaftMessages.NewEntry.Request n1) throws E;

    T handle(RaftMessages.NewEntry.BatchRequest n1) throws E;

    T handle(RaftMessages.PruneRequest n1) throws E;
  }

  abstract class BaseRaftMessage implements RaftMessages.RaftMessage {

    protected final MemberId from;
    private final RaftMessages.Type type;

    BaseRaftMessage(MemberId from, RaftMessages.Type type) {
      this.from = from;
      this.type = type;
    }

    public MemberId from() {
      return this.from;
    }

    public RaftMessages.Type type() {
      return this.type;
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        RaftMessages.BaseRaftMessage that = (RaftMessages.BaseRaftMessage) o;
        return Objects.equals(this.from, that.from) && this.type == that.type;
      } else {
        return false;
      }
    }

    public int hashCode() {
      return Objects.hash(this.from, this.type);
    }
  }

  class PruneRequest extends RaftMessages.BaseRaftMessage {

    private final long pruneIndex;

    public PruneRequest(long pruneIndex) {
      super(null, RaftMessages.Type.PRUNE_REQUEST);
      this.pruneIndex = pruneIndex;
    }

    public long pruneIndex() {
      return this.pruneIndex;
    }

    public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
      return handler.handle(this);
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        if (!super.equals(o)) {
          return false;
        } else {
          RaftMessages.PruneRequest that = (RaftMessages.PruneRequest) o;
          return this.pruneIndex == that.pruneIndex;
        }
      } else {
        return false;
      }
    }

    public int hashCode() {
      return Objects.hash(super.hashCode(), this.pruneIndex);
    }
  }

  class ReceivedInstantRaftIdAwareMessageImpl<RM extends RaftMessages.RaftMessage> implements
      RaftMessages.ReceivedInstantRaftIdAwareMessage<RM> {

    private final Instant receivedAt;
    private final RaftId raftId;
    private final RM message;

    private ReceivedInstantRaftIdAwareMessageImpl(Instant receivedAt, RaftId raftId, RM message) {
      Objects.requireNonNull(message);
      this.raftId = raftId;
      this.receivedAt = receivedAt;
      this.message = message;
    }

    public Instant receivedAt() {
      return this.receivedAt;
    }

    public RaftId raftId() {
      return this.raftId;
    }

    public RM message() {
      return this.message;
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        RaftMessages.ReceivedInstantRaftIdAwareMessageImpl<?> that = (RaftMessages.ReceivedInstantRaftIdAwareMessageImpl) o;
        return Objects.equals(this.receivedAt, that.receivedAt) && Objects
            .equals(this.raftId, that.raftId) &&
            Objects.equals(this.message(), that.message());
      } else {
        return false;
      }
    }

    public int hashCode() {
      return Objects.hash(this.receivedAt, this.raftId, this.message());
    }

    public String toString() {
      return String
          .format("{raftId: %s, receivedAt: %s, message: %s}", this.raftId, this.receivedAt,
              this.message());
    }
  }

  class ReceivedInstantAwareMessageImpl<RM extends RaftMessages.RaftMessage> implements
      RaftMessages.ReceivedInstantAwareMessage<RM> {

    private final Instant receivedAt;
    private final RM message;

    private ReceivedInstantAwareMessageImpl(Instant receivedAt, RM message) {
      Objects.requireNonNull(message);
      this.receivedAt = receivedAt;
      this.message = message;
    }

    public Instant receivedAt() {
      return this.receivedAt;
    }

    public RM message() {
      return this.message;
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        RaftMessages.ReceivedInstantAwareMessageImpl<?> that = (RaftMessages.ReceivedInstantAwareMessageImpl) o;
        return Objects.equals(this.receivedAt, that.receivedAt) && Objects
            .equals(this.message(), that.message());
      } else {
        return false;
      }
    }

    public int hashCode() {
      return Objects.hash(this.receivedAt, this.message());
    }

    public String toString() {
      return String.format("{receivedAt: %s, message: %s}", this.receivedAt, this.message());
    }
  }

  class RaftIdAwareMessageImpl<RM extends RaftMessages.RaftMessage> implements
      RaftMessages.RaftIdAwareMessage<RM> {

    private final RaftId raftId;
    private final RM message;

    private RaftIdAwareMessageImpl(RaftId raftId, RM message) {
      Objects.requireNonNull(message);
      this.raftId = raftId;
      this.message = message;
    }

    public RaftId raftId() {
      return this.raftId;
    }

    public RM message() {
      return this.message;
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        RaftMessages.RaftIdAwareMessageImpl<?> that = (RaftMessages.RaftIdAwareMessageImpl) o;
        return Objects.equals(this.raftId, that.raftId) && Objects
            .equals(this.message(), that.message());
      } else {
        return false;
      }
    }

    public int hashCode() {
      return Objects.hash(this.raftId, this.message());
    }

    public String toString() {
      return String.format("{raftId: %s, message: %s}", this.raftId, this.message());
    }
  }

  class LogCompactionInfo extends RaftMessages.BaseRaftMessage {

    private final long leaderTerm;
    private final long prevIndex;

    public LogCompactionInfo(MemberId from, long leaderTerm, long prevIndex) {
      super(from, RaftMessages.Type.LOG_COMPACTION_INFO);
      this.leaderTerm = leaderTerm;
      this.prevIndex = prevIndex;
    }

    public long leaderTerm() {
      return this.leaderTerm;
    }

    public long prevIndex() {
      return this.prevIndex;
    }

    public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
      return handler.handle(this);
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        if (!super.equals(o)) {
          return false;
        } else {
          RaftMessages.LogCompactionInfo other = (RaftMessages.LogCompactionInfo) o;
          return this.leaderTerm == other.leaderTerm && this.prevIndex == other.prevIndex;
        }
      } else {
        return false;
      }
    }

    public int hashCode() {
      int result = super.hashCode();
      result = 31 * result + (int) (this.leaderTerm ^ this.leaderTerm >>> 32);
      result = 31 * result + (int) (this.prevIndex ^ this.prevIndex >>> 32);
      return result;
    }

    public String toString() {
      return String.format("Log compaction from %s {leaderTerm=%d, prevIndex=%d}", this.from,
          this.leaderTerm, this.prevIndex);
    }
  }

  class HeartbeatResponse extends RaftMessages.BaseRaftMessage {

    public HeartbeatResponse(MemberId from) {
      super(from, RaftMessages.Type.HEARTBEAT_RESPONSE);
    }

    public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
      return handler.handle(this);
    }

    public String toString() {
      return "HeartbeatResponse{from=" + this.from + "}";
    }
  }

  class Heartbeat extends RaftMessages.BaseRaftMessage {

    private final long leaderTerm;
    private final long commitIndex;
    private final long commitIndexTerm;

    public Heartbeat(MemberId from, long leaderTerm, long commitIndex, long commitIndexTerm) {
      super(from, RaftMessages.Type.HEARTBEAT);
      this.leaderTerm = leaderTerm;
      this.commitIndex = commitIndex;
      this.commitIndexTerm = commitIndexTerm;
    }

    public long leaderTerm() {
      return this.leaderTerm;
    }

    public long commitIndex() {
      return this.commitIndex;
    }

    public long commitIndexTerm() {
      return this.commitIndexTerm;
    }

    public <T, E extends Exception> T dispatch(RaftMessages.Handler<T, E> handler) throws E {
      return handler.handle(this);
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        if (!super.equals(o)) {
          return false;
        } else {
          RaftMessages.Heartbeat heartbeat = (RaftMessages.Heartbeat) o;
          return this.leaderTerm == heartbeat.leaderTerm
              && this.commitIndex == heartbeat.commitIndex &&
              this.commitIndexTerm == heartbeat.commitIndexTerm;
        }
      } else {
        return false;
      }
    }

    public int hashCode() {
      int result = super.hashCode();
      result = 31 * result + (int) (this.leaderTerm ^ this.leaderTerm >>> 32);
      result = 31 * result + (int) (this.commitIndex ^ this.commitIndex >>> 32);
      result = 31 * result + (int) (this.commitIndexTerm ^ this.commitIndexTerm >>> 32);
      return result;
    }

    public String toString() {
      return String.format("Heartbeat from %s {leaderTerm=%d, commitIndex=%d, commitIndexTerm=%d}",
          this.from, this.leaderTerm, this.commitIndex,
          this.commitIndexTerm);
    }
  }

  class Directed {

    MemberId to;
    RaftMessages.RaftMessage message;

    public Directed(MemberId to, RaftMessages.RaftMessage message) {
      this.to = to;
      this.message = message;
    }

    public MemberId to() {
      return this.to;
    }

    public RaftMessages.RaftMessage message() {
      return this.message;
    }

    public String toString() {
      return String.format("Directed{to=%s, message=%s}", this.to, this.message);
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        RaftMessages.Directed directed = (RaftMessages.Directed) o;
        return Objects.equals(this.to, directed.to) && Objects
            .equals(this.message, directed.message);
      } else {
        return false;
      }
    }

    public int hashCode() {
      return Objects.hash(this.to, this.message);
    }
  }

  abstract class HandlerAdaptor<T, E extends Exception> implements RaftMessages.Handler<T, E> {

    public T handle(RaftMessages.Vote.Request request) throws E {
      return null;
    }

    public T handle(RaftMessages.Vote.Response response) throws E {
      return null;
    }

    public T handle(RaftMessages.PreVote.Request request) throws E {
      return null;
    }

    public T handle(RaftMessages.PreVote.Response response) throws E {
      return null;
    }

    public T handle(RaftMessages.AppendEntries.Request request) throws E {
      return null;
    }

    public T handle(RaftMessages.AppendEntries.Response response) throws E {
      return null;
    }

    public T handle(RaftMessages.Heartbeat heartbeat) throws E {
      return null;
    }

    public T handle(RaftMessages.HeartbeatResponse heartbeatResponse) throws E {
      return null;
    }

    public T handle(RaftMessages.LogCompactionInfo logCompactionInfo) throws E {
      return null;
    }

    public T handle(RaftMessages.Timeout.Election election) throws E {
      return null;
    }

    public T handle(RaftMessages.Timeout.Heartbeat heartbeat) throws E {
      return null;
    }

    public T handle(RaftMessages.NewEntry.Request request) throws E {
      return null;
    }

    public T handle(RaftMessages.NewEntry.BatchRequest batchRequest) throws E {
      return null;
    }

    public T handle(RaftMessages.PruneRequest pruneRequest) throws E {
      return null;
    }
  }
}
