/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import org.neo4j.internal.helpers.collection.Visitor;

class SizeBasedLogPruningStrategy implements CoreLogPruningStrategy,
    Visitor<SegmentFile, RuntimeException> {

  private final long bytesToKeep;
  private long accumulatedSize;
  private SegmentFile file;

  SizeBasedLogPruningStrategy(long bytesToKeep) {
    this.bytesToKeep = bytesToKeep;
  }

  public synchronized long getIndexToKeep(Segments segments) {
    this.accumulatedSize = 0L;
    this.file = null;
    segments.visitBackwards(this);
    return this.file != null ? this.file.header().prevIndex() + 1L : -1L;
  }

  public boolean visit(SegmentFile segment) throws RuntimeException {
    if (this.accumulatedSize < this.bytesToKeep) {
      this.file = segment;
      this.accumulatedSize += this.file.size();
      return false;
    } else {
      return true;
    }
  }
}
