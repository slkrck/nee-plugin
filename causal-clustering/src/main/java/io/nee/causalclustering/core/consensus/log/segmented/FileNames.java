/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import java.io.File;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.logging.Log;

public class FileNames {

  static final String BASE_FILE_NAME = "raft.log.";
  private static final String VERSION_MATCH = "(0|[1-9]\\d*)";
  private final File baseDirectory;
  private final Pattern logFilePattern;

  public FileNames(File baseDirectory) {
    this.baseDirectory = baseDirectory;
    this.logFilePattern = Pattern.compile("raft.log.(0|[1-9]\\d*)");
  }

  File getForSegment(long version) {
    return new File(this.baseDirectory, "raft.log." + version);
  }

  public SortedMap<Long, File> getAllFiles(FileSystemAbstraction fileSystem, Log log) {
    SortedMap<Long, File> versionFileMap = new TreeMap();
    File[] n4 = fileSystem.listFiles(this.baseDirectory);
    int n5 = n4.length;

    for (int n6 = 0; n6 < n5; ++n6) {
      File file = n4[n6];
      Matcher matcher = this.logFilePattern.matcher(file.getName());
      if (!matcher.matches()) {
        log.warn("Found out of place file: " + file.getName());
      } else {
        versionFileMap.put(Long.valueOf(matcher.group(1)), file);
      }
    }

    return versionFileMap;
  }
}
