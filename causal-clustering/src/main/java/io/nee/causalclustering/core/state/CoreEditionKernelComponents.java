/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.core.state.machines.CoreStateMachines;
import org.neo4j.graphdb.factory.module.id.DatabaseIdContext;
import org.neo4j.kernel.impl.api.CommitProcessFactory;
import org.neo4j.kernel.impl.api.LeaseService;
import org.neo4j.kernel.impl.factory.AccessCapabilityFactory;
import org.neo4j.kernel.impl.locking.Locks;
import org.neo4j.token.TokenHolders;

public class CoreEditionKernelComponents {

  private final CommitProcessFactory commitProcessFactory;
  private final Locks lockManager;
  private final TokenHolders tokenHolders;
  private final DatabaseIdContext idContext;
  private final CoreStateMachines stateMachines;
  private final AccessCapabilityFactory accessCapabilityFactory;
  private final LeaseService leaseService;

  public CoreEditionKernelComponents(CommitProcessFactory commitProcessFactory, Locks lockManager,
      TokenHolders tokenHolders, DatabaseIdContext idContext,
      CoreStateMachines stateMachines, AccessCapabilityFactory accessCapabilityFactory,
      LeaseService leaseService) {
    this.commitProcessFactory = commitProcessFactory;
    this.lockManager = lockManager;
    this.tokenHolders = tokenHolders;
    this.idContext = idContext;
    this.stateMachines = stateMachines;
    this.accessCapabilityFactory = accessCapabilityFactory;
    this.leaseService = leaseService;
  }

  public DatabaseIdContext idContext() {
    return this.idContext;
  }

  public CommitProcessFactory commitProcessFactory() {
    return this.commitProcessFactory;
  }

  public TokenHolders tokenHolders() {
    return this.tokenHolders;
  }

  public Locks lockManager() {
    return this.lockManager;
  }

  public CoreStateMachines stateMachines() {
    return this.stateMachines;
  }

  public AccessCapabilityFactory accessCapabilityFactory() {
    return this.accessCapabilityFactory;
  }

  public LeaseService leaseService() {
    return this.leaseService;
  }
}
