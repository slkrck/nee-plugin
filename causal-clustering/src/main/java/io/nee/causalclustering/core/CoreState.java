/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.SessionTracker;
import io.nee.causalclustering.core.state.CommandDispatcher;
import io.nee.causalclustering.core.state.CoreStateFiles;
import io.nee.causalclustering.core.state.machines.CoreStateMachines;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshot;
import io.nee.causalclustering.core.state.storage.StateStorage;
import java.io.IOException;

public class CoreState {

  private final SessionTracker sessionTracker;
  private final StateStorage<Long> lastFlushedStorage;
  private final CoreStateMachines stateMachines;

  CoreState(SessionTracker sessionTracker, StateStorage<Long> lastFlushedStorage,
      CoreStateMachines stateMachines) {
    this.sessionTracker = sessionTracker;
    this.lastFlushedStorage = lastFlushedStorage;
    this.stateMachines = stateMachines;
  }

  public void augmentSnapshot(CoreSnapshot coreSnapshot) {
    this.stateMachines.augmentSnapshot(coreSnapshot);
    coreSnapshot.add(CoreStateFiles.SESSION_TRACKER, this.sessionTracker.snapshot());
  }

  public void installSnapshot(CoreSnapshot coreSnapshot) {
    this.stateMachines.installSnapshot(coreSnapshot);
    this.sessionTracker.installSnapshot(coreSnapshot.get(CoreStateFiles.SESSION_TRACKER));
  }

  public void flush(long lastApplied) throws IOException {
    this.stateMachines.flush();
    this.sessionTracker.flush();
    this.lastFlushedStorage.writeState(lastApplied);
  }

  public CommandDispatcher commandDispatcher() {
    return this.stateMachines.commandDispatcher();
  }

  public long getLastAppliedIndex() {
    long lastAppliedIndex = this.stateMachines.getLastAppliedIndex();
    long maxFromSession = this.sessionTracker.getLastAppliedIndex();
    return Long.max(lastAppliedIndex, maxFromSession);
  }

  public long getLastFlushed() {
    return this.lastFlushedStorage.getInitialState();
  }
}
