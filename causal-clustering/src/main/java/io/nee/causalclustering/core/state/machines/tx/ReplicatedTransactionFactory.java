/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.tx;

import io.nee.causalclustering.messaging.NetworkReadableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import org.neo4j.function.ThrowingConsumer;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.PhysicalTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryCommand;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryReader;
import org.neo4j.kernel.impl.transaction.log.entry.StorageCommandSerializer;
import org.neo4j.storageengine.api.StorageCommand;

public class ReplicatedTransactionFactory {

  private ReplicatedTransactionFactory() {
    throw new AssertionError("Should not be instantiated");
  }

  public static TransactionRepresentation extractTransactionRepresentation(
      ReplicatedTransaction transactionCommand, byte[] extraHeader,
      LogEntryReader reader) {
    return transactionCommand.extract(
        new ReplicatedTransactionFactory.TransactionRepresentationReader(extraHeader, reader));
  }

  static ReplicatedTransactionFactory.TransactionRepresentationWriter transactionalRepresentationWriter(
      TransactionRepresentation transactionCommand) {
    return new ReplicatedTransactionFactory.TransactionRepresentationWriter(transactionCommand);
  }

  static class TransactionRepresentationWriter {

    private final Iterator<StorageCommand> commands;
    private ThrowingConsumer<WritableChannel, IOException> nextJob;

    private TransactionRepresentationWriter(TransactionRepresentation tx) {
      this.nextJob = (channel) ->
      {
        channel.putLong(tx.getLatestCommittedTxWhenStarted());
        channel.putLong(tx.getTimeStarted());
        channel.putLong(tx.getTimeCommitted());
        channel.putInt(tx.getLeaseId());
        byte[] additionalHeader = tx.additionalHeader();
        if (additionalHeader != null) {
          channel.putInt(additionalHeader.length);
          channel.put(additionalHeader, additionalHeader.length);
        } else {
          channel.putInt(0);
        }
      };
      this.commands = tx.iterator();
    }

    void write(WritableChannel channel) throws IOException {
      this.nextJob.accept(channel);
      if (this.commands.hasNext()) {
        StorageCommand storageCommand = this.commands.next();
        this.nextJob = (c) ->
        {
          (new StorageCommandSerializer(c)).visit(storageCommand);
        };
      } else {
        this.nextJob = null;
      }
    }

    boolean canWrite() {
      return this.nextJob != null;
    }
  }

  private static class TransactionRepresentationReader implements
      TransactionRepresentationExtractor {

    private final byte[] extraHeader;
    private final LogEntryReader reader;

    TransactionRepresentationReader(byte[] extraHeader, LogEntryReader reader) {
      this.extraHeader = extraHeader;
      this.reader = reader;
    }

    public TransactionRepresentation extract(
        TransactionRepresentationReplicatedTransaction replicatedTransaction) {
      PhysicalTransactionRepresentation tx = (PhysicalTransactionRepresentation) replicatedTransaction
          .tx();
      tx.setAdditionalHeader(this.extraHeader);
      return tx;
    }

    public TransactionRepresentation extract(ByteArrayReplicatedTransaction replicatedTransaction) {
      ByteBuf buffer = Unpooled.wrappedBuffer(replicatedTransaction.getTxBytes());
      NetworkReadableChannel channel = new NetworkReadableChannel(buffer);
      return this.read(channel);
    }

    private TransactionRepresentation read(NetworkReadableChannel channel) {
      try {
        long latestCommittedTxWhenStarted = channel.getLong();
        long timeStarted = channel.getLong();
        long timeCommitted = channel.getLong();
        int leaseId = channel.getInt();
        int headerLength = channel.getInt();
        byte[] header;
        if (headerLength == 0) {
          header = this.extraHeader;
        } else {
          header = new byte[headerLength];
        }

        channel.get(header, headerLength);
        LinkedList commands = new LinkedList();

        LogEntryCommand entryRead;
        while ((entryRead = (LogEntryCommand) this.reader.readLogEntry(channel)) != null) {
          commands.add(entryRead.getCommand());
        }

        PhysicalTransactionRepresentation tx = new PhysicalTransactionRepresentation(commands);
        tx.setHeader(header, timeStarted, latestCommittedTxWhenStarted, timeCommitted, leaseId);
        return tx;
      } catch (IOException n14) {
        throw new RuntimeException(n14);
      }
    }
  }
}
