/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log;

import java.util.Objects;
import java.util.Set;
import java.util.function.LongPredicate;
import org.neo4j.internal.helpers.collection.LruCache;

public class RaftLogMetadataCache {

  private final LruCache<Long, RaftLogMetadataCache.RaftLogEntryMetadata> raftLogEntryCache;

  public RaftLogMetadataCache(int logEntryCacheSize) {
    this.raftLogEntryCache = new LruCache("Raft log entry cache", logEntryCacheSize);
  }

  public void clear() {
    this.raftLogEntryCache.clear();
  }

  public RaftLogMetadataCache.RaftLogEntryMetadata getMetadata(long logIndex) {
    return this.raftLogEntryCache.get(logIndex);
  }

  public RaftLogMetadataCache.RaftLogEntryMetadata cacheMetadata(long logIndex, long entryTerm,
      org.neo4j.kernel.impl.transaction.log.LogPosition position) {
    RaftLogMetadataCache.RaftLogEntryMetadata result = new RaftLogMetadataCache.RaftLogEntryMetadata(
        entryTerm, position);
    this.raftLogEntryCache.put(logIndex, result);
    return result;
  }

  public void removeUpTo(long upTo) {
    this.remove((key) ->
    {
      return key <= upTo;
    });
  }

  public void removeUpwardsFrom(long startingFrom) {
    this.remove((key) ->
    {
      return key >= startingFrom;
    });
  }

  private void remove(LongPredicate predicate) {
    Set<Long> keys = this.raftLogEntryCache.keySet();
    Objects.requireNonNull(predicate);
    keys.removeIf(predicate::test);
  }

  public static class RaftLogEntryMetadata {

    private final long entryTerm;
    private final org.neo4j.kernel.impl.transaction.log.LogPosition startPosition;

    public RaftLogEntryMetadata(long entryTerm,
        org.neo4j.kernel.impl.transaction.log.LogPosition startPosition) {
      Objects.requireNonNull(startPosition);
      this.entryTerm = entryTerm;
      this.startPosition = startPosition;
    }

    public long getEntryTerm() {
      return this.entryTerm;
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        RaftLogMetadataCache.RaftLogEntryMetadata that = (RaftLogMetadataCache.RaftLogEntryMetadata) o;
        return this.entryTerm == that.entryTerm && this.startPosition.equals(that.startPosition);
      } else {
        return false;
      }
    }

    public int hashCode() {
      int result = (int) (this.entryTerm ^ this.entryTerm >>> 32);
      result = 31 * result + this.startPosition.hashCode();
      return result;
    }

    public String toString() {
      return "RaftLogEntryMetadata{entryTerm=" + this.entryTerm + ", startPosition="
          + this.startPosition + "}";
    }
  }
}
