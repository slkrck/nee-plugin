/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import com.typesafe.config.ConfigException;
import com.typesafe.config.ConfigFactory;
import io.nee.causalclustering.routing.load_balancing.LoadBalancingPluginLoader;
import io.nee.kernel.impl.enterprise.configuration.EnterpriseEditionSettings;
import java.io.File;
import java.nio.file.Path;
import java.util.Map;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GroupSettingValidator;
import org.neo4j.configuration.connectors.BoltConnector;
import org.neo4j.graphdb.config.Setting;

public class CausalClusterConfigurationValidator implements GroupSettingValidator {

  public String getPrefix() {
    return "causal_clustering";
  }

  public String getDescription() {
    return "Validates causal clustering settings";
  }

  public void validate(Map<Setting<?>, Object> values, Config config) {
    EnterpriseEditionSettings.Mode mode = config.get(EnterpriseEditionSettings.mode);
    if (mode.equals(EnterpriseEditionSettings.Mode.CORE) || mode
        .equals(EnterpriseEditionSettings.Mode.READ_REPLICA)) {
      this.validateInitialDiscoveryMembers(config);
      this.validateBoltConnector(config);
      LoadBalancingPluginLoader.validate(config, null);
      this.validateDeclaredClusterSizes(config);
      this.validateMiddleware(config);
    }
  }

  private void validateMiddleware(Config config) {
    Path akkaConfig = config.get(CausalClusteringSettings.middleware_akka_external_config);
    if (akkaConfig != null) {
      File akkaConfigFile = akkaConfig.toFile();
      if (!akkaConfigFile.exists() || !akkaConfigFile.isFile()) {
        throw new IllegalArgumentException(
            String.format("'%s' must be a file or empty",
                CausalClusteringSettings.middleware_akka_external_config.name()));
      }

      try {
        ConfigFactory.parseFileAnySyntax(akkaConfigFile);
      } catch (ConfigException n5) {
        throw new IllegalArgumentException(String.format("'%s' could not be parsed", akkaConfig),
            n5);
      }
    }
  }

  private void validateDeclaredClusterSizes(Config config) {
    int startup = config.get(CausalClusteringSettings.minimum_core_cluster_size_at_formation);
    int runtime = config.get(CausalClusteringSettings.minimum_core_cluster_size_at_runtime);
    if (runtime > startup) {
      throw new IllegalArgumentException(
          String.format("'%s' must be set greater than or equal to '%s'",
              CausalClusteringSettings.minimum_core_cluster_size_at_formation.name(),
              CausalClusteringSettings.minimum_core_cluster_size_at_runtime.name()));
    }
  }

  private void validateBoltConnector(Config config) {
    if (!config.get(BoltConnector.enabled)) {
      throw new IllegalArgumentException("A Bolt connector must be configured to run a cluster");
    }
  }

  private void validateInitialDiscoveryMembers(Config config) {
    DiscoveryType discoveryType = config.get(CausalClusteringSettings.discovery_type);
    discoveryType.requiredSettings().forEach((setting) ->
    {
      if (!config.isExplicitlySet(setting)) {
        throw new IllegalArgumentException(
            String.format("Missing value for '%s', which is mandatory with '%s=%s'", setting.name(),
                CausalClusteringSettings.discovery_type.name(), discoveryType));
      }
    });
  }
}
