/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus;

import io.nee.causalclustering.core.consensus.roles.follower.FollowerStates;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class Followers {

  private Followers() {
  }

  public static <MEMBER> long quorumAppendIndex(Set<MEMBER> votingMembers,
      FollowerStates<MEMBER> states) {
    TreeMap<Long, Integer> appendedCounts = new TreeMap();
    Iterator<MEMBER> n3 = votingMembers.iterator();

    while (n3.hasNext()) {
      MEMBER member = n3.next();
      long txId = states.get(member).getMatchIndex();
      appendedCounts.merge(txId, 1, (a, b) ->
      {
        return a + b;
      });
    }

    int total = 0;
    Iterator n8 = appendedCounts.descendingMap().entrySet().iterator();

    Entry entry;
    do {
      if (!n8.hasNext()) {
        return -1L;
      }

      entry = (Entry) n8.next();
      total += (Integer) entry.getValue();
    }
    while (!MajorityIncludingSelfQuorum.isQuorum(votingMembers.size(), total));

    return (Long) entry.getKey();
  }
}
