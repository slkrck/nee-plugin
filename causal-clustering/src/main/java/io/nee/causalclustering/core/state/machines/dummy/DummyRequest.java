/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.dummy;

import io.nee.causalclustering.core.state.CommandDispatcher;
import io.nee.causalclustering.core.state.StateMachineResult;
import io.nee.causalclustering.core.state.machines.tx.CoreReplicatedContent;
import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.messaging.marshalling.ByteArrayChunkedEncoder;
import io.nee.causalclustering.messaging.marshalling.ReplicatedContentHandler;
import io.netty.buffer.ByteBuf;
import io.netty.handler.stream.ChunkedInput;
import java.io.IOException;
import java.util.Arrays;
import java.util.OptionalLong;
import java.util.function.Consumer;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class DummyRequest implements CoreReplicatedContent {

  private final byte[] data;

  public DummyRequest(byte[] data) {
    this.data = data;
  }

  public static DummyRequest decode(ByteBuf byteBuf) {
    int length = byteBuf.readableBytes();
    byte[] array = new byte[length];
    byteBuf.readBytes(array);
    return new DummyRequest(array);
  }

  public OptionalLong size() {
    return OptionalLong.of(this.data.length);
  }

  public void dispatch(ReplicatedContentHandler contentHandler) throws IOException {
    contentHandler.handle(this);
  }

  public long byteCount() {
    return this.data != null ? (long) this.data.length : 0L;
  }

  public void dispatch(CommandDispatcher commandDispatcher, long commandIndex,
      Consumer<StateMachineResult> callback) {
    commandDispatcher.dispatch(this, commandIndex, callback);
  }

  public DatabaseId databaseId() {
    return null;
  }

  public ChunkedInput<ByteBuf> encoder() {
    byte[] array = this.data;
    if (array == null) {
      array = new byte[0];
    }

    return new ByteArrayChunkedEncoder(array);
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      DummyRequest that = (DummyRequest) o;
      return Arrays.equals(this.data, that.data);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Arrays.hashCode(this.data);
  }

  public static class Marshal extends SafeChannelMarshal<DummyRequest> {

    public static final DummyRequest.Marshal INSTANCE = new DummyRequest.Marshal();

    public void marshal(DummyRequest dummy, WritableChannel channel) throws IOException {
      if (dummy.data != null) {
        channel.putInt(dummy.data.length);
        channel.put(dummy.data, dummy.data.length);
      } else {
        channel.putInt(0);
      }
    }

    protected DummyRequest unmarshal0(ReadableChannel channel) throws IOException {
      int length = channel.getInt();
      byte[] data;
      if (length > 0) {
        data = new byte[length];
        channel.get(data, length);
      } else {
        data = null;
      }

      return new DummyRequest(data);
    }
  }
}
