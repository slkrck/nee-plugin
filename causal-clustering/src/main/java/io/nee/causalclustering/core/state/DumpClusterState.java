/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.common.state.ClusterStateStorageFactory;
import io.nee.causalclustering.core.state.storage.SimpleStorage;
import io.nee.causalclustering.core.state.storage.StateStorage;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Optional;
import org.neo4j.configuration.Config;
import org.neo4j.io.fs.DefaultFileSystemAbstraction;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.NullLogProvider;
import org.neo4j.logging.internal.DatabaseLogProvider;

public class DumpClusterState {

  private final ClusterStateStorageFactory storageFactory;
  private final PrintStream out;
  private final String databaseToDump;

  DumpClusterState(FileSystemAbstraction fs, File dataDirectory, PrintStream out,
      String databaseToDump) {
    this.storageFactory = newCoreStateStorageService(fs, dataDirectory);
    this.out = out;
    this.databaseToDump = databaseToDump;
  }

  public static void main(String[] args) {
    File dataDirectory;
    Optional databaseToDumpOpt;
    Optional databaseNameOpt;
    if (args.length == 1) {
      dataDirectory = new File(args[0]);
      databaseToDumpOpt = Optional.empty();
      databaseNameOpt = Optional.empty();
    } else if (args.length == 2) {
      dataDirectory = new File(args[0]);
      databaseToDumpOpt = Optional.ofNullable(args[1]);
      databaseNameOpt = Optional.empty();
    } else {
      if (args.length != 3) {
        System.out.println(
            "usage: DumpClusterState <data directory> ?<database to dump> ?<default database name>");
        System.exit(1);
        return;
      }

      dataDirectory = new File(args[0]);
      databaseToDumpOpt = Optional.ofNullable(args[1]);
      databaseNameOpt = Optional.ofNullable(args[2]);
    }

    try {
      DefaultFileSystemAbstraction fileSystem = new DefaultFileSystemAbstraction();

      try {
        String databaseName = (String) databaseNameOpt.orElse("neo4j");
        String databaseToDump = (String) databaseToDumpOpt.orElse(databaseName);
        DumpClusterState dumpTool = new DumpClusterState(fileSystem, dataDirectory, System.out,
            databaseToDump);
        dumpTool.dump();
      } catch (Throwable n9) {
        try {
          fileSystem.close();
        } catch (Throwable n8) {
          n9.addSuppressed(n8);
        }

        throw n9;
      }

      fileSystem.close();
    } catch (Exception n10) {
      System.out.println("[ERROR] We were unable to properly dump cluster state.");
      System.out.println(
          "[ERROR] This usually indicates that the cluster-state folder structure is incomplete or otherwise corrupt.");
    }
  }

  private static ClusterStateStorageFactory newCoreStateStorageService(FileSystemAbstraction fs,
      File dataDirectory) {
    ClusterStateLayout layout = ClusterStateLayout.of(dataDirectory);
    return new ClusterStateStorageFactory(fs, layout, NullLogProvider.getInstance(),
        Config.defaults());
  }

  void dump() {
    LifeSupport life = new LifeSupport();
    life.start();

    try {
      this.dumpSimpleState(CoreStateFiles.CORE_MEMBER_ID,
          this.storageFactory.createMemberIdStorage());
      this.dumpSimpleState(CoreStateFiles.RAFT_ID,
          this.storageFactory.createRaftIdStorage(this.databaseToDump,
              DatabaseLogProvider.nullDatabaseLogProvider()));
      this.dumpState(CoreStateFiles.LAST_FLUSHED,
          this.storageFactory.createLastFlushedStorage(this.databaseToDump, life,
              DatabaseLogProvider.nullDatabaseLogProvider()));
      this.dumpState(CoreStateFiles.LEASE,
          this.storageFactory.createLeaseStorage(this.databaseToDump, life,
              DatabaseLogProvider.nullDatabaseLogProvider()));
      this.dumpState(CoreStateFiles.SESSION_TRACKER,
          this.storageFactory.createSessionTrackerStorage(this.databaseToDump, life,
              DatabaseLogProvider.nullDatabaseLogProvider()));
      this.dumpState(CoreStateFiles.RAFT_MEMBERSHIP,
          this.storageFactory.createRaftMembershipStorage(this.databaseToDump, life,
              DatabaseLogProvider.nullDatabaseLogProvider()));
      this.dumpState(CoreStateFiles.RAFT_TERM,
          this.storageFactory.createRaftTermStorage(this.databaseToDump, life,
              DatabaseLogProvider.nullDatabaseLogProvider()));
      this.dumpState(CoreStateFiles.RAFT_VOTE,
          this.storageFactory.createRaftVoteStorage(this.databaseToDump, life,
              DatabaseLogProvider.nullDatabaseLogProvider()));
    } finally {
      life.shutdown();
    }
  }

  private <E> void dumpState(CoreStateFiles<E> fileType, StateStorage<E> storage) {
    if (storage.exists()) {
      this.out.println(String.format("%s: %s", fileType, storage.getInitialState()));
    }
  }

  private <E> void dumpSimpleState(CoreStateFiles<E> fileType, SimpleStorage<E> storage) {
    if (storage.exists()) {
      String stateStr;
      try {
        stateStr = storage.readState().toString();
      } catch (IOException n5) {
        stateStr = String.format("Error, state unreadable. %s", n5.getMessage());
      }

      this.out.println(String.format("%s: %s", fileType, stateStr));
    }
  }
}
