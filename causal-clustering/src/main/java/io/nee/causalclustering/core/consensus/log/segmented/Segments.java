/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.function.Function;
import org.neo4j.internal.helpers.collection.Visitor;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class Segments implements AutoCloseable {

  private final OpenEndRangeMap<Long, SegmentFile> rangeMap = new OpenEndRangeMap();
  private final List<SegmentFile> allSegments;
  private final Log log;
  private final FileNames fileNames;
  private final Function<Integer, ChannelMarshal<ReplicatedContent>> marshalSelector;
  private final LogProvider logProvider;
  private final ReaderPool readerPool;
  private final FileSystemAbstraction fileSystem;
  private long currentVersion;

  Segments(FileSystemAbstraction fileSystem, FileNames fileNames, ReaderPool readerPool,
      List<SegmentFile> allSegments,
      Function<Integer, ChannelMarshal<ReplicatedContent>> marshalSelector, LogProvider logProvider,
      long currentVersion) {
    this.fileSystem = fileSystem;
    this.fileNames = fileNames;
    this.allSegments = new ArrayList(allSegments);
    this.marshalSelector = marshalSelector;
    this.logProvider = logProvider;
    this.log = logProvider.getLog(this.getClass());
    this.currentVersion = currentVersion;
    this.readerPool = readerPool;
    this.populateRangeMap();
  }

  private void populateRangeMap() {
    Iterator n1 = this.allSegments.iterator();

    while (n1.hasNext()) {
      SegmentFile segment = (SegmentFile) n1.next();
      this.rangeMap.replaceFrom(segment.header().prevIndex() + 1L, segment);
    }
  }

  synchronized SegmentFile truncate(long prevFileLastIndex, long prevIndex, long prevTerm)
      throws IOException {
    if (prevFileLastIndex < prevIndex) {
      throw new IllegalArgumentException(
          String.format("Cannot truncate at index %d which is after current append index %d",
              prevIndex, prevFileLastIndex));
    } else {
      if (prevFileLastIndex == prevIndex) {
        this.log.warn(String.format("Truncating at current log append index %d", prevIndex));
      }

      return this.createNext(prevFileLastIndex, prevIndex, prevTerm);
    }
  }

  synchronized SegmentFile rotate(long prevFileLastIndex, long prevIndex, long prevTerm)
      throws IOException {
    if (prevFileLastIndex != prevIndex) {
      throw new IllegalArgumentException(String.format(
          "Cannot rotate file and have append index go from %d to %d. Going backwards is a truncation operation, going forwards is a skip operation.",
          prevFileLastIndex, prevIndex));
    } else {
      return this.createNext(prevFileLastIndex, prevIndex, prevTerm);
    }
  }

  synchronized SegmentFile skip(long prevFileLastIndex, long prevIndex, long prevTerm)
      throws IOException {
    if (prevFileLastIndex > prevIndex) {
      throw new IllegalArgumentException(String
          .format("Cannot skip from index %d backwards to index %d", prevFileLastIndex, prevIndex));
    } else {
      if (prevFileLastIndex == prevIndex) {
        this.log.warn(String.format("Skipping at current log append index %d", prevIndex));
      }

      return this.createNext(prevFileLastIndex, prevIndex, prevTerm);
    }
  }

  private synchronized SegmentFile createNext(long prevFileLastIndex, long prevIndex, long prevTerm)
      throws IOException {
    ++this.currentVersion;
    SegmentHeader header = new SegmentHeader(prevFileLastIndex, this.currentVersion, prevIndex,
        prevTerm);
    File file = this.fileNames.getForSegment(this.currentVersion);
    ChannelMarshal<ReplicatedContent> contentMarshal = this.marshalSelector
        .apply(header.formatVersion());
    SegmentFile segment = SegmentFile
        .create(this.fileSystem, file, this.readerPool, this.currentVersion, contentMarshal,
            this.logProvider, header);
    segment.flush();
    this.allSegments.add(segment);
    this.rangeMap.replaceFrom(prevIndex + 1L, segment);
    return segment;
  }

  synchronized OpenEndRangeMap.ValueRange<Long, SegmentFile> getForIndex(long logIndex) {
    return this.rangeMap.lookup(logIndex);
  }

  synchronized SegmentFile last() {
    return this.rangeMap.last();
  }

  public synchronized SegmentFile prune(long pruneIndex) {
    Iterator<SegmentFile> itr = this.allSegments.iterator();
    SegmentFile notDisposed = itr.next();

    int firstRemaining;
    SegmentFile current;
    for (firstRemaining = 0; itr.hasNext(); notDisposed = current) {
      current = itr.next();
      if (current.header().prevFileLastIndex() > pruneIndex || !notDisposed.tryClose()) {
        break;
      }

      this.log.info("Pruning %s", notDisposed);
      if (!notDisposed.delete()) {
        this.log.error("Failed to delete %s", notDisposed);
        break;
      }

      ++firstRemaining;
    }

    this.rangeMap.remove(notDisposed.header().prevIndex() + 1L);
    this.allSegments.subList(0, firstRemaining).clear();
    return notDisposed;
  }

  synchronized void visit(Visitor<SegmentFile, RuntimeException> visitor) {
    ListIterator<SegmentFile> itr = this.allSegments.listIterator();

    for (boolean terminate = false; itr.hasNext() && !terminate;
        terminate = visitor.visit(itr.next())) {
    }
  }

  synchronized void visitBackwards(Visitor<SegmentFile, RuntimeException> visitor) {
    ListIterator<SegmentFile> itr = this.allSegments.listIterator(this.allSegments.size());

    for (boolean terminate = false; itr.hasPrevious() && !terminate;
        terminate = visitor.visit(itr.previous())) {
    }
  }

  public synchronized void close() {
    RuntimeException error = null;
    Iterator n2 = this.allSegments.iterator();

    while (n2.hasNext()) {
      SegmentFile segment = (SegmentFile) n2.next();

      try {
        segment.close();
      } catch (RuntimeException n5) {
        if (error == null) {
          error = n5;
        } else {
          error.addSuppressed(n5);
        }
      }
    }

    if (error != null) {
      throw error;
    }
  }
}
