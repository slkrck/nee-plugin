/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.core.state.storage.SimpleStorage;
import io.nee.causalclustering.core.state.version.ClusterStateVersion;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.Objects;
import org.apache.commons.lang3.ArrayUtils;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ClusterStateMigrator extends LifecycleAdapter {

  private static final ClusterStateVersion CURRENT_VERSION = new ClusterStateVersion(1, 0);
  private final ClusterStateLayout clusterStateLayout;
  private final SimpleStorage<ClusterStateVersion> clusterStateVersionStorage;
  private final FileSystemAbstraction fs;
  private final Log log;

  public ClusterStateMigrator(FileSystemAbstraction fs, ClusterStateLayout clusterStateLayout,
      SimpleStorage<ClusterStateVersion> clusterStateVersionStorage,
      LogProvider logProvider) {
    this.clusterStateLayout = clusterStateLayout;
    this.clusterStateVersionStorage = clusterStateVersionStorage;
    this.fs = fs;
    this.log = logProvider.getLog(this.getClass());
  }

  private static void validatePersistedClusterStateVersion(ClusterStateVersion persistedVersion) {
    if (!Objects.equals(persistedVersion, CURRENT_VERSION)) {
      throw new IllegalStateException("Illegal cluster state version: " + persistedVersion
          + ". Migration for this version does not exist");
    }
  }

  public void init() {
    ClusterStateVersion persistedVersion = this.readClusterStateVersion();
    this.log.info("Persisted cluster state version is: %s", persistedVersion);
    if (persistedVersion == null) {
      this.migrateWhenClusterStateVersionIsAbsent();
    } else {
      validatePersistedClusterStateVersion(persistedVersion);
    }
  }

  private void migrateWhenClusterStateVersionIsAbsent() {
    try {
      File[] oldClusterStateFiles = this.fs
          .listFiles(this.clusterStateLayout.getClusterStateDirectory(),
              this::isNotMemberIdStorage);
      if (ArrayUtils.isNotEmpty(oldClusterStateFiles)) {
        File[] n2 = oldClusterStateFiles;
        int n3 = oldClusterStateFiles.length;

        for (int n4 = 0; n4 < n3; ++n4) {
          File oldClusterStateFile = n2[n4];
          this.fs.deleteRecursively(oldClusterStateFile);
        }

        this.log
            .info("Deleted old cluster state entries %s", Arrays.toString(oldClusterStateFiles));
      }

      this.clusterStateVersionStorage.writeState(CURRENT_VERSION);
      this.log.info("Created a version storage for version %s", CURRENT_VERSION);
    } catch (IOException n6) {
      throw new UncheckedIOException("Unable to migrate the cluster state directory", n6);
    }
  }

  private boolean isNotMemberIdStorage(File parentDir, String name) {
    File clusterStateDir = this.clusterStateLayout.getClusterStateDirectory();
    String memberIdDir = this.clusterStateLayout.memberIdStateFile().getParentFile().getName();
    return !parentDir.equals(clusterStateDir) || !name.equals(memberIdDir);
  }

  private ClusterStateVersion readClusterStateVersion() {
    if (this.clusterStateVersionStorage.exists()) {
      try {
        return this.clusterStateVersionStorage.readState();
      } catch (IOException n2) {
        throw new UncheckedIOException("Unable to read cluster state version", n2);
      }
    } else {
      return null;
    }
  }
}
