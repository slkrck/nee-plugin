/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.core.consensus.ContinuousJob;
import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.RaftMessages.ReceivedInstantRaftIdAwareMessage;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.ComposableMessageHandler;
import io.nee.causalclustering.messaging.LifecycleMessageHandler;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import org.neo4j.internal.helpers.ArrayUtil;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class BatchingMessageHandler implements Runnable,
    LifecycleMessageHandler<ReceivedInstantRaftIdAwareMessage<?>> {

  private final LifecycleMessageHandler<ReceivedInstantRaftIdAwareMessage<?>> handler;
  private final Log log;
  private final BoundedPriorityQueue<ReceivedInstantRaftIdAwareMessage<?>> inQueue;
  private final ContinuousJob job;
  private final List<ReplicatedContent> contentBatch;
  private final List<RaftLogEntry> entryBatch;
  private final BatchingMessageHandler.Config batchConfig;
  private final AtomicLong droppedCount;
  private volatile boolean stopped;
  private volatile BoundedPriorityQueue.Result lastResult;

  BatchingMessageHandler(LifecycleMessageHandler<ReceivedInstantRaftIdAwareMessage<?>> handler,
      BoundedPriorityQueue.Config inQueueConfig,
      BatchingMessageHandler.Config batchConfig, Function<Runnable, ContinuousJob> jobFactory,
      LogProvider logProvider) {
    this.lastResult = BoundedPriorityQueue.Result.OK;
    this.droppedCount = new AtomicLong();
    this.handler = handler;
    this.log = logProvider.getLog(this.getClass());
    this.batchConfig = batchConfig;
    this.contentBatch = new ArrayList(batchConfig.maxBatchCount);
    this.entryBatch = new ArrayList(batchConfig.maxBatchCount);
    this.inQueue =
        new BoundedPriorityQueue<ReceivedInstantRaftIdAwareMessage<?>>(inQueueConfig,
            ContentSize::of, new BatchingMessageHandler.MessagePriority());
    this.job = jobFactory.apply(this);
  }

  static ComposableMessageHandler composable(BoundedPriorityQueue.Config inQueueConfig,
      BatchingMessageHandler.Config batchConfig,
      Function<Runnable, ContinuousJob> jobSchedulerFactory, LogProvider logProvider) {
    return (delegate) ->
    {
      return new BatchingMessageHandler(delegate, inQueueConfig, batchConfig, jobSchedulerFactory,
          logProvider);
    };
  }

  public void start(RaftId raftId) throws Exception {
    this.handler.start(raftId);
    this.job.start();
  }

  public void stop() throws Exception {
    this.stopped = true;
    this.handler.stop();
    this.job.stop();
  }

  public void handle(ReceivedInstantRaftIdAwareMessage<?> message) {
    if (this.stopped) {
      this.log.debug("This handler has been stopped, dropping the message: %s", message);
    } else {
      BoundedPriorityQueue.Result result = this.inQueue.offer(message);
      this.logQueueState(result);
    }
  }

  private void logQueueState(BoundedPriorityQueue.Result result) {
    if (result != BoundedPriorityQueue.Result.OK) {
      this.droppedCount.incrementAndGet();
    }

    if (result != this.lastResult) {
      if (result == BoundedPriorityQueue.Result.OK) {
        this.log.info("Raft in-queue not dropping messages anymore. Dropped %d messages.",
            this.droppedCount.getAndSet(0L));
      } else {
        this.log.warn("Raft in-queue dropping messages after: " + result);
      }

      this.lastResult = result;
    }
  }

  public void run() {
    Optional baseMessage;
    try {
      baseMessage = this.inQueue.poll(1, TimeUnit.SECONDS);
    } catch (InterruptedException n3) {
      this.log.warn("Not expecting to be interrupted.", n3);
      return;
    }

    if (baseMessage.isPresent()) {
      ReceivedInstantRaftIdAwareMessage batchedMessage =
          ((ReceivedInstantRaftIdAwareMessage) baseMessage.get()).message().dispatch(
              new BatchingHandler((ReceivedInstantRaftIdAwareMessage) baseMessage.get()));
      this.handler.handle(
          batchedMessage == null ? (ReceivedInstantRaftIdAwareMessage) baseMessage.get()
              : batchedMessage);
    }
  }

  private RaftMessages.NewEntry.BatchRequest batchNewEntries(RaftMessages.NewEntry.Request first) {
    this.contentBatch.clear();
    this.contentBatch.add(first.content());
    long totalBytes = first.content().size().orElse(0L);

    while (this.contentBatch.size() < this.batchConfig.maxBatchCount) {
      Optional<BoundedPriorityQueue.Removable<RaftMessages.NewEntry.Request>> peeked = this
          .peekNext(RaftMessages.NewEntry.Request.class);
      if (!peeked.isPresent()) {
        break;
      }

      ReplicatedContent content = ((RaftMessages.NewEntry.Request) ((BoundedPriorityQueue.Removable) peeked
          .get()).get()).content();
      if (content.size().isPresent()
          && totalBytes + content.size().getAsLong() > this.batchConfig.maxBatchBytes) {
        break;
      }

      this.contentBatch.add(content);
      boolean removed = peeked.get().remove();

      assert removed;
    }

    return new RaftMessages.NewEntry.BatchRequest(this.contentBatch);
  }

  private RaftMessages.AppendEntries.Request batchAppendEntries(
      RaftMessages.AppendEntries.Request first) {
    this.entryBatch.clear();
    long totalBytes = 0L;
    RaftLogEntry[] n4 = first.entries();
    int n5 = n4.length;

    for (int n6 = 0; n6 < n5; ++n6) {
      RaftLogEntry entry = n4[n6];
      totalBytes += entry.content().size().orElse(0L);
      this.entryBatch.add(entry);
    }

    long leaderCommit = first.leaderCommit();
    long lastTerm = ArrayUtil.lastOf(first.entries()).term();

    while (this.entryBatch.size() < this.batchConfig.maxBatchCount) {
      Optional<BoundedPriorityQueue.Removable<RaftMessages.AppendEntries.Request>> peeked = this
          .peekNext(RaftMessages.AppendEntries.Request.class);
      if (!peeked.isPresent()) {
        break;
      }

      RaftMessages.AppendEntries.Request request = (RaftMessages.AppendEntries.Request) ((BoundedPriorityQueue.Removable) peeked
          .get()).get();
      if (request.entries().length == 0 || !this
          .consecutiveOrigin(first, request, this.entryBatch.size())) {
        break;
      }

      assert lastTerm == request.prevLogTerm();

      RaftLogEntry[] entries = request.entries();
      lastTerm = ArrayUtil.lastOf(entries).term();
      if (entries.length + this.entryBatch.size() > this.batchConfig.maxBatchCount) {
        break;
      }

      long requestBytes = Arrays.stream(entries).mapToLong((entryx) ->
      {
        return entryx.content().size().orElse(0L);
      }).sum();
      if (requestBytes > 0L && totalBytes + requestBytes > this.batchConfig.maxBatchBytes) {
        break;
      }

      this.entryBatch.addAll(Arrays.asList(entries));
      totalBytes += requestBytes;
      leaderCommit = Long.max(leaderCommit, request.leaderCommit());
      boolean removed = peeked.get().remove();

      assert removed;
    }

    return new RaftMessages.AppendEntries.Request(first.from(), first.leaderTerm(),
        first.prevLogIndex(), first.prevLogTerm(),
        this.entryBatch.toArray(RaftLogEntry.empty), leaderCommit);
  }

  private boolean consecutiveOrigin(RaftMessages.AppendEntries.Request first,
      RaftMessages.AppendEntries.Request request, int currentSize) {
    if (request.leaderTerm() != first.leaderTerm()) {
      return false;
    } else {
      return request.prevLogIndex() == first.prevLogIndex() + (long) currentSize;
    }
  }

  private <M> Optional<BoundedPriorityQueue.Removable<M>> peekNext(Class<M> acceptedType) {
    return this.inQueue.peek().filter((r) ->
    {
      return acceptedType.isInstance(((ReceivedInstantRaftIdAwareMessage) r.get()).message());
    }).map((r) ->
    {
      return r.map((m) ->
      {
        return acceptedType.cast(m.message());
      });
    });
  }

  private static class ContentSize extends RaftMessages.HandlerAdaptor<Long, RuntimeException> {

    private static final ContentSize INSTANCE = new ContentSize();

    static long of(ReceivedInstantRaftIdAwareMessage<?> message) {
      Long dispatch = message.dispatch(INSTANCE);
      return dispatch == null ? 0L : dispatch;
    }

    public Long handle(RaftMessages.NewEntry.Request request) throws RuntimeException {
      return request.content().size().orElse(0L);
    }

    public Long handle(RaftMessages.AppendEntries.Request request) throws RuntimeException {
      long totalSize = 0L;
      RaftLogEntry[] n4 = request.entries();
      int n5 = n4.length;

      for (int n6 = 0; n6 < n5; ++n6) {
        RaftLogEntry entry = n4[n6];
        if (entry.content().size().isPresent()) {
          totalSize += entry.content().size().getAsLong();
        }
      }

      return totalSize;
    }
  }

  public static class Config {

    private final int maxBatchCount;
    private final long maxBatchBytes;

    Config(int maxBatchCount, long maxBatchBytes) {
      this.maxBatchCount = maxBatchCount;
      this.maxBatchBytes = maxBatchBytes;
    }
  }

  private class BatchingHandler extends
      RaftMessages.HandlerAdaptor<ReceivedInstantRaftIdAwareMessage, RuntimeException> {

    private final ReceivedInstantRaftIdAwareMessage<?> baseMessage;

    BatchingHandler(ReceivedInstantRaftIdAwareMessage<?> baseMessage) {
      this.baseMessage = baseMessage;
    }

    public ReceivedInstantRaftIdAwareMessage handle(RaftMessages.NewEntry.Request request)
        throws RuntimeException {
      RaftMessages.NewEntry.BatchRequest newEntryBatch = BatchingMessageHandler.this
          .batchNewEntries(request);
      return ReceivedInstantRaftIdAwareMessage
          .of(this.baseMessage.receivedAt(), this.baseMessage.raftId(), newEntryBatch);
    }

    public ReceivedInstantRaftIdAwareMessage handle(RaftMessages.AppendEntries.Request request)
        throws RuntimeException {
      if (request.entries().length == 0) {
        return null;
      } else {
        RaftMessages.AppendEntries.Request appendEntriesBatch = BatchingMessageHandler.this
            .batchAppendEntries(request);
        return ReceivedInstantRaftIdAwareMessage
            .of(this.baseMessage.receivedAt(), this.baseMessage.raftId(), appendEntriesBatch);
      }
    }
  }

  private class MessagePriority extends RaftMessages.HandlerAdaptor<Integer, RuntimeException>
      implements Comparator<ReceivedInstantRaftIdAwareMessage<?>> {

    private final Integer BASE_PRIORITY = 10;

    public Integer handle(RaftMessages.AppendEntries.Request request) {
      return request.entries().length == 0 ? this.BASE_PRIORITY : 20;
    }

    public Integer handle(RaftMessages.NewEntry.Request request) {
      return 30;
    }

    public int compare(ReceivedInstantRaftIdAwareMessage<?> messageA,
        ReceivedInstantRaftIdAwareMessage<?> messageB) {
      int priorityA = this.getPriority(messageA);
      int priorityB = this.getPriority(messageB);
      return Integer.compare(priorityA, priorityB);
    }

    private int getPriority(ReceivedInstantRaftIdAwareMessage<?> message) {
      Integer priority = message.dispatch(this);
      return priority == null ? this.BASE_PRIORITY : priority;
    }
  }
}
