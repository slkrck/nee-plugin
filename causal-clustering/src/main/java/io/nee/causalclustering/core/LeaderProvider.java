/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.core.consensus.LeaderInfo;
import io.nee.causalclustering.core.consensus.LeaderLocator;
import io.nee.causalclustering.core.consensus.NoLeaderFoundException;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.identity.MemberId;
import org.neo4j.kernel.database.NamedDatabaseId;

public class LeaderProvider {

  private final LeaderLocator leaderLocator;
  private final TopologyService topologyService;

  public LeaderProvider(LeaderLocator leaderLocator, TopologyService topologyService) {
    this.leaderLocator = leaderLocator;
    this.topologyService = topologyService;
  }

  public MemberId getLeader(NamedDatabaseId databaseId) throws NoLeaderFoundException {
    LeaderInfo leadMemberFromDiscovery = this.topologyService.getLeader(databaseId);
    LeaderInfo leadMember = null;

    try {
      leadMember = this.leaderLocator.getLeaderInfo();
      if (leadMemberFromDiscovery != null && leadMemberFromDiscovery.term() > leadMember.term()) {
        leadMember = leadMemberFromDiscovery;
      }
    } catch (NoLeaderFoundException n5) {
      if (leadMemberFromDiscovery == null || leadMemberFromDiscovery.memberId() == null) {
        throw new NoLeaderFoundException("Neither Raft nor Discovery has leader", n5);
      }

      leadMember = leadMemberFromDiscovery;
    }

    return leadMember.memberId();
  }
}
