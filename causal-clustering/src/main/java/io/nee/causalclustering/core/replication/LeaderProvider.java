/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.replication;

import io.nee.causalclustering.core.consensus.NoLeaderFoundException;
import io.nee.causalclustering.identity.MemberId;
import java.time.Duration;

class LeaderProvider {

  private final long timeoutMillis;
  private volatile MemberId currentLeader;

  LeaderProvider(Duration leaderAwaitTimeout) {
    this.timeoutMillis = leaderAwaitTimeout.toMillis();
  }

  MemberId awaitLeaderOrThrow() throws InterruptedException, NoLeaderFoundException {
    MemberId leader = this.currentLeader;
    if (leader != null) {
      return leader;
    } else {
      leader = this.awaitLeader();
      if (leader == null) {
        throw new NoLeaderFoundException();
      } else {
        return leader;
      }
    }
  }

  private synchronized MemberId awaitLeader() throws InterruptedException {
    if (this.currentLeader == null) {
      this.wait(this.timeoutMillis);
    }

    return this.currentLeader;
  }

  synchronized void setLeader(MemberId leader) {
    this.currentLeader = leader;
    if (leader != null) {
      this.notifyAll();
    }
  }

  MemberId currentLeader() {
    return this.currentLeader;
  }
}
