/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines;

import io.nee.dbms.ReplicatedDatabaseEventService;
import java.util.function.LongConsumer;
import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.io.pagecache.tracing.cursor.PageCursorTracerSupplier;
import org.neo4j.io.pagecache.tracing.cursor.context.VersionContext;
import org.neo4j.io.pagecache.tracing.cursor.context.VersionContextSupplier;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionToApply;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;
import org.neo4j.kernel.impl.transaction.tracing.CommitEvent;
import org.neo4j.storageengine.api.TransactionApplicationMode;

public class StateMachineCommitHelper {

  private static final LongConsumer NO_OP_COMMIT_CALLBACK = (ignore) ->
  {
  };
  private final CommandIndexTracker commandIndexTracker;
  private final PageCursorTracerSupplier pageCursorTracerSupplier;
  private final VersionContextSupplier versionContextSupplier;
  private final ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch;

  public StateMachineCommitHelper(CommandIndexTracker commandIndexTracker,
      PageCursorTracerSupplier pageCursorTracerSupplier,
      VersionContextSupplier versionContextSupplier,
      ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch) {
    this.commandIndexTracker = commandIndexTracker;
    this.pageCursorTracerSupplier = pageCursorTracerSupplier;
    this.versionContextSupplier = versionContextSupplier;
    this.databaseEventDispatch = databaseEventDispatch;
  }

  public void updateLastAppliedCommandIndex(long commandIndex) {
    this.commandIndexTracker.setAppliedCommandIndex(commandIndex);
  }

  public void commit(TransactionCommitProcess commitProcess, TransactionRepresentation tx,
      long commandIndex) throws TransactionFailureException {
    TransactionToApply txToApply = this
        .newTransactionToApply(tx, commandIndex, NO_OP_COMMIT_CALLBACK);
    this.commit(commitProcess, txToApply);
  }

  public void commit(TransactionCommitProcess commitProcess, TransactionToApply txToApply)
      throws TransactionFailureException {
    commitProcess.commit(txToApply, CommitEvent.NULL, TransactionApplicationMode.EXTERNAL);
    this.pageCursorTracerSupplier.get().reportEvents();
  }

  public TransactionToApply newTransactionToApply(TransactionRepresentation txRepresentation,
      long commandIndex, LongConsumer txCommittedCallback) {
    VersionContext versionContext = this.versionContextSupplier.getVersionContext();
    TransactionToApply txToApply = new TransactionToApply(txRepresentation, versionContext);
    txToApply.onClose((committedTxId) ->
    {
      long latestCommittedTxIdWhenStarted = txRepresentation.getLatestCommittedTxWhenStarted();
      if (latestCommittedTxIdWhenStarted >= committedTxId) {
        throw new IllegalStateException(
            String.format("Out of order transaction. Expected that %d < %d",
                latestCommittedTxIdWhenStarted, committedTxId));
      } else {
        txCommittedCallback.accept(committedTxId);
        this.databaseEventDispatch.fireTransactionCommitted(committedTxId);
        this.updateLastAppliedCommandIndex(commandIndex);
      }
    });
    return txToApply;
  }
}
