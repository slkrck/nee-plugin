/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.replication;

import io.nee.causalclustering.core.state.machines.dummy.DummyRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.logging.Log;
import org.neo4j.procedure.Admin;
import org.neo4j.procedure.Context;
import org.neo4j.procedure.Description;
import org.neo4j.procedure.Mode;
import org.neo4j.procedure.Name;
import org.neo4j.procedure.Procedure;

public class ReplicationBenchmarkProcedure {

  private static long startTime;
  private static List<ReplicationBenchmarkProcedure.Worker> workers;
  @Context
  public Replicator replicator;
  @Context
  public SecurityContext securityContext;
  @Context
  public Log log;

  @Admin
  @Description("Start the benchmark.")
  @Procedure(name = "dbms.cluster.benchmark.start", mode = Mode.DBMS)
  public synchronized void start(@Name("nThreads") Long nThreads,
      @Name("blockSize") Long blockSize) {
    if (workers != null) {
      throw new IllegalStateException("Already running.");
    } else {
      this.log.info("Starting replication benchmark procedure");
      startTime = System.currentTimeMillis();
      workers = new ArrayList(Math.toIntExact(nThreads));

      for (int i = 0; (long) i < nThreads; ++i) {
        ReplicationBenchmarkProcedure.Worker worker = new ReplicationBenchmarkProcedure.Worker(
            Math.toIntExact(blockSize));
        workers.add(worker);
        worker.start();
      }
    }
  }

  @Admin
  @Description("Stop a running benchmark.")
  @Procedure(name = "dbms.cluster.benchmark.stop", mode = Mode.DBMS)
  public synchronized Stream<BenchmarkResult> stop() throws InterruptedException {
    if (workers == null) {
      throw new IllegalStateException("Not running.");
    } else {
      this.log.info("Stopping replication benchmark procedure");
      Iterator n1 = workers.iterator();

      ReplicationBenchmarkProcedure.Worker worker;
      while (n1.hasNext()) {
        worker = (ReplicationBenchmarkProcedure.Worker) n1.next();
        worker.stop();
      }

      n1 = workers.iterator();

      while (n1.hasNext()) {
        worker = (ReplicationBenchmarkProcedure.Worker) n1.next();
        worker.join();
      }

      long runTime = System.currentTimeMillis() - startTime;
      long totalRequests = 0L;
      long totalBytes = 0L;

      //ReplicationBenchmarkProcedure.Worker worker;
      for (Iterator n7 = workers.iterator(); n7.hasNext(); totalBytes += worker.totalBytes) {
        worker = (ReplicationBenchmarkProcedure.Worker) n7.next();
        totalRequests += worker.totalRequests;
      }

      workers = null;
      return Stream.of(new BenchmarkResult(totalRequests, totalBytes, runTime));
    }
  }

  private class Worker implements Runnable {

    private final int blockSize;
    long totalRequests;
    long totalBytes;
    private Thread t;
    private volatile boolean stopped;

    Worker(int blockSize) {
      this.blockSize = blockSize;
    }

    void start() {
      this.t = new Thread(this);
      this.t.start();
    }

    public void run() {
      while (true) {
        try {
          if (!this.stopped) {
            ReplicationResult replicationResult =
                ReplicationBenchmarkProcedure.this.replicator
                    .replicate(new DummyRequest(new byte[this.blockSize]));
            if (replicationResult.outcome() != ReplicationResult.Outcome.APPLIED) {
              throw new RuntimeException("Failed to replicate", replicationResult.failure());
            }

            DummyRequest request = (DummyRequest) replicationResult.stateMachineResult().consume();
            ++this.totalRequests;
            this.totalBytes += request.byteCount();
            continue;
          }
        } catch (Throwable n3) {
          ReplicationBenchmarkProcedure.this.log.error("Worker exception", n3);
        }

        return;
      }
    }

    void stop() {
      this.stopped = true;
    }

    void join() throws InterruptedException {
      this.t.join();
    }
  }
}
