/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.catchup.storecopy.StoreFiles;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class BootstrapSaver {

  private final FileSystemAbstraction fileSystem;
  private final Log log;

  public BootstrapSaver(FileSystemAbstraction fileSystem, LogProvider logProvider) {
    this.fileSystem = fileSystem;
    this.log = logProvider.getLog(this.getClass());
  }

  public void save(DatabaseLayout databaseLayout) throws IOException {
    this.saveFiles(databaseLayout.databaseDirectory());
    this.saveFiles(databaseLayout.getTransactionLogsDirectory());
  }

  public void restore(DatabaseLayout databaseLayout) throws IOException {
    this.restoreFiles(databaseLayout.databaseDirectory());
    this.restoreFiles(databaseLayout.getTransactionLogsDirectory());
  }

  public void clean(DatabaseLayout databaseLayout) throws IOException {
    this.clean(databaseLayout.databaseDirectory());
    this.clean(databaseLayout.getTransactionLogsDirectory());
  }

  private void saveFiles(File directory) throws IOException {
    if (this.hasFiles(directory)) {
      this.log.info("Saving: " + directory);
      this.saveInSelf(directory);
      this.assertExistsAndEmpty(directory);
    }
  }

  private boolean hasFiles(File databaseDirectory) {
    File[] dbFsNodes = this.fileSystem
        .listFiles(databaseDirectory, StoreFiles.EXCLUDE_TEMPORARY_DIRS);
    return dbFsNodes != null && dbFsNodes.length > 0;
  }

  private void assertExistsAndEmpty(File databaseDirectory) {
    File[] dbFsNodes = this.fileSystem
        .listFiles(databaseDirectory, StoreFiles.EXCLUDE_TEMPORARY_DIRS);
    if (dbFsNodes == null || dbFsNodes.length != 0) {
      throw new IllegalStateException("Expected empty directory: " + databaseDirectory);
    }
  }

  private void saveInSelf(File baseDir) throws IOException {
    File tempSavedDir = new File(baseDir, "temp-save");
    if (this.fileSystem.fileExists(tempSavedDir)) {
      throw new IllegalStateException("Directory not expected to exist: " + tempSavedDir);
    } else {
      File tempRenameDir = this.tempRenameDir(baseDir);
      this.fileSystem.renameFile(baseDir, tempRenameDir);
      if (!this.fileSystem.mkdir(baseDir)) {
        throw new IllegalStateException("Failed to create: " + baseDir);
      } else {
        this.fileSystem.renameFile(tempRenameDir, tempSavedDir);
      }
    }
  }

  private void restoreFiles(File directory) throws IOException {
    File tempSaveDir = new File(directory, "temp-save");
    if (this.fileSystem.fileExists(tempSaveDir)) {
      File[] dbFsNodes = this.fileSystem.listFiles(directory, StoreFiles.EXCLUDE_TEMPORARY_DIRS);
      if (dbFsNodes != null) {
        if (dbFsNodes.length > 0) {
          throw new IllegalStateException("Unexpected files in directory: " + directory);
        } else {
          this.log.info("Restoring: " + tempSaveDir);
          this.restoreFromSelf(directory);
        }
      }
    }
  }

  private void restoreFromSelf(File directory) throws IOException {
    File tempRenameDir = this.tempRenameDir(directory);
    File tempSavedDir = new File(directory, "temp-save");
    this.fileSystem.renameFile(tempSavedDir, tempRenameDir);
    File[] fsNodes = this.fileSystem.listFiles(directory, StoreFiles.EXCLUDE_TEMPORARY_DIRS);
    if (fsNodes != null && fsNodes.length == 0) {
      this.fileSystem.deleteRecursively(directory);
      this.fileSystem.renameFile(tempRenameDir, directory);
    } else {
      throw new IllegalStateException("Unexpected state of directory: " + Arrays.toString(fsNodes));
    }
  }

  private File tempRenameDir(File baseDir) {
    String n10002 = baseDir.getPath();
    File tempRenameDir = new File(n10002 + "-" + UUID.randomUUID().toString().substring(0, 8));
    if (this.fileSystem.fileExists(tempRenameDir)) {
      throw new IllegalStateException("Directory conflict: " + tempRenameDir);
    } else {
      return tempRenameDir;
    }
  }

  private void clean(File dbDir) throws IOException {
    File tempSavedDir = new File(dbDir, "temp-save");
    this.log.info("Cleaning: " + tempSavedDir);
    this.fileSystem.deleteRecursively(tempSavedDir);
  }
}
