/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import java.util.function.Supplier;
import org.neo4j.internal.id.IdGeneratorFactory;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.impl.transaction.log.LogicalTransactionStore;
import org.neo4j.storageengine.api.StorageEngine;
import org.neo4j.storageengine.api.TransactionIdStore;

public class CoreKernelResolvers {

  private Database database;

  public void registerDatabase(Database database) {
    this.database = database;
  }

  public Supplier<StorageEngine> storageEngine() {
    return () ->
    {
      return (StorageEngine) this.database.getDependencyResolver()
          .resolveDependency(StorageEngine.class);
    };
  }

  public Supplier<TransactionIdStore> txIdStore() {
    return () ->
    {
      return (TransactionIdStore) this.database.getDependencyResolver()
          .resolveDependency(TransactionIdStore.class);
    };
  }

  public Supplier<LogicalTransactionStore> txStore() {
    return () ->
    {
      return (LogicalTransactionStore) this.database.getDependencyResolver()
          .resolveDependency(LogicalTransactionStore.class);
    };
  }

  public Supplier<IdGeneratorFactory> idGeneratorFactory() {
    return () ->
    {
      return (IdGeneratorFactory) this.database.getDependencyResolver()
          .resolveDependency(IdGeneratorFactory.class);
    };
  }
}
