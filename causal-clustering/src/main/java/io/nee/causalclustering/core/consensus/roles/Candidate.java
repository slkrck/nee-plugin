/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.roles;

import io.nee.causalclustering.core.consensus.MajorityIncludingSelfQuorum;
import io.nee.causalclustering.core.consensus.NewLeaderBarrier;
import io.nee.causalclustering.core.consensus.RaftMessageHandler;
import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.outcome.Outcome;
import io.nee.causalclustering.core.consensus.state.ReadableRaftState;
import java.io.IOException;
import org.neo4j.logging.Log;

class Candidate implements RaftMessageHandler {

  public Outcome handle(RaftMessages.RaftMessage message, ReadableRaftState ctx, Log log)
      throws IOException {
    return message.dispatch(new Handler(ctx, log));
  }

  private static class Handler implements RaftMessages.Handler<Outcome, IOException> {

    private final ReadableRaftState ctx;
    private final Log log;
    private final Outcome outcome;

    private Handler(ReadableRaftState ctx, Log log) {
      this.ctx = ctx;
      this.log = log;
      this.outcome = new Outcome(Role.CANDIDATE, ctx);
    }

    public Outcome handle(RaftMessages.Heartbeat req) throws IOException {
      if (req.leaderTerm() < this.ctx.term()) {
        return this.outcome;
      } else {
        this.outcome.setNextRole(Role.FOLLOWER);
        this.log.info(
            "Moving to FOLLOWER state after receiving heartbeat from %s at term %d (I am at %d)",
            req.from(), req.leaderTerm(), this.ctx.term());
        Heart.beat(this.ctx, this.outcome, req, this.log);
        return this.outcome;
      }
    }

    public Outcome handle(RaftMessages.AppendEntries.Request req) throws IOException {
      if (req.leaderTerm() < this.ctx.term()) {
        RaftMessages.AppendEntries.Response appendResponse =
            new RaftMessages.AppendEntries.Response(this.ctx.myself(), this.ctx.term(), false,
                req.prevLogIndex(),
                this.ctx.entryLog().appendIndex());
        this.outcome.addOutgoingMessage(new RaftMessages.Directed(req.from(), appendResponse));
        return this.outcome;
      } else {
        this.outcome.setNextRole(Role.FOLLOWER);
        this.log.info(
            "Moving to FOLLOWER state after receiving append entries request from %s at term %d (I am at %d)n",
            req.from(), req.leaderTerm(), this.ctx.term());
        Appending.handleAppendEntriesRequest(this.ctx, this.outcome, req, this.log);
        return this.outcome;
      }
    }

    public Outcome handle(RaftMessages.Vote.Response res) throws IOException {
      if (res.term() > this.ctx.term()) {
        this.outcome.setNextTerm(res.term());
        this.outcome.setNextRole(Role.FOLLOWER);
        this.log.info(
            "Moving to FOLLOWER state after receiving vote response from %s at term %d (I am at %d)",
            res.from(), res.term(), this.ctx.term());
        return this.outcome;
      } else if (res.term() >= this.ctx.term() && res.voteGranted()) {
        if (!res.from().equals(this.ctx.myself())) {
          this.outcome.addVoteForMe(res.from());
        }

        if (MajorityIncludingSelfQuorum
            .isQuorum(this.ctx.votingMembers(), this.outcome.getVotesForMe())) {
          this.outcome.setLeader(this.ctx.myself());
          Appending.appendNewEntry(this.ctx, this.outcome, new NewLeaderBarrier());
          Leader.sendHeartbeats(this.ctx, this.outcome);
          this.outcome.setLastLogIndexBeforeWeBecameLeader(this.ctx.entryLog().appendIndex());
          this.outcome.electedLeader();
          this.outcome.renewElectionTimeout();
          this.outcome.setNextRole(Role.LEADER);
          this.log.info("Moving to LEADER state at term %d (I am %s), voted for by %s",
              this.ctx.term(), this.ctx.myself(), this.outcome.getVotesForMe());
        }

        return this.outcome;
      } else {
        return this.outcome;
      }
    }

    public Outcome handle(RaftMessages.Vote.Request req) throws IOException {
      if (req.term() > this.ctx.term()) {
        this.outcome.getVotesForMe().clear();
        this.outcome.setNextRole(Role.FOLLOWER);
        this.log.info(
            "Moving to FOLLOWER state after receiving vote request from %s at term %d (I am at %d)",
            req.from(), req.term(), this.ctx.term());
        Voting.handleVoteRequest(this.ctx, this.outcome, req, this.log);
        return this.outcome;
      } else {
        this.outcome.addOutgoingMessage(
            new RaftMessages.Directed(req.from(),
                new RaftMessages.Vote.Response(this.ctx.myself(), this.outcome.getTerm(), false)));
        return this.outcome;
      }
    }

    public Outcome handle(RaftMessages.Timeout.Election election) throws IOException {
      this.log.info("Failed to get elected. Got votes from: %s", this.ctx.votesForMe());
      if (!Election.startRealElection(this.ctx, this.outcome, this.log)) {
        this.log.info("Moving to FOLLOWER state after failing to start election");
        this.outcome.setNextRole(Role.FOLLOWER);
      }

      return this.outcome;
    }

    public Outcome handle(RaftMessages.PreVote.Request req) throws IOException {
      if (this.ctx.supportPreVoting()) {
        if (req.term() > this.ctx.term()) {
          this.outcome.getVotesForMe().clear();
          this.outcome.setNextRole(Role.FOLLOWER);
          this.log.info(
              "Moving to FOLLOWER state after receiving pre vote request from %s at term %d (I am at %d)",
              req.from(), req.term(), this.ctx.term());
        }

        Voting.declinePreVoteRequest(this.ctx, this.outcome, req);
      }

      return this.outcome;
    }

    public Outcome handle(RaftMessages.PreVote.Response response) {
      return this.outcome;
    }

    public Outcome handle(RaftMessages.AppendEntries.Response response) {
      return this.outcome;
    }

    public Outcome handle(RaftMessages.LogCompactionInfo logCompactionInfo) {
      return this.outcome;
    }

    public Outcome handle(RaftMessages.HeartbeatResponse heartbeatResponse) {
      return this.outcome;
    }

    public Outcome handle(RaftMessages.Timeout.Heartbeat heartbeat) {
      return this.outcome;
    }

    public Outcome handle(RaftMessages.NewEntry.Request request) {
      return this.outcome;
    }

    public Outcome handle(RaftMessages.NewEntry.BatchRequest batchRequest) {
      return this.outcome;
    }

    public Outcome handle(RaftMessages.PruneRequest pruneRequest) {
      Pruning.handlePruneRequest(this.outcome, pruneRequest);
      return this.outcome;
    }
  }
}
