/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.storage;

import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.File;
import java.io.IOException;
import org.neo4j.io.ByteUnit;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.PhysicalFlushableChannel;
import org.neo4j.io.fs.ReadAheadChannel;
import org.neo4j.io.memory.BufferScope;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class SimpleFileStorage<T> implements SimpleStorage<T> {

  private final FileSystemAbstraction fileSystem;
  private final ChannelMarshal<T> marshal;
  private final File file;
  private final Log log;

  public SimpleFileStorage(FileSystemAbstraction fileSystem, File file, ChannelMarshal<T> marshal,
      LogProvider logProvider) {
    this.fileSystem = fileSystem;
    this.log = logProvider.getLog(this.getClass());
    this.file = file;
    this.marshal = marshal;
  }

  public boolean exists() {
    return this.fileSystem.fileExists(this.file);
  }

  public T readState() throws IOException {
    try {
      BufferScope bufferScope = new BufferScope(ReadAheadChannel.DEFAULT_READ_AHEAD_SIZE);

      Object n3;
      try {
        ReadAheadChannel channel = new ReadAheadChannel(this.fileSystem.read(this.file),
            bufferScope.buffer);

        try {
          n3 = this.marshal.unmarshal(channel);
        } catch (Throwable n7) {
          try {
            channel.close();
          } catch (Throwable n6) {
            n7.addSuppressed(n6);
          }

          throw n7;
        }

        channel.close();
      } catch (Throwable n8) {
        try {
          bufferScope.close();
        } catch (Throwable n5) {
          n8.addSuppressed(n5);
        }

        throw n8;
      }

      bufferScope.close();
      return (T) n3;
    } catch (EndOfStreamException n9) {
      this.log.error("End of stream reached: " + this.file);
      throw new IOException(n9);
    }
  }

  public void writeState(T state) throws IOException {
    if (this.file.getParentFile() != null) {
      this.fileSystem.mkdirs(this.file.getParentFile());
    }

    this.fileSystem.deleteFile(this.file);
    BufferScope bufferScope = new BufferScope(Math.toIntExact(ByteUnit.kibiBytes(512L)));

    try {
      PhysicalFlushableChannel channel = new PhysicalFlushableChannel(
          this.fileSystem.write(this.file), bufferScope.buffer);

      try {
        this.marshal.marshal(state, channel);
      } catch (Throwable n8) {
        try {
          channel.close();
        } catch (Throwable n7) {
          n8.addSuppressed(n7);
        }

        throw n8;
      }

      channel.close();
    } catch (Throwable n9) {
      try {
        bufferScope.close();
      } catch (Throwable n6) {
        n9.addSuppressed(n6);
      }

      throw n9;
    }

    bufferScope.close();
  }
}
