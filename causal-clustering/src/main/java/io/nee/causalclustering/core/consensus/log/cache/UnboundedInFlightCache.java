/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.cache;

import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import java.util.HashMap;
import java.util.Map;

public class UnboundedInFlightCache implements InFlightCache {

  private final Map<Long, RaftLogEntry> map = new HashMap();
  private boolean enabled;

  public synchronized void enable() {
    this.enabled = true;
  }

  public synchronized void put(long logIndex, RaftLogEntry entry) {
    if (this.enabled) {
      this.map.put(logIndex, entry);
    }
  }

  public synchronized RaftLogEntry get(long logIndex) {
    return !this.enabled ? null : this.map.get(logIndex);
  }

  public synchronized void truncate(long fromIndex) {
    if (this.enabled) {
      this.map.keySet().removeIf((idx) ->
      {
        return idx >= fromIndex;
      });
    }
  }

  public synchronized void prune(long upToIndex) {
    if (this.enabled) {
      this.map.keySet().removeIf((idx) ->
      {
        return idx <= upToIndex;
      });
    }
  }

  public synchronized long totalBytes() {
    return 0L;
  }

  public synchronized int elementCount() {
    return 0;
  }

  public void reportSkippedCacheAccess() {
  }
}
