/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.token;

import io.nee.causalclustering.core.state.CommandDispatcher;
import io.nee.causalclustering.core.state.StateMachineResult;
import io.nee.causalclustering.core.state.machines.tx.CoreReplicatedContent;
import io.nee.causalclustering.messaging.marshalling.ReplicatedContentHandler;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Consumer;
import org.neo4j.kernel.database.DatabaseId;

public class ReplicatedTokenRequest implements CoreReplicatedContent {

  private final TokenType type;
  private final String tokenName;
  private final byte[] commandBytes;
  private final DatabaseId databaseId;

  public ReplicatedTokenRequest(DatabaseId databaseId, TokenType type, String tokenName,
      byte[] commandBytes) {
    this.type = type;
    this.tokenName = tokenName;
    this.commandBytes = commandBytes;
    this.databaseId = databaseId;
  }

  public TokenType type() {
    return this.type;
  }

  String tokenName() {
    return this.tokenName;
  }

  byte[] commandBytes() {
    return this.commandBytes;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ReplicatedTokenRequest that = (ReplicatedTokenRequest) o;
      if (this.type != that.type) {
        return false;
      } else {
        return this.tokenName.equals(that.tokenName) && Arrays
            .equals(this.commandBytes, that.commandBytes);
      }
    } else {
      return false;
    }
  }

  public int hashCode() {
    int result = this.type.hashCode();
    result = 31 * result + this.tokenName.hashCode();
    result = 31 * result + Arrays.hashCode(this.commandBytes);
    return result;
  }

  public String toString() {
    return String.format("ReplicatedTokenRequest{type='%s', name='%s'}", this.type, this.tokenName);
  }

  public void dispatch(CommandDispatcher commandDispatcher, long commandIndex,
      Consumer<StateMachineResult> callback) {
    commandDispatcher.dispatch(this, commandIndex, callback);
  }

  public void dispatch(ReplicatedContentHandler contentHandler) throws IOException {
    contentHandler.handle(this);
  }

  public DatabaseId databaseId() {
    return this.databaseId;
  }
}
