/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.lease;

import io.nee.causalclustering.core.state.CommandDispatcher;
import io.nee.causalclustering.core.state.StateMachineResult;
import io.nee.causalclustering.core.state.machines.tx.CoreReplicatedContent;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.marshalling.ReplicatedContentHandler;
import java.io.IOException;
import java.util.Objects;
import java.util.function.Consumer;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;

public class ReplicatedLeaseRequest implements CoreReplicatedContent, Lease {

  static final ReplicatedLeaseRequest INVALID_LEASE_REQUEST = new ReplicatedLeaseRequest(null, -1,
      null);
  private final MemberId owner;
  private final int leaseId;
  private final DatabaseId databaseId;

  public ReplicatedLeaseRequest(ReplicatedLeaseState state, NamedDatabaseId namedDatabaseId) {
    this(state.owner(), state.leaseId(), namedDatabaseId.databaseId());
  }

  public ReplicatedLeaseRequest(MemberId owner, int leaseId, DatabaseId databaseId) {
    this.owner = owner;
    this.leaseId = leaseId;
    this.databaseId = databaseId;
  }

  public int id() {
    return this.leaseId;
  }

  public MemberId owner() {
    return this.owner;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ReplicatedLeaseRequest that = (ReplicatedLeaseRequest) o;
      return this.leaseId == that.leaseId && Objects.equals(this.owner, that.owner);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.owner, this.leaseId);
  }

  public String toString() {
    return String.format("ReplicatedLeaseRequest{owner=%s, leaseId=%d}", this.owner, this.leaseId);
  }

  public void dispatch(CommandDispatcher commandDispatcher, long commandIndex,
      Consumer<StateMachineResult> callback) {
    commandDispatcher.dispatch(this, commandIndex, callback);
  }

  public void dispatch(ReplicatedContentHandler contentHandler) throws IOException {
    contentHandler.handle(this);
  }

  public DatabaseId databaseId() {
    return this.databaseId;
  }
}
