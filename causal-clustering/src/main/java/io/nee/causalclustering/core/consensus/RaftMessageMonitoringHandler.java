/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus;

import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.ComposableMessageHandler;
import io.nee.causalclustering.messaging.LifecycleMessageHandler;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import org.neo4j.monitoring.Monitors;

public class RaftMessageMonitoringHandler implements
    LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> {

  private final LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> raftMessageHandler;
  private final Clock clock;
  private final RaftMessageProcessingMonitor raftMessageDelayMonitor;

  public RaftMessageMonitoringHandler(
      LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> raftMessageHandler,
      Clock clock,
      Monitors monitors) {
    this.raftMessageHandler = raftMessageHandler;
    this.clock = clock;
    this.raftMessageDelayMonitor = monitors
        .newMonitor(RaftMessageProcessingMonitor.class, new String[0]);
  }

  public static ComposableMessageHandler composable(Clock clock, Monitors monitors) {
    return (delegate) ->
    {
      return new RaftMessageMonitoringHandler(delegate, clock, monitors);
    };
  }

  public synchronized void handle(
      RaftMessages.ReceivedInstantRaftIdAwareMessage<?> incomingMessage) {
    Instant start = this.clock.instant();
    this.logDelay(incomingMessage, start);
    this.timeHandle(incomingMessage, start);
  }

  private void timeHandle(RaftMessages.ReceivedInstantRaftIdAwareMessage<?> incomingMessage,
      Instant start) {
    boolean n7 = false;

    try {
      n7 = true;
      this.raftMessageHandler.handle(incomingMessage);
      n7 = false;
    } finally {
      if (n7) {
        Duration duration = Duration.between(start, this.clock.instant());
        this.raftMessageDelayMonitor.updateTimer(incomingMessage.type(), duration);
      }
    }

    Duration duration = Duration.between(start, this.clock.instant());
    this.raftMessageDelayMonitor.updateTimer(incomingMessage.type(), duration);
  }

  private void logDelay(RaftMessages.ReceivedInstantRaftIdAwareMessage<?> incomingMessage,
      Instant start) {
    Duration delay = Duration.between(incomingMessage.receivedAt(), start);
    this.raftMessageDelayMonitor.setDelay(delay);
  }

  public void start(RaftId raftId) throws Exception {
    this.raftMessageHandler.start(raftId);
  }

  public void stop() throws Exception {
    this.raftMessageHandler.stop();
  }
}
