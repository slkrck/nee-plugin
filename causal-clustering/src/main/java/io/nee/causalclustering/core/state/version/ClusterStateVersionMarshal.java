/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.version;

import io.nee.causalclustering.core.state.storage.SafeStateMarshal;
import io.nee.causalclustering.messaging.EndOfStreamException;
import java.io.IOException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class ClusterStateVersionMarshal extends SafeStateMarshal<ClusterStateVersion> {

  public void marshal(ClusterStateVersion version, WritableChannel channel) throws IOException {
    channel.putInt(version.major());
    channel.putInt(version.minor());
  }

  protected ClusterStateVersion unmarshal0(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    int major = channel.getInt();
    int minor = channel.getInt();
    return new ClusterStateVersion(major, minor);
  }

  public ClusterStateVersion startState() {
    return null;
  }

  public long ordinal(ClusterStateVersion clusterStateVersion) {
    throw new UnsupportedOperationException(
        "Recovery for " + ClusterStateVersion.class.getSimpleName() + " storage is not required");
  }
}
