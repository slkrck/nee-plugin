/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.StoreChannel;

public class Reader implements Closeable {

  private final long version;
  private final StoreChannel storeChannel;
  private long timeStamp;

  Reader(FileSystemAbstraction fsa, File file, long version) throws IOException {
    this.storeChannel = fsa.read(file);
    this.version = version;
  }

  public long version() {
    return this.version;
  }

  public StoreChannel channel() {
    return this.storeChannel;
  }

  public void close() throws IOException {
    this.storeChannel.close();
  }

  long getTimeStamp() {
    return this.timeStamp;
  }

  void setTimeStamp(long timeStamp) {
    this.timeStamp = timeStamp;
  }
}
