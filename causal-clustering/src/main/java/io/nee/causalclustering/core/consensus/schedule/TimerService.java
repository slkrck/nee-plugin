/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.schedule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public class TimerService {

  protected final JobScheduler scheduler;
  private final Collection<Timer> timers = new ArrayList();
  private final Log log;

  public TimerService(JobScheduler scheduler, LogProvider logProvider) {
    this.scheduler = scheduler;
    this.log = logProvider.getLog(this.getClass());
  }

  public synchronized Timer create(TimerService.TimerName name, Group group,
      TimeoutHandler handler) {
    Timer timer = new Timer(name, this.scheduler, this.log, group, handler);
    this.timers.add(timer);
    return timer;
  }

  public synchronized Collection<Timer> getTimers(TimerService.TimerName name) {
    return this.timers.stream().filter((timer) ->
    {
      return timer.name().equals(name);
    }).collect(Collectors.toList());
  }

  public synchronized void invoke(TimerService.TimerName name) {
    this.getTimers(name).forEach(Timer::invoke);
  }

  public interface TimerName {

    String name();
  }
}
