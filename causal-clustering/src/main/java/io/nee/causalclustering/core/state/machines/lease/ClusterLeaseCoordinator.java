/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.lease;

import io.nee.causalclustering.core.consensus.LeaderLocator;
import io.nee.causalclustering.core.consensus.NoLeaderFoundException;
import io.nee.causalclustering.core.replication.ReplicationResult;
import io.nee.causalclustering.core.replication.Replicator;
import io.nee.causalclustering.identity.MemberId;
import org.neo4j.kernel.api.exceptions.Status.Cluster;
import org.neo4j.kernel.api.exceptions.Status.General;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.api.LeaseClient;
import org.neo4j.kernel.impl.api.LeaseException;
import org.neo4j.kernel.impl.api.LeaseService;

public class ClusterLeaseCoordinator implements LeaseService {

  private static final String NOT_ON_LEADER_ERROR_MESSAGE = "Should only attempt to acquire lease when leader.";
  private final MemberId myself;
  private final Replicator replicator;
  private final LeaderLocator leaderLocator;
  private final ReplicatedLeaseStateMachine leaseStateMachine;
  private final NamedDatabaseId namedDatabaseId;
  private volatile int invalidLeaseId = -1;
  private volatile int myLeaseId = -1;

  public ClusterLeaseCoordinator(MemberId myself, Replicator replicator,
      LeaderLocator leaderLocator, ReplicatedLeaseStateMachine leaseStateMachine,
      NamedDatabaseId namedDatabaseId) {
    this.myself = myself;
    this.replicator = replicator;
    this.leaderLocator = leaderLocator;
    this.leaseStateMachine = leaseStateMachine;
    this.namedDatabaseId = namedDatabaseId;
  }

  public LeaseClient newClient() {
    return new ClusterLeaseClient(this);
  }

  public boolean isInvalid(int leaseId) {
    if (leaseId == -1) {
      throw new IllegalArgumentException("Not a lease");
    } else {
      return leaseId == this.invalidLeaseId || leaseId != this.leaseStateMachine.leaseId()
          || leaseId != this.myLeaseId;
    }
  }

  public synchronized void invalidateLease(int leaseId) {
    if (leaseId != -1 && leaseId == this.leaseStateMachine.leaseId()) {
      this.invalidLeaseId = leaseId;
    }
  }

  private Lease currentLease() {
    ReplicatedLeaseState state = this.leaseStateMachine.snapshot();
    return new ReplicatedLeaseRequest(state, this.namedDatabaseId);
  }

  synchronized int acquireLeaseOrThrow() throws LeaseException {
    Lease currentLease = this.currentLease();
    if (this.myself.equals(currentLease.owner()) && !this.isInvalid(currentLease.id())) {
      return currentLease.id();
    } else {
      this.ensureLeader();
      ReplicatedLeaseRequest leaseRequest =
          new ReplicatedLeaseRequest(this.myself, Lease.nextCandidateId(currentLease.id()),
              this.namedDatabaseId.databaseId());
      ReplicationResult replicationResult = this.replicator.replicate(leaseRequest);
      if (replicationResult.outcome() != ReplicationResult.Outcome.APPLIED) {
        throw new LeaseException("Failed to acquire lease", replicationResult.failure(),
            Cluster.ReplicationFailure);
      } else {
        boolean leaseAcquired;
        try {
          leaseAcquired = (Boolean) replicationResult.stateMachineResult().consume();
        } catch (Exception n6) {
          throw new LeaseException("Unexpected exception", n6, General.UnknownError);
        }

        if (!leaseAcquired) {
          throw new LeaseException(
              "Failed to acquire lease since it was taken by another candidate",
              Cluster.NotALeader);
        } else {
          this.invalidLeaseId = currentLease.id();
          return this.myLeaseId = leaseRequest.id();
        }
      }
    }
  }

  private void ensureLeader() throws LeaseException {
    MemberId leader = null;

    try {
      leader = this.leaderLocator.getLeaderInfo().memberId();
    } catch (NoLeaderFoundException n3) {
    }

    if (!this.myself.equals(leader)) {
      throw new LeaseException("Should only attempt to acquire lease when leader.",
          Cluster.NotALeader);
    }
  }
}
