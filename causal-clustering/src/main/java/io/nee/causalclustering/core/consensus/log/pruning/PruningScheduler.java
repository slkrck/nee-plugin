/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.pruning;

import io.nee.causalclustering.core.state.RaftLogPruner;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import org.neo4j.exceptions.UnderlyingStorageException;
import org.neo4j.function.Predicates;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobHandle;
import org.neo4j.scheduler.JobScheduler;

public class PruningScheduler extends LifecycleAdapter {

  private final RaftLogPruner logPruner;
  private final JobScheduler scheduler;
  private final long recurringPeriodMillis;
  private final Log log;
  private volatile JobHandle handle;
  private volatile boolean stopped;
  private volatile boolean checkPointing;
  private final Runnable job = new Runnable() {
    public void run() {
      try {
        PruningScheduler.this.checkPointing = true;
        if (PruningScheduler.this.stopped) {
          return;
        }

        PruningScheduler.this.logPruner.prune();
      } catch (IOException n5) {
        throw new UnderlyingStorageException(n5);
      } finally {
        PruningScheduler.this.checkPointing = false;
      }

      if (!PruningScheduler.this.stopped) {
        PruningScheduler.this.handle = PruningScheduler.this.scheduler
            .schedule(Group.RAFT_LOG_PRUNING, PruningScheduler.this.job,
                PruningScheduler.this.recurringPeriodMillis, TimeUnit.MILLISECONDS);
      }
    }
  };
  private final BooleanSupplier checkPointingCondition = new BooleanSupplier() {
    public boolean getAsBoolean() {
      return !PruningScheduler.this.checkPointing;
    }
  };

  public PruningScheduler(RaftLogPruner logPruner, JobScheduler scheduler,
      long recurringPeriodMillis, LogProvider logProvider) {
    this.logPruner = logPruner;
    this.scheduler = scheduler;
    this.recurringPeriodMillis = recurringPeriodMillis;
    this.log = logProvider.getLog(this.getClass());
  }

  public void start() {
    this.handle = this.scheduler
        .schedule(Group.RAFT_LOG_PRUNING, this.job, this.recurringPeriodMillis,
            TimeUnit.MILLISECONDS);
  }

  public void stop() {
    this.log.info("PruningScheduler stopping");
    this.stopped = true;
    if (this.handle != null) {
      this.handle.cancel();
    }

    Predicates.awaitForever(this.checkPointingCondition, 100L, TimeUnit.MILLISECONDS);
  }
}
