/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import java.io.File;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.util.Preconditions;

public class ClusterStateLayout {

  private static final String CLUSTER_STATE_DIRECTORY_NAME = "cluster-state";
  private static final String DB_DIRECTORY_NAME = "db";
  private static final String STATE_DIRECTORY_SUFFIX = "-state";
  private final File clusterStateDirectory;

  private ClusterStateLayout(File clusterStateDirectory) {
    this.clusterStateDirectory = clusterStateDirectory;
  }

  public static ClusterStateLayout of(File parentDirectory) {
    return new ClusterStateLayout(new File(parentDirectory, "cluster-state"));
  }

  private static String stateDirectoryName(CoreStateFiles<?> coreStateFiles) {
    return coreStateFiles == CoreStateFiles.RAFT_LOG ? coreStateFiles.name()
        : coreStateFiles.name() + "-state";
  }

  private static void checkScope(CoreStateFiles<?> coreStateFiles, CoreStateFiles.Scope scope) {
    Preconditions
        .checkArgument(coreStateFiles.scope() == scope, "Illegal scope: " + coreStateFiles.scope());
  }

  public File getClusterStateDirectory() {
    return this.clusterStateDirectory;
  }

  public File clusterStateVersionFile() {
    return this.globalClusterStateFile(CoreStateFiles.VERSION);
  }

  public File raftIdStateFile(String databaseName) {
    return this.databaseClusterStateFile(CoreStateFiles.RAFT_ID, databaseName);
  }

  public File memberIdStateFile() {
    return this.globalClusterStateFile(CoreStateFiles.CORE_MEMBER_ID);
  }

  public File leaseStateDirectory(String databaseName) {
    return this.databaseClusterStateDirectory(CoreStateFiles.LEASE, databaseName);
  }

  public File lastFlushedStateDirectory(String databaseName) {
    return this.databaseClusterStateDirectory(CoreStateFiles.LAST_FLUSHED, databaseName);
  }

  public File raftMembershipStateDirectory(String databaseName) {
    return this.databaseClusterStateDirectory(CoreStateFiles.RAFT_MEMBERSHIP, databaseName);
  }

  public File raftLogDirectory(String databaseName) {
    return this.databaseClusterStateDirectory(CoreStateFiles.RAFT_LOG, databaseName);
  }

  public File sessionTrackerDirectory(String databaseName) {
    return this.databaseClusterStateDirectory(CoreStateFiles.SESSION_TRACKER, databaseName);
  }

  public File raftTermStateDirectory(String databaseName) {
    return this.databaseClusterStateDirectory(CoreStateFiles.RAFT_TERM, databaseName);
  }

  public File raftVoteStateDirectory(String databaseName) {
    return this.databaseClusterStateDirectory(CoreStateFiles.RAFT_VOTE, databaseName);
  }

  public Set<File> listGlobalAndDatabaseDirectories(String databaseName,
      Predicate<CoreStateFiles<?>> stateFilesFilter) {
    Stream<File> globalDirectories = CoreStateFiles.values().stream().filter((type) ->
    {
      return type.scope() == CoreStateFiles.Scope.GLOBAL;
    }).filter(stateFilesFilter).map(this::globalClusterStateDirectory);
    Stream<File> databaseDirectories = CoreStateFiles.values().stream().filter((type) ->
    {
      return type.scope() == CoreStateFiles.Scope.DATABASE;
    }).filter(stateFilesFilter).map((type) ->
    {
      return this
          .databaseClusterStateDirectory(
              type,
              databaseName);
    });
    return Stream.concat(globalDirectories, databaseDirectories).collect(Collectors.toSet());
  }

  private File globalClusterStateFile(CoreStateFiles<?> coreStateFiles) {
    checkScope(coreStateFiles, CoreStateFiles.Scope.GLOBAL);
    File directory = this.globalClusterStateDirectory(coreStateFiles);
    return new File(directory, coreStateFiles.name());
  }

  private File databaseClusterStateFile(CoreStateFiles<?> coreStateFiles, String databaseName) {
    checkScope(coreStateFiles, CoreStateFiles.Scope.DATABASE);
    File directory = this.databaseClusterStateDirectory(coreStateFiles, databaseName);
    return new File(directory, coreStateFiles.name());
  }

  private File globalClusterStateDirectory(CoreStateFiles<?> coreStateFiles) {
    checkScope(coreStateFiles, CoreStateFiles.Scope.GLOBAL);
    return new File(this.clusterStateDirectory, stateDirectoryName(coreStateFiles));
  }

  private File databaseClusterStateDirectory(CoreStateFiles<?> coreStateFiles,
      String databaseName) {
    checkScope(coreStateFiles, CoreStateFiles.Scope.DATABASE);
    File databaseDirectory = new File(this.dbDirectory(), databaseName);
    return new File(databaseDirectory, stateDirectoryName(coreStateFiles));
  }

  public File raftGroupDir(String databaseName) {
    return new File(this.dbDirectory(), databaseName);
  }

  private File dbDirectory() {
    return new File(this.clusterStateDirectory, "db");
  }
}
