/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.token;

import io.nee.causalclustering.core.state.StateMachineResult;
import io.nee.causalclustering.core.state.machines.StateMachine;
import io.nee.causalclustering.core.state.machines.StateMachineCommitHelper;
import io.nee.causalclustering.core.state.machines.tx.LogIndexTxHeaderEncoding;
import java.util.Collection;
import java.util.function.Consumer;
import org.neo4j.internal.helpers.collection.Iterables;
import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.transaction.log.PhysicalTransactionRepresentation;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StorageCommand;
import org.neo4j.storageengine.api.StorageCommand.TokenCommand;
import org.neo4j.token.TokenRegistry;

public class ReplicatedTokenStateMachine implements StateMachine<ReplicatedTokenRequest> {

  private final StateMachineCommitHelper commitHelper;
  private final TokenRegistry tokenRegistry;
  private final Log log;
  private TransactionCommitProcess commitProcess;
  private long lastCommittedIndex = -1L;

  public ReplicatedTokenStateMachine(StateMachineCommitHelper commitHelper,
      TokenRegistry tokenRegistry, LogProvider logProvider) {
    this.commitHelper = commitHelper;
    this.tokenRegistry = tokenRegistry;
    this.log = logProvider.getLog(this.getClass());
  }

  public synchronized void installCommitProcess(TransactionCommitProcess commitProcess,
      long lastCommittedIndex) {
    this.commitProcess = commitProcess;
    this.lastCommittedIndex = lastCommittedIndex;
    this.commitHelper.updateLastAppliedCommandIndex(lastCommittedIndex);
    this.log.info(String
        .format("(%s) Updated lastCommittedIndex to %d", this.tokenRegistry.getTokenType(),
            lastCommittedIndex));
  }

  public synchronized void applyCommand(ReplicatedTokenRequest tokenRequest, long commandIndex,
      Consumer<StateMachineResult> callback) {
    if (commandIndex <= this.lastCommittedIndex) {
      this.log.warn(String
          .format("Ignored %s because already committed (%d <= %d).", tokenRequest, commandIndex,
              this.lastCommittedIndex));
    } else {
      Collection<StorageCommand> commands = StorageCommandMarshal
          .bytesToCommands(tokenRequest.commandBytes());
      int newTokenId = this.extractTokenId(commands);
      boolean internal = this.isInternal(commands);
      String name = tokenRequest.tokenName();
      Integer existingTokenId =
          internal ? this.tokenRegistry.getIdInternal(name) : this.tokenRegistry.getId(name);
      if (existingTokenId == null) {
        this.log.info(String.format("Applying %s with newTokenId=%d", tokenRequest, newTokenId));
        this.applyToStore(commands, commandIndex);
        callback.accept(StateMachineResult.of(newTokenId));
      } else {
        this.log.warn(String
            .format("Ignored %s (newTokenId=%d) since it already exists with existingTokenId=%d",
                tokenRequest, newTokenId,
                existingTokenId));
        callback.accept(StateMachineResult.of(existingTokenId));
      }
    }
  }

  private void applyToStore(Collection<StorageCommand> commands, long logIndex) {
    PhysicalTransactionRepresentation representation = new PhysicalTransactionRepresentation(
        commands);
    representation
        .setHeader(LogIndexTxHeaderEncoding.encodeLogIndexAsTxHeader(logIndex), 0L, 0L, 0L, 0);

    try {
      this.commitHelper.commit(this.commitProcess, representation, logIndex);
    } catch (TransactionFailureException n6) {
      throw new RuntimeException(n6);
    }
  }

  private int extractTokenId(Collection<StorageCommand> commands) {
    TokenCommand tokenCommand = this.getSingleTokenCommand(commands);
    return tokenCommand.tokenId();
  }

  private boolean isInternal(Collection<StorageCommand> commands) {
    TokenCommand tokenCommand = this.getSingleTokenCommand(commands);
    return tokenCommand.isInternal();
  }

  private TokenCommand getSingleTokenCommand(Collection<StorageCommand> commands) {
    StorageCommand command = Iterables.single(commands);
    if (!(command instanceof TokenCommand)) {
      throw new IllegalArgumentException("Was expecting a single token command, got " + command);
    } else {
      return (TokenCommand) command;
    }
  }

  public synchronized void flush() {
  }

  public long lastAppliedIndex() {
    if (this.commitProcess == null) {
      throw new IllegalStateException("Value has not been installed");
    } else {
      return this.lastCommittedIndex;
    }
  }
}
