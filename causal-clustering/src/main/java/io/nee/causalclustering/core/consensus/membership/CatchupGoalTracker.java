/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.membership;

import io.nee.causalclustering.core.consensus.log.ReadableRaftLog;
import io.nee.causalclustering.core.consensus.roles.follower.FollowerState;
import java.time.Clock;

class CatchupGoalTracker {

  static final long MAX_ROUNDS = 10L;
  private final ReadableRaftLog raftLog;
  private final Clock clock;
  private final long roundTimeout;
  private final long startTime;
  private final long catchupTimeout;
  private long roundStartTime;
  private long roundCount;
  private long targetIndex;
  private boolean finished;
  private boolean goalAchieved;

  CatchupGoalTracker(ReadableRaftLog raftLog, Clock clock, long roundTimeout, long catchupTimeout) {
    this.raftLog = raftLog;
    this.clock = clock;
    this.roundTimeout = roundTimeout;
    this.catchupTimeout = catchupTimeout;
    this.targetIndex = raftLog.appendIndex();
    this.startTime = clock.millis();
    this.roundStartTime = clock.millis();
    this.roundCount = 1L;
  }

  void updateProgress(FollowerState followerState) {
    if (!this.finished) {
      boolean achievedTarget = followerState.getMatchIndex() >= this.targetIndex;
      if (achievedTarget && this.clock.millis() - this.roundStartTime <= this.roundTimeout) {
        this.goalAchieved = true;
        this.finished = true;
      } else if (this.clock.millis() > this.startTime + this.catchupTimeout) {
        this.finished = true;
      } else if (achievedTarget) {
        if (this.roundCount < 10L) {
          ++this.roundCount;
          this.roundStartTime = this.clock.millis();
          this.targetIndex = this.raftLog.appendIndex();
        } else {
          this.finished = true;
        }
      }
    }
  }

  boolean isFinished() {
    return this.finished;
  }

  boolean isGoalAchieved() {
    return this.goalAchieved;
  }

  public String toString() {
    return String.format(
        "CatchupGoalTracker{startTime=%d, roundStartTime=%d, roundTimeout=%d, roundCount=%d, catchupTimeout=%d, targetIndex=%d, finished=%s, goalAchieved=%s}",
        this.startTime, this.roundStartTime, this.roundTimeout, this.roundCount,
        this.catchupTimeout, this.targetIndex, this.finished,
        this.goalAchieved);
  }
}
