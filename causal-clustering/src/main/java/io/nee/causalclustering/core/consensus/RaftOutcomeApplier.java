/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus;

import io.nee.causalclustering.core.consensus.membership.RaftMembershipManager;
import io.nee.causalclustering.core.consensus.outcome.Outcome;
import io.nee.causalclustering.core.consensus.roles.Role;
import io.nee.causalclustering.core.consensus.shipping.RaftLogShippingManager;
import io.nee.causalclustering.core.consensus.state.RaftState;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.Outbound;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class RaftOutcomeApplier {

  private final RaftState state;
  private final Log log;
  private final Outbound<MemberId, RaftMessages.RaftMessage> outbound;
  private final RaftMessageTimerResetMonitor raftMessageTimerResetMonitor;
  private final LeaderAvailabilityTimers leaderAvailabilityTimers;
  private final RaftLogShippingManager logShipping;
  private final RaftMembershipManager membershipManager;
  private final Collection<LeaderListener> leaderListeners = new ArrayList();
  private volatile LeaderInfo leaderInfo;

  RaftOutcomeApplier(RaftState state, Outbound<MemberId, RaftMessages.RaftMessage> outbound,
      LeaderAvailabilityTimers leaderAvailabilityTimers,
      RaftMessageTimerResetMonitor raftMessageTimerResetMonitor, RaftLogShippingManager logShipping,
      RaftMembershipManager membershipManager,
      LogProvider logProvider) {
    this.state = state;
    this.outbound = outbound;
    this.leaderAvailabilityTimers = leaderAvailabilityTimers;
    this.raftMessageTimerResetMonitor = raftMessageTimerResetMonitor;
    this.logShipping = logShipping;
    this.membershipManager = membershipManager;
    this.log = logProvider.getLog(this.getClass());
  }

  synchronized Role handle(Outcome outcome) throws IOException {
    boolean newLeaderWasElected = this.leaderChanged(outcome, this.state.leader());
    this.state.update(outcome);
    this.sendMessages(outcome);
    this.handleTimers(outcome);
    this.handleLogShipping(outcome);
    this.leaderInfo = new LeaderInfo(outcome.getLeader(), outcome.getTerm());
    if (newLeaderWasElected) {
      this.notifyLeaderChanges(outcome);
    }

    this.driveMembership(outcome);
    return outcome.getRole();
  }

  private boolean leaderChanged(Outcome outcome, MemberId oldLeader) {
    return !Objects.equals(oldLeader, outcome.getLeader());
  }

  private void sendMessages(Outcome outcome) {
    Iterator n2 = outcome.getOutgoingMessages().iterator();

    while (n2.hasNext()) {
      RaftMessages.Directed outgoingMessage = (RaftMessages.Directed) n2.next();

      try {
        this.outbound.send(outgoingMessage.to(), outgoingMessage.message());
      } catch (Exception n5) {
        this.log.warn(String.format("Failed to send message %s.", outgoingMessage), n5);
      }
    }
  }

  private void handleTimers(Outcome outcome) {
    if (outcome.electionTimeoutRenewed()) {
      this.raftMessageTimerResetMonitor.timerReset();
      this.leaderAvailabilityTimers.renewElection();
    } else if (outcome.isSteppingDown()) {
      this.raftMessageTimerResetMonitor.timerReset();
    }
  }

  private void handleLogShipping(Outcome outcome) {
    LeaderContext leaderContext = new LeaderContext(outcome.getTerm(), outcome.getLeaderCommit());
    if (outcome.isElectedLeader()) {
      this.logShipping.resume(leaderContext);
    } else if (outcome.isSteppingDown()) {
      this.logShipping.pause();
    }

    if (outcome.getRole() == Role.LEADER) {
      this.logShipping.handleCommands(outcome.getShipCommands(), leaderContext);
    }
  }

  private void notifyLeaderChanges(Outcome outcome) {
    Iterator n2 = this.leaderListeners.iterator();

    while (n2.hasNext()) {
      LeaderListener listener = (LeaderListener) n2.next();
      listener.onLeaderEvent(outcome);
    }
  }

  private void driveMembership(Outcome outcome) throws IOException {
    this.membershipManager.processLog(outcome.getCommitIndex(), outcome.getLogCommands());
    Role nextRole = outcome.getRole();
    this.membershipManager.onRole(nextRole);
    if (nextRole == Role.LEADER) {
      this.membershipManager.onFollowerStateChange(outcome.getFollowerStates());
    }
  }

  Optional<LeaderInfo> getLeaderInfo() {
    return Optional.ofNullable(this.leaderInfo);
  }

  synchronized void registerListener(LeaderListener listener) {
    this.leaderListeners.add(listener);
    listener.onLeaderSwitch(this.state.leaderInfo());
  }

  synchronized void unregisterListener(LeaderListener listener) {
    this.leaderListeners.remove(listener);
  }
}
