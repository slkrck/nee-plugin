/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import io.nee.causalclustering.core.consensus.log.EntryRecord;
import io.nee.causalclustering.core.consensus.log.LogPosition;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.neo4j.cursor.CursorValue;
import org.neo4j.cursor.IOCursor;
import org.neo4j.io.fs.ReadAheadChannel;
import org.neo4j.io.fs.StoreChannel;
import org.neo4j.io.memory.ByteBuffers;

class EntryRecordCursor implements IOCursor<EntryRecord> {

  private final ByteBuffer buffer;
  private final LogPosition position;
  private final CursorValue<EntryRecord> currentRecord = new CursorValue();
  private final Reader reader;
  private final SegmentFile segment;
  private final ChannelMarshal<ReplicatedContent> contentMarshal;
  private ReadAheadChannel<StoreChannel> bufferedReader;
  private boolean hadError;
  private boolean closed;

  EntryRecordCursor(Reader reader, ChannelMarshal<ReplicatedContent> contentMarshal,
      long currentIndex, long wantedIndex, SegmentFile segment)
      throws IOException, EndOfStreamException {
    this.buffer = ByteBuffers.allocateDirect(ReadAheadChannel.DEFAULT_READ_AHEAD_SIZE);
    this.bufferedReader = new ReadAheadChannel(reader.channel(), this.buffer);
    this.reader = reader;
    this.contentMarshal = contentMarshal;

    for (this.segment = segment; currentIndex < wantedIndex; ++currentIndex) {
      EntryRecord.read(this.bufferedReader, contentMarshal);
    }

    this.position = new LogPosition(currentIndex, this.bufferedReader.position());
  }

  public boolean next() throws IOException {
    EntryRecord entryRecord;
    try {
      entryRecord = EntryRecord.read(this.bufferedReader, this.contentMarshal);
    } catch (EndOfStreamException n3) {
      this.currentRecord.invalidate();
      return false;
    } catch (IOException n4) {
      this.hadError = true;
      throw n4;
    }

    this.currentRecord.set(entryRecord);
    this.position.byteOffset = this.bufferedReader.position();
    ++this.position.logIndex;
    return true;
  }

  public void close() throws IOException {
    if (this.closed) {
      throw new IllegalStateException("Already closed");
    } else {
      this.bufferedReader = null;
      ByteBuffers.releaseBuffer(this.buffer);
      this.closed = true;
      this.segment.refCount().decrease();
      if (this.hadError) {
        this.reader.close();
      } else {
        this.segment.positionCache().put(this.position);
        this.segment.readerPool().release(this.reader);
      }
    }
  }

  public EntryRecord get() {
    return this.currentRecord.get();
  }
}
