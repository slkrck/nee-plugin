/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.roles;

import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.outcome.Outcome;
import io.nee.causalclustering.core.consensus.state.ReadableRaftState;
import io.nee.causalclustering.identity.MemberId;
import java.io.IOException;
import java.util.Set;
import org.neo4j.logging.Log;

public class Election {

  private Election() {
  }

  public static boolean startRealElection(ReadableRaftState ctx, Outcome outcome, Log log)
      throws IOException {
    Set<MemberId> currentMembers = ctx.votingMembers();
    if (currentMembers != null && currentMembers.contains(ctx.myself())) {
      outcome.setNextTerm(ctx.term() + 1L);
      RaftMessages.Vote.Request voteForMe = new RaftMessages.Vote.Request(ctx.myself(),
          outcome.getTerm(), ctx.myself(), ctx.entryLog().appendIndex(),
          ctx.entryLog().readEntryTerm(ctx.entryLog().appendIndex()));
      currentMembers.stream().filter((member) ->
      {
        return !member.equals(ctx.myself());
      }).forEach((member) ->
      {
        outcome.addOutgoingMessage(new RaftMessages.Directed(member, voteForMe));
      });
      outcome.setVotedFor(ctx.myself());
      log.info("Election started with vote request: %s and members: %s", voteForMe, currentMembers);
      return true;
    } else {
      log.info("Election attempted but not started, current members are %s, I am %s",
          currentMembers, ctx.myself());
      return false;
    }
  }

  public static boolean startPreElection(ReadableRaftState ctx, Outcome outcome, Log log)
      throws IOException {
    Set<MemberId> currentMembers = ctx.votingMembers();
    if (currentMembers != null && currentMembers.contains(ctx.myself())) {
      RaftMessages.PreVote.Request preVoteForMe =
          new RaftMessages.PreVote.Request(ctx.myself(), outcome.getTerm(), ctx.myself(),
              ctx.entryLog().appendIndex(),
              ctx.entryLog().readEntryTerm(ctx.entryLog().appendIndex()));
      currentMembers.stream().filter((member) ->
      {
        return !member.equals(ctx.myself());
      }).forEach((member) ->
      {
        outcome.addOutgoingMessage(new RaftMessages.Directed(member, preVoteForMe));
      });
      log.info("Pre-election started with: %s and members: %s", preVoteForMe, currentMembers);
      return true;
    } else {
      log.info("Pre-election attempted but not started, current members are %s, I am %s",
          currentMembers, ctx.myself());
      return false;
    }
  }
}
