/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.roles;

import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.outcome.Outcome;
import io.nee.causalclustering.core.consensus.state.ReadableRaftState;
import io.nee.causalclustering.identity.MemberId;
import java.io.IOException;
import org.neo4j.function.ThrowingBooleanSupplier;
import org.neo4j.logging.Log;

public class Voting {

  private Voting() {
  }

  static void handleVoteRequest(ReadableRaftState state, Outcome outcome,
      RaftMessages.Vote.Request voteRequest, Log log) throws IOException {
    if (voteRequest.term() > state.term()) {
      outcome.setNextTerm(voteRequest.term());
      outcome.setVotedFor(null);
    }

    boolean votedForAnother =
        outcome.getVotedFor() != null && !outcome.getVotedFor().equals(voteRequest.candidate());
    boolean willVoteForCandidate = shouldVoteFor(state, outcome, voteRequest, votedForAnother, log,
        false);
    if (willVoteForCandidate) {
      outcome.setVotedFor(voteRequest.from());
      outcome.renewElectionTimeout();
    }

    outcome.addOutgoingMessage(
        new RaftMessages.Directed(voteRequest.from(),
            new RaftMessages.Vote.Response(state.myself(), outcome.getTerm(),
                willVoteForCandidate)));
  }

  static void handlePreVoteRequest(ReadableRaftState state, Outcome outcome,
      RaftMessages.PreVote.Request voteRequest, Log log) throws IOException {
    ThrowingBooleanSupplier<IOException> willVoteForCandidate = () ->
    {
      return shouldVoteFor(state, outcome, voteRequest, false, log, true);
    };
    respondToPreVoteRequest(state, outcome, voteRequest, willVoteForCandidate);
  }

  static void declinePreVoteRequest(ReadableRaftState state, Outcome outcome,
      RaftMessages.PreVote.Request voteRequest) throws IOException {
    respondToPreVoteRequest(state, outcome, voteRequest, () ->
    {
      return false;
    });
  }

  private static void respondToPreVoteRequest(ReadableRaftState state, Outcome outcome,
      RaftMessages.PreVote.Request voteRequest,
      ThrowingBooleanSupplier<IOException> willVoteFor) throws IOException {
    if (voteRequest.term() > state.term()) {
      outcome.setNextTerm(voteRequest.term());
    }

    outcome.addOutgoingMessage(new RaftMessages.Directed(voteRequest.from(),
        new RaftMessages.PreVote.Response(state.myself(), outcome.getTerm(),
            willVoteFor.getAsBoolean())));
  }

  private static boolean shouldVoteFor(ReadableRaftState state, Outcome outcome,
      RaftMessages.AnyVote.Request voteRequest,
      boolean committedToVotingForAnother, Log log, boolean isPreVote) throws IOException {
    long requestTerm = voteRequest.term();
    MemberId candidate = voteRequest.candidate();
    long requestLastLogTerm = voteRequest.lastLogTerm();
    long requestLastLogIndex = voteRequest.lastLogIndex();
    long contextTerm = outcome.getTerm();
    long contextLastAppended = state.entryLog().appendIndex();
    long contextLastLogTerm = state.entryLog().readEntryTerm(contextLastAppended);
    return shouldVoteFor(candidate, contextTerm, requestTerm, contextLastLogTerm,
        requestLastLogTerm, contextLastAppended, requestLastLogIndex,
        committedToVotingForAnother, log, isPreVote);
  }

  public static boolean shouldVoteFor(MemberId candidate, long contextTerm, long requestTerm,
      long contextLastLogTerm, long requestLastLogTerm,
      long contextLastAppended, long requestLastLogIndex, boolean committedToVotingForAnother,
      Log log, boolean isPreVote) {
    String voteType = isPreVote ? "pre-vote" : "vote";
    boolean requestFromPreviousTerm = requestTerm < contextTerm;
    boolean requestLogEndsAtHigherTerm = requestLastLogTerm > contextLastLogTerm;
    boolean logsEndAtSameTerm = requestLastLogTerm == contextLastLogTerm;
    boolean requestLogAtLeastAsLongAsMyLog = requestLastLogIndex >= contextLastAppended;
    boolean requesterLogUpToDate =
        requestLogEndsAtHigherTerm || logsEndAtSameTerm && requestLogAtLeastAsLongAsMyLog;
    boolean votedForOtherInSameTerm = requestTerm == contextTerm && committedToVotingForAnother;
    boolean shouldVoteFor =
        requesterLogUpToDate && !votedForOtherInSameTerm && !requestFromPreviousTerm;
    log.info("Received raft %s request from %s. Should vote for candidate: %s", voteType, candidate,
        shouldVoteFor);
    log.debug(
        "Should vote for raft candidate: %s. Reasons for decision:%nrequester log up to date: %s (request last log term: %s, context last log term: %s, request last log index: %s, context last append: %s),%nrequester from an earlier term: %s (request term: %s, context term: %s),%nvoted for other in same term: %s",
        shouldVoteFor, requesterLogUpToDate, requestLastLogTerm, contextLastLogTerm,
        requestLastLogIndex, contextLastAppended,
        requestFromPreviousTerm, requestTerm, contextTerm, votedForOtherInSameTerm);
    return shouldVoteFor;
  }
}
