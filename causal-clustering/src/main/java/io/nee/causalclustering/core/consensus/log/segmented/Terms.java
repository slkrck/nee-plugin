/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import java.util.Arrays;

public class Terms {

  private int size;
  private long[] indexes;
  private long[] terms;
  private long min;
  private long max;

  Terms(long prevIndex, long prevTerm) {
    this.skip(prevIndex, prevTerm);
  }

  synchronized void append(long index, long term) {
    if (index != this.max + 1L) {
      throw new IllegalStateException(
          String.format("Must append in order. %s but expected index is %d",
              this.appendMessage(index, term), this.max + 1L));
    } else if (this.size > 0 && term < this.terms[this.size - 1]) {
      throw new IllegalStateException(
          String.format("Non-monotonic term. %s but highest term is %d",
              this.appendMessage(index, term), this.terms[this.size - 1]));
    } else {
      this.max = index;
      if (this.size == 0 || term != this.terms[this.size - 1]) {
        this.setSize(this.size + 1);
        this.indexes[this.size - 1] = index;
        this.terms[this.size - 1] = term;
      }
    }
  }

  private String appendMessage(long index, long term) {
    return String.format("Tried to append [index: %d, term: %d]", index, term);
  }

  private void setSize(int newSize) {
    if (newSize != this.size) {
      this.size = newSize;
      this.indexes = Arrays.copyOf(this.indexes, this.size);
      this.terms = Arrays.copyOf(this.terms, this.size);
    }
  }

  synchronized void truncate(long fromIndex) {
    if (fromIndex >= 0L && fromIndex >= this.min) {
      this.max = fromIndex - 1L;

      int newSize;
      for (newSize = this.size; newSize > 0 && this.indexes[newSize - 1] >= fromIndex; --newSize) {
      }

      this.setSize(newSize);
    } else {
      throw new IllegalStateException(
          "Cannot truncate a negative index. Tried to truncate from " + fromIndex);
    }
  }

  synchronized void prune(long upToIndex) {
    this.min = Math.max(upToIndex, this.min);
    int lastToPrune = this.findRangeContaining(this.min) - 1;
    if (lastToPrune >= 0) {
      this.size = this.indexes.length - 1 - lastToPrune;
      this.indexes = Arrays.copyOfRange(this.indexes, lastToPrune + 1, this.indexes.length);
      this.terms = Arrays.copyOfRange(this.terms, lastToPrune + 1, this.terms.length);
    }
  }

  private int findRangeContaining(long index) {
    for (int i = 0; i < this.indexes.length; ++i) {
      if (this.indexes[i] > index) {
        return i - 1;
      }

      if (this.indexes[i] == index) {
        return i;
      }
    }

    return index > this.indexes[this.indexes.length - 1] ? this.indexes.length - 1 : -1;
  }

  synchronized void skip(long prevIndex, long prevTerm) {
    this.min = this.max = prevIndex;
    this.size = 1;
    this.indexes = new long[this.size];
    this.terms = new long[this.size];
    this.indexes[0] = prevIndex;
    this.terms[0] = prevTerm;
  }

  synchronized long get(long logIndex) {
    if (logIndex != -1L && logIndex >= this.min && logIndex <= this.max) {
      for (int i = this.size - 1; i >= 0; --i) {
        if (logIndex >= this.indexes[i]) {
          return this.terms[i];
        }
      }

      throw new RuntimeException("Should be possible to find index >= min");
    } else {
      return -1L;
    }
  }

  synchronized long latest() {
    return this.size == 0 ? -1L : this.terms[this.size - 1];
  }
}
