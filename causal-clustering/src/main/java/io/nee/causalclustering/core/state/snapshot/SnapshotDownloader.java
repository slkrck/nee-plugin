/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.snapshot;

import io.nee.causalclustering.catchup.CatchupClientFactory;
import io.nee.causalclustering.catchup.CatchupResponseAdaptor;
import io.nee.causalclustering.catchup.VersionedCatchupClients;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class SnapshotDownloader {

  private final CatchupClientFactory catchupClientFactory;
  private final Log log;

  public SnapshotDownloader(LogProvider logProvider, CatchupClientFactory catchupClientFactory) {
    this.log = logProvider.getLog(this.getClass());
    this.catchupClientFactory = catchupClientFactory;
  }

  Optional<CoreSnapshot> getCoreSnapshot(NamedDatabaseId namedDatabaseId, SocketAddress address) {
    this.log.info("Downloading snapshot from core server at %s", address);

    CoreSnapshot coreSnapshot;
    try {
      VersionedCatchupClients client = this.catchupClientFactory.getClient(address, this.log);
      CatchupResponseAdaptor<CoreSnapshot> responseHandler = new CatchupResponseAdaptor<CoreSnapshot>() {
        public void onCoreSnapshot(CompletableFuture<CoreSnapshot> signal, CoreSnapshot response) {
          signal.complete(response);
        }
      };
      coreSnapshot = client.v3((c) ->
      {
        return c.getCoreSnapshot(namedDatabaseId);
      }).withResponseHandler(responseHandler).request();
    } catch (Exception n6) {
      this.log.warn("Store copy failed", n6);
      return Optional.empty();
    }

    return Optional.of(coreSnapshot);
  }
}
