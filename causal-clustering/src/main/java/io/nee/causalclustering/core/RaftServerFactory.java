/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.core.consensus.RaftMessageNettyHandler;
import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.protocol.v2.RaftProtocolServerInstallerV2;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.logging.RaftMessageLogger;
import io.nee.causalclustering.messaging.LoggingInbound;
import io.nee.causalclustering.net.BootstrapConfiguration;
import io.nee.causalclustering.net.Server;
import io.nee.causalclustering.protocol.ModifierProtocolInstaller;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.ProtocolInstaller;
import io.nee.causalclustering.protocol.ProtocolInstallerRepository;
import io.nee.causalclustering.protocol.application.ApplicationProtocols;
import io.nee.causalclustering.protocol.handshake.ApplicationProtocolRepository;
import io.nee.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import io.nee.causalclustering.protocol.handshake.HandshakeServerInitializer;
import io.nee.causalclustering.protocol.handshake.ModifierProtocolRepository;
import io.nee.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import io.nee.causalclustering.protocol.init.ServerChannelInitializer;
import io.nee.causalclustering.protocol.modifier.ModifierProtocols;
import io.netty.channel.ChannelInboundHandler;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executor;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;

public class RaftServerFactory {

  public static final String RAFT_SERVER_NAME = "raft-server";
  private final GlobalModule globalModule;
  private final IdentityModule identityModule;
  private final ApplicationSupportedProtocols supportedApplicationProtocol;
  private final RaftMessageLogger<MemberId> raftMessageLogger;
  private final LogProvider logProvider;
  private final NettyPipelineBuilderFactory pipelineBuilderFactory;
  private final Collection<ModifierSupportedProtocols> supportedModifierProtocols;

  RaftServerFactory(GlobalModule globalModule, IdentityModule identityModule,
      NettyPipelineBuilderFactory pipelineBuilderFactory,
      RaftMessageLogger<MemberId> raftMessageLogger,
      ApplicationSupportedProtocols supportedApplicationProtocol,
      Collection<ModifierSupportedProtocols> supportedModifierProtocols) {
    this.globalModule = globalModule;
    this.identityModule = identityModule;
    this.supportedApplicationProtocol = supportedApplicationProtocol;
    this.raftMessageLogger = raftMessageLogger;
    this.logProvider = globalModule.getLogService().getInternalLogProvider();
    this.pipelineBuilderFactory = pipelineBuilderFactory;
    this.supportedModifierProtocols = supportedModifierProtocols;
  }

  Server createRaftServer(RaftMessageDispatcher raftMessageDispatcher,
      ChannelInboundHandler installedProtocolsHandler) {
    Config config = this.globalModule.getGlobalConfig();
    ApplicationProtocolRepository applicationProtocolRepository =
        new ApplicationProtocolRepository(ApplicationProtocols.values(),
            this.supportedApplicationProtocol);
    ModifierProtocolRepository modifierProtocolRepository = new ModifierProtocolRepository(
        ModifierProtocols.values(), this.supportedModifierProtocols);
    RaftMessageNettyHandler nettyHandler = new RaftMessageNettyHandler(this.logProvider);
    RaftProtocolServerInstallerV2.Factory raftProtocolServerInstallerV2 =
        new RaftProtocolServerInstallerV2.Factory(nettyHandler, this.pipelineBuilderFactory,
            this.logProvider);
    ProtocolInstallerRepository<ProtocolInstaller.Orientation.Server> protocolInstallerRepository =
        new ProtocolInstallerRepository(List.of(raftProtocolServerInstallerV2),
            ModifierProtocolInstaller.allServerInstallers);
    HandshakeServerInitializer handshakeInitializer =
        new HandshakeServerInitializer(applicationProtocolRepository, modifierProtocolRepository,
            protocolInstallerRepository,
            this.pipelineBuilderFactory, this.logProvider, config);
    Duration handshakeTimeout = config.get(CausalClusteringSettings.handshake_timeout);
    ServerChannelInitializer channelInitializer =
        new ServerChannelInitializer(handshakeInitializer, this.pipelineBuilderFactory,
            handshakeTimeout, this.logProvider, config);
    SocketAddress raftListenAddress = config.get(CausalClusteringSettings.raft_listen_address);
    Executor raftServerExecutor = this.globalModule.getJobScheduler().executor(Group.RAFT_SERVER);
    Server raftServer = new Server(channelInitializer, installedProtocolsHandler, this.logProvider,
        this.globalModule.getLogService().getUserLogProvider(),
        raftListenAddress, "raft-server", raftServerExecutor,
        this.globalModule.getConnectorPortRegister(),
        BootstrapConfiguration.serverConfig(config));
    LoggingInbound<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> loggingRaftInbound =
        new LoggingInbound(nettyHandler, this.raftMessageLogger, this.identityModule.myself());
    loggingRaftInbound.registerHandler(raftMessageDispatcher);
    return raftServer;
  }
}
