/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.ComposableMessageHandler;
import io.nee.causalclustering.messaging.LifecycleMessageHandler;
import java.util.Objects;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ClusterBindingHandler implements
    LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> {

  private final LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> delegateHandler;
  private final RaftMessageDispatcher raftMessageDispatcher;
  private final Log log;
  private volatile RaftId boundRaftId;

  public ClusterBindingHandler(RaftMessageDispatcher raftMessageDispatcher,
      LifecycleMessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> delegateHandler,
      LogProvider logProvider) {
    this.delegateHandler = delegateHandler;
    this.raftMessageDispatcher = raftMessageDispatcher;
    this.log = logProvider.getLog(this.getClass());
  }

  public static ComposableMessageHandler composable(RaftMessageDispatcher raftMessageDispatcher,
      LogProvider logProvider) {
    return (delegate) ->
    {
      return new ClusterBindingHandler(raftMessageDispatcher, delegate, logProvider);
    };
  }

  public void start(RaftId raftId) throws Exception {
    this.boundRaftId = raftId;
    this.delegateHandler.start(raftId);
    this.raftMessageDispatcher.registerHandlerChain(raftId, this);
  }

  public void stop() throws Exception {
    try {
      if (this.boundRaftId != null) {
        this.raftMessageDispatcher.deregisterHandlerChain(this.boundRaftId);
        this.boundRaftId = null;
      }
    } finally {
      this.delegateHandler.stop();
    }
  }

  public void handle(RaftMessages.ReceivedInstantRaftIdAwareMessage<?> message) {
    RaftId raftId = this.boundRaftId;
    if (Objects.isNull(raftId)) {
      this.log
          .debug("Message handling has been stopped, dropping the message: %s", message.message());
    } else if (!Objects.equals(raftId, message.raftId())) {
      this.log
          .info("Discarding message[%s] owing to mismatched raftId. Expected: %s, Encountered: %s",
              message.message(), raftId, message.raftId());
    } else {
      this.delegateHandler.handle(message);
    }
  }
}
