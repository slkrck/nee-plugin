/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.membership;

import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.EndOfStreamException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class MemberIdSetSerializer {

  private MemberIdSetSerializer() {
  }

  public static void marshal(MemberIdSet memberSet, WritableChannel channel) throws IOException {
    Set<MemberId> members = memberSet.getMembers();
    channel.putInt(members.size());
    MemberId.Marshal memberIdMarshal = new MemberId.Marshal();
    Iterator n4 = members.iterator();

    while (n4.hasNext()) {
      MemberId member = (MemberId) n4.next();
      memberIdMarshal.marshal(member, channel);
    }
  }

  public static MemberIdSet unmarshal(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    HashSet<MemberId> members = new HashSet();
    int memberCount = channel.getInt();
    MemberId.Marshal memberIdMarshal = new MemberId.Marshal();

    for (int i = 0; i < memberCount; ++i) {
      members.add(memberIdMarshal.unmarshal(channel));
    }

    return new MemberIdSet(members);
  }
}
