/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus;

import io.nee.causalclustering.core.consensus.schedule.TimeoutFactory;
import io.nee.causalclustering.core.consensus.schedule.TimeoutHandler;
import io.nee.causalclustering.core.consensus.schedule.Timer;
import io.nee.causalclustering.core.consensus.schedule.TimerService;
import java.time.Clock;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.neo4j.function.ThrowingAction;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;

public class LeaderAvailabilityTimers {

  private final long electionIntervalMillis;
  private final long heartbeatIntervalMillis;
  private final Clock clock;
  private final TimerService timerService;
  private final Log log;
  private volatile long lastElectionRenewalMillis;
  private volatile Timer heartbeatTimer;
  private volatile Timer electionTimer;

  LeaderAvailabilityTimers(Duration electionTimeout, Duration heartbeatInterval, Clock clock,
      TimerService timerService, LogProvider logProvider) {
    this.electionIntervalMillis = electionTimeout.toMillis();
    this.heartbeatIntervalMillis = heartbeatInterval.toMillis();
    this.clock = clock;
    this.timerService = timerService;
    this.log = logProvider.getLog(this.getClass());
    if (this.electionIntervalMillis < this.heartbeatIntervalMillis) {
      throw new IllegalArgumentException(
          String.format("Election timeout %s should not be shorter than heartbeat interval %s",
              this.electionIntervalMillis,
              this.heartbeatIntervalMillis));
    }
  }

  void start(ThrowingAction<Exception> electionAction, ThrowingAction<Exception> heartbeatAction) {
    this.electionTimer = this.timerService
        .create(RaftMachine.Timeouts.ELECTION, Group.RAFT_TIMER, this.renewing(electionAction));
    this.electionTimer.set(TimeoutFactory
        .uniformRandomTimeout(this.electionIntervalMillis, this.electionIntervalMillis * 2L,
            TimeUnit.MILLISECONDS));
    this.heartbeatTimer = this.timerService
        .create(RaftMachine.Timeouts.HEARTBEAT, Group.RAFT_TIMER, this.renewing(heartbeatAction));
    this.heartbeatTimer
        .set(TimeoutFactory.fixedTimeout(this.heartbeatIntervalMillis, TimeUnit.MILLISECONDS));
    this.lastElectionRenewalMillis = this.clock.millis();
  }

  void stop() {
    if (this.electionTimer != null) {
      this.electionTimer.kill(Timer.CancelMode.SYNC_WAIT);
    }

    if (this.heartbeatTimer != null) {
      this.heartbeatTimer.kill(Timer.CancelMode.SYNC_WAIT);
    }
  }

  void renewElection() {
    this.lastElectionRenewalMillis = this.clock.millis();
    if (this.electionTimer != null) {
      this.electionTimer.reset();
    }
  }

  boolean isElectionTimedOut() {
    return this.clock.millis() - this.lastElectionRenewalMillis >= this.electionIntervalMillis;
  }

  long getElectionTimeoutMillis() {
    return this.electionIntervalMillis;
  }

  private TimeoutHandler renewing(ThrowingAction<Exception> action) {
    return (timeout) ->
    {
      try {
        action.apply();
      } catch (Exception n4) {
        this.log.error("Failed to process timeout.", n4);
      }

      timeout.reset();
    };
  }
}
