/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus;

import io.nee.causalclustering.common.state.ClusterStateStorageFactory;
import io.nee.causalclustering.core.state.ClusterStateLayout;
import io.nee.causalclustering.discovery.CoreTopologyService;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.Outbound;
import org.neo4j.collection.Dependencies;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.internal.DatabaseLogService;
import org.neo4j.monitoring.Monitors;

public class RaftGroupFactory {

  private final MemberId myself;
  private final GlobalModule globalModule;
  private final ClusterStateLayout clusterState;
  private final CoreTopologyService topologyService;
  private final ClusterStateStorageFactory storageFactory;

  public RaftGroupFactory(MemberId myself, GlobalModule globalModule,
      ClusterStateLayout clusterState, CoreTopologyService topologyService,
      ClusterStateStorageFactory storageFactory) {
    this.myself = myself;
    this.globalModule = globalModule;
    this.clusterState = clusterState;
    this.topologyService = topologyService;
    this.storageFactory = storageFactory;
  }

  public RaftGroup create(NamedDatabaseId namedDatabaseId,
      Outbound<MemberId, RaftMessages.RaftMessage> outbound, LifeSupport life, Monitors monitors,
      Dependencies dependencies, DatabaseLogService logService) {
    return new RaftGroup(this.globalModule.getGlobalConfig(), logService,
        this.globalModule.getFileSystem(), this.globalModule.getJobScheduler(),
        this.globalModule.getGlobalClock(), this.myself, life, monitors, dependencies, outbound,
        this.clusterState, this.topologyService,
        this.storageFactory, namedDatabaseId);
  }
}
