/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.tx;

import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.LogicalTransactionStore;
import org.neo4j.kernel.impl.transaction.log.NoSuchTransactionException;
import org.neo4j.kernel.impl.transaction.log.TransactionCursor;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.TransactionIdStore;

public class LastCommittedIndexFinder {

  private final TransactionIdStore transactionIdStore;
  private final LogicalTransactionStore transactionStore;
  private final Log log;

  public LastCommittedIndexFinder(TransactionIdStore transactionIdStore,
      LogicalTransactionStore transactionStore, LogProvider logProvider) {
    this.transactionIdStore = transactionIdStore;
    this.transactionStore = transactionStore;
    this.log = logProvider.getLog(this.getClass());
  }

  public long getLastCommittedIndex() {
    long lastTxId = this.transactionIdStore.getLastCommittedTransactionId();
    this.log.info("Last transaction id in metadata store %d", lastTxId);
    CommittedTransactionRepresentation lastTx = null;

    try {
      TransactionCursor transactions = this.transactionStore.getTransactions(lastTxId);

      try {
        while (transactions.next()) {
          lastTx = transactions.get();
        }
      } catch (Throwable n10) {
        if (transactions != null) {
          try {
            transactions.close();
          } catch (Throwable n9) {
            n10.addSuppressed(n9);
          }
        }

        throw n10;
      }

      if (transactions != null) {
        transactions.close();
      }
    } catch (NoSuchTransactionException n11) {
      if (lastTx != null) {
        throw new IllegalStateException(n11);
      }
    } catch (Exception n12) {
      throw new RuntimeException(n12);
    }

    if (lastTx == null) {
      throw new RuntimeException(
          "We must have at least one transaction telling us where we are at in the consensus log.");
    } else {
      this.log.info("Start id of last committed transaction in transaction log %d",
          lastTx.getStartEntry().getLastCommittedTxWhenTransactionStarted());
      this.log.info("Last committed transaction id in transaction log %d",
          lastTx.getCommitEntry().getTxId());
      byte[] lastHeaderFound = lastTx.getStartEntry().getAdditionalHeader();
      long lastConsensusIndex = LogIndexTxHeaderEncoding
          .decodeLogIndexFromTxHeader(lastHeaderFound);
      this.log
          .info("Last committed consensus log index committed into tx log %d", lastConsensusIndex);
      return lastConsensusIndex;
    }
  }
}
