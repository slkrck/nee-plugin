/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.tx;

import io.nee.causalclustering.messaging.marshalling.ByteArrayTransactionChunker;
import io.nee.causalclustering.messaging.marshalling.ReplicatedContentHandler;
import io.netty.buffer.ByteBuf;
import io.netty.handler.stream.ChunkedInput;
import java.io.IOException;
import java.util.Arrays;
import java.util.OptionalLong;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;

public class ByteArrayReplicatedTransaction extends ReplicatedTransaction {

  private final byte[] txBytes;
  private final DatabaseId databaseId;

  ByteArrayReplicatedTransaction(byte[] txBytes, DatabaseId databaseId) {
    super(databaseId);
    this.txBytes = txBytes;
    this.databaseId = databaseId;
  }

  public OptionalLong size() {
    return OptionalLong.of(this.txBytes.length);
  }

  public void dispatch(ReplicatedContentHandler contentHandler) throws IOException {
    contentHandler.handle(this);
  }

  public byte[] getTxBytes() {
    return this.txBytes;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ByteArrayReplicatedTransaction that = (ByteArrayReplicatedTransaction) o;
      return Arrays.equals(this.txBytes, that.txBytes);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Arrays.hashCode(this.txBytes);
  }

  public ChunkedInput<ByteBuf> encode() {
    return new ByteArrayTransactionChunker(this);
  }

  public TransactionRepresentation extract(TransactionRepresentationExtractor extractor) {
    return extractor.extract(this);
  }

  public DatabaseId databaseId() {
    return this.databaseId;
  }

  public String toString() {
    return "ByteArrayReplicatedTransaction{txBytes.length=" + this.txBytes.length + "}";
  }
}
