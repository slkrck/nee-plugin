/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.schedule;

import org.neo4j.logging.Log;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobHandle;
import org.neo4j.scheduler.JobScheduler;

public class Timer {

  private final TimerService.TimerName name;
  private final JobScheduler scheduler;
  private final Log log;
  private final Group group;
  private final TimeoutHandler handler;
  private Timeout timeout;
  private Delay delay;
  private JobHandle job;
  private long activeJobId;
  private boolean isDead;
  private volatile boolean isRunning;

  Timer(TimerService.TimerName name, JobScheduler scheduler, Log log, Group group,
      TimeoutHandler handler) {
    this.name = name;
    this.scheduler = scheduler;
    this.log = log;
    this.group = group;
    this.handler = handler;
  }

  public synchronized boolean set(Timeout newTimeout) {
    if (this.isDead) {
      return false;
    } else {
      this.delay = newTimeout.next();
      this.timeout = newTimeout;
      long jobId = this.newJobId();
      this.job = this.scheduler.schedule(this.group, () ->
      {
        this.handle(jobId);
      }, this.delay.amount(), this.delay.unit());
      return true;
    }
  }

  private long newJobId() {
    ++this.activeJobId;
    return this.activeJobId;
  }

  private void handle(long jobId) {
    synchronized (this) {
      if (this.activeJobId != jobId) {
        return;
      }

      this.isRunning = true;
    }

    try {
      this.handler.onTimeout(this);
    } catch (Throwable n9) {
      this.log.error(String.format("[%s] Handler threw exception", this.canonicalName()), n9);
    } finally {
      this.isRunning = false;
    }
  }

  public synchronized boolean reset() {
    if (this.timeout == null) {
      throw new IllegalStateException("You can't reset until you have set a timeout");
    } else {
      return this.set(this.timeout);
    }
  }

  public void cancel(Timer.CancelMode cancelMode) {
    JobHandle job;
    synchronized (this) {
      ++this.activeJobId;
      job = this.isRunning ? this.job : null;
    }

    if (job != null) {
      try {
        if (cancelMode == Timer.CancelMode.SYNC_WAIT) {
          job.waitTermination();
        } else if (cancelMode == Timer.CancelMode.ASYNC_INTERRUPT) {
          job.cancel();
        }
      } catch (Exception n5) {
        this.log
            .warn(String.format("[%s] Cancelling timer threw exception", this.canonicalName()), n5);
      }
    }
  }

  public void kill(Timer.CancelMode cancelMode) {
    synchronized (this) {
      this.isDead = true;
    }

    this.cancel(cancelMode);
  }

  public synchronized void invoke() {
    long jobId = this.newJobId();
    this.job = this.scheduler.schedule(this.group, () ->
    {
      this.handle(jobId);
    });
  }

  synchronized Delay delay() {
    return this.delay;
  }

  public TimerService.TimerName name() {
    return this.name;
  }

  private String canonicalName() {
    String n10000 = this.name.getClass().getCanonicalName();
    return n10000 + "." + this.name.name();
  }

  public enum CancelMode {
    ASYNC,
    ASYNC_INTERRUPT,
    SYNC_WAIT
  }
}
