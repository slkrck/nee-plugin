/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.core.replication.DistributedOperation;
import java.util.ArrayList;
import java.util.List;
import org.neo4j.function.ThrowingBiConsumer;

class CommandBatcher {

  private final ThrowingBiConsumer<Long, List<DistributedOperation>, Exception> applier;
  private final List<DistributedOperation> batch;
  private final int maxBatchSize;
  private long lastIndex;

  CommandBatcher(int maxBatchSize,
      ThrowingBiConsumer<Long, List<DistributedOperation>, Exception> applier) {
    this.batch = new ArrayList(maxBatchSize);
    this.maxBatchSize = maxBatchSize;
    this.applier = applier;
  }

  void add(long index, DistributedOperation operation) throws Exception {
    assert this.batch.size() <= 0 || index == this.lastIndex + 1L;

    this.batch.add(operation);
    this.lastIndex = index;
    if (this.batch.size() == this.maxBatchSize) {
      this.flush();
    }
  }

  void flush() throws Exception {
    this.applier.accept(this.lastIndex, this.batch);
    this.batch.clear();
  }
}
