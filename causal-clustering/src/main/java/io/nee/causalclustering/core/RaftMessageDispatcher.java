/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.Inbound;
import java.time.Clock;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.CappedLogger;

public class RaftMessageDispatcher implements
    Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> {

  private final Map<RaftId, Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>>> handlersById = new ConcurrentHashMap();
  private final CappedLogger log;

  RaftMessageDispatcher(LogProvider logProvider, Clock clock) {
    this.log = this.createCappedLogger(logProvider, clock);
  }

  public void handle(RaftMessages.ReceivedInstantRaftIdAwareMessage<?> message) {
    RaftId id = message.raftId();
    Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> head = this.handlersById
        .get(id);
    if (head == null) {
      this.log.warn("Unable to process message %s because handler for Raft ID %s is not installed",
          message, id);
    } else {
      head.handle(message);
    }
  }

  void registerHandlerChain(RaftId id,
      Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> head) {
    Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> existingHead =
        this.handlersById.putIfAbsent(id, head);
    if (existingHead != null) {
      throw new IllegalArgumentException(
          "Handler chain for raft ID " + id + " is already registered");
    }
  }

  void deregisterHandlerChain(RaftId id) {
    this.handlersById.remove(id);
  }

  private CappedLogger createCappedLogger(LogProvider logProvider, Clock clock) {
    CappedLogger logger = new CappedLogger(logProvider.getLog(this.getClass()));
    logger.setTimeLimit(5L, TimeUnit.SECONDS, clock);
    return logger;
  }
}
