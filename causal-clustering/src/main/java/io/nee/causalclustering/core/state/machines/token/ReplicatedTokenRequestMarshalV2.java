/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.token;

import io.nee.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.StringMarshal;
import io.netty.buffer.ByteBuf;
import java.io.IOException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;

public class ReplicatedTokenRequestMarshalV2 {

  private ReplicatedTokenRequestMarshalV2() {
    throw new AssertionError("Should not be instantiated");
  }

  public static void marshal(ReplicatedTokenRequest tokenRequest, WritableChannel channel)
      throws IOException {
    DatabaseIdWithoutNameMarshal.INSTANCE.marshal(tokenRequest.databaseId(), channel);
    channel.putInt(tokenRequest.type().ordinal());
    StringMarshal.marshal(channel, tokenRequest.tokenName());
    channel.putInt(tokenRequest.commandBytes().length);
    channel.put(tokenRequest.commandBytes(), tokenRequest.commandBytes().length);
  }

  public static ReplicatedTokenRequest unmarshal(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    DatabaseId databaseId = DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal(channel);
    TokenType type = TokenType.values()[channel.getInt()];
    String tokenName = StringMarshal.unmarshal(channel);
    int commandBytesLength = channel.getInt();
    byte[] commandBytes = new byte[commandBytesLength];
    channel.get(commandBytes, commandBytesLength);
    return new ReplicatedTokenRequest(databaseId, type, tokenName, commandBytes);
  }

  public static void marshal(ReplicatedTokenRequest content, ByteBuf buffer) {
    buffer.writeInt(content.type().ordinal());
    StringMarshal.marshal(buffer, content.tokenName());
    buffer.writeInt(content.commandBytes().length);
    buffer.writeBytes(content.commandBytes());
  }

  public static ReplicatedTokenRequest unmarshal(ByteBuf buffer, NamedDatabaseId namedDatabaseId) {
    TokenType type = TokenType.values()[buffer.readInt()];
    String tokenName = StringMarshal.unmarshal(buffer);
    int commandBytesLength = buffer.readInt();
    byte[] commandBytes = new byte[commandBytesLength];
    buffer.readBytes(commandBytes);
    return new ReplicatedTokenRequest(namedDatabaseId.databaseId(), type, tokenName, commandBytes);
  }
}
