/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.outcome;

import io.nee.causalclustering.core.consensus.log.RaftLog;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.consensus.log.cache.InFlightCache;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import org.neo4j.logging.Log;

public class BatchAppendLogEntries implements RaftLogCommand {

  public final long baseIndex;
  public final int offset;
  public final RaftLogEntry[] entries;

  public BatchAppendLogEntries(long baseIndex, int offset, RaftLogEntry[] entries) {
    this.baseIndex = baseIndex;
    this.offset = offset;
    this.entries = entries;
  }

  public void dispatch(RaftLogCommand.Handler handler) throws IOException {
    handler.append(this.baseIndex + (long) this.offset,
        Arrays.copyOfRange(this.entries, this.offset, this.entries.length));
  }

  public void applyTo(RaftLog raftLog, Log log) throws IOException {
    long lastIndex = this.baseIndex + (long) this.offset;
    if (lastIndex <= raftLog.appendIndex()) {
      throw new IllegalStateException(
          "Attempted to append over an existing entry starting at index " + lastIndex);
    } else {
      raftLog.append(Arrays.copyOfRange(this.entries, this.offset, this.entries.length));
    }
  }

  public void applyTo(InFlightCache inFlightCache, Log log) {
    for (int i = this.offset; i < this.entries.length; ++i) {
      inFlightCache.put(this.baseIndex + (long) i, this.entries[i]);
    }
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      BatchAppendLogEntries that = (BatchAppendLogEntries) o;
      return this.baseIndex == that.baseIndex && this.offset == that.offset && Arrays
          .equals(this.entries, that.entries);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.baseIndex, this.offset, Arrays.hashCode(this.entries));
  }

  public String toString() {
    return String
        .format("BatchAppendLogEntries{baseIndex=%d, offset=%d, entries=%s}", this.baseIndex,
            this.offset, Arrays.toString(this.entries));
  }
}
