/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.snapshot;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.catchup.storecopy.DatabaseShutdownException;
import io.nee.causalclustering.core.state.CommandApplicationProcess;
import io.nee.causalclustering.core.state.CoreSnapshotService;
import io.nee.causalclustering.error_handling.DatabasePanicker;
import io.nee.dbms.ClusterInternalDbmsOperator;
import io.nee.dbms.ReplicatedDatabaseEventService;
import java.io.IOException;
import java.util.Optional;
import org.neo4j.collection.Dependencies;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.TransactionIdStore;

public class PersistentSnapshotDownloader implements Runnable {

  static final String DOWNLOAD_SNAPSHOT = "download of snapshot";
  static final String SHUTDOWN = "shutdown";
  private final StoreDownloadContext context;
  private final CommandApplicationProcess applicationProcess;
  private final CatchupAddressProvider addressProvider;
  private final CoreDownloader downloader;
  private final CoreSnapshotService snapshotService;
  private final ReplicatedDatabaseEventService databaseEventService;
  private final Log log;
  private final TimeoutStrategy backoffStrategy;
  private final DatabasePanicker panicker;
  private final PersistentSnapshotDownloader.Monitor monitor;
  private volatile PersistentSnapshotDownloader.State state;
  private volatile boolean stopped;

  PersistentSnapshotDownloader(CatchupAddressProvider addressProvider,
      CommandApplicationProcess applicationProcess, CoreDownloader downloader,
      CoreSnapshotService snapshotService, ReplicatedDatabaseEventService databaseEventService,
      StoreDownloadContext context,
      Log log,
      TimeoutStrategy backoffStrategy, DatabasePanicker panicker, Monitors monitors) {
    this.applicationProcess = applicationProcess;
    this.addressProvider = addressProvider;
    this.downloader = downloader;
    this.snapshotService = snapshotService;
    this.databaseEventService = databaseEventService;
    this.context = context;
    this.log = log;
    this.backoffStrategy = backoffStrategy;
    this.panicker = panicker;
    this.monitor = monitors.newMonitor(Monitor.class, new String[0]);
    this.state = PersistentSnapshotDownloader.State.INITIATED;
  }

  public void run() {
    if (this.moveToRunningState()) {
      NamedDatabaseId databaseId = this.context.databaseId();
      boolean panicked = false;

      try {
        this.monitor.startedDownloadingSnapshot(databaseId);
        this.applicationProcess.pauseApplier("download of snapshot");
        this.log.info("Stopping kernel before store copy");
        ClusterInternalDbmsOperator.StoreCopyHandle stoppedKernel = this.context.stopForStoreCopy();
        boolean incomplete = false;
        Optional<CoreSnapshot> snapshot = this.downloadSnapshotAndStore(this.context);
        if (snapshot.isPresent()) {
          this.log.info("Core snapshot downloaded: %s", snapshot.get());
        } else {
          this.log.warn("Core snapshot could not be downloaded");
          incomplete = true;
        }

        if (incomplete) {
          this.log.warn("Not starting kernel after failed store copy");
        } else if (this.stopped) {
          this.log.warn(
              "Not starting kernel after store copy because database has been requested to stop");
        } else {
          this.snapshotService.installSnapshot(snapshot.get());
          this.log.info("Attempting kernel start after store copy");
          boolean reconcilerTriggered = stoppedKernel.release();
          if (!reconcilerTriggered) {
            this.log.info(
                "Reconciler could not be triggered at this time. This is expected during bootstrapping.");
          } else if (!this.context.kernelDatabase().isStarted()) {
            this.log.warn(
                "Kernel did not start properly after the store copy. This might be because of unexpected errors or normal early-exit paths.");
          } else {
            this.log.info("Kernel started after store copy");
            this.notifyStoreCopied(this.context.kernelDatabase());
          }
        }
      } catch (InterruptedException n12) {
        Thread.currentThread().interrupt();
        this.log.warn("Persistent snapshot downloader was interrupted");
      } catch (DatabaseShutdownException n13) {
        this.log.warn("Store copy aborted due to shut down", n13);
      } catch (Throwable n14) {
        this.log.error("Unrecoverable error during store copy", n14);
        panicked = true;
        this.panicker.panic(n14);
      } finally {
        if (!panicked) {
          this.applicationProcess.resumeApplier("download of snapshot");
          this.monitor.downloadSnapshotComplete(databaseId);
        }

        this.state = PersistentSnapshotDownloader.State.COMPLETED;
      }
    }
  }

  private void notifyStoreCopied(Database database) {
    Dependencies dependencyResolver = database.getDependencyResolver();
    TransactionIdStore txIdStore = dependencyResolver.resolveDependency(TransactionIdStore.class);
    long lastCommittedTransactionId = txIdStore.getLastCommittedTransactionId();

    assert lastCommittedTransactionId == txIdStore.getLastClosedTransactionId();

    this.databaseEventService.getDatabaseEventDispatch(this.context.databaseId())
        .fireStoreReplaced(lastCommittedTransactionId);
  }

  private Optional<CoreSnapshot> downloadSnapshotAndStore(StoreDownloadContext context)
      throws IOException, DatabaseShutdownException, InterruptedException {
    Timeout backoff = this.backoffStrategy.newTimeout();

    while (!this.stopped) {
      Optional<CoreSnapshot> optionalSnapshot = this.downloader
          .downloadSnapshotAndStore(context, this.addressProvider);
      if (optionalSnapshot.isPresent()) {
        return optionalSnapshot;
      }

      Thread.sleep(backoff.getAndIncrement());
    }

    this.log.info("Persistent snapshot downloader was stopped before download succeeded");
    return Optional.empty();
  }

  private synchronized boolean moveToRunningState() {
    if (this.state != PersistentSnapshotDownloader.State.INITIATED) {
      return false;
    } else {
      this.state = PersistentSnapshotDownloader.State.RUNNING;
      return true;
    }
  }

  void stop() throws InterruptedException {
    this.applicationProcess.pauseApplier("shutdown");
    this.stopped = true;

    while (!this.hasCompleted()) {
      Thread.sleep(100L);
    }
  }

  boolean hasCompleted() {
    return this.state == PersistentSnapshotDownloader.State.COMPLETED;
  }

  private enum State {
    INITIATED,
    RUNNING,
    COMPLETED
  }

  public interface Monitor {

    void startedDownloadingSnapshot(NamedDatabaseId n1);

    void downloadSnapshotComplete(NamedDatabaseId n1);
  }
}
