/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.routing.load_balancing.LeaderService;
import io.nee.causalclustering.routing.load_balancing.LoadBalancingPluginLoader;
import io.nee.causalclustering.routing.load_balancing.LoadBalancingProcessor;
import io.nee.causalclustering.routing.load_balancing.procedure.GetRoutingTableProcedureForMultiDC;
import io.nee.causalclustering.routing.load_balancing.procedure.GetRoutingTableProcedureForSingleDC;
import java.util.List;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.kernel.api.procedure.CallableProcedure;
import org.neo4j.logging.LogProvider;
import org.neo4j.procedure.builtin.routing.BaseRoutingProcedureInstaller;

public class CoreRoutingProcedureInstaller extends BaseRoutingProcedureInstaller {

  private final TopologyService topologyService;
  private final LeaderService leaderService;
  private final DatabaseManager<?> databaseManager;
  private final Config config;
  private final LogProvider logProvider;

  public CoreRoutingProcedureInstaller(TopologyService topologyService, LeaderService leaderService,
      DatabaseManager<?> databaseManager, Config config,
      LogProvider logProvider) {
    this.topologyService = topologyService;
    this.leaderService = leaderService;
    this.databaseManager = databaseManager;
    this.config = config;
    this.logProvider = logProvider;
  }

  protected CallableProcedure createProcedure(List<String> namespace) {
    if (this.config.get(CausalClusteringSettings.multi_dc_license)) {
      LoadBalancingProcessor loadBalancingProcessor = this.loadLoadBalancingProcessor();
      return new GetRoutingTableProcedureForMultiDC(namespace, loadBalancingProcessor,
          this.databaseManager, this.config, this.logProvider);
    } else {
      return new GetRoutingTableProcedureForSingleDC(namespace, this.topologyService,
          this.leaderService, this.databaseManager, this.config,
          this.logProvider);
    }
  }

  private LoadBalancingProcessor loadLoadBalancingProcessor() {
    try {
      return LoadBalancingPluginLoader
          .load(this.topologyService, this.leaderService, this.logProvider, this.config);
    } catch (Throwable n2) {
      throw new RuntimeException(n2);
    }
  }
}
