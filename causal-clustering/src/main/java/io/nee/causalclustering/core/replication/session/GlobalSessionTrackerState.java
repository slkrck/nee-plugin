/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.replication.session;

import io.nee.causalclustering.core.state.storage.SafeStateMarshal;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.UUID;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class GlobalSessionTrackerState {

  private Map<MemberId, GlobalSessionTrackerState.LocalSessionTracker> sessionTrackers = new HashMap();
  private long logIndex = -1L;

  public boolean validateOperation(GlobalSession globalSession, LocalOperationId localOperationId) {
    GlobalSessionTrackerState.LocalSessionTracker existingSessionTracker =
        this.sessionTrackers.get(globalSession.owner());
    return this.isNewSession(globalSession, existingSessionTracker) ? this
        .isFirstOperation(localOperationId)
        : existingSessionTracker.isValidOperation(localOperationId);
  }

  public void update(GlobalSession globalSession, LocalOperationId localOperationId,
      long logIndex) {
    GlobalSessionTrackerState.LocalSessionTracker localSessionTracker = this
        .validateGlobalSessionAndGetLocalSessionTracker(globalSession);
    localSessionTracker.validateAndTrackOperation(localOperationId);
    this.logIndex = logIndex;
  }

  private boolean isNewSession(GlobalSession globalSession,
      GlobalSessionTrackerState.LocalSessionTracker existingSessionTracker) {
    return existingSessionTracker == null || !existingSessionTracker.globalSessionId
        .equals(globalSession.sessionId());
  }

  private boolean isFirstOperation(LocalOperationId id) {
    return id.sequenceNumber() == 0L;
  }

  public long logIndex() {
    return this.logIndex;
  }

  private GlobalSessionTrackerState.LocalSessionTracker validateGlobalSessionAndGetLocalSessionTracker(
      GlobalSession globalSession) {
    GlobalSessionTrackerState.LocalSessionTracker localSessionTracker =
        this.sessionTrackers.get(globalSession.owner());
    if (localSessionTracker == null || !localSessionTracker.globalSessionId
        .equals(globalSession.sessionId())) {
      localSessionTracker = new GlobalSessionTrackerState.LocalSessionTracker(
          globalSession.sessionId(), new HashMap());
      this.sessionTrackers.put(globalSession.owner(), localSessionTracker);
    }

    return localSessionTracker;
  }

  public GlobalSessionTrackerState newInstance() {
    GlobalSessionTrackerState copy = new GlobalSessionTrackerState();
    copy.logIndex = this.logIndex;
    Iterator n2 = this.sessionTrackers.entrySet().iterator();

    while (n2.hasNext()) {
      Entry<MemberId, GlobalSessionTrackerState.LocalSessionTracker> entry = (Entry) n2.next();
      copy.sessionTrackers.put(entry.getKey(), entry.getValue().newInstance());
    }

    return copy;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      GlobalSessionTrackerState that = (GlobalSessionTrackerState) o;
      return this.logIndex == that.logIndex && Objects
          .equals(this.sessionTrackers, that.sessionTrackers);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.sessionTrackers, this.logIndex);
  }

  public String toString() {
    return String
        .format("GlobalSessionTrackerState{sessionTrackers=%s, logIndex=%d}", this.sessionTrackers,
            this.logIndex);
  }

  private static class LocalSessionTracker {

    final UUID globalSessionId;
    final Map<Long, Long> lastSequenceNumberPerSession;

    LocalSessionTracker(UUID globalSessionId, Map<Long, Long> lastSequenceNumberPerSession) {
      this.globalSessionId = globalSessionId;
      this.lastSequenceNumberPerSession = lastSequenceNumberPerSession;
    }

    boolean validateAndTrackOperation(LocalOperationId operationId) {
      if (!this.isValidOperation(operationId)) {
        return false;
      } else {
        this.lastSequenceNumberPerSession
            .put(operationId.localSessionId(), operationId.sequenceNumber());
        return true;
      }
    }

    private boolean isValidOperation(LocalOperationId operationId) {
      Long lastSequenceNumber = this.lastSequenceNumberPerSession.get(operationId.localSessionId());
      if (lastSequenceNumber == null) {
        return operationId.sequenceNumber() == 0L;
      } else {
        return operationId.sequenceNumber() == lastSequenceNumber + 1L;
      }
    }

    public GlobalSessionTrackerState.LocalSessionTracker newInstance() {
      return new GlobalSessionTrackerState.LocalSessionTracker(this.globalSessionId,
          new HashMap(this.lastSequenceNumberPerSession));
    }

    public String toString() {
      return String
          .format("LocalSessionTracker{globalSessionId=%s, lastSequenceNumberPerSession=%s}",
              this.globalSessionId,
              this.lastSequenceNumberPerSession);
    }
  }

  public static class Marshal extends SafeStateMarshal<GlobalSessionTrackerState> {

    private final ChannelMarshal<MemberId> memberMarshal;

    public Marshal() {
      this.memberMarshal = MemberId.Marshal.INSTANCE;
    }

    public void marshal(GlobalSessionTrackerState target, WritableChannel channel)
        throws IOException {
      Map<MemberId, GlobalSessionTrackerState.LocalSessionTracker> sessionTrackers = target.sessionTrackers;
      channel.putLong(target.logIndex);
      channel.putInt(sessionTrackers.size());
      Iterator n4 = sessionTrackers.entrySet().iterator();

      while (n4.hasNext()) {
        Entry<MemberId, GlobalSessionTrackerState.LocalSessionTracker> entry = (Entry) n4.next();
        this.memberMarshal.marshal(entry.getKey(), channel);
        GlobalSessionTrackerState.LocalSessionTracker localSessionTracker = entry.getValue();
        UUID uuid = localSessionTracker.globalSessionId;
        channel.putLong(uuid.getMostSignificantBits());
        channel.putLong(uuid.getLeastSignificantBits());
        Map<Long, Long> map = localSessionTracker.lastSequenceNumberPerSession;
        channel.putInt(map.size());
        Iterator n9 = map.entrySet().iterator();

        while (n9.hasNext()) {
          Entry<Long, Long> sessionSequence = (Entry) n9.next();
          channel.putLong(sessionSequence.getKey());
          channel.putLong(sessionSequence.getValue());
        }
      }
    }

    public GlobalSessionTrackerState unmarshal0(ReadableChannel channel)
        throws IOException, EndOfStreamException {
      long logIndex = channel.getLong();
      int sessionTrackerSize = channel.getInt();
      Map<MemberId, GlobalSessionTrackerState.LocalSessionTracker> sessionTrackers = new HashMap();

      for (int i = 0; i < sessionTrackerSize; ++i) {
        MemberId member = this.memberMarshal.unmarshal(channel);
        if (member == null) {
          throw new IllegalStateException("Null member");
        }

        long mostSigBits = channel.getLong();
        long leastSigBits = channel.getLong();
        UUID globalSessionId = new UUID(mostSigBits, leastSigBits);
        int localSessionTrackerSize = channel.getInt();
        Map<Long, Long> lastSequenceNumberPerSession = new HashMap();

        for (int j = 0; j < localSessionTrackerSize; ++j) {
          long localSessionId = channel.getLong();
          long sequenceNumber = channel.getLong();
          lastSequenceNumberPerSession.put(localSessionId, sequenceNumber);
        }

        GlobalSessionTrackerState.LocalSessionTracker localSessionTracker =
            new GlobalSessionTrackerState.LocalSessionTracker(globalSessionId,
                lastSequenceNumberPerSession);
        sessionTrackers.put(member, localSessionTracker);
      }

      GlobalSessionTrackerState result = new GlobalSessionTrackerState();
      result.sessionTrackers = sessionTrackers;
      result.logIndex = logIndex;
      return result;
    }

    public GlobalSessionTrackerState startState() {
      return new GlobalSessionTrackerState();
    }

    public long ordinal(GlobalSessionTrackerState state) {
      return state.logIndex();
    }
  }
}
