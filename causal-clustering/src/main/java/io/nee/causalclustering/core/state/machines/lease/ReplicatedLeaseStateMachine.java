/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.lease;

import io.nee.causalclustering.core.state.StateMachineResult;
import io.nee.causalclustering.core.state.machines.StateMachine;
import io.nee.causalclustering.core.state.storage.StateStorage;
import java.io.IOException;
import java.util.function.Consumer;
import org.neo4j.util.concurrent.Runnables;

public class ReplicatedLeaseStateMachine implements StateMachine<ReplicatedLeaseRequest> {

  private final StateStorage<ReplicatedLeaseState> storage;
  private final Runnable runOnLeaseChange;
  private ReplicatedLeaseState state;
  private volatile int leaseId;

  public ReplicatedLeaseStateMachine(StateStorage<ReplicatedLeaseState> storage) {
    this(storage, Runnables.EMPTY_RUNNABLE);
  }

  public ReplicatedLeaseStateMachine(StateStorage<ReplicatedLeaseState> storage,
      Runnable runOnLeaseChange) {
    this.leaseId = -1;
    this.storage = storage;
    this.runOnLeaseChange = runOnLeaseChange;
  }

  public synchronized void applyCommand(ReplicatedLeaseRequest leaseRequest, long commandIndex,
      Consumer<StateMachineResult> callback) {
    if (commandIndex > this.state().ordinal()) {
      boolean requestAccepted = leaseRequest.id() == Lease.nextCandidateId(this.state.leaseId());
      if (requestAccepted) {
        this.state = new ReplicatedLeaseState(commandIndex, leaseRequest);
        this.leaseId = this.state.leaseId();
        this.runOnLeaseChange.run();
      }

      callback.accept(StateMachineResult.of(requestAccepted));
    }
  }

  public synchronized void flush() throws IOException {
    this.storage.writeState(this.state());
  }

  public long lastAppliedIndex() {
    return this.state().ordinal();
  }

  private ReplicatedLeaseState state() {
    if (this.state == null) {
      this.state = this.storage.getInitialState();
      this.leaseId = this.state.leaseId();
    }

    return this.state;
  }

  public synchronized ReplicatedLeaseState snapshot() {
    return this.state().newInstance();
  }

  int leaseId() {
    return this.leaseId;
  }

  public synchronized void installSnapshot(ReplicatedLeaseState snapshot) {
    this.state = snapshot;
  }
}
