/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.core.replication.Replicator;
import io.nee.causalclustering.core.state.machines.CoreStateMachines;
import io.nee.causalclustering.core.state.machines.lease.ClusterLeaseCoordinator;
import io.nee.causalclustering.core.state.machines.tx.ReplicatedTransactionCommitProcess;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.api.CommitProcessFactory;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionRepresentationCommitProcess;
import org.neo4j.kernel.impl.transaction.log.TransactionAppender;
import org.neo4j.storageengine.api.StorageEngine;

public class CoreCommitProcessFactory implements CommitProcessFactory {

  private final NamedDatabaseId namedDatabaseId;
  private final Replicator replicator;
  private final CoreStateMachines coreStateMachines;
  private final ClusterLeaseCoordinator leaseCoordinator;

  CoreCommitProcessFactory(NamedDatabaseId namedDatabaseId, Replicator replicator,
      CoreStateMachines coreStateMachines,
      ClusterLeaseCoordinator leaseCoordinator) {
    this.namedDatabaseId = namedDatabaseId;
    this.replicator = replicator;
    this.coreStateMachines = coreStateMachines;
    this.leaseCoordinator = leaseCoordinator;
  }

  public TransactionCommitProcess create(TransactionAppender appender, StorageEngine storageEngine,
      Config config) {
    this.initializeCommitProcessForStateMachines(appender, storageEngine);
    return new ReplicatedTransactionCommitProcess(this.replicator, this.namedDatabaseId,
        this.leaseCoordinator);
  }

  private void initializeCommitProcessForStateMachines(TransactionAppender appender,
      StorageEngine storageEngine) {
    TransactionRepresentationCommitProcess commitProcess = new TransactionRepresentationCommitProcess(
        appender, storageEngine);
    this.coreStateMachines.installCommitProcess(commitProcess);
  }
}
