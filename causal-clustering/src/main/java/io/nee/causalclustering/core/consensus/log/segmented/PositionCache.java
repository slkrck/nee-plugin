/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import io.nee.causalclustering.core.consensus.log.LogPosition;

class PositionCache {

  static final int CACHE_SIZE = 8;
  private final LogPosition first;
  private final LogPosition[] cache = new LogPosition[8];
  private int pos;

  PositionCache(long recordOffset) {
    this.first = new LogPosition(0L, recordOffset);

    for (int i = 0; i < this.cache.length; ++i) {
      this.cache[i] = this.first;
    }
  }

  public synchronized void put(LogPosition position) {
    this.cache[this.pos] = position;
    this.pos = (this.pos + 1) % 8;
  }

  synchronized LogPosition lookup(long offsetIndex) {
    if (offsetIndex == 0L) {
      return this.first;
    } else {
      LogPosition best = this.first;

      for (int i = 0; i < 8; ++i) {
        if (this.cache[i].logIndex <= offsetIndex && this.cache[i].logIndex > best.logIndex) {
          best = this.cache[i];
        }
      }

      return best;
    }
  }
}
