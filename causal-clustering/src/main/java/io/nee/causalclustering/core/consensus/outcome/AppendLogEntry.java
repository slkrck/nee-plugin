/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.outcome;

import io.nee.causalclustering.core.consensus.log.RaftLog;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.consensus.log.cache.InFlightCache;
import java.io.IOException;
import java.util.Objects;
import org.neo4j.logging.Log;

public class AppendLogEntry implements RaftLogCommand {

  public final long index;
  public final RaftLogEntry entry;

  public AppendLogEntry(long index, RaftLogEntry entry) {
    this.index = index;
    this.entry = entry;
  }

  public void applyTo(RaftLog raftLog, Log log) throws IOException {
    if (this.index <= raftLog.appendIndex()) {
      throw new IllegalStateException(
          "Attempted to append over an existing entry at index " + this.index);
    } else {
      raftLog.append(this.entry);
    }
  }

  public void applyTo(InFlightCache inFlightCache, Log log) {
    inFlightCache.put(this.index, this.entry);
  }

  public void dispatch(RaftLogCommand.Handler handler) throws IOException {
    handler.append(this.index, this.entry);
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      AppendLogEntry that = (AppendLogEntry) o;
      return this.index == that.index && Objects.equals(this.entry, that.entry);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.index, this.entry);
  }

  public String toString() {
    return "AppendLogEntry{index=" + this.index + ", entry=" + this.entry + "}";
  }
}
