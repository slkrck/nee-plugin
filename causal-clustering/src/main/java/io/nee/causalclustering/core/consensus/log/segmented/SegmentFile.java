/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import io.nee.causalclustering.core.consensus.log.EntryRecord;
import io.nee.causalclustering.core.consensus.log.LogPosition;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.neo4j.cursor.EmptyIOCursor;
import org.neo4j.cursor.IOCursor;
import org.neo4j.io.ByteUnit;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.PhysicalFlushableChannel;
import org.neo4j.io.fs.StoreChannel;
import org.neo4j.io.memory.ByteBuffers;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class SegmentFile implements AutoCloseable {

  private static final SegmentHeader.Marshal headerMarshal = new SegmentHeader.Marshal();
  private final Log log;
  private final FileSystemAbstraction fileSystem;
  private final File file;
  private final ReaderPool readerPool;
  private final ChannelMarshal<ReplicatedContent> contentMarshal;
  private final PositionCache positionCache;
  private final ReferenceCounter refCount;
  private final SegmentHeader header;
  private final long version;
  private PhysicalFlushableChannel bufferedWriter;
  private ByteBuffer writeBuffer;

  SegmentFile(FileSystemAbstraction fileSystem, File file, ReaderPool readerPool, long version,
      ChannelMarshal<ReplicatedContent> contentMarshal,
      LogProvider logProvider, SegmentHeader header) {
    this.fileSystem = fileSystem;
    this.file = file;
    this.readerPool = readerPool;
    this.contentMarshal = contentMarshal;
    this.header = header;
    this.version = version;
    this.positionCache = new PositionCache(header.recordOffset());
    this.refCount = new ReferenceCounter();
    this.log = logProvider.getLog(this.getClass());
  }

  static SegmentFile create(FileSystemAbstraction fileSystem, File file, ReaderPool readerPool,
      long version,
      ChannelMarshal<ReplicatedContent> contentMarshal, LogProvider logProvider,
      SegmentHeader header) throws IOException {
    if (fileSystem.fileExists(file)) {
      throw new IllegalStateException("File was not expected to exist");
    } else {
      SegmentFile segment = new SegmentFile(fileSystem, file, readerPool, version, contentMarshal,
          logProvider, header);
      headerMarshal.marshal(header, segment.getOrCreateWriter());
      segment.flush();
      return segment;
    }
  }

  IOCursor<EntryRecord> getCursor(long logIndex) throws IOException, DisposedException {
    assert logIndex > this.header.prevIndex();

    if (!this.refCount.increase()) {
      throw new DisposedException();
    } else {
      long offsetIndex = logIndex - (this.header.prevIndex() + 1L);
      LogPosition position = this.positionCache.lookup(offsetIndex);
      Reader reader = this.readerPool.acquire(this.version, position.byteOffset);

      try {
        long currentIndex = position.logIndex;
        return new EntryRecordCursor(reader, this.contentMarshal, currentIndex, offsetIndex, this);
      } catch (EndOfStreamException n9) {
        this.readerPool.release(reader);
        this.refCount.decrease();
        return EmptyIOCursor.empty();
      } catch (IOException n10) {
        reader.close();
        this.refCount.decrease();
        throw n10;
      }
    }
  }

  private synchronized PhysicalFlushableChannel getOrCreateWriter() throws IOException {
    if (this.bufferedWriter == null) {
      if (!this.refCount.increase()) {
        throw new IOException("Writer has been closed");
      }

      StoreChannel channel = this.fileSystem.write(this.file);
      channel.position(channel.size());
      this.writeBuffer = ByteBuffers.allocateDirect(Math.toIntExact(ByteUnit.kibiBytes(512L)));
      this.bufferedWriter = new PhysicalFlushableChannel(channel, this.writeBuffer);
    }

    return this.bufferedWriter;
  }

  synchronized long position() throws IOException {
    return this.getOrCreateWriter().position();
  }

  synchronized void closeWriter() {
    if (this.bufferedWriter != null) {
      try {
        this.flush();
        this.bufferedWriter.close();
      } catch (IOException n5) {
        this.log.error("Failed to close writer for: " + this.file, n5);
      } finally {
        ByteBuffers.releaseBuffer(this.writeBuffer);
        this.writeBuffer = null;
        this.bufferedWriter = null;
        this.refCount.decrease();
      }
    }
  }

  public synchronized void write(long logIndex, RaftLogEntry entry) throws IOException {
    EntryRecord.write(this.getOrCreateWriter(), this.contentMarshal, logIndex, entry.term(),
        entry.content());
  }

  synchronized void flush() throws IOException {
    this.bufferedWriter.prepareForFlush().flush();
  }

  public boolean delete() {
    return this.fileSystem.deleteFile(this.file);
  }

  public SegmentHeader header() {
    return this.header;
  }

  public long size() {
    return this.fileSystem.getFileSize(this.file);
  }

  String getFilename() {
    return this.file.getName();
  }

  boolean tryClose() {
    if (this.refCount.tryDispose()) {
      this.close();
      return true;
    } else {
      return false;
    }
  }

  public void close() {
    this.closeWriter();
    this.readerPool.prune(this.version);
    if (!this.refCount.tryDispose()) {
      throw new IllegalStateException(
          String.format("Segment still referenced. Value: %d", this.refCount.get()));
    }
  }

  public String toString() {
    String n10000 = this.file.getName();
    return "SegmentFile{file=" + n10000 + ", header=" + this.header + "}";
  }

  ReferenceCounter refCount() {
    return this.refCount;
  }

  PositionCache positionCache() {
    return this.positionCache;
  }

  public ReaderPool readerPool() {
    return this.readerPool;
  }
}
