/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.replication;

import io.nee.causalclustering.core.replication.session.GlobalSession;
import io.nee.causalclustering.core.replication.session.LocalOperationId;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ContentBuilder;
import io.nee.causalclustering.messaging.marshalling.ReplicatedContentHandler;
import java.io.IOException;
import java.util.Objects;
import java.util.OptionalLong;
import java.util.UUID;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class DistributedOperation implements ReplicatedContent {

  private final ReplicatedContent content;
  private final GlobalSession globalSession;
  private final LocalOperationId operationId;

  public DistributedOperation(ReplicatedContent content, GlobalSession globalSession,
      LocalOperationId operationId) {
    this.content = content;
    this.globalSession = globalSession;
    this.operationId = operationId;
  }

  public static ContentBuilder<ReplicatedContent> deserialize(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    long mostSigBits = channel.getLong();
    long leastSigBits = channel.getLong();
    MemberId owner = (new MemberId.Marshal()).unmarshal(channel);
    GlobalSession globalSession = new GlobalSession(new UUID(mostSigBits, leastSigBits), owner);
    long localSessionId = channel.getLong();
    long sequenceNumber = channel.getLong();
    LocalOperationId localOperationId = new LocalOperationId(localSessionId, sequenceNumber);
    return ContentBuilder.unfinished((subContent) ->
    {
      return new DistributedOperation(subContent, globalSession, localOperationId);
    });
  }

  public GlobalSession globalSession() {
    return this.globalSession;
  }

  public LocalOperationId operationId() {
    return this.operationId;
  }

  public ReplicatedContent content() {
    return this.content;
  }

  public OptionalLong size() {
    return this.content.size();
  }

  public void dispatch(ReplicatedContentHandler contentHandler) throws IOException {
    contentHandler.handle(this);
    this.content().dispatch(contentHandler);
  }

  public void marshalMetaData(WritableChannel channel) throws IOException {
    channel.putLong(this.globalSession().sessionId().getMostSignificantBits());
    channel.putLong(this.globalSession().sessionId().getLeastSignificantBits());
    (new MemberId.Marshal()).marshal(this.globalSession().owner(), channel);
    channel.putLong(this.operationId.localSessionId());
    channel.putLong(this.operationId.sequenceNumber());
  }

  public String toString() {
    return "DistributedOperation{content=" + this.content + ", globalSession=" + this.globalSession
        + ", operationId=" + this.operationId + "}";
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      DistributedOperation that = (DistributedOperation) o;
      return Objects.equals(this.content, that.content) && Objects
          .equals(this.globalSession, that.globalSession) &&
          Objects.equals(this.operationId, that.operationId);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.content, this.globalSession, this.operationId);
  }
}
