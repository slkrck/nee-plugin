/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.routing.load_balancing.filters.Filter;
import io.nee.causalclustering.routing.load_balancing.plugins.server_policies.FilterConfigParser;
import io.nee.causalclustering.routing.load_balancing.plugins.server_policies.InvalidFilterSpecification;
import io.nee.causalclustering.routing.load_balancing.plugins.server_policies.ServerInfo;
import org.neo4j.configuration.SettingValueParser;
import org.neo4j.graphdb.config.Setting;

public final class LoadBalancingServerPoliciesGroup extends LoadBalancingPluginGroup {

  private static final SettingValueParser<Filter<ServerInfo>> PARSER = new SettingValueParser<Filter<ServerInfo>>() {
    public Filter<ServerInfo> parse(String value) {
      try {
        return FilterConfigParser.parse(value);
      } catch (InvalidFilterSpecification n3) {
        throw new IllegalArgumentException("Not a valid filter", n3);
      }
    }

    public String getDescription() {
      return "a load-balancing filter";
    }

    public Class<Filter<ServerInfo>> getType() {
      return PARSER.getType();
    }
  };
  public final Setting<Filter<ServerInfo>> value;

  public LoadBalancingServerPoliciesGroup() {
    super(null, null);
    this.value = this.getBuilder(PARSER, null).build();
  }

  private LoadBalancingServerPoliciesGroup(String name) {
    super(name, "server_policies");
    this.value = this.getBuilder(PARSER, null).build();
  }

  public static LoadBalancingServerPoliciesGroup group(String name) {
    return new LoadBalancingServerPoliciesGroup(name);
  }
}
