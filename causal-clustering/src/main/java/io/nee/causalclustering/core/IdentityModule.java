/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.common.state.ClusterStateStorageFactory;
import io.nee.causalclustering.core.state.storage.SimpleStorage;
import io.nee.causalclustering.identity.MemberId;
import java.io.IOException;
import java.util.UUID;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.logging.Log;

public class IdentityModule {

  private final MemberId myself;

  IdentityModule(GlobalModule globalModule, ClusterStateStorageFactory storageFactory) {
    Log log = globalModule.getLogService().getInternalLogProvider().getLog(this.getClass());
    SimpleStorage memberIdStorage = storageFactory.createMemberIdStorage();

    try {
      if (memberIdStorage.exists()) {
        this.myself = (MemberId) memberIdStorage.readState();
        if (this.myself == null) {
          throw new RuntimeException("I was null");
        }
      } else {
        UUID uuid = UUID.randomUUID();
        this.myself = new MemberId(uuid);
        memberIdStorage.writeState(this.myself);
        log.info(String.format("Generated new id: %s (%s)", this.myself, uuid));
      }
    } catch (IOException n6) {
      throw new RuntimeException(n6);
    }

    globalModule.getJobScheduler().setTopLevelGroupName("Core " + this.myself);
  }

  public MemberId myself() {
    return this.myself;
  }
}
