/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.membership;

import io.nee.causalclustering.core.state.storage.SafeStateMarshal;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.EndOfStreamException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class MembershipEntry {

  private final long logIndex;
  private final Set<MemberId> members;

  public MembershipEntry(long logIndex, Set<MemberId> members) {
    this.members = members;
    this.logIndex = logIndex;
  }

  public long logIndex() {
    return this.logIndex;
  }

  public Set<MemberId> members() {
    return this.members;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      MembershipEntry that = (MembershipEntry) o;
      return this.logIndex == that.logIndex && Objects.equals(this.members, that.members);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.logIndex, this.members);
  }

  public String toString() {
    return "MembershipEntry{logIndex=" + this.logIndex + ", members=" + this.members + "}";
  }

  public static class Marshal extends SafeStateMarshal<MembershipEntry> {

    MemberId.Marshal memberMarshal = new MemberId.Marshal();

    public MembershipEntry startState() {
      return null;
    }

    public long ordinal(MembershipEntry entry) {
      return entry.logIndex;
    }

    public void marshal(MembershipEntry entry, WritableChannel channel) throws IOException {
      if (entry == null) {
        channel.putInt(0);
      } else {
        channel.putInt(1);
        channel.putLong(entry.logIndex);
        channel.putInt(entry.members.size());
        Iterator n3 = entry.members.iterator();

        while (n3.hasNext()) {
          MemberId member = (MemberId) n3.next();
          this.memberMarshal.marshal(member, channel);
        }
      }
    }

    protected MembershipEntry unmarshal0(ReadableChannel channel)
        throws IOException, EndOfStreamException {
      int hasEntry = channel.getInt();
      if (hasEntry == 0) {
        return null;
      } else {
        long logIndex = channel.getLong();
        int memberCount = channel.getInt();
        Set<MemberId> members = new HashSet();

        for (int i = 0; i < memberCount; ++i) {
          members.add(this.memberMarshal.unmarshal(channel));
        }

        return new MembershipEntry(logIndex, members);
      }
    }
  }
}
