/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.term;

import io.nee.causalclustering.core.state.storage.SafeStateMarshal;
import java.io.IOException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class TermState {

  private volatile long term;

  public TermState() {
  }

  private TermState(long term) {
    this.term = term;
  }

  public long currentTerm() {
    return this.term;
  }

  public boolean update(long newTerm) {
    this.failIfInvalid(newTerm);
    boolean changed = this.term != newTerm;
    this.term = newTerm;
    return changed;
  }

  private void failIfInvalid(long newTerm) {
    if (newTerm < this.term) {
      throw new IllegalArgumentException("Cannot move to a lower term");
    }
  }

  public String toString() {
    return "TermState{term=" + this.term + "}";
  }

  public static class Marshal extends SafeStateMarshal<TermState> {

    public void marshal(TermState termState, WritableChannel channel) throws IOException {
      channel.putLong(termState.currentTerm());
    }

    protected TermState unmarshal0(ReadableChannel channel) throws IOException {
      return new TermState(channel.getLong());
    }

    public TermState startState() {
      return new TermState();
    }

    public long ordinal(TermState state) {
      return state.currentTerm();
    }
  }
}
