/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.tx;

import io.nee.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import io.nee.causalclustering.helper.ErrorHandler;
import io.nee.causalclustering.messaging.ChunkingNetworkChannel;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;
import io.netty.util.ReferenceCountUtil;
import java.util.LinkedList;
import java.util.Queue;
import org.neo4j.kernel.database.DatabaseId;

public class ChunkedTransaction implements ChunkedInput<ByteBuf> {

  private static final int CHUNK_SIZE = 32768;
  private final ReplicatedTransactionFactory.TransactionRepresentationWriter txWriter;
  private final DatabaseId databaseId;
  private final Queue<ByteBuf> chunks = new LinkedList();
  private ChunkingNetworkChannel channel;

  ChunkedTransaction(TransactionRepresentationReplicatedTransaction tx) {
    this.txWriter = ReplicatedTransactionFactory.transactionalRepresentationWriter(tx.tx());
    this.databaseId = tx.databaseId();
  }

  public boolean isEndOfInput() {
    return this.channel != null && this.channel.closed() && this.chunks.isEmpty();
  }

  public void close() {
    ErrorHandler errorHandler = new ErrorHandler("Closing chunked transaction");

    try {
      if (this.channel != null) {
        errorHandler.execute(() ->
        {
          this.channel.close();
        });
      }

      this.chunks.forEach((byteBuf) ->
      {
        errorHandler.execute(() ->
        {
          ReferenceCountUtil.release(byteBuf);
        });
      });
    } catch (Throwable n5) {
      try {
        errorHandler.close();
      } catch (Throwable n4) {
        n5.addSuppressed(n4);
      }

      throw n5;
    }

    errorHandler.close();
  }

  public ByteBuf readChunk(ChannelHandlerContext ctx) throws Exception {
    return this.readChunk(ctx.alloc());
  }

  public ByteBuf readChunk(ByteBufAllocator allocator) throws Exception {
    if (this.isEndOfInput()) {
      return null;
    } else {
      if (this.channel == null) {
        this.channel = new ChunkingNetworkChannel(allocator, 32768, this.chunks);
        DatabaseIdWithoutNameMarshal.INSTANCE.marshal(this.databaseId, this.channel);
      }

      while (this.txWriter.canWrite() && this.chunks.isEmpty()) {
        this.txWriter.write(this.channel);
      }

      if (this.chunks.isEmpty()) {
        this.channel.close();
      }

      return this.chunks.poll();
    }
  }

  public long length() {
    return -1L;
  }

  public long progress() {
    return 0L;
  }
}
