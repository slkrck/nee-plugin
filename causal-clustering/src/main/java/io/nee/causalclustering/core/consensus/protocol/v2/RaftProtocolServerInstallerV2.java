/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.protocol.v2;

import io.nee.causalclustering.messaging.marshalling.v2.ContentTypeProtocol;
import io.nee.causalclustering.messaging.marshalling.v2.decoding.ContentTypeDispatcher;
import io.nee.causalclustering.messaging.marshalling.v2.decoding.DecodingDispatcher;
import io.nee.causalclustering.messaging.marshalling.v2.decoding.RaftMessageComposer;
import io.nee.causalclustering.messaging.marshalling.v2.decoding.ReplicatedContentDecoder;
import io.nee.causalclustering.protocol.ModifierProtocolInstaller;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.ProtocolInstaller;
import io.nee.causalclustering.protocol.application.ApplicationProtocol;
import io.nee.causalclustering.protocol.application.ApplicationProtocols;
import io.nee.causalclustering.protocol.modifier.ModifierProtocol;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInboundHandler;
import java.time.Clock;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RaftProtocolServerInstallerV2 implements
    ProtocolInstaller<ProtocolInstaller.Orientation.Server> {

  private static final ApplicationProtocols APPLICATION_PROTOCOL;

  static {
    APPLICATION_PROTOCOL = ApplicationProtocols.RAFT_2_0;
  }

  private final LogProvider logProvider;
  private final ChannelInboundHandler raftMessageHandler;
  private final NettyPipelineBuilderFactory pipelineBuilderFactory;
  private final List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Server>> modifiers;
  private final Log log;

  public RaftProtocolServerInstallerV2(ChannelInboundHandler raftMessageHandler,
      NettyPipelineBuilderFactory pipelineBuilderFactory,
      List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Server>> modifiers,
      LogProvider logProvider) {
    this.raftMessageHandler = raftMessageHandler;
    this.pipelineBuilderFactory = pipelineBuilderFactory;
    this.modifiers = modifiers;
    this.logProvider = logProvider;
    this.log = this.logProvider.getLog(this.getClass());
  }

  public void install(Channel channel) throws Exception {
    ContentTypeProtocol contentTypeProtocol = new ContentTypeProtocol();
    DecodingDispatcher decodingDispatcher = new DecodingDispatcher(contentTypeProtocol,
        this.logProvider);
    this.pipelineBuilderFactory
        .server(
            channel, this.log).modify(this.modifiers).addFraming()
        .add("raft_content_type_dispatcher",
            new ChannelHandler[]{
                new ContentTypeDispatcher(contentTypeProtocol)})
        .add("raft_component_decoder",
            new ChannelHandler[]{decodingDispatcher}).add("raft_content_decoder",
        new ChannelHandler[]{new ReplicatedContentDecoder(contentTypeProtocol)})
        .add("raft_message_composer",
            new ChannelHandler[]{new RaftMessageComposer(Clock.systemUTC())}).add("raft_handler",
        new ChannelHandler[]{this.raftMessageHandler}).install();
  }

  public ApplicationProtocol applicationProtocol() {
    return APPLICATION_PROTOCOL;
  }

  public Collection<Collection<ModifierProtocol>> modifiers() {
    return this.modifiers.stream().map(ModifierProtocolInstaller::protocols)
        .collect(Collectors.toList());
  }

  public static class Factory extends
      ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Server, RaftProtocolServerInstallerV2> {

    public Factory(ChannelInboundHandler raftMessageHandler,
        NettyPipelineBuilderFactory pipelineBuilderFactory, LogProvider logProvider) {
      super(RaftProtocolServerInstallerV2.APPLICATION_PROTOCOL, (modifiers) ->
      {
        return new RaftProtocolServerInstallerV2(raftMessageHandler, pipelineBuilderFactory,
            modifiers, logProvider);
      });
    }
  }
}
