/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.roles;

import io.nee.causalclustering.core.consensus.MajorityIncludingSelfQuorum;
import io.nee.causalclustering.core.consensus.RaftMessageHandler;
import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.outcome.Outcome;
import io.nee.causalclustering.core.consensus.state.ReadableRaftState;
import io.nee.causalclustering.identity.MemberId;
import java.io.IOException;
import java.util.Set;
import org.neo4j.logging.Log;

class Follower implements RaftMessageHandler {

  static boolean logHistoryMatches(ReadableRaftState ctx, long leaderSegmentPrevIndex,
      long leaderSegmentPrevTerm) throws IOException {
    long localLogPrevIndex = ctx.entryLog().prevIndex();
    long localSegmentPrevTerm = ctx.entryLog().readEntryTerm(leaderSegmentPrevIndex);
    return leaderSegmentPrevIndex > -1L && (leaderSegmentPrevIndex <= localLogPrevIndex
        || localSegmentPrevTerm == leaderSegmentPrevTerm);
  }

  static void commitToLogOnUpdate(ReadableRaftState ctx, long indexOfLastNewEntry,
      long leaderCommit, Outcome outcome) {
    long newCommitIndex = Long.min(leaderCommit, indexOfLastNewEntry);
    if (newCommitIndex > ctx.commitIndex()) {
      outcome.setCommitIndex(newCommitIndex);
    }
  }

  private static void handleLeaderLogCompaction(ReadableRaftState ctx, Outcome outcome,
      RaftMessages.LogCompactionInfo compactionInfo) {
    if (compactionInfo.leaderTerm() >= ctx.term()) {
      long localAppendIndex = ctx.entryLog().appendIndex();
      long leaderPrevIndex = compactionInfo.prevIndex();
      if (localAppendIndex <= -1L || leaderPrevIndex > localAppendIndex) {
        outcome.markNeedForFreshSnapshot(leaderPrevIndex, localAppendIndex);
      }
    }
  }

  private static Follower.Handler visitor(ReadableRaftState ctx, Log log) {
    Follower.ElectionTimeoutHandler electionTimeoutHandler;
    Follower.PreVoteRequestHandler preVoteRequestHandler;
    Follower.PreVoteResponseHandler preVoteResponseHandler;
    if (ctx.refusesToBeLeader()) {
      preVoteResponseHandler = Follower.PreVoteResponseNoOpHandler.INSTANCE;
      if (ctx.supportPreVoting()) {
        electionTimeoutHandler = Follower.PreVoteSupportedRefusesToLeadHandler.INSTANCE;
        if (ctx.isPreElection()) {
          preVoteRequestHandler = Follower.PreVoteRequestVotingHandler.INSTANCE;
        } else {
          preVoteRequestHandler = Follower.PreVoteRequestDecliningHandler.INSTANCE;
        }
      } else {
        preVoteRequestHandler = Follower.PreVoteRequestNoOpHandler.INSTANCE;
        electionTimeoutHandler = Follower.PreVoteUnsupportedRefusesToLead.INSTANCE;
      }
    } else if (ctx.supportPreVoting()) {
      electionTimeoutHandler = Follower.PreVoteSupportedHandler.INSTANCE;
      if (ctx.isPreElection()) {
        preVoteRequestHandler = Follower.PreVoteRequestVotingHandler.INSTANCE;
        preVoteResponseHandler = Follower.PreVoteResponseSolicitingHandler.INSTANCE;
      } else {
        preVoteRequestHandler = Follower.PreVoteRequestDecliningHandler.INSTANCE;
        preVoteResponseHandler = Follower.PreVoteResponseNoOpHandler.INSTANCE;
      }
    } else {
      preVoteRequestHandler = Follower.PreVoteRequestNoOpHandler.INSTANCE;
      preVoteResponseHandler = Follower.PreVoteResponseNoOpHandler.INSTANCE;
      electionTimeoutHandler = Follower.PreVoteUnsupportedHandler.INSTANCE;
    }

    return new Follower.Handler(preVoteRequestHandler, preVoteResponseHandler,
        electionTimeoutHandler, ctx, log);
  }

  public Outcome handle(RaftMessages.RaftMessage message, ReadableRaftState ctx, Log log)
      throws IOException {
    return message.dispatch(visitor(ctx, log));
  }

  private interface PreVoteResponseHandler {

    Outcome handle(RaftMessages.PreVote.Response n1, Outcome n2, ReadableRaftState n3, Log n4)
        throws IOException;
  }

  private interface PreVoteRequestHandler {

    Outcome handle(RaftMessages.PreVote.Request n1, Outcome n2, ReadableRaftState n3, Log n4)
        throws IOException;
  }

  private interface ElectionTimeoutHandler {

    Outcome handle(RaftMessages.Timeout.Election n1, Outcome n2, ReadableRaftState n3, Log n4)
        throws IOException;
  }

  private static class PreVoteResponseNoOpHandler implements Follower.PreVoteResponseHandler {

    private static final Follower.PreVoteResponseHandler INSTANCE = new Follower.PreVoteResponseNoOpHandler();

    public Outcome handle(RaftMessages.PreVote.Response response, Outcome outcome,
        ReadableRaftState ctx, Log log) {
      return outcome;
    }
  }

  private static class PreVoteResponseSolicitingHandler implements Follower.PreVoteResponseHandler {

    private static final Follower.PreVoteResponseHandler INSTANCE = new Follower.PreVoteResponseSolicitingHandler();

    public Outcome handle(RaftMessages.PreVote.Response res, Outcome outcome, ReadableRaftState ctx,
        Log log) throws IOException {
      if (res.term() > ctx.term()) {
        outcome.setNextTerm(res.term());
        outcome.setPreElection(false);
        log.info(
            "Aborting pre-election after receiving pre-vote response from %s at term %d (I am at %d)",
            res.from(), res.term(), ctx.term());
        return outcome;
      } else if (res.term() >= ctx.term() && res.voteGranted()) {
        if (!res.from().equals(ctx.myself())) {
          outcome.addPreVoteForMe(res.from());
        }

        if (MajorityIncludingSelfQuorum.isQuorum(ctx.votingMembers(), outcome.getPreVotesForMe())) {
          outcome.renewElectionTimeout();
          outcome.setPreElection(false);
          if (Election.startRealElection(ctx, outcome, log)) {
            outcome.setNextRole(Role.CANDIDATE);
            log.info("Moving to CANDIDATE state after successful pre-election stage");
          }
        }

        return outcome;
      } else {
        return outcome;
      }
    }
  }

  private static class PreVoteRequestNoOpHandler implements Follower.PreVoteRequestHandler {

    private static final Follower.PreVoteRequestHandler INSTANCE = new Follower.PreVoteRequestNoOpHandler();

    public Outcome handle(RaftMessages.PreVote.Request request, Outcome outcome,
        ReadableRaftState ctx, Log log) {
      return outcome;
    }
  }

  private static class PreVoteRequestDecliningHandler implements Follower.PreVoteRequestHandler {

    private static final Follower.PreVoteRequestHandler INSTANCE = new Follower.PreVoteRequestDecliningHandler();

    public Outcome handle(RaftMessages.PreVote.Request request, Outcome outcome,
        ReadableRaftState ctx, Log log) throws IOException {
      Voting.declinePreVoteRequest(ctx, outcome, request);
      return outcome;
    }
  }

  private static class PreVoteRequestVotingHandler implements Follower.PreVoteRequestHandler {

    private static final Follower.PreVoteRequestHandler INSTANCE = new Follower.PreVoteRequestVotingHandler();

    public Outcome handle(RaftMessages.PreVote.Request request, Outcome outcome,
        ReadableRaftState ctx, Log log) throws IOException {
      Voting.handlePreVoteRequest(ctx, outcome, request, log);
      return outcome;
    }
  }

  private static class PreVoteSupportedRefusesToLeadHandler implements
      Follower.ElectionTimeoutHandler {

    private static final Follower.ElectionTimeoutHandler INSTANCE = new Follower.PreVoteSupportedRefusesToLeadHandler();

    public Outcome handle(RaftMessages.Timeout.Election election, Outcome outcome,
        ReadableRaftState ctx, Log log) {
      log.info("Election timeout triggered but refusing to be leader");
      Set<MemberId> memberIds = ctx.votingMembers();
      if (memberIds != null && memberIds.contains(ctx.myself())) {
        outcome.setPreElection(true);
      }

      return outcome;
    }
  }

  private static class PreVoteUnsupportedRefusesToLead implements Follower.ElectionTimeoutHandler {

    private static final Follower.ElectionTimeoutHandler INSTANCE = new Follower.PreVoteUnsupportedRefusesToLead();

    public Outcome handle(RaftMessages.Timeout.Election election, Outcome outcome,
        ReadableRaftState ctx, Log log) {
      log.info("Election timeout triggered but refusing to be leader");
      return outcome;
    }
  }

  private static class PreVoteUnsupportedHandler implements Follower.ElectionTimeoutHandler {

    private static final Follower.ElectionTimeoutHandler INSTANCE = new Follower.PreVoteUnsupportedHandler();

    public Outcome handle(RaftMessages.Timeout.Election election, Outcome outcome,
        ReadableRaftState ctx, Log log) throws IOException {
      log.info("Election timeout triggered");
      if (Election.startRealElection(ctx, outcome, log)) {
        outcome.setNextRole(Role.CANDIDATE);
        log.info("Moving to CANDIDATE state after successfully starting election");
      }

      return outcome;
    }
  }

  private static class PreVoteSupportedHandler implements Follower.ElectionTimeoutHandler {

    private static final Follower.ElectionTimeoutHandler INSTANCE = new Follower.PreVoteSupportedHandler();

    public Outcome handle(RaftMessages.Timeout.Election election, Outcome outcome,
        ReadableRaftState ctx, Log log) throws IOException {
      log.info("Election timeout triggered");
      if (Election.startPreElection(ctx, outcome, log)) {
        outcome.setPreElection(true);
      }

      return outcome;
    }
  }

  private static class Handler implements RaftMessages.Handler<Outcome, IOException> {

    protected final ReadableRaftState ctx;
    protected final Log log;
    protected final Outcome outcome;
    private final Follower.PreVoteRequestHandler preVoteRequestHandler;
    private final Follower.PreVoteResponseHandler preVoteResponseHandler;
    private final Follower.ElectionTimeoutHandler electionTimeoutHandler;

    Handler(Follower.PreVoteRequestHandler preVoteRequestHandler,
        Follower.PreVoteResponseHandler preVoteResponseHandler,
        Follower.ElectionTimeoutHandler electionTimeoutHandler, ReadableRaftState ctx, Log log) {
      this.ctx = ctx;
      this.log = log;
      this.outcome = new Outcome(Role.FOLLOWER, ctx);
      this.preVoteRequestHandler = preVoteRequestHandler;
      this.preVoteResponseHandler = preVoteResponseHandler;
      this.electionTimeoutHandler = electionTimeoutHandler;
    }

    public Outcome handle(RaftMessages.Heartbeat heartbeat) throws IOException {
      Heart.beat(this.ctx, this.outcome, heartbeat, this.log);
      return this.outcome;
    }

    public Outcome handle(RaftMessages.AppendEntries.Request request) throws IOException {
      Appending.handleAppendEntriesRequest(this.ctx, this.outcome, request, this.log);
      return this.outcome;
    }

    public Outcome handle(RaftMessages.Vote.Request request) throws IOException {
      Voting.handleVoteRequest(this.ctx, this.outcome, request, this.log);
      return this.outcome;
    }

    public Outcome handle(RaftMessages.LogCompactionInfo logCompactionInfo) {
      Follower.handleLeaderLogCompaction(this.ctx, this.outcome, logCompactionInfo);
      return this.outcome;
    }

    public Outcome handle(RaftMessages.Vote.Response response) {
      this.log.info("Late vote response: %s", response);
      return this.outcome;
    }

    public Outcome handle(RaftMessages.PreVote.Request request) throws IOException {
      return this.preVoteRequestHandler.handle(request, this.outcome, this.ctx, this.log);
    }

    public Outcome handle(RaftMessages.PreVote.Response response) throws IOException {
      return this.preVoteResponseHandler.handle(response, this.outcome, this.ctx, this.log);
    }

    public Outcome handle(RaftMessages.PruneRequest pruneRequest) {
      Pruning.handlePruneRequest(this.outcome, pruneRequest);
      return this.outcome;
    }

    public Outcome handle(RaftMessages.AppendEntries.Response response) {
      return this.outcome;
    }

    public Outcome handle(RaftMessages.HeartbeatResponse heartbeatResponse) {
      return this.outcome;
    }

    public Outcome handle(RaftMessages.Timeout.Election election) throws IOException {
      return this.electionTimeoutHandler.handle(election, this.outcome, this.ctx, this.log);
    }

    public Outcome handle(RaftMessages.Timeout.Heartbeat heartbeat) {
      return this.outcome;
    }

    public Outcome handle(RaftMessages.NewEntry.Request request) {
      return this.outcome;
    }

    public Outcome handle(RaftMessages.NewEntry.BatchRequest batchRequest) {
      return this.outcome;
    }
  }
}
