/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import java.util.concurrent.atomic.AtomicInteger;

class ReferenceCounter {

  private static final int DISPOSED_VALUE = -1;
  private final AtomicInteger count = new AtomicInteger();

  boolean increase() {
    int pre;
    do {
      pre = this.count.get();
      if (pre == -1) {
        return false;
      }
    }
    while (!this.count.compareAndSet(pre, pre + 1));

    return true;
  }

  void decrease() {
    int pre;
    do {
      pre = this.count.get();
      if (pre <= 0) {
        throw new IllegalStateException("Illegal count: " + pre);
      }
    }
    while (!this.count.compareAndSet(pre, pre - 1));
  }

  boolean tryDispose() {
    return this.count.get() == -1 || this.count.compareAndSet(0, -1);
  }

  public int get() {
    return this.count.get();
  }
}
