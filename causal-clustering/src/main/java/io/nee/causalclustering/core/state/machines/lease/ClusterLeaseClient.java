/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.lease;

import org.neo4j.kernel.api.exceptions.Status.Cluster;
import org.neo4j.kernel.impl.api.LeaseClient;
import org.neo4j.kernel.impl.api.LeaseException;

public class ClusterLeaseClient implements LeaseClient {

  private final ClusterLeaseCoordinator coordinator;
  private int leaseId = -1;

  ClusterLeaseClient(ClusterLeaseCoordinator coordinator) {
    this.coordinator = coordinator;
  }

  public int leaseId() {
    return this.leaseId;
  }

  public void ensureValid() throws LeaseException {
    if (this.leaseId == -1) {
      this.leaseId = this.coordinator.acquireLeaseOrThrow();
    } else if (this.coordinator.isInvalid(this.leaseId)) {
      throw new LeaseException("Local instance lost lease.", Cluster.NotALeader);
    }
  }
}
