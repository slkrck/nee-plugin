/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.protocol.v2;

import io.nee.causalclustering.messaging.marshalling.ReplicatedContentCodec;
import io.nee.causalclustering.messaging.marshalling.v2.encoding.ContentTypeEncoder;
import io.nee.causalclustering.messaging.marshalling.v2.encoding.RaftMessageContentEncoder;
import io.nee.causalclustering.messaging.marshalling.v2.encoding.RaftMessageEncoder;
import io.nee.causalclustering.protocol.ModifierProtocolInstaller;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.ProtocolInstaller;
import io.nee.causalclustering.protocol.application.ApplicationProtocol;
import io.nee.causalclustering.protocol.application.ApplicationProtocols;
import io.nee.causalclustering.protocol.modifier.ModifierProtocol;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RaftProtocolClientInstallerV2 implements
    ProtocolInstaller<ProtocolInstaller.Orientation.Client> {

  private static final ApplicationProtocols APPLICATION_PROTOCOL;

  static {
    APPLICATION_PROTOCOL = ApplicationProtocols.RAFT_2_0;
  }

  private final List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Client>> modifiers;
  private final Log log;
  private final NettyPipelineBuilderFactory clientPipelineBuilderFactory;

  public RaftProtocolClientInstallerV2(NettyPipelineBuilderFactory clientPipelineBuilderFactory,
      List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Client>> modifiers,
      LogProvider logProvider) {
    this.modifiers = modifiers;
    this.log = logProvider.getLog(this.getClass());
    this.clientPipelineBuilderFactory = clientPipelineBuilderFactory;
  }

  public void install(Channel channel) throws Exception {
    this.clientPipelineBuilderFactory
        .client(
            channel, this.log).modify(this.modifiers).addFraming()
        .add("raft_message_encoder", new ChannelHandler[]{new RaftMessageEncoder()}).add(
        "raft_content_type_encoder", new ChannelHandler[]{new ContentTypeEncoder()})
        .add("raft_chunked_writer",
            new ChannelHandler[]{new ChunkedWriteHandler()})
        .add("raft_message_content_encoder",
            new ChannelHandler[]{new RaftMessageContentEncoder(new ReplicatedContentCodec())})
        .install();
  }

  public ApplicationProtocol applicationProtocol() {
    return APPLICATION_PROTOCOL;
  }

  public Collection<Collection<ModifierProtocol>> modifiers() {
    return this.modifiers.stream().map(ModifierProtocolInstaller::protocols)
        .collect(Collectors.toList());
  }

  public static class Factory extends
      ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Client, RaftProtocolClientInstallerV2> {

    public Factory(NettyPipelineBuilderFactory clientPipelineBuilderFactory,
        LogProvider logProvider) {
      super(RaftProtocolClientInstallerV2.APPLICATION_PROTOCOL, (mods) ->
      {
        return new RaftProtocolClientInstallerV2(clientPipelineBuilderFactory, mods, logProvider);
      });
    }
  }
}
