/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.tx;

import io.nee.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import io.nee.causalclustering.messaging.ByteBufBacked;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.OutputStreamWritableChannel;
import io.netty.buffer.ByteBuf;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;

public class ReplicatedTransactionMarshalV2 {

  private ReplicatedTransactionMarshalV2() {
  }

  public static void marshal(WritableChannel writableChannel,
      ByteArrayReplicatedTransaction replicatedTransaction) throws IOException {
    DatabaseIdWithoutNameMarshal.INSTANCE
        .marshal(replicatedTransaction.databaseId(), writableChannel);
    int length = replicatedTransaction.getTxBytes().length;
    writableChannel.putInt(length);
    writableChannel.put(replicatedTransaction.getTxBytes(), length);
  }

  public static ReplicatedTransaction unmarshal(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    DatabaseId databaseId = DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal(channel);
    int txBytesLength = channel.getInt();
    byte[] txBytes = new byte[txBytesLength];
    channel.get(txBytes, txBytesLength);
    return ReplicatedTransaction.from(txBytes, databaseId);
  }

  public static void marshal(WritableChannel writableChannel,
      TransactionRepresentationReplicatedTransaction replicatedTransaction) throws IOException {
    DatabaseIdWithoutNameMarshal.INSTANCE
        .marshal(replicatedTransaction.databaseId(), writableChannel);
    int txStartIndex;
    if (writableChannel instanceof ByteBufBacked) {
      ByteBuf buffer = ((ByteBufBacked) writableChannel).byteBuf();
      int metaDataIndex = buffer.writerIndex();
      txStartIndex = metaDataIndex + 4;
      buffer.writerIndex(txStartIndex);
      writeTx(writableChannel, replicatedTransaction.tx());
      int txLength = buffer.writerIndex() - txStartIndex;
      buffer.setInt(metaDataIndex, txLength);
    } else {
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream(1024);
      OutputStreamWritableChannel outputStreamWritableChannel = new OutputStreamWritableChannel(
          outputStream);
      writeTx(outputStreamWritableChannel, replicatedTransaction.tx());
      txStartIndex = outputStream.size();
      writableChannel.putInt(txStartIndex);
      writableChannel.put(outputStream.toByteArray(), txStartIndex);
    }
  }

  private static void writeTx(WritableChannel writableChannel, TransactionRepresentation tx)
      throws IOException {
    ReplicatedTransactionFactory.TransactionRepresentationWriter txWriter = ReplicatedTransactionFactory
        .transactionalRepresentationWriter(tx);

    while (txWriter.canWrite()) {
      txWriter.write(writableChannel);
    }
  }
}
