/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.tx;

import io.nee.causalclustering.core.state.CommandDispatcher;
import io.nee.causalclustering.core.state.StateMachineResult;
import io.netty.buffer.ByteBuf;
import io.netty.handler.stream.ChunkedInput;
import java.util.function.Consumer;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.transaction.TransactionRepresentation;

public abstract class ReplicatedTransaction implements CoreReplicatedContent {

  private final DatabaseId databaseId;

  public ReplicatedTransaction(DatabaseId databaseId) {
    this.databaseId = databaseId;
  }

  public static TransactionRepresentationReplicatedTransaction from(TransactionRepresentation tx,
      NamedDatabaseId namedDatabaseId) {
    return new TransactionRepresentationReplicatedTransaction(tx, namedDatabaseId.databaseId());
  }

  public static ByteArrayReplicatedTransaction from(byte[] bytes, DatabaseId databaseId) {
    return new ByteArrayReplicatedTransaction(bytes, databaseId);
  }

  public void dispatch(CommandDispatcher commandDispatcher, long commandIndex,
      Consumer<StateMachineResult> callback) {
    commandDispatcher.dispatch(this, commandIndex, callback);
  }

  public DatabaseId databaseId() {
    return this.databaseId;
  }

  public abstract ChunkedInput<ByteBuf> encode();

  public abstract TransactionRepresentation extract(TransactionRepresentationExtractor n1);
}
