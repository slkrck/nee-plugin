/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log;

import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.IOException;
import org.neo4j.io.fs.ReadPastEndException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class EntryRecord {

  private final long logIndex;
  private final RaftLogEntry logEntry;

  public EntryRecord(long logIndex, RaftLogEntry logEntry) {
    this.logIndex = logIndex;
    this.logEntry = logEntry;
  }

  public static EntryRecord read(ReadableChannel channel,
      ChannelMarshal<ReplicatedContent> contentMarshal) throws IOException, EndOfStreamException {
    try {
      long appendIndex = channel.getLong();
      long term = channel.getLong();
      ReplicatedContent content = contentMarshal.unmarshal(channel);
      return new EntryRecord(appendIndex, new RaftLogEntry(term, content));
    } catch (ReadPastEndException n7) {
      throw new EndOfStreamException(n7);
    }
  }

  public static void write(WritableChannel channel,
      ChannelMarshal<ReplicatedContent> contentMarshal, long logIndex, long term,
      ReplicatedContent content)
      throws IOException {
    channel.putLong(logIndex);
    channel.putLong(term);
    contentMarshal.marshal(content, channel);
  }

  public RaftLogEntry logEntry() {
    return this.logEntry;
  }

  public long logIndex() {
    return this.logIndex;
  }

  public String toString() {
    return String.format("%d: %s", this.logIndex, this.logEntry);
  }
}
