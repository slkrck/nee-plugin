/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.machines.lease;

import java.util.stream.Stream;
import org.neo4j.kernel.impl.api.LeaseClient;
import org.neo4j.kernel.impl.locking.ActiveLock;
import org.neo4j.kernel.impl.locking.Locks;
import org.neo4j.lock.AcquireLockTimeoutException;
import org.neo4j.lock.LockTracer;
import org.neo4j.lock.ResourceType;

public class LeaderOnlyLockManager implements Locks {

  private final Locks localLocks;

  public LeaderOnlyLockManager(Locks localLocks) {
    this.localLocks = localLocks;
  }

  public Client newClient() {
    return new LeaderOnlyLockManager.LeaderOnlyLockClient(this.localLocks.newClient());
  }

  public void accept(Visitor visitor) {
    this.localLocks.accept(visitor);
  }

  public void close() {
    this.localLocks.close();
  }

  private static class LeaderOnlyLockClient implements Client {

    private final Client localClient;
    private LeaseClient leaseClient;

    LeaderOnlyLockClient(Client localClient) {
      this.localClient = localClient;
    }

    public void initialize(LeaseClient leaseClient) {
      this.leaseClient = leaseClient;
    }

    public void acquireShared(LockTracer tracer, ResourceType resourceType, long... resourceId)
        throws AcquireLockTimeoutException {
      this.localClient.acquireShared(tracer, resourceType, resourceId);
    }

    public void acquireExclusive(LockTracer tracer, ResourceType resourceType, long... resourceId)
        throws AcquireLockTimeoutException {
      this.ensureExclusiveLockCanBeAcquired();
      this.localClient.acquireExclusive(tracer, resourceType, resourceId);
    }

    public boolean tryExclusiveLock(ResourceType resourceType, long resourceId) {
      this.ensureExclusiveLockCanBeAcquired();
      return this.localClient.tryExclusiveLock(resourceType, resourceId);
    }

    public boolean trySharedLock(ResourceType resourceType, long resourceId) {
      return this.localClient.trySharedLock(resourceType, resourceId);
    }

    public boolean reEnterShared(ResourceType resourceType, long resourceId) {
      return this.localClient.reEnterShared(resourceType, resourceId);
    }

    public boolean reEnterExclusive(ResourceType resourceType, long resourceId) {
      this.ensureExclusiveLockCanBeAcquired();
      return this.localClient.reEnterExclusive(resourceType, resourceId);
    }

    public void releaseShared(ResourceType resourceType, long... resourceIds) {
      this.localClient.releaseShared(resourceType, resourceIds);
    }

    public void releaseExclusive(ResourceType resourceType, long... resourceIds) {
      this.localClient.releaseExclusive(resourceType, resourceIds);
    }

    public void prepare() {
      this.localClient.prepare();
    }

    public void stop() {
      this.localClient.stop();
    }

    public void close() {
      this.localClient.close();
    }

    public int getLockSessionId() {
      return this.leaseClient.leaseId();
    }

    public Stream<? extends ActiveLock> activeLocks() {
      return this.localClient.activeLocks();
    }

    public long activeLockCount() {
      return this.localClient.activeLockCount();
    }

    void ensureExclusiveLockCanBeAcquired() {
      this.leaseClient.ensureValid();
    }
  }
}
