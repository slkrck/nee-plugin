/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log;

import io.nee.causalclustering.core.replication.ReplicatedContent;
import java.util.Objects;

public class RaftLogEntry {

  public static final RaftLogEntry[] empty = new RaftLogEntry[0];
  private final long term;
  private final ReplicatedContent content;

  public RaftLogEntry(long term, ReplicatedContent content) {
    Objects.requireNonNull(content);
    this.term = term;
    this.content = content;
  }

  public long term() {
    return this.term;
  }

  public ReplicatedContent content() {
    return this.content;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      RaftLogEntry that = (RaftLogEntry) o;
      return this.term == that.term && this.content.equals(that.content);
    } else {
      return false;
    }
  }

  public int hashCode() {
    int result = (int) (this.term ^ this.term >>> 32);
    result = 31 * result + this.content.hashCode();
    return result;
  }

  public String toString() {
    return String.format("{term=%d, content=%s}", this.term, this.content);
  }
}
