/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

class OpenEndRangeMap<K extends Comparable<K>, V> {

  private final TreeMap<K, V> tree = new TreeMap();
  private K endKey;
  private V endValue;

  Collection<V> replaceFrom(K from, V value) {
    Collection<V> removed = new ArrayList();
    Iterator<V> itr = this.tree.tailMap(from).values().iterator();

    while (itr.hasNext()) {
      removed.add(itr.next());
      itr.remove();
    }

    this.tree.put(from, value);
    this.endKey = from;
    this.endValue = value;
    return removed;
  }

  OpenEndRangeMap.ValueRange<K, V> lookup(K at) {
    if (this.endKey != null && this.endKey.compareTo(at) <= 0) {
      return new OpenEndRangeMap.ValueRange(null, this.endValue);
    } else {
      Entry<K, V> entry = this.tree.floorEntry(at);
      return new OpenEndRangeMap.ValueRange(this.tree.higherKey(at),
          entry != null ? entry.getValue() : null);
    }
  }

  public V last() {
    return this.endValue;
  }

  public Set<Entry<K, V>> entrySet() {
    return this.tree.entrySet();
  }

  public Collection<V> remove(K lessThan) {
    Collection<V> removed = new ArrayList();
    K floor = this.tree.floorKey(lessThan);
    Iterator<V> itr = this.tree.headMap(floor, false).values().iterator();

    while (itr.hasNext()) {
      removed.add(itr.next());
      itr.remove();
    }

    if (this.tree.isEmpty()) {
      this.endKey = null;
      this.endValue = null;
    }

    return removed;
  }

  static class ValueRange<K, V> {

    private final K limit;
    private final V value;

    ValueRange(K limit, V value) {
      this.limit = limit;
      this.value = value;
    }

    Optional<K> limit() {
      return Optional.ofNullable(this.limit);
    }

    Optional<V> value() {
      return Optional.ofNullable(this.value);
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        OpenEndRangeMap.ValueRange<?, ?> that = (OpenEndRangeMap.ValueRange) o;
        return Objects.equals(this.limit, that.limit) && Objects.equals(this.value, that.value);
      } else {
        return false;
      }
    }

    public int hashCode() {
      return Objects.hash(this.limit, this.value);
    }
  }
}
