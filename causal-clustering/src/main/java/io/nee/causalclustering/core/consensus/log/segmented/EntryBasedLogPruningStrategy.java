/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import org.neo4j.internal.helpers.collection.Visitor;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class EntryBasedLogPruningStrategy implements CoreLogPruningStrategy {

  private final long entriesToKeep;
  private final Log log;

  EntryBasedLogPruningStrategy(long entriesToKeep, LogProvider logProvider) {
    this.entriesToKeep = entriesToKeep;
    this.log = logProvider.getLog(this.getClass());
  }

  public long getIndexToKeep(Segments segments) {
    EntryBasedLogPruningStrategy.SegmentVisitor visitor = new EntryBasedLogPruningStrategy.SegmentVisitor();
    segments.visitBackwards(visitor);
    if (visitor.visitedCount == 0L) {
      this.log.warn(
          "No log files found during the prune operation. This state should resolve on its own, but if this warning continues, you may want to look for other errors in the user log.");
    }

    return visitor.prevIndex;
  }

  private class SegmentVisitor implements Visitor<SegmentFile, RuntimeException> {

    long visitedCount;
    long accumulated;
    long prevIndex = -1L;
    long lastPrevIndex = -1L;

    public boolean visit(SegmentFile segment) throws RuntimeException {
      ++this.visitedCount;
      if (this.lastPrevIndex == -1L) {
        this.lastPrevIndex = segment.header().prevIndex();
        return false;
      } else {
        this.prevIndex = segment.header().prevIndex();
        this.accumulated += this.lastPrevIndex - this.prevIndex;
        this.lastPrevIndex = this.prevIndex;
        return this.accumulated >= EntryBasedLogPruningStrategy.this.entriesToKeep;
      }
    }
  }
}
