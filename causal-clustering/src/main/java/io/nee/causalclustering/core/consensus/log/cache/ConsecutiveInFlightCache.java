/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.cache;

import io.nee.causalclustering.core.consensus.log.RaftLogEntry;

public class ConsecutiveInFlightCache implements InFlightCache {

  private final ConsecutiveCache<RaftLogEntry> cache;
  private final RaftLogEntry[] evictions;
  private final InFlightCacheMonitor monitor;
  private final long maxBytes;
  private long totalBytes;
  private boolean enabled;

  public ConsecutiveInFlightCache() {
    this(1024, 8388608L, InFlightCacheMonitor.VOID, true);
  }

  public ConsecutiveInFlightCache(int capacity, long maxBytes, InFlightCacheMonitor monitor,
      boolean enabled) {
    this.cache = new ConsecutiveCache(capacity);
    this.evictions = new RaftLogEntry[capacity];
    this.maxBytes = maxBytes;
    this.monitor = monitor;
    this.enabled = enabled;
    monitor.setMaxBytes(maxBytes);
    monitor.setMaxElements(capacity);
  }

  public synchronized void enable() {
    this.enabled = true;
  }

  public synchronized void put(long logIndex, RaftLogEntry entry) {
    if (this.enabled) {
      this.totalBytes += this.sizeOf(entry);
      this.cache.put(logIndex, entry, this.evictions);
      this.processEvictions();

      while (this.totalBytes > this.maxBytes) {
        RaftLogEntry evicted = this.cache.remove();
        this.totalBytes -= this.sizeOf(evicted);
      }
    }
  }

  public synchronized RaftLogEntry get(long logIndex) {
    if (!this.enabled) {
      this.monitor.miss();
      return null;
    } else {
      RaftLogEntry entry = this.cache.get(logIndex);
      if (entry == null) {
        this.monitor.miss();
      } else {
        this.monitor.hit();
      }

      return entry;
    }
  }

  public synchronized void truncate(long fromIndex) {
    if (this.enabled) {
      this.cache.truncate(fromIndex, this.evictions);
      this.processEvictions();
    }
  }

  public synchronized void prune(long upToIndex) {
    if (this.enabled) {
      this.cache.prune(upToIndex, this.evictions);
      this.processEvictions();
    }
  }

  public synchronized long totalBytes() {
    return this.totalBytes;
  }

  public synchronized int elementCount() {
    return this.cache.size();
  }

  public void reportSkippedCacheAccess() {
    this.monitor.miss();
  }

  private long sizeOf(RaftLogEntry entry) {
    return entry.content().size().orElse(0L);
  }

  private void processEvictions() {
    for (int i = 0; i < this.evictions.length; ++i) {
      RaftLogEntry entry = this.evictions[i];
      if (entry == null) {
        break;
      }

      this.evictions[i] = null;
      this.totalBytes -= this.sizeOf(entry);
    }

    this.monitor.setTotalBytes(this.totalBytes);
    this.monitor.setElementCount(this.cache.size());
  }
}
