/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.term;

import io.nee.causalclustering.core.consensus.log.monitoring.RaftTermMonitor;
import io.nee.causalclustering.core.state.storage.StateStorage;
import java.io.IOException;
import org.neo4j.monitoring.Monitors;

public class MonitoredTermStateStorage implements StateStorage<TermState> {

  private static final String TERM_TAG = "term";
  private final StateStorage<TermState> delegate;
  private final RaftTermMonitor termMonitor;

  public MonitoredTermStateStorage(StateStorage<TermState> delegate, Monitors monitors) {
    this.delegate = delegate;
    this.termMonitor = monitors
        .newMonitor(RaftTermMonitor.class, new String[]{this.getClass().getName(), "term"});
  }

  public TermState getInitialState() {
    return this.delegate.getInitialState();
  }

  public void writeState(TermState state) throws IOException {
    this.delegate.writeState(state);
    this.termMonitor.term(state.currentTerm());
  }

  public boolean exists() {
    return this.delegate.exists();
  }
}
