/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state;

import io.nee.causalclustering.SessionTracker;
import io.nee.causalclustering.core.CoreState;
import io.nee.causalclustering.core.consensus.log.RaftLog;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.consensus.log.cache.InFlightCache;
import io.nee.causalclustering.core.consensus.log.monitoring.RaftLogAppliedIndexMonitor;
import io.nee.causalclustering.core.consensus.log.monitoring.RaftLogCommitIndexMonitor;
import io.nee.causalclustering.core.replication.DistributedOperation;
import io.nee.causalclustering.core.replication.ProgressTracker;
import io.nee.causalclustering.core.state.machines.tx.CoreReplicatedContent;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshot;
import io.nee.causalclustering.error_handling.DatabasePanicEventHandler;
import io.nee.causalclustering.error_handling.DatabasePanicker;
import io.nee.causalclustering.helper.StatUtil;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ThreadFactory;
import org.neo4j.function.ThrowingAction;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public class CommandApplicationProcess implements DatabasePanicEventHandler {

  private static final long NOTHING = -1L;
  private final RaftLog raftLog;
  private final int flushEvery;
  private final ProgressTracker progressTracker;
  private final SessionTracker sessionTracker;
  private final CoreState coreState;
  private final InFlightCache inFlightCache;
  private final Log log;
  private final RaftLogCommitIndexMonitor commitIndexMonitor;
  private final RaftLogAppliedIndexMonitor appliedIndexMonitor;
  private final CommandBatcher batcher;
  private final DatabasePanicker panicker;
  private final String databaseName;
  private final JobScheduler jobScheduler;
  private final StatUtil.StatContext batchStat;
  private final CommandApplicationProcess.ApplierState applierState = new CommandApplicationProcess.ApplierState();
  private long lastFlushed = -1L;
  private int pauseCount = 1;
  private Thread applierThread;

  public CommandApplicationProcess(RaftLog raftLog, int maxBatchSize, int flushEvery,
      LogProvider logProvider, ProgressTracker progressTracker,
      SessionTracker sessionTracker, CoreState coreState, InFlightCache inFlightCache,
      Monitors monitors,
      DatabasePanicker panicker,
      JobScheduler jobScheduler, String databaseName) {
    this.raftLog = raftLog;
    this.flushEvery = flushEvery;
    this.progressTracker = progressTracker;
    this.sessionTracker = sessionTracker;
    this.log = logProvider.getLog(this.getClass());
    this.coreState = coreState;
    this.inFlightCache = inFlightCache;
    this.commitIndexMonitor = monitors
        .newMonitor(RaftLogCommitIndexMonitor.class, new String[]{this.getClass().getName()});
    this.appliedIndexMonitor =
        monitors
            .newMonitor(RaftLogAppliedIndexMonitor.class, new String[]{this.getClass().getName()});
    this.batcher = new CommandBatcher(maxBatchSize, this::applyBatch);
    this.panicker = panicker;
    this.jobScheduler = jobScheduler;
    this.databaseName = databaseName;
    this.batchStat = StatUtil.create("BatchSize", this.log, 4096L, true);
  }

  void notifyCommitted(long commitIndex) {
    this.applierState.notifyCommitted(commitIndex);
  }

  public void onPanic(Throwable cause) {
    this.applierState.panic();
  }

  private void applyJob() {
    while (this.applierState.keepRunning) {
      try {
        this.applyUpTo(this.applierState.awaitJob());
      } catch (Throwable n2) {
        this.panicker.panicAsync(n2);
        this.log.error("Failed to apply", n2);
        return;
      }
    }
  }

  private void applyUpTo(long applyUpToIndex) throws Exception {
    InFlightLogEntryReader logEntrySupplier = new InFlightLogEntryReader(this.raftLog,
        this.inFlightCache, true);

    try {
      long logIndex = this.applierState.lastApplied + 1L;

      while (true) {
        if (!this.applierState.keepRunning || logIndex > applyUpToIndex) {
          this.batcher.flush();
          break;
        }

        RaftLogEntry entry = logEntrySupplier.get(logIndex);
        if (entry == null) {
          throw new IllegalStateException(
              String.format("Committed log entry at index %d must exist.", logIndex));
        }

        if (entry.content() instanceof DistributedOperation) {
          DistributedOperation distributedOperation = (DistributedOperation) entry.content();
          this.progressTracker.trackReplication(distributedOperation);
          this.batcher.add(logIndex, distributedOperation);
        } else {
          this.batcher.flush();
          this.applierState.setLastApplied(logIndex);
        }

        ++logIndex;
      }
    } catch (Throwable n9) {
      try {
        logEntrySupplier.close();
      } catch (Throwable n8) {
        n9.addSuppressed(n8);
      }

      throw n9;
    }

    logEntrySupplier.close();
  }

  long lastApplied() {
    return this.applierState.lastApplied;
  }

  void installSnapshot(CoreSnapshot coreSnapshot) {
    assert this.pauseCount > 0;

    this.lastFlushed = coreSnapshot.prevIndex();
    this.applierState.setLastApplied(this.lastFlushed);
  }

  synchronized long lastFlushed() {
    return this.lastFlushed;
  }

  private void applyBatch(long lastIndex, List<DistributedOperation> batch) throws Exception {
    if (batch.size() != 0) {
      this.batchStat.collect(batch.size());
      long startIndex = lastIndex - (long) batch.size() + 1L;
      long lastHandledIndex = this.handleOperations(startIndex, batch);

      assert lastHandledIndex == lastIndex;

      this.applierState.setLastApplied(lastIndex);
      this.maybeFlushToDisk();
    }
  }

  private long handleOperations(long commandIndex, List<DistributedOperation> operations) {
    CommandDispatcher dispatcher = this.coreState.commandDispatcher();

    try {
      Iterator n5 = operations.iterator();

      while (n5.hasNext()) {
        DistributedOperation operation = (DistributedOperation) n5.next();
        if (!this.sessionTracker
            .validateOperation(operation.globalSession(), operation.operationId())) {
          if (this.log.isDebugEnabled()) {
            this.log.debug(
                "Skipped an invalid distributed operation: " + operation
                    + ". Session tracker state: " + this.sessionTracker.snapshot());
          }

          ++commandIndex;
        } else {
          CoreReplicatedContent command = (CoreReplicatedContent) operation.content();
          command.dispatch(dispatcher, commandIndex, (result) ->
          {
            this.progressTracker.trackResult(operation, result);
          });
          this.sessionTracker
              .update(operation.globalSession(), operation.operationId(), commandIndex);
          ++commandIndex;
        }
      }
    } catch (Throwable n9) {
      if (dispatcher != null) {
        try {
          dispatcher.close();
        } catch (Throwable n8) {
          n9.addSuppressed(n8);
        }
      }

      throw n9;
    }

    if (dispatcher != null) {
      dispatcher.close();
    }

    return commandIndex - 1L;
  }

  private void maybeFlushToDisk() throws IOException {
    if (this.applierState.lastApplied - this.lastFlushed > (long) this.flushEvery) {
      this.coreState.flush(this.applierState.lastApplied);
      this.lastFlushed = this.applierState.lastApplied;
    }
  }

  public synchronized void start() throws Exception {
    if (this.lastFlushed == -1L) {
      this.lastFlushed = this.coreState.getLastFlushed();
    }

    this.applierState.setLastApplied(this.lastFlushed);
    this.log.info(String.format("Restoring last applied index to %d", this.lastFlushed));
    this.sessionTracker.start();
    long lastPossiblyApplying = Math
        .max(this.coreState.getLastAppliedIndex(), this.applierState.getLastSeenCommitIndex());
    if (lastPossiblyApplying > this.applierState.lastApplied) {
      this.log.info("Applying up to: " + lastPossiblyApplying);
      this.applyUpTo(lastPossiblyApplying);
    }

    this.resumeApplier("startup");
  }

  public synchronized void stop() throws IOException {
    this.pauseApplier("shutdown");
    this.coreState.flush(this.applierState.lastApplied);
  }

  private void spawnApplierThread() {
    this.applierState.setKeepRunning(true);
    ThreadFactory threadFactory = this.jobScheduler.threadFactory(Group.CORE_STATE_APPLIER);
    this.applierThread = threadFactory.newThread(this::applyJob);
    String previousName = this.applierThread.getName();
    this.applierThread.setName(previousName + "-" + this.databaseName);
    this.applierThread.start();
  }

  private void stopApplierThread() {
    this.applierState.setKeepRunning(false);
    this.ignoringInterrupts(() ->
    {
      this.applierThread.join();
    });
  }

  public synchronized void pauseApplier(String reason) {
    if (this.pauseCount < 0) {
      throw new IllegalStateException("Unmatched pause/resume");
    } else {
      ++this.pauseCount;
      this.log.info(String.format("Pausing due to %s (count = %d)", reason, this.pauseCount));
      if (this.pauseCount == 1) {
        this.stopApplierThread();
      }
    }
  }

  public synchronized void resumeApplier(String reason) {
    if (this.pauseCount <= 0) {
      throw new IllegalStateException("Unmatched pause/resume");
    } else {
      --this.pauseCount;
      this.log.info(String.format("Resuming after %s (count = %d)", reason, this.pauseCount));
      if (this.pauseCount == 0) {
        this.spawnApplierThread();
      }
    }
  }

  private void ignoringInterrupts(ThrowingAction<InterruptedException> action) {
    try {
      action.apply();
    } catch (InterruptedException n3) {
      this.log.warn("Unexpected interrupt", n3);
    }
  }

  private class ApplierState {

    private long lastSeenCommitIndex = -1L;
    private volatile long lastApplied = -1L;
    private volatile boolean panic;
    private volatile boolean keepRunning = true;

    private synchronized long getLastSeenCommitIndex() {
      return this.lastSeenCommitIndex;
    }

    private void panic() {
      this.panic = true;
      this.keepRunning = false;
    }

    synchronized void setKeepRunning(boolean keepRunning) {
      if (this.panic && keepRunning) {
        throw new IllegalStateException("The applier has panicked");
      } else {
        this.keepRunning = keepRunning;
        this.notifyAll();
      }
    }

    synchronized long awaitJob() {
      while (this.lastApplied >= this.lastSeenCommitIndex && this.keepRunning) {
        CommandApplicationProcess.this.ignoringInterrupts(this::wait);
      }

      return this.lastSeenCommitIndex;
    }

    synchronized void notifyCommitted(long commitIndex) {
      if (this.lastSeenCommitIndex < commitIndex) {
        this.lastSeenCommitIndex = commitIndex;
        CommandApplicationProcess.this.commitIndexMonitor.commitIndex(commitIndex);
        this.notifyAll();
      }
    }

    void setLastApplied(long lastApplied) {
      this.lastApplied = lastApplied;
      CommandApplicationProcess.this.appliedIndexMonitor.appliedIndex(lastApplied);
    }
  }
}
