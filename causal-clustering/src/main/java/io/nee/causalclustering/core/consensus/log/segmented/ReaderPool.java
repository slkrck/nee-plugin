/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import java.io.IOException;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class ReaderPool {

  private final int maxSize;
  private final Log log;
  private final FileNames fileNames;
  private final FileSystemAbstraction fsa;
  private final Clock clock;
  private ArrayList<Reader> pool;

  ReaderPool(int maxSize, LogProvider logProvider, FileNames fileNames, FileSystemAbstraction fsa,
      Clock clock) {
    this.pool = new ArrayList(maxSize);
    this.maxSize = maxSize;
    this.log = logProvider.getLog(this.getClass());
    this.fileNames = fileNames;
    this.fsa = fsa;
    this.clock = clock;
  }

  Reader acquire(long version, long byteOffset) throws IOException {
    Reader reader = this.getFromPool(version);
    if (reader == null) {
      reader = this.createFor(version);
    }

    reader.channel().position(byteOffset);
    return reader;
  }

  void release(Reader reader) {
    reader.setTimeStamp(this.clock.millis());
    Optional<Reader> optionalOverflow = this.putInPool(reader);
    optionalOverflow.ifPresent(this::dispose);
  }

  private synchronized Reader getFromPool(long version) {
    Iterator itr = this.pool.iterator();

    Reader reader;
    do {
      if (!itr.hasNext()) {
        return null;
      }

      reader = (Reader) itr.next();
    }
    while (reader.version() != version);

    itr.remove();
    return reader;
  }

  private synchronized Optional<Reader> putInPool(Reader reader) {
    this.pool.add(reader);
    return this.pool.size() > this.maxSize ? Optional.of(this.pool.remove(0)) : Optional.empty();
  }

  private Reader createFor(long version) throws IOException {
    return new Reader(this.fsa, this.fileNames.getForSegment(version), version);
  }

  synchronized void prune(long maxAge, TimeUnit unit) {
    if (this.pool != null) {
      long endTimeMillis = this.clock.millis() - unit.toMillis(maxAge);
      Iterator itr = this.pool.iterator();

      while (itr.hasNext()) {
        Reader reader = (Reader) itr.next();
        if (reader.getTimeStamp() < endTimeMillis) {
          this.dispose(reader);
          itr.remove();
        }
      }
    }
  }

  private void dispose(Reader reader) {
    try {
      reader.close();
    } catch (IOException n3) {
      this.log.error("Failed to close reader", n3);
    }
  }

  synchronized void close() throws IOException {
    Iterator n1 = this.pool.iterator();

    while (n1.hasNext()) {
      Reader reader = (Reader) n1.next();
      reader.close();
    }

    this.pool.clear();
    this.pool = null;
  }

  public synchronized void prune(long version) {
    if (this.pool != null) {
      Iterator itr = this.pool.iterator();

      while (itr.hasNext()) {
        Reader reader = (Reader) itr.next();
        if (reader.version() == version) {
          this.dispose(reader);
          itr.remove();
        }
      }
    }
  }
}
