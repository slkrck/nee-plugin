/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.outcome;

import io.nee.causalclustering.core.consensus.log.RaftLog;
import io.nee.causalclustering.core.consensus.log.cache.InFlightCache;
import java.io.IOException;
import java.util.Objects;
import org.neo4j.logging.Log;

public class PruneLogCommand implements RaftLogCommand {

  private final long pruneIndex;

  public PruneLogCommand(long pruneIndex) {
    this.pruneIndex = pruneIndex;
  }

  public void dispatch(RaftLogCommand.Handler handler) {
    handler.prune(this.pruneIndex);
  }

  public void applyTo(RaftLog raftLog, Log log) throws IOException {
    raftLog.prune(this.pruneIndex);
  }

  public void applyTo(InFlightCache inFlightCache, Log log) {
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      PruneLogCommand that = (PruneLogCommand) o;
      return this.pruneIndex == that.pruneIndex;
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.pruneIndex);
  }

  public String toString() {
    return "PruneLogCommand{pruneIndex=" + this.pruneIndex + "}";
  }
}
