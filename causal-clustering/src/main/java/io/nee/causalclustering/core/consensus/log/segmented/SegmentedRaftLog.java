/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import io.nee.causalclustering.core.consensus.log.EntryRecord;
import io.nee.causalclustering.core.consensus.log.RaftLog;
import io.nee.causalclustering.core.consensus.log.RaftLogCursor;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.File;
import java.io.IOException;
import java.time.Clock;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import org.neo4j.cursor.IOCursor;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobHandle;
import org.neo4j.scheduler.JobScheduler;

public class SegmentedRaftLog extends LifecycleAdapter implements RaftLog {

  private final int READER_POOL_MAX_AGE = 1;
  private final FileSystemAbstraction fileSystem;
  private final File directory;
  private final long rotateAtSize;
  private final Function<Integer, ChannelMarshal<ReplicatedContent>> marshalSelector;
  private final FileNames fileNames;
  private final JobScheduler scheduler;
  private final Log log;
  private final LogProvider logProvider;
  private final SegmentedRaftLogPruner pruner;
  private final ReaderPool readerPool;
  private boolean needsRecovery;
  private State state;
  private JobHandle readerPoolPruner;

  public SegmentedRaftLog(FileSystemAbstraction fileSystem, File directory, long rotateAtSize,
      Function<Integer, ChannelMarshal<ReplicatedContent>> marshalSelector, LogProvider logProvider,
      int readerPoolSize, Clock clock,
      JobScheduler scheduler, CoreLogPruningStrategy pruningStrategy) {
    this.fileSystem = fileSystem;
    this.directory = directory;
    this.rotateAtSize = rotateAtSize;
    this.marshalSelector = marshalSelector;
    this.logProvider = logProvider;
    this.scheduler = scheduler;
    this.fileNames = new FileNames(directory);
    this.readerPool = new ReaderPool(readerPoolSize, logProvider, this.fileNames, fileSystem,
        clock);
    this.pruner = new SegmentedRaftLogPruner(pruningStrategy);
    this.log = logProvider.getLog(this.getClass());
  }

  public synchronized void start()
      throws IOException, DamagedLogStorageException, DisposedException {
    if (!this.directory.exists() && !this.directory.mkdirs()) {
      throw new IOException("Could not create: " + this.directory);
    } else {
      try {
        this.state = (new RecoveryProtocol(this.fileSystem, this.fileNames, this.readerPool,
            this.marshalSelector, this.logProvider)).run();
      } catch (Exception n2) {
        throw new RuntimeException(n2);
      }

      this.log.info("log started with recovered state %s", this.state);
      SegmentFile lastSegment = this.state.segments.last();
      if (lastSegment.size() > lastSegment.header().recordOffset()) {
        this.rotateSegment(this.state.appendIndex, this.state.appendIndex,
            this.state.terms.latest());
      }

      this.readerPoolPruner = this.scheduler.scheduleRecurring(Group.RAFT_READER_POOL_PRUNER, () ->
      {
        this.readerPool.prune(1L, TimeUnit.MINUTES);
      }, 1L, 1L, TimeUnit.MINUTES);
    }
  }

  public synchronized void stop() throws Exception {
    if (this.readerPoolPruner != null) {
      this.readerPoolPruner.cancel();
    }

    this.readerPool.close();
    this.state.segments.close();
  }

  public synchronized long append(RaftLogEntry... entries) throws IOException {
    this.ensureOk();

    try {
      RaftLogEntry[] n2 = entries;
      int n3 = entries.length;
      int n4 = 0;

      while (true) {
        if (n4 >= n3) {
          this.state.segments.last().flush();
          break;
        }

        RaftLogEntry entry = n2[n4];
        ++this.state.appendIndex;
        this.state.terms.append(this.state.appendIndex, entry.term());
        this.state.segments.last().write(this.state.appendIndex, entry);
        ++n4;
      }
    } catch (Throwable n6) {
      this.needsRecovery = true;
      throw n6;
    }

    if (this.state.segments.last().position() >= this.rotateAtSize) {
      this.rotateSegment(this.state.appendIndex, this.state.appendIndex, this.state.terms.latest());
    }

    return this.state.appendIndex;
  }

  private void ensureOk() {
    if (this.needsRecovery) {
      throw new IllegalStateException("Raft log requires recovery");
    }
  }

  public synchronized void truncate(long fromIndex) throws IOException {
    if (this.state.appendIndex < fromIndex) {
      throw new IllegalArgumentException(
          "Cannot truncate at index " + fromIndex + " when append index is "
              + this.state.appendIndex);
    } else {
      long newAppendIndex = fromIndex - 1L;
      long newTerm = this.readEntryTerm(newAppendIndex);
      this.truncateSegment(this.state.appendIndex, newAppendIndex, newTerm);
      this.state.appendIndex = newAppendIndex;
      this.state.terms.truncate(fromIndex);
    }
  }

  private void rotateSegment(long prevFileLastIndex, long prevIndex, long prevTerm)
      throws IOException {
    this.state.segments.last().closeWriter();
    this.state.segments.rotate(prevFileLastIndex, prevIndex, prevTerm);
  }

  private void truncateSegment(long prevFileLastIndex, long prevIndex, long prevTerm)
      throws IOException {
    this.state.segments.last().closeWriter();
    this.state.segments.truncate(prevFileLastIndex, prevIndex, prevTerm);
  }

  private void skipSegment(long prevFileLastIndex, long prevIndex, long prevTerm)
      throws IOException {
    this.state.segments.last().closeWriter();
    this.state.segments.skip(prevFileLastIndex, prevIndex, prevTerm);
  }

  public long appendIndex() {
    return this.state.appendIndex;
  }

  public long prevIndex() {
    return this.state.prevIndex;
  }

  public RaftLogCursor getEntryCursor(long fromIndex) {
    IOCursor<EntryRecord> inner = new EntryCursor(this.state.segments, fromIndex);
    return new SegmentedRaftLogCursor(fromIndex, inner);
  }

  public synchronized long skip(long newIndex, long newTerm) throws IOException {
    this.log.info("Skipping from {index: %d, term: %d} to {index: %d, term: %d}",
        this.state.appendIndex, this.state.terms.latest(), newIndex, newTerm);
    if (this.state.appendIndex < newIndex) {
      this.skipSegment(this.state.appendIndex, newIndex, newTerm);
      this.state.terms.skip(newIndex, newTerm);
      this.state.prevIndex = newIndex;
      this.state.prevTerm = newTerm;
      this.state.appendIndex = newIndex;
    }

    return this.state.appendIndex;
  }

  private RaftLogEntry readLogEntry(long logIndex) throws IOException {
    EntryCursor cursor = new EntryCursor(this.state.segments, logIndex);

    RaftLogEntry n4;
    try {
      n4 = cursor.next() ? cursor.get().logEntry() : null;
    } catch (Throwable n7) {
      try {
        cursor.close();
      } catch (Throwable n6) {
        n7.addSuppressed(n6);
      }

      throw n7;
    }

    cursor.close();
    return n4;
  }

  public long readEntryTerm(long logIndex) throws IOException {
    if (logIndex > this.state.appendIndex) {
      return -1L;
    } else {
      long term = this.state.terms.get(logIndex);
      if (term == -1L && logIndex >= this.state.prevIndex) {
        RaftLogEntry entry = this.readLogEntry(logIndex);
        term = entry != null ? entry.term() : -1L;
      }

      return term;
    }
  }

  public long prune(long safeIndex) {
    long pruneIndex = this.pruner.getIndexToPruneFrom(safeIndex, this.state.segments);
    SegmentFile oldestNotDisposed = this.state.segments.prune(pruneIndex);
    long newPrevIndex = oldestNotDisposed.header().prevIndex();
    long newPrevTerm = oldestNotDisposed.header().prevTerm();
    if (newPrevIndex > this.state.prevIndex) {
      this.state.prevIndex = newPrevIndex;
    }

    if (newPrevTerm > this.state.prevTerm) {
      this.state.prevTerm = newPrevTerm;
    }

    this.state.terms.prune(this.state.prevIndex);
    return this.state.prevIndex;
  }
}
