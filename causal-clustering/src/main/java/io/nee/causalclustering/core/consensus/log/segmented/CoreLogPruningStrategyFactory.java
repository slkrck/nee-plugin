/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.consensus.log.segmented;

import org.neo4j.function.Factory;
import org.neo4j.kernel.impl.transaction.log.pruning.ThresholdConfigParser;
import org.neo4j.kernel.impl.transaction.log.pruning.ThresholdConfigParser.ThresholdConfigValue;
import org.neo4j.logging.LogProvider;

public class CoreLogPruningStrategyFactory implements Factory<CoreLogPruningStrategy> {

  private final String pruningStrategyConfig;
  private final LogProvider logProvider;

  public CoreLogPruningStrategyFactory(String pruningStrategyConfig, LogProvider logProvider) {
    this.pruningStrategyConfig = pruningStrategyConfig;
    this.logProvider = logProvider;
  }

  public CoreLogPruningStrategy newInstance() {
    ThresholdConfigValue thresholdConfigValue = ThresholdConfigParser
        .parse(this.pruningStrategyConfig);
    String type = thresholdConfigValue.type;
    long value = thresholdConfigValue.value;
    byte n6 = -1;
    switch (type.hashCode()) {
      case -1591573360:
        if (type.equals("entries")) {
          n6 = 2;
        }
        break;
      case 115311:
        if (type.equals("txs")) {
          n6 = 1;
        }
        break;
      case 3076183:
        if (type.equals("days")) {
          n6 = 4;
        }
        break;
      case 3530753:
        if (type.equals("size")) {
          n6 = 0;
        }
        break;
      case 97196323:
        if (type.equals("false")) {
          n6 = 5;
        }
        break;
      case 99469071:
        if (type.equals("hours")) {
          n6 = 3;
        }
    }

    switch (n6) {
      case 0:
        return new SizeBasedLogPruningStrategy(value);
      case 1:
      case 2:
        return new EntryBasedLogPruningStrategy(value, this.logProvider);
      case 3:
      case 4:
        throw new IllegalArgumentException(
            "Time based pruning not supported yet for the segmented raft log, got '" + type + "'.");
      case 5:
        return new NoPruningPruningStrategy();
      default:
        throw new IllegalArgumentException(
            "Invalid log pruning configuration value '" + value + "'. Invalid type '" + type
                + "', valid are files, size, txs, entries, hours, days.");
    }
  }
}
