/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core.state.snapshot;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.catchup.CatchupAddressResolutionException;
import io.nee.causalclustering.catchup.storecopy.DatabaseShutdownException;
import java.io.IOException;
import java.util.Optional;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class CoreDownloader {

  private final SnapshotDownloader snapshotDownloader;
  private final StoreDownloader storeDownloader;
  private final Log log;

  public CoreDownloader(SnapshotDownloader snapshotDownloader, StoreDownloader storeDownloader,
      LogProvider logProvider) {
    this.snapshotDownloader = snapshotDownloader;
    this.storeDownloader = storeDownloader;
    this.log = logProvider.getLog(this.getClass());
  }

  Optional<CoreSnapshot> downloadSnapshotAndStore(StoreDownloadContext context,
      CatchupAddressProvider addressProvider)
      throws IOException, DatabaseShutdownException {
    Optional<SocketAddress> primaryOpt = this.lookupPrimary(context.databaseId(), addressProvider);
    if (primaryOpt.isEmpty()) {
      return Optional.empty();
    } else {
      SocketAddress primaryAddress = primaryOpt.get();
      Optional<CoreSnapshot> coreSnapshot = this.snapshotDownloader
          .getCoreSnapshot(context.databaseId(), primaryAddress);
      if (coreSnapshot.isEmpty()) {
        return Optional.empty();
      } else {
        return !this.storeDownloader.bringUpToDate(context, primaryAddress, addressProvider)
            ? Optional.empty() : coreSnapshot;
      }
    }
  }

  private Optional<SocketAddress> lookupPrimary(NamedDatabaseId namedDatabaseId,
      CatchupAddressProvider addressProvider) {
    try {
      return Optional.of(addressProvider.primary(namedDatabaseId));
    } catch (CatchupAddressResolutionException n4) {
      this.log.warn(
          "Store copy failed, as we're unable to find the target catchup address. [Message: %s]",
          n4.getMessage());
      return Optional.empty();
    }
  }
}
