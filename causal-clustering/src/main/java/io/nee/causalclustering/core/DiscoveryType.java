/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.core;

import io.nee.causalclustering.discovery.DnsHostnameResolver;
import io.nee.causalclustering.discovery.DomainNameResolverImpl;
import io.nee.causalclustering.discovery.KubernetesResolver;
import io.nee.causalclustering.discovery.NoOpHostnameResolver;
import io.nee.causalclustering.discovery.RemoteMembersResolver;
import io.nee.causalclustering.discovery.SrvHostnameResolver;
import io.nee.causalclustering.discovery.SrvRecordResolverImpl;
import java.util.Arrays;
import java.util.Collection;
import java.util.function.BiFunction;
import org.neo4j.configuration.Config;
import org.neo4j.graphdb.config.Setting;
import org.neo4j.logging.internal.LogService;

public enum DiscoveryType {
  DNS((logService, conf) ->
  {
    return DnsHostnameResolver.resolver(logService, new DomainNameResolverImpl(), conf);
  }, CausalClusteringSettings.initial_discovery_members),
  LIST((logService, conf) ->
  {
    return NoOpHostnameResolver.resolver(conf);
  }, CausalClusteringSettings.initial_discovery_members),
  SRV((logService, conf) ->
  {
    return SrvHostnameResolver.resolver(logService, new SrvRecordResolverImpl(), conf);
  }, CausalClusteringSettings.initial_discovery_members),
  K8S(KubernetesResolver::resolver,
      CausalClusteringSettings.kubernetes_label_selector,
      CausalClusteringSettings.kubernetes_service_port_name);

  private final BiFunction<LogService, Config, RemoteMembersResolver> resolverSupplier;
  private final Collection<Setting<?>> requiredSettings;

  DiscoveryType(BiFunction<LogService, Config, RemoteMembersResolver> resolverSupplier,
      Setting<?>... requiredSettings) {
    this.resolverSupplier = resolverSupplier;
    this.requiredSettings = Arrays.asList(requiredSettings);
  }

  public RemoteMembersResolver getHostnameResolver(LogService logService, Config config) {
    return this.resolverSupplier.apply(logService, config);
  }

  public Collection<Setting<?>> requiredSettings() {
    return this.requiredSettings;
  }
}
