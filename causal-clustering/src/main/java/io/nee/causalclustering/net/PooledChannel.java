/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.net;

import io.nee.causalclustering.protocol.handshake.ProtocolStack;
import io.netty.channel.Channel;
import io.netty.channel.pool.ChannelPool;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.Future;

public class PooledChannel {

  private final Channel channel;
  private final ChannelPool pool;
  private final ProtocolStack protocolStack;

  PooledChannel(Channel channel, ChannelPool pool, ProtocolStack protocolStack) {
    this.channel = channel;
    this.pool = pool;
    this.protocolStack = protocolStack;
  }

  public <ATTR> ATTR getAttribute(AttributeKey<ATTR> key) {
    return this.channel.attr(key).get();
  }

  public Channel channel() {
    return this.channel;
  }

  public ProtocolStack protocolStack() {
    return this.protocolStack;
  }

  public Future<Void> release() {
    return this.pool.release(this.channel);
  }

  public Future<Void> dispose() {
    this.channel.close();
    return this.pool.release(this.channel);
  }
}
