/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.net;

import io.nee.causalclustering.protocol.handshake.ChannelAttribute;
import io.nee.causalclustering.protocol.handshake.ProtocolStack;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.pool.ChannelPool;
import io.netty.channel.pool.ChannelPoolHandler;
import io.netty.channel.socket.SocketChannel;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import java.util.function.Function;
import java.util.stream.Stream;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public class ChannelPoolService implements Lifecycle {

  private final BootstrapConfiguration<? extends SocketChannel> bootstrapConfiguration;
  private final JobScheduler scheduler;
  private final Group group;
  private final ChannelPoolHandler poolHandler;
  private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
  private final WriteLock exclusiveService;
  private final ReadLock sharedService;
  private final ChannelPoolFactory poolFactory;
  private CompletableFuture<PooledChannel> endOfLife;
  private TrackingChannelPoolMap poolMap;
  private EventLoopGroup eventLoopGroup;

  public ChannelPoolService(BootstrapConfiguration<? extends SocketChannel> bootstrapConfiguration,
      JobScheduler scheduler, Group group,
      ChannelPoolHandler channelPoolHandler, ChannelPoolFactory poolFactory) {
    this.exclusiveService = this.lock.writeLock();
    this.sharedService = this.lock.readLock();
    this.bootstrapConfiguration = bootstrapConfiguration;
    this.scheduler = scheduler;
    this.group = group;
    this.poolHandler = channelPoolHandler;
    this.poolFactory = poolFactory;
  }

  private static CompletionStage<PooledChannel> createPooledChannel(Channel channel,
      ChannelPool pool) {
    CompletableFuture<ProtocolStack> protocolFuture = channel.attr(ChannelAttribute.PROTOCOL_STACK)
        .get();
    return protocolFuture == null ? CompletableFuture.failedFuture(
        new IllegalStateException(
            "Channel " + channel + " does not contain a protocol stack attribute"))
        : protocolFuture.thenApply((protocol) ->
        {
          return new PooledChannel(channel, pool, protocol);
        });
  }

  public CompletableFuture<PooledChannel> acquire(SocketAddress address) {
    this.sharedService.lock();

    CompletableFuture n3;
    try {
      if (this.poolMap == null) {
        CompletableFuture n7 = CompletableFuture.failedFuture(
            new IllegalStateException("Channel pool service is not in a started state."));
        return n7;
      }

      ChannelPool pool = this.poolMap.get(address);
      n3 = NettyUtil.toCompletableFuture(pool.acquire()).thenCompose((channel) ->
      {
        return createPooledChannel(channel, pool);
      }).applyToEither(this.endOfLife, Function.identity());
    } finally {
      this.sharedService.unlock();
    }

    return n3;
  }

  public void init() {
  }

  public void start() {
    this.exclusiveService.lock();

    try {
      this.endOfLife = new CompletableFuture();
      this.eventLoopGroup = this.bootstrapConfiguration
          .eventLoopGroup(this.scheduler.executor(this.group));
      Bootstrap baseBootstrap =
          (new Bootstrap()).group(this.eventLoopGroup)
              .channel(this.bootstrapConfiguration.channelClass());
      this.poolMap = new TrackingChannelPoolMap(baseBootstrap, this.poolHandler, this.poolFactory);
    } finally {
      this.exclusiveService.unlock();
    }
  }

  public void stop() {
    this.endOfLife
        .completeExceptionally(new IllegalStateException("Pool is closed. Lifecycle issue?"));
    this.exclusiveService.lock();

    try {
      if (this.poolMap != null) {
        this.poolMap.close();
        this.poolMap = null;
      }

      this.eventLoopGroup.shutdownGracefully(100L, 5000L, TimeUnit.MILLISECONDS)
          .syncUninterruptibly();
    } finally {
      this.exclusiveService.unlock();
    }
  }

  public void shutdown() {
  }

  public Stream<Pair<SocketAddress, ProtocolStack>> installedProtocols() {
    this.sharedService.lock();

    Stream n1;
    try {
      n1 = this.poolMap == null ? Stream.empty() : this.poolMap.installedProtocols();
    } finally {
      this.sharedService.unlock();
    }

    return n1;
  }
}
