/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.net;

import io.nee.causalclustering.helper.ErrorHandler;
import io.nee.causalclustering.protocol.handshake.ChannelAttribute;
import io.nee.causalclustering.protocol.handshake.ProtocolStack;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.pool.AbstractChannelPoolHandler;
import io.netty.channel.pool.AbstractChannelPoolMap;
import io.netty.channel.pool.ChannelPool;
import io.netty.channel.pool.ChannelPoolHandler;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.collection.Pair;

class TrackingChannelPoolMap extends AbstractChannelPoolMap<SocketAddress, ChannelPool> {

  private final Bootstrap baseBootstrap;
  private final TrackingChannelPoolMap.ChannelPoolHandlers poolHandlers;
  private final TrackingChannelPoolMap.InstalledProtocolsTracker protocolsTracker;
  private final ChannelPoolFactory poolFactory;

  TrackingChannelPoolMap(Bootstrap baseBootstrap, ChannelPoolHandler poolHandler,
      ChannelPoolFactory poolFactory) {
    this.baseBootstrap = baseBootstrap;
    this.protocolsTracker = new TrackingChannelPoolMap.InstalledProtocolsTracker();
    this.poolHandlers = new TrackingChannelPoolMap.ChannelPoolHandlers(
        Arrays.asList(poolHandler, this.protocolsTracker));
    this.poolFactory = poolFactory;
  }

  Stream<Pair<SocketAddress, ProtocolStack>> installedProtocols() {
    return this.protocolsTracker.installedProtocols();
  }

  protected ChannelPool newPool(SocketAddress address) {
    return this.poolFactory.create(
        this.baseBootstrap.clone().remoteAddress(
            InetSocketAddress.createUnresolved(address.getHostname(), address.getPort())),
        this.poolHandlers);
  }

  private static final class ChannelPoolHandlers implements ChannelPoolHandler {

    private final Collection<ChannelPoolHandler> poolHandlers;

    ChannelPoolHandlers(Collection<ChannelPoolHandler> poolHandlers) {
      Objects.requireNonNull(poolHandlers);
      this.poolHandlers = Collections.unmodifiableCollection(poolHandlers);
    }

    public void channelReleased(Channel ch) {
      ErrorHandler errorHandler = new ErrorHandler("Channel released");

      try {
        this.poolHandlers.forEach((chh) ->
        {
          errorHandler.execute(() ->
          {
            chh.channelReleased(ch);
          });
        });
      } catch (Throwable n6) {
        try {
          errorHandler.close();
        } catch (Throwable n5) {
          n6.addSuppressed(n5);
        }

        throw n6;
      }

      errorHandler.close();
    }

    public void channelAcquired(Channel ch) {
      ErrorHandler errorHandler = new ErrorHandler("Channel acquired");

      try {
        this.poolHandlers.forEach((chh) ->
        {
          errorHandler.execute(() ->
          {
            chh.channelAcquired(ch);
          });
        });
      } catch (Throwable n6) {
        try {
          errorHandler.close();
        } catch (Throwable n5) {
          n6.addSuppressed(n5);
        }

        throw n6;
      }

      errorHandler.close();
    }

    public void channelCreated(Channel ch) {
      ErrorHandler errorHandler = new ErrorHandler("Channel created");

      try {
        this.poolHandlers.forEach((chh) ->
        {
          errorHandler.execute(() ->
          {
            chh.channelCreated(ch);
          });
        });
      } catch (Throwable n6) {
        try {
          errorHandler.close();
        } catch (Throwable n5) {
          n6.addSuppressed(n5);
        }

        throw n6;
      }

      errorHandler.close();
    }
  }

  private static class InstalledProtocolsTracker extends AbstractChannelPoolHandler {

    private final Collection<Channel> createdChannels = ConcurrentHashMap.newKeySet();

    Stream<Pair<SocketAddress, ProtocolStack>> installedProtocols() {
      return this.createdChannels.stream().filter(Channel::isActive).map((ch) ->
      {
        InetSocketAddress address = (InetSocketAddress) ch.remoteAddress();
        return Pair.of(new SocketAddress(address.getHostName(),
                address.getPort()),
            (ProtocolStack) ((CompletableFuture) ch
                .attr(ChannelAttribute.PROTOCOL_STACK).get())
                .getNow(null));
      }).filter((pair) ->
      {
        return pair.other() != null;
      });
    }

    public void channelCreated(Channel ch) {
      this.createdChannels.add(ch);
      ch.closeFuture().addListener((f) ->
      {
        this.createdChannels.remove(ch);
      });
    }
  }
}
