/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.net;

import io.nee.causalclustering.core.CausalClusteringSettings;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.socket.ServerSocketChannel;
import io.netty.channel.socket.SocketChannel;
import java.util.concurrent.Executor;
import org.neo4j.configuration.Config;

public interface BootstrapConfiguration<TYPE extends Channel> {

  static BootstrapConfiguration<? extends ServerSocketChannel> serverConfig(Config config) {
    return preferNative(config) && Epoll.isAvailable() ? EpollBootstrapConfig.epollServerConfig()
        : NioBootstrapConfig.nioServerConfig();
  }

  static boolean preferNative(Config config) {
    return config.get(CausalClusteringSettings.use_native_transport);
  }

  static BootstrapConfiguration<? extends SocketChannel> clientConfig(Config config) {
    return preferNative(config) && Epoll.isAvailable() ? EpollBootstrapConfig.epollClientConfig()
        : NioBootstrapConfig.nioClientConfig();
  }

  EventLoopGroup eventLoopGroup(Executor n1);

  Class<TYPE> channelClass();
}
