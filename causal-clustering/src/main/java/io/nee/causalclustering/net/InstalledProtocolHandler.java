/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.net;

import io.nee.causalclustering.protocol.handshake.ProtocolStack;
import io.nee.causalclustering.protocol.handshake.ServerHandshakeFinishedEvent;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.collection.Pair;

@Sharable
public class InstalledProtocolHandler extends ChannelInboundHandlerAdapter {

  private final ConcurrentMap<SocketAddress, ProtocolStack> installedProtocols = new ConcurrentHashMap();

  public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
    if (evt instanceof ServerHandshakeFinishedEvent.Created) {
      ServerHandshakeFinishedEvent.Created created = (ServerHandshakeFinishedEvent.Created) evt;
      this.installedProtocols.put(created.advertisedSocketAddress, created.protocolStack);
    } else if (evt instanceof ServerHandshakeFinishedEvent.Closed) {
      ServerHandshakeFinishedEvent.Closed closed = (ServerHandshakeFinishedEvent.Closed) evt;
      this.installedProtocols.remove(closed.advertisedSocketAddress);
    } else {
      super.userEventTriggered(ctx, evt);
    }
  }

  public Stream<Pair<SocketAddress, ProtocolStack>> installedProtocols() {
    return this.installedProtocols.entrySet().stream().map((entry) ->
    {
      return Pair.of(entry.getKey(), entry.getValue());
    });
  }
}
