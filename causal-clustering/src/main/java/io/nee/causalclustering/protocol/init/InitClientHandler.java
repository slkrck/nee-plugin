/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.init;

import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.DecoderException;
import io.netty.handler.codec.ReplayingDecoder;
import java.util.List;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class InitClientHandler extends ReplayingDecoder<Void> {

  static final String NAME = "init_client_handler";
  private final ChannelInitializer<?> handshakeInitializer;
  private final NettyPipelineBuilderFactory pipelineBuilderFactory;
  private final LogProvider logProvider;
  private final Log log;

  InitClientHandler(ChannelInitializer<?> handshakeInitializer,
      NettyPipelineBuilderFactory pipelineBuilderFactory, LogProvider logProvider) {
    this.handshakeInitializer = handshakeInitializer;
    this.pipelineBuilderFactory = pipelineBuilderFactory;
    this.logProvider = logProvider;
    this.log = logProvider.getLog(this.getClass());
    this.setSingleDecode(true);
  }

  public void channelActive(ChannelHandlerContext ctx) {
    ctx.writeAndFlush(MagicValueUtil.magicValueBuf(), ctx.voidPromise());
  }

  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
    String receivedMagicValue = MagicValueUtil.readMagicValue(in);
    if (MagicValueUtil.isCorrectMagicValue(receivedMagicValue)) {
      this.log.debug("Channel %s received a correct magic message", ctx.channel());
      this.pipelineBuilderFactory.client(ctx.channel(),
          this.logProvider.getLog(this.handshakeInitializer.getClass())).addFraming()
          .add("handshake_initializer",
              new ChannelHandler[]{this.handshakeInitializer}).install();
    } else {
      ctx.close();
      throw new DecoderException("Wrong magic value: '" + receivedMagicValue + "'");
    }
  }
}
