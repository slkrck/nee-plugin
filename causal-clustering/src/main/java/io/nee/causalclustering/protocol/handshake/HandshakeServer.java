/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.messaging.Channel;
import io.nee.causalclustering.protocol.application.ApplicationProtocol;
import io.nee.causalclustering.protocol.application.ApplicationProtocolVersion;
import io.nee.causalclustering.protocol.modifier.ModifierProtocol;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class HandshakeServer implements ServerMessageHandler {

  private final Channel channel;
  private final ApplicationProtocolRepository applicationProtocolRepository;
  private final ModifierProtocolRepository modifierProtocolRepository;
  private final SupportedProtocols<ApplicationProtocolVersion, ApplicationProtocol> supportedApplicationProtocol;
  private final ProtocolStack.Builder protocolStackBuilder = ProtocolStack.builder();
  private final CompletableFuture<ProtocolStack> protocolStackFuture = new CompletableFuture();

  HandshakeServer(ApplicationProtocolRepository applicationProtocolRepository,
      ModifierProtocolRepository modifierProtocolRepository, Channel channel) {
    this.channel = channel;
    this.applicationProtocolRepository = applicationProtocolRepository;
    this.modifierProtocolRepository = modifierProtocolRepository;
    this.supportedApplicationProtocol = applicationProtocolRepository.supportedProtocol();
  }

  public void handle(ApplicationProtocolRequest request) {
    ApplicationProtocolResponse response;
    if (!request.protocolName()
        .equals(this.supportedApplicationProtocol.identifier().canonicalName())) {
      response = ApplicationProtocolResponse.NO_PROTOCOL;
      this.channel.writeAndFlush(response);
      this.decline(String.format("Requested protocol %s not supported", request.protocolName()));
    } else {
      Optional<ApplicationProtocol> selected = this.applicationProtocolRepository
          .select(request.protocolName(), this.supportedVersionsFor(request));
      if (selected.isPresent()) {
        ApplicationProtocol selectedProtocol = selected.get();
        this.protocolStackBuilder.application(selectedProtocol);
        response = new ApplicationProtocolResponse(StatusCode.SUCCESS, selectedProtocol.category(),
            selectedProtocol.implementation());
        this.channel.writeAndFlush(response);
      } else {
        response = ApplicationProtocolResponse.NO_PROTOCOL;
        this.channel.writeAndFlush(response);
        this.decline(String
            .format("Do not support requested protocol %s versions %s", request.protocolName(),
                request.versions()));
      }
    }
  }

  public void handle(ModifierProtocolRequest modifierProtocolRequest) {
    Optional<ModifierProtocol> selected =
        this.modifierProtocolRepository.select(modifierProtocolRequest.protocolName(),
            this.supportedVersionsFor(modifierProtocolRequest));
    ModifierProtocolResponse response;
    if (selected.isPresent()) {
      ModifierProtocol modifierProtocol = selected.get();
      this.protocolStackBuilder.modifier(modifierProtocol);
      response = new ModifierProtocolResponse(StatusCode.SUCCESS, modifierProtocol.category(),
          modifierProtocol.implementation());
    } else {
      response = ModifierProtocolResponse.failure(modifierProtocolRequest.protocolName());
    }

    this.channel.writeAndFlush(response);
  }

  public void handle(SwitchOverRequest switchOverRequest) {
    ProtocolStack protocolStack = this.protocolStackBuilder.build();
    Optional<ApplicationProtocol> switchOverProtocol =
        this.applicationProtocolRepository
            .select(switchOverRequest.protocolName(), switchOverRequest.version());
    List<ModifierProtocol> switchOverModifiers = switchOverRequest.modifierProtocols().stream()
        .map((pair) ->
        {
          return this.modifierProtocolRepository
              .select(pair.first(),
                  pair.other());
        }).flatMap(Optional::stream)
        .collect(Collectors.toList());
    if (switchOverProtocol.isEmpty()) {
      this.channel.writeAndFlush(SwitchOverResponse.FAILURE);
      this.decline(String
          .format("Cannot switch to protocol %s version %s", switchOverRequest.protocolName(),
              switchOverRequest.version()));
    } else if (protocolStack.applicationProtocol() == null) {
      this.channel.writeAndFlush(SwitchOverResponse.FAILURE);
      this.decline(String
          .format("Attempted to switch to protocol %s version %s before negotiation complete",
              switchOverRequest.protocolName(),
              switchOverRequest.version()));
    } else if (!switchOverProtocol.get().equals(protocolStack.applicationProtocol())) {
      this.channel.writeAndFlush(SwitchOverResponse.FAILURE);
      this.decline(String
          .format("Switch over mismatch: requested %s version %s but negotiated %s version %s",
              switchOverRequest.protocolName(),
              switchOverRequest.version(), protocolStack.applicationProtocol().category(),
              protocolStack.applicationProtocol().implementation()));
    } else if (!switchOverModifiers.equals(protocolStack.modifierProtocols())) {
      this.channel.writeAndFlush(SwitchOverResponse.FAILURE);
      this.decline(String.format("Switch over mismatch: requested modifiers %s but negotiated %s",
          switchOverRequest.modifierProtocols(),
          protocolStack.modifierProtocols()));
    } else {
      SwitchOverResponse response = new SwitchOverResponse(StatusCode.SUCCESS);
      this.channel.writeAndFlush(response);
      this.protocolStackFuture.complete(protocolStack);
    }
  }

  private Set<String> supportedVersionsFor(ModifierProtocolRequest request) {
    return this.modifierProtocolRepository.supportedProtocolFor(request.protocolName())
        .map((supported) ->
        {
          return supported.mutuallySupportedVersionsFor(
              request.versions());
        }).orElse(Collections.emptySet());
  }

  private Set<ApplicationProtocolVersion> supportedVersionsFor(ApplicationProtocolRequest request) {
    return this.supportedApplicationProtocol.mutuallySupportedVersionsFor(request.versions());
  }

  private void decline(String message) {
    this.channel.dispose();
    this.protocolStackFuture
        .completeExceptionally(new ServerHandshakeException(message, this.protocolStackBuilder));
  }

  CompletableFuture<ProtocolStack> protocolStackFuture() {
    return this.protocolStackFuture;
  }
}
