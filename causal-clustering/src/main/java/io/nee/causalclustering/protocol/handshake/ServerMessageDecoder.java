/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.messaging.marshalling.StringMarshal;
import io.nee.causalclustering.protocol.application.ApplicationProtocolVersion;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.internal.helpers.collection.Pair;

public class ServerMessageDecoder extends ByteToMessageDecoder {

  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
    int messageCode = in.readInt();
    switch (messageCode) {
      case 1:
        ApplicationProtocolRequest applicationProtocolRequest =
            this.decodeProtocolRequest(ApplicationProtocolRequest::new, in,
                ApplicationProtocolVersion::decode);
        out.add(applicationProtocolRequest);
        return;
      case 2:
        ModifierProtocolRequest modifierProtocolRequest =
            this.decodeProtocolRequest(ModifierProtocolRequest::new, in, StringMarshal::unmarshal);
        out.add(modifierProtocolRequest);
        return;
      case 3:
        String protocolName = StringMarshal.unmarshal(in);
        ApplicationProtocolVersion version = ApplicationProtocolVersion.decode(in);
        int numberOfModifierProtocols = in.readInt();
        List<Pair<String, String>> modifierProtocols = Stream.generate(() ->
        {
          return Pair.of(StringMarshal.unmarshal(in),
              StringMarshal.unmarshal(in));
        }).limit(numberOfModifierProtocols).collect(Collectors.toList());
        out.add(new SwitchOverRequest(protocolName, version, modifierProtocols));
        return;
      default:
        throw new IllegalStateException();
    }
  }

  private <U extends Comparable<U>, T extends BaseProtocolRequest<U>> T decodeProtocolRequest(
      BiFunction<String, Set<U>, T> constructor, ByteBuf in,
      Function<ByteBuf, U> versionDecoder) {
    String protocolName = StringMarshal.unmarshal(in);
    int versionArrayLength = in.readInt();
    Set<U> versions = Stream.generate(() ->
    {
      return (U) versionDecoder.apply(in);
    }).limit(versionArrayLength).collect(Collectors.toSet());
    return constructor.apply(protocolName, versions);
  }
}
