/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.protocol.Protocol;
import io.nee.causalclustering.protocol.application.ApplicationProtocol;
import io.nee.causalclustering.protocol.modifier.ModifierProtocol;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ProtocolStack {

  private final ApplicationProtocol applicationProtocol;
  private final List<ModifierProtocol> modifierProtocols;

  public ProtocolStack(ApplicationProtocol applicationProtocol,
      List<ModifierProtocol> modifierProtocols) {
    this.applicationProtocol = applicationProtocol;
    this.modifierProtocols = Collections.unmodifiableList(modifierProtocols);
  }

  public static ProtocolStack.Builder builder() {
    return new ProtocolStack.Builder();
  }

  public ApplicationProtocol applicationProtocol() {
    return this.applicationProtocol;
  }

  public List<ModifierProtocol> modifierProtocols() {
    return this.modifierProtocols;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ProtocolStack that = (ProtocolStack) o;
      return Objects.equals(this.applicationProtocol, that.applicationProtocol) && Objects
          .equals(this.modifierProtocols, that.modifierProtocols);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.applicationProtocol, this.modifierProtocols);
  }

  public String toString() {
    String desc = String.format("%s version:%s", this.applicationProtocol.category(),
        this.applicationProtocol.implementation());
    List<String> modifierNames = this.modifierProtocols.stream().map(Protocol::implementation)
        .collect(Collectors.toList());
    if (!modifierNames.isEmpty()) {
      desc = String.format("%s (%s)", desc, String.join(", ", modifierNames));
    }

    return desc;
  }

  public static class Builder {

    private final List<ModifierProtocol> modifierProtocols = new ArrayList();
    private ApplicationProtocol applicationProtocol;

    private Builder() {
    }

    public ProtocolStack.Builder modifier(ModifierProtocol modifierProtocol) {
      this.modifierProtocols.add(modifierProtocol);
      return this;
    }

    public ProtocolStack.Builder application(ApplicationProtocol applicationProtocol) {
      this.applicationProtocol = applicationProtocol;
      return this;
    }

    ProtocolStack build() {
      return new ProtocolStack(this.applicationProtocol, this.modifierProtocols);
    }

    public String toString() {
      return "Builder{applicationProtocol=" + this.applicationProtocol + ", modifierProtocols="
          + this.modifierProtocols + "}";
    }
  }
}
