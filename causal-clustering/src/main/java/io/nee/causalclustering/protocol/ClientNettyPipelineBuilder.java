/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import javax.net.ssl.SSLException;
import org.neo4j.logging.Log;
import org.neo4j.ssl.SslPolicy;

public class ClientNettyPipelineBuilder extends
    NettyPipelineBuilder<ProtocolInstaller.Orientation.Client, ClientNettyPipelineBuilder> {

  private static final int LENGTH_FIELD_BYTES = 4;

  ClientNettyPipelineBuilder(ChannelPipeline pipeline, SslPolicy sslPolicy, Log log) {
    super(pipeline, sslPolicy, log);
  }

  public ClientNettyPipelineBuilder addFraming() {
    this.add("frame_encoder", new LengthFieldPrepender(4));
    this.add("frame_decoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
    return this;
  }

  ChannelHandler createSslHandler(Channel channel, SslPolicy sslPolicy) throws SSLException {
    return sslPolicy.nettyClientHandler(channel);
  }
}
