/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.init;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;
import io.netty.buffer.UnpooledByteBufAllocator;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

final class MagicValueUtil {

  private static final String MAGIC_VALUE = "NEO4J_CLUSTER";
  private static final ByteBuf MAGIC_VALUE_BUF = createMagicValueBuf();

  private MagicValueUtil() {
  }

  static ByteBuf magicValueBuf() {
    return MAGIC_VALUE_BUF.duplicate();
  }

  static String readMagicValue(ByteBuf buf) {
    int length = MAGIC_VALUE_BUF.writerIndex();
    return buf.readCharSequence(length, StandardCharsets.US_ASCII).toString();
  }

  static boolean isCorrectMagicValue(String value) {
    return Objects.equals("NEO4J_CLUSTER", value);
  }

  private static ByteBuf createMagicValueBuf() {
    ByteBuf buf = ByteBufUtil.writeAscii(UnpooledByteBufAllocator.DEFAULT, "NEO4J_CLUSTER");
    ByteBuf unreleasableBuf = Unpooled.unreleasableBuffer(buf);
    return unreleasableBuf.asReadOnly();
  }
}
