/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import java.util.Objects;

public class GateEvent {

  private static final GateEvent SUCCESS = new GateEvent(true);
  private static final GateEvent FAILURE = new GateEvent(false);
  private final boolean isSuccess;

  private GateEvent(boolean isSuccess) {
    this.isSuccess = isSuccess;
  }

  public static GateEvent getSuccess() {
    return SUCCESS;
  }

  public static GateEvent getFailure() {
    return FAILURE;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      GateEvent that = (GateEvent) o;
      return this.isSuccess == that.isSuccess;
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.isSuccess);
  }
}
