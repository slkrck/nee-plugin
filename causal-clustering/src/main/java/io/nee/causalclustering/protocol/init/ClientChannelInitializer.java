/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.init;

import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.handshake.ChannelAttribute;
import io.nee.causalclustering.protocol.handshake.ProtocolStack;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.timeout.ReadTimeoutHandler;
import java.nio.channels.ClosedChannelException;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ClientChannelInitializer extends ChannelInitializer<Channel> {

  private final ChannelInitializer<?> handshakeInitializer;
  private final NettyPipelineBuilderFactory pipelineBuilderFactory;
  private final Duration timeout;
  private final LogProvider logProvider;
  private final Log log;

  public ClientChannelInitializer(ChannelInitializer<?> handshakeInitializer,
      NettyPipelineBuilderFactory pipelineBuilderFactory, Duration timeout,
      LogProvider logProvider) {
    this.handshakeInitializer = handshakeInitializer;
    this.pipelineBuilderFactory = pipelineBuilderFactory;
    this.timeout = timeout;
    this.logProvider = logProvider;
    this.log = logProvider.getLog(this.getClass());
  }

  public void initChannel(Channel channel) {
    this.log.info("Initializing client channel %s", channel);
    CompletableFuture<ProtocolStack> protocolFuture = new CompletableFuture();
    channel.attr(ChannelAttribute.PROTOCOL_STACK).set(protocolFuture);
    channel.closeFuture().addListener((ignore) ->
    {
      protocolFuture.completeExceptionally(new ClosedChannelException());
    });
    this.pipelineBuilderFactory.client(channel, this.log).add("read_timeout_handler",
        new ChannelHandler[]{
            new ReadTimeoutHandler(
                this.timeout
                    .toMillis(),
                TimeUnit.MILLISECONDS)})
        .add("init_client_handler",
            new ChannelHandler[]{
                new InitClientHandler(this.handshakeInitializer, this.pipelineBuilderFactory,
                    this.logProvider)}).install();
  }
}
