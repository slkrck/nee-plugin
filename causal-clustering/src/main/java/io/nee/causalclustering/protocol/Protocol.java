/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface Protocol<IMPL extends Comparable<IMPL>> {

  static <IMPL extends Comparable<IMPL>, T extends Protocol<IMPL>> Optional<T> find(T[] values,
      Protocol.Category<T> category, IMPL implementation,
      Function<IMPL, IMPL> normalise) {
    return Stream.of(values).filter((protocol) ->
    {
      return Objects.equals(protocol.category(), category.canonicalName());
    }).filter((protocol) ->
    {
      return Objects
          .equals(normalise.apply(protocol.implementation()), normalise.apply(implementation));
    }).findFirst();
  }

  static <IMPL extends Comparable<IMPL>, T extends Protocol<IMPL>> List<T> filterCategory(
      T[] values, Protocol.Category<T> category,
      Predicate<IMPL> implPredicate) {
    return Stream.of(values).filter((protocol) ->
    {
      return Objects.equals(protocol.category(), category.canonicalName());
    }).filter((protocol) ->
    {
      return implPredicate.test(protocol.implementation());
    }).collect(Collectors.toList());
  }

  String category();

  IMPL implementation();

  interface Category<T extends Protocol<?>> {

    String canonicalName();
  }
}
