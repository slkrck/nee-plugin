/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.application;

import io.netty.buffer.ByteBuf;
import java.util.Objects;
import org.neo4j.util.Preconditions;

public class ApplicationProtocolVersion implements Comparable<ApplicationProtocolVersion> {

  private final int major;
  private final int minor;

  public ApplicationProtocolVersion(int major, int minor) {
    this.major = Preconditions.requireNonNegative(major);
    this.minor = Preconditions.requireNonNegative(minor);
  }

  public static ApplicationProtocolVersion parse(String value) {
    String[] split = value.split("\\.");
    if (split.length != 2) {
      throw illegalStringValue(value);
    } else {
      try {
        int major = Integer.parseInt(split[0]);
        int minor = Integer.parseInt(split[1]);
        return new ApplicationProtocolVersion(major, minor);
      } catch (NumberFormatException n4) {
        throw illegalStringValue(value);
      }
    }
  }

  public static ApplicationProtocolVersion decode(ByteBuf buf) {
    return new ApplicationProtocolVersion(buf.readInt(), buf.readInt());
  }

  private static RuntimeException illegalStringValue(String value) {
    return new IllegalArgumentException("Illegal protocol version string: " + value +
        ". Expected a value in format '<major>.<minor>' where both major and minor are non-negative integer values");
  }

  public int major() {
    return this.major;
  }

  public int minor() {
    return this.minor;
  }

  public void encode(ByteBuf buf) {
    buf.writeInt(this.major);
    buf.writeInt(this.minor);
  }

  public int compareTo(ApplicationProtocolVersion other) {
    int majorComparison = Integer.compare(this.major, other.major);
    return majorComparison == 0 ? Integer.compare(this.minor, other.minor) : majorComparison;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ApplicationProtocolVersion that = (ApplicationProtocolVersion) o;
      return this.major == that.major && this.minor == that.minor;
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.major, this.minor);
  }

  public String toString() {
    return this.major + "." + this.minor;
  }
}
