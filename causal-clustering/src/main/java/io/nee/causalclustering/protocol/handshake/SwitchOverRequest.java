/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.protocol.application.ApplicationProtocolVersion;
import java.util.List;
import java.util.Objects;
import org.neo4j.internal.helpers.collection.Pair;

public class SwitchOverRequest implements ServerMessage {

  static final int MESSAGE_CODE = 3;
  private final String protocolName;
  private final ApplicationProtocolVersion version;
  private final List<Pair<String, String>> modifierProtocols;

  public SwitchOverRequest(String applicationProtocolName,
      ApplicationProtocolVersion applicationProtocolVersion,
      List<Pair<String, String>> modifierProtocols) {
    this.protocolName = applicationProtocolName;
    this.version = applicationProtocolVersion;
    this.modifierProtocols = modifierProtocols;
  }

  public void dispatch(ServerMessageHandler handler) {
    handler.handle(this);
  }

  public String protocolName() {
    return this.protocolName;
  }

  public List<Pair<String, String>> modifierProtocols() {
    return this.modifierProtocols;
  }

  public ApplicationProtocolVersion version() {
    return this.version;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      SwitchOverRequest that = (SwitchOverRequest) o;
      return Objects.equals(this.version, that.version) && Objects
          .equals(this.protocolName, that.protocolName) &&
          Objects.equals(this.modifierProtocols, that.modifierProtocols);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.protocolName, this.version, this.modifierProtocols);
  }

  public String toString() {
    return "SwitchOverRequest{protocolName='" + this.protocolName + "', version=" + this.version
        + ", modifierProtocols=" + this.modifierProtocols + "}";
  }
}
