/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.application;

import io.nee.causalclustering.protocol.Protocol;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import org.neo4j.function.Predicates;

public enum ApplicationProtocols implements ApplicationProtocol {
  RAFT_2_0(ApplicationProtocolCategory.RAFT, new ApplicationProtocolVersion(2, 0)),
  CATCHUP_3_0(ApplicationProtocolCategory.CATCHUP, new ApplicationProtocolVersion(3, 0));

  private final ApplicationProtocolVersion version;
  private final ApplicationProtocolCategory identifier;

  ApplicationProtocols(ApplicationProtocolCategory identifier, ApplicationProtocolVersion version) {
    this.identifier = identifier;
    this.version = version;
  }

  public static Optional<ApplicationProtocol> find(ApplicationProtocolCategory category,
      ApplicationProtocolVersion version) {
    return Protocol.find(values(), category, version, Function.identity());
  }

  public static List<ApplicationProtocol> withCategory(ApplicationProtocolCategory category) {
    return Protocol.filterCategory(values(), category, Predicates.alwaysTrue());
  }

  public String category() {
    return this.identifier.canonicalName();
  }

  public ApplicationProtocolVersion implementation() {
    return this.version;
  }
}
