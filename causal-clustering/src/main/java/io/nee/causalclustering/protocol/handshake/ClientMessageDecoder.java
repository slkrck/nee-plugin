/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.messaging.marshalling.StringMarshal;
import io.nee.causalclustering.protocol.application.ApplicationProtocolVersion;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import org.neo4j.function.TriFunction;

public class ClientMessageDecoder extends ByteToMessageDecoder {

  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out)
      throws ClientHandshakeException {
    int messageCode = in.readInt();
    switch (messageCode) {
      case 0:
        ApplicationProtocolResponse applicationProtocolResponse =
            this.decodeProtocolResponse(ApplicationProtocolResponse::new,
                ApplicationProtocolVersion::decode, in);
        out.add(applicationProtocolResponse);
        return;
      case 1:
        ModifierProtocolResponse modifierProtocolResponse =
            this.decodeProtocolResponse(ModifierProtocolResponse::new, StringMarshal::unmarshal,
                in);
        out.add(modifierProtocolResponse);
        return;
      case 2:
        int statusCodeValue = in.readInt();
        Optional<StatusCode> statusCode = StatusCode.fromCodeValue(statusCodeValue);
        if (statusCode.isPresent()) {
          out.add(new SwitchOverResponse(statusCode.get()));
        }

        return;
      default:
    }
  }

  private <U extends Comparable<U>, T extends BaseProtocolResponse<U>> T decodeProtocolResponse(
      TriFunction<StatusCode, String, U, T> constructor,
      Function<ByteBuf, U> reader, ByteBuf in)
      throws ClientHandshakeException {
    int statusCodeValue = in.readInt();
    String identifier = StringMarshal.unmarshal(in);
    U version = reader.apply(in);
    Optional<StatusCode> statusCode = StatusCode.fromCodeValue(statusCodeValue);
    return statusCode.map((status) ->
    {
      return constructor.apply(status, identifier, version);
    }).orElseThrow(() ->
    {
      return new ClientHandshakeException(
          String.format("Unknown status code %s for protocol %s version %s", statusCodeValue,
              identifier,
              version));
    });
  }
}
