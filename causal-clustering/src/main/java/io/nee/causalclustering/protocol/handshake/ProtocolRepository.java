/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.protocol.Protocol;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.neo4j.internal.helpers.collection.Pair;

public abstract class ProtocolRepository<U extends Comparable<U>, T extends Protocol<U>> {

  private final Map<Pair<String, U>, T> protocolMap;
  private final BiFunction<String, Set<U>, ProtocolSelection<U, T>> protocolSelectionFactory;
  private final Function<String, Comparator<T>> comparator;

  public ProtocolRepository(T[] protocols, Function<String, Comparator<T>> comparators,
      BiFunction<String, Set<U>, ProtocolSelection<U, T>> protocolSelectionFactory) {
    this.protocolSelectionFactory = protocolSelectionFactory;
    Map<Pair<String, U>, T> map = new HashMap();
    Protocol[] n5 = protocols;
    int n6 = protocols.length;

    for (int n7 = 0; n7 < n6; ++n7) {
      T protocol = (T) n5[n7];
      Protocol<U> previous = map
          .put(Pair.of(protocol.category(), protocol.implementation()), protocol);
      if (previous != null) {
        throw new IllegalArgumentException(
            String.format("Multiple protocols with same identifier and version supplied: %s and %s",
                protocol, previous));
      }
    }

    this.protocolMap = Collections.unmodifiableMap(map);
    this.comparator = comparators;
  }

  static <U extends Comparable<U>, T extends Protocol<U>> Comparator<T> versionNumberComparator() {
    return Comparator.comparing(Protocol::implementation);
  }

  Optional<T> select(String protocolName, U version) {
    return Optional.ofNullable(this.protocolMap.get(Pair.of(protocolName, version)));
  }

  Optional<T> select(String protocolName, Set<U> versions) {
    return versions.stream().map((version) ->
    {
      return this.select(protocolName, version);
    }).flatMap(Optional::stream).max((Comparator) this.comparator.apply(protocolName));
  }

  public ProtocolSelection<U, T> getAll(Protocol.Category<T> category, Collection<U> versions) {
    Set<U> selectedVersions = this.protocolMap.keySet().stream().filter((pair) ->
    {
      return pair.first().equals(category.canonicalName());
    }).map(Pair::other).filter((version) ->
    {
      return versions.isEmpty() ||
          versions.contains(version);
    }).collect(Collectors.toSet());
    if (selectedVersions.isEmpty()) {
      throw new IllegalArgumentException(
          String.format(
              "Attempted to select protocols for %s versions %s but no match in known protocols %s",
              category, versions,
              this.protocolMap));
    } else {
      return this.protocolSelectionFactory.apply(category.canonicalName(), selectedVersions);
    }
  }
}
