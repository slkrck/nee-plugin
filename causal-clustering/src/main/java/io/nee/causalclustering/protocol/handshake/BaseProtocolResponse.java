/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import java.util.Objects;

public abstract class BaseProtocolResponse<IMPL extends Comparable<IMPL>> implements ClientMessage {

  private final StatusCode statusCode;
  private final String protocolName;
  private final IMPL version;

  BaseProtocolResponse(StatusCode statusCode, String protocolName, IMPL version) {
    this.statusCode = statusCode;
    this.protocolName = protocolName;
    this.version = version;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      BaseProtocolResponse that = (BaseProtocolResponse) o;
      return Objects.equals(this.version, that.version) && Objects
          .equals(this.protocolName, that.protocolName);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.protocolName, this.version);
  }

  public StatusCode statusCode() {
    return this.statusCode;
  }

  public String protocolName() {
    return this.protocolName;
  }

  public IMPL version() {
    return this.version;
  }

  public String toString() {
    return "BaseProtocolResponse{statusCode=" + this.statusCode + ", protocolName='"
        + this.protocolName + "', version=" + this.version + "}";
  }
}
