/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.messaging.SimpleNettyChannel;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.ProtocolInstaller;
import io.nee.causalclustering.protocol.ProtocolInstallerRepository;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.neo4j.internal.helpers.ExponentialBackoffStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class HandshakeClientInitializer extends ChannelInitializer<SocketChannel> {

  private final ApplicationProtocolRepository applicationProtocolRepository;
  private final ModifierProtocolRepository modifierProtocolRepository;
  private final Duration timeout;
  private final ProtocolInstallerRepository<ProtocolInstaller.Orientation.Client> protocolInstaller;
  private final NettyPipelineBuilderFactory pipelineBuilderFactory;
  private final TimeoutStrategy handshakeDelay;
  private final Log debugLog;
  private final Log userLog;

  public HandshakeClientInitializer(ApplicationProtocolRepository applicationProtocolRepository,
      ModifierProtocolRepository modifierProtocolRepository,
      ProtocolInstallerRepository<ProtocolInstaller.Orientation.Client> protocolInstallerRepository,
      NettyPipelineBuilderFactory pipelineBuilderFactory,
      Duration handshakeTimeout, LogProvider debugLogProvider, LogProvider userLogProvider) {
    this.debugLog = debugLogProvider.getLog(this.getClass());
    this.userLog = userLogProvider.getLog(this.getClass());
    this.applicationProtocolRepository = applicationProtocolRepository;
    this.modifierProtocolRepository = modifierProtocolRepository;
    this.timeout = handshakeTimeout;
    this.protocolInstaller = protocolInstallerRepository;
    this.pipelineBuilderFactory = pipelineBuilderFactory;
    this.handshakeDelay = new ExponentialBackoffStrategy(1L, 2000L, TimeUnit.MILLISECONDS);
  }

  private static HandshakeClient newHandshakeClient(Channel channel) {
    CompletableFuture<ProtocolStack> protocolFuture = channel.attr(ChannelAttribute.PROTOCOL_STACK)
        .get();
    if (protocolFuture == null) {
      throw new IllegalStateException(
          "Channel " + channel + " does not contain a protocol stack attribute");
    } else {
      return new HandshakeClient(protocolFuture);
    }
  }

  protected void initChannel(SocketChannel channel) {
    HandshakeClient handshakeClient = newHandshakeClient(channel);

    try {
      this.installHandlers(channel, handshakeClient);
    } catch (Exception n4) {
      handshakeClient.protocol().completeExceptionally(n4);
    }

    this.scheduleHandshake(channel, handshakeClient, this.handshakeDelay.newTimeout());
    this.scheduleTimeout(channel, handshakeClient);
  }

  private void installHandlers(Channel channel, HandshakeClient handshakeClient) throws Exception {
    this.pipelineBuilderFactory
        .client(
            channel, this.debugLog).addFraming()
        .add("handshake_client_encoder", new ChannelHandler[]{new ClientMessageEncoder()}).add(
        "handshake_client_decoder", new ChannelHandler[]{new ClientMessageDecoder()})
        .add("handshake_client",
            new ChannelHandler[]{
                new NettyHandshakeClient(handshakeClient)})
        .addGate((msg) ->
        {
          return !(msg instanceof ServerMessage);
        }).install();
  }

  private void scheduleHandshake(SocketChannel ch, HandshakeClient handshakeClient,
      Timeout handshakeDelay) {
    ch.eventLoop().schedule(() ->
    {
      if (ch.isActive()) {
        this.initiateHandshake(ch, handshakeClient);
      } else if (ch.isOpen()) {
        handshakeDelay.increment();
        this.scheduleHandshake(ch, handshakeClient, handshakeDelay);
      }
    }, handshakeDelay.getMillis(), TimeUnit.MILLISECONDS);
  }

  private void scheduleTimeout(SocketChannel ch, HandshakeClient handshakeClient) {
    ch.eventLoop().schedule(() ->
    {
      handshakeClient.protocol()
          .completeExceptionally(new TimeoutException("Handshake timed out after " + this.timeout));
    }, this.timeout.toMillis(), TimeUnit.MILLISECONDS);
  }

  private void initiateHandshake(Channel channel, HandshakeClient handshakeClient) {
    this.debugLog.info("Initiating handshake on channel %s", channel);
    SimpleNettyChannel channelWrapper = new SimpleNettyChannel(channel, this.debugLog);
    handshakeClient.initiate(channelWrapper, this.applicationProtocolRepository,
        this.modifierProtocolRepository);
    handshakeClient.protocol().whenComplete((protocolStack, failure) ->
    {
      this.onHandshakeComplete(protocolStack, channel, failure);
    });
  }

  private void onHandshakeComplete(ProtocolStack protocolStack, Channel channel,
      Throwable failure) {
    if (failure != null) {
      this.debugLog
          .error(String.format("Error when negotiating protocol stack on channel %s", channel),
              failure);
      channel.pipeline().fireUserEventTriggered(GateEvent.getFailure());
      channel.close();
    } else {
      try {
        this.userLog(protocolStack, channel);
        this.debugLog
            .info("Handshake completed on channel %s. Installing: %s", channel, protocolStack);
        this.protocolInstaller.installerFor(protocolStack).install(channel);
        channel.pipeline().fireUserEventTriggered(GateEvent.getSuccess());
        channel.flush();
      } catch (Exception n5) {
        this.debugLog
            .error(String.format("Error installing protocol stack on channel %s", channel), n5);
        channel.close();
      }
    }
  }

  private void userLog(ProtocolStack protocolStack, Channel channel) {
    this.userLog
        .info(String.format("Connected to %s [%s]", channel.remoteAddress(), protocolStack));
    channel.closeFuture().addListener((f) ->
    {
      this.userLog.info(
          String.format("Lost connection to %s [%s]", channel.remoteAddress(), protocolStack));
    });
  }
}
