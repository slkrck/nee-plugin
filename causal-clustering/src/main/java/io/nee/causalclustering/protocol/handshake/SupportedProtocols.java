/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.protocol.Protocol;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class SupportedProtocols<U extends Comparable<U>, T extends Protocol<U>> {

  private final Protocol.Category<T> category;
  private final List<U> versions;

  SupportedProtocols(Protocol.Category<T> category, List<U> versions) {
    this.category = category;
    this.versions = Collections.unmodifiableList(versions);
  }

  public Set<U> mutuallySupportedVersionsFor(Set<U> requestedVersions) {
    if (this.versions().isEmpty()) {
      return requestedVersions;
    } else {
      Stream n10000 = requestedVersions.stream();
      List n10001 = this.versions();
      Objects.requireNonNull(n10001);
      return (Set) n10000.filter(n10001::contains).collect(Collectors.toSet());
    }
  }

  public Protocol.Category<T> identifier() {
    return this.category;
  }

  public List<U> versions() {
    return this.versions;
  }
}
