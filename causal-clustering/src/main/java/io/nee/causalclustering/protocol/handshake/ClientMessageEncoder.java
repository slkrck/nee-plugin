/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.messaging.marshalling.StringMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import java.util.function.BiConsumer;

public class ClientMessageEncoder extends MessageToByteEncoder<ServerMessage> {

  protected void encode(ChannelHandlerContext ctx, ServerMessage msg, ByteBuf out) {
    msg.dispatch(new ClientMessageEncoder.Encoder(out));
  }

  class Encoder implements ServerMessageHandler {

    private final ByteBuf out;

    Encoder(ByteBuf out) {
      this.out = out;
    }

    public void handle(ApplicationProtocolRequest applicationProtocolRequest) {
      this.out.writeInt(1);
      this.encodeProtocolRequest(applicationProtocolRequest, (buf, version) ->
      {
        version.encode(buf);
      });
    }

    public void handle(ModifierProtocolRequest modifierProtocolRequest) {
      this.out.writeInt(2);
      this.encodeProtocolRequest(modifierProtocolRequest, StringMarshal::marshal);
    }

    public void handle(SwitchOverRequest switchOverRequest) {
      this.out.writeInt(3);
      StringMarshal.marshal(this.out, switchOverRequest.protocolName());
      switchOverRequest.version().encode(this.out);
      this.out.writeInt(switchOverRequest.modifierProtocols().size());
      switchOverRequest.modifierProtocols().forEach((pair) ->
      {
        StringMarshal.marshal(this.out, pair.first());
        StringMarshal.marshal(this.out, pair.other());
      });
    }

    private <U extends Comparable<U>> void encodeProtocolRequest(
        BaseProtocolRequest<U> protocolRequest, BiConsumer<ByteBuf, U> writer) {
      StringMarshal.marshal(this.out, protocolRequest.protocolName());
      this.out.writeInt(protocolRequest.versions().size());
      protocolRequest.versions().forEach((version) ->
      {
        writer.accept(this.out, version);
      });
    }
  }
}
