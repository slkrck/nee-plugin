/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.messaging.SimpleNettyChannel;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.ProtocolInstaller;
import io.nee.causalclustering.protocol.ProtocolInstallerRepository;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import java.net.InetSocketAddress;
import java.util.concurrent.RejectedExecutionException;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class HandshakeServerInitializer extends ChannelInitializer<SocketChannel> {

  private final Log log;
  private final ApplicationProtocolRepository applicationProtocolRepository;
  private final ModifierProtocolRepository modifierProtocolRepository;
  private final ProtocolInstallerRepository<ProtocolInstaller.Orientation.Server> protocolInstallerRepository;
  private final NettyPipelineBuilderFactory pipelineBuilderFactory;
  private final Config config;

  public HandshakeServerInitializer(ApplicationProtocolRepository applicationProtocolRepository,
      ModifierProtocolRepository modifierProtocolRepository,
      ProtocolInstallerRepository<ProtocolInstaller.Orientation.Server> protocolInstallerRepository,
      NettyPipelineBuilderFactory pipelineBuilderFactory,
      LogProvider logProvider, Config config) {
    this.log = logProvider.getLog(this.getClass());
    this.applicationProtocolRepository = applicationProtocolRepository;
    this.modifierProtocolRepository = modifierProtocolRepository;
    this.protocolInstallerRepository = protocolInstallerRepository;
    this.pipelineBuilderFactory = pipelineBuilderFactory;
    this.config = config;
  }

  public void initChannel(SocketChannel ch) {
    if (this.config
        .get(CausalClusteringSettings.inbound_connection_initialization_logging_enabled)) {
      this.log.info("Installing handshake server on channel %s", ch);
    }

    this.pipelineBuilderFactory.server(ch,
        this.log).addFraming()
        .add("handshake_server_encoder",
            new ChannelHandler[]{
                new ServerMessageEncoder()})
        .add("handshake_server_decoder",
            new ChannelHandler[]{new ServerMessageDecoder()}).add("handshake_server",
        new ChannelHandler[]{this.createHandshakeServer(ch)}).install();
  }

  private NettyHandshakeServer createHandshakeServer(SocketChannel channel) {
    HandshakeServer handshakeServer =
        new HandshakeServer(this.applicationProtocolRepository, this.modifierProtocolRepository,
            new SimpleNettyChannel(channel, this.log));
    handshakeServer.protocolStackFuture().whenComplete((protocolStack, failure) ->
    {
      this.onHandshakeComplete(protocolStack, channel, failure);
    });
    return new NettyHandshakeServer(handshakeServer);
  }

  private void onHandshakeComplete(ProtocolStack protocolStack, SocketChannel channel,
      Throwable failure) {
    if (failure != null) {
      this.log.error(String.format("Error when negotiating protocol stack on channel %s", channel),
          failure);
    } else {
      try {
        this.log.info("Handshake completed on channel %s. Installing: %s", channel, protocolStack);
        this.protocolInstallerRepository.installerFor(protocolStack).install(channel);
        this.registerChannelAndProtocol(channel, protocolStack);
      } catch (Throwable n5) {
        this.log.error(String.format("Error installing protocol stack on channel %s", channel), n5);
      }
    }
  }

  private void registerChannelAndProtocol(SocketChannel channel, ProtocolStack protocolStack) {
    SocketAddress remoteAddress = this.getRemoteAddress(channel);
    if (remoteAddress != null) {
      ChannelPipeline parent = channel.parent().pipeline();
      parent.fireUserEventTriggered(
          new ServerHandshakeFinishedEvent.Created(remoteAddress, protocolStack));
      channel.closeFuture().addListener((f) ->
      {
        try {
          parent.fireUserEventTriggered(new ServerHandshakeFinishedEvent.Closed(remoteAddress));
        } catch (RejectedExecutionException n4) {
        }
      });
    }
  }

  private SocketAddress getRemoteAddress(SocketChannel channel) {
    InetSocketAddress remoteAddress = channel.remoteAddress();
    return remoteAddress == null ? null
        : new SocketAddress(remoteAddress.getHostString(), remoteAddress.getPort());
  }
}
