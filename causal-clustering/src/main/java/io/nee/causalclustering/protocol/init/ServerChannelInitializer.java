/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.init;

import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.net.ChildInitializer;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.timeout.ReadTimeoutHandler;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.neo4j.configuration.Config;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ServerChannelInitializer implements ChildInitializer {

  private final ChannelInitializer<?> handshakeInitializer;
  private final NettyPipelineBuilderFactory pipelineBuilderFactory;
  private final Duration timeout;
  private final LogProvider logProvider;
  private final Log log;
  private final Config config;

  public ServerChannelInitializer(ChannelInitializer<?> handshakeInitializer,
      NettyPipelineBuilderFactory pipelineBuilderFactory, Duration timeout,
      LogProvider logProvider, Config config) {
    this.handshakeInitializer = handshakeInitializer;
    this.pipelineBuilderFactory = pipelineBuilderFactory;
    this.timeout = timeout;
    this.logProvider = logProvider;
    this.log = logProvider.getLog(this.getClass());
    this.config = config;
  }

  public void initChannel(Channel channel) {
    if (this.config
        .get(CausalClusteringSettings.inbound_connection_initialization_logging_enabled)) {
      this.log.info("Initializing server channel %s", channel);
    }

    this.pipelineBuilderFactory.server(channel, this.log).add("read_timeout_handler",
        new ChannelHandler[]{
            new ReadTimeoutHandler(
                this.timeout
                    .toMillis(),
                TimeUnit.MILLISECONDS)})
        .add("init_server_handler",
            new ChannelHandler[]{
                new InitServerHandler(this.handshakeInitializer, this.pipelineBuilderFactory,
                    this.logProvider)}).install();
  }
}
