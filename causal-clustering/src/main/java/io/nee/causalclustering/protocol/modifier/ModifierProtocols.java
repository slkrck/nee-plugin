/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.modifier;

import io.nee.causalclustering.protocol.Protocol;
import java.util.Optional;

public enum ModifierProtocols implements ModifierProtocol {
  COMPRESSION_GZIP(ModifierProtocolCategory.COMPRESSION, "Gzip"),
  COMPRESSION_SNAPPY(ModifierProtocolCategory.COMPRESSION, "Snappy"),
  COMPRESSION_SNAPPY_VALIDATING(ModifierProtocolCategory.COMPRESSION, "Snappy_validating"),
  COMPRESSION_LZ4(ModifierProtocolCategory.COMPRESSION, "LZ4"),
  COMPRESSION_LZ4_HIGH_COMPRESSION(ModifierProtocolCategory.COMPRESSION, "LZ4_high_compression"),
  COMPRESSION_LZ4_VALIDATING(ModifierProtocolCategory.COMPRESSION, "LZ_validating"),
  COMPRESSION_LZ4_HIGH_COMPRESSION_VALIDATING(ModifierProtocolCategory.COMPRESSION,
      "LZ4_high_compression_validating");

  public static final String ALLOWED_VALUES_STRING =
      "[Gzip, Snappy, Snappy_validating, LZ4, LZ4_high_compression, LZ_validating, LZ4_high_compression_validating]";
  private final String friendlyName;
  private final ModifierProtocolCategory identifier;

  ModifierProtocols(ModifierProtocolCategory identifier, String friendlyName) {
    this.identifier = identifier;
    this.friendlyName = friendlyName;
  }

  public static Optional<ModifierProtocol> find(ModifierProtocolCategory category,
      String friendlyName) {
    return Protocol.find(values(), category, friendlyName, String::toLowerCase);
  }

  public String implementation() {
    return this.friendlyName;
  }

  public String category() {
    return this.identifier.canonicalName();
  }

  private static class Implementations {

    static final String GZIP = "Gzip";
    static final String SNAPPY = "Snappy";
    static final String SNAPPY_VALIDATING = "Snappy_validating";
    static final String LZ4 = "LZ4";
    static final String LZ4_HIGH_COMPRESSION = "LZ4_high_compression";
    static final String LZ_VALIDATING = "LZ_validating";
    static final String LZ4_HIGH_COMPRESSION_VALIDATING = "LZ4_high_compression_validating";
  }
}
