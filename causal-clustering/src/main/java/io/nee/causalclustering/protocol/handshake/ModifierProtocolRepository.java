/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.protocol.modifier.ModifierProtocol;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ModifierProtocolRepository extends ProtocolRepository<String, ModifierProtocol> {

  private final Collection<ModifierSupportedProtocols> supportedProtocols;
  private final Map<String, ModifierSupportedProtocols> supportedProtocolsLookup;

  public ModifierProtocolRepository(ModifierProtocol[] protocols,
      Collection<ModifierSupportedProtocols> supportedProtocols) {
    super(protocols, getModifierProtocolComparator(supportedProtocols),
        ModifierProtocolSelection::new);
    this.supportedProtocols = Collections.unmodifiableCollection(supportedProtocols);
    this.supportedProtocolsLookup = supportedProtocols.stream().collect(Collectors.toMap((supp) ->
    {
      return supp.identifier().canonicalName();
    }, Function.identity()));
  }

  static Function<String, Comparator<ModifierProtocol>> getModifierProtocolComparator(
      Collection<ModifierSupportedProtocols> supportedProtocols) {
    return getModifierProtocolComparator(versionMap(supportedProtocols));
  }

  private static Map<String, List<String>> versionMap(
      Collection<ModifierSupportedProtocols> supportedProtocols) {
    return supportedProtocols.stream().collect(Collectors.toMap((supportedProtocol) ->
    {
      return supportedProtocol.identifier().canonicalName();
    }, SupportedProtocols::versions));
  }

  private static Function<String, Comparator<ModifierProtocol>> getModifierProtocolComparator(
      Map<String, List<String>> versionMap) {
    return (protocolName) ->
    {
      Comparator<ModifierProtocol> positionalComparator = Comparator.comparing((modifierProtocol) ->
      {
        return Optional
            .ofNullable((List) versionMap.get(protocolName))
            .map((versions) ->
            {
              return byPosition(modifierProtocol, versions);
            }).orElse(0);
      });
      return fallBackToVersionNumbers(positionalComparator);
    };
  }

  private static Comparator<ModifierProtocol> fallBackToVersionNumbers(
      Comparator<ModifierProtocol> positionalComparator) {
    return positionalComparator.thenComparing(versionNumberComparator());
  }

  private static Integer byPosition(ModifierProtocol modifierProtocol, List<String> versions) {
    int index = versions.indexOf(modifierProtocol.implementation());
    return index == -1 ? Integer.MIN_VALUE : -index;
  }

  public Optional<SupportedProtocols<String, ModifierProtocol>> supportedProtocolFor(
      String protocolName) {
    return Optional
        .ofNullable((SupportedProtocols) this.supportedProtocolsLookup.get(protocolName));
  }

  public Collection<ModifierSupportedProtocols> supportedProtocols() {
    return this.supportedProtocols;
  }
}
