/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum StatusCode {
  SUCCESS(0),
  ONGOING(1),
  FAILURE(-1);

  private static final AtomicReference<Map<Integer, StatusCode>> CODE_MAP = new AtomicReference();
  private final int codeValue;

  StatusCode(int codeValue) {
    this.codeValue = codeValue;
  }

  public static Optional<StatusCode> fromCodeValue(int codeValue) {
    Map<Integer, StatusCode> map = CODE_MAP.get();
    if (map == null) {
      map = Stream.of(values())
          .collect(Collectors.toMap(StatusCode::codeValue, Function.identity()));
      CODE_MAP.compareAndSet(null, map);
    }

    return Optional.ofNullable(map.get(codeValue));
  }

  public int codeValue() {
    return this.codeValue;
  }
}
