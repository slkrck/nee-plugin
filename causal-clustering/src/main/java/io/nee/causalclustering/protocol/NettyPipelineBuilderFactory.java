/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol;

import io.netty.channel.Channel;
import org.neo4j.logging.Log;
import org.neo4j.ssl.SslPolicy;

public class NettyPipelineBuilderFactory {

  private final SslPolicy sslPolicy;

  public NettyPipelineBuilderFactory(SslPolicy sslPolicy) {
    this.sslPolicy = sslPolicy;
  }

  public static NettyPipelineBuilderFactory insecure() {
    return new NettyPipelineBuilderFactory(null);
  }

  public ClientNettyPipelineBuilder client(Channel channel, Log log) {
    return NettyPipelineBuilder.client(channel.pipeline(), this.sslPolicy, log);
  }

  public ServerNettyPipelineBuilder server(Channel channel, Log log) {
    return NettyPipelineBuilder.server(channel.pipeline(), this.sslPolicy, log);
  }
}
