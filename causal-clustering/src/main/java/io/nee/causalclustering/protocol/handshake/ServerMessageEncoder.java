/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol.handshake;

import io.nee.causalclustering.messaging.marshalling.StringMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import java.util.function.BiConsumer;

public class ServerMessageEncoder extends MessageToByteEncoder<ClientMessage> {

  protected void encode(ChannelHandlerContext ctx, ClientMessage msg, ByteBuf out) {
    msg.dispatch(new ServerMessageEncoder.Encoder(out));
  }

  class Encoder implements ClientMessageHandler {

    private final ByteBuf out;

    Encoder(ByteBuf out) {
      this.out = out;
    }

    public void handle(ApplicationProtocolResponse applicationProtocolResponse) {
      this.out.writeInt(0);
      this.encodeProtocolResponse(applicationProtocolResponse, (buf, version) ->
      {
        version.encode(buf);
      });
    }

    public void handle(ModifierProtocolResponse modifierProtocolResponse) {
      this.out.writeInt(1);
      this.encodeProtocolResponse(modifierProtocolResponse, StringMarshal::marshal);
    }

    public void handle(SwitchOverResponse switchOverResponse) {
      this.out.writeInt(2);
      this.out.writeInt(switchOverResponse.status().codeValue());
    }

    private <U extends Comparable<U>> void encodeProtocolResponse(
        BaseProtocolResponse<U> protocolResponse, BiConsumer<ByteBuf, U> writer) {
      this.out.writeInt(protocolResponse.statusCode().codeValue());
      StringMarshal.marshal(this.out, protocolResponse.protocolName());
      writer.accept(this.out, protocolResponse.version());
    }
  }
}
