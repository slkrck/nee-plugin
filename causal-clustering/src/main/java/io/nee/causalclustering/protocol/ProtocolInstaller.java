/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.protocol;

import io.nee.causalclustering.protocol.application.ApplicationProtocol;
import io.nee.causalclustering.protocol.modifier.ModifierProtocol;
import io.netty.channel.Channel;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

public interface ProtocolInstaller<O extends ProtocolInstaller.Orientation> {

  void install(Channel n1) throws Exception;

  ApplicationProtocol applicationProtocol();

  Collection<Collection<ModifierProtocol>> modifiers();

  interface Orientation {

    interface Client extends ProtocolInstaller.Orientation {

      String OUTBOUND = "outbound";
    }

    interface Server extends ProtocolInstaller.Orientation {

      String INBOUND = "inbound";
    }
  }

  abstract class Factory<O extends ProtocolInstaller.Orientation, I extends ProtocolInstaller<O>> {

    private final ApplicationProtocol applicationProtocol;
    private final Function<List<ModifierProtocolInstaller<O>>, I> constructor;

    protected Factory(ApplicationProtocol applicationProtocol,
        Function<List<ModifierProtocolInstaller<O>>, I> constructor) {
      this.applicationProtocol = applicationProtocol;
      this.constructor = constructor;
    }

    I create(List<ModifierProtocolInstaller<O>> modifiers) {
      return this.constructor.apply(modifiers);
    }

    public ApplicationProtocol applicationProtocol() {
      return this.applicationProtocol;
    }
  }
}
