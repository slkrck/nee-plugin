/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging;

import java.io.IOException;
import java.util.Objects;
import org.neo4j.kernel.impl.transaction.log.LogPositionMarker;
import org.neo4j.kernel.impl.transaction.log.ReadableClosablePositionAwareChecksumChannel;

public class ReadableNetworkChannelDelegator implements
    ReadableClosablePositionAwareChecksumChannel {

  private ReadableClosablePositionAwareChecksumChannel delegate;

  public void delegateTo(ReadableClosablePositionAwareChecksumChannel channel) {
    this.delegate = Objects.requireNonNull(channel);
  }

  public LogPositionMarker getCurrentPosition(LogPositionMarker positionMarker) throws IOException {
    this.assertAssigned();
    return this.delegate.getCurrentPosition(positionMarker);
  }

  public byte get() throws IOException {
    this.assertAssigned();
    return this.delegate.get();
  }

  public short getShort() throws IOException {
    this.assertAssigned();
    return this.delegate.getShort();
  }

  public int getInt() throws IOException {
    this.assertAssigned();
    return this.delegate.getInt();
  }

  public long getLong() throws IOException {
    this.assertAssigned();
    return this.delegate.getLong();
  }

  public float getFloat() throws IOException {
    this.assertAssigned();
    return this.delegate.getFloat();
  }

  public double getDouble() throws IOException {
    this.assertAssigned();
    return this.delegate.getDouble();
  }

  public void get(byte[] bytes, int length) throws IOException {
    this.assertAssigned();
    this.delegate.get(bytes, length);
  }

  public void close() throws IOException {
    this.assertAssigned();
    this.delegate.close();
  }

  private void assertAssigned() {
    if (this.delegate == null) {
      throw new IllegalArgumentException("No assigned channel to delegate reads");
    }
  }

  public void beginChecksum() {
  }

  public int endChecksumAndValidate() throws IOException {
    return 0;
  }
}
