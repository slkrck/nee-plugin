/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling;

import io.nee.causalclustering.core.consensus.NewLeaderBarrier;
import io.nee.causalclustering.core.consensus.membership.MemberIdSet;
import io.nee.causalclustering.core.consensus.membership.MemberIdSetSerializer;
import io.nee.causalclustering.core.replication.DistributedOperation;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.core.state.machines.dummy.DummyRequest;
import io.nee.causalclustering.core.state.machines.lease.ReplicatedLeaseMarshalV2;
import io.nee.causalclustering.core.state.machines.lease.ReplicatedLeaseRequest;
import io.nee.causalclustering.core.state.machines.token.ReplicatedTokenRequest;
import io.nee.causalclustering.core.state.machines.token.ReplicatedTokenRequestMarshalV2;
import io.nee.causalclustering.core.state.machines.tx.ByteArrayReplicatedTransaction;
import io.nee.causalclustering.core.state.machines.tx.ReplicatedTransactionMarshalV2;
import io.nee.causalclustering.core.state.machines.tx.TransactionRepresentationReplicatedTransaction;
import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.messaging.EndOfStreamException;
import java.io.IOException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class CoreReplicatedContentMarshalV2 extends SafeChannelMarshal<ReplicatedContent> {

  public static ContentBuilder<ReplicatedContent> unmarshal(byte contentType,
      ReadableChannel channel) throws IOException, EndOfStreamException {
    switch (contentType) {
      case 0:
        return ContentBuilder.finished(ReplicatedTransactionMarshalV2.unmarshal(channel));
      case 1:
        return ContentBuilder.finished(MemberIdSetSerializer.unmarshal(channel));
      case 2:
      case 3:
      default:
        throw new IllegalStateException("Not a recognized content type: " + contentType);
      case 4:
        return ContentBuilder.finished(ReplicatedTokenRequestMarshalV2.unmarshal(channel));
      case 5:
        return ContentBuilder.finished(new NewLeaderBarrier());
      case 6:
        return ContentBuilder.finished(ReplicatedLeaseMarshalV2.unmarshal(channel));
      case 7:
        return DistributedOperation.deserialize(channel);
      case 8:
        return ContentBuilder.finished(DummyRequest.Marshal.INSTANCE.unmarshal(channel));
    }
  }

  public void marshal(ReplicatedContent replicatedContent, WritableChannel channel)
      throws IOException {
    replicatedContent.dispatch(new CoreReplicatedContentMarshalV2.MarshallingHandler(channel));
  }

  protected ReplicatedContent unmarshal0(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    byte type = channel.get();

    ContentBuilder contentBuilder;
    for (contentBuilder = unmarshal(type, channel); !contentBuilder.isComplete();
        contentBuilder = contentBuilder.combine(unmarshal(type, channel))) {
      type = channel.get();
    }

    return (ReplicatedContent) contentBuilder.build();
  }

  private static class MarshallingHandler implements ReplicatedContentHandler {

    private final WritableChannel writableChannel;

    MarshallingHandler(WritableChannel writableChannel) {
      this.writableChannel = writableChannel;
    }

    public void handle(ByteArrayReplicatedTransaction tx) throws IOException {
      this.writableChannel.put((byte) 0);
      ReplicatedTransactionMarshalV2.marshal(this.writableChannel, tx);
    }

    public void handle(TransactionRepresentationReplicatedTransaction tx) throws IOException {
      this.writableChannel.put((byte) 0);
      ReplicatedTransactionMarshalV2.marshal(this.writableChannel, tx);
    }

    public void handle(MemberIdSet memberIdSet) throws IOException {
      this.writableChannel.put((byte) 1);
      MemberIdSetSerializer.marshal(memberIdSet, this.writableChannel);
    }

    public void handle(ReplicatedTokenRequest replicatedTokenRequest) throws IOException {
      this.writableChannel.put((byte) 4);
      ReplicatedTokenRequestMarshalV2.marshal(replicatedTokenRequest, this.writableChannel);
    }

    public void handle(NewLeaderBarrier newLeaderBarrier) throws IOException {
      this.writableChannel.put((byte) 5);
    }

    public void handle(ReplicatedLeaseRequest replicatedLeaseRequest) throws IOException {
      this.writableChannel.put((byte) 6);
      ReplicatedLeaseMarshalV2.marshal(replicatedLeaseRequest, this.writableChannel);
    }

    public void handle(DistributedOperation distributedOperation) throws IOException {
      this.writableChannel.put((byte) 7);
      distributedOperation.marshalMetaData(this.writableChannel);
    }

    public void handle(DummyRequest dummyRequest) throws IOException {
      this.writableChannel.put((byte) 8);
      DummyRequest.Marshal.INSTANCE.marshal(dummyRequest, this.writableChannel);
    }
  }
}
