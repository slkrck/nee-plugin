/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging;

import io.nee.causalclustering.net.ChannelPoolService;
import io.nee.causalclustering.net.PooledChannel;
import io.netty.channel.ChannelFuture;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RaftSender implements Outbound<SocketAddress, Message> {

  private final ChannelPoolService channels;
  private final Log log;

  public RaftSender(LogProvider logProvider, RaftChannelPoolService channelPoolService) {
    this.channels = channelPoolService;
    this.log = logProvider.getLog(this.getClass());
  }

  public void send(SocketAddress to, Message message, boolean block) {
    CompletableFuture<Void> fOperation = this.channels.acquire(to).thenCompose((pooledChannel) ->
    {
      return this.sendMessage(pooledChannel, message);
    });
    if (block) {
      try {
        fOperation.get();
      } catch (ExecutionException n6) {
        this.log.error("Exception while sending to: " + to, n6);
      } catch (InterruptedException n7) {
        fOperation.cancel(true);
        this.log.info("Interrupted while sending", n7);
      }
    } else {
      fOperation.whenComplete((ignore, throwable) ->
      {
        if (throwable != null) {
          this.log.warn("Raft sender failed exceptionally [Address: " + to + "]", throwable);
        }
      });
    }
  }

  private CompletableFuture<Void> sendMessage(PooledChannel pooledChannel, Message message) {
    try {
      CompletableFuture<Void> fOperation = new CompletableFuture();
      fOperation.whenComplete((ignore, ex) ->
      {
        if (ex instanceof CancellationException) {
          pooledChannel.dispose();
        }
      });
      ChannelFuture fWrite = pooledChannel.channel().writeAndFlush(message);
      fWrite.addListener((writeComplete) ->
      {
        if (!writeComplete.isSuccess()) {
          pooledChannel.dispose();
          fOperation.completeExceptionally(this.wrapCause(pooledChannel, writeComplete.cause()));
        } else {
          try {
            pooledChannel.release().addListener((f) ->
            {
              fOperation.complete(null);
            });
          } catch (Throwable n5) {
            fOperation.complete(null);
          }
        }
      });
      return fOperation;
    } catch (Throwable n5) {
      pooledChannel.dispose();
      throw this.wrapCause(pooledChannel, n5);
    }
  }

  private CompletionException wrapCause(PooledChannel pooledChannel, Throwable e) {
    return new CompletionException("[ChannelId: " + pooledChannel.channel().id() + "]", e);
  }
}
