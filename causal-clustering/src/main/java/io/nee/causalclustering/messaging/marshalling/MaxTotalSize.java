/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling;

import io.nee.causalclustering.messaging.MessageTooBigException;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;
import org.neo4j.io.ByteUnit;
import org.neo4j.util.Preconditions;

public class MaxTotalSize implements ChunkedInput<ByteBuf> {

  private static final int DEFAULT_MAX_SIZE = (int) ByteUnit.gibiBytes(1L);
  private final ChunkedInput<ByteBuf> chunkedInput;
  private final int maxSize;
  private int totalSize;

  MaxTotalSize(ChunkedInput<ByteBuf> chunkedInput, int maxSize) {
    Preconditions.requirePositive(maxSize);
    this.chunkedInput = chunkedInput;
    this.maxSize = maxSize;
  }

  MaxTotalSize(ChunkedInput<ByteBuf> chunkedInput) {
    this(chunkedInput, DEFAULT_MAX_SIZE);
  }

  public boolean isEndOfInput() throws Exception {
    return this.chunkedInput.isEndOfInput();
  }

  public void close() throws Exception {
    this.chunkedInput.close();
  }

  public ByteBuf readChunk(ChannelHandlerContext ctx) throws Exception {
    return this.readChunk(ctx.alloc());
  }

  public ByteBuf readChunk(ByteBufAllocator allocator) throws Exception {
    ByteBuf byteBuf = this.chunkedInput.readChunk(allocator);
    if (byteBuf != null) {
      int additionalBytes = byteBuf.readableBytes();
      this.totalSize += additionalBytes;
      if (this.totalSize > this.maxSize) {
        throw new MessageTooBigException(
            String.format("Size limit exceeded. Limit is %d, wanted to write %d, written so far %d",
                this.maxSize, additionalBytes,
                this.totalSize - additionalBytes));
      }
    }

    return byteBuf;
  }

  public long length() {
    return this.chunkedInput.length();
  }

  public long progress() {
    return this.chunkedInput.progress();
  }
}
