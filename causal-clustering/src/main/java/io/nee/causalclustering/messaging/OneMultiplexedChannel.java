/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging;

import io.nee.causalclustering.net.NettyUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.pool.ChannelPoolHandler;
import io.netty.channel.pool.SimpleChannelPool;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.Promise;

public class OneMultiplexedChannel extends SimpleChannelPool {

  private volatile Future<io.netty.channel.Channel> fChannel;

  OneMultiplexedChannel(Bootstrap bootstrap, ChannelPoolHandler handler) {
    super(bootstrap, handler);
  }

  private static void closeAwait(Future<io.netty.channel.Channel> fChannel) {
    if (fChannel != null) {
      fChannel.awaitUninterruptibly();
      io.netty.channel.Channel channel = fChannel.getNow();
      if (channel != null) {
        channel.close().awaitUninterruptibly();
      }
    }
  }

  public Future<io.netty.channel.Channel> acquire(Promise<io.netty.channel.Channel> promise) {
    Future<io.netty.channel.Channel> fChannelAlias = this.fChannel;
    if (fChannelAlias != null && fChannelAlias.isSuccess()) {
      io.netty.channel.Channel channel = fChannelAlias.getNow();
      if (channel != null && channel.isActive()) {
        promise.trySuccess(channel);
        return promise;
      }
    }

    return this.acquireSync(promise);
  }

  private synchronized Future<io.netty.channel.Channel> acquireSync(
      Promise<io.netty.channel.Channel> promise) {
    if (this.fChannel == null) {
      return this.fChannel = super.acquire(promise);
    } else if (!this.fChannel.isDone()) {
      return NettyUtil.chain(this.fChannel, promise);
    } else {
      io.netty.channel.Channel channel = this.fChannel.getNow();
      if (channel != null && channel.isActive()) {
        promise.trySuccess(channel);
        return promise;
      } else {
        return this.fChannel = super.acquire(promise);
      }
    }
  }

  public Future<Void> release(io.netty.channel.Channel channel, Promise<Void> promise) {
    promise.trySuccess(null);
    return promise;
  }

  public void close() {
    super.close();
    closeAwait(this.fChannel);
  }
}
