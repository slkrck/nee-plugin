/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.neo4j.io.fs.ReadableChannel;

public class InputStreamReadableChannel implements ReadableChannel {

  private final DataInputStream dataInputStream;

  public InputStreamReadableChannel(InputStream inputStream) {
    this.dataInputStream = new DataInputStream(inputStream);
  }

  public byte get() throws IOException {
    return this.dataInputStream.readByte();
  }

  public short getShort() throws IOException {
    return this.dataInputStream.readShort();
  }

  public int getInt() throws IOException {
    return this.dataInputStream.readInt();
  }

  public long getLong() throws IOException {
    return this.dataInputStream.readLong();
  }

  public float getFloat() throws IOException {
    return this.dataInputStream.readFloat();
  }

  public double getDouble() throws IOException {
    return this.dataInputStream.readDouble();
  }

  public void get(byte[] bytes, int length) throws IOException {
    this.dataInputStream.read(bytes, 0, length);
  }

  public void close() throws IOException {
    this.dataInputStream.close();
  }
}
