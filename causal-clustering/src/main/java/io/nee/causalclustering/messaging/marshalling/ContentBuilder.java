/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling;

import java.util.function.Function;

public class ContentBuilder<CONTENT> {

  private boolean isComplete;
  private Function<CONTENT, CONTENT> contentFunction;

  private ContentBuilder(Function<CONTENT, CONTENT> contentFunction, boolean isComplete) {
    this.contentFunction = contentFunction;
    this.isComplete = isComplete;
  }

  public static <C> ContentBuilder<C> emptyUnfinished() {
    return new ContentBuilder((content) ->
    {
      return content;
    }, false);
  }

  public static <C> ContentBuilder<C> unfinished(Function<C, C> contentFunction) {
    return new ContentBuilder(contentFunction, false);
  }

  public static <C> ContentBuilder<C> finished(C content) {
    return new ContentBuilder((ignored) ->
    {
      return content;
    }, true);
  }

  public boolean isComplete() {
    return this.isComplete;
  }

  public ContentBuilder<CONTENT> combine(ContentBuilder<CONTENT> contentBuilder) {
    if (this.isComplete) {
      throw new IllegalStateException(
          "This content builder has already completed and cannot be combined.");
    } else {
      this.contentFunction = this.contentFunction.compose(contentBuilder.contentFunction);
      this.isComplete = contentBuilder.isComplete;
      return this;
    }
  }

  public CONTENT build() {
    if (!this.isComplete) {
      throw new IllegalStateException("Cannot build unfinished content");
    } else {
      return this.contentFunction.apply(null);
    }
  }
}
