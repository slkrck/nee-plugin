/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling.v2.decoding;

import io.nee.causalclustering.catchup.Protocol;
import io.nee.causalclustering.messaging.marshalling.v2.ContentType;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

public class ContentTypeDispatcher extends ChannelInboundHandlerAdapter {

  private final Protocol<ContentType> contentTypeProtocol;

  public ContentTypeDispatcher(Protocol<ContentType> contentTypeProtocol) {
    this.contentTypeProtocol = contentTypeProtocol;
  }

  public void channelRead(ChannelHandlerContext ctx, Object msg) {
    if (msg instanceof ByteBuf) {
      ByteBuf buffer = (ByteBuf) msg;
      if (this.contentTypeProtocol.isExpecting(ContentType.ContentType)) {
        byte messageCode = buffer.readByte();
        ContentType contentType = this.getContentType(messageCode);
        this.contentTypeProtocol.expect(contentType);
        if (buffer.readableBytes() == 0) {
          ReferenceCountUtil.release(msg);
          return;
        }
      }
    }

    ctx.fireChannelRead(msg);
  }

  private ContentType getContentType(byte messageCode) {
    ContentType[] n2 = ContentType.values();
    int n3 = n2.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      ContentType contentType = n2[n4];
      if (contentType.get() == messageCode) {
        return contentType;
      }
    }

    throw new IllegalArgumentException(
        "Illegal inbound. Could not find a ContentType with value " + messageCode);
  }
}
