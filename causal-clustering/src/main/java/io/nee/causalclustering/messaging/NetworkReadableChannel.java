/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging;

import io.netty.buffer.ByteBuf;
import java.io.IOException;
import org.neo4j.io.fs.ReadPastEndException;
import org.neo4j.kernel.impl.transaction.log.LogPositionMarker;
import org.neo4j.kernel.impl.transaction.log.ReadableClosablePositionAwareChecksumChannel;

public class NetworkReadableChannel implements ReadableClosablePositionAwareChecksumChannel {

  private final ByteBuf delegate;

  public NetworkReadableChannel(ByteBuf input) {
    this.delegate = input;
  }

  public byte get() throws IOException {
    this.ensureBytes(1);
    return this.delegate.readByte();
  }

  public short getShort() throws IOException {
    this.ensureBytes(2);
    return this.delegate.readShort();
  }

  public int getInt() throws IOException {
    this.ensureBytes(4);
    return this.delegate.readInt();
  }

  public long getLong() throws IOException {
    this.ensureBytes(8);
    return this.delegate.readLong();
  }

  public float getFloat() throws IOException {
    this.ensureBytes(4);
    return this.delegate.readFloat();
  }

  public double getDouble() throws IOException {
    this.ensureBytes(8);
    return this.delegate.readDouble();
  }

  public void get(byte[] bytes, int length) throws IOException {
    this.ensureBytes(length);
    this.delegate.readBytes(bytes, 0, length);
  }

  private void ensureBytes(int byteCount) throws ReadPastEndException {
    if (this.delegate.readableBytes() < byteCount) {
      throw ReadPastEndException.INSTANCE;
    }
  }

  public LogPositionMarker getCurrentPosition(LogPositionMarker positionMarker) {
    positionMarker.unspecified();
    return positionMarker;
  }

  public void close() {
  }

  public void beginChecksum() {
  }

  public int endChecksumAndValidate() {
    return 0;
  }
}
