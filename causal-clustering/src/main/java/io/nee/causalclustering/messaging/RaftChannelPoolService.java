/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging;

import io.nee.causalclustering.net.BootstrapConfiguration;
import io.nee.causalclustering.net.ChannelPoolService;
import io.nee.causalclustering.protocol.init.ClientChannelInitializer;
import io.netty.channel.pool.AbstractChannelPoolHandler;
import io.netty.channel.socket.SocketChannel;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public class RaftChannelPoolService extends ChannelPoolService {

  public RaftChannelPoolService(
      BootstrapConfiguration<? extends SocketChannel> bootstrapConfiguration,
      JobScheduler scheduler, LogProvider logProvider,
      ClientChannelInitializer channelInitializer) {
    super(bootstrapConfiguration, scheduler, Group.RAFT_CLIENT,
        new RaftChannelPoolService.PipelineInstaller(
            logProvider.getLog(RaftChannelPoolService.class), channelInitializer),
        OneMultiplexedChannel::new);
  }

  private static class PipelineInstaller extends AbstractChannelPoolHandler {

    private final Log log;
    private final ClientChannelInitializer channelInitializer;

    PipelineInstaller(Log log, ClientChannelInitializer channelInitializer) {
      this.log = log;
      this.channelInitializer = channelInitializer;
    }

    public void channelCreated(io.netty.channel.Channel ch) {
      this.log.info("Channel created [%s]", ch);
      ch.pipeline().addLast(this.channelInitializer);
    }
  }
}
