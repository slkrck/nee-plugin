/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging;

import io.netty.buffer.ByteBuf;
import org.neo4j.io.fs.WritableChecksumChannel;

public class NetworkWritableChannel implements WritableChecksumChannel, ByteBufBacked {

  private final ByteBuf delegate;

  public NetworkWritableChannel(ByteBuf byteBuf) {
    this.delegate = byteBuf;
  }

  public WritableChecksumChannel put(byte value) {
    this.delegate.writeByte(value);
    return this;
  }

  public WritableChecksumChannel putShort(short value) {
    this.delegate.writeShort(value);
    return this;
  }

  public WritableChecksumChannel putInt(int value) {
    this.delegate.writeInt(value);
    return this;
  }

  public WritableChecksumChannel putLong(long value) {
    this.delegate.writeLong(value);
    return this;
  }

  public WritableChecksumChannel putFloat(float value) {
    this.delegate.writeFloat(value);
    return this;
  }

  public WritableChecksumChannel putDouble(double value) {
    this.delegate.writeDouble(value);
    return this;
  }

  public WritableChecksumChannel put(byte[] value, int length) {
    this.delegate.writeBytes(value, 0, length);
    return this;
  }

  public ByteBuf byteBuf() {
    return this.delegate;
  }

  public void beginChecksum() {
  }

  public int putChecksum() {
    return 0;
  }
}
