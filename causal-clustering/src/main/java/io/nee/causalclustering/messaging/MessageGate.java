/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging;

import io.nee.causalclustering.protocol.handshake.GateEvent;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

@Sharable
public class MessageGate extends ChannelDuplexHandler {

  private final Predicate<Object> gated;
  private List<MessageGate.GatedWrite> pending = new ArrayList();

  public MessageGate(Predicate<Object> gated) {
    this.gated = gated;
  }

  public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
    if (evt instanceof GateEvent) {
      if (GateEvent.getSuccess().equals(evt)) {
        Iterator n3 = this.pending.iterator();

        while (n3.hasNext()) {
          MessageGate.GatedWrite write = (MessageGate.GatedWrite) n3.next();
          ctx.write(write.msg, write.promise);
        }

        ctx.channel().pipeline().remove(this);
      }

      this.pending.clear();
      this.pending = null;
    } else {
      super.userEventTriggered(ctx, evt);
    }
  }

  public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) {
    if (!this.gated.test(msg)) {
      ctx.write(msg, promise);
    } else if (this.pending != null) {
      this.pending.add(new MessageGate.GatedWrite(msg, promise));
    } else {
      promise.setFailure(new RuntimeException("Gate failed and has been permanently closed."));
    }
  }

  static class GatedWrite {

    final Object msg;
    final ChannelPromise promise;

    GatedWrite(Object msg, ChannelPromise promise) {
      this.msg = msg;
      this.promise = promise;
    }
  }
}
