/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging;

import io.netty.buffer.ByteBuf;
import org.neo4j.io.ByteUnit;
import org.neo4j.io.fs.WritableChecksumChannel;

public class BoundedNetworkWritableChannel implements WritableChecksumChannel, ByteBufBacked {

  private static final long DEFAULT_SIZE_LIMIT = ByteUnit.mebiBytes(2L);
  private final ByteBuf delegate;
  private final int initialWriterIndex;
  private final long sizeLimit;

  public BoundedNetworkWritableChannel(ByteBuf delegate) {
    this(delegate, DEFAULT_SIZE_LIMIT);
  }

  public BoundedNetworkWritableChannel(ByteBuf delegate, long sizeLimit) {
    this.delegate = delegate;
    this.initialWriterIndex = delegate.writerIndex();
    this.sizeLimit = sizeLimit;
  }

  public WritableChecksumChannel put(byte value) throws MessageTooBigException {
    this.checkSize(1);
    this.delegate.writeByte(value);
    return this;
  }

  public WritableChecksumChannel putShort(short value) throws MessageTooBigException {
    this.checkSize(2);
    this.delegate.writeShort(value);
    return this;
  }

  public WritableChecksumChannel putInt(int value) throws MessageTooBigException {
    this.checkSize(4);
    this.delegate.writeInt(value);
    return this;
  }

  public WritableChecksumChannel putLong(long value) throws MessageTooBigException {
    this.checkSize(8);
    this.delegate.writeLong(value);
    return this;
  }

  public WritableChecksumChannel putFloat(float value) throws MessageTooBigException {
    this.checkSize(4);
    this.delegate.writeFloat(value);
    return this;
  }

  public WritableChecksumChannel putDouble(double value) throws MessageTooBigException {
    this.checkSize(8);
    this.delegate.writeDouble(value);
    return this;
  }

  public WritableChecksumChannel put(byte[] value, int length) throws MessageTooBigException {
    this.checkSize(length);
    this.delegate.writeBytes(value, 0, length);
    return this;
  }

  private void checkSize(int additional) throws MessageTooBigException {
    int writtenSoFar = this.delegate.writerIndex() - this.initialWriterIndex;
    int countToCheck = writtenSoFar + additional;
    if ((long) countToCheck > this.sizeLimit) {
      throw new MessageTooBigException(
          String.format(
              "Size limit exceeded. Limit is %d, wanted to write %d with the writer index at %d (started at %d), written so far %d",
              this.sizeLimit, additional, this.delegate.writerIndex(), this.initialWriterIndex,
              writtenSoFar));
    }
  }

  public ByteBuf byteBuf() {
    return this.delegate;
  }

  public void beginChecksum() {
  }

  public int putChecksum() {
    return 0;
  }
}
