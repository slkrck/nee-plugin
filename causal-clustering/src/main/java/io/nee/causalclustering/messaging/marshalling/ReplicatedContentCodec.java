/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling;

import io.nee.causalclustering.core.consensus.NewLeaderBarrier;
import io.nee.causalclustering.core.consensus.membership.MemberIdSet;
import io.nee.causalclustering.core.consensus.membership.MemberIdSetSerializer;
import io.nee.causalclustering.core.replication.DistributedOperation;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.core.state.machines.dummy.DummyRequest;
import io.nee.causalclustering.core.state.machines.lease.ReplicatedLeaseMarshalV2;
import io.nee.causalclustering.core.state.machines.lease.ReplicatedLeaseRequest;
import io.nee.causalclustering.core.state.machines.token.ReplicatedTokenRequest;
import io.nee.causalclustering.core.state.machines.token.ReplicatedTokenRequestMarshalV2;
import io.nee.causalclustering.core.state.machines.tx.ByteArrayReplicatedTransaction;
import io.nee.causalclustering.core.state.machines.tx.ReplicatedTransaction;
import io.nee.causalclustering.core.state.machines.tx.TransactionRepresentationReplicatedTransaction;
import io.nee.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.NetworkReadableChannel;
import io.netty.buffer.ByteBuf;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import org.neo4j.kernel.database.DatabaseId;

public class ReplicatedContentCodec implements Codec<ReplicatedContent> {

  private static ReplicatedTransaction decodeTx(ByteBuf byteBuf)
      throws IOException, EndOfStreamException {
    DatabaseId databaseId = DatabaseIdWithoutNameMarshal.INSTANCE
        .unmarshal(new NetworkReadableChannel(byteBuf));
    int length = byteBuf.readableBytes();
    byte[] bytes = new byte[length];
    byteBuf.readBytes(bytes);
    return ReplicatedTransaction.from(bytes, databaseId);
  }

  public void encode(ReplicatedContent type, List<Object> output) throws IOException {
    type.dispatch(new ReplicatedContentCodec.ChunkingHandler(output));
  }

  public ContentBuilder<ReplicatedContent> decode(ByteBuf byteBuf)
      throws IOException, EndOfStreamException {
    byte contentType = byteBuf.readByte();
    return this.unmarshal(contentType, byteBuf);
  }

  private ContentBuilder<ReplicatedContent> unmarshal(byte contentType, ByteBuf buffer)
      throws IOException, EndOfStreamException {
    switch (contentType) {
      case 0:
        return ContentBuilder.finished(decodeTx(buffer));
      case 8:
        return ContentBuilder.finished(DummyRequest.decode(buffer));
      default:
        return CoreReplicatedContentMarshalV2
            .unmarshal(contentType, new NetworkReadableChannel(buffer));
    }
  }

  private static class ChunkingHandler implements ReplicatedContentHandler {

    private final List<Object> output;

    ChunkingHandler(List<Object> output) {
      this.output = output;
    }

    public void handle(ByteArrayReplicatedTransaction replicatedTransaction) {
      this.output.add(ChunkedReplicatedContent
          .chunked((byte) 0, new MaxTotalSize(replicatedTransaction.encode())));
    }

    public void handle(TransactionRepresentationReplicatedTransaction replicatedTransaction) {
      this.output.add(ChunkedReplicatedContent
          .chunked((byte) 0, new MaxTotalSize(replicatedTransaction.encode())));
    }

    public void handle(MemberIdSet memberIdSet) {
      this.output.add(ChunkedReplicatedContent.single((byte) 1, (channel) ->
      {
        MemberIdSetSerializer.marshal(memberIdSet, channel);
      }));
    }

    public void handle(ReplicatedTokenRequest replicatedTokenRequest) {
      this.output.add(ChunkedReplicatedContent.single((byte) 4, (channel) ->
      {
        ReplicatedTokenRequestMarshalV2.marshal(replicatedTokenRequest, channel);
      }));
    }

    public void handle(NewLeaderBarrier newLeaderBarrier) {
      this.output.add(ChunkedReplicatedContent.single((byte) 5, (channel) ->
      {
      }));
    }

    public void handle(ReplicatedLeaseRequest replicatedLeaseRequest) {
      this.output.add(ChunkedReplicatedContent.single((byte) 6, (channel) ->
      {
        ReplicatedLeaseMarshalV2.marshal(replicatedLeaseRequest, channel);
      }));
    }

    public void handle(DistributedOperation distributedOperation) {
      List n10000 = this.output;
      Objects.requireNonNull(distributedOperation);
      n10000.add(ChunkedReplicatedContent.single((byte) 7, distributedOperation::marshalMetaData));
    }

    public void handle(DummyRequest dummyRequest) {
      this.output.add(ChunkedReplicatedContent.chunked((byte) 8, dummyRequest.encoder()));
    }
  }
}
