/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.neo4j.io.fs.WritableChannel;

public class OutputStreamWritableChannel implements WritableChannel {

  private final DataOutputStream dataOutputStream;

  public OutputStreamWritableChannel(OutputStream outputStream) {
    this.dataOutputStream = new DataOutputStream(outputStream);
  }

  public WritableChannel put(byte value) throws IOException {
    this.dataOutputStream.writeByte(value);
    return this;
  }

  public WritableChannel putShort(short value) throws IOException {
    this.dataOutputStream.writeShort(value);
    return this;
  }

  public WritableChannel putInt(int value) throws IOException {
    this.dataOutputStream.writeInt(value);
    return this;
  }

  public WritableChannel putLong(long value) throws IOException {
    this.dataOutputStream.writeLong(value);
    return this;
  }

  public WritableChannel putFloat(float value) throws IOException {
    this.dataOutputStream.writeFloat(value);
    return this;
  }

  public WritableChannel putDouble(double value) throws IOException {
    this.dataOutputStream.writeDouble(value);
    return this;
  }

  public WritableChannel put(byte[] value, int length) throws IOException {
    this.dataOutputStream.write(value, 0, length);
    return this;
  }
}
