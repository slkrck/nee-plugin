/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;
import java.util.Objects;
import org.neo4j.util.Preconditions;

public class ByteArrayChunkedEncoder implements ChunkedInput<ByteBuf> {

  private static final int DEFAULT_CHUNK_SIZE = 32768;
  private final byte[] content;
  private final int chunkSize;
  private int pos;
  private boolean hasRead;

  ByteArrayChunkedEncoder(byte[] content, int chunkSize) {
    Objects.requireNonNull(content, "content cannot be null");
    Preconditions.requireNonNegative(content.length);
    Preconditions.requirePositive(chunkSize);
    this.content = content;
    this.chunkSize = chunkSize;
  }

  public ByteArrayChunkedEncoder(byte[] content) {
    this(content, 32768);
  }

  private int available() {
    return this.content.length - this.pos;
  }

  public boolean isEndOfInput() {
    return this.pos == this.content.length && this.hasRead;
  }

  public void close() {
    this.pos = this.content.length;
  }

  public ByteBuf readChunk(ChannelHandlerContext ctx) {
    return this.readChunk(ctx.alloc());
  }

  public ByteBuf readChunk(ByteBufAllocator allocator) {
    this.hasRead = true;
    if (this.isEndOfInput()) {
      return null;
    } else {
      int toWrite = Math.min(this.available(), this.chunkSize);
      ByteBuf buffer = allocator.buffer(toWrite);

      try {
        buffer.writeBytes(this.content, this.pos, toWrite);
        this.pos += toWrite;
        return buffer;
      } catch (Throwable n5) {
        buffer.release();
        throw n5;
      }
    }
  }

  public long length() {
    return this.content.length;
  }

  public long progress() {
    return this.pos;
  }
}
