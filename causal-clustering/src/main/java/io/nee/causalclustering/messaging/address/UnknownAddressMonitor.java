/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.address;

import io.nee.causalclustering.identity.MemberId;
import java.time.Clock;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.CappedLogger;

public class UnknownAddressMonitor {

  private final Log log;
  private final Clock clock;
  private final long timeLimitMs;
  private final Map<MemberId, CappedLogger> loggers = new ConcurrentHashMap();

  public UnknownAddressMonitor(Log log, Clock clock, long timeLimitMs) {
    this.log = log;
    this.clock = clock;
    this.timeLimitMs = timeLimitMs;
  }

  public void logAttemptToSendToMemberWithNoKnownAddress(MemberId to) {
    CappedLogger cappedLogger = this.loggers.get(to);
    if (cappedLogger == null) {
      cappedLogger = new CappedLogger(this.log);
      cappedLogger.setTimeLimit(this.timeLimitMs, TimeUnit.MILLISECONDS, this.clock);
      this.loggers.put(to, cappedLogger);
    }

    cappedLogger
        .info("No address found for %s, probably because the member has been shut down.", to);
  }
}
