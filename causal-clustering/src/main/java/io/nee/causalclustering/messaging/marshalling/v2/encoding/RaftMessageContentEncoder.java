/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling.v2.encoding;

import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.messaging.marshalling.Codec;
import io.nee.causalclustering.messaging.marshalling.v2.ContentType;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import java.io.IOException;
import java.util.List;

public class RaftMessageContentEncoder extends
    MessageToMessageEncoder<RaftMessages.RaftIdAwareMessage> {

  private final Codec<ReplicatedContent> codec;

  public RaftMessageContentEncoder(Codec<ReplicatedContent> replicatedContentCodec) {
    this.codec = replicatedContentCodec;
  }

  protected void encode(ChannelHandlerContext ctx, RaftMessages.RaftIdAwareMessage msg,
      List<Object> out) throws Exception {
    out.add(msg);
    RaftMessageContentEncoder.Handler replicatedContentHandler = new RaftMessageContentEncoder.Handler(
        out, ctx.alloc());
    msg.message().dispatch(replicatedContentHandler);
  }

  private class Handler implements RaftMessages.Handler<Void, Exception> {

    private final List<Object> out;
    private final ByteBufAllocator alloc;

    private Handler(List<Object> out, ByteBufAllocator alloc) {
      this.out = out;
      this.alloc = alloc;
    }

    public Void handle(RaftMessages.Vote.Request request) throws Exception {
      return null;
    }

    public Void handle(RaftMessages.Vote.Response response) throws Exception {
      return null;
    }

    public Void handle(RaftMessages.PreVote.Request request) throws Exception {
      return null;
    }

    public Void handle(RaftMessages.PreVote.Response response) throws Exception {
      return null;
    }

    public Void handle(RaftMessages.AppendEntries.Request request) throws Exception {
      this.out.add(RaftLogEntryTermsSerializer.serializeTerms(request.entries(), this.alloc));
      RaftLogEntry[] n2 = request.entries();
      int n3 = n2.length;

      for (int n4 = 0; n4 < n3; ++n4) {
        RaftLogEntry entry = n2[n4];
        this.serializableContents(entry.content(), this.out);
      }

      return null;
    }

    public Void handle(RaftMessages.AppendEntries.Response response) throws Exception {
      return null;
    }

    public Void handle(RaftMessages.Heartbeat heartbeat) throws Exception {
      return null;
    }

    public Void handle(RaftMessages.LogCompactionInfo logCompactionInfo) throws Exception {
      return null;
    }

    public Void handle(RaftMessages.HeartbeatResponse heartbeatResponse) throws Exception {
      return null;
    }

    public Void handle(RaftMessages.NewEntry.Request request) throws Exception {
      this.serializableContents(request.content(), this.out);
      return null;
    }

    public Void handle(RaftMessages.Timeout.Election election) throws Exception {
      return this.illegalOutbound(election);
    }

    public Void handle(RaftMessages.Timeout.Heartbeat heartbeat) throws Exception {
      return this.illegalOutbound(heartbeat);
    }

    public Void handle(RaftMessages.NewEntry.BatchRequest batchRequest) throws Exception {
      return this.illegalOutbound(batchRequest);
    }

    public Void handle(RaftMessages.PruneRequest pruneRequest) throws Exception {
      return this.illegalOutbound(pruneRequest);
    }

    private Void illegalOutbound(RaftMessages.BaseRaftMessage raftMessage) {
      throw new IllegalStateException("Illegal outbound call: " + raftMessage.getClass());
    }

    private void serializableContents(ReplicatedContent content, List<Object> out)
        throws IOException {
      out.add(ContentType.ReplicatedContent);
      RaftMessageContentEncoder.this.codec.encode(content, out);
    }
  }
}
