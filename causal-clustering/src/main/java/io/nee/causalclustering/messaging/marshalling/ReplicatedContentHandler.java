/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling;

import io.nee.causalclustering.core.consensus.NewLeaderBarrier;
import io.nee.causalclustering.core.consensus.membership.MemberIdSet;
import io.nee.causalclustering.core.replication.DistributedOperation;
import io.nee.causalclustering.core.state.machines.dummy.DummyRequest;
import io.nee.causalclustering.core.state.machines.lease.ReplicatedLeaseRequest;
import io.nee.causalclustering.core.state.machines.token.ReplicatedTokenRequest;
import io.nee.causalclustering.core.state.machines.tx.ByteArrayReplicatedTransaction;
import io.nee.causalclustering.core.state.machines.tx.TransactionRepresentationReplicatedTransaction;
import java.io.IOException;

public interface ReplicatedContentHandler {

  void handle(ByteArrayReplicatedTransaction n1) throws IOException;

  void handle(TransactionRepresentationReplicatedTransaction n1) throws IOException;

  void handle(MemberIdSet n1) throws IOException;

  void handle(ReplicatedTokenRequest n1) throws IOException;

  void handle(NewLeaderBarrier n1) throws IOException;

  void handle(ReplicatedLeaseRequest n1) throws IOException;

  void handle(DistributedOperation n1) throws IOException;

  void handle(DummyRequest n1) throws IOException;
}
