/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling.v1;

import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.BoundedNetworkWritableChannel;
import io.nee.causalclustering.messaging.NetworkWritableChannel;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.neo4j.io.ByteUnit;

public class RaftMessageEncoder extends MessageToByteEncoder<RaftMessages.RaftIdAwareMessage> {

  private final ChannelMarshal<ReplicatedContent> marshal;

  public RaftMessageEncoder(ChannelMarshal<ReplicatedContent> marshal) {
    this.marshal = marshal;
  }

  public synchronized void encode(ChannelHandlerContext ctx,
      RaftMessages.RaftIdAwareMessage decoratedMessage, ByteBuf out) throws Exception {
    RaftMessages.RaftMessage message = decoratedMessage.message();
    RaftId raftId = decoratedMessage.raftId();
    MemberId.Marshal memberMarshal = new MemberId.Marshal();
    NetworkWritableChannel channel = new NetworkWritableChannel(out);
    RaftId.Marshal.INSTANCE.marshal(raftId, channel);
    channel.putInt(message.type().ordinal());
    memberMarshal.marshal(message.from(), channel);
    message.dispatch(new RaftMessageEncoder.Handler(this.marshal, memberMarshal, channel));
  }

  private static class Handler implements RaftMessages.Handler<Void, Exception> {

    private final ChannelMarshal<ReplicatedContent> marshal;
    private final MemberId.Marshal memberMarshal;
    private final NetworkWritableChannel channel;

    Handler(ChannelMarshal<ReplicatedContent> marshal, MemberId.Marshal memberMarshal,
        NetworkWritableChannel channel) {
      this.marshal = marshal;
      this.memberMarshal = memberMarshal;
      this.channel = channel;
    }

    public Void handle(RaftMessages.Vote.Request voteRequest) throws Exception {
      this.memberMarshal.marshal(voteRequest.candidate(), this.channel);
      this.channel.putLong(voteRequest.term());
      this.channel.putLong(voteRequest.lastLogIndex());
      this.channel.putLong(voteRequest.lastLogTerm());
      return null;
    }

    public Void handle(RaftMessages.Vote.Response voteResponse) {
      this.channel.putLong(voteResponse.term());
      this.channel.put((byte) (voteResponse.voteGranted() ? 1 : 0));
      return null;
    }

    public Void handle(RaftMessages.PreVote.Request preVoteRequest) throws Exception {
      this.memberMarshal.marshal(preVoteRequest.candidate(), this.channel);
      this.channel.putLong(preVoteRequest.term());
      this.channel.putLong(preVoteRequest.lastLogIndex());
      this.channel.putLong(preVoteRequest.lastLogTerm());
      return null;
    }

    public Void handle(RaftMessages.PreVote.Response preVoteResponse) {
      this.channel.putLong(preVoteResponse.term());
      this.channel.put((byte) (preVoteResponse.voteGranted() ? 1 : 0));
      return null;
    }

    public Void handle(RaftMessages.AppendEntries.Request appendRequest) throws Exception {
      this.channel.putLong(appendRequest.leaderTerm());
      this.channel.putLong(appendRequest.prevLogIndex());
      this.channel.putLong(appendRequest.prevLogTerm());
      this.channel.putLong(appendRequest.leaderCommit());
      this.channel.putLong(appendRequest.entries().length);
      RaftLogEntry[] n2 = appendRequest.entries();
      int n3 = n2.length;

      for (int n4 = 0; n4 < n3; ++n4) {
        RaftLogEntry raftLogEntry = n2[n4];
        this.channel.putLong(raftLogEntry.term());
        this.marshal.marshal(raftLogEntry.content(), this.channel);
      }

      return null;
    }

    public Void handle(RaftMessages.AppendEntries.Response appendResponse) {
      this.channel.putLong(appendResponse.term());
      this.channel.put((byte) (appendResponse.success() ? 1 : 0));
      this.channel.putLong(appendResponse.matchIndex());
      this.channel.putLong(appendResponse.appendIndex());
      return null;
    }

    public Void handle(RaftMessages.NewEntry.Request newEntryRequest) throws Exception {
      BoundedNetworkWritableChannel sizeBoundChannel = new BoundedNetworkWritableChannel(
          this.channel.byteBuf(), ByteUnit.gibiBytes(1L));
      this.marshal.marshal(newEntryRequest.content(), sizeBoundChannel);
      return null;
    }

    public Void handle(RaftMessages.Heartbeat heartbeat) {
      this.channel.putLong(heartbeat.leaderTerm());
      this.channel.putLong(heartbeat.commitIndexTerm());
      this.channel.putLong(heartbeat.commitIndex());
      return null;
    }

    public Void handle(RaftMessages.HeartbeatResponse heartbeatResponse) {
      return null;
    }

    public Void handle(RaftMessages.LogCompactionInfo logCompactionInfo) {
      this.channel.putLong(logCompactionInfo.leaderTerm());
      this.channel.putLong(logCompactionInfo.prevIndex());
      return null;
    }

    public Void handle(RaftMessages.Timeout.Election election) {
      return null;
    }

    public Void handle(RaftMessages.Timeout.Heartbeat heartbeat) {
      return null;
    }

    public Void handle(RaftMessages.NewEntry.BatchRequest batchRequest) {
      return null;
    }

    public Void handle(RaftMessages.PruneRequest pruneRequest) {
      return null;
    }
  }
}
