/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling.v2.decoding;

import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.messaging.marshalling.Codec;
import io.nee.causalclustering.messaging.marshalling.ReplicatedContentCodec;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import java.util.List;

public class ReplicatedContentChunkDecoder extends ByteToMessageDecoder {

  private final Codec<ReplicatedContent> codec;
  private boolean isLast;

  ReplicatedContentChunkDecoder() {
    this.setCumulator(new ReplicatedContentChunkDecoder.ContentChunkCumulator());
    this.codec = new ReplicatedContentCodec();
  }

  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
    if (this.isLast) {
      out.add(this.codec.decode(in));
      this.isLast = false;
    }
  }

  private class ContentChunkCumulator implements Cumulator {

    public ByteBuf cumulate(ByteBufAllocator alloc, ByteBuf cumulation, ByteBuf in) {
      ReplicatedContentChunkDecoder.this.isLast = in.readBoolean();
      return ByteToMessageDecoder.COMPOSITE_CUMULATOR.cumulate(alloc, cumulation, in);
    }
  }
}
