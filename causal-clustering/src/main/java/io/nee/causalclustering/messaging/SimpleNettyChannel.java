/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging;

import java.util.concurrent.Future;
import org.neo4j.logging.Log;

public class SimpleNettyChannel implements Channel {

  private final Log log;
  private final io.netty.channel.Channel channel;
  private volatile boolean disposed;

  public SimpleNettyChannel(io.netty.channel.Channel channel, Log log) {
    this.channel = channel;
    this.log = log;
  }

  public boolean isDisposed() {
    return this.disposed;
  }

  public synchronized void dispose() {
    this.log.info("Disposing channel: " + this.channel);
    this.disposed = true;
    this.channel.close();
  }

  public boolean isOpen() {
    return this.channel.isOpen();
  }

  public Future<Void> write(Object msg) {
    this.checkDisposed();
    return this.channel.write(msg);
  }

  public Future<Void> writeAndFlush(Object msg) {
    this.checkDisposed();
    return this.channel.writeAndFlush(msg);
  }

  private void checkDisposed() {
    if (this.disposed) {
      throw new IllegalStateException("sending on disposed channel");
    }
  }
}
