/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling;

import io.netty.buffer.ByteBuf;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class StringMarshal {

  private static final int NULL_STRING_LENGTH = -1;

  private StringMarshal() {
  }

  public static void marshal(ByteBuf buffer, String string) {
    if (string == null) {
      buffer.writeInt(-1);
    } else {
      byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
      buffer.writeInt(bytes.length);
      buffer.writeBytes(bytes);
    }
  }

  public static void marshal(ByteBuffer buffer, String string) {
    if (string == null) {
      buffer.putInt(-1);
    } else {
      byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
      buffer.putInt(bytes.length);
      buffer.put(bytes);
    }
  }

  public static String unmarshal(ByteBuf buffer) {
    int len = buffer.readInt();
    if (len == -1) {
      return null;
    } else {
      byte[] bytes = new byte[len];
      buffer.readBytes(bytes);
      return new String(bytes, StandardCharsets.UTF_8);
    }
  }

  public static void marshal(WritableChannel channel, String string) throws IOException {
    if (string == null) {
      channel.putInt(-1);
    } else {
      byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
      channel.putInt(bytes.length);
      channel.put(bytes, bytes.length);
    }
  }

  public static String unmarshal(ReadableChannel channel) throws IOException {
    int len = channel.getInt();
    if (len == -1) {
      return null;
    } else {
      byte[] stringBytes = new byte[len];
      channel.get(stringBytes, stringBytes.length);
      return new String(stringBytes, StandardCharsets.UTF_8);
    }
  }
}
