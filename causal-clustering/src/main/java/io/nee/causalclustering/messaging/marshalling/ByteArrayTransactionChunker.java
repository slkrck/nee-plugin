/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling;

import io.nee.causalclustering.core.state.machines.tx.ByteArrayReplicatedTransaction;
import io.nee.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameMarshal;
import io.nee.causalclustering.messaging.NetworkWritableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;
import org.neo4j.kernel.database.DatabaseId;

public class ByteArrayTransactionChunker implements ChunkedInput<ByteBuf> {

  private final DatabaseId databaseId;
  private final ByteArrayChunkedEncoder byteChunker;
  private boolean dbNameWritten;

  public ByteArrayTransactionChunker(ByteArrayReplicatedTransaction tx) {
    this.byteChunker = new ByteArrayChunkedEncoder(tx.getTxBytes());
    this.databaseId = tx.databaseId();
  }

  public boolean isEndOfInput() {
    return this.byteChunker.isEndOfInput();
  }

  public void close() {
    this.byteChunker.close();
  }

  public ByteBuf readChunk(ChannelHandlerContext ctx) {
    return this.readChunk(ctx.alloc());
  }

  public ByteBuf readChunk(ByteBufAllocator allocator) {
    if (this.isEndOfInput()) {
      return null;
    } else if (!this.dbNameWritten) {
      ByteBuf buffer = allocator.buffer();

      try {
        DatabaseIdWithoutNameMarshal.INSTANCE
            .marshal(this.databaseId, new NetworkWritableChannel(buffer));
        this.dbNameWritten = true;
        return buffer;
      } catch (Throwable n4) {
        buffer.release();
        throw new RuntimeException(n4);
      }
    } else {
      return this.byteChunker.readChunk(allocator);
    }
  }

  public long length() {
    return this.byteChunker.length();
  }

  public long progress() {
    return this.byteChunker.progress();
  }
}
