/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling.v2.decoding;

import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import java.time.Clock;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;

public class RaftMessageComposer extends MessageToMessageDecoder<Object> {

  private final Queue<ReplicatedContent> replicatedContents = new LinkedList();
  private final Queue<Long> raftLogEntryTerms = new LinkedList();
  private final Clock clock;
  private RaftMessageDecoder.RaftIdAwareMessageComposer messageComposer;

  public RaftMessageComposer(Clock clock) {
    this.clock = clock;
  }

  protected void decode(ChannelHandlerContext ctx, Object msg, List<Object> out) {
    if (msg instanceof ReplicatedContent) {
      this.replicatedContents.add((ReplicatedContent) msg);
    } else if (msg instanceof RaftLogEntryTermsDecoder.RaftLogEntryTerms) {
      long[] n4 = ((RaftLogEntryTermsDecoder.RaftLogEntryTerms) msg).terms();
      int n5 = n4.length;

      for (int n6 = 0; n6 < n5; ++n6) {
        long term = n4[n6];
        this.raftLogEntryTerms.add(term);
      }
    } else {
      if (!(msg instanceof RaftMessageDecoder.RaftIdAwareMessageComposer)) {
        throw new IllegalStateException("Unexpected object in the pipeline: " + msg);
      }

      if (this.messageComposer != null) {
        throw new IllegalStateException(
            "Received raft message header. Pipeline already contains message header waiting to build.");
      }

      this.messageComposer = (RaftMessageDecoder.RaftIdAwareMessageComposer) msg;
    }

    if (this.messageComposer != null) {
      Optional<RaftMessages.RaftIdAwareMessage> raftIdAwareMessage =
          this.messageComposer
              .maybeCompose(this.clock, this.raftLogEntryTerms, this.replicatedContents);
      raftIdAwareMessage.ifPresent((message) ->
      {
        this.clear(message);
        out.add(message);
      });
    }
  }

  private void clear(RaftMessages.RaftIdAwareMessage message) {
    this.messageComposer = null;
    if (!this.replicatedContents.isEmpty() || !this.raftLogEntryTerms.isEmpty()) {
      throw new IllegalStateException(String.format(
          "Message [%s] was composed without using all resources in the pipeline. Pipeline still contains Replicated contents[%s] and RaftLogEntryTerms [%s]",
          message, this.stringify(this.replicatedContents),
          this.stringify(this.raftLogEntryTerms)));
    }
  }

  private String stringify(Iterable<?> objects) {
    StringBuilder stringBuilder = new StringBuilder();
    Iterator iterator = objects.iterator();

    while (iterator.hasNext()) {
      stringBuilder.append(iterator.next());
      if (iterator.hasNext()) {
        stringBuilder.append(", ");
      }
    }

    return stringBuilder.toString();
  }
}
