/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling.storeid;

import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.netty.handler.codec.DecoderException;
import java.io.IOException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.storageengine.api.StoreId;

public final class StoreIdMarshal extends SafeChannelMarshal<StoreId> {

  public static final StoreIdMarshal INSTANCE = new StoreIdMarshal();

  private StoreIdMarshal() {
  }

  public void marshal(StoreId storeId, WritableChannel channel) throws IOException {
    if (storeId == null) {
      channel.put((byte) 0);
    } else {
      channel.put((byte) 1);
      channel.putLong(storeId.getCreationTime());
      channel.putLong(storeId.getRandomId());
      channel.putLong(storeId.getStoreVersion());
      channel.putLong(storeId.getUpgradeTime());
      channel.putLong(storeId.getUpgradeTxId());
    }
  }

  protected StoreId unmarshal0(ReadableChannel channel) throws IOException {
    byte exists = channel.get();
    if (exists == 0) {
      return null;
    } else if (exists != 1) {
      throw new DecoderException("Unexpected value: " + exists);
    } else {
      long creationTime = channel.getLong();
      long randomId = channel.getLong();
      long storeVersion = channel.getLong();
      long upgradeTime = channel.getLong();
      long upgradeId = channel.getLong();
      return new StoreId(creationTime, randomId, storeVersion, upgradeTime, upgradeId);
    }
  }
}
