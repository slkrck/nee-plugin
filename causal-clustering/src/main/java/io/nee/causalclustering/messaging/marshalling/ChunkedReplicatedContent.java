/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling;

import io.nee.causalclustering.messaging.BoundedNetworkWritableChannel;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.CompositeByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;
import java.io.IOException;
import org.neo4j.function.ThrowingConsumer;
import org.neo4j.io.fs.WritableChannel;

public class ChunkedReplicatedContent implements ChunkedInput<ByteBuf> {

  private static final int METADATA_SIZE = 1;
  private final byte contentType;
  private final ChunkedInput<ByteBuf> byteBufAwareMarshal;
  private boolean endOfInput;
  private int progress;

  private ChunkedReplicatedContent(byte contentType, ChunkedInput<ByteBuf> byteBufAwareMarshal) {
    this.byteBufAwareMarshal = byteBufAwareMarshal;
    this.contentType = contentType;
  }

  static ChunkedInput<ByteBuf> single(byte contentType,
      ThrowingConsumer<WritableChannel, IOException> marshaller) {
    return chunked(contentType, new ChunkedReplicatedContent.Single(marshaller));
  }

  static ChunkedInput<ByteBuf> chunked(byte contentType, ChunkedInput<ByteBuf> chunkedInput) {
    return new ChunkedReplicatedContent(contentType, chunkedInput);
  }

  private static int metadataSize(boolean isFirstChunk) {
    return 1 + (isFirstChunk ? 1 : 0);
  }

  private static ByteBuf writeMetadata(boolean isFirstChunk, boolean isLastChunk, byte contentType,
      ByteBuf buffer) {
    buffer.writeBoolean(isLastChunk);
    if (isFirstChunk) {
      buffer.writeByte(contentType);
    }

    return buffer;
  }

  public boolean isEndOfInput() {
    return this.endOfInput;
  }

  public void close() {
  }

  public ByteBuf readChunk(ChannelHandlerContext ctx) throws Exception {
    return this.readChunk(ctx.alloc());
  }

  public ByteBuf readChunk(ByteBufAllocator allocator) throws Exception {
    if (this.endOfInput) {
      return null;
    } else {
      ByteBuf data = this.byteBufAwareMarshal.readChunk(allocator);
      if (data == null) {
        return null;
      } else {
        this.endOfInput = this.byteBufAwareMarshal.isEndOfInput();
        CompositeByteBuf allData = new CompositeByteBuf(allocator, false, 2);
        allData.addComponent(true, data);

        try {
          boolean isFirstChunk = this.progress() == 0L;
          int metaDataCapacity = metadataSize(isFirstChunk);
          ByteBuf metaDataBuffer = allocator.buffer(metaDataCapacity, metaDataCapacity);
          allData.addComponent(true, 0,
              writeMetadata(isFirstChunk, this.byteBufAwareMarshal.isEndOfInput(), this.contentType,
                  metaDataBuffer));
          this.progress += allData.readableBytes();

          assert this.progress > 0;

          return allData;
        } catch (Throwable n7) {
          allData.release();
          throw n7;
        }
      }
    }
  }

  public long length() {
    return -1L;
  }

  public long progress() {
    return this.progress;
  }

  private static class Single implements ChunkedInput<ByteBuf> {

    private final ThrowingConsumer<WritableChannel, IOException> marshaller;
    boolean isEndOfInput;
    int offset;

    private Single(ThrowingConsumer<WritableChannel, IOException> marshaller) {
      this.marshaller = marshaller;
    }

    public boolean isEndOfInput() {
      return this.isEndOfInput;
    }

    public void close() {
      this.isEndOfInput = true;
    }

    public ByteBuf readChunk(ChannelHandlerContext ctx) throws Exception {
      return this.readChunk(ctx.alloc());
    }

    public ByteBuf readChunk(ByteBufAllocator allocator) throws Exception {
      if (this.isEndOfInput) {
        return null;
      } else {
        ByteBuf buffer = allocator.buffer();
        this.marshaller.accept(new BoundedNetworkWritableChannel(buffer));
        this.isEndOfInput = true;
        this.offset = buffer.readableBytes();
        return buffer;
      }
    }

    public long length() {
      return -1L;
    }

    public long progress() {
      return this.offset;
    }
  }
}
