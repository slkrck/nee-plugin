/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging;

import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.discovery.CoreTopologyService;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.address.UnknownAddressMonitor;
import java.time.Clock;
import java.util.Optional;
import java.util.function.Supplier;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.time.Clocks;

public class RaftOutbound implements Outbound<MemberId, RaftMessages.RaftMessage> {

  private final CoreTopologyService coreTopologyService;
  private final Outbound<SocketAddress, Message> outbound;
  private final Supplier<Optional<RaftId>> boundRaftId;
  private final UnknownAddressMonitor unknownAddressMonitor;
  private final Log log;
  private final MemberId myself;
  private final Clock clock;
  private final Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> localMessageHandler;

  public RaftOutbound(CoreTopologyService coreTopologyService,
      Outbound<SocketAddress, Message> outbound,
      Inbound.MessageHandler<RaftMessages.ReceivedInstantRaftIdAwareMessage<?>> localMessageHandler,
      Supplier<Optional<RaftId>> boundRaftId,
      LogProvider logProvider, long logThresholdMillis, MemberId myself, Clock clock) {
    this.coreTopologyService = coreTopologyService;
    this.outbound = outbound;
    this.boundRaftId = boundRaftId;
    this.log = logProvider.getLog(this.getClass());
    this.unknownAddressMonitor = new UnknownAddressMonitor(this.log, Clocks.systemClock(),
        logThresholdMillis);
    this.myself = myself;
    this.clock = clock;
    this.localMessageHandler = localMessageHandler;
  }

  public void send(MemberId to, RaftMessages.RaftMessage message, boolean block) {
    Optional<RaftId> raftId = this.boundRaftId.get();
    if (raftId.isEmpty()) {
      this.log.warn("Attempting to send a message before bound to a cluster");
    } else {
      if (to.equals(this.myself)) {
        this.localMessageHandler.handle(RaftMessages.ReceivedInstantRaftIdAwareMessage
            .of(this.clock.instant(), raftId.get(), message));
      } else {
        CoreServerInfo targetCoreInfo = this.coreTopologyService.allCoreServers().get(to);
        if (targetCoreInfo != null) {
          this.outbound.send(targetCoreInfo.getRaftServer(),
              RaftMessages.RaftIdAwareMessage.of(raftId.get(), message), block);
        } else {
          this.unknownAddressMonitor.logAttemptToSendToMemberWithNoKnownAddress(to);
        }
      }
    }
  }
}
