/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling.v2.decoding;

import io.nee.causalclustering.catchup.Protocol;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.messaging.marshalling.ContentBuilder;
import io.nee.causalclustering.messaging.marshalling.v2.ContentType;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import java.util.List;

public class ReplicatedContentDecoder extends
    MessageToMessageDecoder<ContentBuilder<ReplicatedContent>> {

  private final Protocol<ContentType> protocol;
  private ContentBuilder<ReplicatedContent> contentBuilder = ContentBuilder.emptyUnfinished();

  public ReplicatedContentDecoder(Protocol<ContentType> protocol) {
    this.protocol = protocol;
  }

  protected void decode(ChannelHandlerContext ctx, ContentBuilder<ReplicatedContent> msg,
      List<Object> out) {
    this.contentBuilder.combine(msg);
    if (this.contentBuilder.isComplete()) {
      out.add(this.contentBuilder.build());
      this.contentBuilder = ContentBuilder.emptyUnfinished();
      this.protocol.expect(ContentType.ContentType);
    }
  }
}
