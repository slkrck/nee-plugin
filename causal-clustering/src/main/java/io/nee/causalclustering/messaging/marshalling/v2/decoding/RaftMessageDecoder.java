/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.messaging.marshalling.v2.decoding;

import io.nee.causalclustering.catchup.Protocol;
import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.core.consensus.log.RaftLogEntry;
import io.nee.causalclustering.core.replication.ReplicatedContent;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.NetworkReadableChannel;
import io.nee.causalclustering.messaging.marshalling.v2.ContentType;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import java.io.IOException;
import java.time.Clock;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import org.neo4j.io.fs.ReadableChannel;

public class RaftMessageDecoder extends ByteToMessageDecoder {

  private final Protocol<ContentType> protocol;

  RaftMessageDecoder(Protocol<ContentType> protocol) {
    this.protocol = protocol;
  }

  public void decode(ChannelHandlerContext ctx, ByteBuf buffer, List<Object> list)
      throws Exception {
    ReadableChannel channel = new NetworkReadableChannel(buffer);
    RaftId raftId = RaftId.Marshal.INSTANCE.unmarshal(channel);
    int messageTypeWire = channel.getInt();
    RaftMessages.Type[] values = RaftMessages.Type.values();
    RaftMessages.Type messageType = values[messageTypeWire];
    MemberId from = this.retrieveMember(channel);
    Object composer;
    MemberId candidate;
    long term;
    long matchIndex;
    long appendIndex;
    if (messageType.equals(RaftMessages.Type.VOTE_REQUEST)) {
      candidate = this.retrieveMember(channel);
      term = channel.getLong();
      matchIndex = channel.getLong();
      appendIndex = channel.getLong();
      composer = new RaftMessageDecoder.SimpleMessageComposer(
          new RaftMessages.Vote.Request(from, term, candidate, matchIndex, appendIndex));
    } else {
      boolean success;
      long leaderTerm;
      if (messageType.equals(RaftMessages.Type.VOTE_RESPONSE)) {
        leaderTerm = channel.getLong();
        success = channel.get() == 1;
        composer = new RaftMessageDecoder.SimpleMessageComposer(
            new RaftMessages.Vote.Response(from, leaderTerm, success));
      } else if (messageType.equals(RaftMessages.Type.PRE_VOTE_REQUEST)) {
        candidate = this.retrieveMember(channel);
        term = channel.getLong();
        matchIndex = channel.getLong();
        appendIndex = channel.getLong();
        composer = new RaftMessageDecoder.SimpleMessageComposer(
            new RaftMessages.PreVote.Request(from, term, candidate, matchIndex, appendIndex));
      } else if (messageType.equals(RaftMessages.Type.PRE_VOTE_RESPONSE)) {
        leaderTerm = channel.getLong();
        success = channel.get() == 1;
        composer = new RaftMessageDecoder.SimpleMessageComposer(
            new RaftMessages.PreVote.Response(from, leaderTerm, success));
      } else {
        long commitIndex;
        long prevIndex;
        if (messageType.equals(RaftMessages.Type.APPEND_ENTRIES_REQUEST)) {
          leaderTerm = channel.getLong();
          prevIndex = channel.getLong();
          commitIndex = channel.getLong();
          long leaderCommit = channel.getLong();
          int entryCount = channel.getInt();
          composer = new RaftMessageDecoder.AppendEntriesComposer(entryCount, from, leaderTerm,
              prevIndex, commitIndex, leaderCommit);
        } else if (messageType.equals(RaftMessages.Type.APPEND_ENTRIES_RESPONSE)) {
          leaderTerm = channel.getLong();
          success = channel.get() == 1;
          matchIndex = channel.getLong();
          appendIndex = channel.getLong();
          composer = new RaftMessageDecoder.SimpleMessageComposer(
              new RaftMessages.AppendEntries.Response(from, leaderTerm, success, matchIndex,
                  appendIndex));
        } else if (messageType.equals(RaftMessages.Type.NEW_ENTRY_REQUEST)) {
          composer = new RaftMessageDecoder.NewEntryRequestComposer(from);
        } else if (messageType.equals(RaftMessages.Type.HEARTBEAT)) {
          leaderTerm = channel.getLong();
          prevIndex = channel.getLong();
          commitIndex = channel.getLong();
          composer = new RaftMessageDecoder.SimpleMessageComposer(
              new RaftMessages.Heartbeat(from, leaderTerm, commitIndex, prevIndex));
        } else if (messageType.equals(RaftMessages.Type.HEARTBEAT_RESPONSE)) {
          composer = new RaftMessageDecoder.SimpleMessageComposer(
              new RaftMessages.HeartbeatResponse(from));
        } else {
          if (!messageType.equals(RaftMessages.Type.LOG_COMPACTION_INFO)) {
            throw new IllegalArgumentException("Unknown message type");
          }

          leaderTerm = channel.getLong();
          prevIndex = channel.getLong();
          composer = new RaftMessageDecoder.SimpleMessageComposer(
              new RaftMessages.LogCompactionInfo(from, leaderTerm, prevIndex));
        }
      }
    }

    list.add(new RaftMessageDecoder.RaftIdAwareMessageComposer(
        (RaftMessageDecoder.LazyComposer) composer, raftId));
    this.protocol.expect(ContentType.ContentType);
  }

  private MemberId retrieveMember(ReadableChannel buffer) throws IOException, EndOfStreamException {
    MemberId.Marshal memberIdMarshal = new MemberId.Marshal();
    return memberIdMarshal.unmarshal(buffer);
  }

  interface LazyComposer {

    Optional<RaftMessages.RaftMessage> maybeComplete(Queue<Long> n1, Queue<ReplicatedContent> n2);
  }

  private static class NewEntryRequestComposer implements RaftMessageDecoder.LazyComposer {

    private final MemberId from;

    NewEntryRequestComposer(MemberId from) {
      this.from = from;
    }

    public Optional<RaftMessages.RaftMessage> maybeComplete(Queue<Long> terms,
        Queue<ReplicatedContent> contents) {
      return contents.isEmpty() ? Optional.empty()
          : Optional.of(new RaftMessages.NewEntry.Request(this.from, contents.remove()));
    }
  }

  private static class AppendEntriesComposer implements RaftMessageDecoder.LazyComposer {

    private final int entryCount;
    private final MemberId from;
    private final long term;
    private final long prevLogIndex;
    private final long prevLogTerm;
    private final long leaderCommit;

    AppendEntriesComposer(int entryCount, MemberId from, long term, long prevLogIndex,
        long prevLogTerm, long leaderCommit) {
      this.entryCount = entryCount;
      this.from = from;
      this.term = term;
      this.prevLogIndex = prevLogIndex;
      this.prevLogTerm = prevLogTerm;
      this.leaderCommit = leaderCommit;
    }

    public Optional<RaftMessages.RaftMessage> maybeComplete(Queue<Long> terms,
        Queue<ReplicatedContent> contents) {
      if (terms.size() >= this.entryCount && contents.size() >= this.entryCount) {
        RaftLogEntry[] entries = new RaftLogEntry[this.entryCount];

        for (int i = 0; i < this.entryCount; ++i) {
          long term = terms.remove();
          ReplicatedContent content = contents.remove();
          entries[i] = new RaftLogEntry(term, content);
        }

        return Optional.of(
            new RaftMessages.AppendEntries.Request(this.from, this.term, this.prevLogIndex,
                this.prevLogTerm, entries, this.leaderCommit));
      } else {
        return Optional.empty();
      }
    }
  }

  private static class SimpleMessageComposer implements RaftMessageDecoder.LazyComposer {

    private final RaftMessages.RaftMessage message;

    private SimpleMessageComposer(RaftMessages.RaftMessage message) {
      this.message = message;
    }

    public Optional<RaftMessages.RaftMessage> maybeComplete(Queue<Long> terms,
        Queue<ReplicatedContent> contents) {
      return Optional.of(this.message);
    }
  }

  static class RaftIdAwareMessageComposer {

    private final RaftMessageDecoder.LazyComposer composer;
    private final RaftId raftId;

    RaftIdAwareMessageComposer(RaftMessageDecoder.LazyComposer composer, RaftId raftId) {
      this.composer = composer;
      this.raftId = raftId;
    }

    Optional<RaftMessages.RaftIdAwareMessage> maybeCompose(Clock clock, Queue<Long> terms,
        Queue<ReplicatedContent> contents) {
      return this.composer.maybeComplete(terms, contents).map((m) ->
      {
        return RaftMessages.ReceivedInstantRaftIdAwareMessage
            .of(clock.instant(), this.raftId, m);
      });
    }
  }
}
