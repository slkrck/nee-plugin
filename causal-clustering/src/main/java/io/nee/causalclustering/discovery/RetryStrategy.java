/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Predicate;
import java.util.function.Supplier;
import org.neo4j.internal.helpers.ConstantTimeTimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;

public class RetryStrategy {

  private final TimeoutStrategy timeoutStrategy;
  private final long retries;

  public RetryStrategy(long delayInMillis, long retries) {
    this(new ConstantTimeTimeoutStrategy(delayInMillis, TimeUnit.MILLISECONDS), retries);
  }

  public RetryStrategy(TimeoutStrategy timeoutStrategy, long retries) {
    this.timeoutStrategy = timeoutStrategy;
    this.retries = retries;
  }

  public <T> T apply(Supplier<T> action, Predicate<T> validator) throws TimeoutException {
    Timeout timeout = this.timeoutStrategy.newTimeout();
    T result = action.get();

    for (int n5 = 0; !validator.test(result); result = action.get()) {
      if (this.retries > 0L && (long) (n5++) == this.retries) {
        throw new TimeoutException(
            "Unable to fulfill predicate within " + this.retries + " retries");
      }

      try {
        Thread.sleep(timeout.getAndIncrement());
      } catch (InterruptedException n7) {
        Thread.currentThread().interrupt();
        throw new RuntimeException(n7);
      }
    }

    return result;
  }
}
