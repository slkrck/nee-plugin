/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.marshal;

import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.discovery.DatabaseCoreTopology;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class CoreTopologyMarshal extends SafeChannelMarshal<DatabaseCoreTopology> {

  private final ChannelMarshal<MemberId> memberIdMarshal = new MemberId.Marshal();
  private final ChannelMarshal<CoreServerInfo> coreServerInfoChannelMarshal = new CoreServerInfoMarshal();
  private final ChannelMarshal<RaftId> raftIdMarshal = new RaftId.Marshal();

  protected DatabaseCoreTopology unmarshal0(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    DatabaseId databaseId = DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal(channel);
    RaftId raftId = this.raftIdMarshal.unmarshal(channel);
    int memberCount = channel.getInt();
    HashMap<MemberId, CoreServerInfo> members = new HashMap(memberCount);

    for (int i = 0; i < memberCount; ++i) {
      MemberId memberId = this.memberIdMarshal.unmarshal(channel);
      CoreServerInfo coreServerInfo = this.coreServerInfoChannelMarshal.unmarshal(channel);
      members.put(memberId, coreServerInfo);
    }

    return new DatabaseCoreTopology(databaseId, raftId, members);
  }

  public void marshal(DatabaseCoreTopology coreTopology, WritableChannel channel)
      throws IOException {
    DatabaseIdWithoutNameMarshal.INSTANCE.marshal(coreTopology.databaseId(), channel);
    this.raftIdMarshal.marshal(coreTopology.raftId(), channel);
    channel.putInt(coreTopology.members().size());
    Iterator n3 = coreTopology.members().entrySet().iterator();

    while (n3.hasNext()) {
      Entry<MemberId, CoreServerInfo> entry = (Entry) n3.next();
      MemberId memberId = entry.getKey();
      CoreServerInfo coreServerInfo = entry.getValue();
      this.memberIdMarshal.marshal(memberId, channel);
      this.coreServerInfoChannelMarshal.marshal(coreServerInfo, channel);
    }
  }
}
