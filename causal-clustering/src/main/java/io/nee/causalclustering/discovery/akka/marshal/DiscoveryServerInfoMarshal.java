/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.marshal;

import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.discovery.DiscoveryServerInfo;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.StringMarshal;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

abstract class DiscoveryServerInfoMarshal<T extends DiscoveryServerInfo> extends
    SafeChannelMarshal<T> {

  static Set<String> unmarshalGroups(ReadableChannel channel) throws IOException {
    int size = channel.getInt();
    HashSet<String> groups = new HashSet(size);

    for (int i = 0; i < size; ++i) {
      groups.add(StringMarshal.unmarshal(channel));
    }

    return groups;
  }

  static void marshalGroups(DiscoveryServerInfo info, WritableChannel channel) throws IOException {
    Set<String> groups = info.groups();
    channel.putInt(groups.size());
    Iterator n3 = groups.iterator();

    while (n3.hasNext()) {
      String group = (String) n3.next();
      StringMarshal.marshal(channel, group);
    }
  }

  static Set<DatabaseId> unmarshalDatabaseIds(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    int size = channel.getInt();
    HashSet<DatabaseId> databaseIds = new HashSet(size);

    for (int i = 0; i < size; ++i) {
      databaseIds.add(DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal(channel));
    }

    return databaseIds;
  }

  static void marshalDatabaseIds(DiscoveryServerInfo info, WritableChannel channel)
      throws IOException {
    Set<DatabaseId> databaseIds = info.startedDatabaseIds();
    channel.putInt(databaseIds.size());
    Iterator n3 = databaseIds.iterator();

    while (n3.hasNext()) {
      DatabaseId databaseId = (DatabaseId) n3.next();
      DatabaseIdWithoutNameMarshal.INSTANCE.marshal(databaseId, channel);
    }
  }
}
