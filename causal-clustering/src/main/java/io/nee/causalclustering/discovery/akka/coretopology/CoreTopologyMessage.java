/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.coretopology;

import akka.actor.Address;
import io.nee.causalclustering.discovery.DatabaseCoreTopology;
import java.util.Collection;
import java.util.Objects;

public class CoreTopologyMessage {

  private final DatabaseCoreTopology coreTopology;
  private final Collection<Address> akkaMembers;

  public CoreTopologyMessage(DatabaseCoreTopology coreTopology, Collection<Address> akkaMembers) {
    this.coreTopology = coreTopology;
    this.akkaMembers = akkaMembers;
  }

  public DatabaseCoreTopology coreTopology() {
    return this.coreTopology;
  }

  public Collection<Address> akkaMembers() {
    return this.akkaMembers;
  }

  public String toString() {
    return "CoreTopologyMessage{coreTopology=" + this.coreTopology + ", akkaMembers="
        + this.akkaMembers + "}";
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      CoreTopologyMessage that = (CoreTopologyMessage) o;
      return Objects.equals(this.coreTopology, that.coreTopology) && Objects
          .equals(this.akkaMembers, that.akkaMembers);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.coreTopology, this.akkaMembers);
  }
}
