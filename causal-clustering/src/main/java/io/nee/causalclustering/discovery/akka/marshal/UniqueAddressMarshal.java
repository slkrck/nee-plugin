/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.marshal;

import akka.actor.Address;
import akka.cluster.UniqueAddress;
import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.StringMarshal;
import java.io.IOException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class UniqueAddressMarshal extends SafeChannelMarshal<UniqueAddress> {

  protected UniqueAddress unmarshal0(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    String protocol = StringMarshal.unmarshal(channel);
    String system = StringMarshal.unmarshal(channel);
    String host = StringMarshal.unmarshal(channel);
    int port = channel.getInt();
    long uid = channel.getLong();
    Address address;
    if (host != null) {
      address = new Address(protocol, system, host, port);
    } else {
      address = new Address(protocol, system);
    }

    return new UniqueAddress(address, uid);
  }

  public void marshal(UniqueAddress uniqueAddress, WritableChannel channel) throws IOException {
    Address address = uniqueAddress.address();
    StringMarshal.marshal(channel, address.protocol());
    StringMarshal.marshal(channel, address.system());
    if (address.host().isDefined()) {
      StringMarshal.marshal(channel, address.host().get());
    } else {
      StringMarshal.marshal(channel, null);
    }

    if (address.port().isDefined()) {
      channel.putInt((Integer) address.port().get());
    } else {
      channel.putInt(-1);
    }

    channel.putLong(uniqueAddress.longUid());
  }
}
