/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.readreplicatopology;

import akka.stream.javadsl.SourceQueueWithComplete;
import io.nee.causalclustering.discovery.DatabaseCoreTopology;
import io.nee.causalclustering.discovery.DatabaseReadReplicaTopology;
import io.nee.causalclustering.discovery.ReplicatedDatabaseState;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class PruningStateSink<T> {

  private final String stateType;
  private final SourceQueueWithComplete<T> stateSink;
  private final Function<DatabaseId, T> emptyStateFactory;
  private final Function<T, DatabaseId> getDatabaseId;
  private final Duration maxStateLifetime;
  private final Clock clock;
  private final Log log;
  private final Map<DatabaseId, Instant> lastUpdated = new HashMap();

  public PruningStateSink(String stateType, SourceQueueWithComplete<T> underlying,
      Function<DatabaseId, T> emptyStateFactory,
      Function<T, DatabaseId> getDatabaseId, Duration maxStateLifetime, Clock clock,
      LogProvider logProvider) {
    this.stateType = stateType;
    this.stateSink = underlying;
    this.emptyStateFactory = emptyStateFactory;
    this.maxStateLifetime = maxStateLifetime;
    this.clock = clock;
    this.log = logProvider.getLog(this.getClass());
    this.getDatabaseId = getDatabaseId;
  }

  public static PruningStateSink<DatabaseCoreTopology> forCoreTopologies(
      SourceQueueWithComplete<DatabaseCoreTopology> topologySink,
      Duration maxTopologyLifetime,
      Clock clock, LogProvider logProvider) {
    return null;
  }

  public static PruningStateSink<DatabaseReadReplicaTopology> forReadReplicaTopologies(
      SourceQueueWithComplete<DatabaseReadReplicaTopology> topologySink,
      Duration maxTopologyLifetime, Clock clock, LogProvider logProvider) {
    return null;
  }

  public static PruningStateSink<ReplicatedDatabaseState> forCoreDatabaseStates(
      SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink,
      Duration maxDatabaseStateLifetime, Clock clock, LogProvider logProvider) {
    return null;

  }

  public static PruningStateSink<ReplicatedDatabaseState> forReadReplicaDatabaseStates(
      SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink,
      Duration maxDatabaseStateLifetime, Clock clock,
      LogProvider logProvider) {
    return null;
  }

  void offer(T state) {
    this.stateSink.offer(state);
    this.lastUpdated.put(this.getDatabaseId.apply(state), this.clock.instant());
  }

  void pruneStaleState() {
    Set<DatabaseId> databaseIdsForStaleState = this.findDatabaseIdsForStaleState();
    Iterator n2 = databaseIdsForStaleState.iterator();

    while (n2.hasNext()) {
      DatabaseId databaseId = (DatabaseId) n2.next();
      this.log.info("%s for database %s hasn't been updated for more than %s and will be removed",
          this.stateType, databaseId, this.maxStateLifetime);
      T empty = this.emptyStateFactory.apply(databaseId);
      this.stateSink.offer(empty);
      this.lastUpdated.remove(databaseId);
    }
  }

  private Set<DatabaseId> findDatabaseIdsForStaleState() {
    Instant oldestAllowed = this.clock.instant().minus(this.maxStateLifetime);
    return this.lastUpdated.entrySet().stream().filter((entry) ->
    {
      return entry.getValue().isBefore(oldestAllowed);
    }).map(Entry::getKey).collect(Collectors.toSet());
  }
}
