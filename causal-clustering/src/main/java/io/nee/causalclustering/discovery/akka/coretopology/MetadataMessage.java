/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.coretopology;

import akka.cluster.UniqueAddress;
import akka.cluster.ddata.LWWMap;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;
import org.neo4j.util.VisibleForTesting;

public class MetadataMessage {

  public static final MetadataMessage EMPTY = new MetadataMessage(Collections.emptyMap());
  private final Map<UniqueAddress, CoreServerInfoForMemberId> metadata;

  public MetadataMessage(LWWMap<UniqueAddress, CoreServerInfoForMemberId> metadata) {
    this(metadata.getEntries());
  }

  @VisibleForTesting
  public MetadataMessage(Map<UniqueAddress, CoreServerInfoForMemberId> metadata) {
    this.metadata = Collections.unmodifiableMap(metadata);
  }

  public Optional<CoreServerInfoForMemberId> getOpt(UniqueAddress address) {
    return Optional.ofNullable(this.metadata.get(address));
  }

  public Stream<CoreServerInfoForMemberId> getStream() {
    return this.metadata.values().stream();
  }

  public Stream<CoreServerInfoForMemberId> getStream(UniqueAddress address) {
    return Stream.ofNullable(this.metadata.get(address));
  }

  public String toString() {
    return "MetadataMessage{metadata=" + this.metadata + "}";
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      MetadataMessage that = (MetadataMessage) o;
      return Objects.equals(this.metadata, that.metadata);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.metadata);
  }
}
