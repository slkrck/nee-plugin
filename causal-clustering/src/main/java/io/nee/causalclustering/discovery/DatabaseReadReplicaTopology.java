/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import io.nee.causalclustering.identity.MemberId;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import org.neo4j.kernel.database.DatabaseId;

public class DatabaseReadReplicaTopology implements Topology<ReadReplicaInfo> {

  private final DatabaseId databaseId;
  private final Map<MemberId, ReadReplicaInfo> readReplicaMembers;

  public DatabaseReadReplicaTopology(DatabaseId databaseId,
      Map<MemberId, ReadReplicaInfo> readReplicaMembers) {
    this.databaseId = Objects.requireNonNull(databaseId);
    this.readReplicaMembers = readReplicaMembers;
  }

  public static DatabaseReadReplicaTopology empty(DatabaseId databaseId) {
    return new DatabaseReadReplicaTopology(databaseId, Collections.emptyMap());
  }

  public DatabaseId databaseId() {
    return this.databaseId;
  }

  public Map<MemberId, ReadReplicaInfo> members() {
    return this.readReplicaMembers;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      DatabaseReadReplicaTopology that = (DatabaseReadReplicaTopology) o;
      return Objects.equals(this.databaseId, that.databaseId) && Objects
          .equals(this.readReplicaMembers, that.readReplicaMembers);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.databaseId, this.readReplicaMembers);
  }

  public String toString() {
    return String.format("DatabaseReadReplicaTopology{%s %s}", this.databaseId,
        this.readReplicaMembers.keySet());
  }
}
