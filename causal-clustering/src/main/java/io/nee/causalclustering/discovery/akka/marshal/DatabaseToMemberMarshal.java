/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.marshal;

import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.discovery.akka.database.state.DatabaseToMember;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.EndOfStreamException;
import java.io.IOException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class DatabaseToMemberMarshal extends SafeChannelMarshal<DatabaseToMember> {

  public static final DatabaseToMemberMarshal INSTANCE = new DatabaseToMemberMarshal();

  private DatabaseToMemberMarshal() {
  }

  protected DatabaseToMember unmarshal0(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    DatabaseId databaseId = DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal(channel);
    MemberId memberId = MemberId.Marshal.INSTANCE.unmarshal(channel);
    return new DatabaseToMember(databaseId, memberId);
  }

  public void marshal(DatabaseToMember databaseToMember, WritableChannel channel)
      throws IOException {
    DatabaseIdWithoutNameMarshal.INSTANCE.marshal(databaseToMember.databaseId(), channel);
    MemberId.Marshal.INSTANCE.marshal(databaseToMember.memberId(), channel);
  }
}
