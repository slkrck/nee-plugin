/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.coretopology;

import akka.cluster.UniqueAddress;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.discovery.CoreServerInfo;
import java.util.Objects;
import java.util.Optional;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.NamedDatabaseId;

public class BootstrapState {

  public static final BootstrapState EMPTY;

  static {
    EMPTY = new BootstrapState(ClusterViewMessage.EMPTY, MetadataMessage.EMPTY, null, null);
  }

  private final ClusterViewMessage clusterView;
  private final MetadataMessage memberData;
  private final UniqueAddress selfAddress;
  private final Config config;

  BootstrapState(ClusterViewMessage clusterView, MetadataMessage memberData,
      UniqueAddress selfAddress, Config config) {
    this.clusterView = Objects.requireNonNull(clusterView);
    this.memberData = Objects.requireNonNull(memberData);
    this.selfAddress = selfAddress;
    this.config = config;
  }

  private static boolean isPotentialLeader(CoreServerInfoForMemberId infoForMember,
      NamedDatabaseId namedDatabaseId) {
    CoreServerInfo info = infoForMember.coreServerInfo();
    return !info.refusesToBeLeader() && info.startedDatabaseIds()
        .contains(namedDatabaseId.databaseId());
  }

  public boolean canBootstrapRaft(NamedDatabaseId namedDatabaseId) {
    boolean iDoNotRefuseToBeLeader =
        this.config != null && !this.config.get(CausalClusteringSettings.refuse_to_be_leader);
    boolean clusterHasConverged = this.clusterView.converged();
    boolean iAmFirstPotentialLeader = this.iAmFirstPotentialLeader(namedDatabaseId);
    return iDoNotRefuseToBeLeader && clusterHasConverged && iAmFirstPotentialLeader;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      BootstrapState that = (BootstrapState) o;
      return Objects.equals(this.clusterView, that.clusterView) && Objects
          .equals(this.memberData, that.memberData) &&
          Objects.equals(this.selfAddress, that.selfAddress) && Objects
          .equals(this.config, that.config);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.clusterView, this.memberData, this.selfAddress, this.config);
  }

  private boolean iAmFirstPotentialLeader(NamedDatabaseId namedDatabaseId) {
    Optional<UniqueAddress> firstPotentialLeader = this.clusterView.availableMembers()
        .filter((member) ->
        {
          return this.isPotentialLeader(member, namedDatabaseId);
        }).findFirst();
    return firstPotentialLeader.map((address) ->
    {
      return Objects.equals(address, this.selfAddress);
    }).orElse(false);
  }

  private boolean isPotentialLeader(UniqueAddress member, NamedDatabaseId namedDatabaseId) {
    return this.memberData.getOpt(member).map((metadata) ->
    {
      return isPotentialLeader(metadata, namedDatabaseId);
    }).orElse(false);
  }
}
