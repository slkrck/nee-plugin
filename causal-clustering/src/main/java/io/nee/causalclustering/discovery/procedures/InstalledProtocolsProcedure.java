/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.procedures;

import io.nee.causalclustering.protocol.Protocol;
import io.nee.causalclustering.protocol.handshake.ProtocolStack;
import java.util.Comparator;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.collection.RawIterator;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.collection.Iterators;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes;
import org.neo4j.internal.kernel.api.procs.ProcedureSignature;
import org.neo4j.internal.kernel.api.procs.QualifiedName;
import org.neo4j.kernel.api.ResourceTracker;
import org.neo4j.kernel.api.procedure.CallableProcedure.BasicProcedure;
import org.neo4j.kernel.api.procedure.Context;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;

public class InstalledProtocolsProcedure extends BasicProcedure {

  public static final String PROCEDURE_NAME = "protocols";
  private static final String[] PROCEDURE_NAMESPACE = new String[]{"dbms", "cluster"};
  private final Supplier<Stream<Pair<SocketAddress, ProtocolStack>>> clientInstalledProtocols;
  private final Supplier<Stream<Pair<SocketAddress, ProtocolStack>>> serverInstalledProtocols;

  public InstalledProtocolsProcedure(
      Supplier<Stream<Pair<SocketAddress, ProtocolStack>>> clientInstalledProtocols,
      Supplier<Stream<Pair<SocketAddress, ProtocolStack>>> serverInstalledProtocols) {
    super(ProcedureSignature.procedureSignature(new QualifiedName(PROCEDURE_NAMESPACE, "protocols"))
        .out("orientation", Neo4jTypes.NTString).out(
            "remoteAddress", Neo4jTypes.NTString).out("applicationProtocol", Neo4jTypes.NTString)
        .out("applicationProtocolVersion",
            Neo4jTypes.NTInteger)
        .out("modifierProtocols", Neo4jTypes.NTString).description(
            "Overview of installed protocols").systemProcedure().build());
    this.clientInstalledProtocols = clientInstalledProtocols;
    this.serverInstalledProtocols = serverInstalledProtocols;
  }

  public RawIterator<AnyValue[], ProcedureException> apply(Context ctx, AnyValue[] input,
      ResourceTracker resourceTracker) {
    Stream<AnyValue[]> outbound = this.toOutputRows(this.clientInstalledProtocols, "outbound");
    Stream<AnyValue[]> inbound = this.toOutputRows(this.serverInstalledProtocols, "inbound");
    return Iterators.asRawIterator(Stream.concat(outbound, inbound));
  }

  private <T extends SocketAddress> Stream<AnyValue[]> toOutputRows(
      Supplier<Stream<Pair<T, ProtocolStack>>> installedProtocols, String orientation) {
    Comparator<Pair<T, ProtocolStack>> connectionInfoComparator = Comparator
        .comparing((Pair<T, ProtocolStack> entry) ->
        {
          return entry.first().getHostname();
        }).thenComparing((Pair<T, ProtocolStack> entry) ->
        {
          return entry.first()
              .getPort();
        });
    return installedProtocols.get().sorted(connectionInfoComparator).map((entry) ->
    {
      return this
          .buildRow(entry, orientation);
    });
  }

  private <T extends SocketAddress> AnyValue[] buildRow(Pair<T, ProtocolStack> connectionInfo,
      String orientation) {
    T socketAddress = connectionInfo.first();
    ProtocolStack protocolStack = connectionInfo.other();
    return new AnyValue[]{Values.stringValue(orientation),
        Values.stringValue(socketAddress.toString()),
        Values.stringValue(protocolStack.applicationProtocol().category()),
        Values.stringValue(protocolStack.applicationProtocol().implementation().toString()),
        Values.stringValue(this.modifierString(protocolStack))};
  }

  private String modifierString(ProtocolStack protocolStack) {
    return protocolStack.modifierProtocols().stream().map(Protocol::implementation)
        .collect(Collectors.joining(",", "[", "]"));
  }
}
