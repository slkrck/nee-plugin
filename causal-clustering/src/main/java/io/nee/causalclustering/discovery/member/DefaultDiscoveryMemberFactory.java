/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.member;

import io.nee.causalclustering.identity.MemberId;
import java.util.Set;
import java.util.stream.Collectors;
import org.neo4j.dbms.DatabaseStateService;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;

public class DefaultDiscoveryMemberFactory implements DiscoveryMemberFactory {

  private final DatabaseManager<?> databaseManager;
  private final DatabaseStateService databaseStateService;

  public DefaultDiscoveryMemberFactory(DatabaseManager<?> databaseManager,
      DatabaseStateService databaseStateService) {
    this.databaseManager = databaseManager;
    this.databaseStateService = databaseStateService;
  }

  public DiscoveryMember create(MemberId id) {
    Set<DatabaseId> startedDatabases = this.startedDatabases();
    return new DefaultDiscoveryMember(id, startedDatabases);
  }

  private Set<DatabaseId> startedDatabases() {
    return this.databaseManager.registeredDatabases().values().stream()
        .map(DatabaseContext::database).filter((db) ->
        {
          return !this.hasFailed(
              db.getNamedDatabaseId()) &&
              db.isStarted();
        })
        .map(Database::getNamedDatabaseId).map(NamedDatabaseId::databaseId)
        .collect(Collectors.toUnmodifiableSet());
  }

  private boolean hasFailed(NamedDatabaseId namedDatabaseId) {
    return this.databaseStateService.causeOfFailure(namedDatabaseId).isPresent();
  }
}
