/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import org.neo4j.kernel.database.DatabaseId;

public class DatabaseCoreTopology implements Topology<CoreServerInfo> {

  private final DatabaseId databaseId;
  private final RaftId raftId;
  private final Map<MemberId, CoreServerInfo> coreMembers;

  public DatabaseCoreTopology(DatabaseId databaseId, RaftId raftId,
      Map<MemberId, CoreServerInfo> coreMembers) {
    this.databaseId = Objects.requireNonNull(databaseId);
    this.raftId = raftId;
    this.coreMembers = Map.copyOf(coreMembers);
  }

  public static DatabaseCoreTopology empty(DatabaseId databaseId) {
    return new DatabaseCoreTopology(databaseId, null, Collections.emptyMap());
  }

  public Map<MemberId, CoreServerInfo> members() {
    return this.coreMembers;
  }

  public DatabaseId databaseId() {
    return this.databaseId;
  }

  public RaftId raftId() {
    return this.raftId;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      DatabaseCoreTopology that = (DatabaseCoreTopology) o;
      return Objects.equals(this.databaseId, that.databaseId) && Objects
          .equals(this.raftId, that.raftId) &&
          Objects.equals(this.coreMembers, that.coreMembers);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.databaseId, this.raftId, this.coreMembers);
  }

  public String toString() {
    return String.format("DatabaseCoreTopology{%s %s}", this.databaseId, this.coreMembers.keySet());
  }
}
