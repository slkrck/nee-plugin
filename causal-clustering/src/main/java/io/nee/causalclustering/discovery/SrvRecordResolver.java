/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import java.util.stream.Stream;
import javax.naming.NamingException;

public abstract class SrvRecordResolver {

  public abstract Stream<SrvRecordResolver.SrvRecord> resolveSrvRecord(String n1)
      throws NamingException;

  public Stream<SrvRecordResolver.SrvRecord> resolveSrvRecord(String service, String protocol,
      String hostname) throws NamingException {
    String url = String.format("_%s._%s.%s", service, protocol, hostname);
    return this.resolveSrvRecord(url);
  }

  public static class SrvRecord {

    public final int priority;
    public final int weight;
    public final int port;
    public final String host;

    private SrvRecord(int priority, int weight, int port, String host) {
      this.priority = priority;
      this.weight = weight;
      this.port = port;
      this.host =
          host.charAt(host.length() - 1) == '.' ? host.substring(0, host.length() - 1) : host;
    }

    public static SrvRecord parse(String input) {
      String[] parts = input.split(" ");
      return new SrvRecord(
          Integer.parseInt(parts[0]),
          Integer.parseInt(parts[1]),
          Integer.parseInt(parts[2]),
          parts[3]
      );
    }
  }
}
