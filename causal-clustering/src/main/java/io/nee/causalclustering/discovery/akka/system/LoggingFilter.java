/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.system;

import akka.actor.ActorSystem.Settings;
import akka.event.DefaultLoggingFilter;
import akka.event.EventStream;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;

public class LoggingFilter implements akka.event.LoggingFilter {

  private static LogProvider logProvider = NullLogProvider.getInstance();
  private final DefaultLoggingFilter defaultLoggingFilter;

  LoggingFilter(Settings settings, EventStream eventStream) {
    this.defaultLoggingFilter = new DefaultLoggingFilter(settings, eventStream);
  }

  static void enable(LogProvider logProvider) {
    LoggingFilter.logProvider = logProvider;
  }

  public boolean isErrorEnabled(Class<?> logClass, String logSource) {
    return this.defaultLoggingFilter.isErrorEnabled(logClass, logSource);
  }

  public boolean isWarningEnabled(Class<?> logClass, String logSource) {
    return this.defaultLoggingFilter.isWarningEnabled(logClass, logSource);
  }

  public boolean isInfoEnabled(Class<?> logClass, String logSource) {
    return this.defaultLoggingFilter.isInfoEnabled(logClass, logSource);
  }

  public boolean isDebugEnabled(Class<?> logClass, String logSource) {
    return this.defaultLoggingFilter.isDebugEnabled(logClass, logSource) && logProvider
        .getLog(logClass).isDebugEnabled();
  }
}
