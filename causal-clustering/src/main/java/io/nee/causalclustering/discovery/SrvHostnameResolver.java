/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import javax.naming.NamingException;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;

public class SrvHostnameResolver extends RetryingHostnameResolver {

  private final Log userLog;
  private final Log log;
  private final SrvRecordResolver srvRecordResolver;

  SrvHostnameResolver(LogService logService, SrvRecordResolver srvRecordResolver, Config config,
      RetryStrategy retryStrategy) {
    super(config, retryStrategy);
    this.log = logService.getInternalLog(this.getClass());
    this.userLog = logService.getUserLog(this.getClass());
    this.srvRecordResolver = srvRecordResolver;
  }

  public static RemoteMembersResolver resolver(LogService logService,
      SrvRecordResolver srvHostnameResolver, Config config) {
    SrvHostnameResolver hostnameResolver = new SrvHostnameResolver(logService, srvHostnameResolver,
        config, defaultRetryStrategy(config));
    return new InitialDiscoveryMembersResolver(hostnameResolver, config);
  }

  public Collection<SocketAddress> resolveOnce(SocketAddress initialAddress) {
    try {
      Set<SocketAddress> addresses = this.srvRecordResolver
          .resolveSrvRecord(initialAddress.getHostname()).map((srvRecord) ->
          {
            return new SocketAddress(
                srvRecord.host,
                srvRecord.port);
          }).collect(Collectors.toSet());
      this.userLog.info("Resolved initial host '%s' to %s", initialAddress, addresses);
      if (addresses.isEmpty()) {
        this.log.error("Failed to resolve srv records for '%s'", initialAddress.getHostname());
      }

      return addresses;
    } catch (NamingException n3) {
      this.log.error(
          String.format("Failed to resolve srv records for '%s'", initialAddress.getHostname()),
          n3);
      return Collections.emptySet();
    }
  }
}
