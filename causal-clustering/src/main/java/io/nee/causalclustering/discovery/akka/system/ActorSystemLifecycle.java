/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.system;

import akka.Done;
import akka.actor.ActorPath;
import akka.actor.ActorPaths;
import akka.actor.ActorRef;
import akka.actor.Address;
import akka.actor.CoordinatedShutdown;
import akka.actor.CoordinatedShutdown.Reason;
import akka.actor.Props;
import akka.actor.ProviderSelection;
import akka.cluster.Cluster;
import akka.cluster.client.ClusterClientReceptionist;
import akka.cluster.client.ClusterClientSettings;
import akka.event.EventStream;
import akka.japi.function.Procedure;
import akka.pattern.Patterns;
import akka.stream.javadsl.SourceQueueWithComplete;
import io.nee.causalclustering.discovery.RemoteMembersResolver;
import java.time.Duration;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.util.VisibleForTesting;

public class ActorSystemLifecycle {

  static final int SYSTEM_SHUTDOWN_TIMEOUT_S = 60;
  static final int ACTOR_SHUTDOWN_TIMEOUT_S = 15;
  private final ActorSystemFactory actorSystemFactory;
  private final RemoteMembersResolver resolver;
  private final JoinMessageFactory joinMessageFactory;
  private final Config config;
  private final Log log;
  @VisibleForTesting
  protected ActorSystemComponents actorSystemComponents;

  public ActorSystemLifecycle(ActorSystemFactory actorSystemFactory, RemoteMembersResolver resolver,
      JoinMessageFactory joinMessageFactory, Config config,
      LogProvider logProvider) {
    this.actorSystemFactory = actorSystemFactory;
    this.resolver = resolver;
    this.joinMessageFactory = joinMessageFactory;
    this.config = config;
    this.log = logProvider.getLog(this.getClass());
  }

  public void createClusterActorSystem() {
    this.actorSystemComponents = new ActorSystemComponents(this.actorSystemFactory,
        ProviderSelection.cluster());
    Props props = ClusterJoiningActor.props(this.cluster(), this.resolver, this.config);
    this.applicationActorOf(props, "joiner")
        .tell(this.joinMessageFactory.message(), ActorRef.noSender());
  }

  public void createClientActorSystem() {
    this.actorSystemComponents = new ActorSystemComponents(this.actorSystemFactory,
        ProviderSelection.remote());
  }

  public void addSeenAddresses(Collection<Address> addresses) {
    this.joinMessageFactory.addSeenAddresses(addresses);
  }

  public void shutdown() throws Exception {
    if (this.actorSystemComponents != null) {
      try {
        this.doShutdown(this.actorSystemComponents);
      } catch (Exception n5) {
        this.log.warn("Exception shutting down actor system", n5);
        throw n5;
      } finally {
        LoggingActor.disable(this.actorSystemComponents.actorSystem());
        this.actorSystemComponents = null;
      }
    }
  }

  @VisibleForTesting
  void doShutdown(ActorSystemComponents actorSystemComponents) throws Exception {
    actorSystemComponents.coordinatedShutdown()
        .runAll(ActorSystemLifecycle.ShutdownByNeo4jLifecycle.INSTANCE).toCompletableFuture()
        .get(60L,
            TimeUnit.SECONDS);
  }

  public <T> SourceQueueWithComplete<T> queueMostRecent(Procedure<T> sink) {
    return null;
        /*SourceQueueWithComplete<T> queue = Source.queue( 1, OverflowStrategy.dropHead() ).to( Sink.foreach( sink ) ).run(
                this.actorSystemComponents.materializer() );
        this.actorSystemComponents.coordinatedShutdown().addTask( CoordinatedShutdown.PhaseServiceStop(), "queue-" + UUID.randomUUID(), () -> {
            return this.completeQueue( queue );
        } );
        return queue;

         */
  }

  private <T> CompletionStage<Done> completeQueue(SourceQueueWithComplete<T> queue) {
    queue.complete();
    return queue.watchCompletion();
  }

  public ActorRef applicationActorOf(Props props, String name) {
    ActorRef actorRef = this.actorSystemComponents.actorSystem().actorOf(props, name);
    this.actorSystemComponents.coordinatedShutdown()
        .addTask(CoordinatedShutdown.PhaseServiceUnbind(), name + "-shutdown", () ->
        {
          return this.gracefulShutdown(actorRef);
        });
    return actorRef;
  }

  public ActorRef systemActorOf(Props props, String name) {
    return this.actorSystemComponents.actorSystem().actorOf(props, name);
  }

  private CompletionStage<Done> gracefulShutdown(ActorRef actor) {
    return Patterns.gracefulStop(actor, Duration.ofSeconds(15L)).thenApply((ignored) ->
    {
      return Done.done();
    });
  }

  public EventStream eventStream() {
    return this.actorSystemComponents.actorSystem().eventStream();
  }

  public Cluster cluster() {
    return this.actorSystemComponents.cluster();
  }

  public ActorRef replicator() {
    return this.actorSystemComponents.replicator();
  }

  public ClusterClientReceptionist clusterClientReceptionist() {
    return this.actorSystemComponents.clusterClientReceptionist();
  }

  public ClusterClientSettings clusterClientSettings() {
    Set<ActorPath> actorPaths = this.resolver.resolve(this::toActorPath, HashSet::new);
    return ClusterClientSettings.create(this.actorSystemComponents.actorSystem())
        .withInitialContacts(actorPaths);
  }

  private ActorPath toActorPath(SocketAddress addr) {
    String path = String
        .format("%s://%s@%s/system/receptionist", "akka", "cc-discovery-actor-system",
            addr.toString());
    return ActorPaths.fromString(path);
  }

  private static class ShutdownByNeo4jLifecycle implements Reason {

    static ActorSystemLifecycle.ShutdownByNeo4jLifecycle INSTANCE = new ActorSystemLifecycle.ShutdownByNeo4jLifecycle();
  }
}
