/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.messaging.marshalling.StringMarshal;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.BoltConnector;
import org.neo4j.configuration.connectors.HttpConnector;
import org.neo4j.configuration.connectors.HttpsConnector;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.configuration.helpers.SocketAddressParser;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class ClientConnectorAddresses implements Iterable<ClientConnectorAddresses.ConnectorUri> {

  private final List<ClientConnectorAddresses.ConnectorUri> connectorUris;

  public ClientConnectorAddresses(List<ClientConnectorAddresses.ConnectorUri> connectorUris) {
    this.connectorUris = connectorUris;
  }

  public static ClientConnectorAddresses extractFromConfig(Config config) {
    List<ClientConnectorAddresses.ConnectorUri> connectorUris = new ArrayList();
    connectorUris
        .add(new ClientConnectorAddresses.ConnectorUri(ClientConnectorAddresses.Scheme.bolt,
            config.get(BoltConnector.advertised_address)));
    if (config.get(HttpConnector.enabled)) {
      connectorUris
          .add(new ClientConnectorAddresses.ConnectorUri(ClientConnectorAddresses.Scheme.http,
              config.get(HttpConnector.advertised_address)));
    }

    if (config.get(HttpsConnector.enabled)) {
      connectorUris
          .add(new ClientConnectorAddresses.ConnectorUri(ClientConnectorAddresses.Scheme.https,
              config.get(HttpsConnector.advertised_address)));
    }

    return new ClientConnectorAddresses(connectorUris);
  }

  static ClientConnectorAddresses fromString(String value) {
    return new ClientConnectorAddresses(Stream.of(value.split(",")).map((n) ->
    {
      return ConnectorUri.fromString(n);
    }).collect(Collectors.toList()));
  }

  public SocketAddress boltAddress() {
    return this.connectorUris.stream().filter((connectorUri) ->
    {
      return connectorUri.scheme ==
          Scheme.bolt;
    }).findFirst().orElseThrow(() ->
    {
      return new IllegalArgumentException(
          "A Bolt connector must be configured to run a cluster");
    }).socketAddress;
  }

  public List<URI> uriList() {
    return this.connectorUris.stream().map((rec) ->
    {
      return rec.toUri();
    }).collect(Collectors.toList());
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ClientConnectorAddresses that = (ClientConnectorAddresses) o;
      return Objects.equals(this.connectorUris, that.connectorUris);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.connectorUris);
  }

  public String toString() {
    return this.connectorUris.stream().map(ConnectorUri::toString).collect(Collectors.joining(","));
  }

  public Iterator<ClientConnectorAddresses.ConnectorUri> iterator() {
    return this.connectorUris.iterator();
  }

  public enum Scheme {
    bolt,
    http,
    https
  }

  public static class Marshal extends SafeChannelMarshal<ClientConnectorAddresses> {

    protected ClientConnectorAddresses unmarshal0(ReadableChannel channel) throws IOException {
      int size = channel.getInt();
      List<ClientConnectorAddresses.ConnectorUri> connectorUris = new ArrayList(size);

      for (int i = 0; i < size; ++i) {
        String schemeName = StringMarshal.unmarshal(channel);
        String hostName = StringMarshal.unmarshal(channel);
        int port = channel.getInt();
        connectorUris.add(new ClientConnectorAddresses.ConnectorUri(
            ClientConnectorAddresses.Scheme.valueOf(schemeName),
            new SocketAddress(hostName, port)));
      }

      return new ClientConnectorAddresses(connectorUris);
    }

    public void marshal(ClientConnectorAddresses connectorUris, WritableChannel channel)
        throws IOException {
      channel.putInt(connectorUris.connectorUris.size());
      Iterator n3 = connectorUris.iterator();

      while (n3.hasNext()) {
        ClientConnectorAddresses.ConnectorUri uri = (ClientConnectorAddresses.ConnectorUri) n3
            .next();
        StringMarshal.marshal(channel, uri.scheme.name());
        StringMarshal.marshal(channel, uri.socketAddress.getHostname());
        channel.putInt(uri.socketAddress.getPort());
      }
    }
  }

  public static class ConnectorUri {

    private final ClientConnectorAddresses.Scheme scheme;
    private final SocketAddress socketAddress;

    public ConnectorUri(ClientConnectorAddresses.Scheme scheme, SocketAddress socketAddress) {
      this.scheme = scheme;
      this.socketAddress = socketAddress;
    }

    private static ClientConnectorAddresses.ConnectorUri fromString(String string) {
      URI uri = URI.create(string);
      SocketAddress advertisedSocketAddress = SocketAddressParser
          .socketAddress(uri.getAuthority(), SocketAddress::new);
      return new ClientConnectorAddresses.ConnectorUri(
          ClientConnectorAddresses.Scheme.valueOf(uri.getScheme()), advertisedSocketAddress);
    }

    private URI toUri() {
      try {
        return new URI(this.scheme.name().toLowerCase(), null, this.socketAddress.getHostname(),
            this.socketAddress.getPort(), null,
            null, null);
      } catch (URISyntaxException n2) {
        throw new IllegalArgumentException(n2);
      }
    }

    public String toString() {
      return this.toUri().toString();
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        ClientConnectorAddresses.ConnectorUri that = (ClientConnectorAddresses.ConnectorUri) o;
        return this.scheme == that.scheme && Objects.equals(this.socketAddress, that.socketAddress);
      } else {
        return false;
      }
    }

    public int hashCode() {
      return Objects.hash(this.scheme, this.socketAddress);
    }
  }
}
