/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.client.ClusterClient;
import akka.cluster.client.ClusterClientSettings;
import akka.stream.javadsl.SourceQueueWithComplete;
import io.nee.causalclustering.catchup.CatchupAddressResolutionException;
import io.nee.causalclustering.core.consensus.LeaderInfo;
import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.discovery.DatabaseCoreTopology;
import io.nee.causalclustering.discovery.DatabaseReadReplicaTopology;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.ReplicatedDatabaseState;
import io.nee.causalclustering.discovery.RoleInfo;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.discovery.akka.common.DatabaseStartedMessage;
import io.nee.causalclustering.discovery.akka.common.DatabaseStoppedMessage;
import io.nee.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import io.nee.causalclustering.discovery.akka.readreplicatopology.ClientTopologyActor;
import io.nee.causalclustering.discovery.akka.system.ActorSystemLifecycle;
import io.nee.causalclustering.discovery.member.DiscoveryMember;
import io.nee.causalclustering.discovery.member.DiscoveryMemberFactory;
import io.nee.causalclustering.identity.MemberId;
import java.time.Clock;
import java.util.Map;
import java.util.Objects;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.dbms.DatabaseState;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.SafeLifecycle;
import org.neo4j.logging.LogProvider;
import org.neo4j.util.VisibleForTesting;

public class AkkaTopologyClient extends SafeLifecycle implements TopologyService {

  private final Config config;
  private final ActorSystemLifecycle actorSystemLifecycle;
  private final DiscoveryMemberFactory discoveryMemberFactory;
  private final MemberId myself;
  private final LogProvider logProvider;
  private final Clock clock;
  private volatile ActorRef clientTopologyActorRef;
  private volatile GlobalTopologyState globalTopologyState;

  AkkaTopologyClient(Config config, LogProvider logProvider, MemberId myself,
      ActorSystemLifecycle actorSystemLifecycle,
      DiscoveryMemberFactory discoveryMemberFactory, Clock clock) {
    this.config = config;
    this.myself = myself;
    this.actorSystemLifecycle = actorSystemLifecycle;
    this.discoveryMemberFactory = discoveryMemberFactory;
    this.logProvider = logProvider;
    this.globalTopologyState = newGlobalTopologyState(logProvider);
    this.clock = clock;
  }

  private static GlobalTopologyState newGlobalTopologyState(LogProvider logProvider) {
    return new GlobalTopologyState(logProvider, (ignored) ->
    {
    });
  }

  public void start0() {
    this.actorSystemLifecycle.createClientActorSystem();
    this.startTopologyActors();
  }

  private void startTopologyActors() {
    ClusterClientSettings clusterClientSettings = this.actorSystemLifecycle.clusterClientSettings();
    ActorRef clusterClient = this.actorSystemLifecycle
        .systemActorOf(ClusterClient.props(clusterClientSettings), "cluster-client");
    ActorSystemLifecycle n10000 = this.actorSystemLifecycle;
    GlobalTopologyState n10001 = this.globalTopologyState;
    Objects.requireNonNull(n10001);
    SourceQueueWithComplete<DatabaseCoreTopology> coreTopologySink = n10000
        .queueMostRecent(n10001::onTopologyUpdate);
    n10000 = this.actorSystemLifecycle;
    n10001 = this.globalTopologyState;
    Objects.requireNonNull(n10001);
    SourceQueueWithComplete<DatabaseReadReplicaTopology> rrTopologySink = n10000
        .queueMostRecent(n10001::onTopologyUpdate);
    n10000 = this.actorSystemLifecycle;
    n10001 = this.globalTopologyState;
    Objects.requireNonNull(n10001);
    SourceQueueWithComplete<Map<DatabaseId, LeaderInfo>> directorySink = n10000
        .queueMostRecent(n10001::onDbLeaderUpdate);
    n10000 = this.actorSystemLifecycle;
    n10001 = this.globalTopologyState;
    Objects.requireNonNull(n10001);
    SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink = n10000
        .queueMostRecent(n10001::onDbStateUpdate);
    DiscoveryMember discoveryMember = this.discoveryMemberFactory.create(this.myself);
    Props clientTopologyProps =
        ClientTopologyActor.props(discoveryMember, coreTopologySink, rrTopologySink, directorySink,
            databaseStateSink, clusterClient, this.config,
            this.logProvider, this.clock);
    this.clientTopologyActorRef = this.actorSystemLifecycle
        .applicationActorOf(clientTopologyProps, "cc-client-topology-actor");
  }

  public void stop0() throws Exception {
    this.clientTopologyActorRef = null;
    this.actorSystemLifecycle.shutdown();
    this.globalTopologyState = newGlobalTopologyState(this.logProvider);
  }

  public void onDatabaseStart(NamedDatabaseId namedDatabaseId) {
    ActorRef clientTopologyActor = this.clientTopologyActorRef;
    if (clientTopologyActor != null) {
      clientTopologyActor.tell(new DatabaseStartedMessage(namedDatabaseId), ActorRef.noSender());
    }
  }

  public void onDatabaseStop(NamedDatabaseId namedDatabaseId) {
    ActorRef clientTopologyActor = this.clientTopologyActorRef;
    if (clientTopologyActor != null) {
      clientTopologyActor.tell(new DatabaseStoppedMessage(namedDatabaseId), ActorRef.noSender());
    }
  }

  public void stateChange(DatabaseState previousState, DatabaseState newState) {
    ActorRef clientTopologyActor = this.clientTopologyActorRef;
    if (clientTopologyActor != null) {
      clientTopologyActor.tell(DiscoveryDatabaseState.from(newState), ActorRef.noSender());
    }
  }

  public Map<MemberId, CoreServerInfo> allCoreServers() {
    return this.globalTopologyState.allCoreServers();
  }

  public DatabaseCoreTopology coreTopologyForDatabase(NamedDatabaseId namedDatabaseId) {
    return this.globalTopologyState.coreTopologyForDatabase(namedDatabaseId);
  }

  public Map<MemberId, ReadReplicaInfo> allReadReplicas() {
    return this.globalTopologyState.allReadReplicas();
  }

  public DatabaseReadReplicaTopology readReplicaTopologyForDatabase(
      NamedDatabaseId namedDatabaseId) {
    return this.globalTopologyState.readReplicaTopologyForDatabase(namedDatabaseId);
  }

  public SocketAddress lookupCatchupAddress(MemberId upstream)
      throws CatchupAddressResolutionException {
    SocketAddress advertisedSocketAddress = this.globalTopologyState
        .retrieveCatchupServerAddress(upstream);
    if (advertisedSocketAddress == null) {
      throw new CatchupAddressResolutionException(upstream);
    } else {
      return advertisedSocketAddress;
    }
  }

  public RoleInfo lookupRole(NamedDatabaseId namedDatabaseId, MemberId memberId) {
    return this.globalTopologyState.role(namedDatabaseId, memberId);
  }

  public LeaderInfo getLeader(NamedDatabaseId namedDatabaseId) {
    return this.globalTopologyState.getLeader(namedDatabaseId);
  }

  public MemberId memberId() {
    return this.myself;
  }

  public DiscoveryDatabaseState lookupDatabaseState(NamedDatabaseId namedDatabaseId,
      MemberId memberId) {
    return this.globalTopologyState.stateFor(memberId, namedDatabaseId);
  }

  public Map<MemberId, DiscoveryDatabaseState> allCoreStatesForDatabase(
      NamedDatabaseId namedDatabaseId) {
    return Map
        .copyOf(this.globalTopologyState.coreStatesForDatabase(namedDatabaseId).memberStates());
  }

  public Map<MemberId, DiscoveryDatabaseState> allReadReplicaStatesForDatabase(
      NamedDatabaseId namedDatabaseId) {
    return Map.copyOf(
        this.globalTopologyState.readReplicaStatesForDatabase(namedDatabaseId).memberStates());
  }

  public boolean isHealthy() {
    return true;
  }

  @VisibleForTesting
  GlobalTopologyState topologyState() {
    return this.globalTopologyState;
  }
}
