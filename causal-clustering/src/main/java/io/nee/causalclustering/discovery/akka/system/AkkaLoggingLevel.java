/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.system;

import org.neo4j.logging.Level;

enum AkkaLoggingLevel {
  DEBUG(Level.DEBUG),
  INFO(Level.INFO),
  WARNING(Level.WARN),
  ERROR(Level.ERROR),
  OFF(Level.NONE);

  private final Level neo4jLevel;

  AkkaLoggingLevel(Level neo4jLevel) {
    this.neo4jLevel = neo4jLevel;
  }

  static AkkaLoggingLevel fromNeo4jLevel(Level neo4jLevel) {
    AkkaLoggingLevel[] n1 = values();
    int n2 = n1.length;

    for (int n3 = 0; n3 < n2; ++n3) {
      AkkaLoggingLevel akkaLevel = n1[n3];
      if (akkaLevel.neo4jLevel == neo4jLevel) {
        return akkaLevel;
      }
    }

    throw new IllegalArgumentException("Unknown Neo4j logging level: " + neo4jLevel);
  }

  public Level neo4jLevel() {
    return this.neo4jLevel;
  }
}
