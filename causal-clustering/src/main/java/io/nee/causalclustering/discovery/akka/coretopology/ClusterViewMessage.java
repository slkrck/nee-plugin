/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.coretopology;

import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.cluster.Member;
import akka.cluster.MemberStatus;
import akka.cluster.UniqueAddress;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.util.VisibleForTesting;

public class ClusterViewMessage {

  public static final ClusterViewMessage EMPTY = new ClusterViewMessage();
  private final boolean converged;
  private final SortedSet<Member> members;
  private final Set<Member> unreachable;

  private ClusterViewMessage() {
    this(false, Collections.unmodifiableSortedSet(new TreeSet(Member.ordering())),
        Collections.unmodifiableSet(new HashSet()));
  }

  public ClusterViewMessage(CurrentClusterState clusterState) {
    this(clusterState.leader().isDefined(), membersFrom(clusterState),
        clusterState.getUnreachable());
  }

  @VisibleForTesting
  ClusterViewMessage(boolean converged, SortedSet<Member> members, Set<Member> unreachable) {
    this.converged = converged;
    TreeSet<Member> upMembers = (TreeSet) members.stream().filter(this::memberIsUp)
        .collect(Collectors.toCollection(() ->
        {
          return new TreeSet(
              Member.ordering());
        }));
    this.members = Collections.unmodifiableSortedSet(upMembers);
    this.unreachable = Collections.unmodifiableSet(unreachable);
  }

  private static SortedSet<Member> membersFrom(CurrentClusterState clusterState) {
    TreeSet<Member> tempMembers = new TreeSet(Member.ordering());
    Iterable<Member> n10000 = clusterState.getMembers();
    Objects.requireNonNull(tempMembers);
    n10000.forEach(tempMembers::add);
    return tempMembers;
  }

  public boolean converged() {
    return this.converged;
  }

  public ClusterViewMessage withConverged(boolean converged) {
    return new ClusterViewMessage(converged, this.members, this.unreachable);
  }

  @VisibleForTesting
  public SortedSet<Member> members() {
    return this.members;
  }

  public ClusterViewMessage withMember(Member member) {
    TreeSet<Member> tempMembers = new TreeSet(Member.ordering());
    tempMembers.addAll(this.members);
    tempMembers.remove(member);
    tempMembers.add(member);
    Set<Member> tempUnreachable = new HashSet(this.unreachable);
    if (tempUnreachable.remove(member)) {
      tempUnreachable.add(member);
    }

    return new ClusterViewMessage(this.converged, tempMembers, tempUnreachable);
  }

  public ClusterViewMessage withoutMember(Member member) {
    TreeSet<Member> tempMembers = new TreeSet(Member.ordering());
    tempMembers.addAll(this.members);
    tempMembers.remove(member);
    Set<Member> tempUnreachable = new HashSet(this.unreachable);
    tempUnreachable.remove(member);
    return new ClusterViewMessage(this.converged, tempMembers, tempUnreachable);
  }

  public ClusterViewMessage withUnreachable(Member member) {
    Set<Member> tempUnreachable = new HashSet(this.unreachable);
    tempUnreachable.add(member);
    return new ClusterViewMessage(this.converged, this.members, tempUnreachable);
  }

  public ClusterViewMessage withoutUnreachable(Member member) {
    Set<Member> tempUnreachable = new HashSet(this.unreachable);
    tempUnreachable.remove(member);
    return new ClusterViewMessage(this.converged, this.members, tempUnreachable);
  }

  public Stream<UniqueAddress> availableMembers() {
    return this.members.stream().filter((member) ->
    {
      return !this.unreachable.contains(member);
    }).map(Member::uniqueAddress);
  }

  public Set<Member> unreachable() {
    return this.unreachable;
  }

  public boolean mostAreReachable() {
    int unreachableSize = this.unreachable.size();
    int reachableSize = this.members.size() - unreachableSize;
    return reachableSize > unreachableSize;
  }

  private boolean memberIsUp(Member m) {
    return MemberStatus.up().equals(m.status()) || MemberStatus.weaklyUp().equals(m.status());
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ClusterViewMessage that = (ClusterViewMessage) o;
      return this.converged == that.converged && Objects.equals(this.members, that.members)
          && Objects.equals(this.unreachable, that.unreachable);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.converged, this.members, this.unreachable);
  }

  public String toString() {
    return "ClusterViewMessage{converged=" + this.converged + ", members=" + this.members
        + ", unreachable=" + this.unreachable + "}";
  }
}
