/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.marshal;

import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.BooleanMarshal;
import io.nee.causalclustering.messaging.marshalling.StringMarshal;
import io.nee.dbms.EnterpriseOperatorState;
import io.nee.dbms.RemoteDatabaseManagementException;
import java.io.IOException;
import java.util.Optional;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class DiscoveryDatabaseStateMarshal extends SafeChannelMarshal<DiscoveryDatabaseState> {

  public static DiscoveryDatabaseStateMarshal INSTANCE = new DiscoveryDatabaseStateMarshal();

  private DiscoveryDatabaseStateMarshal() {
  }

  protected DiscoveryDatabaseState unmarshal0(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    DatabaseId databaseId = DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal(channel);
    EnterpriseOperatorState operatorState = this.getOperatorState(channel.getInt());
    boolean hasFailed = BooleanMarshal.unmarshal(channel);
    RemoteDatabaseManagementException failure;
    if (hasFailed) {
      String failureMessage = StringMarshal.unmarshal(channel);
      failure = new RemoteDatabaseManagementException(failureMessage);
    } else {
      failure = null;
    }

    return new DiscoveryDatabaseState(databaseId, operatorState, failure);
  }

  public void marshal(DiscoveryDatabaseState databaseState, WritableChannel channel)
      throws IOException {
    DatabaseIdWithoutNameMarshal.INSTANCE.marshal(databaseState.databaseId(), channel);
    channel.putInt(databaseState.operatorState().ordinal());
    BooleanMarshal.marshal(channel, databaseState.hasFailed());
    Optional<Throwable> failure = databaseState.failure();
    if (failure.isPresent()) {
      StringMarshal.marshal(channel, failure.get().getMessage());
    }
  }

  private EnterpriseOperatorState getOperatorState(int ordinal) {
    EnterpriseOperatorState[] operatorStates = EnterpriseOperatorState.values();
    return ordinal >= 0 && ordinal < operatorStates.length ? operatorStates[ordinal]
        : EnterpriseOperatorState.UNKNOWN;
  }
}
