/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import io.nee.causalclustering.core.consensus.LeaderInfo;
import io.nee.causalclustering.core.consensus.LeaderListener;
import io.nee.causalclustering.core.consensus.RaftMachine;
import io.nee.causalclustering.identity.MemberId;
import java.util.Set;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;

public class RaftCoreTopologyConnector extends LifecycleAdapter implements
    CoreTopologyService.Listener, LeaderListener {

  private final CoreTopologyService coreTopologyService;
  private final RaftMachine raftMachine;
  private final NamedDatabaseId namedDatabaseId;

  public RaftCoreTopologyConnector(CoreTopologyService coreTopologyService, RaftMachine raftMachine,
      NamedDatabaseId namedDatabaseId) {
    this.coreTopologyService = coreTopologyService;
    this.raftMachine = raftMachine;
    this.namedDatabaseId = namedDatabaseId;
  }

  public void start() {
    this.coreTopologyService.addLocalCoreTopologyListener(this);
    this.raftMachine.registerListener(this);
  }

  public void stop() {
    this.raftMachine.unregisterListener(this);
    this.coreTopologyService.removeLocalCoreTopologyListener(this);
  }

  public synchronized void onCoreTopologyChange(DatabaseCoreTopology coreTopology) {
    Set<MemberId> targetMembers = coreTopology.members().keySet();
    this.raftMachine.setTargetMembershipSet(targetMembers);
  }

  public void onLeaderSwitch(LeaderInfo leaderInfo) {
    this.coreTopologyService.setLeader(leaderInfo, this.namedDatabaseId);
  }

  public void onLeaderStepDown(long stepDownTerm) {
    this.coreTopologyService.handleStepDown(stepDownTerm, this.namedDatabaseId);
  }

  public NamedDatabaseId namedDatabaseId() {
    return this.namedDatabaseId;
  }
}
