/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.directory;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ddata.ORMap;
import akka.cluster.ddata.ORMapKey;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.javadsl.SourceQueueWithComplete;
import io.nee.causalclustering.core.consensus.LeaderInfo;
import io.nee.causalclustering.discovery.akka.BaseReplicatedDataActor;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.neo4j.kernel.database.DatabaseId;

public class DirectoryActor extends
    BaseReplicatedDataActor<ORMap<DatabaseId, ReplicatedLeaderInfo>> {

  public static final String NAME = "cc-directory-actor";
  private final SourceQueueWithComplete<Map<DatabaseId, LeaderInfo>> discoveryUpdateSink;
  private final ActorRef rrTopologyActor;

  private DirectoryActor(Cluster cluster, ActorRef replicator,
      SourceQueueWithComplete<Map<DatabaseId, LeaderInfo>> discoveryUpdateSink,
      ActorRef rrTopologyActor, ReplicatedDataMonitor monitor) {
    super(cluster, replicator, ORMapKey::create, ORMap::create, ReplicatedDataIdentifier.DIRECTORY,
        monitor);
    this.discoveryUpdateSink = discoveryUpdateSink;
    this.rrTopologyActor = rrTopologyActor;
  }

  public static Props props(Cluster cluster, ActorRef replicator,
      SourceQueueWithComplete<Map<DatabaseId, LeaderInfo>> discoveryUpdateSink,
      ActorRef rrTopologyActor, ReplicatedDataMonitor monitor) {
    return Props.create(DirectoryActor.class, () ->
    {
      return new DirectoryActor(cluster, replicator, discoveryUpdateSink, rrTopologyActor, monitor);
    });
  }

  protected void sendInitialDataToReplicator() {
  }

  protected void handleCustomEvents(ReceiveBuilder builder) {
    builder.match(LeaderInfoSettingMessage.class, (message) ->
    {
      this.modifyReplicatedData(this.key, (map) ->
      {
        return map
            .put(this.cluster, message.database(), new ReplicatedLeaderInfo(message.leaderInfo()));
      });
    });
  }

  protected void handleIncomingData(ORMap<DatabaseId, ReplicatedLeaderInfo> newData) {
    this.data = newData;
    Map<DatabaseId, LeaderInfo> leaderInfos = (this.data).getEntries().entrySet().stream()
        .collect(Collectors.toMap(Entry::getKey, (e) ->
        {
          return e.getValue().leaderInfo();
        }));
    this.discoveryUpdateSink.offer(leaderInfos);
    this.rrTopologyActor.tell(new LeaderInfoDirectoryMessage(leaderInfos), this.getSelf());
  }

  protected int dataMetricVisible() {
    return this.data.size();
  }

  protected int dataMetricInvisible() {
    return ((ORMap) this.data).keys().vvector().size();
  }
}
