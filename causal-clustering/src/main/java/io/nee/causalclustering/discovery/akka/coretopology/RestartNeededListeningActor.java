/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.coretopology;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.event.EventStream;
import akka.japi.pf.ReceiveBuilder;
import akka.remote.ThisActorSystemQuarantinedEvent;

//import akka.cluster.ClusterEvent.ClusterShuttingDown;

public class RestartNeededListeningActor extends AbstractLoggingActor {

  public static final String NAME = "cc-core-restart-needed-listener";
  private final Runnable restart;
  private final EventStream eventStream;
  private final Cluster cluster;

  private RestartNeededListeningActor(Runnable restart, EventStream eventStream, Cluster cluster) {
    this.restart = restart;
    this.eventStream = eventStream;
    this.cluster = cluster;
  }

  public static Props props(Runnable restart, EventStream eventStream, Cluster cluster) {
    return Props.create(RestartNeededListeningActor.class, () ->
    {
      return new RestartNeededListeningActor(restart, eventStream, cluster);
    });
  }

  public void preStart() {
    this.eventStream.subscribe(this.getSelf(), ThisActorSystemQuarantinedEvent.class);
    this.cluster.subscribe(this.getSelf(), ThisActorSystemQuarantinedEvent.class);
  }

  public void postStop() {
    this.unsubscribe();
  }

  private void unsubscribe() {
    this.eventStream.unsubscribe(this.getSelf(), ThisActorSystemQuarantinedEvent.class);
    this.cluster.unsubscribe(this.getSelf(), ThisActorSystemQuarantinedEvent.class);
  }

  public Receive createReceive() {
    return ReceiveBuilder.create().match(ThisActorSystemQuarantinedEvent.class, this::doRestart)
        .match(ThisActorSystemQuarantinedEvent.class, this::doRestart).
            match(CurrentClusterState.class, (ignore) ->
            {
            }).build();
  }

  private void doRestart(Object event) {
    this.log().info("Restart triggered by {}", event);
    this.restart.run();
    this.unsubscribe();
    this.getContext().become(this.createShuttingDownReceive());
  }

  private Receive createShuttingDownReceive() {
    return ReceiveBuilder.create().match(ThisActorSystemQuarantinedEvent.class, this::ignore)
        .match(ThisActorSystemQuarantinedEvent.class, this::ignore)
        .
            match(CurrentClusterState.class, (ignore) ->
            {
            }).build();
  }

  private void ignore(Object event) {
    this.log().debug("Ignoring as restart has been triggered: {}", event);
  }
}
