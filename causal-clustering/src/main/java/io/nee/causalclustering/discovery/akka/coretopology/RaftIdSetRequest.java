/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.coretopology;

import akka.actor.ActorRef;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import java.time.Duration;
import java.util.Objects;

public class RaftIdSetRequest {

  private final RaftId raftId;
  private final MemberId publisher;
  private final Duration timeout;
  private final ActorRef replyTo;

  public RaftIdSetRequest(RaftId raftId, MemberId publisher, Duration timeout) {
    this(raftId, publisher, timeout, ActorRef.noSender());
  }

  private RaftIdSetRequest(RaftId raftId, MemberId publisher, Duration timeout, ActorRef replyTo) {
    this.raftId = raftId;
    this.publisher = publisher;
    this.timeout = timeout;
    this.replyTo = replyTo;
  }

  RaftIdSetRequest withReplyTo(ActorRef replyTo) {
    return new RaftIdSetRequest(this.raftId, this.publisher, this.timeout, replyTo);
  }

  ActorRef replyTo() {
    return this.replyTo;
  }

  public RaftId raftId() {
    return this.raftId;
  }

  public MemberId publisher() {
    return this.publisher;
  }

  public Duration timeout() {
    return this.timeout;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      RaftIdSetRequest that = (RaftIdSetRequest) o;
      return Objects.equals(this.raftId, that.raftId) && Objects
          .equals(this.publisher, that.publisher);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.raftId, this.publisher);
  }

  public String toString() {
    return "RaftIdSetRequest{raftId=" + this.raftId + ", publisher=" + this.publisher + ", timeout="
        + this.timeout + ", replyTo=" + this.replyTo + "}";
  }
}
