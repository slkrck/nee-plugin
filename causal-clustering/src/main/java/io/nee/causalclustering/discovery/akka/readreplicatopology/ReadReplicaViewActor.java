/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.client.ClusterClientReceptionist;
import akka.japi.pf.ReceiveBuilder;
import io.nee.causalclustering.discovery.akka.AbstractActorWithTimersAndLogging;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

public class ReadReplicaViewActor extends AbstractActorWithTimersAndLogging {

  static final String READ_REPLICA_TOPIC = "rr-topic";
  static final int TICKS_BEFORE_REMOVE_READ_REPLICA = 3;
  private static final String TICK_KEY = "Tick key";
  private final ActorRef parent;
  private final ClusterClientReceptionist receptionist;
  private final Clock clock;
  private final Duration refresh;
  private final Map<ActorRef, ReadReplicaViewRecord> clusterClientReadReplicas = new HashMap();

  private ReadReplicaViewActor(ActorRef parent, ClusterClientReceptionist receptionist, Clock clock,
      Duration refresh) {
    this.parent = parent;
    this.receptionist = receptionist;
    this.clock = clock;
    this.refresh = refresh;
  }

  static Props props(ActorRef parent, ClusterClientReceptionist receptionist, Clock clock,
      Duration refresh) {
    return Props.create(ReadReplicaViewActor.class, () ->
    {
      return new ReadReplicaViewActor(parent, receptionist, clock, refresh);
    });
  }

  public void preStart() {
    this.receptionist.registerSubscriber("rr-topic", this.getSelf());
    this.getTimers()
        .startPeriodicTimer("Tick key", ReadReplicaViewActor.Tick.getInstance(), this.refresh);
  }

  public void postStop() {
    this.receptionist.unregisterSubscriber("rr-topic", this.getSelf());
  }

  public Receive createReceive() {
    return ReceiveBuilder.create()
        .match(ReadReplicaRefreshMessage.class, this::handleRefreshMessage)
        .match(ReadReplicaRemovalMessage.class,
            this::handleRemovalMessage)
        .match(ReadReplicaViewActor.Tick.class, this::handleTick).build();
  }

  private void handleRemovalMessage(ReadReplicaRemovalMessage msg) {
    ReadReplicaViewRecord removed = this.clusterClientReadReplicas.remove(msg.clusterClient());
    this.log().debug("Removed shut down read replica {} -> {}", msg.clusterClient(), removed);
    this.sendClusterView();
  }

  private void handleRefreshMessage(ReadReplicaRefreshMessage msg) {
    this.log().debug("Received {}", msg);
    this.clusterClientReadReplicas
        .put(msg.clusterClient(), new ReadReplicaViewRecord(msg, this.clock));
    this.sendClusterView();
  }

  private void handleTick(ReadReplicaViewActor.Tick tick) {
    Instant nTicksAgo = Instant.now(this.clock).minus(this.refresh.multipliedBy(3L));
    List<ActorRef> remove = this.clusterClientReadReplicas.entrySet().stream().filter((entry) ->
    {
      return entry.getValue()
          .timestamp().isBefore(nTicksAgo);
    }).peek((entry) ->
    {
      this.log()
          .debug("Removing {} after inactivity",
              entry);
    }).map(Entry::getKey)
        .collect(Collectors.toList());
    if (!remove.isEmpty()) {
      Map n10001 = this.clusterClientReadReplicas;
      Objects.requireNonNull(n10001);
      remove.forEach(n10001::remove);
      this.sendClusterView();
    }

    this.parent.tell(tick, this.getSelf());
  }

  private void sendClusterView() {
    this.parent.tell(new ReadReplicaViewMessage(this.clusterClientReadReplicas), this.getSelf());
  }

  static class Tick {

    private static final ReadReplicaViewActor.Tick instance = new ReadReplicaViewActor.Tick();

    private Tick() {
    }

    public static ReadReplicaViewActor.Tick getInstance() {
      return instance;
    }
  }
}
