/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.marshal;

import akka.remote.MessageSerializer.SerializationException;
import akka.serialization.JSerializer;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import io.nee.causalclustering.messaging.marshalling.InputStreamReadableChannel;
import io.nee.causalclustering.messaging.marshalling.OutputStreamWritableChannel;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public abstract class BaseAkkaSerializer<T> extends JSerializer {

  static final int LEADER_INFO = 1000;
  static final int RAFT_ID = 1001;
  static final int UNIQUE_ADDRESS = 1002;
  static final int CORE_SERVER_INFO_FOR_MEMBER_ID = 1003;
  static final int READ_REPLICA_INFO_FOR_MEMBER_ID = 1004;
  static final int MEMBER_ID = 1005;
  static final int READ_REPLICA_INFO = 1006;
  static final int CORE_TOPOLOGY = 1007;
  static final int READ_REPLICA_REMOVAL = 1008;
  static final int READ_REPLICA_TOPOLOGY = 1009;
  static final int DB_LEADER_INFO = 1010;
  static final int REPLICATED_LEADER_INFO = 1011;
  static final int DATABASE_ID = 1012;
  static final int REPLICATED_DATABASE_STATE = 1013;
  static final int DATABASE_TO_MEMBER = 1014;
  static final int DATABASE_STATE = 1015;
  private final ChannelMarshal<T> marshal;
  private final int id;
  private final int sizeHint;

  protected BaseAkkaSerializer(ChannelMarshal<T> marshal, int id, int sizeHint) {
    this.marshal = marshal;
    this.id = id;
    this.sizeHint = sizeHint;
  }

  public byte[] toBinary(Object o) {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(this.sizeHint);
    OutputStreamWritableChannel channel = new OutputStreamWritableChannel(outputStream);

    try {
      this.marshal.marshal((T) o, channel);
    } catch (IOException n5) {
      throw new SerializationException("Failed to serialize", n5);
    }

    return outputStream.toByteArray();
  }

  public Object fromBinaryJava(byte[] bytes, Class<?> manifest) {
    ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
    InputStreamReadableChannel channel = new InputStreamReadableChannel(inputStream);

    try {
      return this.marshal.unmarshal(channel);
    } catch (EndOfStreamException | IOException n6) {
      throw new SerializationException("Failed to deserialize", n6);
    }
  }

  int sizeHint() {
    return this.sizeHint;
  }

  public int identifier() {
    return this.id;
  }

  public boolean includeManifest() {
    return false;
  }
}
