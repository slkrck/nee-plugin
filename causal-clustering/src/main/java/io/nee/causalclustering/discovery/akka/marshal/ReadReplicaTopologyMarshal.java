/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.marshal;

import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.discovery.DatabaseReadReplicaTopology;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaTopologyMarshal extends SafeChannelMarshal<DatabaseReadReplicaTopology> {

  private final ChannelMarshal<ReadReplicaInfo> readReplicaInfoMarshal = new ReadReplicaInfoMarshal();
  private final ChannelMarshal<MemberId> memberIdMarshal = new MemberId.Marshal();

  protected DatabaseReadReplicaTopology unmarshal0(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    DatabaseId databaseId = DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal(channel);
    int size = channel.getInt();
    HashMap<MemberId, ReadReplicaInfo> replicas = new HashMap(size);

    for (int i = 0; i < size; ++i) {
      MemberId memberId = this.memberIdMarshal.unmarshal(channel);
      ReadReplicaInfo readReplicaInfo = this.readReplicaInfoMarshal.unmarshal(channel);
      replicas.put(memberId, readReplicaInfo);
    }

    return new DatabaseReadReplicaTopology(databaseId, replicas);
  }

  public void marshal(DatabaseReadReplicaTopology readReplicaTopology, WritableChannel channel)
      throws IOException {
    DatabaseIdWithoutNameMarshal.INSTANCE.marshal(readReplicaTopology.databaseId(), channel);
    channel.putInt(readReplicaTopology.members().size());
    Iterator n3 = readReplicaTopology.members().entrySet().iterator();

    while (n3.hasNext()) {
      Entry<MemberId, ReadReplicaInfo> entry = (Entry) n3.next();
      MemberId memberId = entry.getKey();
      ReadReplicaInfo readReplicaInfo = entry.getValue();
      this.memberIdMarshal.marshal(memberId, channel);
      this.readReplicaInfoMarshal.marshal(readReplicaInfo, channel);
    }
  }
}
