/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.coretopology;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ddata.LWWMap;
import akka.cluster.ddata.LWWMapKey;
import akka.cluster.ddata.LWWRegister;
import akka.cluster.ddata.LWWRegister.Clock;
import akka.cluster.ddata.Replicator.Get;
import akka.cluster.ddata.Replicator.GetSuccess;
import akka.cluster.ddata.Replicator.ReadConsistency;
import akka.cluster.ddata.Replicator.ReadFrom;
import akka.cluster.ddata.Replicator.UpdateFailure;
import akka.cluster.ddata.Replicator.UpdateSuccess;
import akka.japi.pf.ReceiveBuilder;
import io.nee.causalclustering.discovery.PublishRaftIdOutcome;
import io.nee.causalclustering.discovery.akka.BaseReplicatedDataActor;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import java.util.Objects;
import java.util.Optional;

public class RaftIdActor extends BaseReplicatedDataActor<LWWMap<RaftId, MemberId>> {

  private final ActorRef coreTopologyActor;
  private final Clock<MemberId> clock = LWWRegister.reverseClock();
  private final int minRuntimeQuorumSize;

  RaftIdActor(Cluster cluster, ActorRef replicator, ActorRef coreTopologyActor,
      ReplicatedDataMonitor monitors, int minRuntimeCores) {
    super(cluster, replicator, LWWMapKey::create, LWWMap::create, ReplicatedDataIdentifier.RAFT_ID,
        monitors);
    this.coreTopologyActor = coreTopologyActor;
    this.minRuntimeQuorumSize = minRuntimeCores / 2 + 1;
  }

  public static Props props(Cluster cluster, ActorRef replicator, ActorRef coreTopologyActor,
      ReplicatedDataMonitor monitors, int minRuntimeCores) {
    return Props.create(RaftIdActor.class, () ->
    {
      return new RaftIdActor(cluster, replicator, coreTopologyActor, monitors, minRuntimeCores);
    });
  }

  protected void sendInitialDataToReplicator() {
  }

  protected void handleCustomEvents(ReceiveBuilder builder) {
    builder.match(RaftIdSetRequest.class, this::setRaftId)
        .match(UpdateSuccess.class, this::handleUpdateSuccess).match(GetSuccess.class,
        this::validateRaftIdUpdate)
        .match(UpdateFailure.class, this::handleUpdateFailure);
  }

  private void setRaftId(RaftIdSetRequest message) {
    this.log().debug("Setting RaftId: {}", message);
    this.modifyReplicatedData(this.key, (map) ->
    {
      return map.put(this.cluster, message.raftId(), message.publisher(), this.clock);
    }, message.withReplyTo(this.getSender()));
  }

  private void handleUpdateSuccess(UpdateSuccess<?> updateSuccess) {
    updateSuccess.getRequest().filter((m) ->
    {
      return m instanceof RaftIdSetRequest;
    }).map((m) ->
    {
      return (RaftIdSetRequest) m;
    }).ifPresent((m) ->
    {
      ReadConsistency readConsistency = new ReadFrom(this.minRuntimeQuorumSize, m.timeout());
      Get<LWWMap<RaftId, MemberId>> getOp =
          new Get(this.key, readConsistency, Optional.of(m));
      this.replicator.tell(getOp, this.getSelf());
    });
  }

  private void validateRaftIdUpdate(GetSuccess<LWWMap<RaftId, MemberId>> getSuccess) {
    LWWMap<RaftId, MemberId> current = getSuccess.get(this.key);
    getSuccess.getRequest().filter((m) ->
    {
      return m instanceof RaftIdSetRequest;
    }).map((m) ->
    {
      return (RaftIdSetRequest) m;
    }).ifPresent((request) ->
    {
      MemberId successfulPublisher = current.getEntries().get(request.raftId());
      PublishRaftIdOutcome outcome;
      if (successfulPublisher == null) {
        outcome = PublishRaftIdOutcome.FAILED_PUBLISH;
      } else if (Objects.equals(successfulPublisher, request.publisher())) {
        outcome = PublishRaftIdOutcome.SUCCESSFUL_PUBLISH_BY_ME;
      } else {
        outcome = PublishRaftIdOutcome.SUCCESSFUL_PUBLISH_BY_OTHER;
      }

      request.replyTo().tell(outcome, this.getSelf());
    });
  }

  private void handleUpdateFailure(UpdateFailure<?> updateFailure) {
    updateFailure.getRequest().filter((m) ->
    {
      return m instanceof RaftIdSetRequest;
    }).map((m) ->
    {
      return (RaftIdSetRequest) m;
    }).ifPresent((request) ->
    {
      String message = String.format("Failed to set RaftId with request: %s", request);
      this.log().warning(message);
      request.replyTo().tell(PublishRaftIdOutcome.FAILED_PUBLISH, this.getSelf());
    });
  }

  protected void handleIncomingData(LWWMap<RaftId, MemberId> newData) {
    this.data = newData;
    this.coreTopologyActor
        .tell(new BootstrappedRaftsMessage(((LWWMap) this.data).getEntries().keySet()),
            this.getSelf());
  }

  protected int dataMetricVisible() {
    return this.data.size();
  }

  protected int dataMetricInvisible() {
    return ((LWWMap) this.data).underlying().keys().vvector().size();
  }
}
