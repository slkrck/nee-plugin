/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.marshal;

import io.nee.causalclustering.discovery.ClientConnectorAddresses;
import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.BooleanMarshal;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.IOException;
import java.util.Set;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class CoreServerInfoMarshal extends DiscoveryServerInfoMarshal<CoreServerInfo> {

  private final ChannelMarshal<ClientConnectorAddresses> clientConnectorAddressesMarshal = new ClientConnectorAddresses.Marshal();
  private final ChannelMarshal<SocketAddress> advertisedSocketAddressMarshal = new AdvertisedSocketAddressMarshal();

  protected CoreServerInfo unmarshal0(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    SocketAddress raftServer = this.advertisedSocketAddressMarshal.unmarshal(channel);
    SocketAddress catchupServer = this.advertisedSocketAddressMarshal.unmarshal(channel);
    ClientConnectorAddresses clientConnectorAddresses = this.clientConnectorAddressesMarshal
        .unmarshal(channel);
    Set<String> groups = unmarshalGroups(channel);
    Set<DatabaseId> databaseIds = unmarshalDatabaseIds(channel);
    boolean refuseToBeLeader = BooleanMarshal.unmarshal(channel);
    return new CoreServerInfo(raftServer, catchupServer, clientConnectorAddresses, groups,
        databaseIds, refuseToBeLeader);
  }

  public void marshal(CoreServerInfo coreServerInfo, WritableChannel channel) throws IOException {
    this.advertisedSocketAddressMarshal.marshal(coreServerInfo.getRaftServer(), channel);
    this.advertisedSocketAddressMarshal.marshal(coreServerInfo.catchupServer(), channel);
    this.clientConnectorAddressesMarshal.marshal(coreServerInfo.connectors(), channel);
    marshalGroups(coreServerInfo, channel);
    marshalDatabaseIds(coreServerInfo, channel);
    BooleanMarshal.marshal(channel, coreServerInfo.refusesToBeLeader());
  }
}
