/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.neo4j.ssl.SslPolicy;

public class DiscoverySSLContextFactory {

  public static final String PROTOCOL = "TLS";
  private final SslPolicy sslPolicy;

  public DiscoverySSLContextFactory(SslPolicy sslPolicy) {
    this.sslPolicy = sslPolicy;
  }

  public SSLContext sslContext() {
    try {
      SecurePassword securePassword = new SecurePassword(32, new SecureRandom());

      SSLContext n8;
      try {
        char[] password = securePassword.password();
        KeyManagerFactory keyManagerFactory = KeyManagerFactory
            .getInstance(KeyManagerFactory.getDefaultAlgorithm());
        KeyStore keyStore = this.sslPolicy.getKeyStore(password, password);
        keyManagerFactory.init(keyStore, password);
        SSLContext sslContext = SSLContext.getInstance("TLS");
        KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();
        TrustManager[] trustManagers = this.sslPolicy.getTrustManagerFactory().getTrustManagers();
        sslContext.init(keyManagers, trustManagers, null);
        n8 = sslContext;
      } catch (Throwable n10) {
        try {
          securePassword.close();
        } catch (Throwable n9) {
          n10.addSuppressed(n9);
        }

        throw n10;
      }

      securePassword.close();
      return n8;
    } catch (KeyManagementException | UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException n11) {
      throw new RuntimeException("Error creating SSL context", n11);
    }
  }
}
