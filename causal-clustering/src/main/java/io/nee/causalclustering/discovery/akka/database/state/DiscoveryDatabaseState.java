/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.database.state;

import io.nee.dbms.EnterpriseOperatorState;
import java.util.Objects;
import java.util.Optional;
import org.neo4j.dbms.DatabaseState;
import org.neo4j.dbms.OperatorState;
import org.neo4j.kernel.database.DatabaseId;

public class DiscoveryDatabaseState {

  private final DatabaseId databaseId;
  private final OperatorState operatorState;
  private final Throwable failure;

  public DiscoveryDatabaseState(DatabaseId databaseId, OperatorState operatorState) {
    this(databaseId, operatorState, null);
  }

  public DiscoveryDatabaseState(DatabaseId databaseId, OperatorState operatorState,
      Throwable failure) {
    this.databaseId = databaseId;
    this.operatorState = operatorState;
    this.failure = failure;
  }

  public static DiscoveryDatabaseState from(DatabaseState databaseState) {
    return new DiscoveryDatabaseState(databaseState.databaseId().databaseId(),
        databaseState.operatorState(),
        databaseState.failure().orElse(null));
  }

  public static DiscoveryDatabaseState unknown(DatabaseId id) {
    return new DiscoveryDatabaseState(id, EnterpriseOperatorState.UNKNOWN, null);
  }

  private static String printResult(Throwable failure) {
    return failure == null ? "SUCCESS" : "FAIL [" + failure + "]";
  }

  public DatabaseId databaseId() {
    return this.databaseId;
  }

  public OperatorState operatorState() {
    return this.operatorState;
  }

  public boolean hasFailed() {
    return this.failure != null;
  }

  public Optional<Throwable> failure() {
    return Optional.ofNullable(this.failure);
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      DiscoveryDatabaseState that = (DiscoveryDatabaseState) o;
      return Objects.equals(this.databaseId, that.databaseId) && Objects
          .equals(this.operatorState, that.operatorState) &&
          Objects.equals(this.failure, that.failure);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.databaseId, this.operatorState, this.failure);
  }

  public String toString() {
    DatabaseId n10000 = this.databaseId;
    return "DiscoveryDatabaseState{" + n10000 + ", operatorState=" + this.operatorState
        + ", result=" + printResult(this.failure) + "}";
  }
}
