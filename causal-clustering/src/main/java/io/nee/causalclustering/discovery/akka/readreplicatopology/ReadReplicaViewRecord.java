/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.ActorRef;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import io.nee.causalclustering.identity.MemberId;
import java.time.Clock;
import java.time.Instant;
import java.util.Map;
import java.util.Objects;
import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaViewRecord {

  private final ReadReplicaInfo readReplicaInfo;
  private final Instant timestamp;
  private final ActorRef topologyClientActorRef;
  private final MemberId memberId;
  private final Map<DatabaseId, DiscoveryDatabaseState> databaseStates;

  ReadReplicaViewRecord(ReadReplicaInfo readReplicaInfo, ActorRef topologyClientActorRef,
      MemberId memberId, Instant timestamp,
      Map<DatabaseId, DiscoveryDatabaseState> databaseStates) {
    this.readReplicaInfo = readReplicaInfo;
    this.timestamp = timestamp;
    this.topologyClientActorRef = topologyClientActorRef;
    this.memberId = memberId;
    this.databaseStates = databaseStates;
  }

  ReadReplicaViewRecord(ReadReplicaRefreshMessage message, Clock clock) {
    this(message.readReplicaInfo(), message.topologyClientActorRef(), message.memberId(),
        Instant.now(clock), message.databaseStates());
  }

  public ReadReplicaInfo readReplicaInfo() {
    return this.readReplicaInfo;
  }

  public Instant timestamp() {
    return this.timestamp;
  }

  public ActorRef topologyClientActorRef() {
    return this.topologyClientActorRef;
  }

  public MemberId memberId() {
    return this.memberId;
  }

  Map<DatabaseId, DiscoveryDatabaseState> databaseStates() {
    return this.databaseStates;
  }

  public String toString() {
    return "ReadReplicaViewRecord{readReplicaInfo=" + this.readReplicaInfo + ", timestamp="
        + this.timestamp + ", topologyClientActorRef=" +
        this.topologyClientActorRef + ", memberId=" + this.memberId + ", databaseState="
        + this.databaseStates + "}";
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ReadReplicaViewRecord that = (ReadReplicaViewRecord) o;
      return Objects.equals(this.readReplicaInfo, that.readReplicaInfo) && Objects
          .equals(this.timestamp, that.timestamp) &&
          Objects.equals(this.topologyClientActorRef, that.topologyClientActorRef) && Objects
          .equals(this.memberId, that.memberId) &&
          Objects.equals(this.databaseStates, that.databaseStates);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects
        .hash(this.readReplicaInfo, this.timestamp, this.topologyClientActorRef, this.memberId,
            this.databaseStates);
  }
}
