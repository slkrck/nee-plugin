/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import akka.remote.artery.tcp.SSLEngineProvider;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLSession;
import org.neo4j.configuration.ssl.ClientAuth;
import org.neo4j.ssl.ClientSideHostnameVerificationEngineModification;
import org.neo4j.ssl.EssentialEngineModifications;
import org.neo4j.ssl.SslPolicy;
import scala.Option;

public class AkkaDiscoverySSLEngineProvider implements SSLEngineProvider {

  private final SslPolicy sslPolicy;
  private final SSLContext sslContext;

  public AkkaDiscoverySSLEngineProvider(SslPolicy sslPolicy) {
    this.sslPolicy = sslPolicy;
    this.sslContext = (new DiscoverySSLContextFactory(this.sslPolicy)).sslContext();
  }

  public SSLEngine createServerSSLEngine(String hostname, int port) {
    SSLEngine sslEngine = this.createSSLEngine(false, hostname, port);
    sslEngine.setWantClientAuth(ClientAuth.OPTIONAL.equals(this.sslPolicy.getClientAuth()));
    sslEngine.setNeedClientAuth(ClientAuth.REQUIRE.equals(this.sslPolicy.getClientAuth()));
    return sslEngine;
  }

  public SSLEngine createClientSSLEngine(String hostname, int port) {
    SSLEngine sslEngine = this.createSSLEngine(true, hostname, port);
    if (this.sslPolicy.isVerifyHostname()) {
      sslEngine = (new ClientSideHostnameVerificationEngineModification()).apply(sslEngine);
    }

    return sslEngine;
  }

  private SSLEngine createSSLEngine(boolean isClient, String hostname, int port) {
    SSLEngine sslEngine = this.sslContext.createSSLEngine(hostname, port);
    sslEngine = (new EssentialEngineModifications(this.sslPolicy.getTlsVersions(), isClient))
        .apply(sslEngine);
    if (this.sslPolicy.getCipherSuites() != null) {
      sslEngine.setEnabledCipherSuites(this.sslPolicy.getCipherSuites().toArray(new String[0]));
    }

    return sslEngine;
  }

  public Option<Throwable> verifyClientSession(String hostname, SSLSession session) {
    return Option.apply(null);
  }

  public Option<Throwable> verifyServerSession(String hostname, SSLSession session) {
    return Option.apply(null);
  }
}
