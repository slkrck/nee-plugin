/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import java.net.InetAddress;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;

public class DnsHostnameResolver extends RetryingHostnameResolver {

  private final Log userLog;
  private final Log log;
  private final DomainNameResolver domainNameResolver;

  DnsHostnameResolver(LogService logService, DomainNameResolver domainNameResolver, Config config,
      RetryStrategy retryStrategy) {
    super(config, retryStrategy);
    this.log = logService.getInternalLog(this.getClass());
    this.userLog = logService.getUserLog(this.getClass());
    this.domainNameResolver = domainNameResolver;
  }

  public static RemoteMembersResolver resolver(LogService logService,
      DomainNameResolver domainNameResolver, Config config) {
    DnsHostnameResolver hostnameResolver = new DnsHostnameResolver(logService, domainNameResolver,
        config, defaultRetryStrategy(config));
    return new InitialDiscoveryMembersResolver(hostnameResolver, config);
  }

  protected Collection<SocketAddress> resolveOnce(SocketAddress initialAddress) {
    Set<SocketAddress> addresses = new HashSet();
    InetAddress[] ipAddresses = this.domainNameResolver
        .resolveDomainName(initialAddress.getHostname());
    if (ipAddresses.length == 0) {
      this.log.error("Failed to resolve host '%s'", initialAddress.getHostname());
    }

    InetAddress[] n4 = ipAddresses;
    int n5 = ipAddresses.length;

    for (int n6 = 0; n6 < n5; ++n6) {
      InetAddress ipAddress = n4[n6];
      addresses.add(new SocketAddress(ipAddress.getHostAddress(), initialAddress.getPort()));
    }

    this.userLog.info("Resolved initial host '%s' to %s", initialAddress, addresses);
    return addresses;
  }
}
