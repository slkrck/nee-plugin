/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.coretopology;

import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.identity.MemberId;
import java.util.Objects;

public class CoreServerInfoForMemberId {

  private final MemberId memberId;
  private final CoreServerInfo coreServerInfo;

  public CoreServerInfoForMemberId(MemberId memberId, CoreServerInfo coreServerInfo) {
    this.memberId = memberId;
    this.coreServerInfo = coreServerInfo;
  }

  public MemberId memberId() {
    return this.memberId;
  }

  public CoreServerInfo coreServerInfo() {
    return this.coreServerInfo;
  }

  public String toString() {
    return "CoreServerInfoForMemberId{memberId=" + this.memberId + ", coreServerInfo="
        + this.coreServerInfo + "}";
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      CoreServerInfoForMemberId that = (CoreServerInfoForMemberId) o;
      return Objects.equals(this.memberId, that.memberId) && Objects
          .equals(this.coreServerInfo, that.coreServerInfo);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.memberId, this.coreServerInfo);
  }
}
