/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.system;

import akka.actor.Address;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.japi.pf.ReceiveBuilder;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.discovery.RemoteMembersResolver;
import io.nee.causalclustering.discovery.akka.AbstractActorWithTimersAndLogging;
import java.time.Duration;
import java.util.ArrayList;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;

public class ClusterJoiningActor extends AbstractActorWithTimersAndLogging {

  public static final String NAME = "joiner";
  static final String AKKA_SCHEME = "akka";
  private static final String TIMER = "join timer";
  private final Cluster cluster;
  private final RemoteMembersResolver remoteMembersResolver;
  private final Duration retry;

  private ClusterJoiningActor(Cluster cluster, RemoteMembersResolver remoteMembersResolver,
      Config config) {
    this.cluster = cluster;
    this.remoteMembersResolver = remoteMembersResolver;
    this.retry = config.get(CausalClusteringSettings.cluster_binding_retry_timeout);
  }

  public static Props props(Cluster cluster, RemoteMembersResolver resolver, Config config) {
    return Props.create(ClusterJoiningActor.class, () ->
    {
      return new ClusterJoiningActor(cluster, resolver, config);
    });
  }

  public void preStart() {
    this.cluster.registerOnMemberUp(() ->
    {
      this.log().debug("Join successful, exiting");
      this.getContext().stop(this.getSelf());
    });
  }

  public Receive createReceive() {
    return ReceiveBuilder.create().match(JoinMessage.class, this::join).build();
  }

  private void join(JoinMessage message) {
    this.log().debug("Processing: {}", message);
    ArrayList seedNodes;
    if (!message.isReJoin()) {
      seedNodes = this.resolve();
      this.log().info("Joining seed nodes: {}", seedNodes);
      this.cluster.joinSeedNodes(seedNodes);
      this.startTimer(message);
    } else if (!message.hasAddress()) {
      seedNodes = this.resolve();
      this.getSelf().tell(JoinMessage.initial(message.isReJoin(), seedNodes), this.getSelf());
    } else if (message.head().equals(this.cluster.selfAddress())) {
      this.log().info("Not joining to self. Retrying next.");
      this.getSelf().tell(message.tailMsg(), this.getSelf());
    } else {
      Address address = message.head();
      this.log().info("Attempting to join: {}", address);
      this.cluster.join(address);
      this.startTimer(message.tailMsg());
    }
  }

  private ArrayList<Address> resolve() {
    return this.remoteMembersResolver.resolve(this::toAkkaAddress, ArrayList::new);
  }

  private void startTimer(JoinMessage message) {
    this.getTimers().startSingleTimer("join timer", message, this.retry);
  }

  private Address toAkkaAddress(SocketAddress resolvedAddress) {
    return new Address("akka", this.getContext().getSystem().name(),
        TypesafeConfigService.hostname(resolvedAddress), resolvedAddress.getPort());
  }
}
