/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka;

import akka.remote.artery.tcp.SSLEngineProvider;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.discovery.AkkaDiscoverySSLEngineProvider;
import io.nee.causalclustering.discovery.DiscoveryServiceFactory;
import io.nee.causalclustering.discovery.RemoteMembersResolver;
import io.nee.causalclustering.discovery.RetryStrategy;
import io.nee.causalclustering.discovery.akka.system.ActorSystemFactory;
import io.nee.causalclustering.discovery.akka.system.ActorSystemLifecycle;
import io.nee.causalclustering.discovery.akka.system.JoinMessageFactory;
import io.nee.causalclustering.discovery.member.DiscoveryMemberFactory;
import io.nee.causalclustering.identity.MemberId;
import java.time.Clock;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.ssl.SslPolicyScope;
import org.neo4j.internal.helpers.ExponentialBackoffStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.ssl.SslPolicy;
import org.neo4j.ssl.config.SslPolicyLoader;

public class AkkaDiscoveryServiceFactory implements DiscoveryServiceFactory {

  private static final long RESTART_RETRY_DELAY_MS = 1000L;
  private static final long RESTART_RETRY_DELAY_MAX_MS = 60000L;
  private static final int RESTART_FAILURES_BEFORE_UNHEALTHY = 8;

  protected static ActorSystemFactory actorSystemFactory(SslPolicyLoader sslPolicyLoader,
      Executor executor, Config config, LogProvider logProvider) {
    SslPolicy sslPolicy =
        sslPolicyLoader.hasPolicyForSource(SslPolicyScope.CLUSTER) ? sslPolicyLoader
            .getPolicy(SslPolicyScope.CLUSTER) : null;
    Optional<SSLEngineProvider> sslEngineProvider = Optional.ofNullable(sslPolicy)
        .map(AkkaDiscoverySSLEngineProvider::new);
    return new ActorSystemFactory(sslEngineProvider, executor, config, logProvider);
  }

  private static Executor executorService(Config config, JobScheduler jobScheduler) {
    int parallelism = config
        .get(CausalClusteringSettings.middleware_akka_default_parallelism_level);
    jobScheduler.setParallelism(Group.AKKA_TOPOLOGY_WORKER, parallelism);
    return jobScheduler.executor(Group.AKKA_TOPOLOGY_WORKER);
  }

  public final AkkaCoreTopologyService coreTopologyService(Config config, MemberId myself,
      JobScheduler jobScheduler, LogProvider logProvider,
      LogProvider userLogProvider, RemoteMembersResolver remoteMembersResolver,
      RetryStrategy catchupAddressRetryStrategy,
      SslPolicyLoader sslPolicyLoader, DiscoveryMemberFactory discoveryMemberFactory,
      Monitors monitors,
      Clock clock) {
    Executor executor = executorService(config, jobScheduler);
    TimeoutStrategy timeoutStrategy = new ExponentialBackoffStrategy(1000L, 60000L,
        TimeUnit.MILLISECONDS);
    Restarter restarter = new Restarter(timeoutStrategy, 8);
    return new AkkaCoreTopologyService(config, myself,
        this.actorSystemLifecycle(config, executor, logProvider, remoteMembersResolver,
            sslPolicyLoader),
        logProvider, userLogProvider, catchupAddressRetryStrategy, restarter,
        discoveryMemberFactory, executor, clock,
        monitors);
  }

  public final AkkaTopologyClient readReplicaTopologyService(Config config, LogProvider logProvider,
      JobScheduler jobScheduler, MemberId myself,
      RemoteMembersResolver remoteMembersResolver, SslPolicyLoader sslPolicyLoader,
      DiscoveryMemberFactory discoveryMemberFactory, Clock clock) {
    return new AkkaTopologyClient(config, logProvider, myself,
        this.actorSystemLifecycle(config, executorService(config, jobScheduler), logProvider,
            remoteMembersResolver,
            sslPolicyLoader),
        discoveryMemberFactory, clock);
  }

  protected ActorSystemLifecycle actorSystemLifecycle(Config config, Executor executor,
      LogProvider logProvider, RemoteMembersResolver resolver,
      SslPolicyLoader sslPolicyLoader) {
    return new ActorSystemLifecycle(
        actorSystemFactory(sslPolicyLoader, executor, config, logProvider), resolver,
        new JoinMessageFactory(resolver),
        config, logProvider);
  }
}
