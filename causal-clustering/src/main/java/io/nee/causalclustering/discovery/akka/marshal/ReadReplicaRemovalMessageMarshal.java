/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.marshal;

import akka.actor.ActorRef;
import akka.actor.ExtendedActorSystem;
import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.discovery.akka.readreplicatopology.ReadReplicaRemovalMessage;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.IOException;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;

public class ReadReplicaRemovalMessageMarshal extends
    SafeChannelMarshal<ReadReplicaRemovalMessage> {

  private final ChannelMarshal<ActorRef> actorRefMarshal;

  public ReadReplicaRemovalMessageMarshal(ExtendedActorSystem system) {
    this.actorRefMarshal = new ActorRefMarshal(system);
  }

  protected ReadReplicaRemovalMessage unmarshal0(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    return new ReadReplicaRemovalMessage(this.actorRefMarshal.unmarshal(channel));
  }

  public void marshal(ReadReplicaRemovalMessage readReplicaRemovalMessage, WritableChannel channel)
      throws IOException {
    this.actorRefMarshal.marshal(readReplicaRemovalMessage.clusterClient(), channel);
  }
}
