/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import io.nee.causalclustering.core.CausalClusteringSettings;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.DatabaseId;

public class CoreServerInfo implements DiscoveryServerInfo {

  private final SocketAddress raftServer;
  private final SocketAddress catchupServer;
  private final ClientConnectorAddresses clientConnectorAddresses;
  private final Set<String> groups;
  private final Set<DatabaseId> startedDatabaseIds;
  private final boolean refuseToBeLeader;

  public CoreServerInfo(SocketAddress raftServer, SocketAddress catchupServer,
      ClientConnectorAddresses clientConnectorAddresses, Set<String> groups,
      Set<DatabaseId> startedDatabaseIds, boolean refuseToBeLeader) {
    this.raftServer = raftServer;
    this.catchupServer = catchupServer;
    this.clientConnectorAddresses = clientConnectorAddresses;
    this.groups = groups;
    this.startedDatabaseIds = startedDatabaseIds;
    this.refuseToBeLeader = refuseToBeLeader;
  }

  public static CoreServerInfo fromRaw(Config config, Set<DatabaseId> databaseIds) {
    SocketAddress raftAddress = config.get(CausalClusteringSettings.raft_advertised_address);
    SocketAddress catchupAddress = config
        .get(CausalClusteringSettings.transaction_advertised_address);
    ClientConnectorAddresses connectorUris = ClientConnectorAddresses.extractFromConfig(config);
    Set<String> groups = Set
        .copyOf((Collection) config.get(CausalClusteringSettings.server_groups));
    Boolean refuseToBeLeader = config.get(CausalClusteringSettings.refuse_to_be_leader);
    return new CoreServerInfo(raftAddress, catchupAddress, connectorUris, groups, databaseIds,
        refuseToBeLeader);
  }

  public static CoreServerInfo from(Config config, Set<DatabaseId> databaseIds) {
    return fromRaw(config, databaseIds);
  }

  public Set<DatabaseId> startedDatabaseIds() {
    return this.startedDatabaseIds;
  }

  public SocketAddress getRaftServer() {
    return this.raftServer;
  }

  public SocketAddress catchupServer() {
    return this.catchupServer;
  }

  public ClientConnectorAddresses connectors() {
    return this.clientConnectorAddresses;
  }

  public Set<String> groups() {
    return this.groups;
  }

  public boolean refusesToBeLeader() {
    return this.refuseToBeLeader;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      CoreServerInfo that = (CoreServerInfo) o;
      return this.refuseToBeLeader == that.refuseToBeLeader && Objects
          .equals(this.raftServer, that.raftServer) &&
          Objects.equals(this.catchupServer, that.catchupServer) &&
          Objects.equals(this.clientConnectorAddresses, that.clientConnectorAddresses) && Objects
          .equals(this.groups, that.groups) &&
          Objects.equals(this.startedDatabaseIds, that.startedDatabaseIds);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(
        this.raftServer, this.catchupServer, this.clientConnectorAddresses, this.groups,
        this.startedDatabaseIds, this.refuseToBeLeader);
  }

  public String toString() {
    return "CoreServerInfo{raftServer=" + this.raftServer + ", catchupServer=" + this.catchupServer
        + ", clientConnectorAddresses=" +
        this.clientConnectorAddresses + ", groups=" + this.groups + ", startedDatabaseIds="
        + this.startedDatabaseIds + ", refuseToBeLeader=" +
        this.refuseToBeLeader + "}";
  }
}
