/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.kubernetes;

import java.util.List;

public class ServiceList extends KubernetesType {

  private List<ServiceList.Service> items;

  public List<ServiceList.Service> items() {
    return this.items;
  }

  public void setItems(List<ServiceList.Service> items) {
    this.items = items;
  }

  public <T> T handle(KubernetesType.Visitor<T> visitor) {
    return visitor.visit(this);
  }

  public static class Service {

    private ObjectMetadata metadata;
    private ServiceList.Service.ServiceSpec spec;

    public ObjectMetadata metadata() {
      return this.metadata;
    }

    public ServiceList.Service.ServiceSpec spec() {
      return this.spec;
    }

    public void setMetadata(ObjectMetadata metadata) {
      this.metadata = metadata;
    }

    public void setSpec(ServiceList.Service.ServiceSpec spec) {
      this.spec = spec;
    }

    public static class ServiceSpec {

      private List<ServiceList.Service.ServiceSpec.ServicePort> ports;

      public List<ServiceList.Service.ServiceSpec.ServicePort> ports() {
        return this.ports;
      }

      public void setPorts(List<ServiceList.Service.ServiceSpec.ServicePort> ports) {
        this.ports = ports;
      }

      public static class ServicePort {

        private String name;
        private int port;
        private String protocol;

        public String name() {
          return this.name;
        }

        public int port() {
          return this.port;
        }

        public String protocol() {
          return this.protocol;
        }

        public void setName(String name) {
          this.name = name;
        }

        public void setPort(int port) {
          this.port = port;
        }

        public void setProtocol(String protocol) {
          this.protocol = protocol;
        }
      }
    }
  }
}
