/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.system;

import akka.actor.AbstractActor;
import akka.actor.ActorSystem;
import akka.dispatch.RequiresMessageQueue;
import akka.event.LoggerMessageQueueSemantics;
import akka.event.Logging;
import akka.event.Logging.Debug;
import akka.event.Logging.Info;
import akka.event.Logging.InitializeLogger;
import akka.event.Logging.LogEvent;
import akka.event.Logging.Warning;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;

public class LoggingActor extends AbstractActor implements
    RequiresMessageQueue<LoggerMessageQueueSemantics> {

  private static final Map<ActorSystem, LogProvider> LOG_PROVIDERS = new HashMap();
  private static LogProvider defaultLogProvider = NullLogProvider.getInstance();

  static void enable(ActorSystem system, LogProvider logProvider) {
    LOG_PROVIDERS.put(system, logProvider);
    logProvider.getLog(LoggingActor.class)
        .debug("Added logProvider for %s. %d LogProviders and ActorSystems remaining.",
            system.name(), LOG_PROVIDERS.size());
  }

  static void enable(LogProvider logProvider) {
    defaultLogProvider = logProvider;
  }

  static void disable(ActorSystem system) {
    Optional<LogProvider> removed = Optional.ofNullable(LOG_PROVIDERS.remove(system));
    removed.ifPresent((log) ->
    {
      log.getLog(LoggingActor.class)
          .debug("Removed logProvider for %s. %d LogProviders and ActorSystems remaining.",
              system.name(), LOG_PROVIDERS.size());
    });
  }

  public Receive createReceive() {
    return this.receiveBuilder().match(Error.class, (error) ->
    {
      this.getLog(error.logClass()).error(this.getMessage(error), error.cause());
    }).match(Warning.class, (warning) ->
    {
      this.getLog(warning.logClass()).warn(this.getMessage(warning));
    }).match(Info.class, (info) ->
    {
      this.getLog(info.logClass()).info(this.getMessage(info));
    }).match(Debug.class, (debug) ->
    {
      this.getLog(debug.logClass()).debug(this.getMessage(debug));
    }).match(InitializeLogger.class, (ignored) ->
    {
      this.sender().tell(Logging.loggerInitialized(), this.self());
    }).build();
  }

  private Log getLog(Class loggingClass) {
    LogProvider configuredLogProvider = LOG_PROVIDERS.get(this.getContext().system());
    LogProvider logProvider =
        configuredLogProvider == null ? defaultLogProvider : configuredLogProvider;
    return logProvider.getLog(loggingClass);
  }

  private String getMessage(LogEvent error) {
    return error.message() == null ? "null" : error.message().toString();
  }
}
