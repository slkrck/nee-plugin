/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.system;

import akka.actor.Address;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

class JoinMessage {

  private final boolean isReJoin;
  private final List<Address> addresses;

  private JoinMessage(boolean isReJoin, List<Address> addresses) {
    this.isReJoin = isReJoin;
    this.addresses = Collections.unmodifiableList(addresses);
  }

  static JoinMessage initial(boolean isReJoin, Collection<Address> addresses) {
    return new JoinMessage(isReJoin, new ArrayList(addresses));
  }

  JoinMessage tailMsg() {
    return new JoinMessage(this.isReJoin, this.addresses.subList(1, this.addresses.size()));
  }

  boolean hasAddress() {
    return !this.addresses.isEmpty();
  }

  Address head() {
    return this.addresses.get(0);
  }

  boolean isReJoin() {
    return this.isReJoin;
  }

  public String toString() {
    return "JoinMessage{isReJoin=" + this.isReJoin + ", addresses=" + this.addresses + "}";
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      JoinMessage that = (JoinMessage) o;
      return this.isReJoin == that.isReJoin && Objects.equals(this.addresses, that.addresses);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.isReJoin, this.addresses);
  }
}
