/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.client.ClusterClientReceptionist;
import akka.event.EventStream;
import akka.pattern.AskTimeoutException;
import akka.pattern.Patterns;
import akka.stream.javadsl.SourceQueueWithComplete;
import io.nee.causalclustering.catchup.CatchupAddressResolutionException;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.core.consensus.LeaderInfo;
import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.discovery.CoreTopologyListenerService;
import io.nee.causalclustering.discovery.CoreTopologyService;
import io.nee.causalclustering.discovery.DatabaseCoreTopology;
import io.nee.causalclustering.discovery.DatabaseReadReplicaTopology;
import io.nee.causalclustering.discovery.PublishRaftIdOutcome;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.ReplicatedDatabaseState;
import io.nee.causalclustering.discovery.RetryStrategy;
import io.nee.causalclustering.discovery.RoleInfo;
import io.nee.causalclustering.discovery.akka.common.DatabaseStartedMessage;
import io.nee.causalclustering.discovery.akka.common.DatabaseStoppedMessage;
import io.nee.causalclustering.discovery.akka.coretopology.BootstrapState;
import io.nee.causalclustering.discovery.akka.coretopology.CoreTopologyActor;
import io.nee.causalclustering.discovery.akka.coretopology.CoreTopologyMessage;
import io.nee.causalclustering.discovery.akka.coretopology.RaftIdSetRequest;
import io.nee.causalclustering.discovery.akka.coretopology.RestartNeededListeningActor;
import io.nee.causalclustering.discovery.akka.coretopology.TopologyBuilder;
import io.nee.causalclustering.discovery.akka.database.state.DatabaseStateActor;
import io.nee.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import io.nee.causalclustering.discovery.akka.directory.DirectoryActor;
import io.nee.causalclustering.discovery.akka.directory.LeaderInfoSettingMessage;
import io.nee.causalclustering.discovery.akka.monitoring.ClusterSizeMonitor;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;
import io.nee.causalclustering.discovery.akka.readreplicatopology.ReadReplicaTopologyActor;
import io.nee.causalclustering.discovery.akka.system.ActorSystemLifecycle;
import io.nee.causalclustering.discovery.member.DiscoveryMember;
import io.nee.causalclustering.discovery.member.DiscoveryMemberFactory;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import java.time.Clock;
import java.time.Duration;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeoutException;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.dbms.DatabaseState;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.SafeLifecycle;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.util.VisibleForTesting;

//import org.neo4j.kernel.lifecycle.SafeLifecycle.State;

public class AkkaCoreTopologyService extends SafeLifecycle implements CoreTopologyService {

  private final ActorSystemLifecycle actorSystemLifecycle;
  private final LogProvider logProvider;
  private final RetryStrategy catchupAddressRetryStrategy;
  private final Restarter restarter;
  private final DiscoveryMemberFactory discoveryMemberFactory;
  private final Executor executor;
  private final Clock clock;
  private final ReplicatedDataMonitor replicatedDataMonitor;
  private final ClusterSizeMonitor clusterSizeMonitor;
  private final Config config;
  private final MemberId myself;
  private final Log log;
  private final Log userLog;
  private final CoreTopologyListenerService listenerService = new CoreTopologyListenerService();
  private final Map<NamedDatabaseId, LeaderInfo> localLeadersByDatabaseId = new ConcurrentHashMap();
  private volatile ActorRef coreTopologyActorRef;
  private volatile ActorRef directoryActorRef;
  private volatile ActorRef databaseStateActorRef;
  private volatile GlobalTopologyState globalTopologyState;

  public AkkaCoreTopologyService(Config config, MemberId myself,
      ActorSystemLifecycle actorSystemLifecycle, LogProvider logProvider,
      LogProvider userLogProvider, RetryStrategy catchupAddressRetryStrategy, Restarter restarter,
      DiscoveryMemberFactory discoveryMemberFactory,
      Executor executor, Clock clock, Monitors monitors) {
    this.actorSystemLifecycle = actorSystemLifecycle;
    this.logProvider = logProvider;
    this.catchupAddressRetryStrategy = catchupAddressRetryStrategy;
    this.restarter = restarter;
    this.discoveryMemberFactory = discoveryMemberFactory;
    this.executor = executor;
    this.clock = clock;
    this.config = config;
    this.myself = myself;
    this.log = logProvider.getLog(this.getClass());
    this.userLog = userLogProvider.getLog(this.getClass());
    this.replicatedDataMonitor = monitors.newMonitor(ReplicatedDataMonitor.class, new String[0]);
    this.globalTopologyState = this.newGlobalTopologyState(logProvider, this.listenerService);
    this.clusterSizeMonitor = monitors.newMonitor(ClusterSizeMonitor.class, new String[0]);
  }

  public void start0() {
    this.actorSystemLifecycle.createClusterActorSystem();
    SourceQueueWithComplete<CoreTopologyMessage> coreTopologySink = this.actorSystemLifecycle
        .queueMostRecent(this::onCoreTopologyMessage);
    ActorSystemLifecycle n10000 = this.actorSystemLifecycle;
    GlobalTopologyState n10001 = this.globalTopologyState;
    Objects.requireNonNull(n10001);
    SourceQueueWithComplete<DatabaseReadReplicaTopology> rrTopologySink = n10000
        .queueMostRecent(n10001::onTopologyUpdate);
    n10000 = this.actorSystemLifecycle;
    n10001 = this.globalTopologyState;
    Objects.requireNonNull(n10001);
    SourceQueueWithComplete<Map<DatabaseId, LeaderInfo>> directorySink = n10000
        .queueMostRecent(n10001::onDbLeaderUpdate);
    n10000 = this.actorSystemLifecycle;
    n10001 = this.globalTopologyState;
    Objects.requireNonNull(n10001);
    SourceQueueWithComplete<BootstrapState> bootstrapStateSink = n10000
        .queueMostRecent(n10001::onBootstrapStateUpdate);
    n10000 = this.actorSystemLifecycle;
    n10001 = this.globalTopologyState;
    Objects.requireNonNull(n10001);
    SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink = n10000
        .queueMostRecent(n10001::onDbStateUpdate);
    Cluster cluster = this.actorSystemLifecycle.cluster();
    ActorRef replicator = this.actorSystemLifecycle.replicator();
    ActorRef rrTopologyActor = this.readReplicaTopologyActor(rrTopologySink, databaseStateSink);
    this.coreTopologyActorRef = this
        .coreTopologyActor(cluster, replicator, coreTopologySink, bootstrapStateSink,
            rrTopologyActor);
    this.directoryActorRef = this
        .directoryActor(cluster, replicator, directorySink, rrTopologyActor);
    this.databaseStateActorRef = this
        .databaseStateActor(cluster, replicator, databaseStateSink, rrTopologyActor);
    this.startRestartNeededListeningActor(cluster);
  }

  private ActorRef coreTopologyActor(Cluster cluster, ActorRef replicator,
      SourceQueueWithComplete<CoreTopologyMessage> topologySink,
      SourceQueueWithComplete<BootstrapState> bootstrapStateSink, ActorRef rrTopologyActor) {
    DiscoveryMember discoveryMember = this.discoveryMemberFactory.create(this.myself);
    TopologyBuilder topologyBuilder = new TopologyBuilder();
    Props coreTopologyProps =
        CoreTopologyActor
            .props(discoveryMember, topologySink, bootstrapStateSink, rrTopologyActor, replicator,
                cluster, topologyBuilder, this.config,
                this.replicatedDataMonitor, this.clusterSizeMonitor);
    return this.actorSystemLifecycle
        .applicationActorOf(coreTopologyProps, "cc-core-topology-actor");
  }

  private ActorRef directoryActor(Cluster cluster, ActorRef replicator,
      SourceQueueWithComplete<Map<DatabaseId, LeaderInfo>> directorySink,
      ActorRef rrTopologyActor) {
    Props directoryProps = DirectoryActor
        .props(cluster, replicator, directorySink, rrTopologyActor, this.replicatedDataMonitor);
    return this.actorSystemLifecycle.applicationActorOf(directoryProps, "cc-directory-actor");
  }

  private ActorRef databaseStateActor(Cluster cluster, ActorRef replicator,
      SourceQueueWithComplete<ReplicatedDatabaseState> stateSink,
      ActorRef rrTopologyActor) {
    Props stateProps = DatabaseStateActor
        .props(cluster, replicator, stateSink, rrTopologyActor, this.replicatedDataMonitor,
            this.myself);
    return this.actorSystemLifecycle.applicationActorOf(stateProps, "cc-database-status-actor");
  }

  private ActorRef readReplicaTopologyActor(
      SourceQueueWithComplete<DatabaseReadReplicaTopology> topologySink,
      SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink) {
    ClusterClientReceptionist receptionist = this.actorSystemLifecycle.clusterClientReceptionist();
    Props readReplicaTopologyProps = ReadReplicaTopologyActor
        .props(topologySink, databaseStateSink, receptionist, this.config, this.clock);
    return this.actorSystemLifecycle
        .applicationActorOf(readReplicaTopologyProps, "cc-rr-topology-actor");
  }

  private void startRestartNeededListeningActor(Cluster cluster) {
    Runnable restart = () ->
    {
      this.executor.execute(this::restart);
    };
    EventStream eventStream = this.actorSystemLifecycle.eventStream();
    Props props = RestartNeededListeningActor.props(restart, eventStream, cluster);
    this.actorSystemLifecycle.applicationActorOf(props, "cc-core-restart-needed-listener");
  }

  private void onCoreTopologyMessage(CoreTopologyMessage coreTopologyMessage) {
    this.globalTopologyState.onTopologyUpdate(coreTopologyMessage.coreTopology());
    this.actorSystemLifecycle.addSeenAddresses(coreTopologyMessage.akkaMembers());
  }

  public void stop0() throws Exception {
    this.coreTopologyActorRef = null;
    this.directoryActorRef = null;
    this.actorSystemLifecycle.shutdown();
    this.globalTopologyState = this.newGlobalTopologyState(this.logProvider, this.listenerService);
  }

  public void addLocalCoreTopologyListener(CoreTopologyService.Listener listener) {
    this.listenerService.addCoreTopologyListener(listener);
    listener.onCoreTopologyChange(this.coreTopologyForDatabase(listener.namedDatabaseId()));
  }

  public void removeLocalCoreTopologyListener(CoreTopologyService.Listener listener) {
    this.listenerService.removeCoreTopologyListener(listener);
  }

  public PublishRaftIdOutcome publishRaftId(RaftId raftId) throws TimeoutException {
    ActorRef coreTopologyActor = this.coreTopologyActorRef;
    if (coreTopologyActor != null) {
      Duration timeout = this.config.get(CausalClusteringSettings.raft_id_publish_timeout);
      RaftIdSetRequest request = new RaftIdSetRequest(raftId, this.myself, timeout);
      CompletableFuture idSetJob = Patterns.ask(coreTopologyActor, request, timeout)
          .thenApply((response) ->
          {
            if (!(response instanceof PublishRaftIdOutcome)) {
              throw new IllegalArgumentException(
                  String.format(
                      "Unexpected response when attempting to publish raftId. Expected %s, received %s",
                      PublishRaftIdOutcome.class
                          .getSimpleName(),
                      response.getClass()
                          .getCanonicalName()));
            } else {
              return (PublishRaftIdOutcome) response;
            }
          }).toCompletableFuture();

      try {
        return (PublishRaftIdOutcome) idSetJob.join();
      } catch (CompletionException n7) {
        if (n7.getCause() instanceof AskTimeoutException) {
          throw new TimeoutException(
              "Could not publsh raft id within " + timeout.toSeconds() + " seconds");
        } else {
          throw new RuntimeException(n7.getCause());
        }
      }
    } else {
      return PublishRaftIdOutcome.FAILED_PUBLISH;
    }
  }

  public boolean canBootstrapRaftGroup(NamedDatabaseId namedDatabaseId) {
    return this.globalTopologyState.bootstrapState().canBootstrapRaft(namedDatabaseId);
  }

  @VisibleForTesting
  public synchronized void restart() {
    if (!State.RUN.equals(this.state())) {
      this.log.info("Not restarting because not running. State is %s", this.state());
    } else {
      this.userLog.info("Restarting discovery system after probable network partition");
      this.restarter.restart(this::doRestart);
    }
  }

  private boolean doRestart() {
    try {
      this.stop();
      this.start();
      this.userLog.info("Successfully restarted discovery system");
      return true;
    } catch (Throwable n2) {
      this.userLog.warn("Failed to restart discovery system", n2);
      return false;
    }
  }

  public void onDatabaseStart(NamedDatabaseId namedDatabaseId) {
    ActorRef coreTopologyActor = this.coreTopologyActorRef;
    if (coreTopologyActor != null) {
      coreTopologyActor.tell(new DatabaseStartedMessage(namedDatabaseId), ActorRef.noSender());
    }
  }

  public void onDatabaseStop(NamedDatabaseId namedDatabaseId) {
    this.localLeadersByDatabaseId.remove(namedDatabaseId);
    ActorRef coreTopologyActor = this.coreTopologyActorRef;
    if (coreTopologyActor != null) {
      coreTopologyActor.tell(new DatabaseStoppedMessage(namedDatabaseId), ActorRef.noSender());
    }
  }

  public void stateChange(DatabaseState previousState, DatabaseState newState) {
    ActorRef stateActor = this.databaseStateActorRef;
    if (stateActor != null) {
      stateActor.tell(DiscoveryDatabaseState.from(newState), ActorRef.noSender());
    }
  }

  public void setLeader(LeaderInfo newLeaderInfo, NamedDatabaseId namedDatabaseId) {
    LeaderInfo currentLeaderInfo = this.getLocalLeader(namedDatabaseId);
    if (currentLeaderInfo.term() < newLeaderInfo.term()) {
      this.log.info("I am member %s. Updating leader info to member %s database %s and term %s",
          this.memberId(), newLeaderInfo.memberId(), namedDatabaseId.name(), newLeaderInfo.term());
      this.localLeadersByDatabaseId.put(namedDatabaseId, newLeaderInfo);
      this.sendLeaderInfoIfNeeded(newLeaderInfo, namedDatabaseId);
    }
  }

  public void handleStepDown(long term, NamedDatabaseId namedDatabaseId) {
    LeaderInfo currentLeaderInfo = this.getLocalLeader(namedDatabaseId);
    boolean wasLeaderForTerm =
        Objects.equals(this.memberId(), currentLeaderInfo.memberId()) && term == currentLeaderInfo
            .term();
    if (wasLeaderForTerm) {
      this.log.info(
          "Step down event detected. This topology member, with MemberId %s, was leader for database %s in term %s, now moving to follower.",
          this.memberId(), namedDatabaseId.name(), currentLeaderInfo.term());
      LeaderInfo newLeaderInfo = currentLeaderInfo.stepDown();
      this.localLeadersByDatabaseId.put(namedDatabaseId, newLeaderInfo);
      this.sendLeaderInfoIfNeeded(newLeaderInfo, namedDatabaseId);
    }
  }

  public RoleInfo lookupRole(NamedDatabaseId namedDatabaseId, MemberId memberId) {
    LeaderInfo leaderInfo = this.localLeadersByDatabaseId.get(namedDatabaseId);
    return leaderInfo != null && Objects.equals(memberId, leaderInfo.memberId()) ? RoleInfo.LEADER
        : this.globalTopologyState.role(namedDatabaseId, memberId);
  }

  public LeaderInfo getLeader(NamedDatabaseId namedDatabaseId) {
    return this.globalTopologyState.getLeader(namedDatabaseId);
  }

  public Map<MemberId, CoreServerInfo> allCoreServers() {
    return this.globalTopologyState.allCoreServers();
  }

  public DatabaseCoreTopology coreTopologyForDatabase(NamedDatabaseId namedDatabaseId) {
    return this.globalTopologyState.coreTopologyForDatabase(namedDatabaseId);
  }

  public Map<MemberId, ReadReplicaInfo> allReadReplicas() {
    return this.globalTopologyState.allReadReplicas();
  }

  public DatabaseReadReplicaTopology readReplicaTopologyForDatabase(
      NamedDatabaseId namedDatabaseId) {
    return this.globalTopologyState.readReplicaTopologyForDatabase(namedDatabaseId);
  }

  public SocketAddress lookupCatchupAddress(MemberId upstream)
      throws CatchupAddressResolutionException {
    try {
      return this.catchupAddressRetryStrategy.apply(() ->
      {
        return this.globalTopologyState.retrieveCatchupServerAddress(upstream);
      }, Objects::nonNull);
    } catch (TimeoutException n3) {
      throw new CatchupAddressResolutionException(upstream);
    }
  }

  public MemberId memberId() {
    return this.myself;
  }

  public DiscoveryDatabaseState lookupDatabaseState(NamedDatabaseId namedDatabaseId,
      MemberId memberId) {
    return this.globalTopologyState.stateFor(memberId, namedDatabaseId);
  }

  public Map<MemberId, DiscoveryDatabaseState> allCoreStatesForDatabase(
      NamedDatabaseId namedDatabaseId) {
    return Map
        .copyOf(this.globalTopologyState.coreStatesForDatabase(namedDatabaseId).memberStates());
  }

  public Map<MemberId, DiscoveryDatabaseState> allReadReplicaStatesForDatabase(
      NamedDatabaseId namedDatabaseId) {
    return Map.copyOf(
        this.globalTopologyState.readReplicaStatesForDatabase(namedDatabaseId).memberStates());
  }

  @VisibleForTesting
  GlobalTopologyState topologyState() {
    return this.globalTopologyState;
  }

  private void sendLeaderInfoIfNeeded(LeaderInfo leaderInfo, NamedDatabaseId namedDatabaseId) {
    ActorRef directoryActor = this.directoryActorRef;
    if (directoryActor != null && (leaderInfo.memberId() != null || leaderInfo.isSteppingDown())) {
      directoryActor
          .tell(new LeaderInfoSettingMessage(leaderInfo, namedDatabaseId), ActorRef.noSender());
    }
  }

  private GlobalTopologyState newGlobalTopologyState(LogProvider logProvider,
      CoreTopologyListenerService listenerService) {
    Objects.requireNonNull(listenerService);
    return new GlobalTopologyState(logProvider, listenerService::notifyListeners);
  }

  private LeaderInfo getLocalLeader(NamedDatabaseId namedDatabaseId) {
    return this.localLeadersByDatabaseId.getOrDefault(namedDatabaseId, LeaderInfo.INITIAL);
  }

  public boolean isHealthy() {
    return this.restarter.isHealthy();
  }
}
