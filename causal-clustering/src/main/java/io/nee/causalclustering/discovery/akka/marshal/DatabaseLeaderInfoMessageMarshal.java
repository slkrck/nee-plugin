/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.marshal;

import io.nee.causalclustering.core.consensus.LeaderInfo;
import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.discovery.akka.directory.LeaderInfoDirectoryMessage;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class DatabaseLeaderInfoMessageMarshal extends
    SafeChannelMarshal<LeaderInfoDirectoryMessage> {

  private final ChannelMarshal<LeaderInfo> leaderInfoMarshal = new LeaderInfoMarshal();

  protected LeaderInfoDirectoryMessage unmarshal0(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    int size = channel.getInt();
    HashMap<DatabaseId, LeaderInfo> leaders = new HashMap(size);

    for (int i = 0; i < size; ++i) {
      DatabaseId databaseId = DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal(channel);
      LeaderInfo leaderInfo = this.leaderInfoMarshal.unmarshal(channel);
      leaders.put(databaseId, leaderInfo);
    }

    return new LeaderInfoDirectoryMessage(leaders);
  }

  public void marshal(LeaderInfoDirectoryMessage leaderInfoDirectoryMessage,
      WritableChannel channel) throws IOException {
    channel.putInt(leaderInfoDirectoryMessage.leaders().size());
    Iterator n3 = leaderInfoDirectoryMessage.leaders().entrySet().iterator();

    while (n3.hasNext()) {
      Entry<DatabaseId, LeaderInfo> entry = (Entry) n3.next();
      DatabaseId databaseId = entry.getKey();
      LeaderInfo leaderInfo = entry.getValue();
      DatabaseIdWithoutNameMarshal.INSTANCE.marshal(databaseId, channel);
      this.leaderInfoMarshal.marshal(leaderInfo, channel);
    }
  }
}
