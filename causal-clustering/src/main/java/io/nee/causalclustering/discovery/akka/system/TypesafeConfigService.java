/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.system;

import akka.cluster.UniqueAddress;
import com.typesafe.config.ConfigFactory;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.core.consensus.LeaderInfo;
import io.nee.causalclustering.discovery.DatabaseCoreTopology;
import io.nee.causalclustering.discovery.DatabaseReadReplicaTopology;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.ReplicatedDatabaseState;
import io.nee.causalclustering.discovery.akka.coretopology.CoreServerInfoForMemberId;
import io.nee.causalclustering.discovery.akka.database.state.DatabaseToMember;
import io.nee.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import io.nee.causalclustering.discovery.akka.directory.LeaderInfoDirectoryMessage;
import io.nee.causalclustering.discovery.akka.directory.ReplicatedLeaderInfo;
import io.nee.causalclustering.discovery.akka.marshal.BaseAkkaSerializer;
import io.nee.causalclustering.discovery.akka.marshal.CoreServerInfoForMemberIdSerializer;
import io.nee.causalclustering.discovery.akka.marshal.CoreTopologySerializer;
import io.nee.causalclustering.discovery.akka.marshal.DatabaseIdWithoutNameSerializer;
import io.nee.causalclustering.discovery.akka.marshal.DatabaseLeaderInfoMessageSerializer;
import io.nee.causalclustering.discovery.akka.marshal.DatabaseToMemberSerializer;
import io.nee.causalclustering.discovery.akka.marshal.DiscoveryDatabaseStateSerializer;
import io.nee.causalclustering.discovery.akka.marshal.LeaderInfoSerializer;
import io.nee.causalclustering.discovery.akka.marshal.MemberIdSerializer;
import io.nee.causalclustering.discovery.akka.marshal.RaftIdSerializer;
import io.nee.causalclustering.discovery.akka.marshal.ReadReplicaInfoSerializer;
import io.nee.causalclustering.discovery.akka.marshal.ReadReplicaRefreshMessageSerializer;
import io.nee.causalclustering.discovery.akka.marshal.ReadReplicaRemovalMessageSerializer;
import io.nee.causalclustering.discovery.akka.marshal.ReadReplicaTopologySerializer;
import io.nee.causalclustering.discovery.akka.marshal.ReplicatedDatabaseStateSerializer;
import io.nee.causalclustering.discovery.akka.marshal.ReplicatedLeaderInfoSerializer;
import io.nee.causalclustering.discovery.akka.marshal.UniqueAddressSerializer;
import io.nee.causalclustering.discovery.akka.readreplicatopology.ReadReplicaRefreshMessage;
import io.nee.causalclustering.discovery.akka.readreplicatopology.ReadReplicaRemovalMessage;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.logging.Level;

public final class TypesafeConfigService {

  static final String DISCOVERY_SINK_DISPATCHER = "discovery-dispatcher";
  private final Config config;
  private final TypesafeConfigService.ArteryTransport arteryTransport;

  public TypesafeConfigService(TypesafeConfigService.ArteryTransport arteryTransport,
      Config config) {
    this.config = config;
    this.arteryTransport = arteryTransport;
  }

  static String hostname(SocketAddress socketAddress) {
    return socketAddress.isIPv6() ? "[" + socketAddress.getHostname() + "]"
        : socketAddress.getHostname();
  }

  private static AkkaLoggingLevel logLevel(Config config) {
    Level configuredLevel = config.get(CausalClusteringSettings.middleware_logging_level);
    return AkkaLoggingLevel.fromNeo4jLevel(configuredLevel);
  }

  public com.typesafe.config.Config generate() {
    Path externalConfig = this.config.get(CausalClusteringSettings.middleware_akka_external_config);
    com.typesafe.config.Config base;
    if (externalConfig == null) {
      base = ConfigFactory.empty();
    } else {
      base = ConfigFactory.parseFileAnySyntax(externalConfig.toFile());
    }

    return base.withFallback(this.shutdownConfig()).withFallback(this.transportConfig())
        .withFallback(this.serializationConfig()).withFallback(
            this.failureDetectorConfig()).withFallback(this.loggingConfig())
        .withFallback(this.dispatcherConfig()).withFallback(
            ConfigFactory.defaultReference()).resolve();
  }

  private com.typesafe.config.Config shutdownConfig() {
    HashMap<String, Object> configMap = new HashMap();
    configMap.put("akka.jvm-shutdown-hooks", "off");
    configMap.put("akka.cluster.run-coordinated-shutdown-when-down", "off");
    return ConfigFactory.parseMap(configMap);
  }

  private com.typesafe.config.Config transportConfig() {
    SocketAddress listenAddress = this.config
        .get(CausalClusteringSettings.discovery_listen_address);
    SocketAddress advertisedAddress = this.config
        .get(CausalClusteringSettings.discovery_advertised_address);
    Map<String, Object> configMap = new HashMap();
    configMap.put("akka.remote.artery.enabled", true);
    configMap.put("akka.remote.artery.transport", this.arteryTransport.configValue);
    configMap.put("akka.remote.artery.canonical.hostname", hostname(advertisedAddress));
    configMap.put("akka.remote.artery.canonical.port", advertisedAddress.getPort());
    configMap.put("akka.remote.artery.bind.hostname", hostname(listenAddress));
    configMap.put("akka.remote.artery.bind.port", listenAddress.getPort());
    Duration bindTimeout = this.config.get(CausalClusteringSettings.akka_bind_timeout);
    configMap.put("akka.remote.artery.bind.bind-timeout", bindTimeout.toMillis() + "ms");
    Duration connectionTimeout = this.config.get(CausalClusteringSettings.akka_connection_timeout);
    configMap
        .put("akka.remote.artery.advanced.connection-timeout", connectionTimeout.toMillis() + "ms");
    Duration handshakeTimeout = this.config.get(CausalClusteringSettings.akka_handshake_timeout);
    configMap
        .put("akka.remote.artery.advanced.handshake-timeout", handshakeTimeout.toMillis() + "ms");
    return ConfigFactory.parseMap(configMap);
  }

  private com.typesafe.config.Config failureDetectorConfig() {
    Map<String, Object> configMap = new HashMap();
    long heartbeatIntervalMillis = this.config
        .get(CausalClusteringSettings.akka_failure_detector_heartbeat_interval).toMillis();
    configMap
        .put("akka.cluster.failure-detector.heartbeat-interval", heartbeatIntervalMillis + "ms");
    Double threshold = this.config.get(CausalClusteringSettings.akka_failure_detector_threshold);
    configMap.put("akka.cluster.failure-detector.threshold", threshold);
    Integer maxSampleSize = this.config
        .get(CausalClusteringSettings.akka_failure_detector_max_sample_size);
    configMap.put("akka.cluster.failure-detector.max-sample-size", maxSampleSize);
    long minStdDeviationMillis = this.config
        .get(CausalClusteringSettings.akka_failure_detector_min_std_deviation).toMillis();
    configMap.put("akka.cluster.failure-detector.min-std-deviation", minStdDeviationMillis + "ms");
    long acceptableHeartbeatPauseMillis =
        this.config.get(CausalClusteringSettings.akka_failure_detector_acceptable_heartbeat_pause)
            .toMillis();
    configMap.put("akka.cluster.failure-detector.acceptable-heartbeat-pause",
        acceptableHeartbeatPauseMillis + "ms");
    Integer monitoredByNrOfMembers = this.config
        .get(CausalClusteringSettings.akka_failure_detector_monitored_by_nr_of_members);
    configMap
        .put("akka.cluster.failure-detector.monitored-by-nr-of-members", monitoredByNrOfMembers);
    long expectedResponseAfterMillis = this.config
        .get(CausalClusteringSettings.akka_failure_detector_expected_response_after).toMillis();
    configMap.put("akka.cluster.failure-detector.expected-response-after",
        expectedResponseAfterMillis + "ms");
    return ConfigFactory.parseMap(configMap);
  }

  private com.typesafe.config.Config dispatcherConfig() {
    Integer parallelism = this.config
        .get(CausalClusteringSettings.middleware_akka_sink_parallelism_level);
    Map<String, Object> configMap = new HashMap();
    configMap.put("discovery-dispatcher.type", "Dispatcher");
    configMap.put("discovery-dispatcher.executor", "fork-join-executor");
    configMap.put("discovery-dispatcher.fork-join-executor.parallelism-min", parallelism);
    configMap.put("discovery-dispatcher.fork-join-executor.parallelism-factor", 1.0D);
    configMap.put("discovery-dispatcher.fork-join-executor.parallelism-max", parallelism);
    configMap.put("discovery-dispatcher.throughput", 10);
    return ConfigFactory.parseMap(configMap);
  }

  private com.typesafe.config.Config serializationConfig() {
    HashMap<String, Object> configMap = new HashMap();
    configMap.put("akka.actor.allow-java-serialization", "off");
    this.addSerializer(LeaderInfo.class, LeaderInfoSerializer.class, configMap);
    this.addSerializer(RaftId.class, RaftIdSerializer.class, configMap);
    this.addSerializer(UniqueAddress.class, UniqueAddressSerializer.class, configMap);
    this.addSerializer(CoreServerInfoForMemberId.class, CoreServerInfoForMemberIdSerializer.class,
        configMap);
    this.addSerializer(ReadReplicaRefreshMessage.class, ReadReplicaRefreshMessageSerializer.class,
        configMap);
    this.addSerializer(MemberId.class, MemberIdSerializer.class, configMap);
    this.addSerializer(ReadReplicaInfo.class, ReadReplicaInfoSerializer.class, configMap);
    this.addSerializer(DatabaseCoreTopology.class, CoreTopologySerializer.class, configMap);
    this.addSerializer(ReadReplicaRemovalMessage.class, ReadReplicaRemovalMessageSerializer.class,
        configMap);
    this.addSerializer(DatabaseReadReplicaTopology.class, ReadReplicaTopologySerializer.class,
        configMap);
    this.addSerializer(LeaderInfoDirectoryMessage.class, DatabaseLeaderInfoMessageSerializer.class,
        configMap);
    this.addSerializer(ReplicatedLeaderInfo.class, ReplicatedLeaderInfoSerializer.class, configMap);
    this.addSerializer(DatabaseId.class, DatabaseIdWithoutNameSerializer.class, configMap);
    this.addSerializer(ReplicatedDatabaseState.class, ReplicatedDatabaseStateSerializer.class,
        configMap);
    this.addSerializer(DatabaseToMember.class, DatabaseToMemberSerializer.class, configMap);
    this.addSerializer(DiscoveryDatabaseState.class, DiscoveryDatabaseStateSerializer.class,
        configMap);
    return ConfigFactory.parseMap(configMap);
  }

  private <T, M extends BaseAkkaSerializer<T>> void addSerializer(Class<T> message,
      Class<M> serializer, Map<String, Object> configMap) {
    String customSerializer = message.getSimpleName() + "-serializer";
    configMap.put("akka.actor.serializers." + customSerializer, serializer.getCanonicalName());
    configMap.put("akka.actor.serialization-bindings.\"" + message.getCanonicalName() + "\"",
        customSerializer);
  }

  private com.typesafe.config.Config loggingConfig() {
    HashMap<String, Object> configMap = new HashMap();
    configMap.put("akka.loggers", Collections.singletonList(LoggingActor.class.getCanonicalName()));
    configMap.put("akka.loglevel", logLevel(this.config).toString());
    configMap.put("akka.logging-filter", LoggingFilter.class.getCanonicalName());
    return ConfigFactory.parseMap(configMap);
  }

  public enum ArteryTransport {
    AERON("aeron-udp"),
    TCP("tcp"),
    TLS_TCP("tls-tcp");

    private final String configValue;

    ArteryTransport(String configValue) {
      this.configValue = configValue;
    }
  }
}
