/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka;

import akka.actor.AbstractActor;
import akka.actor.ActorLogging;
import akka.actor.TimerScheduler;
import akka.actor.TimerSchedulerImpl;
import akka.actor.Timers;
import akka.event.LoggingAdapter;
import scala.Option;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

@JavaDocToJava
public abstract class AbstractActorWithTimersAndLogging extends AbstractActor implements Timers,
    ActorLogging {

  private TimerSchedulerImpl timer
  private LoggingAdapter log

  public AbstractActorWithTimersAndLogging() {

  }

  public LoggingAdapter log() {
    return log
  }

  public final TimerScheduler timers() {
    return timer
  }

  public void aroundPreRestart(final Throwable reason, final Option<Object> message) {

  }

  public void aroundPostStop() {
    
  }

  public void aroundReceive(final PartialFunction<Object, BoxedUnit> receive, final Object msg) {

  }

  public LoggingAdapter log() {
    return this.log
  }

  public void eq(final LoggingAdapter la) {
    this.log = la
  }

  public TimerSchedulerImpl timers() {
    return this.timer
  }

  public final void eq(
      final TimerSchedulerImpl x) {
    this.timers = x;
  }

  public final TimerScheduler getTimers() {
    return this.timers();
  }
}
