/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.ActorRef;
import io.nee.causalclustering.discovery.DatabaseReadReplicaTopology;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.ReplicatedDatabaseState;
import io.nee.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import io.nee.causalclustering.identity.MemberId;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.kernel.database.DatabaseId;

class ReadReplicaViewMessage {

  static final ReadReplicaViewMessage EMPTY = new ReadReplicaViewMessage(Collections.emptyMap());
  private final Map<ActorRef, ReadReplicaViewRecord> clusterClientReadReplicas;

  ReadReplicaViewMessage(Map<ActorRef, ReadReplicaViewRecord> clusterClientReadReplicas) {
    this.clusterClientReadReplicas = Map.copyOf(clusterClientReadReplicas);
  }

  Stream<ActorRef> topologyClient(ActorRef clusterClient) {
    return Stream.ofNullable(this.clusterClientReadReplicas.get(clusterClient)).map(
        ReadReplicaViewRecord::topologyClientActorRef);
  }

  DatabaseReadReplicaTopology toReadReplicaTopology(DatabaseId databaseId) {
    Map<MemberId, ReadReplicaInfo> knownReadReplicas = this.clusterClientReadReplicas.values()
        .stream().filter((info) ->
        {
          return info.readReplicaInfo()
              .startedDatabaseIds()
              .contains(databaseId);
        }).map((info) ->
        {
          return Pair
              .of(info.memberId(),
                  info.readReplicaInfo());
        })
        .collect(Collectors.toMap(Pair::first, Pair::other));
    return new DatabaseReadReplicaTopology(databaseId, knownReadReplicas);
  }

  Map<DatabaseId, ReplicatedDatabaseState> allReplicatedDatabaseStates() {
    Map<DatabaseId, Map<MemberId, DiscoveryDatabaseState>> allMemberStatesPerDbMultiMap =
        this.clusterClientReadReplicas.values().stream().flatMap(this::getAllStatesFromMember)
            .collect(Collectors.groupingBy((p) ->
            {
              return p
                  .other()
                  .databaseId();
            }, Collectors
                .toMap(Pair::first,
                    Pair::other)));
    return allMemberStatesPerDbMultiMap.entrySet().stream()
        .collect(Collectors.toMap(Entry::getKey, (e) ->
        {
          return ReplicatedDatabaseState.ofReadReplicas(e.getKey(), e.getValue());
        }));
  }

  private Stream<Pair<MemberId, DiscoveryDatabaseState>> getAllStatesFromMember(
      ReadReplicaViewRecord record) {
    return record.databaseStates().values().stream().map((state) ->
    {
      return Pair.of(record.memberId(), state);
    });
  }

  Set<DatabaseId> databaseIds() {
    return this.clusterClientReadReplicas.values().stream()
        .map(ReadReplicaViewRecord::readReplicaInfo).flatMap((info) ->
        {
          return info
              .startedDatabaseIds()
              .stream();
        }).collect(Collectors.toSet());
  }

  public String toString() {
    return "ReadReplicaViewMessage{clusterClientReadReplicas=" + this.clusterClientReadReplicas
        + "}";
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ReadReplicaViewMessage that = (ReadReplicaViewMessage) o;
      return Objects.equals(this.clusterClientReadReplicas, that.clusterClientReadReplicas);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.clusterClientReadReplicas);
  }
}
