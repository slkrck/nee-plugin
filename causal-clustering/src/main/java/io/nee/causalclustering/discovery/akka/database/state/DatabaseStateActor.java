/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.database.state;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ddata.LWWMap;
import akka.cluster.ddata.LWWMapKey;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.javadsl.SourceQueueWithComplete;
import io.nee.causalclustering.discovery.ReplicatedDatabaseState;
import io.nee.causalclustering.discovery.akka.BaseReplicatedDataActor;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;
import io.nee.causalclustering.identity.MemberId;
import io.nee.dbms.EnterpriseOperatorState;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.neo4j.kernel.database.DatabaseId;

public class DatabaseStateActor extends
    BaseReplicatedDataActor<LWWMap<DatabaseToMember, DiscoveryDatabaseState>> {

  public static final String NAME = "cc-database-status-actor";
  private final SourceQueueWithComplete<ReplicatedDatabaseState> stateUpdateSink;
  private final ActorRef rrTopologyActor;
  private final MemberId memberId;

  private DatabaseStateActor(Cluster cluster, ActorRef replicator,
      SourceQueueWithComplete<ReplicatedDatabaseState> stateUpdateSink,
      ActorRef rrTopologyActor, ReplicatedDataMonitor monitor, MemberId memberId) {
    super(cluster, replicator, LWWMapKey::create, LWWMap::create,
        ReplicatedDataIdentifier.DATABASE_STATE, monitor);
    this.stateUpdateSink = stateUpdateSink;
    this.rrTopologyActor = rrTopologyActor;
    this.memberId = memberId;
  }

  public static Props props(Cluster cluster, ActorRef replicator,
      SourceQueueWithComplete<ReplicatedDatabaseState> discoveryUpdateSink,
      ActorRef rrTopologyActor, ReplicatedDataMonitor monitor, MemberId memberId) {
    return Props.create(DatabaseStateActor.class, () ->
    {
      return new DatabaseStateActor(cluster, replicator, discoveryUpdateSink, rrTopologyActor,
          monitor, memberId);
    });
  }

  private static <K, V> Collector<Entry<K, V>, ?, Map<K, V>> entriesToMap() {
    return Collectors.toMap(Entry::getKey, Entry::getValue);
  }

  protected int dataMetricVisible() {
    return (this.data).size();
  }

  protected int dataMetricInvisible() {
    return (this.data).underlying().keys().vvector().size();
  }

  protected void sendInitialDataToReplicator() {
  }

  protected void handleCustomEvents(ReceiveBuilder builder) {
    builder.match(DiscoveryDatabaseState.class, this::handleDatabaseState);
  }

  private void handleDatabaseState(DiscoveryDatabaseState update) {
    if (update.operatorState() == EnterpriseOperatorState.DROPPED) {
      this.modifyReplicatedData(this.key, (map) ->
      {
        return map.remove(this.cluster, new DatabaseToMember(update.databaseId(), this.memberId));
      });
    } else {
      this.modifyReplicatedData(this.key, (map) ->
      {
        return map
            .put(this.cluster, new DatabaseToMember(update.databaseId(), this.memberId), update);
      });
    }
  }

  protected void handleIncomingData(LWWMap<DatabaseToMember, DiscoveryDatabaseState> newData) {
    this.data = newData;
    Map<DatabaseId, Map<MemberId, DiscoveryDatabaseState>> statesGroupedByDatabase =
        (this.data).getEntries().entrySet().stream().map((e) ->
        {
          return Map.entry((e.getKey()).memberId(), e.getValue());
        }).collect(Collectors.groupingBy((e) ->
        {
          return e.getValue()
              .databaseId();
        }, entriesToMap()));
    List<ReplicatedDatabaseState> allReplicatedStates = statesGroupedByDatabase.entrySet().stream()
        .map((e) ->
        {
          return ReplicatedDatabaseState
              .ofCores(e.getKey(), e.getValue());
        }).collect(Collectors.toList());
    SourceQueueWithComplete n10001 = this.stateUpdateSink;
    Objects.requireNonNull(n10001);
    allReplicatedStates.forEach(n10001::offer);
    this.rrTopologyActor.tell(new AllReplicatedDatabaseStates(allReplicatedStates), this.getSelf());
  }
}
