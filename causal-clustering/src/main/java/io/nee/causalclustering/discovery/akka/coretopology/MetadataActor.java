/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.coretopology;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.UniqueAddress;
import akka.cluster.ddata.LWWMap;
import akka.cluster.ddata.LWWMapKey;
import akka.japi.pf.ReceiveBuilder;
import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.discovery.akka.BaseReplicatedDataActor;
import io.nee.causalclustering.discovery.akka.common.DatabaseStartedMessage;
import io.nee.causalclustering.discovery.akka.common.DatabaseStoppedMessage;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;
import io.nee.causalclustering.discovery.member.DiscoveryMember;
import java.util.HashSet;
import java.util.Set;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.DatabaseId;

public class MetadataActor extends
    BaseReplicatedDataActor<LWWMap<UniqueAddress, CoreServerInfoForMemberId>> {

  private final DiscoveryMember myself;
  private final ActorRef topologyActor;
  private final Config config;
  private final Set<DatabaseId> startedDatabases = new HashSet();

  private MetadataActor(DiscoveryMember myself, Cluster cluster, ActorRef replicator,
      ActorRef topologyActor, Config config, ReplicatedDataMonitor monitor) {
    super(cluster, replicator, LWWMapKey::create, LWWMap::empty, ReplicatedDataIdentifier.METADATA,
        monitor);
    this.myself = myself;
    this.topologyActor = topologyActor;
    this.config = config;
  }

  static Props props(DiscoveryMember myself, Cluster cluster, ActorRef replicator,
      ActorRef topologyActor, Config config, ReplicatedDataMonitor monitor) {
    return Props.create(MetadataActor.class, () ->
    {
      return new MetadataActor(myself, cluster, replicator, topologyActor, config, monitor);
    });
  }

  protected void handleCustomEvents(ReceiveBuilder builder) {
    builder.match(CleanupMessage.class, this::removeDataFromReplicator)
        .match(DatabaseStartedMessage.class, this::handleDatabaseStartedMessage).match(
        DatabaseStoppedMessage.class, this::handleDatabaseStoppedMessage);
  }

  private void handleDatabaseStartedMessage(DatabaseStartedMessage message) {
    if (this.startedDatabases.add(message.namedDatabaseId().databaseId())) {
      this.sendCoreServerInfo();
    }
  }

  private void handleDatabaseStoppedMessage(DatabaseStoppedMessage message) {
    if (this.startedDatabases.remove(message.namedDatabaseId().databaseId())) {
      this.sendCoreServerInfo();
    }
  }

  public void sendInitialDataToReplicator() {
    this.startedDatabases.addAll(this.myself.startedDatabases());
    this.sendCoreServerInfo();
  }

  private void removeDataFromReplicator(CleanupMessage message) {
    this.modifyReplicatedData(this.key, (map) ->
    {
      return map.remove(this.cluster, message.uniqueAddress());
    });
  }

  protected void handleIncomingData(LWWMap<UniqueAddress, CoreServerInfoForMemberId> newData) {
    this.data = newData;
    this.topologyActor.tell(new MetadataMessage(this.data), this.getSelf());
  }

  protected int dataMetricVisible() {
    return this.data.size();
  }

  protected int dataMetricInvisible() {
    return ((LWWMap) this.data).underlying().keys().vvector().size();
  }

  private void sendCoreServerInfo() {
    Set<DatabaseId> startedDatabases = Set.copyOf(this.startedDatabases);
    CoreServerInfo info = CoreServerInfo.from(this.config, startedDatabases);
    CoreServerInfoForMemberId metadata = new CoreServerInfoForMemberId(this.myself.id(), info);
    this.modifyReplicatedData(this.key, (map) ->
    {
      return map.put(this.cluster, this.cluster.selfUniqueAddress(), metadata);
    });
  }
}
