/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.kubernetes;

public class Status extends KubernetesType {

  private String status;
  private String message;
  private String reason;
  private int code;

  public String status() {
    return this.status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String message() {
    return this.message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String reason() {
    return this.reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public int code() {
    return this.code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public <T> T handle(KubernetesType.Visitor<T> visitor) {
    return visitor.visit(this);
  }

  public String toString() {
    return "Status{status='" + this.status + "', message='" + this.message + "', reason='"
        + this.reason + "', code=" + this.code + "}";
  }
}
