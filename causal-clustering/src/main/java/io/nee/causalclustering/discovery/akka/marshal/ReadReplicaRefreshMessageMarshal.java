/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.marshal;

import akka.actor.ActorRef;
import akka.actor.ExtendedActorSystem;
import io.nee.causalclustering.core.state.storage.SafeChannelMarshal;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import io.nee.causalclustering.discovery.akka.readreplicatopology.ReadReplicaRefreshMessage;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.messaging.EndOfStreamException;
import io.nee.causalclustering.messaging.marshalling.ChannelMarshal;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaRefreshMessageMarshal extends
    SafeChannelMarshal<ReadReplicaRefreshMessage> {

  private final ChannelMarshal<ReadReplicaInfo> readReplicaInfoMarshal;
  private final ChannelMarshal<MemberId> memberIdMarshal = new MemberId.Marshal();
  private final ChannelMarshal<ActorRef> actorRefMarshal;

  public ReadReplicaRefreshMessageMarshal(ExtendedActorSystem system) {
    this.actorRefMarshal = new ActorRefMarshal(system);
    this.readReplicaInfoMarshal = new ReadReplicaInfoMarshal();
  }

  protected ReadReplicaRefreshMessage unmarshal0(ReadableChannel channel)
      throws IOException, EndOfStreamException {
    ReadReplicaInfo rrInfo = this.readReplicaInfoMarshal.unmarshal(channel);
    MemberId memberId = this.memberIdMarshal.unmarshal(channel);
    ActorRef clusterClient = this.actorRefMarshal.unmarshal(channel);
    ActorRef topologyClient = this.actorRefMarshal.unmarshal(channel);
    HashMap<DatabaseId, DiscoveryDatabaseState> databaseStates = new HashMap();
    int size = channel.getInt();

    for (int i = 0; i < size; ++i) {
      DatabaseId id = DatabaseIdWithoutNameMarshal.INSTANCE.unmarshal(channel);
      DiscoveryDatabaseState state = DiscoveryDatabaseStateMarshal.INSTANCE.unmarshal(channel);
      databaseStates.put(id, state);
    }

    return new ReadReplicaRefreshMessage(rrInfo, memberId, clusterClient, topologyClient,
        databaseStates);
  }

  public void marshal(ReadReplicaRefreshMessage readReplicaRefreshMessage, WritableChannel channel)
      throws IOException {
    this.readReplicaInfoMarshal.marshal(readReplicaRefreshMessage.readReplicaInfo(), channel);
    this.memberIdMarshal.marshal(readReplicaRefreshMessage.memberId(), channel);
    this.actorRefMarshal.marshal(readReplicaRefreshMessage.clusterClient(), channel);
    this.actorRefMarshal.marshal(readReplicaRefreshMessage.topologyClientActorRef(), channel);
    Map<DatabaseId, DiscoveryDatabaseState> databaseStates = readReplicaRefreshMessage
        .databaseStates();
    channel.putInt(databaseStates.size());
    Iterator n4 = databaseStates.entrySet().iterator();

    while (n4.hasNext()) {
      Entry<DatabaseId, DiscoveryDatabaseState> entry = (Entry) n4.next();
      DatabaseIdWithoutNameMarshal.INSTANCE.marshal(entry.getKey(), channel);
      DiscoveryDatabaseStateMarshal.INSTANCE.marshal(entry.getValue(), channel);
    }
  }
}
