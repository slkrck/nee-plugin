/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.AbstractActorWithTimers;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.client.ClusterClient.Publish;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.javadsl.SourceQueueWithComplete;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.core.consensus.LeaderInfo;
import io.nee.causalclustering.discovery.DatabaseCoreTopology;
import io.nee.causalclustering.discovery.DatabaseReadReplicaTopology;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.ReplicatedDatabaseState;
import io.nee.causalclustering.discovery.akka.common.DatabaseStartedMessage;
import io.nee.causalclustering.discovery.akka.common.DatabaseStoppedMessage;
import io.nee.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import io.nee.causalclustering.discovery.akka.directory.LeaderInfoDirectoryMessage;
import io.nee.causalclustering.discovery.member.DiscoveryMember;
import io.nee.dbms.EnterpriseOperatorState;
import java.time.Clock;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ClientTopologyActor extends AbstractActorWithTimers {

  public static final String NAME = "cc-client-topology-actor";
  private static final String REFRESH = "topology refresh";
  private static final int REFRESHES_BEFORE_REMOVE_TOPOLOGY = 3;
  private final Duration refreshDuration;
  private final DiscoveryMember myself;
  private final PruningStateSink<DatabaseCoreTopology> coreTopologySink;
  private final PruningStateSink<DatabaseReadReplicaTopology> readreplicaTopologySink;
  private final PruningStateSink<ReplicatedDatabaseState> coresDbStateSink;
  private final PruningStateSink<ReplicatedDatabaseState> readReplicasDbStateSink;
  private final SourceQueueWithComplete<Map<DatabaseId, LeaderInfo>> discoverySink;
  private final Map<DatabaseId, DiscoveryDatabaseState> localDatabaseStates;
  private final ActorRef clusterClient;
  private final Config config;
  private final Log log;
  private final Set<DatabaseId> startedDatabases = new HashSet();

  private ClientTopologyActor(DiscoveryMember myself,
      SourceQueueWithComplete<DatabaseCoreTopology> coreTopologySink,
      SourceQueueWithComplete<DatabaseReadReplicaTopology> rrTopologySink,
      SourceQueueWithComplete<Map<DatabaseId, LeaderInfo>> leaderInfoSink,
      SourceQueueWithComplete<ReplicatedDatabaseState> stateSink, Config config,
      LogProvider logProvider, Clock clock,
      ActorRef clusterClient) {
    this.myself = myself;
    this.refreshDuration = config.get(CausalClusteringSettings.cluster_topology_refresh);
    Duration maxTopologyLifetime = this.refreshDuration.multipliedBy(3L);
    this.coreTopologySink = PruningStateSink
        .forCoreTopologies(coreTopologySink, maxTopologyLifetime, clock, logProvider);
    this.readreplicaTopologySink = PruningStateSink
        .forReadReplicaTopologies(rrTopologySink, maxTopologyLifetime, clock, logProvider);
    this.coresDbStateSink = PruningStateSink
        .forCoreDatabaseStates(stateSink, maxTopologyLifetime, clock, logProvider);
    this.readReplicasDbStateSink = PruningStateSink
        .forReadReplicaDatabaseStates(stateSink, maxTopologyLifetime, clock, logProvider);
    this.discoverySink = leaderInfoSink;
    this.clusterClient = clusterClient;
    this.localDatabaseStates = new HashMap();
    this.config = config;
    this.log = logProvider.getLog(this.getClass());
  }

  public static Props props(DiscoveryMember myself,
      SourceQueueWithComplete<DatabaseCoreTopology> coreTopologySink,
      SourceQueueWithComplete<DatabaseReadReplicaTopology> rrTopologySink,
      SourceQueueWithComplete<Map<DatabaseId, LeaderInfo>> discoverySink,
      SourceQueueWithComplete<ReplicatedDatabaseState> stateSink, ActorRef clusterClient,
      Config config, LogProvider logProvider,
      Clock clock) {
    return Props.create(ClientTopologyActor.class, () ->
    {
      return new ClientTopologyActor(myself, coreTopologySink, rrTopologySink, discoverySink,
          stateSink, config, logProvider, clock, clusterClient);
    });
  }

  public Receive createReceive() {
    ReceiveBuilder n10000 = ReceiveBuilder.create();
    PruningStateSink<DatabaseCoreTopology> n10002 = this.coreTopologySink;
    Objects.requireNonNull(n10002);
    n10000 = n10000.match(DatabaseCoreTopology.class, n10002::offer);
    PruningStateSink<DatabaseReadReplicaTopology> n10003 = this.readreplicaTopologySink;
    Objects.requireNonNull(n10003);
    return n10000.match(DatabaseReadReplicaTopology.class, n10003::offer)
        .match(LeaderInfoDirectoryMessage.class, (msg) ->
        {
          this.discoverySink.offer(msg.leaders());
        }).match(ReplicatedDatabaseState.class, this::handleRemoteDatabaseStateUpdate)
        .match(ClientTopologyActor.TopologiesRefresh.class, (ignored) ->
        {
          this.handleRefresh();
        }).match(DatabaseStartedMessage.class, this::handleDatabaseStartedMessage)
        .match(DatabaseStoppedMessage.class,
            this::handleDatabaseStoppedMessage)
        .match(DiscoveryDatabaseState.class, this::handleLocalDatabaseStateUpdate).build();
  }

  public void preStart() {
    this.getTimers()
        .startPeriodicTimer("topology refresh", ClientTopologyActor.TopologiesRefresh.getInstance(),
            this.refreshDuration);
    this.startedDatabases.addAll(this.myself.startedDatabases());
    this.sendReadReplicaInfo();
  }

  private void handleDatabaseStartedMessage(DatabaseStartedMessage message) {
    if (this.startedDatabases.add(message.namedDatabaseId().databaseId())) {
      this.sendReadReplicaInfo();
    }
  }

  private void handleDatabaseStoppedMessage(DatabaseStoppedMessage message) {
    if (this.startedDatabases.remove(message.namedDatabaseId().databaseId())) {
      this.sendReadReplicaInfo();
    }
  }

  private void handleRemoteDatabaseStateUpdate(ReplicatedDatabaseState update) {
    if (update.containsCoreStates()) {
      this.coresDbStateSink.offer(update);
    } else {
      this.readReplicasDbStateSink.offer(update);
    }
  }

  private void handleLocalDatabaseStateUpdate(DiscoveryDatabaseState update) {
    if (update.operatorState() == EnterpriseOperatorState.DROPPED) {
      this.localDatabaseStates.remove(update.databaseId());
    } else {
      this.localDatabaseStates.put(update.databaseId(), update);
    }
  }

  private void handleRefresh() {
    this.coreTopologySink.pruneStaleState();
    this.readreplicaTopologySink.pruneStaleState();
    this.sendReadReplicaInfo();
  }

  private void sendReadReplicaInfo() {
    Set<DatabaseId> databaseIds = Set.copyOf(this.startedDatabases);
    ReadReplicaInfo readReplicaInfo = ReadReplicaInfo.from(this.config, databaseIds);
    ReadReplicaRefreshMessage refreshMsg =
        new ReadReplicaRefreshMessage(readReplicaInfo, this.myself.id(), this.clusterClient,
            this.getSelf(), this.localDatabaseStates);
    this.sendToCore(refreshMsg);
  }

  public void postStop() {
    ReadReplicaRemovalMessage msg = new ReadReplicaRemovalMessage(this.clusterClient);
    this.log.debug("Shutting down and sending removal message: %s", msg);
    this.sendToCore(msg);
  }

  private void sendToCore(Object msg) {
    this.clusterClient.tell(new Publish("rr-topic", msg), this.getSelf());
  }

  private static class TopologiesRefresh {

    private static final ClientTopologyActor.TopologiesRefresh instance = new ClientTopologyActor.TopologiesRefresh();

    public static ClientTopologyActor.TopologiesRefresh getInstance() {
      return instance;
    }
  }
}
