/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import io.nee.causalclustering.core.CausalClusteringSettings;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaInfo implements DiscoveryServerInfo {

  private final SocketAddress catchupServerAddress;
  private final ClientConnectorAddresses clientConnectorAddresses;
  private final Set<String> groups;
  private final Set<DatabaseId> databaseIds;

  public ReadReplicaInfo(ClientConnectorAddresses clientConnectorAddresses,
      SocketAddress catchupServerAddress, Set<String> groups,
      Set<DatabaseId> databaseIds) {
    this.clientConnectorAddresses = clientConnectorAddresses;
    this.catchupServerAddress = catchupServerAddress;
    this.groups = groups;
    this.databaseIds = databaseIds;
  }

  public static ReadReplicaInfo from(Config config, Set<DatabaseId> databaseIds) {
    ClientConnectorAddresses connectorUris = ClientConnectorAddresses.extractFromConfig(config);
    SocketAddress catchupAddress = config
        .get(CausalClusteringSettings.transaction_advertised_address);
    Set<String> groups = Set
        .copyOf((Collection) config.get(CausalClusteringSettings.server_groups));
    return new ReadReplicaInfo(connectorUris, catchupAddress, groups, databaseIds);
  }

  public Set<DatabaseId> startedDatabaseIds() {
    return this.databaseIds;
  }

  public ClientConnectorAddresses connectors() {
    return this.clientConnectorAddresses;
  }

  public SocketAddress catchupServer() {
    return this.catchupServerAddress;
  }

  public Set<String> groups() {
    return this.groups;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ReadReplicaInfo that = (ReadReplicaInfo) o;
      return Objects.equals(this.catchupServerAddress, that.catchupServerAddress) &&
          Objects.equals(this.clientConnectorAddresses, that.clientConnectorAddresses) && Objects
          .equals(this.groups, that.groups) &&
          Objects.equals(this.databaseIds, that.databaseIds);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.catchupServerAddress, this.clientConnectorAddresses, this.groups,
        this.databaseIds);
  }

  public String toString() {
    return "ReadReplicaInfo{catchupServerAddress=" + this.catchupServerAddress
        + ", clientConnectorAddresses=" + this.clientConnectorAddresses +
        ", groups=" + this.groups + ", startedDatabaseIds=" + this.databaseIds + "}";
  }
}
