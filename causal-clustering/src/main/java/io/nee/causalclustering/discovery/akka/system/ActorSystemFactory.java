/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.system;

import akka.actor.ActorSystem;
import akka.actor.BootstrapSetup;
import akka.actor.ProviderSelection;
import akka.actor.setup.ActorSystemSetup;
import akka.dispatch.ExecutionContexts;
import akka.remote.artery.tcp.SSLEngineProvider;
import akka.remote.artery.tcp.SSLEngineProviderSetup;
import com.typesafe.config.ConfigObject;
import com.typesafe.config.ConfigRenderOptions;
import java.util.Optional;
import java.util.concurrent.Executor;
import org.neo4j.configuration.Config;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import scala.concurrent.ExecutionContextExecutor;

public class ActorSystemFactory {

  public static final String ACTOR_SYSTEM_NAME = "cc-discovery-actor-system";
  private final LogProvider logProvider;
  private final Optional<SSLEngineProvider> sslEngineProvider;
  private final TypesafeConfigService configService;
  private final Executor executor;
  private final Log log;

  public ActorSystemFactory(Optional<SSLEngineProvider> sslEngineProvider, Executor executor,
      Config config, LogProvider logProvider) {
    this.executor = executor;
    this.logProvider = logProvider;
    this.sslEngineProvider = sslEngineProvider;
    TypesafeConfigService.ArteryTransport arteryTransport =
        sslEngineProvider.isPresent() ? TypesafeConfigService.ArteryTransport.TLS_TCP
            : TypesafeConfigService.ArteryTransport.TCP;
    this.configService = new TypesafeConfigService(arteryTransport, config);
    this.log = logProvider.getLog(this.getClass());
  }

  ActorSystem createActorSystem(ProviderSelection providerSelection) {
    com.typesafe.config.Config tsConfig = this.configService.generate();
    Log n10000 = this.log;
    ConfigObject n10001 = tsConfig.root();
    n10000.debug("Akka config: " + n10001.render(ConfigRenderOptions.concise()));
    ExecutionContextExecutor ec = ExecutionContexts.fromExecutor(this.executor);
    BootstrapSetup bootstrapSetup = BootstrapSetup.create(tsConfig)
        .withActorRefProvider(providerSelection).withDefaultExecutionContext(ec);
    ActorSystemSetup actorSystemSetup = ActorSystemSetup.create(bootstrapSetup);
    if (this.sslEngineProvider.isPresent()) {
      actorSystemSetup = actorSystemSetup.withSetup(SSLEngineProviderSetup.create((system) ->
      {
        return this.sslEngineProvider.get();
      }));
    }

    LoggingFilter.enable(this.logProvider);
    LoggingActor.enable(this.logProvider);
    ActorSystem actorSystem = ActorSystem.create("cc-discovery-actor-system", actorSystemSetup);
    LoggingActor.enable(actorSystem, this.logProvider);
    return actorSystem;
  }
}
