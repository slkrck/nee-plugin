/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import io.nee.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import io.nee.causalclustering.identity.MemberId;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.neo4j.kernel.database.DatabaseId;

public class ReplicatedDatabaseState {

  private final DatabaseId databaseId;
  private final Map<MemberId, DiscoveryDatabaseState> memberStates;
  private final boolean coreStates;
  private final String name;

  public ReplicatedDatabaseState(DatabaseId databaseId,
      Map<MemberId, DiscoveryDatabaseState> memberStates, boolean isCoreStates) {
    this.databaseId = databaseId;
    this.memberStates = memberStates;
    this.coreStates = isCoreStates;
    this.name = isCoreStates ? "CoreReplicatedDatabaseState" : "ReadReplicaReplicatedDatabaseState";
  }

  public static ReplicatedDatabaseState ofCores(DatabaseId databaseId,
      Map<MemberId, DiscoveryDatabaseState> memberStates) {
    return new ReplicatedDatabaseState(databaseId, memberStates, true);
  }

  public static ReplicatedDatabaseState ofReadReplicas(DatabaseId databaseId,
      Map<MemberId, DiscoveryDatabaseState> memberStates) {
    return new ReplicatedDatabaseState(databaseId, memberStates, false);
  }

  public DatabaseId databaseId() {
    return this.databaseId;
  }

  public Map<MemberId, DiscoveryDatabaseState> memberStates() {
    return this.memberStates;
  }

  public Optional<DiscoveryDatabaseState> stateFor(MemberId memberId) {
    return Optional.ofNullable(this.memberStates.get(memberId));
  }

  public boolean isEmpty() {
    return this.memberStates.isEmpty();
  }

  public boolean containsCoreStates() {
    return this.coreStates;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ReplicatedDatabaseState that = (ReplicatedDatabaseState) o;
      return this.coreStates == that.coreStates && Objects.equals(this.databaseId, that.databaseId)
          &&
          Objects.equals(this.memberStates, that.memberStates);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.databaseId, this.memberStates, this.coreStates);
  }

  public String toString() {
    return String.format("%s{%s}", this.name, this.memberStates);
  }
}
