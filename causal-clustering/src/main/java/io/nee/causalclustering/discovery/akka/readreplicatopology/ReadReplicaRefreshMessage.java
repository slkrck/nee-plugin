/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.ActorRef;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.akka.database.state.DiscoveryDatabaseState;
import io.nee.causalclustering.identity.MemberId;
import java.util.Map;
import java.util.Objects;
import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaRefreshMessage {

  private final ReadReplicaInfo readReplicaInfo;
  private final MemberId memberId;
  private final ActorRef clusterClient;
  private final ActorRef topologyClient;
  private final Map<DatabaseId, DiscoveryDatabaseState> databaseStates;

  public ReadReplicaRefreshMessage(ReadReplicaInfo readReplicaInfo, MemberId memberId,
      ActorRef clusterClient, ActorRef topologyClient,
      Map<DatabaseId, DiscoveryDatabaseState> databaseStates) {
    this.readReplicaInfo = readReplicaInfo;
    this.memberId = memberId;
    this.clusterClient = clusterClient;
    this.topologyClient = topologyClient;
    this.databaseStates = databaseStates;
  }

  public ReadReplicaInfo readReplicaInfo() {
    return this.readReplicaInfo;
  }

  public MemberId memberId() {
    return this.memberId;
  }

  public Map<DatabaseId, DiscoveryDatabaseState> databaseStates() {
    return this.databaseStates;
  }

  public ActorRef clusterClient() {
    return this.clusterClient;
  }

  public ActorRef topologyClientActorRef() {
    return this.topologyClient;
  }

  public String toString() {
    return "ReadReplicaRefreshMessage{readReplicaInfo=" + this.readReplicaInfo + ", memberId="
        + this.memberId + ", clusterClient=" + this.clusterClient +
        ", topologyClient=" + this.topologyClient + ", databaseStates=" + this.databaseStates + "}";
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ReadReplicaRefreshMessage that = (ReadReplicaRefreshMessage) o;
      return Objects.equals(this.readReplicaInfo, that.readReplicaInfo) && Objects
          .equals(this.memberId, that.memberId) &&
          Objects.equals(this.clusterClient, that.clusterClient) && Objects
          .equals(this.topologyClient, that.topologyClient) &&
          Objects.equals(this.databaseStates, that.databaseStates);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects
        .hash(this.readReplicaInfo, this.memberId, this.clusterClient, this.topologyClient,
            this.databaseStates);
  }
}
