/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.procedures;

import io.nee.causalclustering.core.consensus.RaftMachine;
import io.nee.causalclustering.discovery.RoleInfo;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.kernel.api.exceptions.Status.General;

public class CoreRoleProcedure extends RoleProcedure {

  public CoreRoleProcedure(DatabaseManager<?> databaseManager) {
    super(databaseManager);
  }

  RoleInfo role(DatabaseContext databaseContext) throws ProcedureException {
    RaftMachine raftMachine = databaseContext.dependencies().resolveDependency(RaftMachine.class);
    if (raftMachine == null) {
      throw new ProcedureException(General.UnknownError,
          "Unable to resolve role for database. This may be because the database is stopping."
      );
    } else {
      return raftMachine.isLeader() ? RoleInfo.LEADER : RoleInfo.FOLLOWER;
    }
  }
}
