/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.system;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.CoordinatedShutdown;
import akka.actor.ProviderSelection;
import akka.cluster.Cluster;
import akka.cluster.client.ClusterClientReceptionist;
import akka.cluster.ddata.DistributedData;
import akka.stream.ActorMaterializer;
import akka.stream.ActorMaterializerSettings;

public class ActorSystemComponents {

  private final ActorSystem actorSystem;
  private final CoordinatedShutdown coordinatedShutdown;
  private Cluster cluster;
  private ActorRef replicator;
  private ActorMaterializer materializer;
  private ClusterClientReceptionist clusterClientReceptionist;

  public ActorSystemComponents(ActorSystemFactory actorSystemFactory,
      ProviderSelection providerSelection) {
    this.actorSystem = actorSystemFactory.createActorSystem(providerSelection);
    this.coordinatedShutdown = CoordinatedShutdown.get(this.actorSystem);
  }

  public ActorSystem actorSystem() {
    return this.actorSystem;
  }

  public Cluster cluster() {
    if (this.cluster == null) {
      this.cluster = Cluster.get(this.actorSystem);
    }

    return this.cluster;
  }

  public ActorRef replicator() {
    if (this.replicator == null) {
      this.replicator = DistributedData.get(this.actorSystem).replicator();
    }

    return this.replicator;
  }

  ClusterClientReceptionist clusterClientReceptionist() {
    if (this.clusterClientReceptionist == null) {
      this.clusterClientReceptionist = ClusterClientReceptionist.get(this.actorSystem);
    }

    return this.clusterClientReceptionist;
  }

  ActorMaterializer materializer() {
    if (this.materializer == null) {
      ActorMaterializerSettings settings = ActorMaterializerSettings.create(this.actorSystem)
          .withDispatcher("discovery-dispatcher");
      this.materializer = ActorMaterializer.create(settings, this.actorSystem);
    }

    return this.materializer;
  }

  public CoordinatedShutdown coordinatedShutdown() {
    return this.coordinatedShutdown;
  }
}
