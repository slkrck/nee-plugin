/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.coretopology;

import akka.actor.AbstractLoggingActor;
import akka.actor.Address;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.Member;
import akka.japi.pf.ReceiveBuilder;
import java.util.Objects;
import java.util.stream.Stream;

public class ClusterDowningActor extends AbstractLoggingActor {

  private final Cluster cluster;

  public ClusterDowningActor(Cluster cluster) {
    this.cluster = cluster;
  }

  public static Props props(Cluster cluster) {
    return Props.create(ClusterDowningActor.class, () ->
    {
      return new ClusterDowningActor(cluster);
    });
  }

  public Receive createReceive() {
    return ReceiveBuilder.create().match(ClusterViewMessage.class, this::handle).build();
  }

  private void handle(ClusterViewMessage clusterView) {
    if (clusterView.mostAreReachable()) {
      this.log().info("Downing members: {}", clusterView.unreachable());
      Stream<Address> addressStream = clusterView.unreachable().stream().map(Member::address);
      Cluster n10001 = this.cluster;
      Objects.requireNonNull(n10001);
      addressStream.forEach(n10001::down);
    } else {
      this.log().info("In minority side of network partition? {}", clusterView);
    }
  }
}
