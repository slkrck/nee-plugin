/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery;

import io.nee.causalclustering.core.CausalClusteringSettings;
import java.util.Collection;
import java.util.concurrent.TimeoutException;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;

public abstract class RetryingHostnameResolver implements HostnameResolver {

  private final int minResolvedAddresses;
  private final RetryStrategy retryStrategy;

  RetryingHostnameResolver(Config config, RetryStrategy retryStrategy) {
    this.minResolvedAddresses = config
        .get(CausalClusteringSettings.minimum_core_cluster_size_at_formation);
    this.retryStrategy = retryStrategy;
  }

  static RetryStrategy defaultRetryStrategy(Config config) {
    long retryIntervalMillis = config
        .get(CausalClusteringSettings.discovery_resolution_retry_interval).toMillis();
    long clusterBindingTimeout = config.get(CausalClusteringSettings.discovery_resolution_timeout)
        .toMillis();
    long numRetries = clusterBindingTimeout / retryIntervalMillis + 1L;
    return new RetryStrategy(retryIntervalMillis, numRetries);
  }

  public final Collection<SocketAddress> resolve(SocketAddress advertisedSocketAddress) {
    try {
      return this.retryStrategy.apply(() ->
      {
        return this.resolveOnce(advertisedSocketAddress);
      }, (addrs) ->
      {
        return addrs.size() >= this.minResolvedAddresses;
      });
    } catch (TimeoutException n3) {
      return this.resolveOnce(advertisedSocketAddress);
    }
  }

  protected abstract Collection<SocketAddress> resolveOnce(SocketAddress n1);
}
