/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka;

import akka.actor.ActorRef;
import akka.cluster.Cluster;
import akka.cluster.ddata.Key;
import akka.cluster.ddata.ReplicatedData;
import akka.cluster.ddata.Replicator.Changed;
import akka.cluster.ddata.Replicator.ReplicatorMessage;
import akka.cluster.ddata.Replicator.Subscribe;
import akka.cluster.ddata.Replicator.Unsubscribe;
import akka.cluster.ddata.Replicator.Update;
import akka.cluster.ddata.Replicator.UpdateResponse;
import akka.cluster.ddata.Replicator.WriteAll;
import akka.cluster.ddata.Replicator.WriteConsistency;
import akka.japi.pf.ReceiveBuilder;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;
import scala.concurrent.duration.FiniteDuration;

public abstract class BaseReplicatedDataActor<T extends ReplicatedData> extends
    AbstractActorWithTimersAndLogging {

  private static final WriteConsistency WRITE_CONSISTENCY;
  private static final String METRIC_TIMER_KEY = "refresh metric";

  static {
    WRITE_CONSISTENCY = new WriteAll(new FiniteDuration(10L, TimeUnit.SECONDS));
  }

  protected final Cluster cluster;
  protected final ActorRef replicator;
  protected final Key<T> key;
  private final ReplicatedDataIdentifier identifier;
  private final Supplier<T> emptyData;
  private final ReplicatedDataMonitor monitor;
  protected T data;

  protected BaseReplicatedDataActor(Cluster cluster, ActorRef replicator,
      Function<String, Key<T>> keyFunction, Supplier<T> emptyData,
      ReplicatedDataIdentifier identifier, ReplicatedDataMonitor monitor) {
    this.cluster = cluster;
    this.replicator = replicator;
    this.key = keyFunction.apply(identifier.keyName());
    this.emptyData = emptyData;
    this.data = emptyData.get();
    this.identifier = identifier;
    this.monitor = monitor;
  }

  public final void preStart() {
    this.sendInitialDataToReplicator();
    this.subscribeToReplicatorEvents(new Subscribe(this.key, this.getSelf()));
    this.getTimers()
        .startPeriodicTimer("refresh metric", BaseReplicatedDataActor.MetricsRefresh.getInstance(),
            Duration.ofMinutes(1L));
  }

  protected abstract void sendInitialDataToReplicator();

  public final void postStop() {
    this.subscribeToReplicatorEvents(new Unsubscribe(this.key, this.getSelf()));
  }

  public final Receive createReceive() {
    ReceiveBuilder receiveBuilder = new ReceiveBuilder();
    this.handleCustomEvents(receiveBuilder);
    this.handleReplicationEvents(receiveBuilder);
    return receiveBuilder.build();
  }

  private void handleReplicationEvents(ReceiveBuilder builder) {
    builder.match(Changed.class, (c) ->
    {
      return c.key().equals(this.key);
    }, (message) ->
    {
      T newData = (T) message.dataValue();
      this.handleIncomingData(newData);
      this.logDataMetric();
    }).match(UpdateResponse.class, (updated) ->
    {
      this.log().debug("Update: {}", updated);
    }).match(BaseReplicatedDataActor.MetricsRefresh.class, (ignored) ->
    {
      this.logDataMetric();
    });
  }

  protected abstract void handleCustomEvents(ReceiveBuilder n1);

  protected abstract void handleIncomingData(T n1);

  private void logDataMetric() {
    this.monitor.setVisibleDataSize(this.identifier, this.dataMetricVisible());
    this.monitor.setInvisibleDataSize(this.identifier, this.dataMetricInvisible());
  }

  protected abstract int dataMetricVisible();

  protected abstract int dataMetricInvisible();

  private void subscribeToReplicatorEvents(ReplicatorMessage message) {
    this.replicator.tell(message, this.getSelf());
  }

  protected void modifyReplicatedData(Key<T> key, Function<T, T> modify) {
    this.modifyReplicatedData(key, modify, null);
  }

  protected <M> void modifyReplicatedData(Key<T> key, Function<T, T> modify, M message) {
    Update<T> update = new Update(key, this.emptyData.get(), WRITE_CONSISTENCY,
        Optional.ofNullable(message), modify);
    this.replicator.tell(update, this.self());
  }

  static class MetricsRefresh {

    private static final BaseReplicatedDataActor.MetricsRefresh instance = new BaseReplicatedDataActor.MetricsRefresh();

    private MetricsRefresh() {
    }

    public static BaseReplicatedDataActor.MetricsRefresh getInstance() {
      return instance;
    }
  }
}
