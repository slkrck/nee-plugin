/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.coretopology;

import akka.actor.ActorRef;
import akka.actor.Address;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.Member;
import akka.cluster.UniqueAddress;
import akka.stream.javadsl.SourceQueueWithComplete;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.discovery.DatabaseCoreTopology;
import io.nee.causalclustering.discovery.akka.AbstractActorWithTimersAndLogging;
import io.nee.causalclustering.discovery.akka.common.DatabaseStartedMessage;
import io.nee.causalclustering.discovery.akka.common.DatabaseStoppedMessage;
import io.nee.causalclustering.discovery.akka.monitoring.ClusterSizeMonitor;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;
import io.nee.causalclustering.discovery.member.DiscoveryMember;
import io.nee.causalclustering.identity.RaftId;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.DatabaseId;

public class CoreTopologyActor extends AbstractActorWithTimersAndLogging {

  public static final String NAME = "cc-core-topology-actor";
  private final SourceQueueWithComplete<CoreTopologyMessage> topologyUpdateSink;
  private final SourceQueueWithComplete<BootstrapState> bootstrapStateSink;
  private final TopologyBuilder topologyBuilder;
  private final int minCoreHostsAtRuntime;
  private final UniqueAddress myClusterAddress;
  private final Config config;
  private final ActorRef metadataActor;
  private final ActorRef raftIdActor;
  private final ActorRef readReplicaTopologyActor;
  private Set<DatabaseId> knownDatabaseIds = Collections.emptySet();
  private MetadataMessage memberData;
  private Set<RaftId> bootstrappedRafts;
  private ClusterViewMessage clusterView;

  private CoreTopologyActor(DiscoveryMember myself,
      SourceQueueWithComplete<CoreTopologyMessage> topologyUpdateSink,
      SourceQueueWithComplete<BootstrapState> bootstrapStateSink, ActorRef readReplicaTopologyActor,
      ActorRef replicator,
      Cluster cluster,
      TopologyBuilder topologyBuilder, Config config, ReplicatedDataMonitor replicatedDataMonitor,
      ClusterSizeMonitor clusterSizeMonitor) {
    this.topologyUpdateSink = topologyUpdateSink;
    this.bootstrapStateSink = bootstrapStateSink;
    this.readReplicaTopologyActor = readReplicaTopologyActor;
    this.topologyBuilder = topologyBuilder;
    this.minCoreHostsAtRuntime = config
        .get(CausalClusteringSettings.minimum_core_cluster_size_at_runtime);
    this.memberData = MetadataMessage.EMPTY;
    this.bootstrappedRafts = Collections.emptySet();
    this.clusterView = ClusterViewMessage.EMPTY;
    this.myClusterAddress = cluster.selfUniqueAddress();
    this.config = config;
    this.metadataActor = this.getContext().actorOf(MetadataActor
        .props(myself, cluster, replicator, this.getSelf(), config, replicatedDataMonitor));
    ActorRef downingActor = this.getContext().actorOf(ClusterDowningActor.props(cluster));
    this.getContext().actorOf(ClusterStateActor
        .props(cluster, this.getSelf(), downingActor, this.metadataActor, config,
            clusterSizeMonitor));
    this.raftIdActor =
        this.getContext().actorOf(RaftIdActor
            .props(cluster, replicator, this.getSelf(), replicatedDataMonitor,
                this.minCoreHostsAtRuntime));
  }

  public static Props props(DiscoveryMember myself,
      SourceQueueWithComplete<CoreTopologyMessage> topologyUpdateSink,
      SourceQueueWithComplete<BootstrapState> bootstrapStateSink, ActorRef rrTopologyActor,
      ActorRef replicator, Cluster cluster,
      TopologyBuilder topologyBuilder, Config config, ReplicatedDataMonitor replicatedDataMonitor,
      ClusterSizeMonitor clusterSizeMonitor) {
    return Props.create(CoreTopologyActor.class, () ->
    {
      return new CoreTopologyActor(myself, topologyUpdateSink, bootstrapStateSink, rrTopologyActor,
          replicator, cluster, topologyBuilder, config,
          replicatedDataMonitor, clusterSizeMonitor);
    });
  }

  public Receive createReceive() {
    return this.receiveBuilder().match(ClusterViewMessage.class, this::handleClusterViewMessage)
        .match(MetadataMessage.class,
            this::handleMetadataMessage)
        .match(BootstrappedRaftsMessage.class, this::handleBootstrappedRaftsMessage)
        .match(RaftIdSetRequest.class,
            this::handleRaftIdSetRequest)
        .match(DatabaseStartedMessage.class, this::handleDatabaseStartedMessage)
        .match(DatabaseStoppedMessage.class,
            this::handleDatabaseStoppedMessage).build();
  }

  private void handleClusterViewMessage(ClusterViewMessage message) {
    this.clusterView = message;
    this.buildTopologies();
  }

  private void handleMetadataMessage(MetadataMessage message) {
    this.memberData = message;
    this.buildTopologies();
  }

  private void handleBootstrappedRaftsMessage(BootstrappedRaftsMessage message) {
    this.bootstrappedRafts = message.bootstrappedRafts();
    this.buildTopologies();
  }

  private void handleRaftIdSetRequest(RaftIdSetRequest message) {
    this.raftIdActor.forward(message, this.getContext());
  }

  private void handleDatabaseStartedMessage(DatabaseStartedMessage message) {
    this.metadataActor.forward(message, this.context());
  }

  private void handleDatabaseStoppedMessage(DatabaseStoppedMessage message) {
    this.metadataActor.forward(message, this.context());
  }

  private void buildTopologies() {
    Set<DatabaseId> receivedDatabaseIds = this.memberData.getStream().flatMap((info) ->
    {
      return info.coreServerInfo().startedDatabaseIds().stream();
    }).collect(Collectors.toSet());
    Set<DatabaseId> absentDatabaseIds = this.knownDatabaseIds.stream().filter((id) ->
    {
      return !receivedDatabaseIds.contains(id);
    }).collect(Collectors.toSet());
    this.knownDatabaseIds = receivedDatabaseIds;
    absentDatabaseIds.forEach(this::buildTopology);
    receivedDatabaseIds.forEach(this::buildTopology);
  }

  private void buildTopology(DatabaseId databaseId) {
    this.log().debug(
        "Building new view of core topology from actor {}, cluster state is: {}, metadata is {}",
        this.myClusterAddress, this.clusterView,
        this.memberData);
    RaftId raftId = RaftId.from(databaseId);
    raftId = this.bootstrappedRafts.contains(raftId) ? raftId : null;
    DatabaseCoreTopology newCoreTopology = this.topologyBuilder
        .buildCoreTopology(databaseId, raftId, this.clusterView, this.memberData);
    this.log().debug("Returned topology: {}", newCoreTopology);
    Collection<Address> akkaMemberAddresses = this.clusterView.members().stream()
        .map(Member::address).filter((addr) ->
        {
          return !addr.equals(
              this.myClusterAddress
                  .address());
        })
        .collect(Collectors.toList());
    this.topologyUpdateSink.offer(new CoreTopologyMessage(newCoreTopology, akkaMemberAddresses));
    this.readReplicaTopologyActor.tell(newCoreTopology, this.getSelf());
    this.bootstrapStateSink.offer(
        new BootstrapState(this.clusterView, this.memberData, this.myClusterAddress, this.config));
  }
}
