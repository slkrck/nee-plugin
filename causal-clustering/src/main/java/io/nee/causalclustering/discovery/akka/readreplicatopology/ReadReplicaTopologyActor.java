/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.client.ClusterClientReceptionist;
import akka.japi.pf.ReceiveBuilder;
import akka.stream.javadsl.SourceQueueWithComplete;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.discovery.DatabaseCoreTopology;
import io.nee.causalclustering.discovery.DatabaseReadReplicaTopology;
import io.nee.causalclustering.discovery.ReplicatedDatabaseState;
import io.nee.causalclustering.discovery.akka.database.state.AllReplicatedDatabaseStates;
import io.nee.causalclustering.discovery.akka.directory.LeaderInfoDirectoryMessage;
import java.time.Clock;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.DatabaseId;

public class ReadReplicaTopologyActor extends AbstractLoggingActor {

  public static final String NAME = "cc-rr-topology-actor";
  private final SourceQueueWithComplete<DatabaseReadReplicaTopology> topologySink;
  private final SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink;
  private final Map<DatabaseId, DatabaseCoreTopology> coreTopologies = new HashMap();
  private final Map<DatabaseId, DatabaseReadReplicaTopology> readReplicaTopologies = new HashMap();
  private Map<DatabaseId, ReplicatedDatabaseState> coreMemberDbStates = new HashMap();
  private Map<DatabaseId, ReplicatedDatabaseState> rrMemberDbStates = new HashMap();
  private LeaderInfoDirectoryMessage databaseLeaderInfo;
  private Set<ActorRef> myClusterClients;
  private ReadReplicaViewMessage readReplicaViewMessage;

  ReadReplicaTopologyActor(SourceQueueWithComplete<DatabaseReadReplicaTopology> topologySink,
      SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink,
      ClusterClientReceptionist receptionist, Config config,
      Clock clock) {
    this.databaseLeaderInfo = LeaderInfoDirectoryMessage.EMPTY;
    this.myClusterClients = new HashSet();
    this.readReplicaViewMessage = ReadReplicaViewMessage.EMPTY;
    this.topologySink = topologySink;
    this.databaseStateSink = databaseStateSink;
    Duration refresh = config.get(CausalClusteringSettings.cluster_topology_refresh);
    Props readReplicaViewProps = ReadReplicaViewActor
        .props(this.getSelf(), receptionist, clock, refresh);
    this.getContext().actorOf(readReplicaViewProps);
    Props clusterClientViewProps = ClusterClientViewActor
        .props(this.getSelf(), receptionist.underlying());
    this.getContext().actorOf(clusterClientViewProps);
  }

  public static Props props(SourceQueueWithComplete<DatabaseReadReplicaTopology> topologySink,
      SourceQueueWithComplete<ReplicatedDatabaseState> databaseStateSink,
      ClusterClientReceptionist receptionist, Config config,
      Clock clock) {
    return Props.create(ReadReplicaTopologyActor.class, () ->
    {
      return new ReadReplicaTopologyActor(topologySink, databaseStateSink, receptionist, config,
          clock);
    });
  }

  public Receive createReceive() {
    return ReceiveBuilder.create()
        .match(AllReplicatedDatabaseStates.class, this::updateCoreDatabaseStates)
        .match(ClusterClientViewMessage.class,
            this::handleClusterClientView)
        .match(ReadReplicaViewMessage.class, this::handleReadReplicaView)
        .match(ReadReplicaViewActor.Tick.class,
            this::sendTopologiesToClients)
        .match(DatabaseCoreTopology.class, this::addCoreTopology)
        .match(LeaderInfoDirectoryMessage.class,
            this::setDatabaseLeaderInfo).build();
  }

  private void updateCoreDatabaseStates(AllReplicatedDatabaseStates msg) {
    this.coreMemberDbStates = msg.databaseStates();
  }

  private void handleReadReplicaView(ReadReplicaViewMessage msg) {
    this.readReplicaViewMessage = msg;
    this.rrMemberDbStates = msg.allReplicatedDatabaseStates();
    this.rrMemberDbStates.forEach((id, state) ->
    {
      this.databaseStateSink.offer(state);
    });
    this.buildTopologies();
  }

  private void handleClusterClientView(ClusterClientViewMessage msg) {
    this.myClusterClients = msg.clusterClients();
    this.buildTopologies();
  }

  private Stream<ActorRef> myTopologyClients() {
    Stream<ActorRef> clusterClients = this.myClusterClients.stream();
    ReadReplicaViewMessage m = this.readReplicaViewMessage;
    Objects.requireNonNull(m);
    return clusterClients.flatMap(m::topologyClient);
  }

  private void sendTopologiesToClients(ReadReplicaViewActor.Tick ignored) {
    this.log().debug("Sending core topologies to clients: {}", this.coreTopologies);
    this.log().debug("Sending read replica topologies to clients: {}", this.readReplicaTopologies);
    this.log().debug("Sending database leader info to clients: {}", this.databaseLeaderInfo);
    this.log().debug("Sending cores' database states to clients: {}", this.coreMemberDbStates);
    this.log()
        .debug("Sending read replicas' database states to clients: {}", this.rrMemberDbStates);
    this.myTopologyClients().forEach((client) ->
    {
      this.sendReadReplicaTopologiesTo(client);
      this.sendCoreTopologiesTo(client);
      client.tell(this.databaseLeaderInfo, this.getSelf());
      this.sendDatabaseStatesTo(client);
    });
  }

  private void sendReadReplicaTopologiesTo(ActorRef client) {
    Iterator n2 = this.readReplicaTopologies.values().iterator();

    while (n2.hasNext()) {
      DatabaseReadReplicaTopology readReplicaTopology = (DatabaseReadReplicaTopology) n2.next();
      client.tell(readReplicaTopology, this.getSelf());
    }
  }

  private void sendCoreTopologiesTo(ActorRef client) {
    Iterator n2 = this.coreTopologies.values().iterator();

    while (n2.hasNext()) {
      DatabaseCoreTopology coreTopology = (DatabaseCoreTopology) n2.next();
      client.tell(coreTopology, this.getSelf());
    }
  }

  private void sendDatabaseStatesTo(ActorRef client) {
    Iterator n2 = this.coreMemberDbStates.values().iterator();

    ReplicatedDatabaseState replicaState;
    while (n2.hasNext()) {
      replicaState = (ReplicatedDatabaseState) n2.next();
      client.tell(replicaState, this.getSelf());
    }

    n2 = this.rrMemberDbStates.values().iterator();

    while (n2.hasNext()) {
      replicaState = (ReplicatedDatabaseState) n2.next();
      client.tell(replicaState, this.getSelf());
    }
  }

  private void addCoreTopology(DatabaseCoreTopology coreTopology) {
    this.coreTopologies.put(coreTopology.databaseId(), coreTopology);
  }

  private void setDatabaseLeaderInfo(LeaderInfoDirectoryMessage leaderInfo) {
    this.databaseLeaderInfo = leaderInfo;
  }

  private void buildTopologies() {
    Set<DatabaseId> receivedDatabaseIds = this.readReplicaViewMessage.databaseIds();
    Set<DatabaseId> absentDatabaseIds = this.readReplicaTopologies.keySet().stream().filter((id) ->
    {
      return !receivedDatabaseIds.contains(id);
    }).collect(Collectors.toSet());
    absentDatabaseIds.forEach(this::buildTopology);
    receivedDatabaseIds.forEach(this::buildTopology);
  }

  private void buildTopology(DatabaseId databaseId) {
    this.log()
        .debug("Building read replica topology for database {} with read replicas: {}", databaseId,
            this.readReplicaViewMessage);
    DatabaseReadReplicaTopology readReplicaTopology = this.readReplicaViewMessage
        .toReadReplicaTopology(databaseId);
    this.log()
        .debug("Built read replica topology for database {}: {}", databaseId, readReplicaTopology);
    this.topologySink.offer(readReplicaTopology);
    if (readReplicaTopology.members().isEmpty()) {
      this.readReplicaTopologies.remove(databaseId);
    } else {
      this.readReplicaTopologies.put(databaseId, readReplicaTopology);
    }
  }
}
