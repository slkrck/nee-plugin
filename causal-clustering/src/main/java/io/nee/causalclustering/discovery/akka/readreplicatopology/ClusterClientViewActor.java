/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.akka.readreplicatopology;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.client.ClusterClientUnreachable;
import akka.cluster.client.ClusterClientUp;
import akka.cluster.client.ClusterClients;
import akka.cluster.client.SubscribeClusterClients;
import akka.cluster.client.UnsubscribeClusterClients;
import akka.japi.pf.ReceiveBuilder;
import java.util.HashSet;
import java.util.Set;

class ClusterClientViewActor extends AbstractLoggingActor {

  private final ActorRef parent;
  private final ActorRef receptionist;
  private final Set<ActorRef> clusterClients = new HashSet();

  private ClusterClientViewActor(ActorRef parent, ActorRef receptionist) {
    this.parent = parent;
    this.receptionist = receptionist;
  }

  static Props props(ActorRef parent, ActorRef receptionist) {
    return Props.create(ClusterClientViewActor.class, () ->
    {
      return new ClusterClientViewActor(parent, receptionist);
    });
  }

  public void preStart() {
    this.receptionist.tell(SubscribeClusterClients.getInstance(), this.getSelf());
  }

  public void postStop() {
    this.receptionist.tell(UnsubscribeClusterClients.getInstance(), this.getSelf());
  }

  public Receive createReceive() {
    return ReceiveBuilder.create().match(ClusterClients.class, this::handleClusterClients)
        .match(ClusterClientUp.class,
            this::handleClusterClientUp)
        .match(ClusterClientUnreachable.class, this::handleClusterClientUnreachable).build();
  }

  private void handleClusterClients(ClusterClients msg) {
    this.log().debug("All cluster clients: {}", msg);
    this.clusterClients.addAll(msg.getClusterClients());
    this.sendToParent();
  }

  private void handleClusterClientUp(ClusterClientUp msg) {
    this.log().debug("Cluster client up: {}", msg);
    this.clusterClients.add(msg.clusterClient());
    this.sendToParent();
  }

  private void handleClusterClientUnreachable(ClusterClientUnreachable msg) {
    this.log().debug("Cluster client down: {}", msg);
    this.clusterClients.remove(msg.clusterClient());
    this.sendToParent();
  }

  private void sendToParent() {
    this.parent.tell(new ClusterClientViewMessage(this.clusterClients), this.getSelf());
  }
}
