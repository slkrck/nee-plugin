/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.discovery.procedures;

import io.nee.causalclustering.discovery.ClientConnectorAddresses;
import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.discovery.DiscoveryServerInfo;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.RoleInfo;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.identity.MemberId;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.collection.RawIterator;
import org.neo4j.internal.helpers.collection.Iterators;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes;
import org.neo4j.internal.kernel.api.procs.ProcedureSignature;
import org.neo4j.internal.kernel.api.procs.QualifiedName;
import org.neo4j.kernel.api.ResourceTracker;
import org.neo4j.kernel.api.procedure.CallableProcedure.BasicProcedure;
import org.neo4j.kernel.api.procedure.Context;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.MapValueBuilder;
import org.neo4j.values.virtual.VirtualValues;

public class ClusterOverviewProcedure extends BasicProcedure {

  public static final String PROCEDURE_NAME = "overview";
  private static final String[] PROCEDURE_NAMESPACE = new String[]{"dbms", "cluster"};
  private final TopologyService topologyService;
  private final DatabaseIdRepository databaseIdRepository;

  public ClusterOverviewProcedure(TopologyService topologyService,
      DatabaseIdRepository databaseIdRepository) {
    super(ProcedureSignature.procedureSignature(new QualifiedName(PROCEDURE_NAMESPACE, "overview"))
        .out("id", Neo4jTypes.NTString).out("addresses",
            Neo4jTypes
                .NTList(
                    Neo4jTypes.NTString))
        .out("databases", Neo4jTypes.NTMap).out("groups",
            Neo4jTypes.NTList(Neo4jTypes.NTString)).description(
            "Overview of all currently accessible cluster members, their databases and roles.")
        .systemProcedure().build());
    this.topologyService = topologyService;
    this.databaseIdRepository = databaseIdRepository;
  }

  private static AnyValue[] formatResultRow(ClusterOverviewProcedure.ResultRow row) {
    return new AnyValue[]{Values.stringValue(row.memberId.toString()), formatAddresses(row),
        formatDatabases(row), formatGroups(row)};
  }

  private static AnyValue formatAddresses(ClusterOverviewProcedure.ResultRow row) {
    List<AnyValue> stringValues = row.addresses.uriList().stream().map(URI::toString)
        .map(Values::stringValue).collect(Collectors.toList());
    return VirtualValues.fromList(stringValues);
  }

  private static AnyValue formatDatabases(ClusterOverviewProcedure.ResultRow row) {
    MapValueBuilder builder = new MapValueBuilder();
    Iterator n2 = row.databases.entrySet().iterator();

    while (n2.hasNext()) {
      Entry<NamedDatabaseId, RoleInfo> entry = (Entry) n2.next();
      NamedDatabaseId databaseId = entry.getKey();
      String roleString = entry.getValue().toString();
      builder.add(databaseId.name(), Values.stringValue(roleString));
    }

    return builder.build();
  }

  private static AnyValue formatGroups(ClusterOverviewProcedure.ResultRow row) {
    List<AnyValue> stringValues = row.groups.stream().sorted().map(Values::stringValue)
        .collect(Collectors.toList());
    return VirtualValues.fromList(stringValues);
  }

  public RawIterator<AnyValue[], ProcedureException> apply(Context ctx, AnyValue[] input,
      ResourceTracker resourceTracker) {
    ArrayList<ClusterOverviewProcedure.ResultRow> resultRows = new ArrayList();
    Iterator n5 = this.topologyService.allCoreServers().entrySet().iterator();

    Entry entry;
    ClusterOverviewProcedure.ResultRow row;
    while (n5.hasNext()) {
      entry = (Entry) n5.next();
      row = this
          .buildResultRowForCore((MemberId) entry.getKey(), (CoreServerInfo) entry.getValue());
      resultRows.add(row);
    }

    n5 = this.topologyService.allReadReplicas().entrySet().iterator();

    while (n5.hasNext()) {
      entry = (Entry) n5.next();
      row = this.buildResultRowForReadReplica((MemberId) entry.getKey(),
          (ReadReplicaInfo) entry.getValue());
      resultRows.add(row);
    }

    Stream<AnyValue[]> resultStream = resultRows.stream().sorted()
        .map(ClusterOverviewProcedure::formatResultRow);
    return Iterators.asRawIterator(resultStream);
  }

  private ClusterOverviewProcedure.ResultRow buildResultRowForCore(MemberId memberId,
      CoreServerInfo coreInfo) {
    return this.buildResultRow(memberId, coreInfo, (databaseId) ->
    {
      return this.topologyService.lookupRole(databaseId, memberId);
    });
  }

  private ClusterOverviewProcedure.ResultRow buildResultRowForReadReplica(MemberId memberId,
      ReadReplicaInfo readReplicaInfo) {
    return this.buildResultRow(memberId, readReplicaInfo, (ignore) ->
    {
      return RoleInfo.READ_REPLICA;
    });
  }

  private ClusterOverviewProcedure.ResultRow buildResultRow(MemberId memberId,
      DiscoveryServerInfo serverInfo, Function<NamedDatabaseId, RoleInfo> result) {
    Map<NamedDatabaseId, RoleInfo> databases = serverInfo.startedDatabaseIds().stream()
        .flatMap((databaseId) ->
        {
          return this.databaseIdRepository
              .getById(databaseId).stream();
        })
        .collect(Collectors.toMap(Function.identity(), result));
    return new ClusterOverviewProcedure.ResultRow(memberId.getUuid(), serverInfo.connectors(),
        databases, serverInfo.groups());
  }

  static class ResultRow implements Comparable<ClusterOverviewProcedure.ResultRow> {

    final UUID memberId;
    final ClientConnectorAddresses addresses;
    final Map<NamedDatabaseId, RoleInfo> databases;
    final Set<String> groups;

    ResultRow(UUID memberId, ClientConnectorAddresses addresses,
        Map<NamedDatabaseId, RoleInfo> databases, Set<String> groups) {
      this.memberId = memberId;
      this.addresses = addresses;
      this.databases = databases;
      this.groups = groups;
    }

    public int compareTo(ClusterOverviewProcedure.ResultRow other) {
      return this.memberId.compareTo(other.memberId);
    }
  }
}
