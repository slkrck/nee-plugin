/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.error_handling;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.util.Preconditions;

public class PanicService {

  private final Map<NamedDatabaseId, PanicService.DatabasePanicEventHandlers> handlersByDatabase = new ConcurrentHashMap();
  private final Executor executor;
  private final Log log;

  public PanicService(JobScheduler jobScheduler, LogService logService) {
    this.executor = jobScheduler.executor(Group.PANIC_SERVICE);
    this.log = logService.getUserLog(this.getClass());
  }

  public void addPanicEventHandlers(NamedDatabaseId namedDatabaseId,
      List<? extends DatabasePanicEventHandler> handlers) {
    PanicService.DatabasePanicEventHandlers newHandlers = new PanicService.DatabasePanicEventHandlers(
        handlers);
    PanicService.DatabasePanicEventHandlers oldHandlers =
        this.handlersByDatabase.putIfAbsent(namedDatabaseId, newHandlers);
    Preconditions
        .checkState(oldHandlers == null, "Panic handlers for database %s are already installed",
            namedDatabaseId.name());
  }

  public void removePanicEventHandlers(NamedDatabaseId namedDatabaseId) {
    this.handlersByDatabase.remove(namedDatabaseId);
  }

  public DatabasePanicker panickerFor(NamedDatabaseId namedDatabaseId) {
    return (error) ->
    {
      return CompletableFuture.runAsync(() ->
      {
        this.panic(namedDatabaseId, error);
      }, this.executor);
    };
  }

  private void panic(NamedDatabaseId namedDatabaseId, Throwable error) {
    this.log.error("Clustering components for database '" + namedDatabaseId.name()
        + "' have encountered a critical error", error);
    PanicService.DatabasePanicEventHandlers handlers = this.handlersByDatabase.get(namedDatabaseId);
    if (handlers != null) {
      handlers.handlePanic(error);
    }
  }

  private class DatabasePanicEventHandlers {

    final List<? extends DatabasePanicEventHandler> handlers;
    final AtomicBoolean panicked;

    DatabasePanicEventHandlers(List<? extends DatabasePanicEventHandler> handlers) {
      this.handlers = handlers;
      this.panicked = new AtomicBoolean();
    }

    void handlePanic(Throwable cause) {
      if (this.panicked.compareAndSet(false, true)) {
        Iterator n2 = this.handlers.iterator();

        while (n2.hasNext()) {
          DatabasePanicEventHandler handler = (DatabasePanicEventHandler) n2.next();

          try {
            handler.onPanic(cause);
          } catch (Throwable n5) {
            PanicService.this.log.error("Failed to handle a panic event", n5);
          }
        }
      }
    }
  }
}
