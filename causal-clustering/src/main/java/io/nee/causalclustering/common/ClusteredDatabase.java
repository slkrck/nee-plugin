/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.common;

import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.kernel.lifecycle.LifecycleException;

public class ClusteredDatabase {

  private final LifeSupport components = new LifeSupport();
  private boolean hasBeenStarted;

  public final void start() {
    if (this.hasBeenStarted) {
      throw new IllegalStateException("Clustered databases do not support component reuse.");
    } else {
      this.hasBeenStarted = true;

      try {
        this.components.start();
      } catch (LifecycleException n4) {
        try {
          this.components.shutdown();
        } catch (Throwable n3) {
          n4.addSuppressed(n3);
        }

        throw n4;
      }
    }
  }

  public final void stop() {
    this.components.shutdown();
  }

  public final boolean hasBeenStarted() {
    return this.hasBeenStarted;
  }

  protected void addComponent(Lifecycle component) {
    this.components.add(component);
  }
}
