/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.common;

import io.nee.causalclustering.catchup.CatchupServerBuilder;
import io.nee.causalclustering.catchup.CatchupServerHandler;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.net.BootstrapConfiguration;
import io.nee.causalclustering.net.Server;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import io.nee.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import io.nee.kernel.impl.enterprise.configuration.OnlineBackupSettings;
import io.netty.channel.ChannelInboundHandler;
import java.util.Collection;
import java.util.Optional;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.JobScheduler;

public class TransactionBackupServiceProvider {

  public static final String BACKUP_SERVER_NAME = "backup-server";
  private final LogProvider logProvider;
  private final ChannelInboundHandler parentHandler;
  private final ApplicationSupportedProtocols catchupProtocols;
  private final Collection<ModifierSupportedProtocols> supportedModifierProtocols;
  private final NettyPipelineBuilderFactory serverPipelineBuilderFactory;
  private final CatchupServerHandler catchupServerHandler;
  private final JobScheduler scheduler;
  private final ConnectorPortRegister portRegister;

  public TransactionBackupServiceProvider(LogProvider logProvider,
      ApplicationSupportedProtocols catchupProtocols,
      Collection<ModifierSupportedProtocols> supportedModifierProtocols,
      NettyPipelineBuilderFactory serverPipelineBuilderFactory,
      CatchupServerHandler catchupServerHandler, ChannelInboundHandler parentHandler,
      JobScheduler scheduler,
      ConnectorPortRegister portRegister) {
    this.logProvider = logProvider;
    this.parentHandler = parentHandler;
    this.catchupProtocols = catchupProtocols;
    this.supportedModifierProtocols = supportedModifierProtocols;
    this.serverPipelineBuilderFactory = serverPipelineBuilderFactory;
    this.catchupServerHandler = catchupServerHandler;
    this.scheduler = scheduler;
    this.portRegister = portRegister;
  }

  public Optional<Server> resolveIfBackupEnabled(Config config) {
    if (config.get(OnlineBackupSettings.online_backup_enabled)) {
      SocketAddress backupAddress = config.get(OnlineBackupSettings.online_backup_listen_address);
      this.logProvider.getLog(TransactionBackupServiceProvider.class)
          .info("Binding backup service on address %s", backupAddress);
      Server catchupServer = CatchupServerBuilder.builder()
          .catchupServerHandler(this.catchupServerHandler).catchupProtocols(
              this.catchupProtocols).modifierProtocols(this.supportedModifierProtocols)
          .pipelineBuilder(
              this.serverPipelineBuilderFactory).installedProtocolsHandler(this.parentHandler)
          .listenAddress(backupAddress).scheduler(
              this.scheduler).config(config)
          .bootstrapConfig(BootstrapConfiguration.serverConfig(config)).portRegister(
              this.portRegister).debugLogProvider(this.logProvider).serverName("backup-server")
          .handshakeTimeout(
              config.get(CausalClusteringSettings.handshake_timeout)).build();
      return Optional.of(catchupServer);
    } else {
      return Optional.empty();
    }
  }
}
