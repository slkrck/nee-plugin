/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.common;

import io.nee.kernel.impl.enterprise.EnterpriseConstraintSemantics;
import io.nee.kernel.impl.enterprise.transaction.log.checkpoint.ConfigurableIOLimiter;
import io.nee.kernel.impl.net.DefaultNetworkConnectionTracker;
import java.time.Duration;
import java.util.function.Predicate;
import org.neo4j.bolt.dbapi.BoltGraphDatabaseManagementServiceSPI;
import org.neo4j.bolt.dbapi.impl.BoltKernelDatabaseManagementServiceProvider;
import org.neo4j.bolt.txtracking.DefaultReconciledTransactionTracker;
import org.neo4j.bolt.txtracking.ReconciledTransactionTracker;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.function.Predicates;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.graphdb.factory.module.edition.AbstractEditionModule;
import org.neo4j.kernel.api.net.NetworkConnectionTracker;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.time.SystemNanoClock;

public abstract class ClusteringEditionModule extends AbstractEditionModule {

  protected final ReconciledTransactionTracker reconciledTxTracker;

  protected ClusteringEditionModule(GlobalModule globalModule) {
    this.reconciledTxTracker = new DefaultReconciledTransactionTracker(
        globalModule.getLogService());
  }

  public static Predicate<String> fileWatcherFileNameFilter() {
    return Predicates.any(new Predicate[]{(fileName) ->
    {
      return ((String) fileName).startsWith("neostore.transaction.db");
    }, (filename) ->
    {
      return ((String) filename).endsWith(".cacheprof");
    }});
  }

  protected void editionInvariants(GlobalModule globalModule, Dependencies dependencies) {
    this.ioLimiter = new ConfigurableIOLimiter(globalModule.getGlobalConfig());
    this.constraintSemantics = new EnterpriseConstraintSemantics();
    this.connectionTracker = dependencies.satisfyDependency(this.createConnectionTracker());
  }

  protected NetworkConnectionTracker createConnectionTracker() {
    return new DefaultNetworkConnectionTracker();
  }

  public BoltGraphDatabaseManagementServiceSPI createBoltDatabaseManagementServiceProvider(
      Dependencies dependencies,
      DatabaseManagementService managementService, Monitors monitors,
      SystemNanoClock clock, LogService logService) {
    Config config = dependencies.resolveDependency(Config.class);
    Duration bookmarkAwaitDuration = config.get(GraphDatabaseSettings.bookmark_ready_timeout);
    return new BoltKernelDatabaseManagementServiceProvider(managementService,
        this.reconciledTxTracker, monitors, clock, bookmarkAwaitDuration);
  }
}
