/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.common.state;

import io.nee.causalclustering.core.consensus.membership.RaftMembershipState;
import io.nee.causalclustering.core.consensus.term.TermState;
import io.nee.causalclustering.core.consensus.vote.VoteState;
import io.nee.causalclustering.core.replication.session.GlobalSessionTrackerState;
import io.nee.causalclustering.core.state.ClusterStateLayout;
import io.nee.causalclustering.core.state.CoreStateFiles;
import io.nee.causalclustering.core.state.machines.lease.ReplicatedLeaseState;
import io.nee.causalclustering.core.state.storage.DurableStateStorage;
import io.nee.causalclustering.core.state.storage.SimpleFileStorage;
import io.nee.causalclustering.core.state.storage.SimpleStorage;
import io.nee.causalclustering.core.state.storage.StateStorage;
import io.nee.causalclustering.core.state.version.ClusterStateVersion;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.identity.RaftId;
import java.io.File;
import java.io.IOException;
import java.util.Objects;
import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.FileSystemUtils;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.DatabaseLog;
import org.neo4j.logging.internal.DatabaseLogProvider;

public class ClusterStateStorageFactory {

  private final FileSystemAbstraction fs;
  private final LogProvider globalLogProvider;
  private final ClusterStateLayout layout;
  private final Config config;

  public ClusterStateStorageFactory(FileSystemAbstraction fs, ClusterStateLayout layout,
      LogProvider globalLogProvider, Config config) {
    this.fs = fs;
    this.globalLogProvider = globalLogProvider;
    this.layout = layout;
    this.config = config;
  }

  public SimpleStorage<ClusterStateVersion> createClusterStateVersionStorage() {
    return this.createSimpleStorage(this.layout.clusterStateVersionFile(), CoreStateFiles.VERSION,
        this.globalLogProvider);
  }

  public SimpleStorage<MemberId> createMemberIdStorage() {
    return this.createSimpleStorage(this.layout.memberIdStateFile(), CoreStateFiles.CORE_MEMBER_ID,
        this.globalLogProvider);
  }

  public SimpleStorage<RaftId> createRaftIdStorage(String databaseName,
      DatabaseLogProvider logProvider) {
    return this
        .createSimpleStorage(this.layout.raftIdStateFile(databaseName), CoreStateFiles.RAFT_ID,
            logProvider);
  }

  public StateStorage<ReplicatedLeaseState> createLeaseStorage(String databaseName,
      LifeSupport life, DatabaseLogProvider logProvider) {
    return this
        .createDurableStorage(this.layout.leaseStateDirectory(databaseName), CoreStateFiles.LEASE,
            life, logProvider);
  }

  public StateStorage<Long> createLastFlushedStorage(String databaseName, LifeSupport life,
      DatabaseLogProvider logProvider) {
    return this.createDurableStorage(this.layout.lastFlushedStateDirectory(databaseName),
        CoreStateFiles.LAST_FLUSHED, life, logProvider);
  }

  public StateStorage<RaftMembershipState> createRaftMembershipStorage(String databaseName,
      LifeSupport life, DatabaseLogProvider logProvider) {
    return this.createDurableStorage(this.layout.raftMembershipStateDirectory(databaseName),
        CoreStateFiles.RAFT_MEMBERSHIP, life, logProvider);
  }

  public StateStorage<GlobalSessionTrackerState> createSessionTrackerStorage(String databaseName,
      LifeSupport life, DatabaseLogProvider logProvider) {
    return this.createDurableStorage(this.layout.sessionTrackerDirectory(databaseName),
        CoreStateFiles.SESSION_TRACKER, life, logProvider);
  }

  public StateStorage<TermState> createRaftTermStorage(String databaseName, LifeSupport life,
      DatabaseLogProvider logProvider) {
    return this.createDurableStorage(this.layout.raftTermStateDirectory(databaseName),
        CoreStateFiles.RAFT_TERM, life, logProvider);
  }

  public StateStorage<VoteState> createRaftVoteStorage(String databaseName, LifeSupport life,
      DatabaseLogProvider logProvider) {
    return this.createDurableStorage(this.layout.raftVoteStateDirectory(databaseName),
        CoreStateFiles.RAFT_VOTE, life, logProvider);
  }

  private <T> SimpleStorage<T> createSimpleStorage(File file, CoreStateFiles<T> type,
      LogProvider logProvider) {
    return new SimpleFileStorage(this.fs, file, type.marshal(), logProvider);
  }

  private <T> StateStorage<T> createDurableStorage(File directory, CoreStateFiles<T> type,
      LifeSupport life, LogProvider logProvider) {
    DurableStateStorage<T> storage = new DurableStateStorage(this.fs, directory, type,
        type.rotationSize(this.config), logProvider);
    life.add(storage);
    return storage;
  }

  public void clearFor(NamedDatabaseId id, DatabaseLogProvider logProvider) throws IOException {
    File clusterStateForDb = this.layout.raftGroupDir(id.name());
    if (this.fs.fileExists(clusterStateForDb)) {
      File idStateFile = this.layout.raftIdStateFile(id.name());
      if (idStateFile.exists()) {
        SimpleStorage<RaftId> raftIdSimpleStorage =
            this.createSimpleStorage(this.layout.raftIdStateFile(id.name()), CoreStateFiles.RAFT_ID,
                logProvider);
        RaftId raftId = raftIdSimpleStorage.readState();
        if (!Objects.equals(id.databaseId().uuid(), raftId.uuid())) {
          DatabaseLog log = logProvider.getLog(this.getClass());
          log.warn(String.format(
              "There was orphan cluster state belonging to a previous database %s with a different id {Old:%s New:%s} This likely means a previous DROP did not complete successfully.",
              id.name(), raftId.uuid(), id.databaseId().uuid()));
          FileSystemUtils.deleteFile(this.fs, clusterStateForDb);
        }
      }
    }
  }

  public ClusterStateLayout layout() {
    return this.layout;
  }
}
