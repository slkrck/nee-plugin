/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.common;

import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import org.neo4j.configuration.ssl.SslPolicyScope;
import org.neo4j.ssl.SslPolicy;
import org.neo4j.ssl.config.SslPolicyLoader;

public final class PipelineBuilders {

  private final NettyPipelineBuilderFactory clientPipelineBuilderFactory;
  private final NettyPipelineBuilderFactory serverPipelineBuilderFactory;
  private final NettyPipelineBuilderFactory backupServerPipelineBuilderFactory;

  public PipelineBuilders(SslPolicyLoader sslPolicyLoader) {
    SslPolicy clusterSslPolicy =
        sslPolicyLoader.hasPolicyForSource(SslPolicyScope.CLUSTER) ? sslPolicyLoader
            .getPolicy(SslPolicyScope.CLUSTER) : null;
    SslPolicy backupSslPolicy =
        sslPolicyLoader.hasPolicyForSource(SslPolicyScope.BACKUP) ? sslPolicyLoader
            .getPolicy(SslPolicyScope.BACKUP) : null;
    this.clientPipelineBuilderFactory = new NettyPipelineBuilderFactory(clusterSslPolicy);
    this.serverPipelineBuilderFactory = new NettyPipelineBuilderFactory(clusterSslPolicy);
    this.backupServerPipelineBuilderFactory = new NettyPipelineBuilderFactory(backupSslPolicy);
  }

  public NettyPipelineBuilderFactory client() {
    return this.clientPipelineBuilderFactory;
  }

  public NettyPipelineBuilderFactory server() {
    return this.serverPipelineBuilderFactory;
  }

  public NettyPipelineBuilderFactory backupServer() {
    return this.backupServerPipelineBuilderFactory;
  }
}
