/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ErrorHandler implements AutoCloseable {

  private final List<Throwable> throwables = new ArrayList();
  private final String message;

  public ErrorHandler(String message) {
    this.message = message;
  }

  public static void runAll(String description, ErrorHandler.ThrowingRunnable... actions)
      throws RuntimeException {
    ErrorHandler errorHandler = new ErrorHandler(description);

    try {
      ErrorHandler.ThrowingRunnable[] n3 = actions;
      int n4 = actions.length;

      for (int n5 = 0; n5 < n4; ++n5) {
        ErrorHandler.ThrowingRunnable action = n3[n5];
        errorHandler.execute(action);
      }
    } catch (Throwable n8) {
      try {
        errorHandler.close();
      } catch (Throwable n7) {
        n8.addSuppressed(n7);
      }

      throw n8;
    }

    errorHandler.close();
  }

  public void execute(ErrorHandler.ThrowingRunnable throwingRunnable) {
    try {
      throwingRunnable.run();
    } catch (Throwable n3) {
      this.throwables.add(n3);
    }
  }

  public void add(Throwable throwable) {
    this.throwables.add(throwable);
  }

  public void close() throws RuntimeException {
    this.throwIfException();
  }

  private void throwIfException() {
    RuntimeException runtimeException = null;
    Iterator n2 = this.throwables.iterator();

    while (n2.hasNext()) {
      Throwable throwable = (Throwable) n2.next();
      if (runtimeException == null) {
        runtimeException = new RuntimeException(this.message, throwable);
      } else {
        runtimeException.addSuppressed(throwable);
      }
    }

    if (runtimeException != null) {
      throw runtimeException;
    }
  }

  public interface ThrowingRunnable {

    void run() throws Throwable;
  }
}
