/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.helper;

import java.util.OptionalLong;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;
import org.neo4j.logging.Log;

public final class OperationProgressMonitor<T> {

  private final Future<T> future;
  private final long inactivityTimeoutMillis;
  private final Supplier<OptionalLong> millisSinceLastResponseSupplier;
  private final Log log;

  private OperationProgressMonitor(Future<T> future, long inactivityTimeoutMillis,
      Supplier<OptionalLong> millisSinceLastResponseSupplier, Log log) {
    this.future = future;
    this.inactivityTimeoutMillis = inactivityTimeoutMillis;
    this.millisSinceLastResponseSupplier = millisSinceLastResponseSupplier;
    this.log = log;
  }

  public static <T> OperationProgressMonitor<T> of(Future<T> future, long inactivityTimeoutMillis,
      Supplier<OptionalLong> millisSinceLastResponseSupplier,
      Log log) {
    return new OperationProgressMonitor(future, inactivityTimeoutMillis,
        millisSinceLastResponseSupplier, log);
  }

  public T get() throws ExecutionException, InterruptedException, TimeoutException {
    long remainingTimeoutMillis = this.inactivityTimeoutMillis;

    while (true) {
      try {
        return this.future.get(remainingTimeoutMillis, TimeUnit.MILLISECONDS);
      } catch (ExecutionException | InterruptedException n5) {
        this.future.cancel(false);
        throw n5;
      } catch (TimeoutException n6) {
        OptionalLong millisSinceLastResponse = this.millisSinceLastResponseSupplier.get();
        if (!millisSinceLastResponse.isPresent()) {
          this.throwOnTimeout(
              "Request timed out with no responses after " + this.inactivityTimeoutMillis + " ms.");
        } else if (millisSinceLastResponse.getAsLong() < this.inactivityTimeoutMillis) {
          remainingTimeoutMillis =
              this.inactivityTimeoutMillis - millisSinceLastResponse.getAsLong();
        } else {
          this.throwOnTimeout(
              "Request timed out after period of inactivity. Time since last response: "
                  + millisSinceLastResponse + " ms.");
        }
      }
    }
  }

  private void throwOnTimeout(String message) throws TimeoutException {
    this.log.warn(message);
    this.future.cancel(false);
    throw new TimeoutException(message);
  }
}
