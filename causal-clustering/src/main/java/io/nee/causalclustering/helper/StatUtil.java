/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.helper;

import java.util.concurrent.TimeUnit;
import org.neo4j.logging.FormattedLogProvider;
import org.neo4j.logging.Log;
import org.neo4j.time.Stopwatch;

public class StatUtil {

  private StatUtil() {
  }

  public static synchronized StatUtil.StatContext create(String name, long printEvery,
      boolean clearAfterPrint) {
    return create(name, FormattedLogProvider.toOutputStream(System.out).getLog(name), printEvery,
        clearAfterPrint);
  }

  public static synchronized StatUtil.StatContext create(String name, Log log, long printEvery,
      boolean clearAfterPrint) {
    return new StatUtil.StatContext(name, log, printEvery, clearAfterPrint);
  }

  private static class BasicStats {

    private Double min;
    private Double max;
    private Double avg = 0.0D;
    private long count;

    void collect(double val) {
      ++this.count;
      this.avg = this.avg + (val - this.avg) / (double) this.count;
      this.min = this.min == null ? val : Math.min(this.min, val);
      this.max = this.max == null ? val : Math.max(this.max, val);
    }

    public String toString() {
      return String
          .format("{min=%s, max=%s, avg=%s, count=%d}", this.min, this.max, this.avg, this.count);
    }
  }

  public static class TimingContext {

    private final StatUtil.StatContext context;
    private final Stopwatch startTime = Stopwatch.start();

    TimingContext(StatUtil.StatContext context) {
      this.context = context;
    }

    public void end() {
      this.context.collect((double) this.startTime.elapsed(TimeUnit.MILLISECONDS));
    }
  }

  public static class StatContext {

    private static final int N_BUCKETS = 10;
    private final String name;
    private final Log log;
    private final long printEvery;
    private final boolean clearAfterPrint;
    private final StatUtil.BasicStats[] bucket = new StatUtil.BasicStats[10];
    private long totalCount;

    private StatContext(String name, Log log, long printEvery, boolean clearAfterPrint) {
      this.name = name;
      this.log = log;
      this.printEvery = printEvery;
      this.clearAfterPrint = clearAfterPrint;
      this.clear();
    }

    public synchronized void clear() {
      for (int i = 0; i < 10; ++i) {
        this.bucket[i] = new StatUtil.BasicStats();
      }

      this.totalCount = 0L;
    }

    public void collect(double value) {
      int bucketIndex = this.bucketFor(value);
      synchronized (this) {
        ++this.totalCount;
        this.bucket[bucketIndex].collect(value);
        if (this.totalCount % this.printEvery == 0L) {
          this.print();
        }
      }
    }

    private int bucketFor(double value) {
      int bucketIndex;
      if (value <= 0.0D) {
        bucketIndex = 0;
      } else {
        bucketIndex = (int) Math.log10(value);
        bucketIndex = Math.min(bucketIndex, 9);
      }

      return bucketIndex;
    }

    public StatUtil.TimingContext time() {
      return new StatUtil.TimingContext(this);
    }

    public synchronized void print() {
      StatUtil.BasicStats[] n1 = this.bucket;
      int n2 = n1.length;

      for (int n3 = 0; n3 < n2; ++n3) {
        StatUtil.BasicStats stats = n1[n3];
        if (stats.count > 0L) {
          this.log.info(String.format("%s%s", this.name, stats));
        }
      }

      if (this.clearAfterPrint) {
        this.clear();
      }
    }
  }
}
