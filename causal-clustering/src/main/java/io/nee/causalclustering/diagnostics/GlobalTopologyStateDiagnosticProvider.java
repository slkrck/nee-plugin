/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.diagnostics;

import io.nee.causalclustering.discovery.TopologyService;
import org.neo4j.internal.diagnostics.DiagnosticsProvider;
import org.neo4j.internal.helpers.Strings;
import org.neo4j.logging.Logger;

public class GlobalTopologyStateDiagnosticProvider implements DiagnosticsProvider {

  private final TopologyService topologyService;

  public GlobalTopologyStateDiagnosticProvider(TopologyService topologyService) {
    this.topologyService = topologyService;
  }

  private static String newPaddedLIne() {
    return System.lineSeparator() + "  ";
  }

  public String getDiagnosticsName() {
    return "Global topology state";
  }

  public void dump(Logger logger) {
    logger.log("Current core topology:%s%s", newPaddedLIne(),
        Strings.printMap(this.topologyService.allCoreServers(), newPaddedLIne()));
    logger.log("Current read replica topology:%s%s",
        newPaddedLIne(), Strings.printMap(this.topologyService.allReadReplicas(), newPaddedLIne()));
  }
}
