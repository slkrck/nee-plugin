/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.diagnostics;

import io.nee.causalclustering.core.consensus.log.segmented.FileNames;
import io.nee.causalclustering.core.state.ClusterStateLayout;
import io.nee.causalclustering.core.state.CoreStateFiles;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.diagnostics.DiagnosticsOfflineReportProvider;
import org.neo4j.kernel.diagnostics.DiagnosticsReportSource;
import org.neo4j.kernel.diagnostics.DiagnosticsReportSources;
import org.neo4j.logging.NullLog;

public class ClusterDiagnosticsOfflineReportProvider extends DiagnosticsOfflineReportProvider {

  private FileSystemAbstraction fs;
  private ClusterStateLayout clusterStateLayout;
  private String defaultDatabaseName;

  public ClusterDiagnosticsOfflineReportProvider() {
    super("raft", "ccstate");
  }

  public void init(FileSystemAbstraction fs, String defaultDatabaseName, Config config,
      File storeDirectory) {
    this.fs = fs;
    this.clusterStateLayout = ClusterStateLayout
        .of(config.get(GraphDatabaseSettings.data_directory).toFile());
    this.defaultDatabaseName = defaultDatabaseName;
  }

  protected List<DiagnosticsReportSource> provideSources(Set<String> classifiers) {
    List<DiagnosticsReportSource> sources = new ArrayList();
    if (classifiers.contains("raft")) {
      this.getRaftLogs(sources);
    }

    if (classifiers.contains("ccstate")) {
      this.getClusterState(sources);
    }

    return sources;
  }

  private void getRaftLogs(List<DiagnosticsReportSource> sources) {
    File raftLogDirectory = this.clusterStateLayout.raftLogDirectory(this.defaultDatabaseName);
    FileNames fileNames = new FileNames(raftLogDirectory);
    SortedMap<Long, File> allFiles = fileNames.getAllFiles(this.fs, NullLog.getInstance());
    Iterator n5 = allFiles.values().iterator();

    while (n5.hasNext()) {
      File logFile = (File) n5.next();
      sources.add(DiagnosticsReportSources
          .newDiagnosticsFile("raft/" + logFile.getName(), this.fs, logFile));
    }
  }

  private void getClusterState(List<DiagnosticsReportSource> sources) {
    Set<File> directories = this.clusterStateLayout
        .listGlobalAndDatabaseDirectories(this.defaultDatabaseName, (type) ->
        {
          return type != CoreStateFiles.RAFT_LOG;
        });
    Iterator n3 = directories.iterator();

    while (n3.hasNext()) {
      File directory = (File) n3.next();
      this.addDirectory("ccstate", directory, sources);
    }
  }

  private void addDirectory(String path, File dir, List<DiagnosticsReportSource> sources) {
    String currentLevel = path + File.separator + dir.getName();
    if (this.fs.isDirectory(dir)) {
      File[] files = this.fs.listFiles(dir);
      if (files != null) {
        File[] n6 = files;
        int n7 = files.length;

        for (int n8 = 0; n8 < n7; ++n8) {
          File file = n6[n8];
          this.addDirectory(currentLevel, file, sources);
        }
      }
    } else {
      sources.add(DiagnosticsReportSources.newDiagnosticsFile(currentLevel, this.fs, dir));
    }
  }
}
