/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering;

import io.nee.causalclustering.core.replication.session.GlobalSession;
import io.nee.causalclustering.core.replication.session.GlobalSessionTrackerState;
import io.nee.causalclustering.core.replication.session.LocalOperationId;
import io.nee.causalclustering.core.state.storage.StateStorage;
import java.io.IOException;

public class SessionTracker {

  private final StateStorage<GlobalSessionTrackerState> sessionTrackerStorage;
  private GlobalSessionTrackerState sessionState;

  public SessionTracker(StateStorage<GlobalSessionTrackerState> sessionTrackerStorage) {
    this.sessionTrackerStorage = sessionTrackerStorage;
  }

  public void start() {
    if (this.sessionState == null) {
      this.sessionState = this.sessionTrackerStorage.getInitialState();
    }
  }

  public long getLastAppliedIndex() {
    return this.sessionState.logIndex();
  }

  public void flush() throws IOException {
    this.sessionTrackerStorage.writeState(this.sessionState);
  }

  public GlobalSessionTrackerState snapshot() {
    return this.sessionState.newInstance();
  }

  public void installSnapshot(GlobalSessionTrackerState sessionState) {
    this.sessionState = sessionState;
  }

  public boolean validateOperation(GlobalSession globalSession, LocalOperationId localOperationId) {
    return this.sessionState.validateOperation(globalSession, localOperationId);
  }

  public void update(GlobalSession globalSession, LocalOperationId localOperationId,
      long logIndex) {
    this.sessionState.update(globalSession, localOperationId, logIndex);
  }

  public GlobalSessionTrackerState newInstance() {
    return this.sessionState.newInstance();
  }
}
