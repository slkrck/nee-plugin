/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.catchup.storecopy.FileChunk;
import io.nee.causalclustering.catchup.storecopy.FileHeader;
import io.nee.causalclustering.catchup.storecopy.GetStoreIdResponse;
import io.nee.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import io.nee.causalclustering.catchup.storecopy.StoreCopyFinishedResponse;
import io.nee.causalclustering.catchup.tx.TxPullResponse;
import io.nee.causalclustering.catchup.tx.TxStreamFinishedResponse;
import io.nee.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponse;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshot;
import java.nio.channels.ClosedChannelException;
import java.time.Clock;
import java.util.OptionalLong;
import java.util.concurrent.CompletableFuture;

class TrackingResponseHandler implements CatchupResponseHandler {

  private static final CompletableFuture<Object> ILLEGAL_FUTURE = CompletableFuture
      .failedFuture(new IllegalStateException("Not expected"));
  private static final CatchupResponseAdaptor ILLEGAL_HANDLER = new CatchupResponseAdaptor();
  private static final long NO_RESPONSE_TIME = 1L;
  private final Clock clock;
  private CatchupResponseCallback delegate;
  private CompletableFuture<?> requestOutcomeSignal;
  private long lastResponseTime = 1L;

  TrackingResponseHandler(Clock clock) {
    this.clock = clock;
    this.clearResponseHandler();
  }

  void clearResponseHandler() {
    this.requestOutcomeSignal = ILLEGAL_FUTURE;
    this.delegate = ILLEGAL_HANDLER;
    this.lastResponseTime = 1L;
  }

  void setResponseHandler(CatchupResponseCallback responseHandler,
      CompletableFuture<?> requestOutcomeSignal) {
    this.delegate = responseHandler;
    this.requestOutcomeSignal = requestOutcomeSignal;
    this.lastResponseTime = 1L;
  }

  public void onFileHeader(FileHeader fileHeader) {
    this.ifNotCancelled(() ->
    {
      this.delegate.onFileHeader(this.requestOutcomeSignal, fileHeader);
    });
  }

  public boolean onFileContent(FileChunk fileChunk) {
    if (!this.requestOutcomeSignal.isCancelled()) {
      this.recordLastResponse();
      return this.delegate.onFileContent(this.requestOutcomeSignal, fileChunk);
    } else {
      return true;
    }
  }

  public void onFileStreamingComplete(StoreCopyFinishedResponse response) {
    this.ifNotCancelled(() ->
    {
      this.delegate.onFileStreamingComplete(this.requestOutcomeSignal, response);
    });
  }

  public void onTxPullResponse(TxPullResponse tx) {
    this.ifNotCancelled(() ->
    {
      this.delegate.onTxPullResponse(this.requestOutcomeSignal, tx);
    });
  }

  public void onTxStreamFinishedResponse(TxStreamFinishedResponse response) {
    this.ifNotCancelled(() ->
    {
      this.delegate.onTxStreamFinishedResponse(this.requestOutcomeSignal, response);
    });
  }

  public void onGetStoreIdResponse(GetStoreIdResponse response) {
    this.ifNotCancelled(() ->
    {
      this.delegate.onGetStoreIdResponse(this.requestOutcomeSignal, response);
    });
  }

  public void onGetDatabaseIdResponse(GetDatabaseIdResponse response) {
    this.ifNotCancelled(() ->
    {
      this.delegate.onGetDatabaseIdResponse(this.requestOutcomeSignal, response);
    });
  }

  public void onCoreSnapshot(CoreSnapshot coreSnapshot) {
    this.ifNotCancelled(() ->
    {
      this.delegate.onCoreSnapshot(this.requestOutcomeSignal, coreSnapshot);
    });
  }

  public void onStoreListingResponse(PrepareStoreCopyResponse storeListingRequest) {
    this.ifNotCancelled(() ->
    {
      this.delegate.onStoreListingResponse(this.requestOutcomeSignal, storeListingRequest);
    });
  }

  public void onCatchupErrorResponse(CatchupErrorResponse catchupErrorResponse) {
    this.ifNotCancelled(() ->
    {
      this.delegate.onCatchupErrorResponse(this.requestOutcomeSignal, catchupErrorResponse);
    });
  }

  private void ifNotCancelled(Runnable runnable) {
    if (!this.requestOutcomeSignal.isCancelled()) {
      this.recordLastResponse();
      runnable.run();
    }
  }

  public void onClose() {
    this.requestOutcomeSignal.completeExceptionally(new ClosedChannelException());
  }

  OptionalLong millisSinceLastResponse() {
    return this.lastResponseTime == 1L ? OptionalLong.empty()
        : OptionalLong.of(this.clock.millis() - this.lastResponseTime);
  }

  private void recordLastResponse() {
    this.lastResponseTime = this.clock.millis();
  }
}
