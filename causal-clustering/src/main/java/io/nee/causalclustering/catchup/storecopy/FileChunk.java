/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import java.util.Objects;

public class FileChunk {

  static final int HEADER_SIZE = 4;
  private static final int HEADER_IS_LAST_FALSE = 0;
  private static final int HEADER_IS_LAST_TRUE = 1;
  private final boolean isLast;
  private final ByteBuf payload;

  FileChunk(boolean isLast, ByteBuf payload) {
    this.isLast = isLast;
    this.payload = payload;
  }

  static FileChunk create(ByteBuf payload, boolean isLast, int chunkSize) {
    if (!isLast && payload.readableBytes() != chunkSize) {
      throw new IllegalArgumentException("All chunks except for the last must be of max size.");
    } else {
      return new FileChunk(isLast, payload);
    }
  }

  static int makeHeader(boolean isLast) {
    return isLast ? 1 : 0;
  }

  static boolean parseHeader(int header) {
    if (header == 1) {
      return true;
    } else if (header == 0) {
      return false;
    } else {
      throw new IllegalStateException("Illegal header value: " + header);
    }
  }

  public boolean isLast() {
    return this.isLast;
  }

  ByteBuf payload() {
    return this.payload;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      FileChunk fileChunk = (FileChunk) o;
      return this.isLast == fileChunk.isLast && ByteBufUtil.equals(this.payload, fileChunk.payload);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.isLast, this.payload);
  }

  public String toString() {
    return "FileChunk{isLast=" + this.isLast + ", payload=" + this.payload + "}";
  }
}
