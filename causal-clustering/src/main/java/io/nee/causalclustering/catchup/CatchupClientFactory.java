/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import java.time.Duration;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.logging.Log;

public class CatchupClientFactory implements Lifecycle {

  private final CatchupChannelPoolService pool;
  private final Duration inactivityTimeout;

  public CatchupClientFactory(Duration inactivityTimeout,
      CatchupChannelPoolService catchupChannelPoolService) {
    this.inactivityTimeout = inactivityTimeout;
    this.pool = catchupChannelPoolService;
  }

  public VersionedCatchupClients getClient(SocketAddress upstream, Log log) {
    return new CatchupClient(this.pool.acquire(upstream).thenApply(CatchupChannel::new),
        this.inactivityTimeout, log);
  }

  public void init() {
    this.pool.init();
  }

  public void start() {
    this.pool.start();
  }

  public void stop() {
    this.pool.stop();
  }

  public void shutdown() {
    this.pool.shutdown();
  }
}
