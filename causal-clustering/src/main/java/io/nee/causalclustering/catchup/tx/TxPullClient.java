/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.tx;

import io.nee.causalclustering.catchup.CatchupClientFactory;
import io.nee.causalclustering.catchup.CatchupResponseAdaptor;
import java.util.concurrent.CompletableFuture;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StoreId;

public class TxPullClient {

  private final CatchupClientFactory catchUpClient;
  private final NamedDatabaseId namedDatabaseId;
  private final Monitors monitors;
  private final LogProvider logProvider;
  private final TxPullClient.PullRequestMonitor pullRequestMonitor = new TxPullClient.PullRequestMonitor();

  public TxPullClient(CatchupClientFactory catchUpClient, NamedDatabaseId namedDatabaseId,
      Monitors monitors, LogProvider logProvider) {
    this.catchUpClient = catchUpClient;
    this.namedDatabaseId = namedDatabaseId;
    this.monitors = monitors;
    this.logProvider = logProvider;
  }

  public TxStreamFinishedResponse pullTransactions(SocketAddress fromAddress, StoreId storeId,
      long previousTxId,
      final TxPullResponseListener txPullResponseListener) throws Exception {
    CatchupResponseAdaptor<TxStreamFinishedResponse> responseHandler = new CatchupResponseAdaptor<TxStreamFinishedResponse>() {
      public void onTxPullResponse(CompletableFuture<TxStreamFinishedResponse> signal,
          TxPullResponse response) {
        txPullResponseListener.onTxReceived(response);
      }

      public void onTxStreamFinishedResponse(CompletableFuture<TxStreamFinishedResponse> signal,
          TxStreamFinishedResponse response) {
        signal.complete(response);
      }
    };
    this.pullRequestMonitor.get().txPullRequest(previousTxId);
    Log log = this.logProvider.getLog(this.getClass());
    return this.catchUpClient.getClient(fromAddress, log).v3((c) ->
    {
      return c.pullTransactions(storeId, previousTxId,
          this.namedDatabaseId);
    }).withResponseHandler(responseHandler).request();
  }

  private class PullRequestMonitor {

    private io.nee.causalclustering.catchup.tx.PullRequestMonitor pullRequestMonitor;

    private io.nee.causalclustering.catchup.tx.PullRequestMonitor get() {
      if (this.pullRequestMonitor == null) {
        this.pullRequestMonitor = monitors.newMonitor(
            io.nee.causalclustering.catchup.tx.PullRequestMonitor.class, new String[0]);
      }

      return this.pullRequestMonitor;
    }
  }
}
