/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.tx;

import java.io.File;
import java.io.IOException;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.Config.Builder;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.dbms.database.DatabasePageCache;
import org.neo4j.internal.helpers.collection.LongRange;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.io.pagecache.tracing.cursor.context.EmptyVersionContextSupplier;
import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.FlushablePositionAwareChecksumChannel;
import org.neo4j.kernel.impl.transaction.log.LogPosition;
import org.neo4j.kernel.impl.transaction.log.LogPositionMarker;
import org.neo4j.kernel.impl.transaction.log.TransactionLogWriter;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryWriter;
import org.neo4j.kernel.impl.transaction.log.entry.LogHeaderReader;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.kernel.impl.transaction.log.files.LogFilesBuilder;
import org.neo4j.kernel.impl.transaction.log.files.TransactionLogFilesHelper;
import org.neo4j.kernel.lifecycle.Lifespan;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.storageengine.api.TransactionId;
import org.neo4j.storageengine.api.TransactionMetaDataStore;

public class TransactionLogCatchUpWriter implements TxPullResponseListener, AutoCloseable {

  private final Lifespan lifespan = new Lifespan();
  private final Log log;
  private final boolean asPartOfStoreCopy;
  private final TransactionLogWriter writer;
  private final LogFiles logFiles;
  private final TransactionMetaDataStore metaDataStore;
  private final DatabasePageCache databasePageCache;
  private final boolean rotateTransactionsManually;
  private final LongRange validInitialTxId;
  private final FlushablePositionAwareChecksumChannel logChannel;
  private final LogPositionMarker logPositionMarker = new LogPositionMarker();
  private long lastTxId = -1L;
  private long expectedTxId = -1L;

  TransactionLogCatchUpWriter(DatabaseLayout databaseLayout, FileSystemAbstraction fs,
      PageCache pageCache, Config config, LogProvider logProvider,
      StorageEngineFactory storageEngineFactory, LongRange validInitialTxId,
      boolean asPartOfStoreCopy, boolean keepTxLogsInStoreDir,
      boolean forceTransactionRotations) throws IOException {
    this.log = logProvider.getLog(this.getClass());
    this.asPartOfStoreCopy = asPartOfStoreCopy;
    this.rotateTransactionsManually = forceTransactionRotations;
    Config configWithoutSpecificStoreFormat = configWithoutSpecificStoreFormat(config);
    this.databasePageCache = new DatabasePageCache(pageCache, EmptyVersionContextSupplier.EMPTY);
    Dependencies dependencies = new Dependencies();
    dependencies.satisfyDependencies(databaseLayout, fs, this.databasePageCache,
        configWithoutSpecificStoreFormat);
    this.metaDataStore = storageEngineFactory
        .transactionMetaDataStore(fs, databaseLayout, configWithoutSpecificStoreFormat,
            this.databasePageCache);
    LogPosition startPosition = getLastClosedTransactionPosition(databaseLayout, this.metaDataStore,
        fs);
    LogFilesBuilder logFilesBuilder =
        LogFilesBuilder.builder(databaseLayout, fs).withDependencies(dependencies)
            .withLastCommittedTransactionIdSupplier(() ->
            {
              return validInitialTxId
                  .from() -
                  1L;
            })
            .withConfig(customisedConfig(config, keepTxLogsInStoreDir, forceTransactionRotations,
                asPartOfStoreCopy))
            .withLogVersionRepository(
                this.metaDataStore).withTransactionIdStore(this.metaDataStore).withStoreId(
            this.metaDataStore.getStoreId()).withLastClosedTransactionPositionSupplier(() ->
        {
          return startPosition;
        });
    this.logFiles = logFilesBuilder.build();
    this.lifespan.add(this.logFiles);
    this.logChannel = this.logFiles.getLogFile().getWriter();
    this.writer = new TransactionLogWriter(new LogEntryWriter(this.logChannel));
    this.validInitialTxId = validInitialTxId;
  }

  private static LogPosition getLastClosedTransactionPosition(DatabaseLayout databaseLayout,
      TransactionMetaDataStore metaDataStore,
      FileSystemAbstraction fs) throws IOException {
    TransactionLogFilesHelper logFilesHelper = new TransactionLogFilesHelper(fs,
        databaseLayout.getTransactionLogsDirectory());
    File logFile = logFilesHelper.getLogFileForVersion(metaDataStore.getCurrentLogVersion());
    return fs.fileExists(logFile) ? LogHeaderReader.readLogHeader(fs, logFile).getStartPosition()
        : new LogPosition(0L, 64L);
  }

  private static Config configWithoutSpecificStoreFormat(Config config) {
    return Config.newBuilder().fromConfig(config).set(GraphDatabaseSettings.record_format, null)
        .build();
  }

  private static Config customisedConfig(Config original, boolean keepTxLogsInStoreDir,
      boolean forceTransactionRotations, boolean asPartOfStoreCopy) {
    Builder builder = Config.newBuilder();
    if (!keepTxLogsInStoreDir && original
        .isExplicitlySet(GraphDatabaseSettings.transaction_logs_root_path)) {
      builder.set(GraphDatabaseSettings.transaction_logs_root_path,
          original.get(GraphDatabaseSettings.transaction_logs_root_path));
    }

    if (forceTransactionRotations && original
        .isExplicitlySet(GraphDatabaseSettings.logical_log_rotation_threshold)) {
      builder.set(GraphDatabaseSettings.logical_log_rotation_threshold,
          original.get(GraphDatabaseSettings.logical_log_rotation_threshold));
    }

    if (asPartOfStoreCopy) {
      builder.set(GraphDatabaseSettings.preallocate_logical_logs, false);
    }

    return builder.build();
  }

  private static void rotateTransactionLogs(LogFiles logFiles) {
    try {
      logFiles.getLogFile().rotate();
    } catch (IOException n2) {
      throw new RuntimeException(n2);
    }
  }

  public synchronized void onTxReceived(TxPullResponse txPullResponse) {
    CommittedTransactionRepresentation tx = txPullResponse.tx();
    long receivedTxId = tx.getCommitEntry().getTxId();
    if (this.rotateTransactionsManually && this.logFiles.getLogFile().rotationNeeded()) {
      rotateTransactionLogs(this.logFiles);
    }

    this.validateReceivedTxId(receivedTxId);
    this.lastTxId = receivedTxId;
    ++this.expectedTxId;

    try {
      this.logChannel.getCurrentPosition(this.logPositionMarker);
      this.writer.append(tx.getTransactionRepresentation(), this.lastTxId,
          tx.getStartEntry().getPreviousChecksum());
    } catch (IOException n6) {
      this.log.error("Failed when appending to transaction log", n6);
    }
  }

  private void validateReceivedTxId(long receivedTxId) {
    if (this.isFirstTx()) {
      if (!this.validInitialTxId.isWithinRange(receivedTxId)) {
        throw new RuntimeException(
            String.format("Expected the first received txId to be within the range: %s but got: %d",
                this.validInitialTxId, receivedTxId));
      }

      this.expectedTxId = receivedTxId;
    }

    if (receivedTxId != this.expectedTxId) {
      throw new RuntimeException(
          String.format("Expected txId: %d but got: %d", this.expectedTxId, receivedTxId));
    }
  }

  private boolean isFirstTx() {
    return this.expectedTxId == -1L;
  }

  public long lastTx() {
    return this.lastTxId;
  }

  public synchronized void close() throws IOException {
    if (this.asPartOfStoreCopy) {
      long logVersion = this.logFiles.getLowestLogVersion();
      LogPosition checkPointPosition = this.logFiles.extractHeader(logVersion).getStartPosition();
      this.log.info("Writing checkpoint as part of store copy: " + checkPointPosition);
      this.writer.checkPoint(checkPointPosition);
      TransactionId lastCommittedTx = this.metaDataStore.getLastCommittedTransaction();
      this.metaDataStore.setLastCommittedAndClosedTransactionId(lastCommittedTx.transactionId(),
          lastCommittedTx.checksum(),
          lastCommittedTx.commitTimestamp(), checkPointPosition.getByteOffset(), logVersion);
    }

    this.lifespan.close();
    if (this.lastTxId != -1L) {
      this.metaDataStore
          .setLastCommittedAndClosedTransactionId(this.lastTxId, 0, System.currentTimeMillis(),
              this.logPositionMarker.getByteOffset(),
              this.logPositionMarker.getLogVersion());
    }

    this.metaDataStore.close();
    this.databasePageCache.close();
  }
}
