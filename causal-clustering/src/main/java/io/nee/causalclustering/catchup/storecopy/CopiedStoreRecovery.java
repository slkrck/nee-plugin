/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import java.io.IOException;
import java.util.Optional;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.kernel.recovery.Recovery;
import org.neo4j.logging.internal.NullLogService;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.storageengine.api.StoreVersion;
import org.neo4j.storageengine.api.StoreVersionCheck;
import org.neo4j.storageengine.migration.UpgradeNotAllowedException;

public class CopiedStoreRecovery extends LifecycleAdapter {

  private final PageCache pageCache;
  private final FileSystemAbstraction fs;
  private final StorageEngineFactory storageEngineFactory;
  private boolean shutdown;

  public CopiedStoreRecovery(PageCache pageCache, FileSystemAbstraction fs,
      StorageEngineFactory storageEngineFactory) {
    this.pageCache = pageCache;
    this.fs = fs;
    this.storageEngineFactory = storageEngineFactory;
  }

  public synchronized void shutdown() {
    this.shutdown = true;
  }

  public synchronized void recoverCopiedStore(Config config, DatabaseLayout databaseLayout)
      throws DatabaseShutdownException, IOException {
    if (this.shutdown) {
      throw new DatabaseShutdownException(
          "Abort store-copied store recovery due to database shutdown");
    } else {
      StoreVersionCheck storeVersionCheck =
          this.storageEngineFactory.versionCheck(this.fs, databaseLayout, config, this.pageCache,
              NullLogService.getInstance());
      Optional<String> storeVersion = storeVersionCheck.storeVersion();
      if (databaseLayout.getDatabaseName().equals("system")) {
        config = Config.newBuilder().fromConfig(config)
            .set(GraphDatabaseSettings.record_format, "standard").build();
      } else if (storeVersion.isPresent()) {
        StoreVersion version = storeVersionCheck.versionInformation(storeVersion.get());
        String configuredVersion = storeVersionCheck.configuredVersion();
        if (configuredVersion != null && !version
            .isCompatibleWith(storeVersionCheck.versionInformation(configuredVersion))) {
          throw new RuntimeException(this.failedToStartMessage(config));
        }
      }

      try {
        Recovery.performRecovery(this.fs, this.pageCache, config, databaseLayout,
            this.storageEngineFactory);
      } catch (Exception n7) {
        if (ExceptionUtils.indexOfThrowable(n7, UpgradeNotAllowedException.class) != -1) {
          throw new RuntimeException(this.failedToStartMessage(config), n7);
        } else {
          throw n7;
        }
      }
    }
  }

  private String failedToStartMessage(Config config) {
    String recordFormat = config.get(GraphDatabaseSettings.record_format);
    return String.format(
        "Failed to start database with copied store. This may be because the core servers and read replicas have a different record format. On this machine: `%s=%s`. Check the equivalent value on the core server.",
        GraphDatabaseSettings.record_format.name(), recordFormat);
  }
}
