/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import org.neo4j.internal.helpers.collection.LongRange;

class RequiredTransactions {

  private static final long NO_REQUIRED_TX_ID = -1L;
  private final long startTxId;
  private final long requiredTx;

  private RequiredTransactions(long startTxId, long requiredTx) {
    if (startTxId < 0L) {
      throw new IllegalArgumentException("Start tx id cannot be negative. Got: " + startTxId);
    } else {
      this.startTxId = startTxId;
      this.requiredTx = requiredTx;
    }
  }

  static RequiredTransactions requiredRange(long startTxId, long requiredTxId) {
    LongRange.assertIsRange(startTxId, requiredTxId);
    return new RequiredTransactions(startTxId, requiredTxId);
  }

  static RequiredTransactions noConstraint(long myHighestTxId) {
    return new RequiredTransactions(myHighestTxId, -1L);
  }

  long startTxId() {
    return this.startTxId;
  }

  boolean noRequiredTxId() {
    return this.requiredTx == -1L;
  }

  long requiredTxId() {
    return this.requiredTx;
  }

  public String toString() {
    return "RequiredTransactions{from=" + this.startTxId + ", to=" + this.requiredTx + "}";
  }
}
