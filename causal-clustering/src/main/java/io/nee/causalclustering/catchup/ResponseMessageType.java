/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

public enum ResponseMessageType {
  TX((byte) 1),
  STORE_ID((byte) 2),
  FILE((byte) 3),
  STORE_COPY_FINISHED((byte) 4),
  CORE_SNAPSHOT((byte) 5),
  TX_STREAM_FINISHED((byte) 6),
  PREPARE_STORE_COPY_RESPONSE((byte) 7),
  INDEX_SNAPSHOT_RESPONSE((byte) 8),
  DATABASE_ID_RESPONSE((byte) 9),
  ERROR((byte) -57),
  UNKNOWN((byte) -56);

  private final byte messageType;

  ResponseMessageType(byte messageType) {
    this.messageType = messageType;
  }

  public static ResponseMessageType from(byte b) {
    ResponseMessageType[] n1 = values();
    int n2 = n1.length;

    for (int n3 = 0; n3 < n2; ++n3) {
      ResponseMessageType responseMessageType = n1[n3];
      if (responseMessageType.messageType == b) {
        return responseMessageType;
      }
    }

    return UNKNOWN;
  }

  public byte messageType() {
    return this.messageType;
  }

  public String toString() {
    return String.format("ResponseMessageType{messageType=%s}", this.messageType);
  }
}
