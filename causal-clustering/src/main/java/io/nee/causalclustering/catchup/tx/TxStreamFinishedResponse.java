/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.tx;

import io.nee.causalclustering.catchup.CatchupResult;
import java.util.Objects;

public class TxStreamFinishedResponse {

  private final CatchupResult status;
  private final long latestTxId;

  public TxStreamFinishedResponse(CatchupResult status, long latestTxId) {
    this.status = status;
    this.latestTxId = latestTxId;
  }

  public long lastTxId() {
    return this.latestTxId;
  }

  public CatchupResult status() {
    return this.status;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      TxStreamFinishedResponse that = (TxStreamFinishedResponse) o;
      return this.latestTxId == that.latestTxId && this.status == that.status;
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.status, this.latestTxId);
  }

  public String toString() {
    return "TxStreamFinishedResponse{status=" + this.status + ", lastTxId=" + this.latestTxId + "}";
  }
}
