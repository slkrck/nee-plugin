/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import io.nee.causalclustering.catchup.storecopy.StoreCopyFinishedResponse;
import io.nee.causalclustering.catchup.tx.TxStreamFinishedResponse;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshot;
import java.io.File;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.storageengine.api.StoreId;

public interface VersionedCatchupClients extends AutoCloseable {

  <RESULT> VersionedCatchupClients.NeedsResponseHandler<RESULT> v3(
      Function<VersionedCatchupClients.CatchupClientV3, VersionedCatchupClients.PreparedRequest<RESULT>> n1);

  @FunctionalInterface
  interface PreparedRequest<RESULT> {

    CompletableFuture<RESULT> execute(CatchupResponseCallback<RESULT> n1);
  }

  interface CatchupClientV3 {

    VersionedCatchupClients.PreparedRequest<NamedDatabaseId> getDatabaseId(String n1);

    VersionedCatchupClients.PreparedRequest<CoreSnapshot> getCoreSnapshot(NamedDatabaseId n1);

    VersionedCatchupClients.PreparedRequest<StoreId> getStoreId(NamedDatabaseId n1);

    VersionedCatchupClients.PreparedRequest<TxStreamFinishedResponse> pullTransactions(StoreId n1,
        long n2, NamedDatabaseId n4);

    VersionedCatchupClients.PreparedRequest<PrepareStoreCopyResponse> prepareStoreCopy(StoreId n1,
        NamedDatabaseId n2);

    VersionedCatchupClients.PreparedRequest<StoreCopyFinishedResponse> getStoreFile(StoreId n1,
        File n2, long n3, NamedDatabaseId n5);
  }

  interface IsPrepared<RESULT> {

    RESULT request() throws Exception;
  }

  interface NeedsResponseHandler<RESULT> {

    VersionedCatchupClients.IsPrepared<RESULT> withResponseHandler(
        CatchupResponseCallback<RESULT> n1);
  }

  interface NeedsV3Handler<RESULT> {

    VersionedCatchupClients.NeedsResponseHandler<RESULT> v3(
        Function<VersionedCatchupClients.CatchupClientV3, VersionedCatchupClients.PreparedRequest<RESULT>> n1);
  }

  interface CatchupRequestBuilder<RESULT> extends VersionedCatchupClients.NeedsV3Handler<RESULT>,
      VersionedCatchupClients.NeedsResponseHandler<RESULT>,
      VersionedCatchupClients.IsPrepared<RESULT> {

  }
}
