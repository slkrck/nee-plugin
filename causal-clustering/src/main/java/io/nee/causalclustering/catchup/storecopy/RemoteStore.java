/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.catchup.tx.TransactionLogCatchUpFactory;
import io.nee.causalclustering.catchup.tx.TransactionLogCatchUpWriter;
import io.nee.causalclustering.catchup.tx.TxPullClient;
import io.nee.causalclustering.core.CausalClusteringSettings;
import java.io.IOException;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.helpers.collection.LongRange;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.storageengine.api.StoreId;

public class RemoteStore {

  private final Log log;
  private final Monitors monitors;
  private final Config config;
  private final FileSystemAbstraction fs;
  private final PageCache pageCache;
  private final LogProvider logProvider;
  private final StoreCopyClient storeCopyClient;
  private final TxPullClient txPullClient;
  private final TransactionLogCatchUpFactory transactionLogFactory;
  private final CommitStateHelper commitStateHelper;
  private final StoreCopyClientMonitor storeCopyClientMonitor;
  private final StorageEngineFactory storageEngineFactory;
  private final NamedDatabaseId namedDatabaseId;

  public RemoteStore(LogProvider logProvider, FileSystemAbstraction fs, PageCache pageCache,
      StoreCopyClient storeCopyClient, TxPullClient txPullClient,
      TransactionLogCatchUpFactory transactionLogFactory, Config config, Monitors monitors,
      StorageEngineFactory storageEngineFactory,
      NamedDatabaseId namedDatabaseId) {
    this.logProvider = logProvider;
    this.storeCopyClient = storeCopyClient;
    this.txPullClient = txPullClient;
    this.fs = fs;
    this.pageCache = pageCache;
    this.transactionLogFactory = transactionLogFactory;
    this.config = config;
    this.log = logProvider.getLog(this.getClass());
    this.monitors = monitors;
    this.storeCopyClientMonitor = monitors.newMonitor(StoreCopyClientMonitor.class, new String[0]);
    this.storageEngineFactory = storageEngineFactory;
    this.namedDatabaseId = namedDatabaseId;
    this.commitStateHelper = new CommitStateHelper(pageCache, fs, config, storageEngineFactory);
  }

  public void tryCatchingUp(CatchupAddressProvider catchupAddressProvider, StoreId expectedStoreId,
      DatabaseLayout databaseLayout, boolean keepTxLogsInDir,
      boolean forceTransactionLogRotation) throws StoreCopyFailedException, IOException {
    CommitState commitState = this.commitStateHelper.getStoreState(databaseLayout);
    this.log.info("Store commit state: " + commitState);
    TxPullRequestContext txPullRequestContext = TxPullRequestContext
        .createContextFromCatchingUp(expectedStoreId, commitState);
    this.pullTransactions(catchupAddressProvider, databaseLayout, txPullRequestContext, false,
        keepTxLogsInDir, forceTransactionLogRotation);
  }

  public void copy(CatchupAddressProvider addressProvider, StoreId expectedStoreId,
      DatabaseLayout destinationLayout, boolean rotateTransactionsManually)
      throws StoreCopyFailedException {
    StreamToDiskProvider streamToDiskProvider = new StreamToDiskProvider(
        destinationLayout.databaseDirectory(), this.fs, this.monitors);
    RequiredTransactions requiredTransactions =
        this.storeCopyClient.copyStoreFiles(addressProvider, expectedStoreId, streamToDiskProvider,
            this::getTerminationCondition,
            destinationLayout.databaseDirectory());
    this.log.info("Store files need to be recovered starting from: %s", requiredTransactions);
    TxPullRequestContext context = TxPullRequestContext
        .createContextFromStoreCopy(requiredTransactions, expectedStoreId);
    this.pullTransactions(addressProvider, destinationLayout, context, true, true,
        rotateTransactionsManually);
  }

  private MaximumTotalTime getTerminationCondition() {
    return new MaximumTotalTime(
        this.config.get(CausalClusteringSettings.store_copy_max_retry_time_per_request));
  }

  private void pullTransactions(CatchupAddressProvider catchupAddressProvider,
      DatabaseLayout databaseLayout, TxPullRequestContext context,
      boolean asPartOfStoreCopy, boolean keepTxLogsInStoreDir, boolean rotateTransactionsManually)
      throws StoreCopyFailedException {
    this.storeCopyClientMonitor.startReceivingTransactions(context.startTxIdExclusive());

    try {
      TransactionLogCatchUpWriter writer =
          this.transactionLogFactory
              .create(databaseLayout, this.fs, this.pageCache, this.config, this.logProvider,
                  this.storageEngineFactory,
                  this.validInitialTxRange(context), asPartOfStoreCopy, keepTxLogsInStoreDir,
                  rotateTransactionsManually);

      try {
        TxPuller txPuller = TxPuller
            .createTxPuller(catchupAddressProvider, this.logProvider, this.config,
                this.namedDatabaseId);
        txPuller.pullTransactions(context, writer, this.txPullClient);
        this.storeCopyClientMonitor.finishReceivingTransactions(writer.lastTx());
      } catch (Throwable n11) {
        if (writer != null) {
          try {
            writer.close();
          } catch (Throwable n10) {
            n11.addSuppressed(n10);
          }
        }

        throw n11;
      }

      if (writer != null) {
        writer.close();
      }
    } catch (IOException n12) {
      throw new StoreCopyFailedException(n12);
    }
  }

  private LongRange validInitialTxRange(TxPullRequestContext context) {
    return LongRange.range(context.startTxIdExclusive() + 1L,
        context.fallbackStartId().orElse(context.startTxIdExclusive()) + 1L);
  }

  public StoreId getStoreId(SocketAddress from) throws StoreIdDownloadFailedException {
    return this.storeCopyClient.fetchStoreId(from);
  }
}
