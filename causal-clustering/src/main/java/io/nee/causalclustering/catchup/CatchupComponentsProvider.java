/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.catchup.storecopy.CopiedStoreRecovery;
import io.nee.causalclustering.catchup.storecopy.RemoteStore;
import io.nee.causalclustering.catchup.storecopy.StoreCopyClient;
import io.nee.causalclustering.catchup.storecopy.StoreCopyProcess;
import io.nee.causalclustering.catchup.tx.TransactionLogCatchUpFactory;
import io.nee.causalclustering.catchup.tx.TxPullClient;
import io.nee.causalclustering.common.PipelineBuilders;
import io.nee.causalclustering.common.TransactionBackupServiceProvider;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.core.SupportedProtocolCreator;
import io.nee.causalclustering.net.BootstrapConfiguration;
import io.nee.causalclustering.net.InstalledProtocolHandler;
import io.nee.causalclustering.net.Server;
import io.nee.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import io.nee.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import io.nee.dbms.database.ClusteredDatabaseContext;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.internal.helpers.ExponentialBackoffStrategy;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.DatabaseLogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.storageengine.api.StorageEngineFactory;

public final class CatchupComponentsProvider {

  private static final String CATCHUP_SERVER_NAME = "catchup-server";
  private final PipelineBuilders pipelineBuilders;
  private final LogProvider logProvider;
  private final ApplicationSupportedProtocols supportedCatchupProtocols;
  private final List<ModifierSupportedProtocols> supportedModifierProtocols;
  private final Config config;
  private final LogProvider userLogProvider;
  private final JobScheduler scheduler;
  private final LifeSupport globalLife;
  private final CatchupClientFactory catchupClientFactory;
  private final ConnectorPortRegister portRegister;
  private final CopiedStoreRecovery copiedStoreRecovery;
  private final PageCache pageCache;
  private final FileSystemAbstraction fileSystem;
  private final StorageEngineFactory storageEngineFactory;
  private final ExponentialBackoffStrategy storeCopyBackoffStrategy;

  public CatchupComponentsProvider(GlobalModule globalModule, PipelineBuilders pipelineBuilders) {
    this.pipelineBuilders = pipelineBuilders;
    this.logProvider = globalModule.getLogService().getInternalLogProvider();
    this.config = globalModule.getGlobalConfig();
    SupportedProtocolCreator supportedProtocolCreator = new SupportedProtocolCreator(this.config,
        this.logProvider);
    this.supportedCatchupProtocols = supportedProtocolCreator
        .getSupportedCatchupProtocolsFromConfiguration();
    this.supportedModifierProtocols = supportedProtocolCreator.createSupportedModifierProtocols();
    this.userLogProvider = globalModule.getLogService().getUserLogProvider();
    this.scheduler = globalModule.getJobScheduler();
    this.pageCache = globalModule.getPageCache();
    this.globalLife = globalModule.getGlobalLife();
    this.fileSystem = globalModule.getFileSystem();
    this.catchupClientFactory = this.createCatchupClientFactory();
    this.portRegister = globalModule.getConnectorPortRegister();
    this.storageEngineFactory = globalModule.getStorageEngineFactory();
    this.copiedStoreRecovery =
        this.globalLife.add(new CopiedStoreRecovery(this.pageCache, this.fileSystem,
            globalModule.getStorageEngineFactory()));
    this.storeCopyBackoffStrategy =
        new ExponentialBackoffStrategy(1L,
            this.config.get(CausalClusteringSettings.store_copy_backoff_max_wait).toMillis(),
            TimeUnit.MILLISECONDS);
  }

  private CatchupClientFactory createCatchupClientFactory() {
    CatchupClientFactory catchupClient = CatchupClientBuilder.builder()
        .catchupProtocols(this.supportedCatchupProtocols).modifierProtocols(
            this.supportedModifierProtocols).pipelineBuilder(this.pipelineBuilders.client())
        .inactivityTimeout(
            this.config.get(CausalClusteringSettings.catch_up_client_inactivity_timeout))
        .scheduler(this.scheduler).bootstrapConfig(
            BootstrapConfiguration.clientConfig(this.config)).handShakeTimeout(
            this.config.get(CausalClusteringSettings.handshake_timeout))
        .debugLogProvider(this.logProvider).userLogProvider(
            this.userLogProvider).build();
    this.globalLife.add(catchupClient);
    return catchupClient;
  }

  public Server createCatchupServer(InstalledProtocolHandler installedProtocolsHandler,
      CatchupServerHandler catchupServerHandler) {
    return CatchupServerBuilder.builder().catchupServerHandler(catchupServerHandler)
        .catchupProtocols(this.supportedCatchupProtocols).modifierProtocols(
            this.supportedModifierProtocols).pipelineBuilder(this.pipelineBuilders.server())
        .installedProtocolsHandler(
            installedProtocolsHandler)
        .listenAddress(this.config.get(CausalClusteringSettings.transaction_listen_address))
        .scheduler(
            this.scheduler).config(this.config)
        .bootstrapConfig(BootstrapConfiguration.serverConfig(this.config)).portRegister(
            this.portRegister).userLogProvider(this.userLogProvider)
        .debugLogProvider(this.logProvider).serverName(
            "catchup-server")
        .handshakeTimeout(this.config.get(CausalClusteringSettings.handshake_timeout)).build();
  }

  public Optional<Server> createBackupServer(InstalledProtocolHandler installedProtocolsHandler,
      CatchupServerHandler catchupServerHandler) {
    TransactionBackupServiceProvider transactionBackupServiceProvider =
        new TransactionBackupServiceProvider(this.logProvider, this.supportedCatchupProtocols,
            this.supportedModifierProtocols,
            this.pipelineBuilders.backupServer(), catchupServerHandler, installedProtocolsHandler,
            this.scheduler,
            this.portRegister);
    return transactionBackupServiceProvider.resolveIfBackupEnabled(this.config);
  }

  public CatchupComponentsRepository.CatchupComponents createDatabaseComponents(
      ClusteredDatabaseContext clusteredDatabaseContext) {
    DatabaseLogProvider databaseLogProvider = clusteredDatabaseContext.database()
        .getInternalLogProvider();
    Monitors monitors = clusteredDatabaseContext.monitors();
    StoreCopyClient storeCopyClient = new StoreCopyClient(this.catchupClientFactory,
        clusteredDatabaseContext.databaseId(), monitors, databaseLogProvider,
        this.storeCopyBackoffStrategy);
    TransactionLogCatchUpFactory transactionLogFactory = new TransactionLogCatchUpFactory();
    TxPullClient txPullClient = new TxPullClient(this.catchupClientFactory,
        clusteredDatabaseContext.databaseId(), monitors, databaseLogProvider);
    RemoteStore remoteStore =
        new RemoteStore(databaseLogProvider, this.fileSystem, this.pageCache, storeCopyClient,
            txPullClient, transactionLogFactory, this.config,
            monitors, this.storageEngineFactory, clusteredDatabaseContext.databaseId());
    StoreCopyProcess storeCopy =
        new StoreCopyProcess(this.fileSystem, this.pageCache, clusteredDatabaseContext,
            this.copiedStoreRecovery, remoteStore, databaseLogProvider);
    return new CatchupComponentsRepository.CatchupComponents(remoteStore, storeCopy);
  }

  public CatchupClientFactory catchupClientFactory() {
    return this.catchupClientFactory;
  }
}
