/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import io.netty.buffer.ByteBuf;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.neo4j.io.IOUtils;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.StoreChannel;

public class StreamToDisk implements StoreFileStream {

  private final StoreChannel storeChannel;
  private final List<AutoCloseable> closeables;

  private StreamToDisk(StoreChannel storeChannel, AutoCloseable... closeables) {
    this.storeChannel = storeChannel;
    this.closeables = new ArrayList();
    this.closeables.add(storeChannel);
    this.closeables.addAll(Arrays.asList(closeables));
  }

  static StreamToDisk fromFile(FileSystemAbstraction fsa, File file) throws IOException {
    return new StreamToDisk(fsa.write(file));
  }

  public void write(ByteBuf data) throws IOException {
    int expectedTotal = data.readableBytes();

    int bytesWrittenOrEOF;
    for (int totalWritten = 0; totalWritten < expectedTotal; totalWritten += bytesWrittenOrEOF) {
      bytesWrittenOrEOF = data.readBytes(this.storeChannel, expectedTotal);
      if (bytesWrittenOrEOF < 0) {
        throw new IOException("Unexpected failure writing to channel: " + bytesWrittenOrEOF);
      }
    }
  }

  public void close() throws IOException {
    IOUtils.closeAll(this.closeables);
  }
}
