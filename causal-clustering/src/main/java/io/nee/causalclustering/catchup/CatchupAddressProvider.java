/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.core.LeaderProvider;
import io.nee.causalclustering.core.consensus.NoLeaderFoundException;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.upstream.UpstreamDatabaseStrategySelector;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;

public interface CatchupAddressProvider {

  SocketAddress primary(NamedDatabaseId n1) throws CatchupAddressResolutionException;

  SocketAddress secondary(NamedDatabaseId n1) throws CatchupAddressResolutionException;

  class LeaderOrUpstreamStrategyBasedAddressProvider implements CatchupAddressProvider {

    private final LeaderProvider leaderProvider;
    private final TopologyService topologyService;
    private final UpstreamAddressLookup secondaryUpstreamAddressLookup;

    public LeaderOrUpstreamStrategyBasedAddressProvider(LeaderProvider leaderProvider,
        TopologyService topologyService,
        UpstreamDatabaseStrategySelector strategySelector) {
      this.leaderProvider = leaderProvider;
      this.topologyService = topologyService;
      this.secondaryUpstreamAddressLookup = new UpstreamAddressLookup(strategySelector,
          topologyService);
    }

    public SocketAddress primary(NamedDatabaseId namedDatabaseId)
        throws CatchupAddressResolutionException {
      try {
        MemberId leadMember = this.leaderProvider.getLeader(namedDatabaseId);
        return this.topologyService.lookupCatchupAddress(leadMember);
      } catch (NoLeaderFoundException n3) {
        throw new CatchupAddressResolutionException(n3);
      }
    }

    public SocketAddress secondary(NamedDatabaseId namedDatabaseId)
        throws CatchupAddressResolutionException {
      return this.secondaryUpstreamAddressLookup.lookupAddressForDatabase(namedDatabaseId);
    }
  }

  class UpstreamStrategyBasedAddressProvider implements CatchupAddressProvider {

    private final UpstreamAddressLookup upstreamAddressLookup;

    public UpstreamStrategyBasedAddressProvider(TopologyService topologyService,
        UpstreamDatabaseStrategySelector strategySelector) {
      this.upstreamAddressLookup = new UpstreamAddressLookup(strategySelector, topologyService);
    }

    public SocketAddress primary(NamedDatabaseId namedDatabaseId)
        throws CatchupAddressResolutionException {
      return this.upstreamAddressLookup.lookupAddressForDatabase(namedDatabaseId);
    }

    public SocketAddress secondary(NamedDatabaseId namedDatabaseId)
        throws CatchupAddressResolutionException {
      return this.upstreamAddressLookup.lookupAddressForDatabase(namedDatabaseId);
    }
  }

  class SingleAddressProvider implements CatchupAddressProvider {

    private final SocketAddress socketAddress;

    public SingleAddressProvider(SocketAddress socketAddress) {
      this.socketAddress = socketAddress;
    }

    public SocketAddress primary(NamedDatabaseId namedDatabaseId) {
      return this.socketAddress;
    }

    public SocketAddress secondary(NamedDatabaseId namedDatabaseId) {
      return this.socketAddress;
    }
  }
}
