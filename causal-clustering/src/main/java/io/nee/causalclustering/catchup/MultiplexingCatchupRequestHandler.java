/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.catchup.error.UnavailableDatabaseHandler;
import io.nee.causalclustering.catchup.error.UnknownDatabaseHandler;
import io.nee.causalclustering.messaging.CatchupProtocolMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.util.function.Function;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.kernel.availability.DatabaseAvailabilityGuard;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.logging.LogProvider;

class MultiplexingCatchupRequestHandler<T extends CatchupProtocolMessage.WithDatabaseId> extends
    SimpleChannelInboundHandler<T> {

  private final Class<T> messageType;
  private final DatabaseManager<?> databaseManager;
  private final Function<Database, SimpleChannelInboundHandler<T>> handlerFactory;
  private final CatchupServerProtocol protocol;
  private final LogProvider logProvider;

  MultiplexingCatchupRequestHandler(CatchupServerProtocol protocol,
      DatabaseManager<?> databaseManager,
      Function<Database, SimpleChannelInboundHandler<T>> handlerFactory, Class<T> messageType,
      LogProvider logProvider) {
    super(messageType);
    this.messageType = messageType;
    this.databaseManager = databaseManager;
    this.handlerFactory = handlerFactory;
    this.protocol = protocol;
    this.logProvider = logProvider;
  }

  protected void channelRead0(ChannelHandlerContext ctx, T request) throws Exception {
    DatabaseId databaseId = request.databaseId();
    DatabaseContext databaseContext = this.databaseManager.getDatabaseContext(databaseId)
        .orElse(null);
    SimpleChannelInboundHandler<T> handler = this.createHandler(databaseContext);
    handler.channelRead(ctx, request);
  }

  private SimpleChannelInboundHandler<T> createHandler(DatabaseContext databaseContext) {
    if (databaseContext == null) {
      return new UnknownDatabaseHandler(this.messageType, this.protocol, this.logProvider);
    } else {
      DatabaseAvailabilityGuard availabilityGuard = databaseContext.database()
          .getDatabaseAvailabilityGuard();
      return (SimpleChannelInboundHandler) (!availabilityGuard.isAvailable()
          ? new UnavailableDatabaseHandler(this.messageType, this.protocol,
          availabilityGuard, this.logProvider)
          : this.handlerFactory
              .apply(databaseContext.database()));
    }
  }
}
