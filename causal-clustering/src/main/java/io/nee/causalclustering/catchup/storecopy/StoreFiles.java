/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Set;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.storageengine.api.StoreId;

public class StoreFiles {

  public static final FilenameFilter EXCLUDE_TEMPORARY_DIRS = (dir, name) ->
  {
    return !name.equals("temp-copy") && !name.equals("temp-bootstrap") && !name.equals("temp-save");
  };
  private final FileSystemAbstraction fs;
  private final PageCache pageCache;
  private final FilenameFilter filenameFilter;

  public StoreFiles(FileSystemAbstraction fs, PageCache pageCache) {
    this(fs, pageCache, EXCLUDE_TEMPORARY_DIRS);
  }

  public StoreFiles(FileSystemAbstraction fs, PageCache pageCache, FilenameFilter filenameFilter) {
    this.fs = fs;
    this.pageCache = pageCache;
    this.filenameFilter = filenameFilter;
  }

  public void delete(DatabaseLayout databaseLayout, LogFiles logFiles) throws IOException {
    File databaseDirectory = databaseLayout.databaseDirectory();
    File[] files = this.fs.listFiles(databaseDirectory, this.filenameFilter);
    File[] n5;
    int n6;
    int n7;
    File txLog;
    if (files != null) {
      n5 = files;
      n6 = files.length;

      for (n7 = 0; n7 < n6; ++n7) {
        txLog = n5[n7];
        this.fs.deleteRecursively(txLog);
      }
    }

    n5 = logFiles.logFiles();
    n6 = n5.length;

    for (n7 = 0; n7 < n6; ++n7) {
      txLog = n5[n7];
      this.fs.deleteFile(txLog);
    }

    this.fs.deleteFile(databaseDirectory);
  }

  public void delete(LogFiles logFiles) {
    File[] n2 = logFiles.logFiles();
    int n3 = n2.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      File txLog = n2[n4];
      this.fs.deleteFile(txLog);
    }
  }

  public void moveTo(File source, DatabaseLayout target, LogFiles logFiles) throws IOException {
    this.fs.mkdirs(logFiles.logFilesDirectory());
    File[] files = this.fs.listFiles(source, this.filenameFilter);
    if (files != null) {
      File[] n5 = files;
      int n6 = files.length;

      for (int n7 = 0; n7 < n6; ++n7) {
        File file = n5[n7];
        File destination = logFiles.isLogFile(file) ? target.getTransactionLogsDirectory()
            : target.databaseDirectory();
        this.fs.moveToDirectory(file, destination);
      }
    }
  }

  public boolean isEmpty(DatabaseLayout databaseLayout) {
    Set<File> storeFiles = databaseLayout.storeFiles();
    File[] files = this.fs.listFiles(databaseLayout.databaseDirectory());
    if (files != null) {
      File[] n4 = files;
      int n5 = files.length;

      for (int n6 = 0; n6 < n5; ++n6) {
        File file = n4[n6];
        if (storeFiles.contains(file)) {
          return false;
        }
      }
    }

    return true;
  }

  public StoreId readStoreId(DatabaseLayout databaseLayout) throws IOException {
    return StorageEngineFactory.selectStorageEngine().storeId(databaseLayout, this.pageCache);
  }
}
