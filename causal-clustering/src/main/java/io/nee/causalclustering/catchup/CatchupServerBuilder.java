/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.catchup.v3.CatchupProtocolServerInstaller;
import io.nee.causalclustering.net.BootstrapConfiguration;
import io.nee.causalclustering.net.Server;
import io.nee.causalclustering.protocol.ModifierProtocolInstaller;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.ProtocolInstaller;
import io.nee.causalclustering.protocol.ProtocolInstallerRepository;
import io.nee.causalclustering.protocol.application.ApplicationProtocols;
import io.nee.causalclustering.protocol.handshake.ApplicationProtocolRepository;
import io.nee.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import io.nee.causalclustering.protocol.handshake.HandshakeServerInitializer;
import io.nee.causalclustering.protocol.handshake.ModifierProtocolRepository;
import io.nee.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import io.nee.causalclustering.protocol.init.ServerChannelInitializer;
import io.nee.causalclustering.protocol.modifier.ModifierProtocols;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.socket.ServerSocketChannel;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Executor;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public final class CatchupServerBuilder {

  private CatchupServerBuilder() {
  }

  public static CatchupServerBuilder.NeedsCatchupServerHandler builder() {
    return new CatchupServerBuilder.StepBuilder();
  }

  public interface AcceptsOptionalParams {

    CatchupServerBuilder.AcceptsOptionalParams serverName(String n1);

    CatchupServerBuilder.AcceptsOptionalParams userLogProvider(LogProvider n1);

    CatchupServerBuilder.AcceptsOptionalParams debugLogProvider(LogProvider n1);

    CatchupServerBuilder.AcceptsOptionalParams handshakeTimeout(Duration n1);

    Server build();
  }

  public interface NeedsPortRegister {

    CatchupServerBuilder.AcceptsOptionalParams portRegister(ConnectorPortRegister n1);
  }

  public interface NeedsBootstrapConfig {

    CatchupServerBuilder.NeedsPortRegister bootstrapConfig(
        BootstrapConfiguration<? extends ServerSocketChannel> n1);
  }

  public interface NeedsConfig {

    CatchupServerBuilder.NeedsBootstrapConfig config(Config n1);
  }

  public interface NeedsScheduler {

    CatchupServerBuilder.NeedsConfig scheduler(JobScheduler n1);
  }

  public interface NeedsListenAddress {

    CatchupServerBuilder.NeedsScheduler listenAddress(SocketAddress n1);
  }

  public interface NeedsInstalledProtocolsHandler {

    CatchupServerBuilder.NeedsListenAddress installedProtocolsHandler(ChannelInboundHandler n1);
  }

  public interface NeedsPipelineBuilder {

    CatchupServerBuilder.NeedsInstalledProtocolsHandler pipelineBuilder(
        NettyPipelineBuilderFactory n1);
  }

  public interface NeedsModifierProtocols {

    CatchupServerBuilder.NeedsPipelineBuilder modifierProtocols(
        Collection<ModifierSupportedProtocols> n1);
  }

  public interface NeedsCatchupProtocols {

    CatchupServerBuilder.NeedsModifierProtocols catchupProtocols(ApplicationSupportedProtocols n1);
  }

  public interface NeedsCatchupServerHandler {

    CatchupServerBuilder.NeedsCatchupProtocols catchupServerHandler(CatchupServerHandler n1);
  }

  private static class StepBuilder
      implements CatchupServerBuilder.NeedsCatchupServerHandler,
      CatchupServerBuilder.NeedsCatchupProtocols, CatchupServerBuilder.NeedsModifierProtocols,
      CatchupServerBuilder.NeedsPipelineBuilder,
      CatchupServerBuilder.NeedsInstalledProtocolsHandler, CatchupServerBuilder.NeedsListenAddress,
      CatchupServerBuilder.NeedsScheduler, CatchupServerBuilder.NeedsConfig,
      CatchupServerBuilder.NeedsBootstrapConfig,
      CatchupServerBuilder.NeedsPortRegister, CatchupServerBuilder.AcceptsOptionalParams {

    private CatchupServerHandler catchupServerHandler;
    private NettyPipelineBuilderFactory pipelineBuilder;
    private ApplicationSupportedProtocols catchupProtocols;
    private Collection<ModifierSupportedProtocols> modifierProtocols;
    private ChannelInboundHandler parentHandler;
    private SocketAddress listenAddress;
    private JobScheduler scheduler;
    private LogProvider debugLogProvider = NullLogProvider.getInstance();
    private LogProvider userLogProvider = NullLogProvider.getInstance();
    private Duration handshakeTimeout = Duration.ofSeconds(5L);
    private ConnectorPortRegister portRegister;
    private String serverName = "catchup-server";
    private BootstrapConfiguration<? extends ServerSocketChannel> bootstrapConfiguration;
    private Config config = Config.defaults();

    public CatchupServerBuilder.NeedsCatchupProtocols catchupServerHandler(
        CatchupServerHandler catchupServerHandler) {
      this.catchupServerHandler = catchupServerHandler;
      return this;
    }

    public CatchupServerBuilder.NeedsModifierProtocols catchupProtocols(
        ApplicationSupportedProtocols catchupProtocols) {
      this.catchupProtocols = catchupProtocols;
      return this;
    }

    public CatchupServerBuilder.NeedsPipelineBuilder modifierProtocols(
        Collection<ModifierSupportedProtocols> modifierProtocols) {
      this.modifierProtocols = modifierProtocols;
      return this;
    }

    public CatchupServerBuilder.NeedsInstalledProtocolsHandler pipelineBuilder(
        NettyPipelineBuilderFactory pipelineBuilder) {
      this.pipelineBuilder = pipelineBuilder;
      return this;
    }

    public CatchupServerBuilder.NeedsListenAddress installedProtocolsHandler(
        ChannelInboundHandler parentHandler) {
      this.parentHandler = parentHandler;
      return this;
    }

    public CatchupServerBuilder.NeedsScheduler listenAddress(SocketAddress listenAddress) {
      this.listenAddress = listenAddress;
      return this;
    }

    public CatchupServerBuilder.NeedsConfig scheduler(JobScheduler scheduler) {
      this.scheduler = scheduler;
      return this;
    }

    public CatchupServerBuilder.AcceptsOptionalParams portRegister(
        ConnectorPortRegister portRegister) {
      this.portRegister = portRegister;
      return this;
    }

    public CatchupServerBuilder.AcceptsOptionalParams serverName(String serverName) {
      this.serverName = serverName;
      return this;
    }

    public CatchupServerBuilder.AcceptsOptionalParams userLogProvider(LogProvider userLogProvider) {
      this.userLogProvider = userLogProvider;
      return this;
    }

    public CatchupServerBuilder.AcceptsOptionalParams debugLogProvider(
        LogProvider debugLogProvider) {
      this.debugLogProvider = debugLogProvider;
      return this;
    }

    public CatchupServerBuilder.AcceptsOptionalParams handshakeTimeout(Duration handshakeTimeout) {
      this.handshakeTimeout = handshakeTimeout;
      return this;
    }

    public CatchupServerBuilder.NeedsBootstrapConfig config(Config config) {
      this.config = config;
      return this;
    }

    public CatchupServerBuilder.NeedsPortRegister bootstrapConfig(
        BootstrapConfiguration<? extends ServerSocketChannel> bootstrapConfiguration) {
      this.bootstrapConfiguration = bootstrapConfiguration;
      return this;
    }

    public Server build() {
      ApplicationProtocolRepository applicationProtocolRepository =
          new ApplicationProtocolRepository(ApplicationProtocols.values(), this.catchupProtocols);
      ModifierProtocolRepository modifierProtocolRepository = new ModifierProtocolRepository(
          ModifierProtocols.values(), this.modifierProtocols);
      List<ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Server, ?>> protocolInstallers =
          List.of(new CatchupProtocolServerInstaller.Factory(this.pipelineBuilder,
              this.debugLogProvider, this.catchupServerHandler));
      ProtocolInstallerRepository<ProtocolInstaller.Orientation.Server> protocolInstallerRepository =
          new ProtocolInstallerRepository(protocolInstallers,
              ModifierProtocolInstaller.allServerInstallers);
      HandshakeServerInitializer handshakeInitializer =
          new HandshakeServerInitializer(applicationProtocolRepository, modifierProtocolRepository,
              protocolInstallerRepository,
              this.pipelineBuilder, this.debugLogProvider, this.config);
      ServerChannelInitializer channelInitializer =
          new ServerChannelInitializer(handshakeInitializer, this.pipelineBuilder,
              this.handshakeTimeout, this.debugLogProvider, this.config);
      Executor executor = this.scheduler.executor(Group.CATCHUP_SERVER);
      return new Server(channelInitializer, this.parentHandler, this.debugLogProvider,
          this.userLogProvider, this.listenAddress, this.serverName,
          executor, this.portRegister, this.bootstrapConfiguration);
    }
  }
}
