/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.dbms.database.ClusteredDatabaseContext;
import java.io.IOException;
import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StoreId;

public class StoreCopyProcess {

  private final FileSystemAbstraction fs;
  private final PageCache pageCache;
  private final ClusteredDatabaseContext clusteredDatabaseContext;
  private final Config config;
  private final CopiedStoreRecovery copiedStoreRecovery;
  private final Log log;
  private final RemoteStore remoteStore;

  public StoreCopyProcess(FileSystemAbstraction fs, PageCache pageCache,
      ClusteredDatabaseContext clusteredDatabaseContext,
      CopiedStoreRecovery copiedStoreRecovery, RemoteStore remoteStore, LogProvider logProvider) {
    this.fs = fs;
    this.pageCache = pageCache;
    this.clusteredDatabaseContext = clusteredDatabaseContext;
    this.config = clusteredDatabaseContext.database().getConfig();
    this.copiedStoreRecovery = copiedStoreRecovery;
    this.remoteStore = remoteStore;
    this.log = logProvider.getLog(this.getClass());
  }

  public void replaceWithStoreFrom(CatchupAddressProvider addressProvider, StoreId expectedStoreId)
      throws IOException, StoreCopyFailedException, DatabaseShutdownException {
    TemporaryStoreDirectory tempStore = new TemporaryStoreDirectory(this.fs, this.pageCache,
        this.clusteredDatabaseContext.databaseLayout());

    try {
      this.remoteStore.copy(addressProvider, expectedStoreId, tempStore.databaseLayout(), false);

      try {
        this.copiedStoreRecovery.recoverCopiedStore(this.config, tempStore.databaseLayout());
      } catch (Throwable n7) {
        tempStore.keepStore();
        throw n7;
      }

      this.clusteredDatabaseContext.replaceWith(tempStore.databaseLayout().databaseDirectory());
    } catch (Throwable n8) {
      try {
        tempStore.close();
      } catch (Throwable n6) {
        n8.addSuppressed(n6);
      }

      throw n8;
    }

    tempStore.close();
    this.log.info("Replaced store successfully");
  }
}
