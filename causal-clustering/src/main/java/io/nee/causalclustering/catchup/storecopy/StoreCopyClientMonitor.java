/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

public interface StoreCopyClientMonitor {

  void start();

  void startReceivingStoreFiles();

  void finishReceivingStoreFiles();

  void startReceivingStoreFile(String n1);

  void finishReceivingStoreFile(String n1);

  void startReceivingTransactions(long n1);

  void finishReceivingTransactions(long n1);

  void startRecoveringStore();

  void finishRecoveringStore();

  void startReceivingIndexSnapshots();

  void startReceivingIndexSnapshot(long n1);

  void finishReceivingIndexSnapshot(long n1);

  void finishReceivingIndexSnapshots();

  void finish();

  class Adapter implements StoreCopyClientMonitor {

    public void start() {
    }

    public void startReceivingStoreFiles() {
    }

    public void finishReceivingStoreFiles() {
    }

    public void startReceivingStoreFile(String file) {
    }

    public void finishReceivingStoreFile(String file) {
    }

    public void startReceivingTransactions(long startTxId) {
    }

    public void finishReceivingTransactions(long endTxId) {
    }

    public void startRecoveringStore() {
    }

    public void finishRecoveringStore() {
    }

    public void startReceivingIndexSnapshots() {
    }

    public void startReceivingIndexSnapshot(long indexId) {
    }

    public void finishReceivingIndexSnapshot(long indexId) {
    }

    public void finishReceivingIndexSnapshots() {
    }

    public void finish() {
    }
  }
}
