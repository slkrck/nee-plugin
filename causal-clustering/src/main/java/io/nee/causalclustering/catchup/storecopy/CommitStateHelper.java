/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import java.io.IOException;
import java.util.Optional;
import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.NoSuchTransactionException;
import org.neo4j.kernel.impl.transaction.log.ReadOnlyTransactionStore;
import org.neo4j.kernel.impl.transaction.log.TransactionCursor;
import org.neo4j.kernel.impl.transaction.log.files.LogFilesBuilder;
import org.neo4j.kernel.lifecycle.Lifespan;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StorageEngineFactory;
import org.neo4j.storageengine.api.TransactionIdStore;

public class CommitStateHelper {

  private final StorageEngineFactory storageEngineFactory;
  private final PageCache pageCache;
  private final FileSystemAbstraction fs;
  private final Config config;

  public CommitStateHelper(PageCache pageCache, FileSystemAbstraction fs, Config config,
      StorageEngineFactory storageEngineFactory) {
    this.pageCache = pageCache;
    this.fs = fs;
    this.config = config;
    this.storageEngineFactory = storageEngineFactory;
  }

  CommitState getStoreState(DatabaseLayout databaseLayout) throws IOException {
    TransactionIdStore txIdStore = this.storageEngineFactory
        .readOnlyTransactionIdStore(this.fs, databaseLayout, this.pageCache);
    long lastCommittedTxId = txIdStore.getLastCommittedTransactionId();
    Optional<Long> latestTransactionLogIndex = this
        .getLatestTransactionLogIndex(lastCommittedTxId, databaseLayout);
    return latestTransactionLogIndex.isPresent() ? new CommitState(lastCommittedTxId,
        latestTransactionLogIndex.get())
        : new CommitState(lastCommittedTxId);
  }

  private Optional<Long> getLatestTransactionLogIndex(long startTxId, DatabaseLayout databaseLayout)
      throws IOException {
    if (!this.hasTxLogs(databaseLayout)) {
      return Optional.empty();
    } else {
      ReadOnlyTransactionStore txStore = new ReadOnlyTransactionStore(this.pageCache, this.fs,
          databaseLayout, this.config, new Monitors());
      long lastTxId = 1L;

      try {
        Lifespan ignored = new Lifespan(txStore);

        Optional n16;
        try {
          TransactionCursor cursor = txStore.getTransactions(startTxId);

          try {
            while (cursor.next()) {
              CommittedTransactionRepresentation tx = cursor.get();
              lastTxId = tx.getCommitEntry().getTxId();
            }

            n16 = Optional.of(lastTxId);
          } catch (Throwable n13) {
            if (cursor != null) {
              try {
                cursor.close();
              } catch (Throwable n12) {
                n13.addSuppressed(n12);
              }
            }

            throw n13;
          }

          if (cursor != null) {
            cursor.close();
          }
        } catch (Throwable n14) {
          try {
            ignored.close();
          } catch (Throwable n11) {
            n14.addSuppressed(n11);
          }

          throw n14;
        }

        ignored.close();
        return n16;
      } catch (NoSuchTransactionException n15) {
        return Optional.empty();
      }
    }
  }

  public boolean hasTxLogs(DatabaseLayout databaseLayout) throws IOException {
    return LogFilesBuilder.activeFilesBuilder(databaseLayout, this.fs, this.pageCache)
        .withConfig(this.config).build().logFiles().length > 0;
  }
}
