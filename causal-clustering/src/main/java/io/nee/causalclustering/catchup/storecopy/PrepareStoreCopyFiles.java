/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import java.io.File;
import java.io.IOException;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.FileUtils;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.kernel.database.Database;
import org.neo4j.storageengine.api.StoreFileMetadata;

public class PrepareStoreCopyFiles implements AutoCloseable {

  private final Database database;
  private final FileSystemAbstraction fileSystemAbstraction;
  private final CloseablesListener closeablesListener = new CloseablesListener();

  PrepareStoreCopyFiles(Database database, FileSystemAbstraction fileSystemAbstraction) {
    this.database = database;
    this.fileSystemAbstraction = fileSystemAbstraction;
  }

  private static Predicate<StoreFileMetadata> isCountFile(DatabaseLayout databaseLayout) {
    return (storeFileMetadata) ->
    {
      return databaseLayout.countStore().equals(storeFileMetadata.file());
    };
  }

  StoreResource[] getAtomicFilesSnapshot() throws IOException {
    ResourceIterator<StoreFileMetadata> neoStoreFilesIterator =
        this.closeablesListener.add(
            this.database.getDatabaseFileListing().builder().excludeAll().includeNeoStoreFiles()
                .build());
    ResourceIterator<StoreFileMetadata> indexIterator = this.closeablesListener.add(
        this.database.getDatabaseFileListing().builder().excludeAll().includeAdditionalProviders()
            .includeLabelScanStoreFiles()
            .includeSchemaIndexStoreFiles().includeIdFiles().build());
    return Stream.concat(
        neoStoreFilesIterator.stream().filter(isCountFile(this.database.getDatabaseLayout())),
        indexIterator.stream()).map(this.mapToStoreResource()).toArray((n) ->
    {
      return new StoreResource[n];
    });
  }

  private Function<StoreFileMetadata, StoreResource> mapToStoreResource() {
    return (storeFileMetadata) ->
    {
      try {
        return this.toStoreResource(storeFileMetadata);
      } catch (IOException n3) {
        throw new IllegalStateException("Unable to create store resource", n3);
      }
    };
  }

  File[] listReplayableFiles() throws IOException {
    Stream stream = this.database.getDatabaseFileListing().builder().excludeAll()
        .includeNeoStoreFiles().build().stream();

    File[] n2;
    try {
      n2 = (File[]) stream.filter(isCountFile(this.database.getDatabaseLayout()).negate())
          .map(s -> s).toArray(File[]::new);
    } catch (Throwable n5) {
      if (stream != null) {
        try {
          stream.close();
        } catch (Throwable n4) {
          n5.addSuppressed(n4);
        }
      }

      throw n5;
    }

    if (stream != null) {
      stream.close();
    }

    return n2;
  }

  private StoreResource toStoreResource(StoreFileMetadata storeFileMetadata) throws IOException {
    File databaseDirectory = this.database.getDatabaseLayout().databaseDirectory();
    File file = storeFileMetadata.file();
    String relativePath = FileUtils.relativePath(databaseDirectory, file);
    return new StoreResource(file, relativePath, storeFileMetadata.recordSize(),
        this.fileSystemAbstraction);
  }

  public void close() {
    this.closeablesListener.close();
  }
}
