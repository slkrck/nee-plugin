/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.v3.databaseid;

import io.nee.causalclustering.catchup.CatchupErrorResponse;
import io.nee.causalclustering.catchup.CatchupResult;
import io.nee.causalclustering.catchup.CatchupServerProtocol;
import io.nee.causalclustering.catchup.ResponseMessageType;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.util.Optional;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.NamedDatabaseId;

public class GetDatabaseIdRequestHandler extends SimpleChannelInboundHandler<GetDatabaseIdRequest> {

  private final CatchupServerProtocol protocol;
  private final DatabaseIdRepository databaseIdRepository;

  public GetDatabaseIdRequestHandler(DatabaseIdRepository databaseIdRepository,
      CatchupServerProtocol protocol) {
    this.protocol = protocol;
    this.databaseIdRepository = databaseIdRepository;
  }

  private static CatchupErrorResponse unknownDatabaseResponse(String databaseName) {
    return new CatchupErrorResponse(CatchupResult.E_DATABASE_UNKNOWN,
        "Database '" + databaseName + "' does not exist");
  }

  protected void channelRead0(ChannelHandlerContext ctx, GetDatabaseIdRequest msg) {
    String databaseName = msg.databaseName();
    Optional<NamedDatabaseId> databaseIdOptional = this.databaseIdRepository
        .getByName(databaseName);
    if (databaseIdOptional.isPresent()) {
      ctx.write(ResponseMessageType.DATABASE_ID_RESPONSE);
      ctx.writeAndFlush(databaseIdOptional.get().databaseId());
    } else {
      ctx.write(ResponseMessageType.ERROR);
      ctx.writeAndFlush(unknownDatabaseResponse(databaseName));
    }

    this.protocol.expect(CatchupServerProtocol.State.MESSAGE_TYPE);
  }
}
