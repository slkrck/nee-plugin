/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.v3.storecopy;

import io.nee.causalclustering.catchup.RequestMessageType;
import io.nee.causalclustering.messaging.CatchupProtocolMessage;
import java.util.Objects;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.storageengine.api.StoreId;

public class PrepareStoreCopyRequest extends CatchupProtocolMessage.WithDatabaseId {

  private final StoreId storeId;

  public PrepareStoreCopyRequest(StoreId expectedStoreId, DatabaseId databaseId) {
    super(RequestMessageType.PREPARE_STORE_COPY, databaseId);
    this.storeId = expectedStoreId;
  }

  public StoreId storeId() {
    return this.storeId;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      if (!super.equals(o)) {
        return false;
      } else {
        PrepareStoreCopyRequest that = (PrepareStoreCopyRequest) o;
        return Objects.equals(this.storeId, that.storeId);
      }
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(super.hashCode(), this.storeId);
  }

  public String toString() {
    StoreId n10000 = this.storeId;
    return "PrepareStoreCopyRequest{storeId=" + n10000 + ", databaseId='" + this.databaseId() + "}";
  }
}
