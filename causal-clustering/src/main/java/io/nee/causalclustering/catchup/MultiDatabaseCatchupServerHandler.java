/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.catchup.storecopy.GetStoreFileRequestHandler;
import io.nee.causalclustering.catchup.storecopy.PrepareStoreCopyFilesProvider;
import io.nee.causalclustering.catchup.storecopy.PrepareStoreCopyRequestHandler;
import io.nee.causalclustering.catchup.storecopy.StoreFileStreamingProtocol;
import io.nee.causalclustering.catchup.tx.TxPullRequestHandler;
import io.nee.causalclustering.catchup.v3.databaseid.GetDatabaseIdRequestHandler;
import io.nee.causalclustering.catchup.v3.storecopy.GetStoreFileRequest;
import io.nee.causalclustering.catchup.v3.storecopy.GetStoreIdRequest;
import io.nee.causalclustering.catchup.v3.storecopy.GetStoreIdRequestHandler;
import io.nee.causalclustering.catchup.v3.storecopy.PrepareStoreCopyRequest;
import io.nee.causalclustering.catchup.v3.tx.TxPullRequest;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshotRequest;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshotRequestHandler;
import io.netty.channel.ChannelHandler;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.database.Database;
import org.neo4j.logging.LogProvider;

public class MultiDatabaseCatchupServerHandler implements CatchupServerHandler {

  private final DatabaseManager<?> databaseManager;
  private final LogProvider logProvider;
  private final FileSystemAbstraction fs;
  private final int maxChunkSize;

  public MultiDatabaseCatchupServerHandler(DatabaseManager<?> databaseManager,
      FileSystemAbstraction fs, int maxChunkSize, LogProvider logProvider) {
    this.databaseManager = databaseManager;
    this.maxChunkSize = maxChunkSize;
    this.logProvider = logProvider;
    this.fs = fs;
  }

  private static GetStoreIdRequestHandler buildStoreIdRequestHandler(Database db,
      CatchupServerProtocol protocol) {
    return new GetStoreIdRequestHandler(protocol, db);
  }

  private static CoreSnapshotRequestHandler buildCoreSnapshotRequestRequestHandler(Database db,
      CatchupServerProtocol protocol) {
    return new CoreSnapshotRequestHandler(protocol, db);
  }

  public ChannelHandler getDatabaseIdRequestHandler(CatchupServerProtocol protocol) {
    return new GetDatabaseIdRequestHandler(this.databaseManager.databaseIdRepository(), protocol);
  }

  public ChannelHandler txPullRequestHandler(CatchupServerProtocol protocol) {
    return new MultiplexingCatchupRequestHandler(protocol, this.databaseManager, (db) ->
    {
      return this.buildTxPullRequestHandler((Database) db, protocol);
    }, TxPullRequest.class, this.logProvider);
  }

  public ChannelHandler getStoreIdRequestHandler(CatchupServerProtocol protocol) {
    return new MultiplexingCatchupRequestHandler(protocol, this.databaseManager, (db) ->
    {
      return buildStoreIdRequestHandler((Database) db, protocol);
    }, GetStoreIdRequest.class, this.logProvider);
  }

  public ChannelHandler storeListingRequestHandler(CatchupServerProtocol protocol) {
    return new MultiplexingCatchupRequestHandler(protocol, this.databaseManager, (db) ->
    {
      return this.buildStoreListingRequestHandler((Database) db, protocol);
    }, PrepareStoreCopyRequest.class, this.logProvider);
  }

  public ChannelHandler getStoreFileRequestHandler(CatchupServerProtocol protocol) {
    return new MultiplexingCatchupRequestHandler(protocol, this.databaseManager, (db) ->
    {
      return this.buildStoreFileRequestHandler((Database) db, protocol);
    }, GetStoreFileRequest.class, this.logProvider);
  }

  public ChannelHandler snapshotHandler(CatchupServerProtocol protocol) {
    return new MultiplexingCatchupRequestHandler(protocol, this.databaseManager, (db) ->
    {
      return buildCoreSnapshotRequestRequestHandler((Database) db, protocol);
    }, CoreSnapshotRequest.class, this.logProvider);
  }

  private TxPullRequestHandler buildTxPullRequestHandler(Database db,
      CatchupServerProtocol protocol) {
    return new TxPullRequestHandler(protocol, db);
  }

  private PrepareStoreCopyRequestHandler buildStoreListingRequestHandler(Database db,
      CatchupServerProtocol protocol) {
    return new PrepareStoreCopyRequestHandler(protocol, db,
        new PrepareStoreCopyFilesProvider(this.fs), this.maxChunkSize);
  }

  private GetStoreFileRequestHandler buildStoreFileRequestHandler(Database db,
      CatchupServerProtocol protocol) {
    return new GetStoreFileRequestHandler(protocol, db,
        new StoreFileStreamingProtocol(this.maxChunkSize), this.fs);
  }
}
