/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.v3;

import io.nee.causalclustering.catchup.CatchupServerHandler;
import io.nee.causalclustering.catchup.CatchupServerProtocol;
import io.nee.causalclustering.catchup.RequestDecoderDispatcher;
import io.nee.causalclustering.catchup.RequestMessageTypeEncoder;
import io.nee.causalclustering.catchup.ResponseMessageTypeEncoder;
import io.nee.causalclustering.catchup.ServerMessageTypeHandler;
import io.nee.causalclustering.catchup.storecopy.FileChunkEncoder;
import io.nee.causalclustering.catchup.storecopy.FileHeaderEncoder;
import io.nee.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import io.nee.causalclustering.catchup.tx.TxStreamFinishedResponseEncoder;
import io.nee.causalclustering.catchup.v3.databaseid.GetDatabaseIdRequestDecoder;
import io.nee.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponseEncoder;
import io.nee.causalclustering.catchup.v3.storecopy.CatchupErrorResponseEncoder;
import io.nee.causalclustering.catchup.v3.storecopy.CoreSnapshotRequestDecoder;
import io.nee.causalclustering.catchup.v3.storecopy.GetStoreFileRequestDecoder;
import io.nee.causalclustering.catchup.v3.storecopy.GetStoreIdRequestDecoder;
import io.nee.causalclustering.catchup.v3.storecopy.GetStoreIdResponseEncoder;
import io.nee.causalclustering.catchup.v3.storecopy.PrepareStoreCopyRequestDecoder;
import io.nee.causalclustering.catchup.v3.storecopy.StoreCopyFinishedResponseEncoder;
import io.nee.causalclustering.catchup.v3.tx.TxPullRequestDecoder;
import io.nee.causalclustering.catchup.v3.tx.TxPullResponseEncoder;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshotEncoder;
import io.nee.causalclustering.protocol.ModifierProtocolInstaller;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.ProtocolInstaller;
import io.nee.causalclustering.protocol.application.ApplicationProtocol;
import io.nee.causalclustering.protocol.application.ApplicationProtocols;
import io.nee.causalclustering.protocol.modifier.ModifierProtocol;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelInboundHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class CatchupProtocolServerInstaller implements
    ProtocolInstaller<ProtocolInstaller.Orientation.Server> {

  private static final ApplicationProtocols APPLICATION_PROTOCOL;

  static {
    APPLICATION_PROTOCOL = ApplicationProtocols.CATCHUP_3_0;
  }

  private final NettyPipelineBuilderFactory pipelineBuilderFactory;
  private final List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Server>> modifiers;
  private final Log log;
  private final LogProvider logProvider;
  private final CatchupServerHandler catchupServerHandler;

  public CatchupProtocolServerInstaller(NettyPipelineBuilderFactory pipelineBuilderFactory,
      List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Server>> modifiers,
      LogProvider logProvider,
      CatchupServerHandler catchupServerHandler) {
    this.pipelineBuilderFactory = pipelineBuilderFactory;
    this.modifiers = modifiers;
    this.log = logProvider.getLog(this.getClass());
    this.logProvider = logProvider;
    this.catchupServerHandler = catchupServerHandler;
  }

  public void install(Channel channel) {
    CatchupServerProtocol state = new CatchupServerProtocol();
    this.pipelineBuilderFactory
        .server(
            channel, this.log).modify(this.modifiers).addFraming()
        .add("enc_req_type", new ChannelHandler[]{new RequestMessageTypeEncoder()})
        .add(
            "enc_res_type", new ChannelHandler[]{new ResponseMessageTypeEncoder()})
        .add("enc_res_tx_pull",
            new ChannelHandler[]{new TxPullResponseEncoder()})
        .add("enc_res_store_id", new ChannelHandler[]{new GetStoreIdResponseEncoder()}).add(
        "enc_res_database_id", new ChannelHandler[]{new GetDatabaseIdResponseEncoder()})
        .add("enc_res_copy_fin",
            new ChannelHandler[]{
                new StoreCopyFinishedResponseEncoder()})
        .add("enc_res_tx_fin",
            new ChannelHandler[]{new TxStreamFinishedResponseEncoder()}).add("enc_res_pre_copy",
        new ChannelHandler[]{new PrepareStoreCopyResponse.Encoder()})
        .add("enc_snapshot", new ChannelHandler[]{new CoreSnapshotEncoder()}).add(
        "enc_file_chunk", new ChannelHandler[]{new FileChunkEncoder()})
        .add("enc_file_header", new ChannelHandler[]{new FileHeaderEncoder()}).add(
        "enc_catchup_error", new ChannelHandler[]{new CatchupErrorResponseEncoder()})
        .add("in_req_type",
            new ChannelHandler[]{this.serverMessageHandler(state)})
        .add("dec_req_dispatch", new ChannelHandler[]{this.requestDecoders(state)}).add(
        "out_chunked_write", new ChannelHandler[]{new ChunkedWriteHandler()})
        .add("hnd_req_database_id",
            new ChannelHandler[]{
                this.catchupServerHandler.getDatabaseIdRequestHandler(
                    state)}).add("hnd_req_tx",
        new ChannelHandler[]{
            this.catchupServerHandler.txPullRequestHandler(
                state)})
        .add("hnd_req_store_id",
            new ChannelHandler[]{this.catchupServerHandler.getStoreIdRequestHandler(state)})
        .add("hnd_req_store_listing",
            new ChannelHandler[]{
                this.catchupServerHandler.storeListingRequestHandler(
                    state)})
        .add("hnd_req_store_file",
            new ChannelHandler[]{this.catchupServerHandler.getStoreFileRequestHandler(state)})
        .add("hnd_req_snapshot",
            new ChannelHandler[]{
                this.catchupServerHandler.snapshotHandler(
                    state)}).install();
  }

  private ChannelHandler serverMessageHandler(CatchupServerProtocol state) {
    return new ServerMessageTypeHandler(state, this.logProvider);
  }

  private ChannelInboundHandler requestDecoders(CatchupServerProtocol protocol) {
    RequestDecoderDispatcher<CatchupServerProtocol.State> decoderDispatcher = new RequestDecoderDispatcher(
        protocol, this.logProvider);
    decoderDispatcher
        .register(CatchupServerProtocol.State.GET_DATABASE_ID, new GetDatabaseIdRequestDecoder());
    decoderDispatcher.register(CatchupServerProtocol.State.TX_PULL, new TxPullRequestDecoder());
    decoderDispatcher
        .register(CatchupServerProtocol.State.GET_STORE_ID, new GetStoreIdRequestDecoder());
    decoderDispatcher
        .register(CatchupServerProtocol.State.GET_CORE_SNAPSHOT, new CoreSnapshotRequestDecoder());
    decoderDispatcher.register(CatchupServerProtocol.State.PREPARE_STORE_COPY,
        new PrepareStoreCopyRequestDecoder());
    decoderDispatcher
        .register(CatchupServerProtocol.State.GET_STORE_FILE, new GetStoreFileRequestDecoder());
    return decoderDispatcher;
  }

  public ApplicationProtocol applicationProtocol() {
    return APPLICATION_PROTOCOL;
  }

  public Collection<Collection<ModifierProtocol>> modifiers() {
    return this.modifiers.stream().map(ModifierProtocolInstaller::protocols)
        .collect(Collectors.toList());
  }

  public static class Factory extends
      ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Server, CatchupProtocolServerInstaller> {

    public Factory(NettyPipelineBuilderFactory pipelineBuilderFactory, LogProvider logProvider,
        CatchupServerHandler catchupServerHandler) {
      super(CatchupProtocolServerInstaller.APPLICATION_PROTOCOL, (modifiers) ->
      {
        return new CatchupProtocolServerInstaller(pipelineBuilderFactory, modifiers, logProvider,
            catchupServerHandler);
      });
    }
  }
}
