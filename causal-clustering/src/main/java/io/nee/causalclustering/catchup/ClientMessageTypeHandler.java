/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ClientMessageTypeHandler extends ChannelInboundHandlerAdapter {

  private final Log log;
  private final CatchupClientProtocol protocol;

  public ClientMessageTypeHandler(CatchupClientProtocol protocol, LogProvider logProvider) {
    this.protocol = protocol;
    this.log = logProvider.getLog(this.getClass());
  }

  public void channelRead(ChannelHandlerContext ctx, Object msg) {
    if (this.protocol.isExpecting(CatchupClientProtocol.State.MESSAGE_TYPE)) {
      byte byteValue = ((ByteBuf) msg).readByte();
      ResponseMessageType responseMessageType = ResponseMessageType.from(byteValue);
      switch (responseMessageType) {
        case STORE_ID:
          this.protocol.expect(CatchupClientProtocol.State.STORE_ID);
          break;
        case TX:
          this.protocol.expect(CatchupClientProtocol.State.TX_PULL_RESPONSE);
          break;
        case FILE:
          this.protocol.expect(CatchupClientProtocol.State.FILE_HEADER);
          break;
        case STORE_COPY_FINISHED:
          this.protocol.expect(CatchupClientProtocol.State.STORE_COPY_FINISHED);
          break;
        case CORE_SNAPSHOT:
          this.protocol.expect(CatchupClientProtocol.State.CORE_SNAPSHOT);
          break;
        case TX_STREAM_FINISHED:
          this.protocol.expect(CatchupClientProtocol.State.TX_STREAM_FINISHED);
          break;
        case PREPARE_STORE_COPY_RESPONSE:
          this.protocol.expect(CatchupClientProtocol.State.PREPARE_STORE_COPY_RESPONSE);
          break;
        case INDEX_SNAPSHOT_RESPONSE:
          this.protocol.expect(CatchupClientProtocol.State.INDEX_SNAPSHOT_RESPONSE);
          break;
        case ERROR:
          this.protocol.expect(CatchupClientProtocol.State.ERROR_RESPONSE);
          break;
        case DATABASE_ID_RESPONSE:
          this.protocol.expect(CatchupClientProtocol.State.DATABASE_ID);
          break;
        default:
          this.log.warn("No handler found for message type %s (%d)", responseMessageType.name(),
              byteValue);
      }

      ReferenceCountUtil.release(msg);
    } else {
      ctx.fireChannelRead(msg);
    }
  }
}
