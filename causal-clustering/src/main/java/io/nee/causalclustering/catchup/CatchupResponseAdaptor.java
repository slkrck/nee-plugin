/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.catchup.storecopy.FileChunk;
import io.nee.causalclustering.catchup.storecopy.FileHeader;
import io.nee.causalclustering.catchup.storecopy.GetStoreIdResponse;
import io.nee.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import io.nee.causalclustering.catchup.storecopy.StoreCopyFinishedResponse;
import io.nee.causalclustering.catchup.tx.TxPullResponse;
import io.nee.causalclustering.catchup.tx.TxStreamFinishedResponse;
import io.nee.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponse;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshot;
import java.util.concurrent.CompletableFuture;

public class CatchupResponseAdaptor<T> implements CatchupResponseCallback<T> {

  public void onGetDatabaseIdResponse(CompletableFuture<T> signal, GetDatabaseIdResponse response) {
    this.unimplementedMethod(signal, response);
  }

  public void onFileHeader(CompletableFuture<T> signal, FileHeader response) {
    this.unimplementedMethod(signal, response);
  }

  public boolean onFileContent(CompletableFuture<T> signal, FileChunk response) {
    this.unimplementedMethod(signal, response);
    return false;
  }

  public void onFileStreamingComplete(CompletableFuture<T> signal,
      StoreCopyFinishedResponse response) {
    this.unimplementedMethod(signal, response);
  }

  public void onTxPullResponse(CompletableFuture<T> signal, TxPullResponse response) {
    this.unimplementedMethod(signal, response);
  }

  public void onTxStreamFinishedResponse(CompletableFuture<T> signal,
      TxStreamFinishedResponse response) {
    this.unimplementedMethod(signal, response);
  }

  public void onGetStoreIdResponse(CompletableFuture<T> signal, GetStoreIdResponse response) {
    this.unimplementedMethod(signal, response);
  }

  public void onCoreSnapshot(CompletableFuture<T> signal, CoreSnapshot response) {
    this.unimplementedMethod(signal, response);
  }

  public void onStoreListingResponse(CompletableFuture<T> signal,
      PrepareStoreCopyResponse response) {
    this.unimplementedMethod(signal, response);
  }

  public void onCatchupErrorResponse(CompletableFuture<T> signal,
      CatchupErrorResponse catchupErrorResponse) {
    signal.completeExceptionally(new RuntimeException(
        String.format("Request returned an error [Status: '%s' Message: '%s']",
            catchupErrorResponse.status(), catchupErrorResponse.message())));
  }

  private <U> void unimplementedMethod(CompletableFuture<T> signal, U response) {
    signal.completeExceptionally(
        new CatchUpProtocolViolationException("This Adaptor has unimplemented methods for: %s",
            response));
  }
}
