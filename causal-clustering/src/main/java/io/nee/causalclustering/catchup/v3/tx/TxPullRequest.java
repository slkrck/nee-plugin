/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.v3.tx;

import io.nee.causalclustering.catchup.RequestMessageType;
import io.nee.causalclustering.messaging.CatchupProtocolMessage;
import java.util.Objects;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.storageengine.api.StoreId;

public class TxPullRequest extends CatchupProtocolMessage.WithDatabaseId {

  private final long previousTxId;
  private final StoreId expectedStoreId;

  public TxPullRequest(long previousTxId, StoreId expectedStoreId, DatabaseId databaseId) {
    super(RequestMessageType.TX_PULL_REQUEST, databaseId);
    if (previousTxId < 1L) {
      throw new IllegalArgumentException("Cannot request transaction from " + previousTxId);
    } else {
      this.previousTxId = previousTxId;
      this.expectedStoreId = expectedStoreId;
    }
  }

  public long previousTxId() {
    return this.previousTxId;
  }

  public StoreId expectedStoreId() {
    return this.expectedStoreId;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      if (!super.equals(o)) {
        return false;
      } else {
        TxPullRequest that = (TxPullRequest) o;
        return this.previousTxId == that.previousTxId && Objects
            .equals(this.expectedStoreId, that.expectedStoreId);
      }
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(super.hashCode(), this.previousTxId, this.expectedStoreId);
  }

  public String toString() {
    long n10000 = this.previousTxId;
    return "TxPullRequest{previousTxId=" + n10000 + ", expectedStoreId=" + this.expectedStoreId
        + ", databaseId='" + this.databaseId() + "'}";
  }
}
