/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.tx;

import java.util.Objects;
import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.storageengine.api.StoreId;

public class TxPullResponse {

  public static final TxPullResponse EMPTY = new TxPullResponse(null, null);
  private final StoreId storeId;
  private final CommittedTransactionRepresentation tx;

  public TxPullResponse(StoreId storeId, CommittedTransactionRepresentation tx) {
    this.storeId = storeId;
    this.tx = tx;
  }

  public StoreId storeId() {
    return this.storeId;
  }

  public CommittedTransactionRepresentation tx() {
    return this.tx;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      TxPullResponse that = (TxPullResponse) o;
      return Objects.equals(this.storeId, that.storeId) && Objects.equals(this.tx, that.tx);
    } else {
      return false;
    }
  }

  public int hashCode() {
    int result = this.storeId != null ? this.storeId.hashCode() : 0;
    result = 31 * result + (this.tx != null ? this.tx.hashCode() : 0);
    return result;
  }

  public String toString() {
    return String.format("TxPullResponse{storeId=%s, tx=%s}", this.storeId, this.tx);
  }
}
