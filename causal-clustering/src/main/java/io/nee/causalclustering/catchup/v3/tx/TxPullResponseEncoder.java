/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.v3.tx;

import io.nee.causalclustering.catchup.tx.TxPullResponse;
import io.nee.causalclustering.helper.ErrorHandler;
import io.nee.causalclustering.messaging.ChunkingNetworkChannel;
import io.nee.causalclustering.messaging.marshalling.storeid.StoreIdMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.util.ReferenceCountUtil;
import java.io.IOException;
import java.util.LinkedList;
import org.neo4j.io.ByteUnit;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryWriter;
import org.neo4j.util.VisibleForTesting;

public class TxPullResponseEncoder extends ChannelOutboundHandlerAdapter {

  private static final int DEFAULT_CHUNK_SIZE = (int) ByteUnit.mebiBytes(1L);
  private static final int PLACEHOLDER_TX_INFO = -1;
  private final LinkedList<ByteBuf> pendingChunks = new LinkedList();
  private final int chunkSize;
  private ChunkingNetworkChannel channel;

  @VisibleForTesting
  TxPullResponseEncoder(int chunkSize) {
    this.chunkSize = chunkSize;
  }

  public TxPullResponseEncoder() {
    this.chunkSize = DEFAULT_CHUNK_SIZE;
  }

  public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise)
      throws Exception {
    if (msg instanceof TxPullResponse) {
      try {
        this.writeResponse(ctx, (TxPullResponse) msg, promise);
      } catch (Exception n10) {
        Exception e = n10;
        if (this.pendingChunks.isEmpty()) {
          throw n10;
        }

        ErrorHandler errorHandler = new ErrorHandler("Release buffers");

        try {
          errorHandler.add(e);
          this.pendingChunks.forEach((byteBuf) ->
          {
            errorHandler.execute(() ->
            {
              ReferenceCountUtil.release(byteBuf);
            });
          });
          this.pendingChunks.clear();
        } catch (Throwable n9) {
          try {
            errorHandler.close();
          } catch (Throwable n8) {
            n9.addSuppressed(n8);
          }

          throw n9;
        }

        errorHandler.close();
      }
    } else {
      super.write(ctx, msg, promise);
    }
  }

  private void writeResponse(ChannelHandlerContext ctx, TxPullResponse msg, ChannelPromise promise)
      throws IOException {
    assert this.pendingChunks.isEmpty();

    if (this.isFirstTx()) {
      this.handleFirstTx(ctx, msg);
    }

    int metadataIndex = this.channel.currentIndex();
    this.channel.putInt(-1);
    if (this.isEndOfTxStream(msg)) {
      this.channel.close();
      this.channel = null;
    } else {
      (new LogEntryWriter(this.channel)).serialize(msg.tx());
    }

    if (!this.pendingChunks.isEmpty()) {
      this.replacePlaceholderSize(metadataIndex);
    }

    if (this.channel != null && this.channel.currentIndex() + 4 > this.chunkSize) {
      this.channel.flush();
    }

    this.writePendingChunks(ctx, promise);
  }

  private void writePendingChunks(ChannelHandlerContext ctx, ChannelPromise promise) {
    if (this.pendingChunks.isEmpty()) {
      promise.setSuccess();
    } else {
      do {
        ByteBuf nextChunk = this.pendingChunks.poll();
        ChannelPromise nextPromise = this.pendingChunks.isEmpty() ? promise : ctx.voidPromise();
        ctx.write(nextChunk, nextPromise);
      }
      while (!this.pendingChunks.isEmpty());
    }
  }

  private void handleFirstTx(ChannelHandlerContext ctx, TxPullResponse msg) throws IOException {
    if (this.isEndOfTxStream(msg)) {
      throw new IllegalArgumentException("Requires at least one transaction.");
    } else {
      this.channel = new ChunkingNetworkChannel(ctx.alloc(), this.chunkSize, this.pendingChunks);
      StoreIdMarshal.INSTANCE.marshal(msg.storeId(), this.channel);
    }
  }

  private void replacePlaceholderSize(int metadataIndex) {
    this.pendingChunks.peek().setInt(metadataIndex, this.calculateTxSize(metadataIndex + 4));
  }

  private int calculateTxSize(int txStartIndex) {
    return (this.channel != null ? this.channel.currentIndex() : 0) + this.pendingChunks.stream()
        .mapToInt(ByteBuf::readableBytes).sum() - txStartIndex;
  }

  private boolean isEndOfTxStream(TxPullResponse msg) {
    return msg.equals(TxPullResponse.EMPTY);
  }

  private boolean isFirstTx() {
    return this.channel == null;
  }
}
