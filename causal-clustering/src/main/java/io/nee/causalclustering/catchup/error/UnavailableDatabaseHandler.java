/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.error;

import io.nee.causalclustering.catchup.CatchupErrorResponse;
import io.nee.causalclustering.catchup.CatchupResult;
import io.nee.causalclustering.catchup.CatchupServerProtocol;
import io.nee.causalclustering.messaging.CatchupProtocolMessage;
import org.neo4j.kernel.availability.AvailabilityGuard;
import org.neo4j.logging.LogProvider;

public class UnavailableDatabaseHandler<T extends CatchupProtocolMessage.WithDatabaseId> extends
    ErrorReportingHandler<T> {

  private final AvailabilityGuard availabilityGuard;

  public UnavailableDatabaseHandler(Class<T> messageType, CatchupServerProtocol protocol,
      AvailabilityGuard availabilityGuard, LogProvider logProvider) {
    super(messageType, protocol, logProvider);
    this.availabilityGuard = availabilityGuard;
  }

  CatchupErrorResponse newErrorResponse(T request) {
    String databaseStatus = this.availabilityGuard.isShutdown() ? "shutdown" : "unavailable";
    return new CatchupErrorResponse(CatchupResult.E_STORE_UNAVAILABLE,
        String.format("CatchupRequest %s refused as intended database %s is %s", request,
            request.databaseId(),
            databaseStatus));
  }
}
