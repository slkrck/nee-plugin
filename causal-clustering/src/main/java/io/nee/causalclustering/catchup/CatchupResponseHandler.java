/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.catchup.storecopy.FileChunk;
import io.nee.causalclustering.catchup.storecopy.FileHeader;
import io.nee.causalclustering.catchup.storecopy.GetStoreIdResponse;
import io.nee.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import io.nee.causalclustering.catchup.storecopy.StoreCopyFinishedResponse;
import io.nee.causalclustering.catchup.tx.TxPullResponse;
import io.nee.causalclustering.catchup.tx.TxStreamFinishedResponse;
import io.nee.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponse;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshot;
import java.io.IOException;

public interface CatchupResponseHandler {

  void onFileHeader(FileHeader n1);

  boolean onFileContent(FileChunk n1) throws IOException;

  void onFileStreamingComplete(StoreCopyFinishedResponse n1);

  void onTxPullResponse(TxPullResponse n1);

  void onTxStreamFinishedResponse(TxStreamFinishedResponse n1);

  void onGetStoreIdResponse(GetStoreIdResponse n1);

  void onGetDatabaseIdResponse(GetDatabaseIdResponse n1);

  void onCoreSnapshot(CoreSnapshot n1);

  void onStoreListingResponse(PrepareStoreCopyResponse n1);

  void onCatchupErrorResponse(CatchupErrorResponse n1);

  void onClose();
}
