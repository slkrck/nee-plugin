/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import io.nee.causalclustering.catchup.CatchupErrorResponse;
import io.nee.causalclustering.catchup.CatchupResponseAdaptor;
import io.nee.causalclustering.catchup.CatchupResult;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import org.neo4j.logging.Log;

public abstract class StoreCopyResponseAdaptors<T> extends CatchupResponseAdaptor<T> {

  private final StoreFileStreamProvider storeFileStreamProvider;
  private final Log log;
  private StoreFileStream storeFileStream;

  private StoreCopyResponseAdaptors(StoreFileStreamProvider storeFileStreamProvider, Log log) {
    this.storeFileStreamProvider = storeFileStreamProvider;
    this.log = log;
  }

  public static StoreCopyResponseAdaptors<StoreCopyFinishedResponse> filesCopyAdaptor(
      StoreFileStreamProvider storeFileStreamProvider, Log log) {
    return new StoreCopyResponseAdaptors.StoreFilesCopyResponseAdaptors(storeFileStreamProvider,
        log);
  }

  public static StoreCopyResponseAdaptors<PrepareStoreCopyResponse> prepareStoreCopyAdaptor(
      StoreFileStreamProvider storeFileStreamProvider, Log log) {
    return new StoreCopyResponseAdaptors.PrepareStoreCopyResponseAdaptors(storeFileStreamProvider,
        log);
  }

  public void onFileHeader(CompletableFuture<T> requestOutcomeSignal, FileHeader fileHeader) {
    try {
      this.log.info("Receiving file: %s", fileHeader.fileName());
      StoreFileStream fileStream = this.storeFileStreamProvider
          .acquire(fileHeader.fileName(), fileHeader.requiredAlignment());
      requestOutcomeSignal.whenComplete(
          new StoreCopyResponseAdaptors.CloseFileStreamOnComplete(fileStream,
              fileHeader.fileName()));
      this.storeFileStream = fileStream;
    } catch (Exception n4) {
      requestOutcomeSignal.completeExceptionally(n4);
    }
  }

  public boolean onFileContent(CompletableFuture<T> signal, FileChunk fileChunk) {
    try {
      this.storeFileStream.write(fileChunk.payload());
    } catch (Exception n4) {
      signal.completeExceptionally(n4);
    }

    return fileChunk.isLast();
  }

  private static class StoreFilesCopyResponseAdaptors extends
      StoreCopyResponseAdaptors<StoreCopyFinishedResponse> {

    StoreFilesCopyResponseAdaptors(StoreFileStreamProvider storeFileStreamProvider, Log log) {
      super(storeFileStreamProvider, log);
    }

    public void onFileStreamingComplete(CompletableFuture<StoreCopyFinishedResponse> signal,
        StoreCopyFinishedResponse response) {
      signal.complete(response);
    }

    public void onCatchupErrorResponse(CompletableFuture<StoreCopyFinishedResponse> signal,
        CatchupErrorResponse catchupErrorResponse) {
      StoreCopyFinishedResponse.Status status =
          catchupErrorResponse.status() == CatchupResult.E_DATABASE_UNKNOWN
              ? StoreCopyFinishedResponse.Status.E_DATABASE_UNKNOWN
              : StoreCopyFinishedResponse.Status.E_UNKNOWN;
      signal.complete(new StoreCopyFinishedResponse(status, -1L));
    }
  }

  private static class PrepareStoreCopyResponseAdaptors extends
      StoreCopyResponseAdaptors<PrepareStoreCopyResponse> {

    PrepareStoreCopyResponseAdaptors(StoreFileStreamProvider storeFileStreamProvider, Log log) {
      super(storeFileStreamProvider, log);
    }

    public void onStoreListingResponse(CompletableFuture<PrepareStoreCopyResponse> signal,
        PrepareStoreCopyResponse response) {
      signal.complete(response);
    }
  }

  private class CloseFileStreamOnComplete<RESPONSE> implements BiConsumer<RESPONSE, Throwable> {

    private final StoreFileStream fileStream;
    private final String fileName;

    private CloseFileStreamOnComplete(StoreFileStream fileStream, String fileName) {
      this.fileStream = fileStream;
      this.fileName = fileName;
    }

    public void accept(RESPONSE response, Throwable throwable) {
      try {
        this.fileStream.close();
      } catch (Exception n4) {
        StoreCopyResponseAdaptors.this.log
            .error(String.format("Unable to close store file stream for file '%s'", this.fileName),
                n4);
      }
    }
  }
}
