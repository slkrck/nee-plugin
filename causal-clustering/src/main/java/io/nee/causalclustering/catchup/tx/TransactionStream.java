/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.tx;

import io.nee.causalclustering.catchup.CatchupResult;
import io.nee.causalclustering.catchup.CatchupServerProtocol;
import io.nee.causalclustering.catchup.ResponseMessageType;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;
import java.util.LinkedList;
import java.util.Queue;
import org.neo4j.cursor.IOCursor;
import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.logging.Log;
import org.neo4j.storageengine.api.StoreId;

public class TransactionStream implements ChunkedInput<Object> {

  private final Log log;
  private final StoreId storeId;
  private final IOCursor<CommittedTransactionRepresentation> txCursor;
  private final CatchupServerProtocol protocol;
  private final long txIdPromise;
  private final Queue<Object> pending = new LinkedList();
  private boolean endOfInput;
  private boolean noMoreTransactions;
  private long expectedTxId;
  private long lastTxId;

  TransactionStream(Log log, TxPullingContext txPullingContext, CatchupServerProtocol protocol) {
    this.log = log;
    this.storeId = txPullingContext.localStoreId();
    this.expectedTxId = txPullingContext.firstTxId();
    this.txIdPromise = txPullingContext.txIdPromise();
    this.txCursor = txPullingContext.transactions();
    this.protocol = protocol;
  }

  public boolean isEndOfInput() {
    return this.endOfInput;
  }

  public void close() throws Exception {
    this.txCursor.close();
  }

  public Object readChunk(ChannelHandlerContext ctx) throws Exception {
    return this.readChunk(ctx.alloc());
  }

  public Object readChunk(ByteBufAllocator allocator) throws Exception {
    assert !this.endOfInput;

    if (!this.pending.isEmpty()) {
      Object prevPending = this.pending.poll();
      if (this.pending.isEmpty() && this.noMoreTransactions) {
        this.endOfInput = true;
      }

      return prevPending;
    } else if (this.txCursor.next()) {
      boolean isFirst = this.lastTxId == 0L;
      CommittedTransactionRepresentation tx = this.txCursor.get();
      this.lastTxId = tx.getCommitEntry().getTxId();
      if (this.lastTxId != this.expectedTxId) {
        String msg = String
            .format("Transaction cursor out of order. Expected %d but was %d", this.expectedTxId,
                this.lastTxId);
        throw new IllegalStateException(msg);
      } else {
        ++this.expectedTxId;
        return this.sendTx(isFirst, tx);
      }
    } else {
      if (this.lastTxId != 0L) {
        this.pending.add(TxPullResponse.EMPTY);
      }

      this.noMoreTransactions = true;
      this.protocol.expect(CatchupServerProtocol.State.MESSAGE_TYPE);
      CatchupResult result;
      if (this.lastTxId >= this.txIdPromise) {
        result = CatchupResult.SUCCESS_END_OF_STREAM;
      } else {
        result = CatchupResult.E_TRANSACTION_PRUNED;
        this.log.warn("Transaction cursor fell short. Expected at least %d but only got to %d.",
            this.txIdPromise, this.lastTxId);
      }

      this.pending.add(ResponseMessageType.TX_STREAM_FINISHED);
      this.pending.add(new TxStreamFinishedResponse(result, this.lastTxId));
      return this.pending.poll();
    }
  }

  private Object sendTx(boolean isFirst, CommittedTransactionRepresentation tx) {
    if (isFirst) {
      this.pending.add(new TxPullResponse(this.storeId, tx));
      return ResponseMessageType.TX;
    } else {
      return new TxPullResponse(this.storeId, tx);
    }
  }

  public long length() {
    return -1L;
  }

  public long progress() {
    return 0L;
  }

  public long lastTxId() {
    return this.lastTxId;
  }
}
