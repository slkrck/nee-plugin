/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.catchup.CatchupAddressResolutionException;
import io.nee.causalclustering.catchup.CatchupResult;
import io.nee.causalclustering.catchup.tx.TransactionLogCatchUpWriter;
import io.nee.causalclustering.catchup.tx.TxPullClient;
import io.nee.causalclustering.core.CausalClusteringSettings;
import java.net.ConnectException;
import java.time.Clock;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.CappedLogger;
import org.neo4j.storageengine.api.StoreId;
import org.neo4j.util.VisibleForTesting;

class TxPuller {

  private final TxPuller.StateBasedAddressProvider addressProvider;
  private final Log log;
  private final ResettableCondition resettableCondition;
  private final CappedLogger connectionErrorLogger;
  private TxPuller.State currentState;
  private long highestTx;

  @VisibleForTesting
  TxPuller(CatchupAddressProvider catchupAddressProvider, LogProvider logProvider,
      ResettableCondition noProgressHandler, Clock clock,
      NamedDatabaseId namedDatabaseId) {
    this.currentState = TxPuller.State.NORMAL;
    this.addressProvider = new TxPuller.StateBasedAddressProvider(namedDatabaseId,
        catchupAddressProvider);
    this.log = logProvider.getLog(this.getClass());
    this.resettableCondition = noProgressHandler;
    this.connectionErrorLogger = new CappedLogger(this.log);
    this.connectionErrorLogger.setTimeLimit(1L, TimeUnit.SECONDS, clock);
  }

  static TxPuller createTxPuller(CatchupAddressProvider catchupAddressProvider,
      LogProvider logProvider, Config config, NamedDatabaseId namedDatabaseId) {
    Clock clock = Clock.systemUTC();
    Duration inactivityTimeout = config
        .get(CausalClusteringSettings.catch_up_client_inactivity_timeout);
    return new TxPuller(catchupAddressProvider, logProvider,
        new ResettableTimeout(inactivityTimeout, clock), clock, namedDatabaseId);
  }

  void pullTransactions(TxPullRequestContext context, TransactionLogCatchUpWriter writer,
      TxPullClient client) throws StoreCopyFailedException {
    StoreId expectedStoreId = context.expectedStoreId();
    long requestedTxId = context.startTxIdExclusive();

    do {
      TxPuller.Result result = this.execute(client, expectedStoreId, writer, requestedTxId);
      this.updateState(result, writer, context);
      requestedTxId = Long.max(requestedTxId, this.fallbackOnLastAttempt(writer, context));
    }
    while (this.shouldContinue());

    this.execute(client, expectedStoreId, writer, requestedTxId);
  }

  private long fallbackOnLastAttempt(TransactionLogCatchUpWriter writer,
      TxPullRequestContext context) {
    long currentTxId = writer.lastTx();
    return this.currentState == TxPuller.State.LAST_ATTEMPT && currentTxId == -1L ? context
        .fallbackStartId().orElse(currentTxId) : currentTxId;
  }

  private TxPuller.Result execute(TxPullClient txPullClient, StoreId expectedStoreId,
      TransactionLogCatchUpWriter writer, long fromTxId) {
    try {
      SocketAddress fromAddress = this.addressProvider.get(this.currentState);

      try {
        this.log.info("Pulling transactions from %s starting with txId: %d", fromAddress, fromTxId);
        CatchupResult status = txPullClient
            .pullTransactions(fromAddress, expectedStoreId, fromTxId, writer).status();
        return this.setResult(status);
      } catch (ConnectException n8) {
        this.connectionErrorLogger
            .info("Unable to connect. [Address: %s] [Message: %s]", fromAddress, n8.getMessage());
        return TxPuller.Result.TRANSIENT_ERROR;
      } catch (Exception n9) {
        this.log.warn(String
                .format("Unexpected exception when pulling transactions. [Address: %s]", fromAddress),
            n9);
        return TxPuller.Result.ERROR;
      }
    } catch (CatchupAddressResolutionException n10) {
      this.log.info("Unable to find a suitable address to pull transactions from [Message: %s]",
          n10.getMessage());
      return TxPuller.Result.TRANSIENT_ERROR;
    }
  }

  private TxPuller.Result setResult(CatchupResult status) {
    if (status == CatchupResult.SUCCESS_END_OF_STREAM) {
      return TxPuller.Result.SUCCESS;
    } else {
      this.log.info("Transaction pulling attempt failed with error: %s", status);
      return TxPuller.Result.ERROR;
    }
  }

  private boolean shouldContinue() {
    return this.currentState != TxPuller.State.COMPLETE;
  }

  private void updateState(TxPuller.Result result, TransactionLogCatchUpWriter writer,
      TxPullRequestContext context) throws StoreCopyFailedException {
    long currentHighest = Long.max(this.highestTx, writer.lastTx());

    try {
      boolean completed =
          context.constraintReached(currentHighest) && result == TxPuller.Result.SUCCESS;
      if (!completed) {
        this.currentState = this.checkProgress(currentHighest, result);
        return;
      }

      this.currentState = TxPuller.State.COMPLETE;
    } finally {
      this.highestTx = currentHighest;
    }
  }

  private TxPuller.State checkProgress(long lastWrittenTx, TxPuller.Result result)
      throws StoreCopyFailedException {
    if (this.hasProgressed(lastWrittenTx)) {
      this.resettableCondition.reset();
      return TxPuller.State.NORMAL;
    } else if (this.currentState == TxPuller.State.LAST_ATTEMPT) {
      this.log.warn("Failed to pull transactions");
      throw new StoreCopyFailedException("Pulling tx failed consecutively without progress");
    } else {
      return this.resettableCondition.canContinue() && result != TxPuller.Result.ERROR
          ? TxPuller.State.NORMAL : TxPuller.State.LAST_ATTEMPT;
    }
  }

  private boolean hasProgressed(long lastWrittenTx) {
    return lastWrittenTx > this.highestTx;
  }

  private enum State {
    COMPLETE,
    LAST_ATTEMPT,
    NORMAL
  }

  private enum Result {
    SUCCESS,
    TRANSIENT_ERROR,
    ERROR
  }

  private static class StateBasedAddressProvider {

    private final NamedDatabaseId namedDatabaseId;
    private final CatchupAddressProvider catchupAddressProvider;

    private StateBasedAddressProvider(NamedDatabaseId namedDatabaseId,
        CatchupAddressProvider catchupAddressProvider) {
      this.namedDatabaseId = namedDatabaseId;
      this.catchupAddressProvider = catchupAddressProvider;
    }

    public SocketAddress get(TxPuller.State state) throws CatchupAddressResolutionException {
      switch (state) {
        case LAST_ATTEMPT:
        case COMPLETE:
          return this.catchupAddressProvider.primary(this.namedDatabaseId);
        default:
          return this.catchupAddressProvider.secondary(this.namedDatabaseId);
      }
    }
  }
}
