/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.v3;

import io.nee.causalclustering.catchup.CatchupClientProtocol;
import io.nee.causalclustering.catchup.CatchupResponseHandler;
import io.nee.causalclustering.catchup.ClientMessageTypeHandler;
import io.nee.causalclustering.catchup.RequestDecoderDispatcher;
import io.nee.causalclustering.catchup.RequestMessageTypeEncoder;
import io.nee.causalclustering.catchup.ResponseMessageTypeEncoder;
import io.nee.causalclustering.catchup.StoreListingResponseHandler;
import io.nee.causalclustering.catchup.storecopy.FileChunkDecoder;
import io.nee.causalclustering.catchup.storecopy.FileChunkHandler;
import io.nee.causalclustering.catchup.storecopy.FileHeaderDecoder;
import io.nee.causalclustering.catchup.storecopy.FileHeaderHandler;
import io.nee.causalclustering.catchup.storecopy.GetStoreIdResponseDecoder;
import io.nee.causalclustering.catchup.storecopy.GetStoreIdResponseHandler;
import io.nee.causalclustering.catchup.storecopy.PrepareStoreCopyResponse;
import io.nee.causalclustering.catchup.storecopy.StoreCopyFinishedResponseHandler;
import io.nee.causalclustering.catchup.tx.TxStreamFinishedResponseDecoder;
import io.nee.causalclustering.catchup.tx.TxStreamFinishedResponseHandler;
import io.nee.causalclustering.catchup.v3.databaseid.GetDatabaseIdRequestEncoder;
import io.nee.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponseDecoder;
import io.nee.causalclustering.catchup.v3.databaseid.GetDatabaseIdResponseHandler;
import io.nee.causalclustering.catchup.v3.storecopy.CatchupErrorResponseDecoder;
import io.nee.causalclustering.catchup.v3.storecopy.CatchupErrorResponseHandler;
import io.nee.causalclustering.catchup.v3.storecopy.CoreSnapshotRequestEncoder;
import io.nee.causalclustering.catchup.v3.storecopy.GetStoreFileRequestEncoder;
import io.nee.causalclustering.catchup.v3.storecopy.GetStoreIdRequestEncoder;
import io.nee.causalclustering.catchup.v3.storecopy.PrepareStoreCopyRequestEncoder;
import io.nee.causalclustering.catchup.v3.storecopy.StoreCopyFinishedResponseDecoder;
import io.nee.causalclustering.catchup.v3.tx.TxPullRequestEncoder;
import io.nee.causalclustering.catchup.v3.tx.TxPullResponseDecoder;
import io.nee.causalclustering.catchup.v3.tx.TxPullResponseHandler;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshotDecoder;
import io.nee.causalclustering.core.state.snapshot.CoreSnapshotResponseHandler;
import io.nee.causalclustering.protocol.ModifierProtocolInstaller;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.ProtocolInstaller;
import io.nee.causalclustering.protocol.application.ApplicationProtocol;
import io.nee.causalclustering.protocol.application.ApplicationProtocols;
import io.nee.causalclustering.protocol.modifier.ModifierProtocol;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class CatchupProtocolClientInstaller implements
    ProtocolInstaller<ProtocolInstaller.Orientation.Client> {

  private static final ApplicationProtocols APPLICATION_PROTOCOL;

  static {
    APPLICATION_PROTOCOL = ApplicationProtocols.CATCHUP_3_0;
  }

  private final List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Client>> modifiers;
  private final LogProvider logProvider;
  private final Log log;
  private final NettyPipelineBuilderFactory pipelineBuilder;
  private final CatchupResponseHandler handler;

  public CatchupProtocolClientInstaller(NettyPipelineBuilderFactory pipelineBuilder,
      List<ModifierProtocolInstaller<ProtocolInstaller.Orientation.Client>> modifiers,
      LogProvider logProvider,
      CatchupResponseHandler handler) {
    this.modifiers = modifiers;
    this.logProvider = logProvider;
    this.log = logProvider.getLog(this.getClass());
    this.pipelineBuilder = pipelineBuilder;
    this.handler = handler;
  }

  public void install(Channel channel) {
    CatchupClientProtocol protocol = new CatchupClientProtocol();
    RequestDecoderDispatcher<CatchupClientProtocol.State> decoderDispatcher = new RequestDecoderDispatcher(
        protocol, this.logProvider);
    decoderDispatcher
        .register(CatchupClientProtocol.State.STORE_ID, new GetStoreIdResponseDecoder());
    decoderDispatcher
        .register(CatchupClientProtocol.State.DATABASE_ID, new GetDatabaseIdResponseDecoder());
    decoderDispatcher
        .register(CatchupClientProtocol.State.TX_PULL_RESPONSE, new TxPullResponseDecoder());
    decoderDispatcher
        .register(CatchupClientProtocol.State.CORE_SNAPSHOT, new CoreSnapshotDecoder());
    decoderDispatcher.register(CatchupClientProtocol.State.STORE_COPY_FINISHED,
        new StoreCopyFinishedResponseDecoder());
    decoderDispatcher.register(CatchupClientProtocol.State.TX_STREAM_FINISHED,
        new TxStreamFinishedResponseDecoder());
    decoderDispatcher.register(CatchupClientProtocol.State.FILE_HEADER, new FileHeaderDecoder());
    decoderDispatcher.register(CatchupClientProtocol.State.PREPARE_STORE_COPY_RESPONSE,
        new PrepareStoreCopyResponse.Decoder());
    decoderDispatcher.register(CatchupClientProtocol.State.FILE_CHUNK, new FileChunkDecoder());
    decoderDispatcher
        .register(CatchupClientProtocol.State.ERROR_RESPONSE, new CatchupErrorResponseDecoder());
    this.pipelineBuilder
        .client(
            channel, this.log).modify(this.modifiers).addFraming()
        .add("enc_req_tx", new ChannelHandler[]{new TxPullRequestEncoder()}).add(
        "enc_req_store", new ChannelHandler[]{new GetStoreFileRequestEncoder()})
        .add("enc_req_snapshot",
            new ChannelHandler[]{new CoreSnapshotRequestEncoder()})
        .add("enc_req_store_id", new ChannelHandler[]{new GetStoreIdRequestEncoder()}).add(
        "enc_req_database_id", new ChannelHandler[]{new GetDatabaseIdRequestEncoder()})
        .add("enc_req_type",
            new ChannelHandler[]{new ResponseMessageTypeEncoder()})
        .add("enc_res_type", new ChannelHandler[]{new RequestMessageTypeEncoder()}).add(
        "enc_req_precopy", new ChannelHandler[]{new PrepareStoreCopyRequestEncoder()})
        .add("in_res_type",
            new ChannelHandler[]{
                new ClientMessageTypeHandler(protocol,
                    this.logProvider)})
        .add("dec_dispatch",
            new ChannelHandler[]{decoderDispatcher})
        .add("hnd_res_tx", new ChannelHandler[]{new TxPullResponseHandler(protocol, this.handler)})
        .add(
            "hnd_res_snapshot",
            new ChannelHandler[]{new CoreSnapshotResponseHandler(protocol, this.handler)})
        .add("hnd_res_copy_fin",
            new ChannelHandler[]{
                new StoreCopyFinishedResponseHandler(
                    protocol,
                    this.handler)})
        .add("hnd_res_tx_fin",
            new ChannelHandler[]{new TxStreamFinishedResponseHandler(protocol, this.handler)})
        .add("hnd_res_file_header",
            new ChannelHandler[]{
                new FileHeaderHandler(protocol,
                    this.handler)})
        .add("hnd_res_file_chunk",
            new ChannelHandler[]{new FileChunkHandler(protocol, this.handler)})
        .add("hnd_res_store_id",
            new ChannelHandler[]{new GetStoreIdResponseHandler(protocol,
                this.handler)})
        .add("hnd_res_database_id",
            new ChannelHandler[]{new GetDatabaseIdResponseHandler(protocol, this.handler)})
        .add("hnd_res_store_listing",
            new ChannelHandler[]{
                new StoreListingResponseHandler(
                    protocol, this.handler)})
        .add("hnd_res_catchup_error",
            new ChannelHandler[]{new CatchupErrorResponseHandler(protocol, this.handler)})
        .install();
  }

  public ApplicationProtocol applicationProtocol() {
    return APPLICATION_PROTOCOL;
  }

  public Collection<Collection<ModifierProtocol>> modifiers() {
    return this.modifiers.stream().map(ModifierProtocolInstaller::protocols)
        .collect(Collectors.toList());
  }

  public static class Factory extends
      ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Client, CatchupProtocolClientInstaller> {

    public Factory(NettyPipelineBuilderFactory pipelineBuilder, LogProvider logProvider,
        CatchupResponseHandler handler) {
      super(CatchupProtocolClientInstaller.APPLICATION_PROTOCOL, (modifiers) ->
      {
        return new CatchupProtocolClientInstaller(pipelineBuilder, modifiers, logProvider, handler);
      });
    }
  }
}
