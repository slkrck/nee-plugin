/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import io.nee.causalclustering.catchup.ResponseMessageType;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.concurrent.Future;

public class StoreFileStreamingProtocol {

  private final int maxChunkSize;

  public StoreFileStreamingProtocol(int maxChunkSize) {
    this.maxChunkSize = maxChunkSize;
  }

  void stream(ChannelHandlerContext ctx, StoreResource resource) {
    ctx.write(ResponseMessageType.FILE);
    ctx.write(new FileHeader(resource.path(), resource.recordSize()));
    ctx.write(new FileSender(resource, this.maxChunkSize));
  }

  Future<Void> end(ChannelHandlerContext ctx, StoreCopyFinishedResponse.Status status,
      long lastCheckpointedTx) {
    ctx.write(ResponseMessageType.STORE_COPY_FINISHED);
    return ctx.writeAndFlush(new StoreCopyFinishedResponse(status, lastCheckpointedTx));
  }
}
