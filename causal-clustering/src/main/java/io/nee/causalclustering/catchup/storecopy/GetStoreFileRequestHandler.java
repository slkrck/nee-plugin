/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import io.nee.causalclustering.catchup.CatchupServerProtocol;
import io.nee.causalclustering.catchup.v3.storecopy.GetStoreFileRequest;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.internal.helpers.collection.Iterators;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.FileUtils;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.impl.transaction.log.checkpoint.CheckPointer;
import org.neo4j.kernel.impl.transaction.log.checkpoint.SimpleTriggerInfo;
import org.neo4j.logging.Log;
import org.neo4j.scheduler.Group;
import org.neo4j.storageengine.api.StoreFileMetadata;
import org.neo4j.util.VisibleForTesting;

public class GetStoreFileRequestHandler extends SimpleChannelInboundHandler<GetStoreFileRequest> {

  private final CatchupServerProtocol protocol;
  private final Database db;
  private final StoreFileStreamingProtocol storeFileStreamingProtocol;
  private final FileSystemAbstraction fs;
  private final Log log;

  public GetStoreFileRequestHandler(CatchupServerProtocol protocol, Database db,
      StoreFileStreamingProtocol storeFileStreamingProtocol,
      FileSystemAbstraction fs) {
    this.protocol = protocol;
    this.db = db;
    this.storeFileStreamingProtocol = storeFileStreamingProtocol;
    this.fs = fs;
    this.log = db.getInternalLogProvider().getLog(this.getClass());
  }

  private static Iterator<StoreFileMetadata> onlyOne(List<StoreFileMetadata> files,
      String description) {
    if (files.size() != 1) {
      throw new IllegalStateException(
          String.format("Expected exactly one file '%s'. Got %d", description, files.size()));
    } else {
      return files.iterator();
    }
  }

  private static Predicate<StoreFileMetadata> matchesRequested(String fileName) {
    return (f) ->
    {
      return f.file().getName().equals(fileName);
    };
  }

  protected final void channelRead0(ChannelHandlerContext ctx, GetStoreFileRequest request)
      throws Exception {
    this.log.debug("Handling request %s", request);
    StoreCopyFinishedResponse.Status responseStatus = StoreCopyFinishedResponse.Status.E_UNKNOWN;
    long lastCheckpointedTx = -1L;

    try {
      CheckPointer checkPointer = this.db.getDependencyResolver()
          .resolveDependency(CheckPointer.class);
      if (!Objects.equals(request.expectedStoreId(), this.db.getStoreId())) {
        responseStatus = StoreCopyFinishedResponse.Status.E_STORE_ID_MISMATCH;
      } else if (checkPointer.lastCheckPointedTransactionId() < request.requiredTransactionId()) {
        responseStatus = StoreCopyFinishedResponse.Status.E_TOO_FAR_BEHIND;
        this.tryAsyncCheckpoint(this.db, checkPointer);
      } else {
        File databaseDirectory = this.db.getDatabaseLayout().databaseDirectory();
        ResourceIterator resourceIterator = this.files(request, this.db);

        try {
          while (resourceIterator.hasNext()) {
            StoreFileMetadata storeFileMetadata = (StoreFileMetadata) resourceIterator.next();
            StoreResource storeResource =
                new StoreResource(storeFileMetadata.file(),
                    FileUtils.relativePath(databaseDirectory, storeFileMetadata.file()),
                    storeFileMetadata.recordSize(), this.fs);
            this.storeFileStreamingProtocol.stream(ctx, storeResource);
          }
        } catch (Throwable n16) {
          if (resourceIterator != null) {
            try {
              resourceIterator.close();
            } catch (Throwable n15) {
              n16.addSuppressed(n15);
            }
          }

          throw n16;
        }

        if (resourceIterator != null) {
          resourceIterator.close();
        }

        lastCheckpointedTx = checkPointer.lastCheckPointedTransactionId();
        responseStatus = StoreCopyFinishedResponse.Status.SUCCESS;
      }
    } finally {
      this.storeFileStreamingProtocol.end(ctx, responseStatus, lastCheckpointedTx);
      this.protocol.expect(CatchupServerProtocol.State.MESSAGE_TYPE);
    }
  }

  @VisibleForTesting
  ResourceIterator<StoreFileMetadata> files(GetStoreFileRequest request, Database database)
      throws IOException {
    ResourceIterator resourceIterator = database.listStoreFiles(false);

    ResourceIterator n5;
    try {
      String fileName = request.file().getName();
      n5 = Iterators.asResourceIterator(
          onlyOne((List) resourceIterator.stream().filter(matchesRequested(fileName))
              .collect(Collectors.toList()), fileName));
    } catch (Throwable n7) {
      if (resourceIterator != null) {
        try {
          resourceIterator.close();
        } catch (Throwable n6) {
          n7.addSuppressed(n6);
        }
      }

      throw n7;
    }

    if (resourceIterator != null) {
      resourceIterator.close();
    }

    return n5;
  }

  private void tryAsyncCheckpoint(Database db, CheckPointer checkPointer) {
    db.getScheduler().schedule(Group.CHECKPOINT, () ->
    {
      try {
        checkPointer.tryCheckPointNoWait(new SimpleTriggerInfo("Store file copy"));
      } catch (IOException n3) {
        this.log.error(
            "Failed to do a checkpoint that was invoked after a too far behind error on store copy request",
            n3);
      }
    });
  }
}
