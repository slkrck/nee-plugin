/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.v3.tx;

import io.nee.causalclustering.catchup.tx.TxPullResponse;
import io.nee.causalclustering.messaging.NetworkReadableChannel;
import io.nee.causalclustering.messaging.ReadableNetworkChannelDelegator;
import io.nee.causalclustering.messaging.marshalling.storeid.StoreIdMarshal;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import java.util.List;
import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.log.PhysicalTransactionCursor;
import org.neo4j.kernel.impl.transaction.log.ServiceLoadingCommandReaderFactory;
import org.neo4j.kernel.impl.transaction.log.entry.LogEntryReader;
import org.neo4j.kernel.impl.transaction.log.entry.VersionAwareLogEntryReader;
import org.neo4j.storageengine.api.StoreId;

public class TxPullResponseDecoder extends ByteToMessageDecoder {

  private static final ServiceLoadingCommandReaderFactory commandReaderFactory = new ServiceLoadingCommandReaderFactory();
  private final LogEntryReader reader;
  private final ReadableNetworkChannelDelegator delegatingChannel;
  private PhysicalTransactionCursor transactionCursor;
  private TxPullResponseDecoder.NextTxInfo nextTxInfo;
  private StoreId storeId;

  public TxPullResponseDecoder() {
    this.reader = new VersionAwareLogEntryReader(commandReaderFactory);
    this.delegatingChannel = new ReadableNetworkChannelDelegator();
    this.setCumulator(COMPOSITE_CUMULATOR);
  }

  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
    this.delegatingChannel.delegateTo(new NetworkReadableChannel(in));
    if (this.isFirstChunk()) {
      this.storeId = StoreIdMarshal.INSTANCE.unmarshal(this.delegatingChannel);
      this.transactionCursor = new PhysicalTransactionCursor(this.delegatingChannel, this.reader);
      this.nextTxInfo = new TxPullResponseDecoder.NextTxInfo();
    }

    while (this.nextTxInfo.canReadNextTx(in)) {
      this.transactionCursor.next();
      CommittedTransactionRepresentation tx = this.transactionCursor.get();
      out.add(new TxPullResponse(this.storeId, tx));
      this.nextTxInfo.update(in);
    }

    if (this.nextTxInfo.noMoreTx()) {
      this.transactionCursor.close();
      this.transactionCursor = null;
      this.nextTxInfo = null;
      out.add(TxPullResponse.EMPTY);
    }
  }

  private boolean isFirstChunk() {
    return this.transactionCursor == null;
  }

  private static class NextTxInfo {

    private boolean unknown = true;
    private int nextSize;

    boolean canReadNextTx(ByteBuf byteBuf) {
      if (this.unknown) {
        this.update(byteBuf);
      }

      return !this.unknown && this.nextSize < byteBuf.readableBytes();
    }

    void update(ByteBuf byteBuf) {
      this.unknown = !byteBuf.isReadable();
      this.nextSize = this.unknown ? 0 : byteBuf.readInt();
    }

    boolean noMoreTx() {
      return !this.unknown && this.nextSize == 0;
    }
  }
}
