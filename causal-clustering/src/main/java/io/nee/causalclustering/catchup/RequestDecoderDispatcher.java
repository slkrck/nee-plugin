/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandler;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import java.util.HashMap;
import java.util.Map;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class RequestDecoderDispatcher<E extends Enum<E>> extends ChannelInboundHandlerAdapter {

  private final Map<E, ChannelInboundHandler> decoders = new HashMap();
  private final Protocol<E> protocol;
  private final Log log;

  public RequestDecoderDispatcher(Protocol<E> protocol, LogProvider logProvider) {
    this.protocol = protocol;
    this.log = logProvider.getLog(this.getClass());
  }

  public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    ChannelInboundHandler delegate = this.protocol.select(this.decoders);
    if (delegate == null) {
      this.log.warn("Unregistered handler for protocol %s", this.protocol);
      ReferenceCountUtil.release(msg);
    } else {
      delegate.channelRead(ctx, msg);
    }
  }

  public void register(E type, ChannelInboundHandler decoder) {
    assert !this.decoders.containsKey(type) :
        "registering twice a decoder for the same type (" + type + ")?";

    this.decoders.put(type, decoder);
  }
}
