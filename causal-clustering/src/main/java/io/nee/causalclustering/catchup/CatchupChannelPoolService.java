/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.net.BootstrapConfiguration;
import io.nee.causalclustering.net.ChannelPoolService;
import io.nee.causalclustering.protocol.init.ClientChannelInitializer;
import io.netty.channel.Channel;
import io.netty.channel.pool.AbstractChannelPoolHandler;
import io.netty.channel.pool.SimpleChannelPool;
import io.netty.channel.socket.SocketChannel;
import io.netty.util.AttributeKey;
import java.time.Clock;
import java.util.function.Function;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

class CatchupChannelPoolService extends ChannelPoolService {

  static final AttributeKey<TrackingResponseHandler> TRACKING_RESPONSE_HANDLER = AttributeKey
      .valueOf("TRACKING_RESPONSE_HANDLER");

  CatchupChannelPoolService(BootstrapConfiguration<? extends SocketChannel> bootstrapConfiguration,
      JobScheduler jobScheduler, Clock clock,
      Function<CatchupResponseHandler, ClientChannelInitializer> initializerFactory) {
    super(bootstrapConfiguration, jobScheduler, Group.CATCHUP_CLIENT,
        new CatchupChannelPoolService.TrackingResponsePoolHandler(initializerFactory, clock),
        SimpleChannelPool::new);
  }

  private static class TrackingResponsePoolHandler extends AbstractChannelPoolHandler {

    private final Function<CatchupResponseHandler, ClientChannelInitializer> initializerFactory;
    private final Clock clock;

    TrackingResponsePoolHandler(
        Function<CatchupResponseHandler, ClientChannelInitializer> initializerFactory,
        Clock clock) {
      this.initializerFactory = initializerFactory;
      this.clock = clock;
    }

    public void channelReleased(Channel ch) {
      ch.attr(CatchupChannelPoolService.TRACKING_RESPONSE_HANDLER).get().clearResponseHandler();
    }

    public void channelCreated(Channel ch) {
      TrackingResponseHandler trackingResponseHandler = new TrackingResponseHandler(this.clock);
      ch.pipeline().addLast(this.initializerFactory.apply(trackingResponseHandler));
      ch.attr(CatchupChannelPoolService.TRACKING_RESPONSE_HANDLER).set(trackingResponseHandler);
      ch.closeFuture().addListener((f) ->
      {
        trackingResponseHandler.onClose();
      });
    }
  }
}
