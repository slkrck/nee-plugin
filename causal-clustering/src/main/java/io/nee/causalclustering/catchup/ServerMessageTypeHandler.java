/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class ServerMessageTypeHandler extends ChannelInboundHandlerAdapter {

  private final Log log;
  private final CatchupServerProtocol protocol;

  public ServerMessageTypeHandler(CatchupServerProtocol protocol, LogProvider logProvider) {
    this.protocol = protocol;
    this.log = logProvider.getLog(this.getClass());
  }

  public void channelRead(ChannelHandlerContext ctx, Object msg) {
    if (this.protocol.isExpecting(CatchupServerProtocol.State.MESSAGE_TYPE)) {
      RequestMessageType requestMessageType = RequestMessageType.from(((ByteBuf) msg).readByte());
      if (requestMessageType.equals(RequestMessageType.TX_PULL_REQUEST)) {
        this.protocol.expect(CatchupServerProtocol.State.TX_PULL);
      } else if (requestMessageType.equals(RequestMessageType.STORE_ID)) {
        this.protocol.expect(CatchupServerProtocol.State.GET_STORE_ID);
      } else if (requestMessageType.equals(RequestMessageType.DATABASE_ID)) {
        this.protocol.expect(CatchupServerProtocol.State.GET_DATABASE_ID);
      } else if (requestMessageType.equals(RequestMessageType.CORE_SNAPSHOT)) {
        this.protocol.expect(CatchupServerProtocol.State.GET_CORE_SNAPSHOT);
      } else if (requestMessageType.equals(RequestMessageType.PREPARE_STORE_COPY)) {
        this.protocol.expect(CatchupServerProtocol.State.PREPARE_STORE_COPY);
      } else if (requestMessageType.equals(RequestMessageType.STORE_FILE)) {
        this.protocol.expect(CatchupServerProtocol.State.GET_STORE_FILE);
      } else {
        this.log.warn("No handler found for message type %s", requestMessageType);
      }

      ReferenceCountUtil.release(msg);
    } else {
      ctx.fireChannelRead(msg);
    }
  }
}
