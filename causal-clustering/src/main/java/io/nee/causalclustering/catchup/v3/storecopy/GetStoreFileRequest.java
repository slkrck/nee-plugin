/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.v3.storecopy;

import io.nee.causalclustering.catchup.RequestMessageType;
import io.nee.causalclustering.messaging.StoreCopyRequest;
import java.io.File;
import java.util.Objects;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.storageengine.api.StoreId;

public class GetStoreFileRequest extends StoreCopyRequest {

  private final File file;

  public GetStoreFileRequest(StoreId expectedStoreId, File file, long requiredTransactionId,
      DatabaseId databaseId) {
    super(RequestMessageType.STORE_FILE, databaseId, expectedStoreId, requiredTransactionId);
    this.file = file;
  }

  public File file() {
    return this.file;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      if (!super.equals(o)) {
        return false;
      } else {
        GetStoreFileRequest that = (GetStoreFileRequest) o;
        return Objects.equals(this.file, that.file);
      }
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(super.hashCode(), this.file);
  }

  public String toString() {
    StoreId n10000 = this.expectedStoreId();
    return "GetStoreFileRequest{expectedStoreId=" + n10000 + ", file=" + this.file.getName()
        + ", requiredTransactionId=" + this.requiredTransactionId() +
        ", databaseId=" + this.databaseId() + "}";
  }
}
