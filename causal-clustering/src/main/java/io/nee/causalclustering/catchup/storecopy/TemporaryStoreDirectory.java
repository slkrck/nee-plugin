/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import java.io.File;
import java.io.IOException;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.layout.Neo4jLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.kernel.impl.transaction.log.files.LogFilesBuilder;

public class TemporaryStoreDirectory implements AutoCloseable {

  private final File tempHomeDir;
  private final DatabaseLayout tempDatabaseLayout;
  private final FileSystemAbstraction fs;
  private final StoreFiles storeFiles;
  private final LogFiles tempLogFiles;
  private boolean keepStore;

  TemporaryStoreDirectory(FileSystemAbstraction fs, PageCache pageCache,
      DatabaseLayout databaseLayout) throws IOException {
    this.tempHomeDir = databaseLayout.file("temp-copy");
    this.tempDatabaseLayout = Neo4jLayout.ofFlat(this.tempHomeDir)
        .databaseLayout(databaseLayout.getDatabaseName());
    this.fs = fs;
    this.storeFiles = new StoreFiles(fs, pageCache, (directory, name) ->
    {
      return true;
    });
    this.tempLogFiles = LogFilesBuilder
        .logFilesBasedOnlyBuilder(this.tempDatabaseLayout.getTransactionLogsDirectory(), fs)
        .build();
    this.storeFiles.delete(this.tempDatabaseLayout, this.tempLogFiles);
  }

  public DatabaseLayout databaseLayout() {
    return this.tempDatabaseLayout;
  }

  void keepStore() {
    this.keepStore = true;
  }

  public void close() throws IOException {
    if (!this.keepStore) {
      this.storeFiles.delete(this.tempDatabaseLayout, this.tempLogFiles);
      this.fs.deleteFile(this.tempHomeDir);
    }
  }
}
