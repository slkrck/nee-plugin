/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.messaging.Message;

public enum RequestMessageType implements Message {
  TX_PULL_REQUEST((byte) 1),
  STORE((byte) 2),
  CORE_SNAPSHOT((byte) 3),
  STORE_ID((byte) 4),
  PREPARE_STORE_COPY((byte) 5),
  STORE_FILE((byte) 6),
  DATABASE_ID((byte) 7),
  UNKNOWN((byte) -108);

  private final byte messageType;

  RequestMessageType(byte messageType) {
    this.messageType = messageType;
  }

  public static RequestMessageType from(byte b) {
    RequestMessageType[] n1 = values();
    int n2 = n1.length;

    for (int n3 = 0; n3 < n2; ++n3) {
      RequestMessageType responseMessageType = n1[n3];
      if (responseMessageType.messageType == b) {
        return responseMessageType;
      }
    }

    return UNKNOWN;
  }

  public byte messageType() {
    return this.messageType;
  }

  public String toString() {
    return String.format("RequestMessageType{messageType=%s}", this.messageType);
  }
}
