/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.StoreChannel;

class StoreResource {

  private final File file;
  private final String path;
  private final int recordSize;
  private final FileSystemAbstraction fs;

  StoreResource(File file, String relativePath, int recordSize, FileSystemAbstraction fs) {
    this.file = file;
    this.path = relativePath;
    this.recordSize = recordSize;
    this.fs = fs;
  }

  StoreChannel open() throws IOException {
    return this.fs.read(this.file);
  }

  public String path() {
    return this.path;
  }

  File file() {
    return this.file;
  }

  int recordSize() {
    return this.recordSize;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      StoreResource that = (StoreResource) o;
      return this.recordSize == that.recordSize && Objects.equals(this.file, that.file) && Objects
          .equals(this.path, that.path);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.file, this.path, this.recordSize);
  }

  public String toString() {
    return "StoreResource{path='" + this.path + "', recordSize=" + this.recordSize + "}";
  }
}
