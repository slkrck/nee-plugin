/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import io.nee.causalclustering.catchup.CatchupServerProtocol;
import io.nee.causalclustering.catchup.ResponseMessageType;
import io.nee.causalclustering.catchup.v3.storecopy.PrepareStoreCopyRequest;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.io.File;
import java.io.IOException;
import java.util.Objects;
import org.neo4j.graphdb.Resource;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.impl.transaction.log.checkpoint.CheckPointer;
import org.neo4j.kernel.impl.transaction.log.checkpoint.SimpleTriggerInfo;
import org.neo4j.logging.Log;

public class PrepareStoreCopyRequestHandler extends
    SimpleChannelInboundHandler<PrepareStoreCopyRequest> {

  private final CatchupServerProtocol protocol;
  private final PrepareStoreCopyFilesProvider prepareStoreCopyFilesProvider;
  private final Database db;
  private final StoreFileStreamingProtocol streamingProtocol;
  private final Log log;

  public PrepareStoreCopyRequestHandler(CatchupServerProtocol catchupServerProtocol, Database db,
      PrepareStoreCopyFilesProvider prepareStoreCopyFilesProvider, int maxChunkSize) {
    this.protocol = catchupServerProtocol;
    this.prepareStoreCopyFilesProvider = prepareStoreCopyFilesProvider;
    this.db = db;
    this.streamingProtocol = new StoreFileStreamingProtocol(maxChunkSize);
    this.log = db.getInternalLogProvider().getLog(this.getClass());
  }

  protected void channelRead0(ChannelHandlerContext channelHandlerContext,
      PrepareStoreCopyRequest prepareStoreCopyRequest) throws IOException {
    CloseablesListener closeablesListener = new CloseablesListener();
    PrepareStoreCopyResponse response = PrepareStoreCopyResponse
        .error(PrepareStoreCopyResponse.Status.E_LISTING_STORE);

    try {
      if (!this.canPrepareForStoreCopy(this.db)) {
        return;
      }

      if (!Objects.equals(prepareStoreCopyRequest.storeId(), this.db.getStoreId())) {
        response = PrepareStoreCopyResponse
            .error(PrepareStoreCopyResponse.Status.E_STORE_ID_MISMATCH);
      } else {
        CheckPointer checkPointer = this.db.getDependencyResolver()
            .resolveDependency(CheckPointer.class);
        closeablesListener.add(this.tryCheckpointAndAcquireMutex(checkPointer));
        PrepareStoreCopyFiles prepareStoreCopyFiles =
            closeablesListener
                .add(this.prepareStoreCopyFilesProvider.prepareStoreCopyFiles(this.db));
        StoreResource[] nonReplayable = prepareStoreCopyFiles.getAtomicFilesSnapshot();
        StoreResource[] n8 = nonReplayable;
        int n9 = nonReplayable.length;

        for (int n10 = 0; n10 < n9; ++n10) {
          StoreResource storeResource = n8[n10];
          this.streamingProtocol.stream(channelHandlerContext, storeResource);
        }

        response = this.createSuccessfulResponse(checkPointer, prepareStoreCopyFiles);
      }
    } finally {
      channelHandlerContext.write(ResponseMessageType.PREPARE_STORE_COPY_RESPONSE);
      channelHandlerContext.writeAndFlush(response).addListener(closeablesListener);
      this.protocol.expect(CatchupServerProtocol.State.MESSAGE_TYPE);
    }
  }

  private PrepareStoreCopyResponse createSuccessfulResponse(CheckPointer checkPointer,
      PrepareStoreCopyFiles prepareStoreCopyFiles) throws IOException {
    File[] files = prepareStoreCopyFiles.listReplayableFiles();
    long lastCheckPointedTransactionId = checkPointer.lastCheckPointedTransactionId();
    return PrepareStoreCopyResponse.success(files, lastCheckPointedTransactionId);
  }

  private Resource tryCheckpointAndAcquireMutex(CheckPointer checkPointer) throws IOException {
    return this.db.getStoreCopyCheckPointMutex().storeCopy(() ->
    {
      checkPointer.tryCheckPoint(new SimpleTriggerInfo("Store copy"));
    });
  }

  private boolean canPrepareForStoreCopy(Database db) {
    if (!db.getDatabaseAvailabilityGuard().isAvailable()) {
      this.log.warn(
          "Unable to prepare for store copy because database '" + db.getNamedDatabaseId().name()
              + "' is unavailable");
      return false;
    } else {
      return true;
    }
  }
}
