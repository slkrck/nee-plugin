/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup;

import io.nee.causalclustering.catchup.v3.CatchupProtocolClientInstaller;
import io.nee.causalclustering.net.BootstrapConfiguration;
import io.nee.causalclustering.protocol.ModifierProtocolInstaller;
import io.nee.causalclustering.protocol.NettyPipelineBuilderFactory;
import io.nee.causalclustering.protocol.ProtocolInstaller;
import io.nee.causalclustering.protocol.ProtocolInstallerRepository;
import io.nee.causalclustering.protocol.application.ApplicationProtocols;
import io.nee.causalclustering.protocol.handshake.ApplicationProtocolRepository;
import io.nee.causalclustering.protocol.handshake.ApplicationSupportedProtocols;
import io.nee.causalclustering.protocol.handshake.HandshakeClientInitializer;
import io.nee.causalclustering.protocol.handshake.ModifierProtocolRepository;
import io.nee.causalclustering.protocol.handshake.ModifierSupportedProtocols;
import io.nee.causalclustering.protocol.init.ClientChannelInitializer;
import io.nee.causalclustering.protocol.modifier.ModifierProtocols;
import io.netty.channel.socket.SocketChannel;
import java.time.Clock;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.time.Clocks;

public final class CatchupClientBuilder {

  private CatchupClientBuilder() {
  }

  public static CatchupClientBuilder.NeedsCatchupProtocols builder() {
    return new CatchupClientBuilder.StepBuilder();
  }

  public interface AcceptsOptionalParams {

    CatchupClientBuilder.AcceptsOptionalParams handShakeTimeout(Duration n1);

    CatchupClientBuilder.AcceptsOptionalParams clock(Clock n1);

    CatchupClientBuilder.AcceptsOptionalParams debugLogProvider(LogProvider n1);

    CatchupClientBuilder.AcceptsOptionalParams userLogProvider(LogProvider n1);

    CatchupClientFactory build();
  }

  public interface NeedBootstrapConfig {

    CatchupClientBuilder.AcceptsOptionalParams bootstrapConfig(
        BootstrapConfiguration<? extends SocketChannel> n1);
  }

  public interface NeedsInactivityTimeout {

    CatchupClientBuilder.NeedsScheduler inactivityTimeout(Duration n1);
  }

  public interface NeedsScheduler {

    CatchupClientBuilder.NeedBootstrapConfig scheduler(JobScheduler n1);
  }

  public interface NeedsPipelineBuilder {

    CatchupClientBuilder.NeedsInactivityTimeout pipelineBuilder(NettyPipelineBuilderFactory n1);
  }

  public interface NeedsModifierProtocols {

    CatchupClientBuilder.NeedsPipelineBuilder modifierProtocols(
        Collection<ModifierSupportedProtocols> n1);
  }

  public interface NeedsCatchupProtocols {

    CatchupClientBuilder.NeedsModifierProtocols catchupProtocols(ApplicationSupportedProtocols n1);
  }

  private static class StepBuilder
      implements CatchupClientBuilder.NeedsCatchupProtocols,
      CatchupClientBuilder.NeedsModifierProtocols, CatchupClientBuilder.NeedsPipelineBuilder,
      CatchupClientBuilder.NeedsInactivityTimeout, CatchupClientBuilder.NeedsScheduler,
      CatchupClientBuilder.NeedBootstrapConfig,
      CatchupClientBuilder.AcceptsOptionalParams {

    private NettyPipelineBuilderFactory pipelineBuilder;
    private ApplicationSupportedProtocols catchupProtocols;
    private Collection<ModifierSupportedProtocols> modifierProtocols;
    private JobScheduler scheduler;
    private LogProvider debugLogProvider = NullLogProvider.getInstance();
    private LogProvider userLogProvider = NullLogProvider.getInstance();
    private Duration inactivityTimeout;
    private Duration handshakeTimeout = Duration.ofSeconds(5L);
    private Clock clock = Clocks.systemClock();
    private BootstrapConfiguration<? extends SocketChannel> bootstrapConfiguration;

    public CatchupClientBuilder.NeedsModifierProtocols catchupProtocols(
        ApplicationSupportedProtocols catchupProtocols) {
      this.catchupProtocols = catchupProtocols;
      return this;
    }

    public CatchupClientBuilder.NeedsPipelineBuilder modifierProtocols(
        Collection<ModifierSupportedProtocols> modifierProtocols) {
      this.modifierProtocols = modifierProtocols;
      return this;
    }

    public CatchupClientBuilder.NeedsInactivityTimeout pipelineBuilder(
        NettyPipelineBuilderFactory pipelineBuilder) {
      this.pipelineBuilder = pipelineBuilder;
      return this;
    }

    public CatchupClientBuilder.NeedsScheduler inactivityTimeout(Duration inactivityTimeout) {
      this.inactivityTimeout = inactivityTimeout;
      return this;
    }

    public CatchupClientBuilder.NeedBootstrapConfig scheduler(JobScheduler scheduler) {
      this.scheduler = scheduler;
      return this;
    }

    public CatchupClientBuilder.AcceptsOptionalParams handShakeTimeout(Duration handshakeTimeout) {
      this.handshakeTimeout = handshakeTimeout;
      return this;
    }

    public CatchupClientBuilder.AcceptsOptionalParams clock(Clock clock) {
      this.clock = clock;
      return this;
    }

    public CatchupClientBuilder.AcceptsOptionalParams debugLogProvider(
        LogProvider debugLogProvider) {
      this.debugLogProvider = debugLogProvider;
      return this;
    }

    public CatchupClientBuilder.AcceptsOptionalParams userLogProvider(LogProvider userLogProvider) {
      this.userLogProvider = userLogProvider;
      return this;
    }

    public CatchupClientBuilder.AcceptsOptionalParams bootstrapConfig(
        BootstrapConfiguration<? extends SocketChannel> bootstrapConfiguration) {
      this.bootstrapConfiguration = bootstrapConfiguration;
      return this;
    }

    public CatchupClientFactory build() {
      ApplicationProtocolRepository applicationProtocolRepository =
          new ApplicationProtocolRepository(ApplicationProtocols.values(), this.catchupProtocols);
      ModifierProtocolRepository modifierProtocolRepository = new ModifierProtocolRepository(
          ModifierProtocols.values(), this.modifierProtocols);
      Function<CatchupResponseHandler, ClientChannelInitializer> channelInitializerFactory = (handler) ->
      {
        List<ProtocolInstaller.Factory<ProtocolInstaller.Orientation.Client, ?>> installers =
            List.of(new CatchupProtocolClientInstaller.Factory(this.pipelineBuilder,
                this.debugLogProvider, handler));
        ProtocolInstallerRepository<ProtocolInstaller.Orientation.Client> protocolInstallerRepository =
            new ProtocolInstallerRepository(installers,
                ModifierProtocolInstaller.allClientInstallers);
        HandshakeClientInitializer handshakeInitializer =
            new HandshakeClientInitializer(applicationProtocolRepository,
                modifierProtocolRepository, protocolInstallerRepository,
                this.pipelineBuilder, this.handshakeTimeout, this.debugLogProvider,
                this.debugLogProvider);
        return new ClientChannelInitializer(handshakeInitializer, this.pipelineBuilder,
            this.handshakeTimeout, this.debugLogProvider);
      };
      CatchupChannelPoolService catchupChannelPoolService =
          new CatchupChannelPoolService(this.bootstrapConfiguration, this.scheduler, this.clock,
              channelInitializerFactory);
      return new CatchupClientFactory(this.inactivityTimeout, catchupChannelPoolService);
    }
  }
}
