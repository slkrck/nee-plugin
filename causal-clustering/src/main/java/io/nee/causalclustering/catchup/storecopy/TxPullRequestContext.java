/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import java.util.OptionalLong;
import org.neo4j.storageengine.api.StoreId;

class TxPullRequestContext {

  private final RequiredTransactions requiredTransactions;
  private final StoreId expectedStoreId;
  private final boolean hasFallbackStartId;

  private TxPullRequestContext(RequiredTransactions requiredTransactions, StoreId expectedStoreId,
      boolean hasFallbackStartId) {
    this.requiredTransactions = requiredTransactions;
    this.expectedStoreId = expectedStoreId;
    this.hasFallbackStartId = hasFallbackStartId;
  }

  private TxPullRequestContext(RequiredTransactions requiredTransactions, StoreId expectedStoreId) {
    this(requiredTransactions, expectedStoreId, false);
  }

  static TxPullRequestContext createContextFromStoreCopy(RequiredTransactions requiredTransactions,
      StoreId expectedStoreId) {
    return new TxPullRequestContext(requiredTransactions, expectedStoreId);
  }

  static TxPullRequestContext createContextFromCatchingUp(StoreId expectedStoreId,
      CommitState commitState) {
    if (commitState.transactionLogIndex().isPresent()) {
      return new TxPullRequestContext(
          RequiredTransactions.noConstraint(commitState.transactionLogIndex().get() + 1L),
          expectedStoreId);
    } else {
      long metaDataStoreIndex = commitState.metaDataStoreIndex();
      return metaDataStoreIndex == 1L ? new TxPullRequestContext(
          RequiredTransactions.noConstraint(metaDataStoreIndex + 1L), expectedStoreId)
          : new TxPullRequestContext(RequiredTransactions.noConstraint(metaDataStoreIndex),
              expectedStoreId, true);
    }
  }

  OptionalLong fallbackStartId() {
    return this.hasFallbackStartId ? OptionalLong.of(this.startTxIdExclusive() + 1L)
        : OptionalLong.empty();
  }

  long startTxIdExclusive() {
    return this.requiredTransactions.startTxId() - 1L;
  }

  StoreId expectedStoreId() {
    return this.expectedStoreId;
  }

  boolean constraintReached(long lastWrittenTx) {
    return this.requiredTransactions.noRequiredTxId()
        || this.requiredTransactions.requiredTxId() <= lastWrittenTx;
  }
}
