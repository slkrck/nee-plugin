/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.catchup.storecopy;

import io.nee.causalclustering.catchup.tx.FileCopyMonitor;
import java.io.File;
import java.io.IOException;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.monitoring.Monitors;

public class StreamToDiskProvider implements StoreFileStreamProvider {

  private final File storeDir;
  private final FileSystemAbstraction fs;
  private final FileCopyMonitor fileCopyMonitor;

  public StreamToDiskProvider(File storeDir, FileSystemAbstraction fs, Monitors monitors) {
    this.storeDir = storeDir;
    this.fs = fs;
    this.fileCopyMonitor = monitors.newMonitor(FileCopyMonitor.class, new String[0]);
  }

  public StoreFileStream acquire(String destination, int requiredAlignment) throws IOException {
    File fileName = new File(this.storeDir, destination);
    this.fs.mkdirs(fileName.getParentFile());
    this.fileCopyMonitor.copyFile(fileName);
    return StreamToDisk.fromFile(this.fs, fileName);
  }
}
