/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.routing.load_balancing.plugins.server_policies;

import io.nee.causalclustering.routing.load_balancing.filters.Filter;
import io.nee.causalclustering.routing.load_balancing.filters.FilterChain;
import io.nee.causalclustering.routing.load_balancing.filters.FirstValidRule;
import io.nee.causalclustering.routing.load_balancing.filters.IdentityFilter;
import io.nee.causalclustering.routing.load_balancing.filters.MinimumCountFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FilterConfigParser {

  private FilterConfigParser() {
  }

  private static Filter<ServerInfo> filterFor(String filterName, String[] args)
      throws InvalidFilterSpecification {
    byte n3 = -1;
    switch (filterName.hashCode()) {
      case -1237460524:
        if (filterName.equals("groups")) {
          n3 = 0;
        }
        break;
      case 96673:
        if (filterName.equals("all")) {
          n3 = 2;
        }
        break;
      case 108114:
        if (filterName.equals("min")) {
          n3 = 1;
        }
        break;
      case 3194945:
        if (filterName.equals("halt")) {
          n3 = 3;
        }
    }

    switch (n3) {
      case 0:
        if (args.length < 1) {
          throw new InvalidFilterSpecification(String
              .format("Invalid number of arguments for filter '%s': %d", filterName, args.length));
        } else {
          String[] n9 = args;
          int n5 = args.length;

          for (int n6 = 0; n6 < n5; ++n6) {
            String group = n9[n6];
            if (group.matches("\\W")) {
              throw new InvalidFilterSpecification(
                  String.format("Invalid group for filter '%s': '%s'", filterName, group));
            }
          }

          return new AnyGroupFilter(args);
        }
      case 1:
        if (args.length != 1) {
          throw new InvalidFilterSpecification(String
              .format("Invalid number of arguments for filter '%s': %d", filterName, args.length));
        } else {
          int minCount;
          try {
            minCount = Integer.parseInt(args[0]);
          } catch (NumberFormatException n8) {
            throw new InvalidFilterSpecification(
                String.format("Invalid argument for filter '%s': '%s'", filterName, args[0]), n8);
          }

          return new MinimumCountFilter(minCount);
        }
      case 2:
        if (args.length != 0) {
          throw new InvalidFilterSpecification(String
              .format("Invalid number of arguments for filter '%s': %d", filterName, args.length));
        }

        return IdentityFilter.as();
      case 3:
        if (args.length != 0) {
          throw new InvalidFilterSpecification(String
              .format("Invalid number of arguments for filter '%s': %d", filterName, args.length));
        }

        return HaltFilter.INSTANCE;
      default:
        throw new InvalidFilterSpecification("Unknown filter: " + filterName);
    }
  }

  public static Filter<ServerInfo> parse(String filterConfig) throws InvalidFilterSpecification {
    if (filterConfig.length() == 0) {
      throw new InvalidFilterSpecification("Filter config is empty");
    } else {
      List<FilterChain<ServerInfo>> rules = new ArrayList();
      String[] ruleSpecs = filterConfig.split(";");
      if (ruleSpecs.length == 0) {
        throw new InvalidFilterSpecification("No rules specified");
      } else {
        boolean haltFilterEncountered = false;
        String[] n4 = ruleSpecs;
        int n5 = ruleSpecs.length;

        for (int n6 = 0; n6 < n5; ++n6) {
          String ruleSpec = n4[n6];
          ruleSpec = ruleSpec.trim();
          List<Filter<ServerInfo>> filterChain = new ArrayList();
          String[] filterSpecs = ruleSpec.split("->");
          boolean allFilterEncountered = false;
          String[] n11 = filterSpecs;
          int n12 = filterSpecs.length;

          for (int n13 = 0; n13 < n12; ++n13) {
            String filterSpec = n11[n13];
            filterSpec = filterSpec.trim();
            String[] nameAndArgs = filterSpec.split("\\(", 0);
            if (nameAndArgs.length != 2) {
              throw new InvalidFilterSpecification(
                  String.format("Syntax error filter specification: '%s'", filterSpec));
            }

            String namePart = nameAndArgs[0].trim();
            String argsPart = nameAndArgs[1].trim();
            if (!argsPart.endsWith(")")) {
              throw new InvalidFilterSpecification(
                  String.format("No closing parenthesis: '%s'", filterSpec));
            }

            argsPart = argsPart.substring(0, argsPart.length() - 1);
            String filterName = namePart.trim();
            if (!filterName.matches("\\w+")) {
              throw new InvalidFilterSpecification(
                  String.format("Syntax error filter name: '%s'", filterName));
            }

            String[] nonEmptyArgs = (String[]) ((List) Arrays.stream(argsPart.split(","))
                .map(String::trim).filter((s) ->
                {
                  return !s.isEmpty();
                })
                .collect(Collectors.toList())).toArray(new String[0]);
            String[] n19 = nonEmptyArgs;
            int n20 = nonEmptyArgs.length;

            for (int n21 = 0; n21 < n20; ++n21) {
              String arg = n19[n21];
              if (!arg.matches("[\\w-]+")) {
                throw new InvalidFilterSpecification(
                    String.format("Syntax error argument: '%s'", arg));
              }
            }

            if (haltFilterEncountered) {
              if (filterChain.size() > 0) {
                throw new InvalidFilterSpecification(String
                    .format("Filter 'halt' may not be followed by other filters: '%s'", ruleSpec));
              }

              throw new InvalidFilterSpecification(
                  String.format("Rule 'halt' may not followed by other rules: '%s'", filterConfig));
            }

            Filter<ServerInfo> filter = filterFor(filterName, nonEmptyArgs);
            if (filter == HaltFilter.INSTANCE) {
              if (filterChain.size() != 0) {
                throw new InvalidFilterSpecification(String
                    .format("Filter 'halt' must be the only filter in a rule: '%s'", ruleSpec));
              }

              haltFilterEncountered = true;
            } else if (filter == IdentityFilter.INSTANCE) {
              if (allFilterEncountered || filterChain.size() != 0) {
                throw new InvalidFilterSpecification(
                    String.format("Filter 'all' is implicit but allowed only first in a rule: '%s'",
                        ruleSpec));
              }

              allFilterEncountered = true;
            } else {
              filterChain.add(filter);
            }
          }

          if (filterChain.size() > 0) {
            rules.add(new FilterChain(filterChain));
          }
        }

        if (!haltFilterEncountered) {
          rules.add(new FilterChain(Collections.singletonList(IdentityFilter.as())));
        }

        return new FirstValidRule(rules);
      }
    }
  }
}
