/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.routing.load_balancing;

import io.nee.causalclustering.core.consensus.LeaderInfo;
import io.nee.causalclustering.core.consensus.LeaderLocator;
import io.nee.causalclustering.core.consensus.NoLeaderFoundException;
import io.nee.causalclustering.discovery.ClientConnector;
import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.discovery.RoleInfo;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.identity.MemberId;
import java.util.Map;
import java.util.Optional;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class DefaultLeaderService implements LeaderService {

  private final LeaderLocatorForDatabase leaderLocatorForDatabase;
  private final TopologyService topologyService;
  private final Log log;

  public DefaultLeaderService(LeaderLocatorForDatabase leaderLocatorForDatabase,
      TopologyService topologyService, LogProvider logProvider) {
    this.leaderLocatorForDatabase = leaderLocatorForDatabase;
    this.topologyService = topologyService;
    this.log = logProvider.getLog(this.getClass());
  }

  private static Optional<MemberId> getLeaderIdFromLeaderLocator(LeaderLocator leaderLocator) {
    try {
      return Optional.of(leaderLocator.getLeaderInfo()).map(LeaderInfo::memberId);
    } catch (NoLeaderFoundException n2) {
      return Optional.empty();
    }
  }

  public Optional<MemberId> getLeaderId(NamedDatabaseId namedDatabaseId) {
    Optional<LeaderLocator> leaderLocator = this.leaderLocatorForDatabase
        .getLeader(namedDatabaseId);
    Optional leaderId;
    if (leaderLocator.isPresent()) {
      leaderId = getLeaderIdFromLeaderLocator(leaderLocator.get());
      this.log.debug("Leader locator %s for database %s returned leader ID %s", leaderLocator,
          namedDatabaseId, leaderId);
      return leaderId;
    } else {
      leaderId = this.getLeaderIdFromTopologyService(namedDatabaseId);
      this.log.debug("Topology service for database %s returned leader ID %s", namedDatabaseId,
          leaderId);
      return leaderId;
    }
  }

  public Optional<SocketAddress> getLeaderBoltAddress(NamedDatabaseId namedDatabaseId) {
    Optional<SocketAddress> leaderBoltAddress = this.getLeaderId(namedDatabaseId)
        .flatMap(this::resolveBoltAddress);
    this.log
        .debug("Leader for database %s has Bolt address %s", namedDatabaseId, leaderBoltAddress);
    return leaderBoltAddress;
  }

  private Optional<MemberId> getLeaderIdFromTopologyService(NamedDatabaseId namedDatabaseId) {
    return this.topologyService.coreTopologyForDatabase(namedDatabaseId).members().keySet().stream()
        .filter((memberId) ->
        {
          return this.topologyService.lookupRole(
              namedDatabaseId, memberId) ==
              RoleInfo.LEADER;
        }).findFirst();
  }

  private Optional<SocketAddress> resolveBoltAddress(MemberId memberId) {
    Map<MemberId, CoreServerInfo> coresById = this.topologyService.allCoreServers();
    this.log.debug("Resolving Bolt address for member %s using %s", memberId, coresById);
    return Optional.ofNullable(coresById.get(memberId)).map(ClientConnector::boltAddress);
  }
}
