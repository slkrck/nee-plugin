/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.routing.load_balancing.plugins;

import io.nee.causalclustering.routing.load_balancing.LoadBalancingProcessor;
import java.util.Collections;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.procedure.builtin.routing.RoutingResult;
import org.neo4j.values.virtual.MapValue;

public class ServerShufflingProcessor implements LoadBalancingProcessor {

  private final LoadBalancingProcessor delegate;

  public ServerShufflingProcessor(LoadBalancingProcessor delegate) {
    this.delegate = delegate;
  }

  public RoutingResult run(NamedDatabaseId namedDatabaseId, MapValue context)
      throws ProcedureException {
    RoutingResult result = this.delegate.run(namedDatabaseId, context);
    Collections.shuffle(result.routeEndpoints());
    Collections.shuffle(result.writeEndpoints());
    Collections.shuffle(result.readEndpoints());
    return result;
  }

  public LoadBalancingProcessor delegate() {
    return this.delegate;
  }
}
