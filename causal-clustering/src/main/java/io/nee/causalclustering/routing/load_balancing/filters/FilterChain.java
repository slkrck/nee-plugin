/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.routing.load_balancing.filters;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class FilterChain<T> implements Filter<T> {

  private final List<Filter<T>> chain;

  public FilterChain(List<Filter<T>> chain) {
    this.chain = chain;
  }

  public Set<T> apply(Set<T> data) {
    Filter filter;
    for (Iterator n2 = this.chain.iterator(); n2.hasNext(); data = filter.apply(data)) {
      filter = (Filter) n2.next();
    }

    return data;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      FilterChain<?> that = (FilterChain) o;
      return Objects.equals(this.chain, that.chain);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.chain);
  }

  public String toString() {
    return "FilterChain{chain=" + this.chain + "}";
  }
}
