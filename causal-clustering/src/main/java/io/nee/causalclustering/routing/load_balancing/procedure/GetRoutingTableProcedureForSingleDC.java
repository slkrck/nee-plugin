/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.routing.load_balancing.procedure;

import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.discovery.ClientConnector;
import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.routing.load_balancing.LeaderService;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.LogProvider;
import org.neo4j.procedure.builtin.routing.BaseGetRoutingTableProcedure;
import org.neo4j.procedure.builtin.routing.RoutingResult;
import org.neo4j.values.virtual.MapValue;

public class GetRoutingTableProcedureForSingleDC extends BaseGetRoutingTableProcedure {

  private static final String DESCRIPTION = "Returns cluster endpoints and their capabilities for single data center setup.";
  private final TopologyService topologyService;
  private final LeaderService leaderService;

  public GetRoutingTableProcedureForSingleDC(List<String> namespace,
      TopologyService topologyService, LeaderService leaderService,
      DatabaseManager<?> databaseManager, Config config, LogProvider logProvider) {
    super(namespace, databaseManager, config, logProvider);
    this.topologyService = topologyService;
    this.leaderService = leaderService;
  }

  protected String description() {
    return "Returns cluster endpoints and their capabilities for single data center setup.";
  }

  protected RoutingResult invoke(NamedDatabaseId namedDatabaseId, MapValue routingContext) {
    List<SocketAddress> routeEndpoints = this.routeEndpoints(namedDatabaseId);
    List<SocketAddress> writeEndpoints = this.writeEndpoints(namedDatabaseId);
    List<SocketAddress> readEndpoints = this.readEndpoints(namedDatabaseId);
    long timeToLiveMillis = this.config.get(GraphDatabaseSettings.routing_ttl).toMillis();
    return new RoutingResult(routeEndpoints, writeEndpoints, readEndpoints, timeToLiveMillis);
  }

  private List<SocketAddress> routeEndpoints(NamedDatabaseId namedDatabaseId) {
    List<SocketAddress> routers = this.coreServersFor(namedDatabaseId).stream()
        .map(ClientConnector::boltAddress).collect(Collectors.toList());
    Collections.shuffle(routers);
    return routers;
  }

  private List<SocketAddress> writeEndpoints(NamedDatabaseId namedDatabaseId) {
    Optional<SocketAddress> optionalLeaderAddress = this.leaderService
        .getLeaderBoltAddress(namedDatabaseId);
    if (optionalLeaderAddress.isEmpty()) {
      this.log.debug(
          "No leader server found. This can happen during a leader switch. No write end points available");
    }

    return optionalLeaderAddress.stream().collect(Collectors.toList());
  }

  private List<SocketAddress> readEndpoints(NamedDatabaseId namedDatabaseId) {
    List<SocketAddress> readReplicas =
        this.readReplicasFor(namedDatabaseId).stream().map(ClientConnector::boltAddress)
            .collect(Collectors.toList());
    boolean allowReadsOnFollowers = readReplicas.isEmpty() || this.config
        .get(CausalClusteringSettings.cluster_allow_reads_on_followers);
    Stream<SocketAddress> coreReadEndPoints =
        allowReadsOnFollowers ? this.coreReadEndPoints(namedDatabaseId) : Stream.empty();
    List<SocketAddress> readEndPoints = Stream.concat(readReplicas.stream(), coreReadEndPoints)
        .collect(Collectors.toList());
    Collections.shuffle(readEndPoints);
    return readEndPoints;
  }

  private Stream<SocketAddress> coreReadEndPoints(NamedDatabaseId namedDatabaseId) {
    Optional<SocketAddress> optionalLeaderAddress = this.leaderService
        .getLeaderBoltAddress(namedDatabaseId);
    Collection<CoreServerInfo> coreServerInfos = this.coreServersFor(namedDatabaseId);
    Stream<SocketAddress> coreAddresses = coreServerInfos.stream()
        .map(ClientConnector::boltAddress);
    if (optionalLeaderAddress.isPresent() && coreServerInfos.size() > 1) {
      SocketAddress leaderAddress = optionalLeaderAddress.get();
      return coreAddresses.filter((address) ->
      {
        return !leaderAddress.equals(address);
      });
    } else {
      return coreAddresses;
    }
  }

  private Collection<CoreServerInfo> coreServersFor(NamedDatabaseId namedDatabaseId) {
    return this.topologyService.coreTopologyForDatabase(namedDatabaseId).members().values();
  }

  private Collection<ReadReplicaInfo> readReplicasFor(NamedDatabaseId namedDatabaseId) {
    return this.topologyService.readReplicaTopologyForDatabase(namedDatabaseId).members().values();
  }
}
