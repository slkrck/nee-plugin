/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.routing.load_balancing.procedure;

import io.nee.causalclustering.routing.load_balancing.LoadBalancingProcessor;
import java.util.List;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.LogProvider;
import org.neo4j.procedure.builtin.routing.BaseGetRoutingTableProcedure;
import org.neo4j.procedure.builtin.routing.RoutingResult;
import org.neo4j.values.virtual.MapValue;

public class GetRoutingTableProcedureForMultiDC extends BaseGetRoutingTableProcedure {

  private static final String DESCRIPTION = "Returns cluster endpoints and their capabilities.";
  private final LoadBalancingProcessor loadBalancingProcessor;

  public GetRoutingTableProcedureForMultiDC(List<String> namespace,
      LoadBalancingProcessor loadBalancingProcessor, DatabaseManager<?> databaseManager,
      Config config, LogProvider logProvider) {
    super(namespace, databaseManager, config, logProvider);
    this.loadBalancingProcessor = loadBalancingProcessor;
  }

  protected String description() {
    return "Returns cluster endpoints and their capabilities.";
  }

  protected RoutingResult invoke(NamedDatabaseId namedDatabaseId, MapValue routingContext)
      throws ProcedureException {
    return this.loadBalancingProcessor.run(namedDatabaseId, routingContext);
  }
}
