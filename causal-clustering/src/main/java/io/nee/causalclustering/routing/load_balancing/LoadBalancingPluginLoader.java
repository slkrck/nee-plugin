/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.routing.load_balancing;

import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.routing.load_balancing.plugins.ServerShufflingProcessor;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.neo4j.configuration.Config;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.service.Services;

public class LoadBalancingPluginLoader {

  private LoadBalancingPluginLoader() {
  }

  public static void validate(Config config, Log log) {
    LoadBalancingPlugin plugin = findPlugin(config);
    plugin.validate(config, log);
  }

  public static LoadBalancingProcessor load(TopologyService topologyService,
      LeaderService leaderService, LogProvider logProvider, Config config)
      throws Throwable {
    LoadBalancingPlugin plugin = findPlugin(config);
    plugin.init(topologyService, leaderService, logProvider, config);
    return
        config.get(CausalClusteringSettings.load_balancing_shuffle) && !plugin.isShufflingPlugin()
            ? new ServerShufflingProcessor(plugin) : plugin;
  }

  private static LoadBalancingPlugin findPlugin(Config config) {
    Set<String> availableOptions = new HashSet();
    Iterable<LoadBalancingPlugin> allImplementationsOnClasspath = Services
        .loadAll(LoadBalancingPlugin.class);
    String configuredName = config.get(CausalClusteringSettings.load_balancing_plugin);
    Iterator n4 = allImplementationsOnClasspath.iterator();

    while (n4.hasNext()) {
      LoadBalancingPlugin plugin = (LoadBalancingPlugin) n4.next();
      if (plugin.pluginName().equals(configuredName)) {
        return plugin;
      }

      availableOptions.add(plugin.pluginName());
    }

    throw new IllegalArgumentException(
        String.format(
            "Could not find load balancing plugin with name: '%s' among available options: %s",
            configuredName, availableOptions));
  }
}
