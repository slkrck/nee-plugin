/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.routing.load_balancing.plugins.server_policies;

import io.nee.causalclustering.routing.load_balancing.filters.IdentityFilter;
import java.util.HashMap;
import java.util.Map;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.kernel.api.exceptions.Status.Procedure;
import org.neo4j.logging.Log;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.MapValue;

public class Policies {

  public static final String POLICY_KEY = "policy";
  static final String DEFAULT_POLICY_NAME = "default";
  static final Policy DEFAULT_POLICY = new FilteringPolicy(IdentityFilter.as());
  private final Map<String, Policy> policies = new HashMap();
  private final Log log;

  Policies(Log log) {
    this.log = log;
  }

  void addPolicy(String policyName, Policy policy) {
    Policy oldPolicy = this.policies.putIfAbsent(policyName, policy);
    if (oldPolicy != null) {
      this.log.error(String.format("Policy name conflict for '%s'.", policyName));
    }
  }

  Policy selectFor(MapValue context) throws ProcedureException {
    AnyValue policyName = context.get("policy");
    if (policyName == Values.NO_VALUE) {
      return this.defaultPolicy();
    } else {
      Policy selectedPolicy = this.policies.get(((TextValue) policyName).stringValue());
      if (selectedPolicy == null) {
        throw new ProcedureException(Procedure.ProcedureCallFailed,
            String.format("Policy definition for '%s' could not be found.", policyName)
        );
      } else {
        return selectedPolicy;
      }
    }
  }

  private Policy defaultPolicy() {
    Policy registeredDefault = this.policies.get("default");
    return registeredDefault != null ? registeredDefault : DEFAULT_POLICY;
  }
}
