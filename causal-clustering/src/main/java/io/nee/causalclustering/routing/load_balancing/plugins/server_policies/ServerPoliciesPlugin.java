/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.routing.load_balancing.plugins.server_policies;

import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.discovery.CoreServerInfo;
import io.nee.causalclustering.discovery.DatabaseCoreTopology;
import io.nee.causalclustering.discovery.DatabaseReadReplicaTopology;
import io.nee.causalclustering.discovery.DiscoveryServerInfo;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.routing.load_balancing.LeaderService;
import io.nee.causalclustering.routing.load_balancing.LoadBalancingPlugin;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.procedure.builtin.routing.RoutingResult;
import org.neo4j.values.virtual.MapValue;

public class ServerPoliciesPlugin implements LoadBalancingPlugin {

  public static final String PLUGIN_NAME = "server_policies";
  private TopologyService topologyService;
  private LeaderService leaderService;
  private Long timeToLive;
  private boolean allowReadsOnFollowers;
  private Policies policies;
  private boolean shouldShuffle;

  private static ServerInfo newServerInfo(Entry<MemberId, ? extends DiscoveryServerInfo> entry) {
    return newServerInfo(entry.getKey(), entry.getValue());
  }

  private static ServerInfo newServerInfo(MemberId memberId,
      DiscoveryServerInfo discoveryServerInfo) {
    return new ServerInfo(discoveryServerInfo.connectors().boltAddress(), memberId,
        discoveryServerInfo.groups());
  }

  public void validate(Config config, Log log) {
    FilteringPolicyLoader.loadServerPolicies(config, log);
  }

  public void init(TopologyService topologyService, LeaderService leaderService,
      LogProvider logProvider, Config config) {
    this.topologyService = topologyService;
    this.leaderService = leaderService;
    this.timeToLive = config.get(GraphDatabaseSettings.routing_ttl).toMillis();
    this.allowReadsOnFollowers = config
        .get(CausalClusteringSettings.cluster_allow_reads_on_followers);
    this.policies = FilteringPolicyLoader
        .loadServerPolicies(config, logProvider.getLog(this.getClass()));
    this.shouldShuffle = config.get(CausalClusteringSettings.load_balancing_shuffle);
  }

  public String pluginName() {
    return "server_policies";
  }

  public boolean isShufflingPlugin() {
    return true;
  }

  public RoutingResult run(NamedDatabaseId namedDatabaseId, MapValue context)
      throws ProcedureException {
    Policy policy = this.policies.selectFor(context);
    DatabaseCoreTopology coreTopology = this.coreTopologyFor(namedDatabaseId);
    DatabaseReadReplicaTopology rrTopology = this.readReplicaTopology(namedDatabaseId);
    return new RoutingResult(this.routeEndpoints(coreTopology, policy),
        this.writeEndpoints(namedDatabaseId),
        this.readEndpoints(coreTopology, rrTopology, policy, namedDatabaseId), this.timeToLive);
  }

  private List<SocketAddress> routeEndpoints(DatabaseCoreTopology coreTopology, Policy policy) {
    Set<ServerInfo> routers = coreTopology.members().entrySet().stream()
        .map(ServerPoliciesPlugin::newServerInfo).collect(Collectors.toSet());
    Set<ServerInfo> preferredRouters = policy.apply(routers);
    Stream n10000 = routers.stream();
    Objects.requireNonNull(preferredRouters);
    List<ServerInfo> otherRoutersList = (List) n10000
        .filter(Predicate.not(preferredRouters::contains)).collect(Collectors.toList());
    ArrayList<ServerInfo> preferredRoutersList = new ArrayList(preferredRouters);
    if (this.shouldShuffle) {
      Collections.shuffle(preferredRoutersList);
      Collections.shuffle(otherRoutersList);
    }

    return Stream.concat(preferredRoutersList.stream(), otherRoutersList.stream())
        .map(ServerInfo::boltAddress).collect(Collectors.toList());
  }

  private List<SocketAddress> writeEndpoints(NamedDatabaseId namedDatabaseId) {
    return this.leaderService.getLeaderBoltAddress(namedDatabaseId).map(List::of)
        .orElse(Collections.emptyList());
  }

  private List<SocketAddress> readEndpoints(DatabaseCoreTopology coreTopology,
      DatabaseReadReplicaTopology rrTopology, Policy policy,
      NamedDatabaseId namedDatabaseId) {
    Set<ServerInfo> possibleReaders =
        rrTopology.members().entrySet().stream().map(ServerPoliciesPlugin::newServerInfo)
            .collect(Collectors.toSet());
    if (this.allowReadsOnFollowers || possibleReaders.isEmpty()) {
      Map<MemberId, CoreServerInfo> coreMembers = coreTopology.members();
      Set<MemberId> validCores = coreMembers.keySet();
      Optional<MemberId> optionalLeaderId = this.leaderService.getLeaderId(namedDatabaseId);
      if (optionalLeaderId.isPresent()) {
        MemberId leaderId = optionalLeaderId.get();
        validCores = validCores.stream().filter((memberId) ->
        {
          return !memberId.equals(leaderId);
        }).collect(Collectors.toSet());
      }

      Iterator n13 = validCores.iterator();

      while (n13.hasNext()) {
        MemberId validCore = (MemberId) n13.next();
        CoreServerInfo coreServerInfo = coreMembers.get(validCore);
        if (coreServerInfo != null) {
          possibleReaders.add(newServerInfo(validCore, coreServerInfo));
        }
      }
    }

    ArrayList<ServerInfo> readers = new ArrayList(policy.apply(possibleReaders));
    if (this.shouldShuffle) {
      Collections.shuffle(readers);
    }

    return readers.stream().map(ServerInfo::boltAddress).collect(Collectors.toList());
  }

  private DatabaseCoreTopology coreTopologyFor(NamedDatabaseId namedDatabaseId) {
    return this.topologyService.coreTopologyForDatabase(namedDatabaseId);
  }

  private DatabaseReadReplicaTopology readReplicaTopology(NamedDatabaseId namedDatabaseId) {
    return this.topologyService.readReplicaTopologyForDatabase(namedDatabaseId);
  }
}
