/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.routing.load_balancing.plugins.server_policies;

import io.nee.causalclustering.identity.MemberId;
import java.util.Objects;
import java.util.Set;
import org.neo4j.configuration.helpers.SocketAddress;

public class ServerInfo {

  private final SocketAddress boltAddress;
  private final MemberId memberId;
  private final Set<String> groups;

  public ServerInfo(SocketAddress boltAddress, MemberId memberId, Set<String> groups) {
    this.boltAddress = boltAddress;
    this.memberId = memberId;
    this.groups = groups;
  }

  public MemberId memberId() {
    return this.memberId;
  }

  SocketAddress boltAddress() {
    return this.boltAddress;
  }

  Set<String> groups() {
    return this.groups;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ServerInfo that = (ServerInfo) o;
      return Objects.equals(this.boltAddress, that.boltAddress) && Objects
          .equals(this.memberId, that.memberId) &&
          Objects.equals(this.groups, that.groups);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.boltAddress, this.memberId, this.groups);
  }

  public String toString() {
    return "ServerInfo{boltAddress=" + this.boltAddress + ", memberId=" + this.memberId
        + ", groups=" + this.groups + "}";
  }
}
