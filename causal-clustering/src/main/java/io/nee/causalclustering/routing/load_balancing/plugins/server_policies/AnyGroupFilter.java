/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.routing.load_balancing.plugins.server_policies;

import io.nee.causalclustering.routing.load_balancing.filters.Filter;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.neo4j.internal.helpers.collection.Iterators;

public class AnyGroupFilter implements Filter<ServerInfo> {

  private final Predicate<ServerInfo> matchesAnyGroup;
  private final Set<String> groups;

  AnyGroupFilter(String... groups) {
    this(Iterators.asSet(groups));
  }

  AnyGroupFilter(Set<String> groups) {
    this.matchesAnyGroup = (serverInfo) ->
    {
      Iterator n2 = serverInfo.groups().iterator();

      String group;
      do {
        if (!n2.hasNext()) {
          return false;
        }

        group = (String) n2.next();
      }
      while (!groups.contains(group));

      return true;
    };
    this.groups = groups;
  }

  public Set<ServerInfo> apply(Set<ServerInfo> data) {
    return data.stream().filter(this.matchesAnyGroup).collect(Collectors.toSet());
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      AnyGroupFilter that = (AnyGroupFilter) o;
      return Objects.equals(this.groups, that.groups);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.groups);
  }

  public String toString() {
    return "AnyGroupFilter{groups=" + this.groups + "}";
  }
}
