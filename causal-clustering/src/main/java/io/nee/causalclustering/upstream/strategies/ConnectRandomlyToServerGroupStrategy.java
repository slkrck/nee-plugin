/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.upstream.strategies;

import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.upstream.UpstreamDatabaseSelectionStrategy;
import java.util.List;
import java.util.Optional;
import org.neo4j.kernel.database.NamedDatabaseId;

public class ConnectRandomlyToServerGroupStrategy extends UpstreamDatabaseSelectionStrategy {

  static final String IDENTITY = "connect-randomly-to-server-group";
  private ConnectRandomlyToServerGroupImpl strategyImpl;

  public ConnectRandomlyToServerGroupStrategy() {
    super("connect-randomly-to-server-group");
  }

  public void init() {
    List<String> groups = this.config
        .get(CausalClusteringSettings.connect_randomly_to_server_group_strategy);
    this.strategyImpl = new ConnectRandomlyToServerGroupImpl(groups, this.topologyService,
        this.myself);
    if (groups.isEmpty()) {
      this.log.warn("No server groups configured for upstream strategy " + this.name
          + ". Strategy will not find upstream servers.");
    } else {
      String readableGroups = String.join(", ", groups);
      this.log.info("Upstream selection strategy " + this.name + " configured with server groups "
          + readableGroups);
    }
  }

  public Optional<MemberId> upstreamMemberForDatabase(NamedDatabaseId namedDatabaseId) {
    return this.strategyImpl.upstreamMemberForDatabase(namedDatabaseId);
  }
}
