/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.upstream;

import io.nee.causalclustering.identity.MemberId;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import org.neo4j.internal.helpers.collection.Iterables;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.NullLogProvider;

public class UpstreamDatabaseStrategySelector {

  private final Set<UpstreamDatabaseSelectionStrategy> strategies;
  private final Log log;

  public UpstreamDatabaseStrategySelector(UpstreamDatabaseSelectionStrategy defaultStrategy) {
    this(defaultStrategy, Iterables.empty(), NullLogProvider.getInstance());
  }

  public UpstreamDatabaseStrategySelector(UpstreamDatabaseSelectionStrategy defaultStrategy,
      Iterable<UpstreamDatabaseSelectionStrategy> otherStrategies,
      LogProvider logProvider) {
    this.strategies = new LinkedHashSet();
    this.log = logProvider.getLog(this.getClass());
    Iterables.addAll(this.strategies, otherStrategies);
    this.strategies.add(defaultStrategy);
  }

  public MemberId bestUpstreamMemberForDatabase(NamedDatabaseId namedDatabaseId)
      throws UpstreamDatabaseSelectionException {
    Iterator n2 = this.strategies.iterator();

    Optional upstreamMember;
    do {
      if (!n2.hasNext()) {
        throw new UpstreamDatabaseSelectionException(
            "Could not find an upstream member for database " + namedDatabaseId.name()
                + " with which to connect");
      }

      UpstreamDatabaseSelectionStrategy strategy = (UpstreamDatabaseSelectionStrategy) n2.next();
      this.log.debug("Trying selection strategy [%s]", strategy);
      upstreamMember = strategy.upstreamMemberForDatabase(namedDatabaseId);
    }
    while (!upstreamMember.isPresent());

    MemberId memberId = (MemberId) upstreamMember.get();
    this.log.debug("Selected upstream database [%s]", memberId);
    return memberId;
  }
}
