/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.upstream.strategies;

import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.discovery.DatabaseCoreTopology;
import io.nee.causalclustering.discovery.DatabaseReadReplicaTopology;
import io.nee.causalclustering.discovery.DiscoveryServerInfo;
import io.nee.causalclustering.discovery.Topology;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.routing.load_balancing.filters.Filter;
import io.nee.causalclustering.routing.load_balancing.plugins.server_policies.FilterConfigParser;
import io.nee.causalclustering.routing.load_balancing.plugins.server_policies.InvalidFilterSpecification;
import io.nee.causalclustering.routing.load_balancing.plugins.server_policies.ServerInfo;
import io.nee.causalclustering.upstream.UpstreamDatabaseSelectionStrategy;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.kernel.database.NamedDatabaseId;

public class UserDefinedConfigurationStrategy extends UpstreamDatabaseSelectionStrategy {

  public static final String IDENTITY = "user-defined";
  private Optional<Filter<ServerInfo>> filters;

  public UserDefinedConfigurationStrategy() {
    super("user-defined");
  }

  public void init() {
    String filterConfig = this.config
        .get(CausalClusteringSettings.user_defined_upstream_selection_strategy);

    try {
      Filter<ServerInfo> parsed = FilterConfigParser.parse(filterConfig);
      this.filters = Optional.of(parsed);
      this.log
          .info("Upstream selection strategy " + this.name + " configured with " + filterConfig);
    } catch (InvalidFilterSpecification n3) {
      this.filters = Optional.empty();
      this.log.warn(
          "Cannot parse configuration '" + filterConfig + "' for upstream selection strategy "
              + this.name + ". " + n3.getMessage());
    }
  }

  public Optional<MemberId> upstreamMemberForDatabase(NamedDatabaseId namedDatabaseId) {
    return this.filters.flatMap((filters) ->
    {
      Set<ServerInfo> possibleServers = this.possibleServers(namedDatabaseId);
      return filters.apply(possibleServers).stream().map(ServerInfo::memberId).filter((memberId) ->
      {
        return !Objects
            .equals(this.myself,
                memberId);
      }).findFirst();
    });
  }

  private Set<ServerInfo> possibleServers(NamedDatabaseId namedDatabaseId) {
    DatabaseCoreTopology coreTopology = this.topologyService
        .coreTopologyForDatabase(namedDatabaseId);
    DatabaseReadReplicaTopology readReplicaTopology = this.topologyService
        .readReplicaTopologyForDatabase(namedDatabaseId);
    Stream<? extends Entry<MemberId, ? extends DiscoveryServerInfo>> infoMap =
        Stream.of(coreTopology, readReplicaTopology).map(Topology::members).map(Map::entrySet)
            .flatMap(Collection::stream);
    return infoMap.map(this::toServerInfo).collect(Collectors.toSet());
  }

  private <T extends DiscoveryServerInfo> ServerInfo toServerInfo(Entry<MemberId, T> entry) {
    T server = entry.getValue();
    MemberId memberId = entry.getKey();
    return new ServerInfo(server.connectors().boltAddress(), memberId, server.groups());
  }
}
