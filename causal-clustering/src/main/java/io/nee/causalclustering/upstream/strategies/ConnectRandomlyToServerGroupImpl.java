/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.upstream.strategies;

import io.nee.causalclustering.discovery.ReadReplicaInfo;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.identity.MemberId;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.neo4j.kernel.database.NamedDatabaseId;

public class ConnectRandomlyToServerGroupImpl {

  private final List<String> groups;
  private final TopologyService topologyService;
  private final MemberId myself;
  private final Random random = new Random();

  ConnectRandomlyToServerGroupImpl(List<String> groups, TopologyService topologyService,
      MemberId myself) {
    this.groups = groups;
    this.topologyService = topologyService;
    this.myself = myself;
  }

  public Optional<MemberId> upstreamMemberForDatabase(NamedDatabaseId namedDatabaseId) {
    Map<MemberId, ReadReplicaInfo> replicas = this.topologyService
        .readReplicaTopologyForDatabase(namedDatabaseId).members();
    List<MemberId> choices = this.groups.stream().flatMap((group) ->
    {
      return replicas.entrySet().stream().filter(this.isMyGroupAndNotMe(group));
    }).map(Entry::getKey).collect(Collectors.toList());
    return choices.isEmpty() ? Optional.empty()
        : Optional.of(choices.get(this.random.nextInt(choices.size())));
  }

  private Predicate<Entry<MemberId, ReadReplicaInfo>> isMyGroupAndNotMe(String group) {
    return (entry) ->
    {
      return entry.getValue().groups().contains(group) && !entry.getKey().equals(this.myself);
    };
  }
}
