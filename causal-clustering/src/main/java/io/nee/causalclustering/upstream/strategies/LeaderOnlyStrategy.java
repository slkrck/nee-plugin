/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.upstream.strategies;

import io.nee.causalclustering.discovery.RoleInfo;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.upstream.UpstreamDatabaseSelectionException;
import io.nee.causalclustering.upstream.UpstreamDatabaseSelectionStrategy;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import org.neo4j.kernel.database.NamedDatabaseId;

public class LeaderOnlyStrategy extends UpstreamDatabaseSelectionStrategy {

  public static final String IDENTITY = "leader-only";

  public LeaderOnlyStrategy() {
    super("leader-only");
  }

  public Optional<MemberId> upstreamMemberForDatabase(NamedDatabaseId namedDatabaseId)
      throws UpstreamDatabaseSelectionException {
    Set<MemberId> coreMemberIds = this.topologyService.coreTopologyForDatabase(namedDatabaseId)
        .members().keySet();
    if (coreMemberIds.isEmpty()) {
      throw new UpstreamDatabaseSelectionException("No core servers available");
    } else {
      Iterator n3 = coreMemberIds.iterator();

      MemberId memberId;
      RoleInfo role;
      do {
        if (!n3.hasNext()) {
          return Optional.empty();
        }

        memberId = (MemberId) n3.next();
        role = this.topologyService.lookupRole(namedDatabaseId, memberId);
      }
      while (role != RoleInfo.LEADER || Objects.equals(this.myself, memberId));

      return Optional.of(memberId);
    }
  }
}
