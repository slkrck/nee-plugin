/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.upstream;

import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.identity.MemberId;
import java.util.Optional;
import org.neo4j.annotations.service.Service;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.service.NamedService;

@Service
public abstract class UpstreamDatabaseSelectionStrategy implements NamedService {

  protected TopologyService topologyService;
  protected Config config;
  protected Log log;
  protected MemberId myself;
  protected String name;

  protected UpstreamDatabaseSelectionStrategy(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void inject(TopologyService topologyService, Config config, LogProvider logProvider,
      MemberId myself) {
    this.topologyService = topologyService;
    this.config = config;
    this.log = logProvider.getLog(this.getClass());
    this.myself = myself;
    this.log.info("Using upstream selection strategy " + this.name);
    this.init();
  }

  public void init() {
  }

  public abstract Optional<MemberId> upstreamMemberForDatabase(NamedDatabaseId n1)
      throws UpstreamDatabaseSelectionException;

  public String toString() {
    return this.name;
  }
}
