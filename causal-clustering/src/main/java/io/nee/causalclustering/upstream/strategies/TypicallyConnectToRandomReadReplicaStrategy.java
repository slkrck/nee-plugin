/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.upstream.strategies;

import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.upstream.UpstreamDatabaseSelectionStrategy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.kernel.database.NamedDatabaseId;

public class TypicallyConnectToRandomReadReplicaStrategy extends UpstreamDatabaseSelectionStrategy {

  public static final String IDENTITY = "typically-connect-to-random-read-replica";
  private final TypicallyConnectToRandomReadReplicaStrategy.ModuloCounter counter;

  public TypicallyConnectToRandomReadReplicaStrategy() {
    this(10);
  }

  public TypicallyConnectToRandomReadReplicaStrategy(int connectToCoreInterval) {
    super("typically-connect-to-random-read-replica");
    this.counter = new TypicallyConnectToRandomReadReplicaStrategy.ModuloCounter(
        connectToCoreInterval);
  }

  public Optional<MemberId> upstreamMemberForDatabase(NamedDatabaseId namedDatabaseId) {
    if (this.counter.shouldReturnCoreMemberId()) {
      return this.randomCoreMember(namedDatabaseId);
    } else {
      List<MemberId> readReplicaMembers = new ArrayList(
          this.topologyService.readReplicaTopologyForDatabase(namedDatabaseId).members().keySet());
      Collections.shuffle(readReplicaMembers);
      List<MemberId> coreMembers = new ArrayList(
          this.topologyService.coreTopologyForDatabase(namedDatabaseId).members().keySet());
      Collections.shuffle(coreMembers);
      Stream n10000 = Stream.concat(readReplicaMembers.stream(), coreMembers.stream());
      MemberId n10001 = this.myself;
      Objects.requireNonNull(n10001);
      return n10000.filter(Predicate.not(n10001::equals)).findFirst();
    }
  }

  private Optional<MemberId> randomCoreMember(NamedDatabaseId namedDatabaseId) {
    Stream n10000 = this.topologyService.coreTopologyForDatabase(namedDatabaseId).members().keySet()
        .stream();
    MemberId n10001 = this.myself;
    Objects.requireNonNull(n10001);
    List<MemberId> coreMembersNotSelf = (List) n10000.filter(Predicate.not(n10001::equals))
        .collect(Collectors.toList());
    if (coreMembersNotSelf.isEmpty()) {
      return Optional.empty();
    } else {
      int randomIndex = ThreadLocalRandom.current().nextInt(coreMembersNotSelf.size());
      return Optional.of(coreMembersNotSelf.get(randomIndex));
    }
  }

  private static class ModuloCounter {

    private final int modulo;
    private int counter;

    ModuloCounter(int modulo) {
      this.modulo = modulo;
    }

    boolean shouldReturnCoreMemberId() {
      this.counter = (this.counter + 1) % this.modulo;
      return this.counter == 0;
    }
  }
}
