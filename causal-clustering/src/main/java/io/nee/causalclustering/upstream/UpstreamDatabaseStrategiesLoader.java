/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.upstream;

import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.identity.MemberId;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.eclipse.collections.impl.block.factory.Predicates;
import org.neo4j.configuration.Config;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.service.Services;

public class UpstreamDatabaseStrategiesLoader implements
    Iterable<UpstreamDatabaseSelectionStrategy> {

  private final TopologyService topologyService;
  private final Config config;
  private final MemberId myself;
  private final Log log;
  private final LogProvider logProvider;

  public UpstreamDatabaseStrategiesLoader(TopologyService topologyService, Config config,
      MemberId myself, LogProvider logProvider) {
    this.topologyService = topologyService;
    this.config = config;
    this.myself = myself;
    this.log = logProvider.getLog(this.getClass());
    this.logProvider = logProvider;
  }

  private static String nicelyCommaSeparatedList(
      Collection<UpstreamDatabaseSelectionStrategy> items) {
    return items.stream().map(UpstreamDatabaseSelectionStrategy::toString)
        .collect(Collectors.joining(", "));
  }

  public Iterator<UpstreamDatabaseSelectionStrategy> iterator() {
    List<String> configuredNames = this.config
        .get(CausalClusteringSettings.upstream_selection_strategy);
    Map<String, UpstreamDatabaseSelectionStrategy> availableStrategies = Services
        .loadAll(UpstreamDatabaseSelectionStrategy.class).stream().collect(
            Collectors.toMap(UpstreamDatabaseSelectionStrategy::getName, Function.identity()));
    Stream n10000 = configuredNames.stream().distinct();
    Objects.requireNonNull(availableStrategies);
    List<UpstreamDatabaseSelectionStrategy> strategies =
        (List) n10000.map(availableStrategies::get).filter(Predicates.notNull())
            .collect(Collectors.toList());
    strategies.forEach((strategy) ->
    {
      strategy.inject(this.topologyService, this.config, this.logProvider, this.myself);
    });
    this.log.debug(
        "Upstream database strategies loaded in order of precedence: " + nicelyCommaSeparatedList(
            strategies));
    return strategies.iterator();
  }
}
