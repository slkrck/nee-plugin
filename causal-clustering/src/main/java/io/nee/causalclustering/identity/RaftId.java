/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.identity;

import io.nee.causalclustering.core.state.storage.SafeStateMarshal;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.kernel.database.DatabaseId;
import org.neo4j.util.Id;

public final class RaftId {

  private final Id id;

  RaftId(UUID uuid) {
    this.id = new Id(uuid);
  }

  public static RaftId from(DatabaseId databaseId) {
    return new RaftId(databaseId.uuid());
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      RaftId raftId = (RaftId) o;
      return Objects.equals(this.id, raftId.id);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.id);
  }

  public UUID uuid() {
    return this.id.uuid();
  }

  public String toString() {
    return "RaftId{" + this.id + "}";
  }

  public static class Marshal extends SafeStateMarshal<RaftId> {

    public static final RaftId.Marshal INSTANCE = new RaftId.Marshal();
    private static final UUID NIL = new UUID(0L, 0L);

    public void marshal(RaftId raftId, WritableChannel channel) throws IOException {
      UUID uuid = raftId == null ? NIL : raftId.uuid();
      channel.putLong(uuid.getMostSignificantBits());
      channel.putLong(uuid.getLeastSignificantBits());
    }

    public RaftId unmarshal0(ReadableChannel channel) throws IOException {
      long mostSigBits = channel.getLong();
      long leastSigBits = channel.getLong();
      UUID uuid = new UUID(mostSigBits, leastSigBits);
      return uuid.equals(NIL) ? null : new RaftId(uuid);
    }

    public RaftId startState() {
      return null;
    }

    public long ordinal(RaftId raftId) {
      return raftId == null ? 0L : 1L;
    }
  }
}
