/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.identity;

import io.nee.causalclustering.core.state.storage.SafeStateMarshal;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;
import org.neo4j.io.fs.ReadableChannel;
import org.neo4j.io.fs.WritableChannel;
import org.neo4j.util.Id;

public class MemberId {

  private final Id id;

  public MemberId(UUID uuid) {
    this.id = new Id(uuid);
  }

  public UUID getUuid() {
    return this.id.uuid();
  }

  public String toString() {
    return String.format("MemberId{%s}", this.id);
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      MemberId that = (MemberId) o;
      return Objects.equals(this.id, that.id);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hashCode(this.id);
  }

  public static class Marshal extends SafeStateMarshal<MemberId> {

    public static final MemberId.Marshal INSTANCE = new MemberId.Marshal();

    public void marshal(MemberId memberId, WritableChannel channel) throws IOException {
      if (memberId == null) {
        channel.put((byte) 0);
      } else {
        channel.put((byte) 1);
        channel.putLong(memberId.getUuid().getMostSignificantBits());
        channel.putLong(memberId.getUuid().getLeastSignificantBits());
      }
    }

    public MemberId unmarshal0(ReadableChannel channel) throws IOException {
      byte nullMarker = channel.get();
      if (nullMarker == 0) {
        return null;
      } else {
        long mostSigBits = channel.getLong();
        long leastSigBits = channel.getLong();
        return new MemberId(new UUID(mostSigBits, leastSigBits));
      }
    }

    public MemberId startState() {
      return null;
    }

    public long ordinal(MemberId memberId) {
      return memberId == null ? 0L : 1L;
    }
  }
}
