/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.monitoring;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.function.Supplier;
import org.neo4j.util.Preconditions;

class QualitySampler<T> {

  private static final int DEFAULT_MAXIMUM_ATTEMPTS = 16;
  private final Clock clock;
  private final Duration samplingTolerance;
  private final int maximumAttempts;
  private final Supplier<T> sampler;

  QualitySampler(Clock clock, Duration samplingTolerance, Supplier<T> sampler) {
    this(clock, samplingTolerance, 16, sampler);
  }

  QualitySampler(Clock clock, Duration samplingTolerance, int maximumAttempts,
      Supplier<T> sampler) {
    Preconditions.requirePositive(maximumAttempts);
    this.clock = clock;
    this.samplingTolerance = samplingTolerance;
    this.maximumAttempts = maximumAttempts;
    this.sampler = sampler;
  }

  public Optional<Sample<T>> sample() {
    int attempt = 0;

    Sample sample;
    for (sample = null; sample == null && attempt < this.maximumAttempts; ++attempt) {
      sample = this.sample0();
    }

    return Optional.ofNullable(sample);
  }

  private Sample<T> sample0() {
    Instant before = this.clock.instant();
    T sample = this.sampler.get();
    Instant after = this.clock.instant();
    Duration samplingDuration = Duration.between(before, after);
    if (samplingDuration.compareTo(this.samplingTolerance) > 0) {
      return null;
    } else {
      Instant sampleTime = before.plus(samplingDuration.dividedBy(2L));
      return new Sample(sampleTime, sample);
    }
  }
}
