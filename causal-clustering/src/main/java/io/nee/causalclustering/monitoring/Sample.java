/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.monitoring;

import java.time.Instant;
import java.util.Objects;

class Sample<T> {

  private final Instant instant;
  private final T value;

  Sample(Instant instant, T value) {
    this.instant = instant;
    this.value = value;
  }

  Instant instant() {
    return this.instant;
  }

  T value() {
    return this.value;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      Sample<?> sample = (Sample) o;
      return Objects.equals(this.instant, sample.instant) && Objects
          .equals(this.value, sample.value);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.instant, this.value);
  }

  public String toString() {
    return "Sample{instant=" + this.instant + ", value=" + this.value + "}";
  }
}
