/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.logging;

import io.nee.causalclustering.core.consensus.RaftMessages;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.neo4j.io.IOUtils;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.util.Preconditions;
import org.neo4j.util.VisibleForTesting;

public class BetterRaftMessageLogger<MEMBER> extends LifecycleAdapter implements
    RaftMessageLogger<MEMBER> {

  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter
      .ofPattern("yyyy-MM-dd HH:mm:ss.SSSZ");
  private final MEMBER me;
  private final File logFile;
  private final FileSystemAbstraction fs;
  private final Clock clock;
  private final ReadWriteLock lifecycleLock = new ReentrantReadWriteLock();
  private PrintWriter printWriter;

  public BetterRaftMessageLogger(MEMBER me, File logFile, FileSystemAbstraction fs, Clock clock) {
    this.me = me;
    this.logFile = logFile;
    this.fs = fs;
    this.clock = clock;
  }

  private static String nullSafeMessageType(RaftMessages.RaftMessage message) {
    return Objects.isNull(message) ? "null" : message.type().toString();
  }

  public void start() throws IOException {
    this.lifecycleLock.writeLock().lock();

    try {
      Preconditions.checkState(this.printWriter == null, "Already started");
      this.printWriter = this.openPrintWriter();
      this.log(this.me, BetterRaftMessageLogger.Direction.INFO, this.me, "Info", "I am " + this.me);
    } finally {
      this.lifecycleLock.writeLock().unlock();
    }
  }

  public void stop() {
    this.lifecycleLock.writeLock().lock();

    try {
      IOUtils.closeAllSilently(this.printWriter);
      this.printWriter = null;
    } finally {
      this.lifecycleLock.writeLock().unlock();
    }
  }

  public void logOutbound(MEMBER me, RaftMessages.RaftMessage message, MEMBER remote) {
    this.log(me, BetterRaftMessageLogger.Direction.OUTBOUND, remote, nullSafeMessageType(message),
        String.valueOf(message));
  }

  public void logInbound(MEMBER me, RaftMessages.RaftMessage message, MEMBER remote) {
    this.log(me, BetterRaftMessageLogger.Direction.INBOUND, remote, nullSafeMessageType(message),
        String.valueOf(message));
  }

  @VisibleForTesting
  protected PrintWriter openPrintWriter() throws IOException {
    this.fs.mkdirs(this.logFile.getParentFile());
    return new PrintWriter(this.fs.openAsOutputStream(this.logFile, true));
  }

  private void log(MEMBER me, BetterRaftMessageLogger.Direction direction, MEMBER remote,
      String type, String message) {
    this.lifecycleLock.readLock().lock();

    try {
      if (this.printWriter != null) {
        String timestamp = ZonedDateTime.now(this.clock).format(DATE_TIME_FORMATTER);
        this.printWriter.println(String
            .format("%s %s %s %s %s \"%s\"", timestamp, me, direction.arrow, remote, type,
                message));
        this.printWriter.flush();
      }
    } finally {
      this.lifecycleLock.readLock().unlock();
    }
  }

  private enum Direction {
    INFO("---"),
    OUTBOUND("-->"),
    INBOUND("<--");

    public final String arrow;

    Direction(String arrow) {
      this.arrow = arrow;
    }
  }
}
