/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import io.nee.causalclustering.catchup.tx.PullRequestMonitor;
import io.nee.causalclustering.core.state.machines.CommandIndexTracker;
import io.nee.causalclustering.core.state.machines.tx.LogIndexTxHeaderEncoding;
import io.nee.dbms.ReplicatedDatabaseEventService;
import java.util.Objects;
import java.util.function.Supplier;
import org.neo4j.io.pagecache.tracing.cursor.PageCursorTracerSupplier;
import org.neo4j.io.pagecache.tracing.cursor.context.VersionContextSupplier;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionQueue;
import org.neo4j.kernel.impl.api.TransactionToApply;
import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.kernel.impl.transaction.tracing.CommitEvent;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.TransactionApplicationMode;
import org.neo4j.storageengine.api.TransactionIdStore;

public class BatchingTxApplier extends LifecycleAdapter {

  private final int maxBatchSize;
  private final Supplier<TransactionIdStore> txIdStoreSupplier;
  private final Supplier<TransactionCommitProcess> commitProcessSupplier;
  private final VersionContextSupplier versionContextSupplier;
  private final PullRequestMonitor monitor;
  private final PageCursorTracerSupplier pageCursorTracerSupplier;
  private final CommandIndexTracker commandIndexTracker;
  private final Log log;
  private final ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch;
  private TransactionQueue txQueue;
  private TransactionCommitProcess commitProcess;
  private volatile long lastQueuedTxId;
  private volatile boolean stopped;

  BatchingTxApplier(int maxBatchSize, Supplier<TransactionIdStore> txIdStoreSupplier,
      Supplier<TransactionCommitProcess> commitProcessSupplier,
      Monitors monitors, PageCursorTracerSupplier pageCursorTracerSupplier,
      VersionContextSupplier versionContextSupplier,
      CommandIndexTracker commandIndexTracker, LogProvider logProvider,
      ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch) {
    this.maxBatchSize = maxBatchSize;
    this.txIdStoreSupplier = txIdStoreSupplier;
    this.commitProcessSupplier = commitProcessSupplier;
    this.pageCursorTracerSupplier = pageCursorTracerSupplier;
    this.log = logProvider.getLog(this.getClass());
    this.monitor = monitors.newMonitor(PullRequestMonitor.class, new String[0]);
    this.versionContextSupplier = versionContextSupplier;
    this.commandIndexTracker = commandIndexTracker;
    this.databaseEventDispatch = databaseEventDispatch;
  }

  public void start() {
    this.stopped = false;
    this.refreshFromNewStore();
    this.txQueue = new TransactionQueue(this.maxBatchSize, (first, last) ->
    {
      this.commitProcess.commit(first, CommitEvent.NULL, TransactionApplicationMode.EXTERNAL);
      this.pageCursorTracerSupplier.get().reportEvents();
      long lastAppliedRaftLogIndex = LogIndexTxHeaderEncoding
          .decodeLogIndexFromTxHeader(last.transactionRepresentation().additionalHeader());
      this.commandIndexTracker.setAppliedCommandIndex(lastAppliedRaftLogIndex);
    });
  }

  public void stop() {
    this.stopped = true;
  }

  void refreshFromNewStore() {
    assert this.txQueue == null || this.txQueue.isEmpty();

    this.lastQueuedTxId = this.txIdStoreSupplier.get().getLastCommittedTransactionId();

    assert this.lastQueuedTxId == this.txIdStoreSupplier.get().getLastClosedTransactionId();

    this.commitProcess = this.commitProcessSupplier.get();
  }

  public void queue(CommittedTransactionRepresentation tx) throws Exception {
    long receivedTxId = tx.getCommitEntry().getTxId();
    long expectedTxId = this.lastQueuedTxId + 1L;
    if (receivedTxId != expectedTxId) {
      this.log
          .warn("Out of order transaction. Received: %d Expected: %d", receivedTxId, expectedTxId);
    } else {
      TransactionToApply toApply =
          new TransactionToApply(tx.getTransactionRepresentation(), receivedTxId,
              this.versionContextSupplier.getVersionContext());
      ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch n10001 = this.databaseEventDispatch;
      Objects.requireNonNull(n10001);
      toApply.onClose(n10001::fireTransactionCommitted);
      this.txQueue.queue(toApply);
      if (!this.stopped) {
        this.lastQueuedTxId = receivedTxId;
        this.monitor.txPullResponse(receivedTxId);
      }
    }
  }

  void applyBatch() throws Exception {
    this.txQueue.empty();
  }

  long lastQueuedTxId() {
    return this.lastQueuedTxId;
  }
}
