/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.catchup.CatchupAddressResolutionException;
import io.nee.causalclustering.catchup.CatchupClientFactory;
import io.nee.causalclustering.catchup.CatchupResponseAdaptor;
import io.nee.causalclustering.catchup.storecopy.DatabaseShutdownException;
import io.nee.causalclustering.catchup.storecopy.StoreCopyFailedException;
import io.nee.causalclustering.catchup.storecopy.StoreCopyProcess;
import io.nee.causalclustering.catchup.tx.PullRequestMonitor;
import io.nee.causalclustering.catchup.tx.TxPullResponse;
import io.nee.causalclustering.catchup.tx.TxStreamFinishedResponse;
import io.nee.causalclustering.error_handling.DatabasePanicker;
import io.nee.dbms.ClusterInternalDbmsOperator;
import io.nee.dbms.ReplicatedDatabaseEventService;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.Executor;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.kernel.impl.transaction.CommittedTransactionRepresentation;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StoreId;
import org.neo4j.util.Preconditions;
import org.neo4j.util.VisibleForTesting;

public class CatchupPollingProcess extends LifecycleAdapter {

  private final ReadReplicaDatabaseContext databaseContext;
  private final CatchupAddressProvider upstreamProvider;
  private final Log log;
  private final StoreCopyProcess storeCopyProcess;
  private final CatchupClientFactory catchUpClient;
  private final DatabasePanicker panicker;
  private final BatchingTxApplier applier;
  private final ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch;
  private final PullRequestMonitor pullRequestMonitor;
  private final Executor executor;
  private volatile CatchupPollingProcess.State state;
  private CompletableFuture<Boolean> upToDateFuture;
  private ClusterInternalDbmsOperator.StoreCopyHandle storeCopyHandle;

  CatchupPollingProcess(Executor executor, ReadReplicaDatabaseContext databaseContext,
      CatchupClientFactory catchUpClient, BatchingTxApplier applier,
      ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch,
      StoreCopyProcess storeCopyProcess,
      LogProvider logProvider,
      DatabasePanicker panicker, CatchupAddressProvider upstreamProvider) {
    this.state = CatchupPollingProcess.State.TX_PULLING;
    this.databaseContext = databaseContext;
    this.upstreamProvider = upstreamProvider;
    this.catchUpClient = catchUpClient;
    this.applier = applier;
    this.databaseEventDispatch = databaseEventDispatch;
    this.pullRequestMonitor = databaseContext.monitors()
        .newMonitor(PullRequestMonitor.class, new String[0]);
    this.storeCopyProcess = storeCopyProcess;
    this.log = logProvider.getLog(this.getClass());
    this.panicker = panicker;
    this.executor = executor;
  }

  public synchronized void start() {
    this.state = CatchupPollingProcess.State.TX_PULLING;
    this.upToDateFuture = new CompletableFuture();
  }

  @VisibleForTesting
  public CompletableFuture<Boolean> upToDateFuture() {
    return this.upToDateFuture;
  }

  public void stop() {
    this.state = CatchupPollingProcess.State.CANCELLED;
  }

  public CatchupPollingProcess.State state() {
    return this.state;
  }

  public CompletableFuture<Void> tick() {
    return this.state != CatchupPollingProcess.State.CANCELLED
        && this.state != CatchupPollingProcess.State.PANIC ? CompletableFuture.runAsync(() ->
        {
          try {
            switch (this.state) {
              case TX_PULLING:
                this.pullTransactions();
                break;
              case STORE_COPYING:
                this.copyStore();
                break;
              default:
                throw new IllegalStateException(
                    "Tried to execute catchup but was in state " +
                        this.state);
            }
          } catch (Throwable n2) {
            throw new CompletionException(
                n2);
          }
        },
        this.executor)
        .exceptionally(
            (e) ->
            {
              this.panic(
                  e);
              return null;
            })
        : CompletableFuture
            .completedFuture(null);
  }

  private synchronized void panic(Throwable e) {
    this.upToDateFuture.completeExceptionally(e);
    this.state = CatchupPollingProcess.State.PANIC;
    this.panicker.panic(e);
  }

  private void pullTransactions() {
    SocketAddress address;
    try {
      address = this.upstreamProvider.primary(this.databaseContext.databaseId());
    } catch (CatchupAddressResolutionException n3) {
      this.log.warn("Could not find upstream database from which to pull. [Message: %s].",
          n3.getMessage());
      return;
    }

    this.pullAndApplyBatchOfTransactions(address, this.databaseContext.storeId());
  }

  private synchronized void handleTransaction(CommittedTransactionRepresentation tx) {
    if (this.state != CatchupPollingProcess.State.PANIC) {
      try {
        this.applier.queue(tx);
      } catch (Throwable n3) {
        this.panic(n3);
      }
    }
  }

  private synchronized void streamComplete() {
    if (this.state != CatchupPollingProcess.State.PANIC) {
      try {
        this.applier.applyBatch();
      } catch (Throwable n2) {
        this.panic(n2);
      }
    }
  }

  private void pullAndApplyBatchOfTransactions(SocketAddress address, StoreId localStoreId) {
    long lastQueuedTxId = this.applier.lastQueuedTxId();
    this.pullRequestMonitor.txPullRequest(lastQueuedTxId);
    this.log.debug("Pull transactions from %s where tx id > %d", address, lastQueuedTxId);
    CatchupResponseAdaptor responseHandler = new CatchupResponseAdaptor<TxStreamFinishedResponse>() {
      public void onTxPullResponse(CompletableFuture<TxStreamFinishedResponse> signal,
          TxPullResponse response) {
        CatchupPollingProcess.this.handleTransaction(response.tx());
      }

      public void onTxStreamFinishedResponse(CompletableFuture<TxStreamFinishedResponse> signal,
          TxStreamFinishedResponse response) {
        CatchupPollingProcess.this.streamComplete();
        signal.complete(response);
      }
    };

    TxStreamFinishedResponse result;
    try {
      result = (TxStreamFinishedResponse) this.catchUpClient.getClient(address, this.log).v3((c) ->
      {
        return c.pullTransactions(localStoreId,
            lastQueuedTxId,
            this.databaseContext
                .databaseId());
      }).withResponseHandler(responseHandler).request();
    } catch (Exception n8) {
      this.log.warn("Exception occurred while pulling transactions. Will retry shortly.", n8);
      this.streamComplete();
      return;
    }

    switch (result.status()) {
      case SUCCESS_END_OF_STREAM:
        this.log.debug("Successfully pulled transactions from tx id %d", lastQueuedTxId);
        this.upToDateFuture.complete(Boolean.TRUE);
        break;
      case E_TRANSACTION_PRUNED:
        this.log.info(
            "Tx pull unable to get transactions starting from %d since transactions have been pruned. Attempting a store copy.",
            lastQueuedTxId);
        this.transitionToStoreCopy();
        break;
      default:
        this.log.info("Tx pull request unable to get transactions > %d ", lastQueuedTxId);
    }
  }

  private void transitionToStoreCopy() {
    this.state = CatchupPollingProcess.State.STORE_COPYING;
  }

  private void transitionToTxPulling() {
    this.state = CatchupPollingProcess.State.TX_PULLING;
  }

  private void copyStore() {
    try {
      this.ensureKernelStopped();
      this.storeCopyProcess
          .replaceWithStoreFrom(this.upstreamProvider, this.databaseContext.storeId());
      if (this.restartDatabaseAfterStoreCopy()) {
        this.transitionToTxPulling();
        this.databaseEventDispatch.fireStoreReplaced(this.applier.lastQueuedTxId());
      }
    } catch (StoreCopyFailedException | IOException n2) {
      this.log.warn("Error copying store. Will retry shortly.", n2);
    } catch (DatabaseShutdownException n3) {
      this.log.warn("Store copy aborted due to shutdown.", n3);
    }
  }

  private void ensureKernelStopped() {
    if (this.storeCopyHandle == null) {
      this.log.info("Stopping kernel for store copy");
      this.storeCopyHandle = this.databaseContext.stopForStoreCopy();
    } else {
      this.log.info("Kernel still stopped for store copy");
    }
  }

  private boolean restartDatabaseAfterStoreCopy() {
    Preconditions.checkState(this.storeCopyHandle != null, "Store copy handle not initialized");
    this.log.info("Attempting kernel start after store copy");
    ClusterInternalDbmsOperator.StoreCopyHandle handle = this.storeCopyHandle;
    this.storeCopyHandle = null;
    boolean triggeredReconciler = handle.release();
    if (!triggeredReconciler) {
      this.log.warn("Reconciler could not be triggered at this time.");
      return false;
    } else if (!this.databaseContext.kernelDatabase().isStarted()) {
      this.log.warn(
          "Kernel did not start properly after the store copy. This might be because of unexpected errors or normal early-exit paths.");
      return false;
    } else {
      this.log.info("Kernel started after store copy");
      this.applier.refreshFromNewStore();
      return true;
    }
  }

  enum State {
    TX_PULLING,
    STORE_COPYING,
    PANIC,
    CANCELLED
  }
}
