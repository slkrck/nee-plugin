/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.catchup.CatchupClientFactory;
import io.nee.causalclustering.catchup.CatchupComponentsRepository;
import io.nee.causalclustering.core.CausalClusteringSettings;
import io.nee.causalclustering.core.consensus.schedule.TimeoutFactory;
import io.nee.causalclustering.core.consensus.schedule.Timer;
import io.nee.causalclustering.core.consensus.schedule.TimerService;
import io.nee.causalclustering.core.state.machines.CommandIndexTracker;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.error_handling.DatabasePanicker;
import io.nee.causalclustering.upstream.UpstreamDatabaseStrategySelector;
import io.nee.dbms.ReplicatedDatabaseEventService;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import org.neo4j.configuration.Config;
import org.neo4j.io.pagecache.tracing.cursor.PageCursorTracerSupplier;
import org.neo4j.kernel.impl.api.TransactionCommitProcess;
import org.neo4j.kernel.impl.api.TransactionRepresentationCommitProcess;
import org.neo4j.kernel.impl.transaction.log.TransactionAppender;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.kernel.lifecycle.SafeLifecycle;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.storageengine.api.StorageEngine;
import org.neo4j.storageengine.api.TransactionIdStore;
import org.neo4j.util.VisibleForTesting;

public class CatchupProcessManager extends SafeLifecycle {

  private final TopologyService topologyService;
  private final CatchupClientFactory catchupClient;
  private final UpstreamDatabaseStrategySelector selectionStrategyPipeline;
  private final TimerService timerService;
  private final long txPullIntervalMillis;
  private final CommandIndexTracker commandIndexTracker;
  private final PageCursorTracerSupplier pageCursorTracerSupplier;
  private final Executor executor;
  private final ReadReplicaDatabaseContext databaseContext;
  private final LogProvider logProvider;
  private final Log log;
  private final Config config;
  private final CatchupComponentsRepository catchupComponents;
  private final DatabasePanicker panicker;
  private final ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch;
  private CatchupPollingProcess catchupProcess;
  private LifeSupport txPulling;
  private volatile boolean isPanicked;
  private Timer timer;

  CatchupProcessManager(Executor executor, CatchupComponentsRepository catchupComponents,
      ReadReplicaDatabaseContext databaseContext,
      DatabasePanicker panicker, TopologyService topologyService,
      CatchupClientFactory catchUpClient,
      UpstreamDatabaseStrategySelector selectionStrategyPipeline, TimerService timerService,
      CommandIndexTracker commandIndexTracker,
      LogProvider logProvider, PageCursorTracerSupplier pageCursorTracerSupplier, Config config,
      ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch databaseEventDispatch) {
    this.logProvider = logProvider;
    this.log = logProvider.getLog(this.getClass());
    this.pageCursorTracerSupplier = pageCursorTracerSupplier;
    this.config = config;
    this.commandIndexTracker = commandIndexTracker;
    this.timerService = timerService;
    this.executor = executor;
    this.catchupComponents = catchupComponents;
    this.databaseContext = databaseContext;
    this.panicker = panicker;
    this.topologyService = topologyService;
    this.catchupClient = catchUpClient;
    this.selectionStrategyPipeline = selectionStrategyPipeline;
    this.txPullIntervalMillis = config.get(CausalClusteringSettings.pull_interval).toMillis();
    this.databaseEventDispatch = databaseEventDispatch;
    this.txPulling = new LifeSupport();
    this.isPanicked = false;
  }

  public void start0() {
    this.log.info("Starting " + this.getClass().getSimpleName());
    this.txPulling = new LifeSupport();
    this.catchupProcess = this.createCatchupProcess(this.databaseContext);
    this.txPulling.start();
    this.initTimer();
  }

  public void stop0() {
    this.log.info("Shutting down " + this.getClass().getSimpleName());
    this.timer.kill(Timer.CancelMode.SYNC_WAIT);
    this.txPulling.stop();
  }

  synchronized CompletableFuture<Void> panic(Throwable e) {
    this.log
        .error("Unexpected issue in catchup process. No more catchup requests will be scheduled.",
            e);
    this.isPanicked = true;
    return this.panicker.panicAsync(e);
  }

  private CatchupPollingProcess createCatchupProcess(ReadReplicaDatabaseContext databaseContext) {
    CatchupComponentsRepository.CatchupComponents dbCatchupComponents =
        this.catchupComponents.componentsFor(databaseContext.databaseId()).orElseThrow(() ->
        {
          return new IllegalArgumentException(
              String.format(
                  "No StoreCopyProcess instance exists for database %s.",
                  databaseContext
                      .databaseId()));
        });
    Supplier<TransactionCommitProcess> writableCommitProcess = () ->
    {
      return new TransactionRepresentationCommitProcess(
          databaseContext.kernelDatabase().getDependencyResolver()
              .resolveDependency(TransactionAppender.class),
          databaseContext.kernelDatabase().getDependencyResolver()
              .resolveDependency(StorageEngine.class));
    };
    int maxBatchSize = this.config
        .get(CausalClusteringSettings.read_replica_transaction_applier_batch_size);
    BatchingTxApplier batchingTxApplier = new BatchingTxApplier(maxBatchSize, () ->
    {
      return databaseContext.kernelDatabase().getDependencyResolver()
          .resolveDependency(TransactionIdStore.class);
    }, writableCommitProcess, databaseContext.monitors(), this.pageCursorTracerSupplier,
        databaseContext.kernelDatabase().getVersionContextSupplier(),
        this.commandIndexTracker, this.logProvider, this.databaseEventDispatch);
    CatchupPollingProcess catchupProcess =
        new CatchupPollingProcess(this.executor, databaseContext, this.catchupClient,
            batchingTxApplier, this.databaseEventDispatch,
            dbCatchupComponents.storeCopyProcess(), this.logProvider, this::panic,
            new CatchupAddressProvider.UpstreamStrategyBasedAddressProvider(this.topologyService,
                this.selectionStrategyPipeline));
    this.txPulling.add(batchingTxApplier);
    this.txPulling.add(catchupProcess);
    return catchupProcess;
  }

  private void onTimeout() throws Exception {
    this.catchupProcess.tick().get();
    if (!this.isPanicked) {
      this.timer.reset();
    }
  }

  @VisibleForTesting
  public CatchupPollingProcess getCatchupProcess() {
    return this.catchupProcess;
  }

  @VisibleForTesting
  void setCatchupProcess(CatchupPollingProcess catchupProcess) {
    this.catchupProcess = catchupProcess;
  }

  void initTimer() {
    this.timer = this.timerService
        .create(CatchupProcessManager.Timers.TX_PULLER_TIMER, Group.PULL_UPDATES, (timeout) ->
        {
          this.onTimeout();
        });
    this.timer.set(TimeoutFactory.fixedTimeout(this.txPullIntervalMillis, TimeUnit.MILLISECONDS));
  }

  public enum Timers implements TimerService.TimerName {
    TX_PULLER_TIMER
  }
}
