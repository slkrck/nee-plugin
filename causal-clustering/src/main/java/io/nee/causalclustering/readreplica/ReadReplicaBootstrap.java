/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import io.nee.causalclustering.catchup.CatchupAddressProvider;
import io.nee.causalclustering.catchup.CatchupComponentsRepository;
import io.nee.causalclustering.catchup.storecopy.DatabaseShutdownException;
import io.nee.causalclustering.catchup.storecopy.RemoteStore;
import io.nee.causalclustering.catchup.storecopy.StoreCopyFailedException;
import io.nee.causalclustering.catchup.storecopy.StoreIdDownloadFailedException;
import io.nee.causalclustering.core.state.snapshot.TopologyLookupException;
import io.nee.causalclustering.discovery.TopologyService;
import io.nee.causalclustering.identity.MemberId;
import io.nee.causalclustering.upstream.UpstreamDatabaseSelectionException;
import io.nee.causalclustering.upstream.UpstreamDatabaseStrategySelector;
import io.nee.dbms.ClusterInternalDbmsOperator;
import io.nee.dbms.DatabaseStartAborter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import org.neo4j.configuration.helpers.SocketAddress;
import org.neo4j.dbms.database.DatabaseStartAbortedException;
import org.neo4j.internal.helpers.ExponentialBackoffStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.StoreId;

class ReadReplicaBootstrap {

  private final Log debugLog;
  private final Log userLog;
  private final TimeoutStrategy syncRetryStrategy;
  private final UpstreamDatabaseStrategySelector selectionStrategy;
  private final DatabaseStartAborter databaseStartAborter;
  private final ClusterInternalDbmsOperator internalOperator;
  private final TopologyService topologyService;
  private final Supplier<CatchupComponentsRepository.CatchupComponents> catchupComponentsSupplier;
  private final ReadReplicaDatabaseContext databaseContext;

  ReadReplicaBootstrap(ReadReplicaDatabaseContext databaseContext,
      UpstreamDatabaseStrategySelector selectionStrategy, LogProvider debugLogProvider,
      LogProvider userLogProvider, TopologyService topologyService,
      Supplier<CatchupComponentsRepository.CatchupComponents> catchupComponentsSupplier,
      ClusterInternalDbmsOperator internalOperator, DatabaseStartAborter databaseStartAborter) {
    this.databaseContext = databaseContext;
    this.catchupComponentsSupplier = catchupComponentsSupplier;
    this.selectionStrategy = selectionStrategy;
    this.databaseStartAborter = databaseStartAborter;
    this.syncRetryStrategy = new ExponentialBackoffStrategy(1L, 30L, TimeUnit.SECONDS);
    this.debugLog = debugLogProvider.getLog(this.getClass());
    this.userLog = userLogProvider.getLog(this.getClass());
    this.topologyService = topologyService;
    this.internalOperator = internalOperator;
  }

  public void perform() throws Exception {
    ClusterInternalDbmsOperator.BootstrappingHandle bootstrapHandle = this.internalOperator
        .bootstrap(this.databaseContext.databaseId());
    boolean shouldAbort = false;

    try {
      Timeout syncRetryWaitPeriod = this.syncRetryStrategy.newTimeout();
      boolean synced = false;

      while (!synced && !shouldAbort) {
        try {
          this.debugLog.info("Syncing db: %s", this.databaseContext.databaseId());
          synced = this.doSyncStoreCopyWithUpstream(this.databaseContext);
          if (synced) {
            this.debugLog.info("Successfully synced db: %s", this.databaseContext.databaseId());
          } else {
            Thread.sleep(syncRetryWaitPeriod.getMillis());
            syncRetryWaitPeriod.increment();
          }

          shouldAbort = this.databaseStartAborter.shouldAbort(this.databaseContext.databaseId());
        } catch (InterruptedException n10) {
          Thread.currentThread().interrupt();
          this.userLog.error("Interrupted while trying to start read replica");
          throw new RuntimeException(n10);
        } catch (Exception n11) {
          this.debugLog.error("Unexpected error when syncing stores", n11);
          throw new RuntimeException(n11);
        }
      }
    } finally {
      this.databaseStartAborter.started(this.databaseContext.databaseId());
      bootstrapHandle.release();
    }

    if (shouldAbort) {
      throw new DatabaseStartAbortedException(this.databaseContext.databaseId());
    }
  }

  private boolean doSyncStoreCopyWithUpstream(ReadReplicaDatabaseContext databaseContext) {
    MemberId source;
    try {
      source = this.selectionStrategy.bestUpstreamMemberForDatabase(databaseContext.databaseId());
    } catch (UpstreamDatabaseSelectionException n8) {
      this.debugLog.warn(
          "Unable to find upstream member for database " + databaseContext.databaseId().name());
      return false;
    }

    try {
      this.syncStoreWithUpstream(databaseContext, source);
      return true;
    } catch (TopologyLookupException n4) {
      this.debugLog.warn("Unable to get address of %s", source);
      return false;
    } catch (StoreIdDownloadFailedException n5) {
      this.debugLog.warn("Unable to get store ID from %s", source);
      return false;
    } catch (StoreCopyFailedException n6) {
      this.debugLog.warn("Unable to copy store files from %s", source);
      return false;
    } catch (IOException | DatabaseShutdownException n7) {
      this.debugLog
          .warn(String.format("Syncing of stores failed unexpectedly from %s", source), n7);
      return false;
    }
  }

  private void syncStoreWithUpstream(ReadReplicaDatabaseContext databaseContext, MemberId source)
      throws IOException, StoreIdDownloadFailedException, StoreCopyFailedException, TopologyLookupException, DatabaseShutdownException {
    CatchupComponentsRepository.CatchupComponents catchupComponents = this.catchupComponentsSupplier
        .get();
    if (databaseContext.isEmpty()) {
      this.debugLog
          .info("Local database is empty, attempting to replace with copy from upstream server %s",
              source);
      this.debugLog.info("Finding store ID of upstream server %s", source);
      SocketAddress fromAddress = this.topologyService.lookupCatchupAddress(source);
      StoreId storeId = catchupComponents.remoteStore().getStoreId(fromAddress);
      this.debugLog.info("Copying store from upstream server %s", source);
      databaseContext.delete();
      catchupComponents.storeCopyProcess()
          .replaceWithStoreFrom(new CatchupAddressProvider.SingleAddressProvider(fromAddress),
              storeId);
      this.debugLog.info("Restarting local database after copy.", source);
    } else {
      this.ensureStoreIsPresentAt(databaseContext, catchupComponents.remoteStore(), source);
    }
  }

  private void ensureStoreIsPresentAt(ReadReplicaDatabaseContext databaseContext,
      RemoteStore remoteStore, MemberId upstream)
      throws StoreIdDownloadFailedException, TopologyLookupException {
    StoreId localStoreId = databaseContext.storeId();
    SocketAddress advertisedSocketAddress = this.topologyService.lookupCatchupAddress(upstream);
    StoreId remoteStoreId = remoteStore.getStoreId(advertisedSocketAddress);
    if (!localStoreId.equals(remoteStoreId)) {
      throw new IllegalStateException(String.format(
          "This read replica cannot join the cluster. The local version of database %s is not empty and has a mismatching storeId: expected %s actual %s.",
          databaseContext.databaseId().name(), remoteStoreId, localStoreId));
    }
  }
}
