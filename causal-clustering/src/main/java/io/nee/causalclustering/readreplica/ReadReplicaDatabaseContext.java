/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import io.nee.causalclustering.catchup.storecopy.StoreFiles;
import io.nee.dbms.ClusterInternalDbmsOperator;
import java.io.IOException;
import org.neo4j.collection.Dependencies;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.logging.Log;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StoreId;

public class ReadReplicaDatabaseContext {

  private final Database kernelDatabase;
  private final Monitors monitors;
  private final Dependencies dependencies;
  private final StoreFiles storeFiles;
  private final LogFiles transactionLogs;
  private final Log log;
  private final ClusterInternalDbmsOperator internalOperator;

  ReadReplicaDatabaseContext(Database kernelDatabase, Monitors monitors, Dependencies dependencies,
      StoreFiles storeFiles, LogFiles transactionLogs,
      ClusterInternalDbmsOperator internalOperator) {
    this.kernelDatabase = kernelDatabase;
    this.monitors = monitors;
    this.dependencies = dependencies;
    this.storeFiles = storeFiles;
    this.transactionLogs = transactionLogs;
    this.log = kernelDatabase.getInternalLogProvider().getLog(this.getClass());
    this.internalOperator = internalOperator;
  }

  public NamedDatabaseId databaseId() {
    return this.kernelDatabase.getNamedDatabaseId();
  }

  public StoreId storeId() {
    return this.readStoreIdFromDisk();
  }

  private StoreId readStoreIdFromDisk() {
    try {
      return this.storeFiles.readStoreId(this.kernelDatabase.getDatabaseLayout());
    } catch (IOException n2) {
      this.log.warn("Failure reading store id", n2);
      return null;
    }
  }

  ClusterInternalDbmsOperator.StoreCopyHandle stopForStoreCopy() {
    return this.internalOperator.stopForStoreCopy(this.kernelDatabase.getNamedDatabaseId());
  }

  public Database kernelDatabase() {
    return this.kernelDatabase;
  }

  public Monitors monitors() {
    return this.monitors;
  }

  public Dependencies dependencies() {
    return this.dependencies;
  }

  public boolean isEmpty() {
    return this.storeFiles.isEmpty(this.kernelDatabase.getDatabaseLayout());
  }

  public void delete() throws IOException {
    this.storeFiles.delete(this.kernelDatabase.getDatabaseLayout(), this.transactionLogs);
  }
}
