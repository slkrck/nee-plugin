/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import io.nee.causalclustering.common.ClusteringEditionModule;
import io.nee.causalclustering.discovery.DiscoveryServiceFactory;
import io.nee.causalclustering.identity.MemberId;
import java.util.UUID;
import org.neo4j.configuration.Config;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.graphdb.facade.DatabaseManagementServiceFactory;
import org.neo4j.graphdb.facade.ExternalDependencies;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.kernel.impl.factory.DatabaseInfo;

public class ReadReplicaGraphDatabase {

  private final DatabaseManagementService managementService;

  public ReadReplicaGraphDatabase(Config config, ExternalDependencies dependencies,
      DiscoveryServiceFactory discoveryServiceFactory,
      ReadReplicaGraphDatabase.ReadReplicaEditionModuleFactory editionModuleFactory) {
    this(config, dependencies, discoveryServiceFactory, new MemberId(UUID.randomUUID()),
        editionModuleFactory);
  }

  public ReadReplicaGraphDatabase(Config config, ExternalDependencies dependencies,
      DiscoveryServiceFactory discoveryServiceFactory, MemberId memberId,
      ReadReplicaGraphDatabase.ReadReplicaEditionModuleFactory editionModuleFactory) {
    this.managementService = this
        .createManagementService(config, dependencies, discoveryServiceFactory, memberId,
            editionModuleFactory);
  }

  protected DatabaseManagementService createManagementService(Config config,
      ExternalDependencies dependencies,
      DiscoveryServiceFactory discoveryServiceFactory, MemberId memberId,
      ReadReplicaGraphDatabase.ReadReplicaEditionModuleFactory editionModuleFactory) {
    return (new DatabaseManagementServiceFactory(DatabaseInfo.READ_REPLICA, (globalModule) ->
    {
      return editionModuleFactory.create(globalModule, discoveryServiceFactory, memberId);
    })).build(config, dependencies);
  }

  public DatabaseManagementService getManagementService() {
    return this.managementService;
  }

  public interface ReadReplicaEditionModuleFactory {

    ClusteringEditionModule create(GlobalModule n1, DiscoveryServiceFactory n2, MemberId n3);
  }
}
