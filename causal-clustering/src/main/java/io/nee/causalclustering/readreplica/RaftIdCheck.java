/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import io.nee.causalclustering.core.state.storage.SimpleStorage;
import io.nee.causalclustering.identity.RaftId;
import java.util.Objects;
import org.neo4j.kernel.database.NamedDatabaseId;

class RaftIdCheck {

  private final SimpleStorage<RaftId> raftIdStorage;
  private final NamedDatabaseId namedDatabaseId;

  RaftIdCheck(SimpleStorage<RaftId> raftIdStorage, NamedDatabaseId namedDatabaseId) {
    this.raftIdStorage = raftIdStorage;
    this.namedDatabaseId = namedDatabaseId;
  }

  public void perform() throws Exception {
    RaftId raftId;
    if (this.raftIdStorage.exists()) {
      raftId = this.raftIdStorage.readState();
      if (!Objects.equals(raftId.uuid(), this.namedDatabaseId.databaseId().uuid())) {
        throw new IllegalStateException(String.format(
            "Pre-existing cluster state found with an unexpected id %s. The id for this database is %s. This may indicate a previous DROP operation for %s did not complete.",
            raftId.uuid(), this.namedDatabaseId.databaseId().uuid(), this.namedDatabaseId.name()));
      }
    } else {
      raftId = RaftId.from(this.namedDatabaseId.databaseId());
      this.raftIdStorage.writeState(raftId);
    }
  }
}
