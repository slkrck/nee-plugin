/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.causalclustering.readreplica;

import java.util.stream.Stream;
import org.neo4j.kernel.api.exceptions.ReadOnlyDbException;
import org.neo4j.kernel.impl.api.LeaseClient;
import org.neo4j.kernel.impl.locking.ActiveLock;
import org.neo4j.kernel.impl.locking.Locks;
import org.neo4j.lock.AcquireLockTimeoutException;
import org.neo4j.lock.LockTracer;
import org.neo4j.lock.ResourceType;

public class ReadReplicaLockManager implements Locks {

  public org.neo4j.kernel.impl.locking.Locks.Client newClient() {
    return new ReadReplicaLockManager.Client();
  }

  public void accept(Visitor visitor) {
  }

  public void close() {
  }

  private static class Client implements org.neo4j.kernel.impl.locking.Locks.Client {

    public void initialize(LeaseClient leaseClient) {
    }

    public void acquireShared(LockTracer tracer, ResourceType resourceType, long... resourceIds)
        throws AcquireLockTimeoutException {
    }

    public void acquireExclusive(LockTracer tracer, ResourceType resourceType, long... resourceIds)
        throws AcquireLockTimeoutException {
      throw new RuntimeException(new ReadOnlyDbException());
    }

    public boolean tryExclusiveLock(ResourceType resourceType, long resourceId) {
      throw new RuntimeException(new ReadOnlyDbException());
    }

    public boolean trySharedLock(ResourceType resourceType, long resourceId) {
      return false;
    }

    public boolean reEnterShared(ResourceType resourceType, long resourceId) {
      return false;
    }

    public boolean reEnterExclusive(ResourceType resourceType, long resourceId) {
      throw new IllegalStateException("Should never happen");
    }

    public void releaseShared(ResourceType resourceType, long... resourceIds) {
    }

    public void releaseExclusive(ResourceType resourceType, long... resourceIds) {
      throw new IllegalStateException("Should never happen");
    }

    public void prepare() {
    }

    public void stop() {
    }

    public void close() {
    }

    public int getLockSessionId() {
      return 0;
    }

    public Stream<ActiveLock> activeLocks() {
      return Stream.empty();
    }

    public long activeLockCount() {
      return 0L;
    }
  }
}
