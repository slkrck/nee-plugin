/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.neo4j.kernel.database.NamedDatabaseId;

public class ClusterInternalDbmsOperator extends DbmsOperator {

  private final List<ClusterInternalDbmsOperator.StoreCopyHandle> storeCopying = new CopyOnWriteArrayList();
  private final Set<NamedDatabaseId> bootstrapping = ConcurrentHashMap.newKeySet();
  private final Set<NamedDatabaseId> panicked = ConcurrentHashMap.newKeySet();

  protected Map<String, EnterpriseDatabaseState> desired0() {
    HashMap<String, EnterpriseDatabaseState> result = new HashMap();
    Iterator n2 = this.storeCopying.iterator();

    while (n2.hasNext()) {
      ClusterInternalDbmsOperator.StoreCopyHandle storeCopyHandle = (ClusterInternalDbmsOperator.StoreCopyHandle) n2
          .next();
      NamedDatabaseId id = storeCopyHandle.namedDatabaseId;
      if (!this.bootstrapping.contains(id)) {
        result
            .put(id.name(), new EnterpriseDatabaseState(id, EnterpriseOperatorState.STORE_COPYING));
      }
    }

    n2 = this.panicked.iterator();

    while (n2.hasNext()) {
      NamedDatabaseId id = (NamedDatabaseId) n2.next();
      result.put(id.name(), new EnterpriseDatabaseState(id, EnterpriseOperatorState.STOPPED));
    }

    return result;
  }

  public ClusterInternalDbmsOperator.StoreCopyHandle stopForStoreCopy(
      NamedDatabaseId namedDatabaseId) {
    ClusterInternalDbmsOperator.StoreCopyHandle storeCopyHandle = new ClusterInternalDbmsOperator.StoreCopyHandle(
        this, namedDatabaseId);
    this.storeCopying.add(storeCopyHandle);
    this.triggerReconcilerOnStoreCopy(namedDatabaseId);
    return storeCopyHandle;
  }

  public void stopOnPanic(NamedDatabaseId namedDatabaseId, Throwable causeOfPanic) {
    Objects.requireNonNull(causeOfPanic, "The cause of a panic cannot be null!");
    this.panicked.add(namedDatabaseId);
    ReconcilerResult reconcilerResult = this
        .trigger(ReconcilerRequest.forPanickedDatabase(namedDatabaseId, causeOfPanic));
    reconcilerResult.whenComplete(() ->
    {
      this.panicked.remove(namedDatabaseId);
    });
  }

  private boolean triggerReconcilerOnStoreCopy(NamedDatabaseId namedDatabaseId) {
    if (!this.bootstrapping.contains(namedDatabaseId) && !this.panicked.contains(namedDatabaseId)) {
      this.trigger(ReconcilerRequest.simple()).await(namedDatabaseId);
      return true;
    } else {
      return false;
    }
  }

  public ClusterInternalDbmsOperator.BootstrappingHandle bootstrap(
      NamedDatabaseId namedDatabaseId) {
    this.bootstrapping.add(namedDatabaseId);
    return new ClusterInternalDbmsOperator.BootstrappingHandle(this, namedDatabaseId);
  }

  public static class BootstrappingHandle {

    private final ClusterInternalDbmsOperator operator;
    private final NamedDatabaseId namedDatabaseId;

    private BootstrappingHandle(ClusterInternalDbmsOperator operator,
        NamedDatabaseId namedDatabaseId) {
      this.operator = operator;
      this.namedDatabaseId = namedDatabaseId;
    }

    public void release() {
      if (!this.operator.bootstrapping.remove(this.namedDatabaseId)) {
        throw new IllegalStateException(
            "Bootstrapped was already called for " + this.namedDatabaseId);
      }
    }
  }

  public static class StoreCopyHandle {

    private final ClusterInternalDbmsOperator operator;
    private final NamedDatabaseId namedDatabaseId;

    private StoreCopyHandle(ClusterInternalDbmsOperator operator, NamedDatabaseId namedDatabaseId) {
      this.operator = operator;
      this.namedDatabaseId = namedDatabaseId;
    }

    public boolean release() {
      boolean exists = this.operator.storeCopying.remove(this);
      if (!exists) {
        throw new IllegalStateException("Restart was already called for " + this.namedDatabaseId);
      } else {
        return this.operator.triggerReconcilerOnStoreCopy(this.namedDatabaseId);
      }
    }

    public NamedDatabaseId databaseId() {
      return this.namedDatabaseId;
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        ClusterInternalDbmsOperator.StoreCopyHandle that = (ClusterInternalDbmsOperator.StoreCopyHandle) o;
        return Objects.equals(this.namedDatabaseId, that.namedDatabaseId);
      } else {
        return false;
      }
    }

    public int hashCode() {
      return Objects.hash(this.namedDatabaseId);
    }
  }
}
