/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.NotFoundException;
import org.neo4j.graphdb.Transaction;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.store.MetaDataStore;
import org.neo4j.storageengine.api.StoreId;

public class ClusterSystemGraphDbmsModel extends EnterpriseSystemGraphDbmsModel {

  static final String INITIAL_MEMBERS = "initial_members";
  static final String STORE_CREATION_TIME = "store_creation_time";
  static final String STORE_RANDOM_ID = "store_random_id";
  static final String STORE_VERSION = "store_version";

  public ClusterSystemGraphDbmsModel(Supplier<GraphDatabaseService> systemDatabase) {
    super(systemDatabase);
  }

  public Set<UUID> getInitialMembers(NamedDatabaseId namedDatabaseId) {
    try {
      Transaction tx = this.systemDatabase.get().beginTx();

      Set n5;
      try {
        Node node = this.databaseNode(namedDatabaseId, tx);
        String[] initialMembers = (String[]) node.getProperty("initial_members");
        n5 = Arrays.stream(initialMembers).map(UUID::fromString).collect(Collectors.toSet());
      } catch (Throwable n7) {
        if (tx != null) {
          try {
            tx.close();
          } catch (Throwable n6) {
            n7.addSuppressed(n6);
          }
        }

        throw n7;
      }

      if (tx != null) {
        tx.close();
      }

      return n5;
    } catch (NotFoundException n8) {
      return Collections.emptySet();
    }
  }

  public StoreId getStoreId(NamedDatabaseId namedDatabaseId) {
    Transaction tx = this.systemDatabase.get().beginTx();

    StoreId n9;
    try {
      Node node = this.databaseNode(namedDatabaseId, tx);
      long creationTime = (Long) node.getProperty("store_creation_time");
      long randomId = (Long) node.getProperty("store_random_id");
      String storeVersion = (String) node.getProperty("store_version");
      n9 = new StoreId(creationTime, randomId, MetaDataStore.versionStringToLong(storeVersion));
    } catch (Throwable n11) {
      if (tx != null) {
        try {
          tx.close();
        } catch (Throwable n10) {
          n11.addSuppressed(n10);
        }
      }

      throw n11;
    }

    if (tx != null) {
      tx.close();
    }

    return n9;
  }

  private Node databaseNode(NamedDatabaseId databaseId, Transaction tx) {
    return Optional
        .ofNullable(tx.findNode(DATABASE_LABEL, "uuid", databaseId.databaseId().uuid().toString()))
        .or(() ->
        {
          return Optional
              .ofNullable(
                  tx.findNode(
                      DELETED_DATABASE_LABEL,
                      "uuid",
                      databaseId
                          .databaseId()
                          .uuid()
                          .toString()));
        }).orElseThrow(() ->
        {
          return new IllegalStateException(
              "System database must contain a node for database " +
                  databaseId
                      .name());
        });
  }
}
