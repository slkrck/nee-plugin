/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.database;

import io.nee.causalclustering.catchup.CatchupComponentsFactory;
import io.nee.causalclustering.catchup.CatchupComponentsRepository;
import io.nee.causalclustering.catchup.storecopy.StoreFiles;
import io.nee.causalclustering.common.ClusteredDatabase;
import java.io.File;
import java.io.IOException;
import org.neo4j.collection.Dependencies;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.factory.GraphDatabaseFacade;
import org.neo4j.kernel.impl.transaction.log.files.LogFiles;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.monitoring.Monitors;
import org.neo4j.storageengine.api.StoreId;

public class DefaultClusteredDatabaseContext implements ClusteredDatabaseContext {

  private final DatabaseLayout databaseLayout;
  private final StoreFiles storeFiles;
  private final Log log;
  private final NamedDatabaseId namedDatabaseId;
  private final LogFiles txLogs;
  private final Database database;
  private final GraphDatabaseFacade facade;
  private final CatchupComponentsRepository.CatchupComponents catchupComponents;
  private final ClusteredDatabase clusterDatabase;
  private final Monitors clusterDatabaseMonitors;
  private volatile Throwable failureCause;
  private volatile StoreId storeId;

  DefaultClusteredDatabaseContext(Database database, GraphDatabaseFacade facade, LogFiles txLogs,
      StoreFiles storeFiles, LogProvider logProvider,
      CatchupComponentsFactory catchupComponentsFactory, ClusteredDatabase clusterDatabase,
      Monitors clusterDatabaseMonitors) {
    this.database = database;
    this.facade = facade;
    this.databaseLayout = database.getDatabaseLayout();
    this.storeFiles = storeFiles;
    this.txLogs = txLogs;
    this.namedDatabaseId = database.getNamedDatabaseId();
    this.log = logProvider.getLog(this.getClass());
    this.clusterDatabase = clusterDatabase;
    this.clusterDatabaseMonitors = clusterDatabaseMonitors;
    this.catchupComponents = catchupComponentsFactory.createDatabaseComponents(this);
  }

  public StoreId storeId() {
    if (this.storeId == null) {
      this.storeId = this.readStoreIdFromDisk();
    }

    return this.storeId;
  }

  private StoreId readStoreIdFromDisk() {
    try {
      return this.storeFiles.readStoreId(this.databaseLayout);
    } catch (IOException n2) {
      this.log.error("Failure reading store id", n2);
      return null;
    }
  }

  public Monitors monitors() {
    return this.clusterDatabaseMonitors;
  }

  public Dependencies dependencies() {
    return this.database().getDependencyResolver();
  }

  public void delete() throws IOException {
    this.storeFiles.delete(this.databaseLayout, this.txLogs);
  }

  public void fail(Throwable failureCause) {
    this.failureCause = failureCause;
  }

  public Throwable failureCause() {
    return this.failureCause;
  }

  public boolean isFailed() {
    return this.failureCause != null;
  }

  public boolean isEmpty() {
    return this.storeFiles.isEmpty(this.databaseLayout);
  }

  public DatabaseLayout databaseLayout() {
    return this.databaseLayout;
  }

  public void replaceWith(File sourceDir) throws IOException {
    this.storeFiles.delete(this.databaseLayout, this.txLogs);
    this.storeFiles.moveTo(sourceDir, this.databaseLayout, this.txLogs);
  }

  public Database database() {
    return this.database;
  }

  public GraphDatabaseFacade databaseFacade() {
    return this.facade;
  }

  public NamedDatabaseId databaseId() {
    return this.namedDatabaseId;
  }

  public CatchupComponentsRepository.CatchupComponents catchupComponents() {
    return this.catchupComponents;
  }

  public ClusteredDatabase clusteredDatabase() {
    return this.clusterDatabase;
  }
}
