/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.neo4j.function.ThrowingConsumer;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class SystemDbOnlyReplicatedDatabaseEventService implements ReplicatedDatabaseEventService {

  private final Log log;
  private final List<ReplicatedDatabaseEventService.ReplicatedDatabaseEventListener> listeners = new CopyOnWriteArrayList();

  public SystemDbOnlyReplicatedDatabaseEventService(LogProvider logProvider) {
    this.log = logProvider.getLog(this.getClass());
  }

  public void registerListener(NamedDatabaseId namedDatabaseId,
      ReplicatedDatabaseEventService.ReplicatedDatabaseEventListener listener) {
    this.assertIsSystemDatabase(namedDatabaseId);
    this.listeners.add(listener);
  }

  public void unregisterListener(NamedDatabaseId namedDatabaseId,
      ReplicatedDatabaseEventService.ReplicatedDatabaseEventListener listener) {
    this.assertIsSystemDatabase(namedDatabaseId);
    this.listeners.remove(listener);
  }

  public ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch getDatabaseEventDispatch(
      NamedDatabaseId namedDatabaseId) {
    return !namedDatabaseId.isSystemDatabase() ? NO_EVENT_DISPATCH
        : new ReplicatedDatabaseEventService.ReplicatedDatabaseEventDispatch() {
          public void fireTransactionCommitted(long txId) {
            SystemDbOnlyReplicatedDatabaseEventService.this.dispatchEvent((listener) ->
            {
              listener.transactionCommitted(txId);
            });
          }

          public void fireStoreReplaced(long txId) {
            SystemDbOnlyReplicatedDatabaseEventService.this.dispatchEvent((listener) ->
            {
              listener.storeReplaced(txId);
            });
          }
        };
  }

  private void dispatchEvent(
      ThrowingConsumer<ReplicatedDatabaseEventService.ReplicatedDatabaseEventListener, Exception> action) {
    this.listeners.forEach((listener) ->
    {
      try {
        action.accept(listener);
      } catch (Exception n4) {
        this.log.error("Error handling database event", n4);
      }
    });
  }

  private void assertIsSystemDatabase(NamedDatabaseId namedDatabaseId) {
    if (!namedDatabaseId.isSystemDatabase()) {
      throw new IllegalArgumentException("Not supported " + namedDatabaseId);
    }
  }
}
