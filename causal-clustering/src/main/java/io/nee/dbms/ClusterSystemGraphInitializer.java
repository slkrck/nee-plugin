/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import org.neo4j.configuration.Config;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.dbms.database.SystemGraphDbmsModel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;

public class ClusterSystemGraphInitializer extends EnterpriseSystemGraphInitializer {

  public ClusterSystemGraphInitializer(DatabaseManager<?> databaseManager, Config config) {
    super(databaseManager, config);
  }

  public void initializeSystemGraph(GraphDatabaseService system) throws Exception {
    super.initializeSystemGraph(system);
    this.clearClusterProperties(system);
  }

  private void clearClusterProperties(GraphDatabaseService system) {
    Transaction tx = system.beginTx();

    try {
      ResourceIterator databaseItr = tx.findNodes(SystemGraphDbmsModel.DATABASE_LABEL);

      try {
        while (databaseItr.hasNext()) {
          Node database = (Node) databaseItr.next();
          database.removeProperty("initial_members");
          database.removeProperty("store_creation_time");
          database.removeProperty("store_random_id");
          database.removeProperty("store_version");
        }
      } catch (Throwable n8) {
        if (databaseItr != null) {
          try {
            databaseItr.close();
          } catch (Throwable n7) {
            n8.addSuppressed(n7);
          }
        }

        throw n8;
      }

      if (databaseItr != null) {
        databaseItr.close();
      }

      tx.commit();
    } catch (Throwable n9) {
      if (tx != null) {
        try {
          tx.close();
        } catch (Throwable n6) {
          n9.addSuppressed(n6);
        }
      }

      throw n9;
    }

    if (tx != null) {
      tx.close();
    }
  }
}
