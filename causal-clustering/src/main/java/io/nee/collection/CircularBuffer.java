/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.collection;

import java.util.Arrays;

public class CircularBuffer<V> {

  private final int arraySize;
  private final V[] elementArr;
  private int S;
  private int E;

  public CircularBuffer(int capacity) {
    if (capacity <= 0) {
      throw new IllegalArgumentException("Capacity must be > 0.");
    } else {
      this.arraySize = capacity + 1;
      this.elementArr = (V[]) new Object[this.arraySize];
    }
  }

  public void clear(V[] evictions) {
    if (evictions.length != this.arraySize - 1) {
      throw new IllegalArgumentException(
          "The eviction array must be of the same size as the capacity of the circular buffer.");
    } else {
      for (int n2 = 0; this.S != this.E; this.S = this.pos(this.S, 1)) {
        evictions[n2++] = this.elementArr[this.S];
      }

      this.S = 0;
      this.E = 0;
      Arrays.fill(this.elementArr, null);
    }
  }

  private int pos(int base, int delta) {
    return Math.floorMod(base + delta, this.arraySize);
  }

  public V append(V e) {
    this.elementArr[this.E] = e;
    this.E = this.pos(this.E, 1);
    if (this.E == this.S) {
      V old = this.elementArr[this.E];
      this.elementArr[this.E] = null;
      this.S = this.pos(this.S, 1);
      return old;
    } else {
      return null;
    }
  }

  public V read(int idx) {
    return this.elementArr[this.pos(this.S, idx)];
  }

  public V remove() {
    if (this.S == this.E) {
      return null;
    } else {
      V e = this.elementArr[this.S];
      this.elementArr[this.S] = null;
      this.S = this.pos(this.S, 1);
      return e;
    }
  }

  public V removeHead() {
    if (this.S == this.E) {
      return null;
    } else {
      this.E = this.pos(this.E, -1);
      V e = this.elementArr[this.E];
      this.elementArr[this.E] = null;
      return e;
    }
  }

  public int size() {
    return Math.floorMod(this.E - this.S, this.arraySize);
  }
}
