/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.store.format.highlimit.v340;

import io.nee.kernel.impl.store.format.highlimit.Reference;
import java.io.IOException;
import java.util.function.Function;
import org.neo4j.exceptions.UnderlyingStorageException;
import org.neo4j.internal.id.IdSequence;
import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.io.pagecache.impl.CompositePageCursor;
import org.neo4j.kernel.impl.store.RecordPageLocationCalculator;
import org.neo4j.kernel.impl.store.StoreHeader;
import org.neo4j.kernel.impl.store.format.BaseOneByteHeaderRecordFormat;
import org.neo4j.kernel.impl.store.record.AbstractBaseRecord;
import org.neo4j.kernel.impl.store.record.Record;
import org.neo4j.kernel.impl.store.record.RecordLoad;

abstract class BaseHighLimitRecordFormatV3_4_0<RECORD extends AbstractBaseRecord> extends
    BaseOneByteHeaderRecordFormat<RECORD> {

  static final int HEADER_BYTE = 1;
  static final long NULL;
  static final int HEADER_BIT_RECORD_UNIT = 2;
  static final int HEADER_BIT_FIRST_RECORD_UNIT = 4;
  static final int HEADER_BIT_FIXED_REFERENCE = 4;

  static {
    NULL = Record.NULL_REFERENCE.intValue();
  }

  protected BaseHighLimitRecordFormatV3_4_0(Function<StoreHeader, Integer> recordSize,
      int recordHeaderSize, int maxIdBits) {
    super(recordSize, recordHeaderSize, 1, maxIdBits);
  }

  protected static int length(long reference) {
    return Reference.length(reference);
  }

  protected static int length(long reference, long nullValue) {
    return reference == nullValue ? 0 : length(reference);
  }

  protected static long decodeCompressedReference(PageCursor cursor) {
    return Reference.decode(cursor);
  }

  protected static long decodeCompressedReference(PageCursor cursor, long headerByte,
      int headerBitMask, long nullValue) {
    return has(headerByte, headerBitMask) ? decodeCompressedReference(cursor) : nullValue;
  }

  protected static void encode(PageCursor cursor, long reference) {
    Reference.encode(reference, cursor);
  }

  protected static void encode(PageCursor cursor, long reference, long nullValue) {
    if (reference != nullValue) {
      Reference.encode(reference, cursor);
    }
  }

  protected static byte set(byte header, int bitMask, long reference, long nullValue) {
    return set(header, bitMask, reference != nullValue);
  }

  public void read(RECORD record, PageCursor primaryCursor, RecordLoad mode, int recordSize)
      throws IOException {
    int primaryStartOffset = primaryCursor.getOffset();
    byte headerByte = primaryCursor.getByte();
    boolean inUse = this.isInUse(headerByte);
    boolean doubleRecordUnit = has(headerByte, 2);
    record.setUseFixedReferences(false);
    if (doubleRecordUnit) {
      boolean firstRecordUnit = has(headerByte, 4);
      if (!firstRecordUnit) {
        record.clear();
        primaryCursor.setCursorException(
            "Expected record to be the first unit in the chain, but record header says it's not");
        return;
      }

      long secondaryId = Reference.decode(primaryCursor);
      long pageId = RecordPageLocationCalculator
          .pageIdForRecord(secondaryId, primaryCursor.getCurrentPageSize(), recordSize);
      int offset = RecordPageLocationCalculator
          .offsetForId(secondaryId, primaryCursor.getCurrentPageSize(), recordSize);
      PageCursor secondaryCursor = primaryCursor.openLinkedCursor(pageId);
      if (!secondaryCursor.next() | offset < 0) {
        record.clear();
        primaryCursor.setCursorException(this.illegalSecondaryReferenceMessage(pageId));
        return;
      }

      secondaryCursor.setOffset(offset + 1);
      int primarySize = recordSize - (primaryCursor.getOffset() - primaryStartOffset);
      int secondarySize = recordSize - 1;
      PageCursor composite = CompositePageCursor
          .compose(primaryCursor, primarySize, secondaryCursor, secondarySize);
      this.doReadInternal(record, composite, recordSize, headerByte, inUse);
      record.setSecondaryUnitIdOnLoad(secondaryId);
    } else {
      record.setUseFixedReferences(this.isUseFixedReferences(headerByte));
      this.doReadInternal(record, primaryCursor, recordSize, headerByte, inUse);
    }

    primaryCursor.setOffset(primaryStartOffset + recordSize);
  }

  private boolean isUseFixedReferences(byte headerByte) {
    return !has(headerByte, 4);
  }

  private String illegalSecondaryReferenceMessage(long secondaryId) {
    return "Illegal secondary record reference: " + secondaryId;
  }

  protected abstract void doReadInternal(RECORD n1, PageCursor n2, int n3, long n4, boolean n6);

  public void write(RECORD record, PageCursor primaryCursor, int recordSize) throws IOException {
    if (record.inUse()) {
      byte headerByte = this.headerBits(record);

      assert
          (headerByte & 7) == 0 : "Format-specific header bits (" + headerByte
          + ") collides with format-generic header bits";

      headerByte = set(headerByte, 1, record.inUse());
      headerByte = set(headerByte, 2, record.requiresSecondaryUnit());
      if (record.requiresSecondaryUnit()) {
        headerByte = set(headerByte, 4, true);
      } else {
        headerByte = set(headerByte, 4, !record.isUseFixedReferences());
      }

      primaryCursor.putByte(headerByte);
      if (record.requiresSecondaryUnit()) {
        long secondaryUnitId = record.getSecondaryUnitId();
        long pageId = RecordPageLocationCalculator
            .pageIdForRecord(secondaryUnitId, primaryCursor.getCurrentPageSize(), recordSize);
        int offset = RecordPageLocationCalculator
            .offsetForId(secondaryUnitId, primaryCursor.getCurrentPageSize(), recordSize);
        PageCursor secondaryCursor = primaryCursor.openLinkedCursor(pageId);
        if (!secondaryCursor.next()) {
          record.clear();
          return;
        }

        secondaryCursor.setOffset(offset);
        secondaryCursor.putByte((byte) 3);
        int recordSizeWithoutHeader = recordSize - 1;
        PageCursor composite = CompositePageCursor
            .compose(primaryCursor, recordSizeWithoutHeader, secondaryCursor,
                recordSizeWithoutHeader);
        Reference.encode(secondaryUnitId, composite);
        this.doWriteInternal(record, composite);
      } else {
        this.doWriteInternal(record, primaryCursor);
      }
    } else {
      this.markAsUnused(primaryCursor, record, recordSize);
    }
  }

  protected void markAsUnused(PageCursor cursor, RECORD record, int recordSize) throws IOException {
    this.markAsUnused(cursor);
    if (record.hasSecondaryUnitId()) {
      long secondaryUnitId = record.getSecondaryUnitId();
      long pageIdForSecondaryRecord = RecordPageLocationCalculator
          .pageIdForRecord(secondaryUnitId, cursor.getCurrentPageSize(), recordSize);
      int offsetForSecondaryId = RecordPageLocationCalculator
          .offsetForId(secondaryUnitId, cursor.getCurrentPageSize(), recordSize);
      if (!cursor.next(pageIdForSecondaryRecord)) {
        throw new UnderlyingStorageException(
            "Couldn't move to secondary page " + pageIdForSecondaryRecord);
      }

      cursor.setOffset(offsetForSecondaryId);
      this.markAsUnused(cursor);
    }
  }

  protected abstract void doWriteInternal(RECORD n1, PageCursor n2);

  protected abstract byte headerBits(RECORD n1);

  public final void prepare(RECORD record, int recordSize, IdSequence idSequence) {
    if (record.inUse()) {
      record.setUseFixedReferences(this.canUseFixedReferences(record, recordSize));
      if (!record.isUseFixedReferences()) {
        int requiredLength = 1 + this.requiredDataLength(record);
        boolean requiresSecondaryUnit = requiredLength > recordSize;
        record.setRequiresSecondaryUnit(requiresSecondaryUnit);
        if (record.requiresSecondaryUnit() && !record.hasSecondaryUnitId()) {
          record.setSecondaryUnitIdOnCreate(idSequence.nextId());
        }
      }
    }
  }

  protected abstract boolean canUseFixedReferences(RECORD n1, int n2);

  protected abstract int requiredDataLength(RECORD n1);
}
