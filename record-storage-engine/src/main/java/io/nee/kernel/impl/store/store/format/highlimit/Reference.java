/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.store.format.highlimit;

import org.neo4j.io.pagecache.PageCursor;

public enum Reference {
  BYTE_3(3, (byte) 0, 1),
  BYTE_4(4, (byte) 2, 2),
  BYTE_5(5, (byte) 6, 3),
  BYTE_6(6, (byte) 14, 4),
  BYTE_7(7, (byte) 30, 5),
  BYTE_8(8, (byte) 31, 5);

  static final int MAX_BITS = 58;
  private static final Reference[] ENCODINGS = values();
  private final int numberOfBytes;
  private final short highHeader;
  private final int headerShift;
  private final long valueOverflowMask;

  Reference(int numberOfBytes, byte header, int headerBits) {
    this.numberOfBytes = numberOfBytes;
    this.headerShift = 8 - headerBits;
    this.highHeader = (short) ((byte) (header << this.headerShift) & 255);
    this.valueOverflowMask = ~this.valueMask(numberOfBytes, this.headerShift - 1);
  }

  public static void encode(long reference, PageCursor target) {
    boolean positive = reference >= 0L;
    long absoluteReference = positive ? reference : ~reference;
    Reference[] n6 = ENCODINGS;
    int n7 = n6.length;

    for (int n8 = 0; n8 < n7; ++n8) {
      Reference encoding = n6[n8];
      if (encoding.canEncode(absoluteReference)) {
        encoding.encode(absoluteReference, positive, target);
        return;
      }
    }

    throw unsupportedOperationDueToTooBigReference(reference);
  }

  private static UnsupportedOperationException unsupportedOperationDueToTooBigReference(
      long reference) {
    return new UnsupportedOperationException(
        String.format(
            "Reference %d uses too many bits to be encoded by current compression scheme, max %d bits allowed",
            reference, maxBits()));
  }

  public static int length(long reference) {
    boolean positive = reference >= 0L;
    long absoluteReference = positive ? reference : ~reference;
    Reference[] n5 = ENCODINGS;
    int n6 = n5.length;

    for (int n7 = 0; n7 < n6; ++n7) {
      Reference encoding = n5[n7];
      if (encoding.canEncode(absoluteReference)) {
        return encoding.numberOfBytes;
      }
    }

    throw unsupportedOperationDueToTooBigReference(reference);
  }

  private static int maxBits() {
    int max = 0;
    Reference[] n1 = ENCODINGS;
    int n2 = n1.length;

    for (int n3 = 0; n3 < n2; ++n3) {
      Reference encoding = n1[n3];
      max = Math.max(max, encoding.maxBitsSupported());
    }

    return max;
  }

  public static long decode(PageCursor source) {
    int header = source.getByte() & 255;
    int sizeMarks = Integer.numberOfLeadingZeros(~(header & 248) & 255) - 24;
    int signShift = 8 - sizeMarks - (sizeMarks == 5 ? 1 : 2);
    long signComponent = ~(header >>> signShift & 1) + 1;
    long register = (header & (1 << signShift) - 1) << 16;

    for (register += ((source.getByte() & 255) << 8) + (source.getByte() & 255); sizeMarks > 0;
        --sizeMarks) {
      register <<= 8;
      register += source.getByte() & 255;
    }

    return signComponent ^ register;
  }

  public static long toRelative(long reference, long basisReference) {
    return Math.subtractExact(reference, basisReference);
  }

  public static long toAbsolute(long relativeReference, long basisReference) {
    return Math.addExact(relativeReference, basisReference);
  }

  private long valueMask(int numberOfBytes, int headerShift) {
    long mask = (1L << headerShift) - 1L;

    for (int i = 0; i < numberOfBytes - 1; ++i) {
      mask <<= 8;
      mask |= 255L;
    }

    return mask;
  }

  private boolean canEncode(long absoluteReference) {
    return (absoluteReference & this.valueOverflowMask) == 0L;
  }

  private void encode(long absoluteReference, boolean positive, PageCursor source) {
    int shift = this.numberOfBytes - 1 << 3;
    byte signBit = (byte) ((positive ? 0 : 1) << this.headerShift - 1);
    source
        .putByte((byte) (this.highHeader | signBit | (byte) ((int) (absoluteReference >>> shift))));

    do {
      shift -= 8;
      source.putByte((byte) ((int) (absoluteReference >>> shift)));
    }
    while (shift > 0);
  }

  private int maxBitsSupported() {
    return 64 - Long.numberOfLeadingZeros(~this.valueOverflowMask);
  }
}
