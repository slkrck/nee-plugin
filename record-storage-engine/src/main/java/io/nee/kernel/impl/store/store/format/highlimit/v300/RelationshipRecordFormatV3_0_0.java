/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.store.format.highlimit.v300;

import io.nee.kernel.impl.store.format.highlimit.Reference;
import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.kernel.impl.store.record.RelationshipRecord;

public class RelationshipRecordFormatV3_0_0 extends
    BaseHighLimitRecordFormatV3_0_0<RelationshipRecord> {

  public static final int RECORD_SIZE = 32;
  private static final int FIRST_IN_FIRST_CHAIN_BIT = 8;
  private static final int FIRST_IN_SECOND_CHAIN_BIT = 16;
  private static final int HAS_FIRST_CHAIN_NEXT_BIT = 32;
  private static final int HAS_SECOND_CHAIN_NEXT_BIT = 64;
  private static final int HAS_PROPERTY_BIT = 128;

  public RelationshipRecordFormatV3_0_0() {
    this(32);
  }

  RelationshipRecordFormatV3_0_0(int recordSize) {
    super(fixedRecordSize(recordSize), 0);
  }

  public RelationshipRecord newRecord() {
    return new RelationshipRecord(-1L);
  }

  protected void doReadInternal(RelationshipRecord record, PageCursor cursor, int recordSize,
      long headerByte, boolean inUse) {
    int type = cursor.getShort() & '\uffff';
    long recordId = record.getId();
    record.initialize(inUse, decodeCompressedReference(cursor, headerByte, 128, NULL),
        decodeCompressedReference(cursor),
        decodeCompressedReference(cursor), type,
        this.decodeAbsoluteOrRelative(cursor, headerByte, 8, recordId),
        this.decodeAbsoluteIfPresent(cursor, headerByte, 32, recordId),
        this.decodeAbsoluteOrRelative(cursor, headerByte, 16, recordId),
        this.decodeAbsoluteIfPresent(cursor, headerByte, 64, recordId), has(headerByte, 8),
        has(headerByte, 16));
  }

  private long decodeAbsoluteOrRelative(PageCursor cursor, long headerByte, int firstInStartBit,
      long recordId) {
    return has(headerByte, firstInStartBit) ? decodeCompressedReference(cursor)
        : Reference.toAbsolute(decodeCompressedReference(cursor), recordId);
  }

  protected byte headerBits(RelationshipRecord record) {
    byte header = 0;
    header = set(header, 8, record.isFirstInFirstChain());
    header = set(header, 16, record.isFirstInSecondChain());
    header = set(header, 128, record.getNextProp(), NULL);
    header = set(header, 32, record.getFirstNextRel(), NULL);
    header = set(header, 64, record.getSecondNextRel(), NULL);
    return header;
  }

  protected int requiredDataLength(RelationshipRecord record) {
    long recordId = record.getId();
    return 2 + length(record.getNextProp(), NULL) + length(record.getFirstNode()) + length(
        record.getSecondNode()) +
        length(this.getFirstPrevReference(record, recordId)) + this
        .getRelativeReferenceLength(record.getFirstNextRel(), recordId) +
        length(this.getSecondPrevReference(record, recordId)) + this
        .getRelativeReferenceLength(record.getSecondNextRel(), recordId);
  }

  protected void doWriteInternal(RelationshipRecord record, PageCursor cursor) {
    cursor.putShort((short) record.getType());
    long recordId = record.getId();
    encode(cursor, record.getNextProp(), NULL);
    encode(cursor, record.getFirstNode());
    encode(cursor, record.getSecondNode());
    encode(cursor, this.getFirstPrevReference(record, recordId));
    if (record.getFirstNextRel() != NULL) {
      encode(cursor, Reference.toRelative(record.getFirstNextRel(), recordId));
    }

    encode(cursor, this.getSecondPrevReference(record, recordId));
    if (record.getSecondNextRel() != NULL) {
      encode(cursor, Reference.toRelative(record.getSecondNextRel(), recordId));
    }
  }

  private long getSecondPrevReference(RelationshipRecord record, long recordId) {
    return record.isFirstInSecondChain() ? record.getSecondPrevRel()
        : Reference.toRelative(record.getSecondPrevRel(), recordId);
  }

  private long getFirstPrevReference(RelationshipRecord record, long recordId) {
    return record.isFirstInFirstChain() ? record.getFirstPrevRel()
        : Reference.toRelative(record.getFirstPrevRel(), recordId);
  }

  private int getRelativeReferenceLength(long absoluteReference, long recordId) {
    return absoluteReference != NULL ? length(Reference.toRelative(absoluteReference, recordId))
        : 0;
  }

  private long decodeAbsoluteIfPresent(PageCursor cursor, long headerByte, int conditionBit,
      long recordId) {
    return has(headerByte, conditionBit) ? Reference
        .toAbsolute(decodeCompressedReference(cursor), recordId) : NULL;
  }
}
