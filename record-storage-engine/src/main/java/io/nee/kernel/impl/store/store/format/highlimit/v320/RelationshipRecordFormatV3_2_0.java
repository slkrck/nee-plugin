/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.store.format.highlimit.v320;

import io.nee.kernel.impl.store.format.highlimit.Reference;
import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.kernel.impl.store.record.RelationshipRecord;

class RelationshipRecordFormatV3_2_0 extends BaseHighLimitRecordFormatV3_2_0<RelationshipRecord> {

  static final int RECORD_SIZE = 32;
  static final int FIXED_FORMAT_RECORD_SIZE = 32;
  private static final int TYPE_FIELD_BYTES = 3;
  private static final int FIRST_IN_FIRST_CHAIN_BIT = 8;
  private static final int FIRST_IN_SECOND_CHAIN_BIT = 16;
  private static final int HAS_FIRST_CHAIN_NEXT_BIT = 32;
  private static final int HAS_SECOND_CHAIN_NEXT_BIT = 64;
  private static final int HAS_PROPERTY_BIT = 128;
  private static final long FIRST_NODE_BIT = 1L;
  private static final long SECOND_NODE_BIT = 2L;
  private static final long FIRST_PREV_REL_BIT = 4L;
  private static final long FIRST_NEXT_REL_BIT = 8L;
  private static final long SECOND_RREV_REL_BIT = 16L;
  private static final long SECOND_NEXT_REL_BIT = 32L;
  private static final long NEXT_PROP_BIT = 192L;
  private static final long ONE_BIT_OVERFLOW_BIT_MASK = -8589934592L;
  private static final long THREE_BITS_OVERFLOW_BIT_MASK = -17179869184L;
  private static final long HIGH_DWORD_LAST_BIT_MASK = 4294967296L;
  private static final long TWO_BIT_FIXED_REFERENCE_BIT_MASK = 12884901888L;

  RelationshipRecordFormatV3_2_0() {
    this(32);
  }

  RelationshipRecordFormatV3_2_0(int recordSize) {
    super(fixedRecordSize(recordSize), 0, 50);
  }

  public RelationshipRecord newRecord() {
    return new RelationshipRecord(-1L);
  }

  protected void doReadInternal(RelationshipRecord record, PageCursor cursor, int recordSize,
      long headerByte, boolean inUse) {
    int type;
    if (record.isUseFixedReferences()) {
      type = cursor.getShort() & '\uffff';
      this.readFixedReferencesRecord(record, cursor, headerByte, inUse, type);
      record.setUseFixedReferences(true);
    } else {
      type = cursor.getShort() & '\uffff';
      int typeHighWord = cursor.getByte() & 255;
      type = typeHighWord << 16 | type;
      long recordId = record.getId();
      record.initialize(inUse, decodeCompressedReference(cursor, headerByte, 128, NULL),
          decodeCompressedReference(cursor),
          decodeCompressedReference(cursor), type,
          this.decodeAbsoluteOrRelative(cursor, headerByte, 8, recordId),
          this.decodeAbsoluteIfPresent(cursor, headerByte, 32, recordId),
          this.decodeAbsoluteOrRelative(cursor, headerByte, 16, recordId),
          this.decodeAbsoluteIfPresent(cursor, headerByte, 64, recordId), has(headerByte, 8),
          has(headerByte, 16));
    }
  }

  protected byte headerBits(RelationshipRecord record) {
    byte header = 0;
    header = set(header, 8, record.isFirstInFirstChain());
    header = set(header, 16, record.isFirstInSecondChain());
    header = set(header, 128, record.getNextProp(), NULL);
    header = set(header, 32, record.getFirstNextRel(), NULL);
    header = set(header, 64, record.getSecondNextRel(), NULL);
    return header;
  }

  protected int requiredDataLength(RelationshipRecord record) {
    long recordId = record.getId();
    return 3 + length(record.getNextProp(), NULL) + length(record.getFirstNode()) + length(
        record.getSecondNode()) +
        length(this.getFirstPrevReference(record, recordId)) + this
        .getRelativeReferenceLength(record.getFirstNextRel(), recordId) +
        length(this.getSecondPrevReference(record, recordId)) + this
        .getRelativeReferenceLength(record.getSecondNextRel(), recordId);
  }

  protected void doWriteInternal(RelationshipRecord record, PageCursor cursor) {
    if (record.isUseFixedReferences()) {
      this.writeFixedReferencesRecord(record, cursor);
    } else {
      int type = record.getType();
      cursor.putShort((short) type);
      cursor.putByte((byte) (type >>> 16));
      long recordId = record.getId();
      encode(cursor, record.getNextProp(), NULL);
      encode(cursor, record.getFirstNode());
      encode(cursor, record.getSecondNode());
      encode(cursor, this.getFirstPrevReference(record, recordId));
      if (record.getFirstNextRel() != NULL) {
        encode(cursor, Reference.toRelative(record.getFirstNextRel(), recordId));
      }

      encode(cursor, this.getSecondPrevReference(record, recordId));
      if (record.getSecondNextRel() != NULL) {
        encode(cursor, Reference.toRelative(record.getSecondNextRel(), recordId));
      }
    }
  }

  protected boolean canUseFixedReferences(RelationshipRecord record, int recordSize) {
    return this.isRecordBigEnoughForFixedReferences(recordSize) && record.getType() < 65536
        && (record.getFirstNode() & -8589934592L) == 0L &&
        (record.getSecondNode() & -8589934592L) == 0L && (record.getFirstPrevRel() == NULL
        || (record.getFirstPrevRel() & -8589934592L) == 0L) &&
        (record.getFirstNextRel() == NULL || (record.getFirstNextRel() & -8589934592L) == 0L) &&
        (record.getSecondPrevRel() == NULL || (record.getSecondPrevRel() & -8589934592L) == 0L) &&
        (record.getSecondNextRel() == NULL || (record.getSecondNextRel() & -8589934592L) == 0L) &&
        (record.getNextProp() == NULL || (record.getNextProp() & -17179869184L) == 0L);
  }

  private boolean isRecordBigEnoughForFixedReferences(int recordSize) {
    return 32 <= recordSize;
  }

  private long decodeAbsoluteOrRelative(PageCursor cursor, long headerByte, int firstInStartBit,
      long recordId) {
    return has(headerByte, firstInStartBit) ? decodeCompressedReference(cursor)
        : Reference.toAbsolute(decodeCompressedReference(cursor), recordId);
  }

  private long getSecondPrevReference(RelationshipRecord record, long recordId) {
    return record.isFirstInSecondChain() ? record.getSecondPrevRel()
        : Reference.toRelative(record.getSecondPrevRel(), recordId);
  }

  private long getFirstPrevReference(RelationshipRecord record, long recordId) {
    return record.isFirstInFirstChain() ? record.getFirstPrevRel()
        : Reference.toRelative(record.getFirstPrevRel(), recordId);
  }

  private int getRelativeReferenceLength(long absoluteReference, long recordId) {
    return absoluteReference != NULL ? length(Reference.toRelative(absoluteReference, recordId))
        : 0;
  }

  private long decodeAbsoluteIfPresent(PageCursor cursor, long headerByte, int conditionBit,
      long recordId) {
    return has(headerByte, conditionBit) ? Reference
        .toAbsolute(decodeCompressedReference(cursor), recordId) : NULL;
  }

  private void readFixedReferencesRecord(RelationshipRecord record, PageCursor cursor,
      long headerByte, boolean inUse, int type) {
    long modifiers = cursor.getByte();
    long firstNode = (long) cursor.getInt() & 4294967295L;
    long firstNodeMod = (modifiers & 1L) << 32;
    long secondNode = (long) cursor.getInt() & 4294967295L;
    long secondNodeMod = (modifiers & 2L) << 31;
    long firstPrevRel = (long) cursor.getInt() & 4294967295L;
    long firstPrevRelMod = (modifiers & 4L) << 30;
    long firstNextRel = (long) cursor.getInt() & 4294967295L;
    long firstNextRelMod = (modifiers & 8L) << 29;
    long secondPrevRel = (long) cursor.getInt() & 4294967295L;
    long secondPrevRelMod = (modifiers & 16L) << 28;
    long secondNextRel = (long) cursor.getInt() & 4294967295L;
    long secondNextRelMod = (modifiers & 32L) << 27;
    long nextProp = (long) cursor.getInt() & 4294967295L;
    long nextPropMod = (modifiers & 192L) << 26;
    record.initialize(inUse, longFromIntAndMod(nextProp, nextPropMod),
        longFromIntAndMod(firstNode, firstNodeMod),
        longFromIntAndMod(secondNode, secondNodeMod), type,
        longFromIntAndMod(firstPrevRel, firstPrevRelMod),
        longFromIntAndMod(firstNextRel, firstNextRelMod),
        longFromIntAndMod(secondPrevRel, secondPrevRelMod),
        longFromIntAndMod(secondNextRel, secondNextRelMod), has(headerByte, 8),
        has(headerByte, 16));
  }

  private void writeFixedReferencesRecord(RelationshipRecord record, PageCursor cursor) {
    cursor.putShort((short) record.getType());
    long firstNode = record.getFirstNode();
    short firstNodeMod = (short) ((int) ((firstNode & 4294967296L) >> 32));
    long secondNode = record.getSecondNode();
    long secondNodeMod = (secondNode & 4294967296L) >> 31;
    long firstPrevRel = record.getFirstPrevRel();
    long firstPrevRelMod = firstPrevRel == NULL ? 0L : (firstPrevRel & 4294967296L) >> 30;
    long firstNextRel = record.getFirstNextRel();
    long firstNextRelMod = firstNextRel == NULL ? 0L : (firstNextRel & 4294967296L) >> 29;
    long secondPrevRel = record.getSecondPrevRel();
    long secondPrevRelMod = secondPrevRel == NULL ? 0L : (secondPrevRel & 4294967296L) >> 28;
    long secondNextRel = record.getSecondNextRel();
    long secondNextRelMod = secondNextRel == NULL ? 0L : (secondNextRel & 4294967296L) >> 27;
    long nextProp = record.getNextProp();
    long nextPropMod = nextProp == NULL ? 0L : (nextProp & 12884901888L) >> 26;
    short modifiers =
        (short) ((int) ((long) firstNodeMod | secondNodeMod | firstPrevRelMod | firstNextRelMod
            | secondPrevRelMod | secondNextRelMod | nextPropMod));
    cursor.putByte((byte) modifiers);
    cursor.putInt((int) firstNode);
    cursor.putInt((int) secondNode);
    cursor.putInt((int) firstPrevRel);
    cursor.putInt((int) firstNextRel);
    cursor.putInt((int) secondPrevRel);
    cursor.putInt((int) secondNextRel);
    cursor.putInt((int) nextProp);
  }
}
