/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.store.format.highlimit.v340;

import io.nee.kernel.impl.store.format.highlimit.Reference;
import java.util.Iterator;
import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.kernel.impl.store.format.BaseOneByteHeaderRecordFormat;
import org.neo4j.kernel.impl.store.format.BaseRecordFormat;
import org.neo4j.kernel.impl.store.record.PropertyBlock;
import org.neo4j.kernel.impl.store.record.PropertyRecord;
import org.neo4j.kernel.impl.store.record.RecordLoad;

public class PropertyRecordFormatV3_4_0 extends BaseOneByteHeaderRecordFormat<PropertyRecord> {

  public static final int RECORD_SIZE = 48;
  static final int FIXED_FORMAT_RECORD_SIZE = 16;
  private static final int PROPERTY_BLOCKS_PADDING = 3;
  private static final long HIGH_DWORD_LOWER_WORD_MASK = 281470681743360L;
  private static final long HIGH_DWORD_LOWER_WORD_CHECK_MASK = -281474976710656L;

  public PropertyRecordFormatV3_4_0() {
    super(fixedRecordSize(48), 0, 1, 50);
  }

  public PropertyRecord newRecord() {
    return new PropertyRecord(-1L);
  }

  public void read(PropertyRecord record, PageCursor cursor, RecordLoad mode, int recordSize) {
    int offset = cursor.getOffset();
    byte headerByte = cursor.getByte();
    boolean inUse = this.isInUse(headerByte);
    boolean useFixedReferences = has(headerByte, 4);
    if (mode.shouldLoad(inUse)) {
      int blockCount = headerByte >>> 4;
      long recordId = record.getId();
      if (useFixedReferences) {
        this.readFixedReferencesRecord(record, cursor, inUse);
      } else {
        record.initialize(inUse, Reference.toAbsolute(Reference.decode(cursor), recordId),
            Reference.toAbsolute(Reference.decode(cursor), recordId));
      }

      record.setUseFixedReferences(useFixedReferences);
      if (blockCount > record.getBlockCapacity()
          | 48 - (cursor.getOffset() - offset) < blockCount * 8) {
        cursor.setCursorException(
            "PropertyRecord claims to contain more blocks than can fit in a record");
        return;
      }

      while (blockCount-- > 0) {
        record.addLoadedBlock(cursor.getLong());
      }
    }
  }

  public void write(PropertyRecord record, PageCursor cursor, int recordSize) {
    if (record.inUse()) {
      byte headerByte = (byte) (1 | this.numberOfBlocks(record) << 4);
      boolean canUseFixedReferences = this.canUseFixedReferences(record, recordSize);
      record.setUseFixedReferences(canUseFixedReferences);
      headerByte = set(headerByte, 4, canUseFixedReferences);
      cursor.putByte(headerByte);
      long recordId = record.getId();
      if (canUseFixedReferences) {
        this.writeFixedReferencesRecord(record, cursor);
      } else {
        Reference.encode(Reference.toRelative(record.getPrevProp(), recordId), cursor);
        Reference.encode(Reference.toRelative(record.getNextProp(), recordId), cursor);
      }

      Iterator n8 = record.iterator();

      while (n8.hasNext()) {
        PropertyBlock block = (PropertyBlock) n8.next();
        long[] n10 = block.getValueBlocks();
        int n11 = n10.length;

        for (int n12 = 0; n12 < n11; ++n12) {
          long propertyBlock = n10[n12];
          cursor.putLong(propertyBlock);
        }
      }
    } else {
      this.markAsUnused(cursor);
    }
  }

  private int numberOfBlocks(PropertyRecord record) {
    int count = 0;

    PropertyBlock block;
    for (Iterator n3 = record.iterator(); n3.hasNext(); count += block.getValueBlocks().length) {
      block = (PropertyBlock) n3.next();
    }

    return count;
  }

  public long getNextRecordReference(PropertyRecord record) {
    return record.getNextProp();
  }

  private boolean canUseFixedReferences(PropertyRecord record, int recordSize) {
    return this.isRecordBigEnoughForFixedReferences(recordSize) &&
        (record.getNextProp() == BaseHighLimitRecordFormatV3_4_0.NULL
            || (record.getNextProp() & -281474976710656L) == 0L) &&
        (record.getPrevProp() == BaseHighLimitRecordFormatV3_4_0.NULL
            || (record.getPrevProp() & -281474976710656L) == 0L);
  }

  private boolean isRecordBigEnoughForFixedReferences(int recordSize) {
    return 16 <= recordSize;
  }

  private void readFixedReferencesRecord(PropertyRecord record, PageCursor cursor, boolean inUse) {
    long prevMod = (long) cursor.getShort() & 65535L;
    long prevProp = (long) cursor.getInt() & 4294967295L;
    long nextMod = (long) cursor.getShort() & 65535L;
    long nextProp = (long) cursor.getInt() & 4294967295L;
    record.initialize(inUse, BaseRecordFormat.longFromIntAndMod(prevProp, prevMod << 32),
        BaseRecordFormat.longFromIntAndMod(nextProp, nextMod << 32));
    cursor.setOffset(cursor.getOffset() + 3);
  }

  private void writeFixedReferencesRecord(PropertyRecord record, PageCursor cursor) {
    short prevModifier =
        record.getPrevProp() == BaseHighLimitRecordFormatV3_4_0.NULL ? 0
            : (short) ((int) ((record.getPrevProp() & 281470681743360L) >> 32));
    short nextModifier =
        record.getNextProp() == BaseHighLimitRecordFormatV3_4_0.NULL ? 0
            : (short) ((int) ((record.getNextProp() & 281470681743360L) >> 32));
    cursor.putShort(prevModifier);
    cursor.putInt((int) record.getPrevProp());
    cursor.putShort(nextModifier);
    cursor.putInt((int) record.getNextProp());
    cursor.setOffset(cursor.getOffset() + 3);
  }
}
