/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.store.format.highlimit.v300;

import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.kernel.impl.store.record.NodeRecord;
import org.neo4j.kernel.impl.store.record.Record;

public class NodeRecordFormatV3_0_0 extends BaseHighLimitRecordFormatV3_0_0<NodeRecord> {

  public static final int RECORD_SIZE = 16;
  private static final long NULL_LABELS;
  private static final int DENSE_NODE_BIT = 8;
  private static final int HAS_RELATIONSHIP_BIT = 16;
  private static final int HAS_PROPERTY_BIT = 32;
  private static final int HAS_LABELS_BIT = 64;

  static {
    NULL_LABELS = Record.NO_LABELS_FIELD.intValue();
  }

  public NodeRecordFormatV3_0_0() {
    this(16);
  }

  private NodeRecordFormatV3_0_0(int recordSize) {
    super(fixedRecordSize(recordSize), 0);
  }

  public NodeRecord newRecord() {
    return new NodeRecord(-1L);
  }

  protected void doReadInternal(NodeRecord record, PageCursor cursor, int recordSize,
      long headerByte, boolean inUse) {
    boolean dense = has(headerByte, 8);
    long nextRel = decodeCompressedReference(cursor, headerByte, 16, NULL);
    long nextProp = decodeCompressedReference(cursor, headerByte, 32, NULL);
    long labelField = decodeCompressedReference(cursor, headerByte, 64, NULL_LABELS);
    record.initialize(inUse, nextProp, dense, nextRel, labelField);
  }

  public int requiredDataLength(NodeRecord record) {
    return length(record.getNextRel(), NULL) + length(record.getNextProp(), NULL) + length(
        record.getLabelField(), NULL_LABELS);
  }

  protected byte headerBits(NodeRecord record) {
    byte header = 0;
    header = set(header, 8, record.isDense());
    header = set(header, 16, record.getNextRel(), NULL);
    header = set(header, 32, record.getNextProp(), NULL);
    header = set(header, 64, record.getLabelField(), NULL_LABELS);
    return header;
  }

  protected void doWriteInternal(NodeRecord record, PageCursor cursor) {
    encode(cursor, record.getNextRel(), NULL);
    encode(cursor, record.getNextProp(), NULL);
    encode(cursor, record.getLabelField(), NULL_LABELS);
  }
}
