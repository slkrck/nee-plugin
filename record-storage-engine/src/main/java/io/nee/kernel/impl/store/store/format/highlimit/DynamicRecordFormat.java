/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.store.format.highlimit;

import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.kernel.impl.store.format.BaseOneByteHeaderRecordFormat;
import org.neo4j.kernel.impl.store.record.DynamicRecord;
import org.neo4j.kernel.impl.store.record.RecordLoad;

public class DynamicRecordFormat extends BaseOneByteHeaderRecordFormat<DynamicRecord> {

  private static final int RECORD_HEADER_SIZE = 12;
  private static final int START_RECORD_BIT = 8;

  public DynamicRecordFormat() {
    super(INT_STORE_HEADER_READER, 12, 1, 50);
  }

  private static String payloadLengthErrorMessage(DynamicRecord record, int recordSize,
      int length) {
    return length < 0 ? negativePayloadErrorMessage(record, length)
        : org.neo4j.kernel.impl.store.format.standard.DynamicRecordFormat
            .payloadTooBigErrorMessage(record, recordSize, length);
  }

  private static String negativePayloadErrorMessage(DynamicRecord record, int length) {
    return String
        .format("DynamicRecord[%s] claims to have a negative payload of %s bytes.", record.getId(),
            length);
  }

  public DynamicRecord newRecord() {
    return new DynamicRecord(-1L);
  }

  public void read(DynamicRecord record, PageCursor cursor, RecordLoad mode, int recordSize) {
    byte headerByte = cursor.getByte();
    boolean inUse = this.isInUse(headerByte);
    if (mode.shouldLoad(inUse)) {
      int length = cursor.getShort() | cursor.getByte() << 16;
      if (length > recordSize || length < 0) {
        cursor.setCursorException(payloadLengthErrorMessage(record, recordSize, length));
        return;
      }

      long next = cursor.getLong();
      boolean isStartRecord = (headerByte & 8) != 0;
      record.initialize(inUse, isStartRecord, next, -1, length);
      org.neo4j.kernel.impl.store.format.standard.DynamicRecordFormat.readData(record, cursor);
    } else {
      record.setInUse(inUse);
    }
  }

  public void write(DynamicRecord record, PageCursor cursor, int recordSize) {
    if (record.inUse()) {
      assert record.getLength() < 16777215;

      byte headerByte = (byte) ((record.inUse() ? 1 : 0) | (record.isStartRecord() ? 8 : 0));
      cursor.putByte(headerByte);
      cursor.putShort((short) record.getLength());
      cursor.putByte((byte) (record.getLength() >>> 16));
      cursor.putLong(record.getNextBlock());
      cursor.putBytes(record.getData());
    } else {
      this.markAsUnused(cursor);
    }
  }

  public long getNextRecordReference(DynamicRecord record) {
    return record.getNextBlock();
  }
}
