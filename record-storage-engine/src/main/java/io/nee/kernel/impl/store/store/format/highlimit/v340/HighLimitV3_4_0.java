/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.store.format.highlimit.v340;

import io.nee.kernel.impl.store.format.highlimit.DynamicRecordFormat;
import io.nee.kernel.impl.store.format.highlimit.HighLimitFormatFamily;
import org.neo4j.kernel.impl.store.format.BaseRecordFormats;
import org.neo4j.kernel.impl.store.format.FormatFamily;
import org.neo4j.kernel.impl.store.format.RecordFormat;
import org.neo4j.kernel.impl.store.format.RecordFormats;
import org.neo4j.kernel.impl.store.format.RecordStorageCapability;
import org.neo4j.kernel.impl.store.format.StoreVersion;
import org.neo4j.kernel.impl.store.format.standard.LabelTokenRecordFormat;
import org.neo4j.kernel.impl.store.format.standard.PropertyKeyTokenRecordFormat;
import org.neo4j.kernel.impl.store.format.standard.RelationshipTypeTokenRecordFormat;
import org.neo4j.kernel.impl.store.record.DynamicRecord;
import org.neo4j.kernel.impl.store.record.LabelTokenRecord;
import org.neo4j.kernel.impl.store.record.NodeRecord;
import org.neo4j.kernel.impl.store.record.PropertyKeyTokenRecord;
import org.neo4j.kernel.impl.store.record.PropertyRecord;
import org.neo4j.kernel.impl.store.record.RelationshipGroupRecord;
import org.neo4j.kernel.impl.store.record.RelationshipRecord;
import org.neo4j.kernel.impl.store.record.RelationshipTypeTokenRecord;
import org.neo4j.storageengine.api.IndexCapabilities.LuceneCapability;

public class HighLimitV3_4_0 extends BaseRecordFormats {

  public static final String STORE_VERSION;
  public static final RecordFormats RECORD_FORMATS;
  public static final String NAME = "high_limitV3_4_0";

  static {
    STORE_VERSION = StoreVersion.HIGH_LIMIT_V3_4_0.versionString();
    RECORD_FORMATS = new HighLimitV3_4_0();
  }

  protected HighLimitV3_4_0() {
    super(STORE_VERSION, StoreVersion.HIGH_LIMIT_V3_4_0.introductionVersion(), 5,
        RecordStorageCapability.DENSE_NODES, RecordStorageCapability.RELATIONSHIP_TYPE_3BYTES,
        RecordStorageCapability.SCHEMA,
        RecordStorageCapability.POINT_PROPERTIES, RecordStorageCapability.TEMPORAL_PROPERTIES,
        RecordStorageCapability.SECONDARY_RECORD_UNITS,
        LuceneCapability.LUCENE_5);
  }

  public RecordFormat<NodeRecord> node() {
    return new NodeRecordFormatV3_4_0();
  }

  public RecordFormat<RelationshipRecord> relationship() {
    return new RelationshipRecordFormatV3_4_0();
  }

  public RecordFormat<RelationshipGroupRecord> relationshipGroup() {
    return new RelationshipGroupRecordFormatV3_4_0();
  }

  public RecordFormat<PropertyRecord> property() {
    return new PropertyRecordFormatV3_4_0();
  }

  public RecordFormat<LabelTokenRecord> labelToken() {
    return new LabelTokenRecordFormat();
  }

  public RecordFormat<PropertyKeyTokenRecord> propertyKeyToken() {
    return new PropertyKeyTokenRecordFormat();
  }

  public RecordFormat<RelationshipTypeTokenRecord> relationshipTypeToken() {
    return new RelationshipTypeTokenRecordFormat(24);
  }

  public RecordFormat<DynamicRecord> dynamic() {
    return new DynamicRecordFormat();
  }

  public FormatFamily getFormatFamily() {
    return HighLimitFormatFamily.INSTANCE;
  }

  public String name() {
    return "high_limitV3_4_0";
  }
}
