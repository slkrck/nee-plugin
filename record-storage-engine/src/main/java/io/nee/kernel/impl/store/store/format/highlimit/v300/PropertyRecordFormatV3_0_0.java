/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.store.format.highlimit.v300;

import io.nee.kernel.impl.store.format.highlimit.Reference;
import java.util.Iterator;
import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.kernel.impl.store.format.BaseOneByteHeaderRecordFormat;
import org.neo4j.kernel.impl.store.record.PropertyBlock;
import org.neo4j.kernel.impl.store.record.PropertyRecord;
import org.neo4j.kernel.impl.store.record.RecordLoad;

public class PropertyRecordFormatV3_0_0 extends BaseOneByteHeaderRecordFormat<PropertyRecord> {

  public static final int RECORD_SIZE = 48;

  public PropertyRecordFormatV3_0_0() {
    super(fixedRecordSize(48), 0, 1, 50);
  }

  public PropertyRecord newRecord() {
    return new PropertyRecord(-1L);
  }

  public void read(PropertyRecord record, PageCursor cursor, RecordLoad mode, int recordSize) {
    int offset = cursor.getOffset();
    byte headerByte = cursor.getByte();
    boolean inUse = this.isInUse(headerByte);
    if (mode.shouldLoad(inUse)) {
      int blockCount = headerByte >>> 4;
      long recordId = record.getId();
      record.initialize(inUse, Reference.toAbsolute(Reference.decode(cursor), recordId),
          Reference.toAbsolute(Reference.decode(cursor), recordId));
      if (blockCount > record.getBlockCapacity()
          | 48 - (cursor.getOffset() - offset) < blockCount * 8) {
        cursor.setCursorException(
            "PropertyRecord claims to contain more blocks than can fit in a record");
        return;
      }

      while (blockCount-- > 0) {
        record.addLoadedBlock(cursor.getLong());
      }
    }
  }

  public void write(PropertyRecord record, PageCursor cursor, int recordSize) {
    if (record.inUse()) {
      cursor.putByte((byte) (1 | this.numberOfBlocks(record) << 4));
      long recordId = record.getId();
      Reference.encode(Reference.toRelative(record.getPrevProp(), recordId), cursor);
      Reference.encode(Reference.toRelative(record.getNextProp(), recordId), cursor);
      Iterator n6 = record.iterator();

      while (n6.hasNext()) {
        PropertyBlock block = (PropertyBlock) n6.next();
        long[] n8 = block.getValueBlocks();
        int n9 = n8.length;

        for (int n10 = 0; n10 < n9; ++n10) {
          long propertyBlock = n8[n10];
          cursor.putLong(propertyBlock);
        }
      }
    } else {
      this.markAsUnused(cursor);
    }
  }

  private int numberOfBlocks(PropertyRecord record) {
    int count = 0;

    PropertyBlock block;
    for (Iterator n3 = record.iterator(); n3.hasNext(); count += block.getValueBlocks().length) {
      block = (PropertyBlock) n3.next();
    }

    return count;
  }

  public long getNextRecordReference(PropertyRecord record) {
    return record.getNextProp();
  }
}
