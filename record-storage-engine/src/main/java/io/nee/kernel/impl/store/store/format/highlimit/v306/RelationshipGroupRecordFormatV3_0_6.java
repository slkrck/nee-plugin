/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.store.format.highlimit.v306;

import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.kernel.impl.store.record.RelationshipGroupRecord;

class RelationshipGroupRecordFormatV3_0_6 extends
    BaseHighLimitRecordFormatV3_0_6<RelationshipGroupRecord> {

  static final int RECORD_SIZE = 32;
  static final int FIXED_FORMAT_RECORD_SIZE = 24;
  private static final int HAS_OUTGOING_BIT = 8;
  private static final int HAS_INCOMING_BIT = 16;
  private static final int HAS_LOOP_BIT = 32;
  private static final int HAS_NEXT_BIT = 64;
  private static final int NEXT_RECORD_BIT = 1;
  private static final int FIRST_OUT_BIT = 2;
  private static final int FIRST_IN_BIT = 4;
  private static final int FIRST_LOOP_BIT = 8;
  private static final int OWNING_NODE_BIT = 16;
  private static final long ONE_BIT_OVERFLOW_BIT_MASK = -8589934592L;
  private static final long HIGH_DWORD_LAST_BIT_MASK = 4294967296L;

  RelationshipGroupRecordFormatV3_0_6() {
    this(32);
  }

  RelationshipGroupRecordFormatV3_0_6(int recordSize) {
    super(fixedRecordSize(recordSize), 0);
  }

  public RelationshipGroupRecord newRecord() {
    return new RelationshipGroupRecord(-1L);
  }

  protected void doReadInternal(RelationshipGroupRecord record, PageCursor cursor, int recordSize,
      long headerByte, boolean inUse) {
    if (record.isUseFixedReferences()) {
      this.readFixedReferencesMethod(record, cursor, inUse);
      record.setUseFixedReferences(true);
    } else {
      record.initialize(inUse, cursor.getShort() & '\uffff',
          decodeCompressedReference(cursor, headerByte, 8, NULL),
          decodeCompressedReference(cursor, headerByte, 16, NULL),
          decodeCompressedReference(cursor, headerByte, 32, NULL),
          decodeCompressedReference(cursor),
          decodeCompressedReference(cursor, headerByte, 64, NULL));
    }
  }

  protected byte headerBits(RelationshipGroupRecord record) {
    byte header = 0;
    header = set(header, 8, record.getFirstOut(), NULL);
    header = set(header, 16, record.getFirstIn(), NULL);
    header = set(header, 32, record.getFirstLoop(), NULL);
    header = set(header, 64, record.getNext(), NULL);
    return header;
  }

  protected int requiredDataLength(RelationshipGroupRecord record) {
    return 2 + length(record.getFirstOut(), NULL) + length(record.getFirstIn(), NULL) + length(
        record.getFirstLoop(), NULL) +
        length(record.getOwningNode()) + length(record.getNext(), NULL);
  }

  protected void doWriteInternal(RelationshipGroupRecord record, PageCursor cursor) {
    if (record.isUseFixedReferences()) {
      this.writeFixedReferencesRecord(record, cursor);
    } else {
      cursor.putShort((short) record.getType());
      encode(cursor, record.getFirstOut(), NULL);
      encode(cursor, record.getFirstIn(), NULL);
      encode(cursor, record.getFirstLoop(), NULL);
      encode(cursor, record.getOwningNode());
      encode(cursor, record.getNext(), NULL);
    }
  }

  protected boolean canUseFixedReferences(RelationshipGroupRecord record, int recordSize) {
    return this.isRecordBigEnoughForFixedReferences(recordSize) && (record.getNext() == NULL
        || (record.getNext() & -8589934592L) == 0L) &&
        (record.getFirstOut() == NULL || (record.getFirstOut() & -8589934592L) == 0L) &&
        (record.getFirstIn() == NULL || (record.getFirstIn() & -8589934592L) == 0L) &&
        (record.getFirstLoop() == NULL || (record.getFirstLoop() & -8589934592L) == 0L) &&
        (record.getOwningNode() == NULL || (record.getOwningNode() & -8589934592L) == 0L);
  }

  private boolean isRecordBigEnoughForFixedReferences(int recordSize) {
    return 24 <= recordSize;
  }

  private void readFixedReferencesMethod(RelationshipGroupRecord record, PageCursor cursor,
      boolean inUse) {
    long modifiers = cursor.getByte();
    int type = cursor.getShort() & '\uffff';
    long nextLowBits = (long) cursor.getInt() & 4294967295L;
    long firstOutLowBits = (long) cursor.getInt() & 4294967295L;
    long firstInLowBits = (long) cursor.getInt() & 4294967295L;
    long firstLoopLowBits = (long) cursor.getInt() & 4294967295L;
    long owningNodeLowBits = (long) cursor.getInt() & 4294967295L;
    long nextMod = (modifiers & 1L) << 32;
    long firstOutMod = (modifiers & 2L) << 31;
    long firstInMod = (modifiers & 4L) << 30;
    long firstLoopMod = (modifiers & 8L) << 29;
    long owningNodeMod = (modifiers & 16L) << 28;
    record.initialize(inUse, type, longFromIntAndMod(firstOutLowBits, firstOutMod),
        longFromIntAndMod(firstInLowBits, firstInMod),
        longFromIntAndMod(firstLoopLowBits, firstLoopMod),
        longFromIntAndMod(owningNodeLowBits, owningNodeMod),
        longFromIntAndMod(nextLowBits, nextMod));
  }

  private void writeFixedReferencesRecord(RelationshipGroupRecord record, PageCursor cursor) {
    long nextMod = record.getNext() == NULL ? 0L : (record.getNext() & 4294967296L) >> 32;
    long firstOutMod =
        record.getFirstOut() == NULL ? 0L : (record.getFirstOut() & 4294967296L) >> 31;
    long firstInMod = record.getFirstIn() == NULL ? 0L : (record.getFirstIn() & 4294967296L) >> 30;
    long firstLoopMod =
        record.getFirstLoop() == NULL ? 0L : (record.getFirstLoop() & 4294967296L) >> 29;
    long owningNodeMod =
        record.getOwningNode() == NULL ? 0L : (record.getOwningNode() & 4294967296L) >> 28;
    cursor.putByte(
        (byte) ((int) (nextMod | firstOutMod | firstInMod | firstLoopMod | owningNodeMod)));
    cursor.putShort((short) record.getType());
    cursor.putInt((int) record.getNext());
    cursor.putInt((int) record.getFirstOut());
    cursor.putInt((int) record.getFirstIn());
    cursor.putInt((int) record.getFirstLoop());
    cursor.putInt((int) record.getOwningNode());
  }
}
