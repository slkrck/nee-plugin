/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.kernel.impl.store.format.highlimit.v300;

import org.neo4j.io.pagecache.PageCursor;
import org.neo4j.kernel.impl.store.record.RelationshipGroupRecord;

public class RelationshipGroupRecordFormatV3_0_0 extends
    BaseHighLimitRecordFormatV3_0_0<RelationshipGroupRecord> {

  public static final int RECORD_SIZE = 32;
  private static final int HAS_OUTGOING_BIT = 8;
  private static final int HAS_INCOMING_BIT = 16;
  private static final int HAS_LOOP_BIT = 32;
  private static final int HAS_NEXT_BIT = 64;

  public RelationshipGroupRecordFormatV3_0_0() {
    this(32);
  }

  private RelationshipGroupRecordFormatV3_0_0(int recordSize) {
    super(fixedRecordSize(recordSize), 0);
  }

  public RelationshipGroupRecord newRecord() {
    return new RelationshipGroupRecord(-1L);
  }

  protected void doReadInternal(RelationshipGroupRecord record, PageCursor cursor, int recordSize,
      long headerByte, boolean inUse) {
    record.initialize(inUse, cursor.getShort() & '\uffff',
        decodeCompressedReference(cursor, headerByte, 8, NULL),
        decodeCompressedReference(cursor, headerByte, 16, NULL),
        decodeCompressedReference(cursor, headerByte, 32, NULL),
        decodeCompressedReference(cursor), decodeCompressedReference(cursor, headerByte, 64, NULL));
  }

  protected byte headerBits(RelationshipGroupRecord record) {
    byte header = 0;
    header = set(header, 8, record.getFirstOut(), NULL);
    header = set(header, 16, record.getFirstIn(), NULL);
    header = set(header, 32, record.getFirstLoop(), NULL);
    header = set(header, 64, record.getNext(), NULL);
    return header;
  }

  protected int requiredDataLength(RelationshipGroupRecord record) {
    return 2 + length(record.getFirstOut(), NULL) + length(record.getFirstIn(), NULL) + length(
        record.getFirstLoop(), NULL) +
        length(record.getOwningNode()) + length(record.getNext(), NULL);
  }

  protected void doWriteInternal(RelationshipGroupRecord record, PageCursor cursor) {
    cursor.putShort((short) record.getType());
    encode(cursor, record.getFirstOut(), NULL);
    encode(cursor, record.getFirstIn(), NULL);
    encode(cursor, record.getFirstLoop(), NULL);
    encode(cursor, record.getOwningNode());
    encode(cursor, record.getNext(), NULL);
  }
}
