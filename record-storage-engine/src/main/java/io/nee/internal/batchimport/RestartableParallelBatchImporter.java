/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.internal.batchimport;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import org.neo4j.configuration.Config;
import org.neo4j.internal.batchimport.AdditionalInitialIds;
import org.neo4j.internal.batchimport.BatchImporter;
import org.neo4j.internal.batchimport.Configuration;
import org.neo4j.internal.batchimport.DataStatistics;
import org.neo4j.internal.batchimport.ImportLogic;
import org.neo4j.internal.batchimport.ImportLogic.Monitor;
import org.neo4j.internal.batchimport.LogFilesInitializer;
import org.neo4j.internal.batchimport.input.Collector;
import org.neo4j.internal.batchimport.input.Input;
import org.neo4j.internal.batchimport.staging.ExecutionMonitor;
import org.neo4j.internal.batchimport.store.BatchingNeoStores;
import org.neo4j.internal.helpers.ArrayUtil;
import org.neo4j.internal.helpers.collection.Iterators;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.internal.helpers.collection.PrefetchingIterator;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.impl.store.PropertyType;
import org.neo4j.kernel.impl.store.StoreType;
import org.neo4j.kernel.impl.store.format.RecordFormats;
import org.neo4j.logging.internal.LogService;
import org.neo4j.scheduler.JobScheduler;

public class RestartableParallelBatchImporter implements BatchImporter {

  static final String FILE_NAME_STATE = "state";
  private static final String FILE_NAME_RELATIONSHIP_DISTRIBUTION = "relationship-type-distribution";
  private static final String STATE_NEW_IMPORT = "";
  private static final String STATE_INIT = "init";
  private static final String STATE_START = "start";
  private static final String STATE_DATA_IMPORT = "data-import";
  private static final String STATE_DATA_LINK = "data-link";
  private static final String STATE_DEFRAGMENT = "defragment";
  private final PageCache externalPageCache;
  private final DatabaseLayout databaseLayout;
  private final FileSystemAbstraction fileSystem;
  private final Configuration config;
  private final LogService logService;
  private final Config dbConfig;
  private final RecordFormats recordFormats;
  private final ExecutionMonitor executionMonitor;
  private final AdditionalInitialIds additionalInitialIds;
  private final RelationshipTypeDistributionStorage dataStatisticsStorage;
  private final Monitor monitor;
  private final JobScheduler jobScheduler;
  private final Collector badCollector;
  private final LogFilesInitializer logFilesInitializer;

  public RestartableParallelBatchImporter(DatabaseLayout databaseLayout,
      FileSystemAbstraction fileSystem, PageCache externalPageCache, Configuration config,
      LogService logService, ExecutionMonitor executionMonitor,
      AdditionalInitialIds additionalInitialIds,
      Config dbConfig, RecordFormats recordFormats,
      Monitor monitor, JobScheduler jobScheduler, Collector badCollector,
      LogFilesInitializer logFilesInitializer) {
    this.externalPageCache = externalPageCache;
    this.databaseLayout = databaseLayout;
    this.fileSystem = fileSystem;
    this.config = config;
    this.logService = logService;
    this.dbConfig = dbConfig;
    this.recordFormats = recordFormats;
    this.executionMonitor = executionMonitor;
    this.additionalInitialIds = additionalInitialIds;
    this.monitor = monitor;
    this.dataStatisticsStorage =
        new RelationshipTypeDistributionStorage(fileSystem,
            new File(this.databaseLayout.databaseDirectory(), "relationship-type-distribution"));
    this.jobScheduler = jobScheduler;
    this.badCollector = badCollector;
    this.logFilesInitializer = logFilesInitializer;
  }

  private static void fastForwardToLastCompletedState(BatchingNeoStores store,
      StateStorage stateStore, String stateName, byte[] checkPoint,
      PrefetchingIterator<State> states) throws IOException {
    if ("".equals(stateName)) {
      store.createNew();
      stateStore.set("init", PropertyType.EMPTY_BYTE_ARRAY);
    } else {
      Set<StoreType> mainStoresToKeep = new HashSet();
      HashSet tempStoresToKeep = new HashSet();

      while (states.hasNext()) {
        State state = states.peek();
        mainStoresToKeep.addAll(Arrays.asList(state.completesMainStoreTypes()));
        tempStoresToKeep.addAll(Arrays.asList(state.completesTempStoreTypes()));
        state.load();
        if (state.name().equals(stateName)) {
          Objects.requireNonNull(mainStoresToKeep);
          Predicate n10001 = mainStoresToKeep::contains;
          Objects.requireNonNull(tempStoresToKeep);
          store.pruneAndOpenExistingStore(n10001, tempStoresToKeep::contains);
          if (checkPoint.length == 0) {
            states.next();
          }
          break;
        }

        states.next();
      }
    }
  }

  public void doImport(Input input) throws IOException {
    BatchingNeoStores store =
        ImportLogic
            .instantiateNeoStores(this.fileSystem, this.databaseLayout, this.externalPageCache,
                this.recordFormats, this.config,
                this.logService, this.additionalInitialIds, this.dbConfig, this.jobScheduler);

    try {
      ImportLogic logic =
          new ImportLogic(this.databaseLayout, store, this.config, this.dbConfig, this.logService,
              this.executionMonitor, this.recordFormats,
              this.badCollector, this.monitor);

      try {
        StateStorage stateStore = new StateStorage(this.fileSystem,
            new File(this.databaseLayout.databaseDirectory(), "state"));
        PrefetchingIterator<State> states = this.initializeStates(logic, store);
        Pair<String, byte[]> previousState = stateStore.get();
        fastForwardToLastCompletedState(store, stateStore, previousState.first(),
            previousState.other(), states);
        logic.initialize(input);
        this.runRemainingStates(store, stateStore, previousState.other(), states);
        logic.success();
      } catch (Throwable n9) {
        try {
          logic.close();
        } catch (Throwable n8) {
          n9.addSuppressed(n8);
        }

        throw n9;
      }

      logic.close();
    } catch (Throwable n10) {
      if (store != null) {
        try {
          store.close();
        } catch (Throwable n7) {
          n10.addSuppressed(n7);
        }
      }

      throw n10;
    }

    if (store != null) {
      store.close();
    }
  }

  private PrefetchingIterator<State> initializeStates(final ImportLogic logic,
      final BatchingNeoStores store) {
    List<State> states = new ArrayList();
    states.add(
        new State("init", ArrayUtil.array(new StoreType[0]), ArrayUtil.array(new StoreType[0])));
    states.add(new State("start", ArrayUtil.array(new StoreType[]{StoreType.META_DATA}),
        ArrayUtil.array(new StoreType[0])));
    states.add(new State("data-import", ArrayUtil.array(
        new StoreType[]{StoreType.NODE, StoreType.NODE_LABEL, StoreType.LABEL_TOKEN,
            StoreType.LABEL_TOKEN_NAME, StoreType.RELATIONSHIP,
            StoreType.RELATIONSHIP_TYPE_TOKEN, StoreType.RELATIONSHIP_TYPE_TOKEN_NAME,
            StoreType.PROPERTY, StoreType.PROPERTY_ARRAY,
            StoreType.PROPERTY_STRING, StoreType.PROPERTY_KEY_TOKEN,
            StoreType.PROPERTY_KEY_TOKEN_NAME}),
        ArrayUtil.array(new StoreType[0])) {
      void run(byte[] fromCheckPoint, State.CheckPointer checkPointer) throws IOException {
        logic.importNodes();
        logic.prepareIdMapper();
        logic.importRelationships();
      }

      void save() throws IOException {
        RestartableParallelBatchImporter.this.dataStatisticsStorage
            .store(logic.getState(DataStatistics.class));
      }

      void load() throws IOException {
        logic.putState(RestartableParallelBatchImporter.this.dataStatisticsStorage.load());
      }
    });
    states.add(new State("data-link", ArrayUtil.array(new StoreType[0]),
        ArrayUtil.array(new StoreType[]{StoreType.RELATIONSHIP_GROUP})) {
      void run(byte[] fromCheckPoint, State.CheckPointer checkPointer) {
        logic.calculateNodeDegrees();

        for (int type = 0; type != -1; type = logic.linkRelationships(type)) {
        }
      }
    });
    states
        .add(new State("defragment", ArrayUtil.array(new StoreType[]{StoreType.RELATIONSHIP_GROUP}),
            ArrayUtil.array(new StoreType[0])) {
          void run(byte[] fromCheckPoint, State.CheckPointer checkPointer) {
            logic.defragmentRelationshipGroups();
          }
        });
    states
        .add(new State(null, ArrayUtil.array(new StoreType[0]), ArrayUtil.array(new StoreType[0])) {
          void run(byte[] fromCheckPoint, State.CheckPointer checkPointer) {
            logic.buildCountsStore();
            RestartableParallelBatchImporter.this.logFilesInitializer
                .initializeLogFiles(RestartableParallelBatchImporter.this.dbConfig,
                    RestartableParallelBatchImporter.this.databaseLayout,
                    store.getNeoStores(),
                    RestartableParallelBatchImporter.this.fileSystem);
          }
        });
    return Iterators.prefetching(states.iterator());
  }

  private void runRemainingStates(BatchingNeoStores store, StateStorage stateStore,
      byte[] checkPoint, Iterator<State> states) throws IOException {
    while (states.hasNext()) {
      State state = states.next();
      String stateName = state.name();
      state.run(checkPoint, (cp) ->
      {
        this.writeState(store, stateStore, stateName, cp);
      });
      state.save();
      this.writeState(store, stateStore, stateName, checkPoint = PropertyType.EMPTY_BYTE_ARRAY);
    }
  }

  private void writeState(BatchingNeoStores store, StateStorage state, String stateName,
      byte[] checkPoint) throws IOException {
    store.flushAndForce();
    if (stateName != null) {
      state.set(stateName, checkPoint);
    } else {
      state.remove();
      this.dataStatisticsStorage.remove();
    }
  }
}
