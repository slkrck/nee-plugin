/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.internal.batchimport;

import java.io.File;
import java.io.IOException;
import org.neo4j.internal.batchimport.DataStatistics;
import org.neo4j.internal.batchimport.DataStatistics.RelationshipTypeCount;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.PhysicalFlushableChannel;
import org.neo4j.io.fs.ReadAheadChannel;
import org.neo4j.io.memory.BufferScope;

class RelationshipTypeDistributionStorage {

  private final FileSystemAbstraction fs;
  private final File file;

  RelationshipTypeDistributionStorage(FileSystemAbstraction fs, File file) {
    this.fs = fs;
    this.file = file;
  }

  void store(DataStatistics distribution) throws IOException {
    PhysicalFlushableChannel channel = new PhysicalFlushableChannel(this.fs.write(this.file));

    try {
      channel.putLong(distribution.getNodeCount());
      channel.putLong(distribution.getPropertyCount());
      channel.putInt(distribution.getNumberOfRelationshipTypes());

      for (RelationshipTypeCount entry : distribution) {
        channel.putInt(entry.getTypeId());
        channel.putLong(entry.getCount());
      }
    } catch (Throwable n6) {
      try {
        channel.close();
      } catch (Throwable n5) {
        n6.addSuppressed(n5);
      }

      throw n6;
    }

    channel.close();
  }

  DataStatistics load() throws IOException {
    BufferScope bufferScope = new BufferScope(ReadAheadChannel.DEFAULT_READ_AHEAD_SIZE);

    DataStatistics dataStats;
    try {
      ReadAheadChannel channel = new ReadAheadChannel(this.fs.read(this.file), bufferScope.buffer);

      try {
        long nodeCount = channel.getLong();
        long propertyCount = channel.getLong();
        RelationshipTypeCount[] entries = new RelationshipTypeCount[channel.getInt()];
        int i = 0;

        while (true) {
          if (i >= entries.length) {
            dataStats = new DataStatistics(nodeCount, propertyCount, entries);
            break;
          }

          int typeId = channel.getInt();
          long count = channel.getLong();
          entries[i] = new RelationshipTypeCount(typeId, count);
          ++i;
        }
      } catch (Throwable n14) {
        try {
          channel.close();
        } catch (Throwable n13) {
          n14.addSuppressed(n13);
        }

        throw n14;
      }

      channel.close();
    } catch (Throwable n15) {
      try {
        bufferScope.close();
      } catch (Throwable n12) {
        n15.addSuppressed(n12);
      }

      throw n15;
    }

    bufferScope.close();
    return dataStats;
  }

  void remove() {
    this.fs.deleteFile(this.file);
  }
}
