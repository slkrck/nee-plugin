/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.internal.batchimport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.StandardCopyOption;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.fs.PhysicalFlushableChannel;
import org.neo4j.io.fs.ReadAheadChannel;
import org.neo4j.io.fs.ReadPastEndException;
import org.neo4j.io.memory.BufferScope;
import org.neo4j.kernel.impl.store.PropertyType;

public class StateStorage {

  static final String NO_STATE = "";
  static final String INIT = "init";
  private final FileSystemAbstraction fs;
  private final File stateFile;
  private final File tempFile;

  StateStorage(FileSystemAbstraction fs, File stateFile) {
    this.fs = fs;
    this.stateFile = stateFile;
    this.tempFile = new File(stateFile.getAbsolutePath() + ".tmp");
  }

  public Pair<String, byte[]> get() throws IOException {
    if (!this.stateFile.exists()) {
      return Pair.of("", PropertyType.EMPTY_BYTE_ARRAY);
    } else {
      try {
        BufferScope bufferScope = new BufferScope(ReadAheadChannel.DEFAULT_READ_AHEAD_SIZE);

        Pair n5;
        try {
          ReadAheadChannel channel = new ReadAheadChannel(this.fs.read(this.stateFile),
              bufferScope.buffer);

          try {
            String name = ChannelUtils.readString(channel);
            byte[] checkPoint = new byte[channel.getInt()];
            channel.get(checkPoint, checkPoint.length);
            n5 = Pair.of(name, checkPoint);
          } catch (Throwable n8) {
            try {
              channel.close();
            } catch (Throwable n7) {
              n8.addSuppressed(n7);
            }

            throw n8;
          }

          channel.close();
        } catch (Throwable n9) {
          try {
            bufferScope.close();
          } catch (Throwable n6) {
            n9.addSuppressed(n6);
          }

          throw n9;
        }

        bufferScope.close();
        return n5;
      } catch (FileNotFoundException n10) {
        return Pair.of("", PropertyType.EMPTY_BYTE_ARRAY);
      } catch (ReadPastEndException n11) {
        return Pair.of("init", PropertyType.EMPTY_BYTE_ARRAY);
      }
    }
  }

  public void set(String name, byte[] checkPoint) throws IOException {
    this.fs.mkdirs(this.tempFile.getParentFile());
    PhysicalFlushableChannel channel = new PhysicalFlushableChannel(this.fs.write(this.tempFile));

    try {
      ChannelUtils.writeString(name, channel);
      channel.putInt(checkPoint.length);
      channel.put(checkPoint, checkPoint.length);
    } catch (Throwable n7) {
      try {
        channel.close();
      } catch (Throwable n6) {
        n7.addSuppressed(n6);
      }

      throw n7;
    }

    channel.close();
    this.fs.renameFile(this.tempFile, this.stateFile, StandardCopyOption.ATOMIC_MOVE);
  }

  public void remove() throws IOException {
    this.fs.renameFile(this.stateFile, this.tempFile, StandardCopyOption.ATOMIC_MOVE);
    this.fs.deleteFileOrThrow(this.tempFile);
  }
}
