/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.internal.batchimport;

import org.neo4j.configuration.Config;
import org.neo4j.internal.batchimport.AdditionalInitialIds;
import org.neo4j.internal.batchimport.BatchImporter;
import org.neo4j.internal.batchimport.BatchImporterFactory;
import org.neo4j.internal.batchimport.Configuration;
import org.neo4j.internal.batchimport.ImportLogic.Monitor;
import org.neo4j.internal.batchimport.LogFilesInitializer;
import org.neo4j.internal.batchimport.input.Collector;
import org.neo4j.internal.batchimport.staging.ExecutionMonitor;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.layout.DatabaseLayout;
import org.neo4j.io.pagecache.PageCache;
import org.neo4j.kernel.impl.store.format.RecordFormats;
import org.neo4j.logging.internal.LogService;
import org.neo4j.scheduler.JobScheduler;

public class RestartableBatchImporterFactory extends BatchImporterFactory {

  public RestartableBatchImporterFactory() {
    super(10);
  }

  public String getName() {
    return "restartable";
  }

  public BatchImporter instantiate(DatabaseLayout databaseLayout, FileSystemAbstraction fileSystem,
      PageCache externalPageCache, Configuration config,
      LogService logService, ExecutionMonitor executionMonitor,
      AdditionalInitialIds additionalInitialIds, Config dbConfig,
      RecordFormats recordFormats,
      Monitor monitor, JobScheduler jobScheduler, Collector badCollector,
      LogFilesInitializer logFilesInitializer) {
    return new RestartableParallelBatchImporter(databaseLayout, fileSystem, externalPageCache,
        config, logService, executionMonitor, additionalInitialIds,
        dbConfig, recordFormats, monitor, jobScheduler, badCollector, logFilesInitializer);
  }
}
