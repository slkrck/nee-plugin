/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package io.nee.fabric.eval

case class Catalog(val entries: _root_.scala.Predef.Map[org.neo4j.cypher.internal.v4_0.ast.CatalogName, io.nee.fabric.eval.Catalog.Entry]) extends scala.AnyRef with scala.Product with scala.Serializable {
  def resolve(name: org.neo4j.cypher.internal.v4_0.ast.CatalogName): io.nee.fabric.eval.Catalog.Graph = {
    null
  }

  def resolve(name: org.neo4j.cypher.internal.v4_0.ast.CatalogName, args: scala.Seq[org.neo4j.values.AnyValue]): io.nee.fabric.eval.Catalog.Graph = {
    null
  }

  def ++(that: io.nee.fabric.eval.Catalog): io.nee.fabric.eval.Catalog = {
    null
  }
}

object Catalog extends scala.AnyRef with scala.Serializable {

  def fromConfig(config: io.nee.fabric.config.FabricConfig): io.nee.fabric.eval.Catalog = {
    null
  }

  sealed trait Entry extends scala.AnyRef {
  }

  sealed trait Graph extends scala.AnyRef with Catalog.Entry {
  }

  trait View extends scala.AnyRef with Catalog.Entry {
    val arity: scala.Int
    val signature: scala.Seq[Catalog.Arg[_]]

    def eval(args: scala.Seq[org.neo4j.values.AnyValue]): Catalog.Graph

    def checkArity(args: scala.Seq[org.neo4j.values.AnyValue]): scala.Unit = {
      null
    }

    def cast[T <: org.neo4j.values.AnyValue](a: Catalog.Arg[T], v: org.neo4j.values.AnyValue, args: scala.Seq[org.neo4j.values.AnyValue]): T = {
      null.asInstanceOf[T]
    }
  }

  case class RemoteGraph(val graph: io.nee.fabric.config.FabricConfig.Graph) extends scala.AnyRef with Catalog.Graph with scala.Product with scala.Serializable {
  }

  case class Arg[T <: org.neo4j.values.AnyValue](val name: String,
                                                 val tpe: _root_.scala.Predef.Class[T]) extends scala.AnyRef with scala.Product with scala.Serializable {

  }

  case class View1[A1 <: org.neo4j.values.AnyValue](val a1: Catalog.Arg[A1])
                                                   (f: scala.Function1[A1, Catalog.Graph]) extends scala.AnyRef with Catalog.View with scala.Product with scala.Serializable {
    val arity: scala.Int = {
      0
    }
    val signature: scala.collection.Seq[Catalog.Arg[A1]] = {
      null
    }

    def eval(args: scala.Seq[org.neo4j.values.AnyValue]): Catalog.Graph = {
      null
    }
  }

}
