/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package io.nee.fabric.eval

case class UseEvaluation(val catalog: io.nee.fabric.eval.Catalog,
                         val proceduresSupplier: java.util.function.Supplier[org.neo4j.kernel.api.procedure.GlobalProcedures],
                         val signatureResolver: org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver) extends scala.AnyRef with scala.Product with scala.Serializable {
  def evaluate(originalStatement: String,
               use: org.neo4j.cypher.internal.v4_0.ast.UseGraph,
               parameters: org.neo4j.values.virtual.MapValue,
               context: java.util.Map[String, org.neo4j.values.AnyValue]): io.nee.fabric.eval.Catalog.Graph = {
    null
  }

  def evaluate(originalStatement: String,
               use: org.neo4j.cypher.internal.v4_0.ast.UseGraph,
               parameters: org.neo4j.values.virtual.MapValue,
               context: scala.collection.mutable.Map[String, org.neo4j.values.AnyValue]): io.nee.fabric.eval.Catalog.Graph = {
    null
  }

  def resolveFunctions(expr: org.neo4j.cypher.internal.v4_0.expressions.Expression): org.neo4j.cypher.internal.v4_0.expressions.Expression = {
    null
  }
}
