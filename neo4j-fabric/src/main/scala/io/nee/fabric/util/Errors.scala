/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package io.nee.fabric.util

object Errors extends scala.AnyRef {

  def openCypherSemantic(msg: String,
                         node: org.neo4j.cypher.internal.v4_0.util.ASTNode): org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticError = {
    null
  }

  def openCypherInvalidOnError(errors: scala.Seq[org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef]): scala.Unit = {
    null
  }

  def openCypherInvalid(errors: scala.Seq[org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef]): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def openCypherInvalid(error: org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def openCypherFailure(errors: scala.Seq[org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef]): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def openCypherFailure(error: org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def openCypherUnexpected(exp: String, pos: org.neo4j.cypher.internal.v4_0.util.InputPosition): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def openCypherUnexpected(exp: String,
                           got: String,
                           pos: org.neo4j.cypher.internal.v4_0.util.InputPosition): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def openCypherUnexpected(exp: String,
                           got: String,
                           in: String,
                           pos: org.neo4j.cypher.internal.v4_0.util.InputPosition): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def openCypherUnexpected(exp: String, got: org.neo4j.cypher.internal.v4_0.util.ASTNode): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def openCypherUnknownFunction(qualifiedName: String, pos: org.neo4j.cypher.internal.v4_0.util.InputPosition): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def wrongType(exp: String, got: String): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def wrongArity(exp: scala.Int, got: scala.Int, pos: org.neo4j.cypher.internal.v4_0.util.InputPosition): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def syntax(msg: String, query: String, pos: org.neo4j.cypher.internal.v4_0.util.InputPosition): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def semantic(message: String): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def ddlNotSupported(ddl: org.neo4j.cypher.internal.v4_0.ast.CatalogDDL): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def notSupported(feature: String): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def entityNotFound(kind: String, needle: String): scala.Nothing = {
    null.asInstanceOf[Nothing]
  }

  def errorContext[T](query: String, node: org.neo4j.cypher.internal.v4_0.util.ASTNode)(block: => T): T = {
    null.asInstanceOf[Nothing]
  }

  def show(n: org.neo4j.cypher.internal.v4_0.ast.CatalogName): String = {
    null
  }

  def show(t: org.neo4j.cypher.internal.v4_0.util.symbols.CypherType): String = {
    null
  }

  def show(av: org.neo4j.values.AnyValue): String = {
    null
  }

  def show(a: io.nee.fabric.eval.Catalog.Arg[_]): String = {
    null
  }

  def show(seq: scala.Seq[_]): String = {
    null
  }

  trait HasErrors extends scala.Throwable {
    def update(upd: scala.Function1[org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef, org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef]): Errors.HasErrors
  }

  case class InvalidQueryException(val errors: scala.Seq[org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef]) extends scala.RuntimeException with Errors.HasErrors with scala.Product with scala.Serializable {
    override def update(upd: scala.Function1[org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef, org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef]): Errors.InvalidQueryException = {
      null
    }
  }

  case class EvaluationFailedException(val errors: scala.Seq[org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef]) extends scala.RuntimeException with Errors.HasErrors with scala.Product with scala.Serializable {
    override def update(upd: scala.Function1[org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef, org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef]): Errors.EvaluationFailedException = {
      null
    }
  }

}
