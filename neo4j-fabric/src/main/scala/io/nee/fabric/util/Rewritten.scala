/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package io.nee.fabric.util

object Rewritten extends scala.AnyRef {

  implicit final class RewritingOps[T <: scala.AnyRef](val that: T) extends scala.AnyVal {
    def rewritten: Rewritten[T] = {
      null
    }
  }

  case class Rewritten[T <: scala.AnyRef](val that: T, val stopper: scala.Function1[scala.AnyRef, scala.Boolean] = {
    null
  }) extends scala.AnyRef with scala.Product with scala.Serializable {
    def stoppingAt(stop: scala.PartialFunction[scala.AnyRef, scala.Boolean]): Rewritten[T] = {
      null
    }

    def bottomUp(pf: scala.PartialFunction[scala.AnyRef, scala.AnyRef]): T = {
      null.asInstanceOf[T]
    }

    def topDown(pf: scala.PartialFunction[scala.AnyRef, scala.AnyRef]): T = {
      null.asInstanceOf[T]
    }
  }

}
