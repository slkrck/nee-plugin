/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package io.nee.fabric.pipeline

trait Pipeline extends scala.AnyRef {
  val parts: scala.Seq[org.neo4j.cypher.internal.v4_0.frontend.phases.Transformer[org.neo4j.cypher.internal.v4_0.frontend.phases.BaseContext, org.neo4j.cypher.internal.v4_0.frontend.phases.BaseState, org.neo4j.cypher.internal.v4_0.frontend.phases.BaseState]]
  val context: org.neo4j.cypher.internal.v4_0.frontend.phases.BaseContext
  val transformer: org.neo4j.cypher.internal.v4_0.frontend.phases.Transformer[org.neo4j.cypher.internal.v4_0.frontend.phases.BaseContext, org.neo4j.cypher.internal.v4_0.frontend.phases.BaseState, org.neo4j.cypher.internal.v4_0.frontend.phases.BaseState] = {
    null
  }
}

object Pipeline extends scala.AnyRef {

  case class Instance(val kernelMonitors: org.neo4j.monitoring.Monitors,
                      val queryText: String,
                      val signatures: org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver) extends scala.AnyRef with scala.Product with scala.Serializable {
    val parseAndPrepare: io.nee.fabric.pipeline.ParsingPipeline = {
      null
    }
    val checkAndFinalize: io.nee.fabric.pipeline.AnalysisPipeline = {
      null
    }
  }

  class BlankBaseContext(val cypherExceptionFactory: org.neo4j.cypher.internal.v4_0.util.CypherExceptionFactory,
                         val monitors: org.neo4j.cypher.internal.v4_0.frontend.phases.Monitors,
                         val tracer: org.neo4j.cypher.internal.v4_0.frontend.phases.CompilationPhaseTracer = {
                           null
                         },
                         val notificationLogger: org.neo4j.cypher.internal.v4_0.frontend.phases.InternalNotificationLogger = {
                           null
                         }) extends scala.AnyRef with org.neo4j.cypher.internal.v4_0.frontend.phases.BaseContext {
    override val errorHandler: scala.Function1[scala.Seq[org.neo4j.cypher.internal.v4_0.ast.semantics.SemanticErrorDef], scala.Unit] = {
      null
    }
  }

}
