/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package io.nee.fabric.planning

case class FabricPlan(val query: io.nee.fabric.planning.FabricQuery,
                      val queryType: io.nee.fabric.planning.QueryType,
                      val executionType: io.nee.fabric.planning.FabricPlan.ExecutionType,
                      val debugOptions: io.nee.fabric.planning.FabricPlan.DebugOptions) extends scala.AnyRef with scala.Product with scala.Serializable {
}

object FabricPlan extends scala.AnyRef with scala.Serializable {

  val EXECUTE: FabricPlan.ExecutionType = {
    null
  }
  val EXPLAIN: FabricPlan.ExecutionType = {
    null
  }
  val PROFILE: FabricPlan.ExecutionType = {
    null
  }

  sealed trait ExecutionType extends scala.AnyRef {
  }

  case class DebugOptions(val logPlan: scala.Boolean, val logRecords: scala.Boolean) extends scala.AnyRef with scala.Product with scala.Serializable {
  }

  case object Execute extends scala.AnyRef with FabricPlan.ExecutionType with scala.Product with scala.Serializable {
  }

  case object Explain extends scala.AnyRef with FabricPlan.ExecutionType with scala.Product with scala.Serializable {
  }

  case object Profile extends scala.AnyRef with FabricPlan.ExecutionType with scala.Product with scala.Serializable {
  }

  object DebugOptions extends scala.AnyRef with scala.Serializable {
    def from(debugOptions: _root_.scala.Predef.Set[String]): FabricPlan.DebugOptions = {
      null
    }
  }

}
