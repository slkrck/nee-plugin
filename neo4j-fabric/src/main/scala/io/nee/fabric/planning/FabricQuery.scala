/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package io.nee.fabric.planning

sealed trait FabricQuery extends scala.AnyRef {
  def columns: io.nee.fabric.planning.FabricQuery.Columns

  def children: scala.Seq[io.nee.fabric.planning.FabricQuery]
}

object FabricQuery extends scala.AnyRef {

  val pretty: io.nee.fabric.util.PrettyPrinting[io.nee.fabric.planning.FabricQuery] = {
    null
  }

  sealed trait LeafQuery extends scala.AnyRef with io.nee.fabric.planning.FabricQuery {
    def queryType: io.nee.fabric.planning.QueryType
  }

  sealed trait CompositeQuery extends scala.AnyRef with io.nee.fabric.planning.FabricQuery {
  }

  case class Columns(val incoming: scala.Seq[String],
                     val local: scala.Seq[String],
                     val imports: scala.Seq[String],
                     val output: scala.Seq[String]) extends scala.AnyRef with scala.Product with scala.Serializable {
  }

  case class Direct(val query: io.nee.fabric.planning.FabricQuery,
                    val columns: FabricQuery.Columns) extends scala.AnyRef with io.nee.fabric.planning.FabricQuery with scala.Product with scala.Serializable {
    def children: scala.Seq[io.nee.fabric.planning.FabricQuery] = {
      null
    }
  }

  case class Apply(val query: io.nee.fabric.planning.FabricQuery,
                   val columns: FabricQuery.Columns) extends scala.AnyRef with io.nee.fabric.planning.FabricQuery with scala.Product with scala.Serializable {
    def children: scala.Seq[io.nee.fabric.planning.FabricQuery] = {
      null
    }
  }

  case class LocalQuery(val query: org.neo4j.cypher.internal.FullyParsedQuery,
                        val columns: FabricQuery.Columns,
                        val queryType: io.nee.fabric.planning.QueryType) extends scala.AnyRef with FabricQuery.LeafQuery with scala.Product with scala.Serializable {
    def input: scala.Seq[String] = {
      null
    }

    def children: scala.Seq[io.nee.fabric.planning.FabricQuery] = {
      null
    }
  }

  case class RemoteQuery(val use: org.neo4j.cypher.internal.v4_0.ast.UseGraph,
                         val query: org.neo4j.cypher.internal.v4_0.ast.Query,
                         val columns: FabricQuery.Columns,
                         val queryType: io.nee.fabric.planning.QueryType) extends scala.AnyRef with FabricQuery.LeafQuery with scala.Product with scala.Serializable {
    def parameters: _root_.scala.Predef.Map[String, String] = {
      null
    }

    def queryString: String = {
      null
    }

    def children: scala.Seq[io.nee.fabric.planning.FabricQuery] = {
      null
    }
  }

  case class ChainedQuery(val queries: scala.Seq[io.nee.fabric.planning.FabricQuery],
                          val columns: FabricQuery.Columns) extends scala.AnyRef with FabricQuery.CompositeQuery with scala.Product with scala.Serializable {
    def children: scala.Seq[io.nee.fabric.planning.FabricQuery] = {
      null
    }
  }

  case class UnionQuery(val lhs: io.nee.fabric.planning.FabricQuery,
                        val rhs: io.nee.fabric.planning.FabricQuery,
                        val distinct: scala.Boolean,
                        val columns: FabricQuery.Columns) extends scala.AnyRef with FabricQuery.CompositeQuery with scala.Product with scala.Serializable {
    def children: scala.Seq[io.nee.fabric.planning.FabricQuery] = {
      null
    }
  }

  object Columns extends scala.AnyRef with scala.Serializable {
    def combine(left: scala.Seq[String], right: scala.Seq[String]): scala.Seq[String] = {
      null
    }

    def paramName(varName: String): String = {
      null
    }

    def fields(c: FabricQuery.Columns): scala.Seq[scala.Tuple2[String, String]] = {
      null
    }
  }

}
