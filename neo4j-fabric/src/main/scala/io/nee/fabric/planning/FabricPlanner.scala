/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package io.nee.fabric.planning

case class FabricPlanner(val config: io.nee.fabric.config.FabricConfig,
                         val cypherConfig: org.neo4j.cypher.internal.CypherConfiguration,
                         val monitors: org.neo4j.monitoring.Monitors,
                         val signatures: org.neo4j.cypher.internal.planner.spi.ProcedureSignatureResolver) extends scala.AnyRef with scala.Product with scala.Serializable {
  private[planning] val queryCache: io.nee.fabric.cache.FabricQueryCache = {
    null
  }

  def plan(query: String, parameters: org.neo4j.values.virtual.MapValue): io.nee.fabric.planning.FabricPlan = {
    null
  }
}
