/*
 * NEE Plugin 
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law. 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/


package io.nee.fabric.cache

class FabricQueryCache(size: scala.Int) extends scala.AnyRef {
  type Query = String
  type Params = org.neo4j.values.virtual.MapValue
  type ParamTypes = _root_.scala.Predef.Map[String, _root_.scala.Predef.Class[_]]
  type Key = scala.Tuple2[FabricQueryCache.this.Query, FabricQueryCache.this.ParamTypes]
  type Value = io.nee.fabric.planning.FabricPlan

  var cache: LFUCache[String, String] = new LFUCache[String, String](size)
  var hits: Long = 0L;
  var misses: Long = 0L;

  def computeIfAbsent(query: String, params: MapValue, compute: scala.Function2[String, MapValue, FabricPlan]): FabricPlan = {

    val paramTypes = QueryCache.extractParameterTypeMap(params)
    val key = "" //new Tuple2[_, _](query, paramTypes)
    val cacheObj = cache.get(key.toString());
    val result = compute.apply(query, params)
    //this.cache.put(key, result)

    return result

  }

  def getHits: scala.Long = {
    hits
  }

  def getMisses: scala.Long = {
    misses
  }
}
