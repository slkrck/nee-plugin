/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.localdb;

import io.nee.fabric.config.FabricConfig;
import java.util.UUID;
import java.util.function.Function;
import org.neo4j.common.DependencyResolver;
import org.neo4j.configuration.helpers.NormalizedDatabaseName;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.dbms.api.DatabaseNotFoundException;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.dbms.database.SystemGraphDbmsModel;
import org.neo4j.graphdb.ConstraintViolationException;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.ResourceIterator;
import org.neo4j.graphdb.Transaction;
import org.neo4j.kernel.availability.UnavailableException;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.impl.factory.GraphDatabaseFacade;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

public class FabricDatabaseManager extends LifecycleAdapter {

  private final FabricConfig fabricConfig;
  private final DependencyResolver dependencyResolver;
  private final Log log;
  private DatabaseManager<DatabaseContext> databaseManager;
  private DatabaseIdRepository databaseIdRepository;
  private DatabaseManagementService managementService;

  public FabricDatabaseManager(FabricConfig fabricConfig, DependencyResolver dependencyResolver,
      LogProvider logProvider) {
    this.dependencyResolver = dependencyResolver;
    this.fabricConfig = fabricConfig;
    this.log = logProvider.getLog(this.getClass());
  }

  public void start() {
    this.databaseManager = (DatabaseManager) this.dependencyResolver
        .resolveDependency(DatabaseManager.class);
    this.databaseIdRepository = this.databaseManager.databaseIdRepository();
  }

  public DatabaseIdRepository databaseIdRepository() {
    return this.databaseIdRepository;
  }

  public void manageFabricDatabases(GraphDatabaseService system, boolean update) {
    Transaction tx = system.beginTx();

    try {
      boolean exists = false;
      if (update) {
        exists = this.checkExisting(tx);
      }

      if (this.fabricConfig.isEnabled()) {
        NormalizedDatabaseName dbName = this.fabricConfig.getDatabase().getName();
        if (exists) {
          this.log.info("Using existing Fabric virtual database '%s'", dbName.name());
        } else {
          this.log.info("Creating Fabric virtual database '%s'", dbName.name());
          this.newFabricDb(tx, dbName);
        }
      }

      tx.commit();
    } catch (Throwable n7) {
      if (tx != null) {
        try {
          tx.close();
        } catch (Throwable n6) {
          n7.addSuppressed(n6);
        }
      }

      throw n7;
    }

    if (tx != null) {
      tx.close();
    }
  }

  public boolean isFabricDatabase(String databaseNameRaw) {
    NormalizedDatabaseName databaseName = new NormalizedDatabaseName(databaseNameRaw);
    return this.fabricConfig.isEnabled() && this.fabricConfig.getDatabase().getName()
        .equals(databaseName);
  }

  public GraphDatabaseFacade getDatabase(String databaseNameRaw) throws UnavailableException {
    GraphDatabaseFacade graphDatabaseFacade = this.databaseIdRepository.getByName(databaseNameRaw)
        .flatMap((databaseId) ->
        {
          return this.databaseManager
              .getDatabaseContext(
                  databaseId);
        }).orElseThrow(() ->
        {
          return new DatabaseNotFoundException(
              "Database " +
                  databaseNameRaw +
                  " not found");
        })
        .databaseFacade();
    if (!graphDatabaseFacade.isAvailable(0L)) {
      throw new UnavailableException("Database %s not available " + databaseNameRaw);
    } else {
      return graphDatabaseFacade;
    }
  }

  private boolean checkExisting(Transaction tx) {
    Function<ResourceIterator<Node>, Boolean> iterator = (nodes) ->
    {
      boolean found = false;

      while (true) {
        while (nodes.hasNext()) {
          Node fabricDb = nodes.next();
          Object dbName = fabricDb.getProperty("name");
          if (this.fabricConfig.isEnabled() && this.fabricConfig.getDatabase().getName().name()
              .equals(dbName)) {
            this.log.info("Setting Fabric virtual database '%s' status to online", dbName);
            fabricDb.setProperty("status", "online");
            found = true;
          } else {
            this.log.info("Setting Fabric virtual database '%s' status to offline", dbName);
            fabricDb.setProperty("status", "offline");
          }
        }

        nodes.close();
        return found;
      }
    };
    return iterator.apply(tx.findNodes(SystemGraphDbmsModel.DATABASE_LABEL, "fabric", true));
  }

  private void newFabricDb(Transaction tx, NormalizedDatabaseName dbName) {
    try {
      Node node = tx.createNode(SystemGraphDbmsModel.DATABASE_LABEL);
      node.setProperty("name", dbName.name());
      node.setProperty("uuid", UUID.randomUUID().toString());
      node.setProperty("status", "online");
      node.setProperty("default", false);
      node.setProperty("fabric", true);
    } catch (ConstraintViolationException n4) {
      throw new IllegalStateException(
          "The specified database '" + dbName.name() + "' already exists.");
    }
  }
}
