/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.bolt;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.executor.Exceptions;
import io.nee.fabric.stream.Record;
import io.nee.fabric.stream.Rx2SyncStream;
import io.nee.fabric.stream.StatementResult;
import io.nee.fabric.stream.summary.Summary;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import org.neo4j.bolt.dbapi.BoltQueryExecution;
import org.neo4j.graphdb.ExecutionPlanDescription;
import org.neo4j.graphdb.Notification;
import org.neo4j.graphdb.QueryExecutionType;
import org.neo4j.graphdb.QueryStatistics;
import org.neo4j.graphdb.Result.ResultVisitor;
import org.neo4j.kernel.api.exceptions.Status.Statement;
import org.neo4j.kernel.impl.query.QueryExecution;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class BoltQueryExecutionImpl implements BoltQueryExecution {

  private final BoltQueryExecutionImpl.QueryExecutionImpl queryExecution;

  public BoltQueryExecutionImpl(StatementResult statementResult, QuerySubscriber subscriber,
      FabricConfig fabricConfig) {
    FabricConfig.DataStream config = fabricConfig.getDataStream();
    Rx2SyncStream rx2SyncStream = new Rx2SyncStream(statementResult.records(),
        config.getBatchSize());
    this.queryExecution = new BoltQueryExecutionImpl.QueryExecutionImpl(rx2SyncStream, subscriber,
        statementResult.columns(), statementResult.summary());
  }

  public QueryExecution getQueryExecution() {
    return this.queryExecution;
  }

  public void close() {
    this.queryExecution.cancel();
  }

  public void terminate() {
    this.queryExecution.cancel();
  }

  private static class QueryExecutionImpl implements QueryExecution {

    private final Rx2SyncStream rx2SyncStream;
    private final QuerySubscriber subscriber;
    private final Mono<Summary> summary;
    private final Supplier<List<String>> columns;
    private boolean hasMore = true;
    private boolean initialised;

    private QueryExecutionImpl(Rx2SyncStream rx2SyncStream, QuerySubscriber subscriber,
        Flux<String> columns, Mono<Summary> summary) {
      this.rx2SyncStream = rx2SyncStream;
      this.subscriber = subscriber;
      this.summary = summary;
      AtomicReference<List<String>> columnsStore = new AtomicReference();
      this.columns = () ->
      {
        if (columnsStore.get() == null) {
          columnsStore.compareAndSet(null, columns.collectList().block());
        }

        return (List) columnsStore.get();
      };
    }

    private Summary getSummary() {
      return this.summary.block();
    }

    public QueryExecutionType executionType() {
      return this.getSummary().executionType();
    }

    public ExecutionPlanDescription executionPlanDescription() {
      return this.getSummary().executionPlanDescription();
    }

    public Iterable<Notification> getNotifications() {
      return this.getSummary().getNotifications();
    }

    public String[] fieldNames() {
      return (String[]) ((List) this.columns.get()).toArray(new String[0]);
    }

    public void request(long numberOfRecords) throws Exception {
      if (this.hasMore) {
        if (!this.initialised) {
          this.initialised = true;
          this.subscriber.onResult(this.columns.get().size());
        }

        try {
          for (int i = 0; (long) i < numberOfRecords; ++i) {
            Record record = this.rx2SyncStream.readRecord();
            if (record == null) {
              this.hasMore = false;
              this.subscriber.onResultCompleted(this.getSummary().getQueryStatistics());
              return;
            }

            this.subscriber.onRecord();
            this.publishFields(record);
            this.subscriber.onRecordCompleted();
          }
        } catch (Exception n5) {
          throw Exceptions.transform(Statement.ExecutionFailed, n5);
        }
      }
    }

    private void publishFields(Record record) throws Exception {
      for (int i = 0; i < this.columns.get().size(); ++i) {
        this.subscriber.onField(record.getValue(i));
      }
    }

    public void cancel() {
      this.rx2SyncStream.close();
    }

    public boolean await() {
      return this.hasMore;
    }

    public boolean isVisitable() {
      return false;
    }

    public <VisitationException extends Exception> QueryStatistics accept(
        ResultVisitor<VisitationException> visitor) {
      throw new IllegalStateException("Results are not visitable");
    }
  }
}
