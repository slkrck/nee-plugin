/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.bolt;

import io.nee.fabric.driver.RemoteBookmark;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.neo4j.bolt.dbapi.CustomBookmarkFormatParser;
import org.neo4j.bolt.runtime.Bookmark;

public class FabricBookmarkParser implements CustomBookmarkFormatParser {

  public boolean isCustomBookmark(String string) {
    return string.startsWith("FB:");
  }

  public List<Bookmark> parse(List<String> customBookmarks) {
    return customBookmarks.stream().map(this::parse).collect(Collectors.toList());
  }

  private Bookmark parse(String bookmarkString) {
    if (!this.isCustomBookmark(bookmarkString)) {
      throw new IllegalArgumentException(
          String.format("'%s' is not a valid Fabric bookmark", bookmarkString));
    } else {
      String content = bookmarkString.substring("FB:".length());
      if (content.isEmpty()) {
        return new FabricBookmark(List.of());
      } else {
        String[] graphParts = content.split("-");
        List<FabricBookmark.GraphState> remoteStates = Arrays.stream(graphParts)
            .map((rawGraphState) ->
            {
              return this.parseGraphState(bookmarkString,
                  rawGraphState);
            }).collect(Collectors.toList());
        return new FabricBookmark(remoteStates);
      }
    }
  }

  private FabricBookmark.GraphState parseGraphState(String bookmarkString, String rawGraphState) {
    String[] parts = rawGraphState.split(":");
    if (parts.length != 2) {
      throw new IllegalArgumentException(String.format("Bookmark '%s' not valid", bookmarkString));
    } else {
      long graphId;
      try {
        graphId = Long.parseLong(parts[0]);
      } catch (NumberFormatException n7) {
        throw new IllegalArgumentException(
            String.format("Could not parse graph ID in '%s'", bookmarkString), n7);
      }

      List<RemoteBookmark> remoteBookmarks =
          Arrays.stream(parts[1].split(",")).map(this::decodeRemoteBookmark)
              .collect(Collectors.toList());
      return new FabricBookmark.GraphState(graphId, remoteBookmarks);
    }
  }

  private RemoteBookmark decodeRemoteBookmark(String encodedBookmark) {
    Set<String> decodedBookmarkState = Arrays.stream(encodedBookmark.split("\\|"))
        .map((bookmarkPart) ->
        {
          return Base64.getDecoder().decode(bookmarkPart);
        }).map((decodedPart) ->
        {
          return new String(decodedPart,
              StandardCharsets.UTF_8);
        }).collect(Collectors.toSet());
    return new RemoteBookmark(decodedBookmarkState);
  }
}
