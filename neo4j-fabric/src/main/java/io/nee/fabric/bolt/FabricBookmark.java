/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.bolt;

import io.nee.fabric.driver.RemoteBookmark;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import org.neo4j.bolt.dbapi.BookmarkMetadata;
import org.neo4j.bolt.runtime.BoltResponseHandler;
import org.neo4j.bolt.runtime.Bookmark;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.values.storable.Values;

public class FabricBookmark extends BookmarkMetadata implements Bookmark {

  public static final String PREFIX = "FB:";
  private final List<FabricBookmark.GraphState> graphStates;

  public FabricBookmark(List<FabricBookmark.GraphState> graphStates) {
    super(-1L, null);
    this.graphStates = graphStates;
  }

  public long txId() {
    return this.getTxId();
  }

  public NamedDatabaseId databaseId() {
    return this.getNamedDatabaseId();
  }

  public void attachTo(BoltResponseHandler state) {
    state.onMetadata("bookmark", Values.stringValue(this.serialize()));
  }

  public List<FabricBookmark.GraphState> getGraphStates() {
    return this.graphStates;
  }

  public Bookmark toBookmark(BiFunction<Long, NamedDatabaseId, Bookmark> defaultBookmarkFormat) {
    return this;
  }

  public String toString() {
    return "FabricBookmark{graphStates=" + this.graphStates + "}";
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      FabricBookmark that = (FabricBookmark) o;
      return this.graphStates.equals(that.graphStates);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.graphStates);
  }

  public String serialize() {
    return this.graphStates.stream().map((rec) ->
    {
      return rec.erialize();
    }).collect(Collectors.joining("-", "FB:", ""));
  }

  public static class GraphState {

    private final long remoteGraphId;
    private final List<RemoteBookmark> bookmarks;

    public GraphState(long remoteGraphId, List<RemoteBookmark> bookmarks) {
      this.remoteGraphId = remoteGraphId;
      this.bookmarks = bookmarks;
    }

    public long getRemoteGraphId() {
      return this.remoteGraphId;
    }

    public List<RemoteBookmark> getBookmarks() {
      return this.bookmarks;
    }

    public String toString() {
      return "GraphState{remoteGraphId=" + this.remoteGraphId + ", bookmarks=" + this.bookmarks
          + "}";
    }

    private String serialize() {
      return this.bookmarks.stream().map(this::serialize)
          .collect(Collectors.joining(",", this.remoteGraphId + ":", ""));
    }

    private String serialize(RemoteBookmark remoteBookmark) {
      return remoteBookmark.getSerialisedState().stream().map((bookmarkPart) ->
      {
        return Base64.getEncoder().encodeToString(
            bookmarkPart.getBytes(StandardCharsets.UTF_8));
      }).collect(Collectors.joining("|"));
    }

    public boolean equals(Object o) {
      if (this == o) {
        return true;
      } else if (o != null && this.getClass() == o.getClass()) {
        FabricBookmark.GraphState that = (FabricBookmark.GraphState) o;
        return this.remoteGraphId == that.remoteGraphId && this.bookmarks.equals(that.bookmarks);
      } else {
        return false;
      }
    }

    public int hashCode() {
      return Objects.hash(this.remoteGraphId, this.bookmarks);
    }
  }
}
