/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.bolt;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.executor.FabricExecutor;
import io.nee.fabric.localdb.FabricDatabaseManager;
import io.nee.fabric.transaction.TransactionManager;
import java.time.Duration;
import java.util.Optional;
import org.neo4j.bolt.dbapi.BoltGraphDatabaseManagementServiceSPI;
import org.neo4j.bolt.dbapi.BoltGraphDatabaseServiceSPI;
import org.neo4j.bolt.dbapi.CustomBookmarkFormatParser;
import org.neo4j.bolt.txtracking.TransactionIdTracker;
import org.neo4j.dbms.api.DatabaseNotFoundException;
import org.neo4j.kernel.availability.UnavailableException;
import org.neo4j.kernel.impl.factory.GraphDatabaseFacade;

public class BoltFabricDatabaseManagementService implements BoltGraphDatabaseManagementServiceSPI {

  private final FabricBookmarkParser fabricBookmarkParser = new FabricBookmarkParser();
  private final FabricExecutor fabricExecutor;
  private final FabricConfig config;
  private final TransactionManager transactionManager;
  private final FabricDatabaseManager fabricDatabaseManager;
  private final Duration bookmarkTimeout;
  private final TransactionIdTracker transactionIdTracker;

  public BoltFabricDatabaseManagementService(FabricExecutor fabricExecutor, FabricConfig config,
      TransactionManager transactionManager,
      FabricDatabaseManager fabricDatabaseManager, Duration bookmarkTimeout,
      TransactionIdTracker transactionIdTracker) {
    this.fabricExecutor = fabricExecutor;
    this.config = config;
    this.transactionManager = transactionManager;
    this.fabricDatabaseManager = fabricDatabaseManager;
    this.bookmarkTimeout = bookmarkTimeout;
    this.transactionIdTracker = transactionIdTracker;
  }

  public BoltGraphDatabaseServiceSPI database(String databaseName)
      throws UnavailableException, DatabaseNotFoundException {
    GraphDatabaseFacade database = this.fabricDatabaseManager.getDatabase(databaseName);
    return new BoltFabricDatabaseService(database.databaseId(), this.fabricExecutor, this.config,
        this.transactionManager, this.bookmarkTimeout,
        this.transactionIdTracker);
  }

  public Optional<CustomBookmarkFormatParser> getCustomBookmarkFormatParser() {
    return Optional.of(this.fabricBookmarkParser);
  }
}
