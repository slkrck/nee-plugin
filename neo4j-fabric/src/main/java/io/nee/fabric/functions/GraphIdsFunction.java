/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.functions;

import io.nee.fabric.config.FabricConfig;
import java.util.Collections;
import java.util.List;
import org.neo4j.internal.kernel.api.procs.FieldSignature;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes;
import org.neo4j.internal.kernel.api.procs.Neo4jTypes.ListType;
import org.neo4j.internal.kernel.api.procs.QualifiedName;
import org.neo4j.internal.kernel.api.procs.UserFunctionSignature;
import org.neo4j.kernel.api.procedure.CallableUserFunction;
import org.neo4j.kernel.api.procedure.Context;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.Values;

public class GraphIdsFunction implements CallableUserFunction {

  private static final String NAME = "graphIds";
  private static final String DESCRIPTION = "List all fabric graph ids";
  private static final List<FieldSignature> ARGUMENT_TYPES = Collections.emptyList();
  private static final ListType RESULT_TYPE;

  static {
    RESULT_TYPE = Neo4jTypes.NTList(Neo4jTypes.NTInteger);
  }

  private final UserFunctionSignature signature;
  private final FabricConfig fabricConfig;

  public GraphIdsFunction(FabricConfig fabricConfig) {
    this.fabricConfig = fabricConfig;
    String namespace = fabricConfig.getDatabase().getName().name();
    this.signature =
        new UserFunctionSignature(new QualifiedName(new String[]{namespace}, "graphIds"),
            ARGUMENT_TYPES, RESULT_TYPE, null, new String[0],
            "List all fabric graph ids", true);
  }

  public UserFunctionSignature signature() {
    return this.signature;
  }

  public boolean threadSafe() {
    return true;
  }

  public AnyValue apply(Context ctx, AnyValue[] input) {
    return Values.longArray(
        this.fabricConfig.getDatabase().getGraphs().stream().mapToLong(FabricConfig.Graph::getId)
            .toArray());
  }
}
