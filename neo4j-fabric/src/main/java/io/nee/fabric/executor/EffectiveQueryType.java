/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.executor;

import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.graphdb.QueryExecutionType.QueryType;

public class EffectiveQueryType {

  public static QueryType effectiveQueryType(AccessMode requested,
      io.nee.fabric.planning.QueryType queryType) {
    if (queryType == io.nee.fabric.planning.QueryType.READ()) {
      return QueryType.READ_ONLY;
    } else if (queryType == io.nee.fabric.planning.QueryType.READ_PLUS_UNRESOLVED()) {
      switch (requested) {
        case READ:
          return QueryType.READ_ONLY;
        case WRITE:
          return QueryType.READ_WRITE;
        default:
          throw new IllegalArgumentException("Unexpected access mode: " + requested);
      }
    } else if (queryType == io.nee.fabric.planning.QueryType.WRITE()) {
      return QueryType.READ_WRITE;
    } else {
      throw new IllegalArgumentException("Unexpected query type: " + queryType);
    }
  }

  public static AccessMode effectiveAccessMode(AccessMode requested,
      io.nee.fabric.planning.QueryType queryType) {
    if (queryType == io.nee.fabric.planning.QueryType.READ()) {
      return AccessMode.READ;
    } else if (queryType == io.nee.fabric.planning.QueryType.READ_PLUS_UNRESOLVED()) {
      return requested;
    } else if (queryType == io.nee.fabric.planning.QueryType.WRITE()) {
      return AccessMode.WRITE;
    } else {
      throw new IllegalArgumentException("Unexpected query type: " + queryType);
    }
  }
}
