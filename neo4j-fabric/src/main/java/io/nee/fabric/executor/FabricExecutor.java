/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.executor;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.eval.Catalog;
import io.nee.fabric.eval.UseEvaluation;
import io.nee.fabric.planning.FabricPlan;
import io.nee.fabric.planning.FabricPlanner;
import io.nee.fabric.planning.FabricQuery;
import io.nee.fabric.planning.QueryType;
import io.nee.fabric.stream.CompletionDelegatingOperator;
import io.nee.fabric.stream.Prefetcher;
import io.nee.fabric.stream.Record;
import io.nee.fabric.stream.Records;
import io.nee.fabric.stream.StatementResult;
import io.nee.fabric.stream.summary.MergedSummary;
import io.nee.fabric.stream.summary.Summary;
import io.nee.fabric.transaction.FabricTransaction;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.cypher.internal.v4_0.ast.UseGraph;
import org.neo4j.exceptions.InvalidSemanticsException;
import org.neo4j.kernel.api.exceptions.Status.Statement;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.values.AnyValue;
import org.neo4j.values.virtual.MapValue;
import org.neo4j.values.virtual.MapValueBuilder;
import org.neo4j.values.virtual.PathValue;
import org.neo4j.values.virtual.VirtualNodeValue;
import org.neo4j.values.virtual.VirtualRelationshipValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import scala.collection.JavaConverters;

public class FabricExecutor {

  private final FabricConfig.DataStream dataStreamConfig;
  private final FabricPlanner planner;
  private final UseEvaluation useEvaluation;
  private final Log log;
  private final FabricQueryMonitoring queryMonitoring;
  private final Executor fabricWorkerExecutor;

  public FabricExecutor(FabricConfig config, FabricPlanner planner, UseEvaluation useEvaluation,
      LogProvider internalLog,
      FabricQueryMonitoring queryMonitoring, Executor fabricWorkerExecutor) {
    this.dataStreamConfig = config.getDataStream();
    this.planner = planner;
    this.useEvaluation = useEvaluation;
    this.log = internalLog.getLog(this.getClass());
    this.queryMonitoring = queryMonitoring;
    this.fabricWorkerExecutor = fabricWorkerExecutor;
  }

  public StatementResult run(FabricTransaction fabricTransaction, String statement,
      MapValue params) {
    Thread thread = Thread.currentThread();
    FabricQueryMonitoring.QueryMonitor queryMonitor =
        this.queryMonitoring
            .queryMonitor(fabricTransaction.getTransactionInfo(), statement, params, thread);
    queryMonitor.start();
    FabricPlan plan = this.planner.plan(statement, params);
    AccessMode accessMode = fabricTransaction.getTransactionInfo().getAccessMode();
    if (plan.debugOptions().logPlan()) {
      this.log.debug(String.format("Fabric plan: %s", FabricQuery.pretty().asString(plan.query())));
    }

    return fabricTransaction.execute((ctx) ->
    {
      Object execution;
      if (plan.debugOptions().logRecords()) {
        execution = new FabricExecutor.FabricLoggingStatementExecution(statement, plan, params,
            accessMode, ctx,
            this.log, queryMonitor,
            this.dataStreamConfig);
      } else {
        execution =
            new FabricExecutor.FabricStatementExecution(statement, plan, params, accessMode, ctx,
                queryMonitor,
                this.dataStreamConfig);
      }

      return ((FabricExecutor.FabricStatementExecution) execution).run();
    });
  }

  class FabricLoggingStatementExecution extends FabricExecutor.FabricStatementExecution {

    private final AtomicInteger step = new AtomicInteger(0);
    private final Log log;

    FabricLoggingStatementExecution(String originalStatement, FabricPlan plan, MapValue params,
        AccessMode accessMode,
        FabricTransaction.FabricExecutionContext ctx, Log log,
        FabricQueryMonitoring.QueryMonitor queryMonitor,
        FabricConfig.DataStream dataStreamConfig) {
      super(originalStatement, plan, params, accessMode, ctx, queryMonitor, dataStreamConfig);
      this.log = log;
    }

    Flux<Record> runLocalQuery(FabricQuery.LocalQuery query, Flux<Record> input) {
      String id = this.executionId();
      this.trace(id, "local", this.compact(query.query().description()));
      return this.traceRecords(id, super.runLocalQuery(query, input));
    }

    Flux<Record> runRemoteQueryAt(FabricConfig.Graph graph, String queryString, QueryType queryType,
        MapValue parameters) {
      String id = this.executionId();
      this.trace(id, "remote " + graph.getId(), this.compact(queryString));
      return this
          .traceRecords(id, super.runRemoteQueryAt(graph, queryString, queryType, parameters));
    }

    private String compact(String in) {
      return in.replaceAll("\\r?\\n", " ").replaceAll("\\s+", " ");
    }

    private Flux<Record> traceRecords(String id, Flux<Record> flux) {
      return flux.doOnNext((record) ->
      {
        String rec = IntStream.range(0, record.size()).mapToObj((i) ->
        {
          return record.getValue(i).toString();
        }).collect(Collectors.joining(", ", "[", "]"));
        this.trace(id, "output", rec);
      });
    }

    private void trace(String id, String event, String data) {
      this.log.debug(String.format("%s: %s: %s", id, event, data));
    }

    private String executionId() {
      String stmtId = this.idString(this.hashCode());
      String step = this.idString(this.step.getAndIncrement());
      return String.format("%s/%s", stmtId, step);
    }

    private String idString(int code) {
      return String.format("%08X", code);
    }
  }

  class FabricStatementExecution {

    private final String originalStatement;
    private final FabricPlan plan;
    private final MapValue params;
    private final FabricTransaction.FabricExecutionContext ctx;
    private final MergedSummary mergedSummary;
    private final FabricQueryMonitoring.QueryMonitor queryMonitor;
    private final Prefetcher prefetcher;

    FabricStatementExecution(String originalStatement, FabricPlan plan, MapValue params,
        AccessMode accessMode,
        FabricTransaction.FabricExecutionContext ctx,
        FabricQueryMonitoring.QueryMonitor queryMonitor,
        FabricConfig.DataStream dataStreamConfig) {
      this.originalStatement = originalStatement;
      this.plan = plan;
      this.params = params;
      this.ctx = ctx;
      this.mergedSummary = new MergedSummary(plan, accessMode);
      this.queryMonitor = queryMonitor;
      this.prefetcher = new Prefetcher(dataStreamConfig);
    }

    StatementResult run() {
      this.queryMonitor.startExecution();
      FabricQuery query = this.plan.query();
      Flux<Record> unit = Flux.just(Records.empty());
      Flux<String> columns = Flux
          .fromIterable(JavaConverters.asJavaIterable(query.columns().output()));
      Mono<Summary> summary = Mono.just(this.mergedSummary);
      Flux records;
      if (this.plan.executionType() == FabricPlan.EXPLAIN()) {
        records = Flux.empty();
      } else {
        records = this.run(query, unit);
      }

      FabricQueryMonitoring.QueryMonitor n10002 = this.queryMonitor;
      Objects.requireNonNull(n10002);
      Flux n10001 = records.doOnComplete(n10002::endSuccess);
      n10002 = this.queryMonitor;
      Objects.requireNonNull(n10002);
      return null;
    }

    Flux<Record> run(FabricQuery query, Flux<Record> input) {
      if (query instanceof FabricQuery.Direct) {
        return this.runDirectQuery((FabricQuery.Direct) query, input);
      } else if (query instanceof FabricQuery.Apply) {
        return this.runApplyQuery((FabricQuery.Apply) query, input);
      } else if (query instanceof FabricQuery.LocalQuery) {
        return this.runLocalQuery((FabricQuery.LocalQuery) query, input);
      } else if (query instanceof FabricQuery.RemoteQuery) {
        return this.runRemoteQuery((FabricQuery.RemoteQuery) query, input);
      } else if (query instanceof FabricQuery.ChainedQuery) {
        return this.runChainedQuery((FabricQuery.ChainedQuery) query, input);
      } else if (query instanceof FabricQuery.UnionQuery) {
        return this.runUnionQuery((FabricQuery.UnionQuery) query, input);
      } else {
        throw this.notImplemented("Unsupported query", query);
      }
    }

    Flux<Record> runChainedQuery(FabricQuery.ChainedQuery query, Flux<Record> input) {
      Flux<Record> previous = input;

      FabricQuery q;
      for (Iterator n4 = JavaConverters.asJavaIterable(query.queries()).iterator(); n4.hasNext();
          previous = this.run(q, previous)) {
        q = (FabricQuery) n4.next();
      }

      return previous;
    }

    Flux<Record> runUnionQuery(FabricQuery.UnionQuery query, Flux<Record> input) {
      Flux<Record> lhs = this.run(query.lhs(), input);
      Flux<Record> rhs = this.run(query.rhs(), input);
      Flux<Record> merged = Flux.merge(lhs, rhs);
      if (query.distinct()) {
        merged = merged.distinct();
      }

      return merged;
    }

    Flux<Record> runDirectQuery(FabricQuery.Direct query, Flux<Record> input) {
      return this.run(query.query(), input);
    }

    Flux<Record> runApplyQuery(FabricQuery.Apply query, Flux<Record> input) {
      return input.flatMap((inputRecord) ->
      {
        return this.run(query.query(), Flux.just(inputRecord)).map((outputRecord) ->
        {
          return Records.join(inputRecord, outputRecord);
        });
      }, FabricExecutor.this.dataStreamConfig.getConcurrency(), 1);
    }

    Flux<Record> runLocalQuery(FabricQuery.LocalQuery query, Flux<Record> input) {
      return this.ctx.getLocal()
          .run(this.queryMonitor.getMonitoredQuery(), query.query(), this.params, input).records();
    }

    Flux<Record> runRemoteQuery(FabricQuery.RemoteQuery query, Flux<Record> input) {
      String queryString = query.queryString();
      QueryType queryType = query.queryType();
      return input.flatMap((inputRecord) ->
      {
        Map<String, AnyValue> recordValues = this.recordAsMap(query, inputRecord);
        FabricConfig.Graph graph = this.evalUse(query.use(), recordValues);
        MapValue parameters = this
            .addImportParams(recordValues, JavaConverters.mapAsJavaMap(query.parameters()));
        return this.runRemoteQueryAt(graph, queryString, queryType, parameters);
      }, FabricExecutor.this.dataStreamConfig.getConcurrency(), 1);
    }

    Flux<Record> runRemoteQueryAt(FabricConfig.Graph graph, String queryString, QueryType queryType,
        MapValue parameters) {
      Flux<Record> records = this.ctx.getRemote().run(graph, queryString, queryType, parameters)
          .flatMapMany((statementResult) ->
          {
            return statementResult.records()
                .doOnComplete(
                    () ->
                    {
                      statementResult
                          .summary()
                          .subscribe(
                              this::updateSummary);
                    });
          });
      Flux<Record> recordsWithCompletionDelegation = new CompletionDelegatingOperator(records,
          FabricExecutor.this.fabricWorkerExecutor);
      return this.prefetcher.addPrefetch(recordsWithCompletionDelegation);
    }

    private Map<String, AnyValue> recordAsMap(FabricQuery.RemoteQuery query, Record inputRecord) {
      return Records.asMap(inputRecord, JavaConverters.seqAsJavaList(query.columns().incoming()));
    }

    private FabricConfig.Graph evalUse(UseGraph use, Map<String, AnyValue> record) {
      Catalog.Graph graph = FabricExecutor.this.useEvaluation
          .evaluate(this.originalStatement, use, this.params, record);
      if (graph instanceof Catalog.RemoteGraph) {
        return ((Catalog.RemoteGraph) graph).graph();
      } else {
        throw this.notImplemented("Graph was not a ShardGraph", graph.toString());
      }
    }

    private MapValue addImportParams(Map<String, AnyValue> record, Map<String, String> bindings) {
      MapValueBuilder builder = new MapValueBuilder(this.params.size() + bindings.size());
      MapValue n10000 = this.params;
      Objects.requireNonNull(builder);
      n10000.foreach(builder::add);
      bindings.forEach((var, par) ->
      {
        builder.add(par, this.validateValue(record.get(var)));
      });
      return builder.build();
    }

    private AnyValue validateValue(AnyValue value) {
      if (value instanceof VirtualNodeValue) {
        throw new FabricException(Statement.TypeError,
            "Importing node values in remote subqueries is currently not supported");
      } else if (value instanceof VirtualRelationshipValue) {
        throw new FabricException(Statement.TypeError,
            "Importing relationship values in remote subqueries is currently not supported"
        );
      } else if (value instanceof PathValue) {
        throw new FabricException(Statement.TypeError,
            "Importing path values in remote subqueries is currently not supported");
      } else {
        return value;
      }
    }

    private void updateSummary(Summary summary) {
      if (summary != null) {
        this.mergedSummary.add(summary.getQueryStatistics());
        this.mergedSummary.add(summary.getNotifications());
      }
    }

    private RuntimeException notImplemented(String msg, FabricQuery query) {
      return this.notImplemented(msg, query.toString());
    }

    private RuntimeException notImplemented(String msg, String info) {
      return new InvalidSemanticsException(msg + ": " + info);
    }
  }
}
