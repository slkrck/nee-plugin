/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.executor;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.stream.InputDataStreamImpl;
import io.nee.fabric.stream.Record;
import io.nee.fabric.stream.Rx2SyncStream;
import io.nee.fabric.stream.StatementResult;
import io.nee.fabric.stream.StatementResults;
import org.neo4j.cypher.internal.FullyParsedQuery;
import org.neo4j.cypher.internal.javacompat.ExecutionEngine;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.internal.kernel.api.exceptions.TransactionFailureException;
import org.neo4j.kernel.api.KernelTransaction;
import org.neo4j.kernel.api.exceptions.Status;
import org.neo4j.kernel.api.exceptions.Status.Statement;
import org.neo4j.kernel.api.query.ExecutingQuery;
import org.neo4j.kernel.impl.coreapi.InternalTransaction;
import org.neo4j.kernel.impl.query.QueryExecution;
import org.neo4j.kernel.impl.query.QueryExecutionKernelException;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.kernel.impl.query.TransactionalContext;
import org.neo4j.kernel.impl.query.TransactionalContextFactory;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Flux;

public class SingleStatementKernelTransaction {

  private final ExecutingQuery parentQuery;
  private final ExecutionEngine queryExecutionEngine;
  private final TransactionalContextFactory transactionalContextFactory;
  private final KernelTransaction kernelTransaction;
  private final InternalTransaction internalTransaction;
  private final FabricConfig config;
  private TransactionalContext executionContext;

  SingleStatementKernelTransaction(ExecutingQuery parentQuery, ExecutionEngine queryExecutionEngine,
      TransactionalContextFactory transactionalContextFactory,
      KernelTransaction kernelTransaction, InternalTransaction internalTransaction,
      FabricConfig config) {
    this.parentQuery = parentQuery;
    this.queryExecutionEngine = queryExecutionEngine;
    this.transactionalContextFactory = transactionalContextFactory;
    this.kernelTransaction = kernelTransaction;
    this.internalTransaction = internalTransaction;
    this.config = config;
  }

  public StatementResult run(FullyParsedQuery query, MapValue params, Flux<Record> input) {
    if (this.executionContext != null) {
      throw new IllegalStateException("This transaction can be used to execute only one statement");
    } else {
      return StatementResults.create((subscriber) ->
      {
        return this.execute(query, params, this.convert(input), subscriber);
      });
    }
  }

  private QueryExecution execute(FullyParsedQuery query, MapValue params, InputDataStream input,
      QuerySubscriber subscriber) {
    try {
      String queryText = "Internal query for Fabric query id:" + this.parentQuery.id();
      this.executionContext = this.transactionalContextFactory
          .newContext(this.internalTransaction, queryText, params);
      return this.queryExecutionEngine
          .executeQuery(query, params, this.executionContext, true, input, subscriber);
    } catch (QueryExecutionKernelException n6) {
      if (n6.getCause() == null) {
        throw Exceptions.transform(Statement.ExecutionFailed, n6);
      } else {
        throw Exceptions.transform(Statement.ExecutionFailed, n6.getCause());
      }
    }
  }

  private InputDataStream convert(Flux<Record> input) {
    return new InputDataStreamImpl(
        new Rx2SyncStream(input, this.config.getDataStream().getBatchSize()));
  }

  public void commit() {
    synchronized (this.kernelTransaction) {
      if (this.kernelTransaction.isOpen()) {
        try {
          this.closeContext();
          this.kernelTransaction.commit();
        } catch (TransactionFailureException n4) {
          throw new FabricException(n4.status(), n4);
        }
      }
    }
  }

  public void rollback() {
    synchronized (this.kernelTransaction) {
      if (this.kernelTransaction.isOpen()) {
        try {
          this.closeContext();
          this.kernelTransaction.rollback();
        } catch (TransactionFailureException n4) {
          throw new FabricException(n4.status(), n4);
        }
      }
    }
  }

  private void closeContext() {
    if (this.executionContext != null) {
      this.executionContext.close();
    }
  }

  public void markForTermination(Status reason) {
    this.kernelTransaction.markForTermination(reason);
  }
}
