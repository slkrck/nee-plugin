/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.executor;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.driver.AutoCommitStatementResult;
import io.nee.fabric.driver.DriverPool;
import io.nee.fabric.driver.FabricDriverTransaction;
import io.nee.fabric.driver.PooledDriver;
import io.nee.fabric.driver.RemoteBookmark;
import io.nee.fabric.planning.QueryType;
import io.nee.fabric.stream.StatementResult;
import io.nee.fabric.transaction.FabricTransactionInfo;
import io.nee.fabric.transaction.TransactionBookmarkManager;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.kernel.api.exceptions.Status.Fabric;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Mono;

public class FabricRemoteExecutor {

  private final DriverPool driverPool;

  public FabricRemoteExecutor(DriverPool driverPool) {
    this.driverPool = driverPool;
  }

  public FabricRemoteExecutor.FabricRemoteTransaction begin(FabricTransactionInfo transactionInfo,
      TransactionBookmarkManager bookmarkManager) {
    return new FabricRemoteExecutor.FabricRemoteTransaction(transactionInfo, bookmarkManager);
  }

  public class FabricRemoteTransaction {

    private final Object writeTransactionStartLock = new Object();
    private final FabricTransactionInfo transactionInfo;
    private final TransactionBookmarkManager bookmarkManager;
    private final Map<Long, PooledDriver> usedDrivers = new ConcurrentHashMap();
    private volatile FabricConfig.Graph writingTo;
    private volatile Mono<FabricDriverTransaction> writeTransaction;

    private FabricRemoteTransaction(FabricTransactionInfo transactionInfo,
        TransactionBookmarkManager bookmarkManager) {
      this.transactionInfo = transactionInfo;
      this.bookmarkManager = bookmarkManager;
    }

    public Mono<StatementResult> run(FabricConfig.Graph location, String query, QueryType queryType,
        MapValue params) {
      if (location.equals(this.writingTo)) {
        return this.runInWriteTransaction(query, params);
      } else {
        AccessMode requestedMode = this.transactionInfo.getAccessMode();
        AccessMode effectiveMode = EffectiveQueryType.effectiveAccessMode(requestedMode, queryType);
        if (effectiveMode == AccessMode.READ) {
          return this.runInAutoCommitReadTransaction(location, query, params);
        } else if (requestedMode == AccessMode.READ && effectiveMode == AccessMode.WRITE) {
          throw this.writeInReadError(location);
        } else {
          synchronized (this.writeTransactionStartLock) {
            if (this.writingTo != null && !this.writingTo.equals(location)) {
              throw this.multipleWriteError(location, this.writingTo);
            }

            if (this.writingTo == null) {
              this.beginWriteTransaction(location);
            }
          }

          return this.runInWriteTransaction(query, params);
        }
      }
    }

    public Mono<Void> commit() {
      if (this.writeTransaction == null) {
        this.releaseTransactionResources();
        return Mono.empty();
      } else {
        return this.writeTransaction.flatMap(FabricDriverTransaction::commit)
            .doOnSuccess((bookmark) ->
            {
              this.bookmarkManager.recordBookmarkReceivedFromGraph(
                  this.writingTo, bookmark);
            }).then().doFinally((signal) ->
            {
              this.releaseTransactionResources();
            });
      }
    }

    public Mono<Void> rollback() {
      if (this.writeTransaction == null) {
        this.releaseTransactionResources();
        return Mono.empty();
      } else {
        return this.writeTransaction.flatMap(FabricDriverTransaction::rollback)
            .doFinally((signal) ->
            {
              this.releaseTransactionResources();
            });
      }
    }

    private void beginWriteTransaction(FabricConfig.Graph location) {
      this.writingTo = location;
      PooledDriver driver = this.getDriver(location);
      this.writeTransaction = driver
          .beginTransaction(location, AccessMode.WRITE, this.transactionInfo, List.of());
    }

    private Mono<StatementResult> runInAutoCommitReadTransaction(FabricConfig.Graph location,
        String query, MapValue params) {
      PooledDriver driver = this.getDriver(location);
      List<RemoteBookmark> bookmarks = this.bookmarkManager.getBookmarksForGraph(location);
      AutoCommitStatementResult autoCommitStatementResult = driver
          .run(query, params, location, AccessMode.READ, this.transactionInfo, bookmarks);
      autoCommitStatementResult.getBookmark().subscribe((bookmark) ->
      {
        this.bookmarkManager.recordBookmarkReceivedFromGraph(location, bookmark);
      });
      return Mono.just(autoCommitStatementResult);
    }

    private Mono<StatementResult> runInWriteTransaction(String query, MapValue params) {
      return this.writeTransaction.map((rxTransaction) ->
      {
        return rxTransaction.run(query, params);
      });
    }

    private PooledDriver getDriver(FabricConfig.Graph location) {
      return this.usedDrivers.computeIfAbsent(location.getId(), (gid) ->
      {
        return FabricRemoteExecutor.this.driverPool
            .getDriver(location, this.transactionInfo.getLoginContext().subject());
      });
    }

    private FabricException writeInReadError(FabricConfig.Graph attempt) {
      return new FabricException(Fabric.AccessMode,
          "Writing in read access mode not allowed. Attempted write to %s", attempt);
    }

    private FabricException multipleWriteError(FabricConfig.Graph attempt,
        FabricConfig.Graph writingTo) {
      return new FabricException(Fabric.AccessMode,
          "Multi-shard writes not allowed. Attempted write to %s, currently writing to %s",
          attempt, writingTo);
    }

    private void releaseTransactionResources() {
      this.usedDrivers.values().forEach(PooledDriver::release);
    }
  }
}
