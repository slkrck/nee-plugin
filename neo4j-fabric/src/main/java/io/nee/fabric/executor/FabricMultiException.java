/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.executor;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class FabricMultiException extends RuntimeException {

  private final List<RuntimeException> errors;

  public FabricMultiException(List<RuntimeException> errors) {
    super(getFirst(errors).getMessage(), getFirst(errors));
    this.errors = errors;
  }

  private static RuntimeException getFirst(List<RuntimeException> errors) {
    if (errors.isEmpty()) {
      throw new IllegalArgumentException("The submitted error list cannot be empty");
    } else {
      return errors.get(0);
    }
  }

  public List<RuntimeException> getErros() {
    return this.errors;
  }

  public String getMessage() {
    return this.errors.stream().map((e) ->
    {
      String n10000 = e.getClass().getName();
      return n10000 + (e.getMessage() != null ? ": " + e.getMessage() : "");
    })
        .collect(Collectors.joining("\n",
            "A MultiException has the following " + this.errors.size() + " exceptions:\n", ""));
  }

  public void printStackTrace(PrintStream s) {
    Iterator n2 = this.errors.iterator();

    while (n2.hasNext()) {
      Throwable error = (Throwable) n2.next();
      error.printStackTrace(s);
    }
  }

  public void printStackTrace(PrintWriter s) {
    Iterator n2 = this.errors.iterator();

    while (n2.hasNext()) {
      Throwable error = (Throwable) n2.next();
      error.printStackTrace(s);
    }
  }
}
