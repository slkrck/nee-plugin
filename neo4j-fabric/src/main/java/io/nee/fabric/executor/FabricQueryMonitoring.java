/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.executor;

import io.nee.fabric.localdb.FabricDatabaseManager;
import io.nee.fabric.transaction.FabricTransactionInfo;
import java.util.Map;
import org.neo4j.common.DependencyResolver;
import org.neo4j.kernel.api.query.ExecutingQuery;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.query.QueryExecutionMonitor;
import org.neo4j.kernel.impl.util.MonotonicCounter;
import org.neo4j.memory.OptionalMemoryTracker;
import org.neo4j.monitoring.Monitors;
import org.neo4j.resources.CpuClock;
import org.neo4j.time.Clocks;
import org.neo4j.values.virtual.MapValue;

public class FabricQueryMonitoring {

  private final DependencyResolver dependencyResolver;
  private final Monitors monitors;
  private DatabaseIdRepository databaseIdRepository;

  public FabricQueryMonitoring(DependencyResolver dependencyResolver, Monitors monitors) {
    this.dependencyResolver = dependencyResolver;
    this.monitors = monitors;
  }

  FabricQueryMonitoring.QueryMonitor queryMonitor(FabricTransactionInfo transactionInfo,
      String statement, MapValue params, Thread thread) {
    return new FabricQueryMonitoring.QueryMonitor(
        this.executingQuery(transactionInfo, statement, params, thread),
        this.monitors.newMonitor(QueryExecutionMonitor.class, new String[0]));
  }

  private ExecutingQuery executingQuery(FabricTransactionInfo transactionInfo, String statement,
      MapValue params, Thread thread) {
    NamedDatabaseId namedDatabaseId = this.getDatabaseIdRepository()
        .getByName(transactionInfo.getDatabaseName()).get();
    return new FabricQueryMonitoring.FabricExecutingQuery(transactionInfo, statement, params,
        thread, namedDatabaseId);
  }

  private DatabaseIdRepository getDatabaseIdRepository() {
    if (this.databaseIdRepository == null) {
      this.databaseIdRepository =
          this.dependencyResolver.resolveDependency(FabricDatabaseManager.class)
              .databaseIdRepository();
    }

    return this.databaseIdRepository;
  }

  private static class FabricExecutingQuery extends ExecutingQuery {

    private static final MonotonicCounter internalFabricQueryIdGenerator = MonotonicCounter
        .newAtomicMonotonicCounter();
    private final String internalFabricId;

    private FabricExecutingQuery(FabricTransactionInfo transactionInfo, String statement,
        MapValue params, Thread thread, NamedDatabaseId namedDatabaseId) {
      super(-1L, transactionInfo.getClientConnectionInfo(), namedDatabaseId,
          transactionInfo.getLoginContext().subject().username(), statement, params,
          Map.of(), () ->
          {
            return 0L;
          }, () ->
          {
            return 0L;
          }, () ->
          {
            return 0L;
          }, thread.getId(), thread.getName(), Clocks.nanoClock(), CpuClock.NOT_AVAILABLE);
      this.internalFabricId = "Fabric-" + internalFabricQueryIdGenerator.incrementAndGet();
    }

    public String id() {
      return this.internalFabricId;
    }
  }

  static class QueryMonitor {

    private final ExecutingQuery executingQuery;
    private final QueryExecutionMonitor monitor;

    private QueryMonitor(ExecutingQuery executingQuery, QueryExecutionMonitor monitor) {
      this.executingQuery = executingQuery;
      this.monitor = monitor;
    }

    void start() {
      this.monitor.start(this.executingQuery);
    }

    void startExecution() {
      this.executingQuery.onCompilationCompleted(null, null, null);
      this.executingQuery.onExecutionStarted(OptionalMemoryTracker.NONE);
    }

    void endSuccess() {
      this.monitor.endSuccess(this.executingQuery);
    }

    void endFailure(Throwable failure) {
      this.monitor.endFailure(this.executingQuery, failure);
    }

    ExecutingQuery getMonitoredQuery() {
      return this.executingQuery;
    }
  }
}
