/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.driver;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.stream.Record;
import io.nee.fabric.stream.StatementResult;
import java.util.Map;
import org.neo4j.driver.async.AsyncSession;
import org.neo4j.driver.async.AsyncTransaction;
import org.neo4j.driver.async.ResultCursor;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

//import io.nee.fabric.shaded.driver.async.AsyncSession;
//import io.nee.fabric.shaded.driver.async.AsyncTransaction;
//import io.nee.fabric.shaded.driver.async.ResultCursor;

class FabricDriverAsyncTransaction implements FabricDriverTransaction {

  private final ParameterConverter parameterConverter = new ParameterConverter();
  private final AsyncTransaction asyncTransaction;
  private final AsyncSession asyncSession;
  private final FabricConfig.Graph location;

  FabricDriverAsyncTransaction(AsyncTransaction asyncTransaction, AsyncSession asyncSession,
      FabricConfig.Graph location) {
    this.asyncTransaction = asyncTransaction;
    this.asyncSession = asyncSession;
    this.location = location;
  }

  public Mono<RemoteBookmark> commit() {
    return null;
      /*
      return Mono.fromFuture(this.asyncTransaction.commitAsync().toCompletableFuture()).then(Mono.fromSupplier(() -> {
         return  Utils.convertBookmark(this.asyncSession.lastBookmark());
      })).doFinally((s) -> {
         this.asyncSession.closeAsync();
      });

       */
  }

  public Mono<Void> rollback() {
    return Mono.fromFuture(this.asyncTransaction.rollbackAsync().toCompletableFuture()).then()
        .doFinally((s) ->
        {
          this.asyncSession.closeAsync();
        });
  }

  public StatementResult run(String query, MapValue params) {
    Map<String, Object> paramMap = (Map) this.parameterConverter.convertValue(params);
    Mono<ResultCursor> statementResultCursor = null; //Mono.fromFuture(this.asyncTransaction.runAsync(query, paramMap).toCompletableFuture());
    return new FabricDriverAsyncTransaction.StatementResultImpl(statementResultCursor,
        this.location.getId());
  }

  private static class StatementResultImpl extends AbstractRemoteStatementResult {

    private final Mono<ResultCursor> statementResultCursor;
    private final RecordConverter recordConverter;

    StatementResultImpl(Mono<ResultCursor> statementResultCursor, long sourceTag) {
      super(statementResultCursor.map(ResultCursor::keys).flatMapMany(Flux::fromIterable),
          statementResultCursor.map(ResultCursor::consumeAsync).flatMap(Mono::fromCompletionStage),
          sourceTag);
      this.statementResultCursor = statementResultCursor;
      this.recordConverter = new RecordConverter(sourceTag);
    }

    protected Flux<Record> doGetRecords() {
      return this.statementResultCursor.flatMapMany((cursor) ->
      {
        return null;//Flux.from(new RecordPublisher(cursor, this.recordConverter));
      });
    }
  }
}
