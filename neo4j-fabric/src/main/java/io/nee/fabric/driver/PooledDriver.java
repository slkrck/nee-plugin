/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.driver;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.transaction.FabricTransactionInfo;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.driver.Bookmark;
import org.neo4j.driver.Driver;
import org.neo4j.driver.SessionConfig;
import org.neo4j.driver.TransactionConfig;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Mono;


public abstract class PooledDriver {

  private final Driver driver;
  private final AtomicInteger referenceCounter = new AtomicInteger();
  private final Consumer<PooledDriver> releaseCallback;
  private Instant lastUsedTimestamp;

  PooledDriver(Driver driver, Consumer<PooledDriver> releaseCallback) {
    this.driver = driver;
    this.releaseCallback = releaseCallback;
  }

  public void release() {
    this.releaseCallback.accept(this);
  }

  public abstract AutoCommitStatementResult run(String n1, MapValue n2, FabricConfig.Graph n3,
      AccessMode n4, FabricTransactionInfo n5,
      List<RemoteBookmark> n6);

  public abstract Mono<FabricDriverTransaction> beginTransaction(FabricConfig.Graph n1,
      AccessMode n2, FabricTransactionInfo n3,
      List<RemoteBookmark> n4);

  AtomicInteger getReferenceCounter() {
    return this.referenceCounter;
  }

  Instant getLastUsedTimestamp() {
    return this.lastUsedTimestamp;
  }

  void setLastUsedTimestamp(Instant lastUsedTimestamp) {
    this.lastUsedTimestamp = lastUsedTimestamp;
  }

  void close() {
    this.driver.close();
  }

  protected SessionConfig createSessionConfig(FabricConfig.Graph location, AccessMode accessMode,
      List<RemoteBookmark> bookmarks) {
    SessionConfig.Builder builder = null;//SessionConfig.builder().withDefaultAccessMode(translateAccessMode(accessMode));
    HashSet<String> mergedBookmarks = new HashSet();
    bookmarks.forEach((remoteBookmark) ->
    {
      mergedBookmarks.addAll(remoteBookmark.getSerialisedState());
    });
    builder.withBookmarks(Bookmark.from(mergedBookmarks));
    if (location.getDatabase() != null) {
      builder.withDatabase(location.getDatabase());
    }

    return builder.build();
  }

  protected TransactionConfig getTransactionConfig(FabricTransactionInfo transactionInfo) {
    return transactionInfo.getTxTimeout().equals(Duration.ZERO) ? TransactionConfig.empty()
        : TransactionConfig.builder().withTimeout(transactionInfo.getTxTimeout()).build();
  }

  private AccessMode translateAccessMode(AccessMode accessMode) {
    return accessMode == AccessMode.READ ? AccessMode.READ : AccessMode.WRITE;
  }
}
