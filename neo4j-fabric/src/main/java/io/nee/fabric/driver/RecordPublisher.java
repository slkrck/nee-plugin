/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.driver;



import io.nee.fabric.stream.Record;
import io.nee.fabric.stream.Records;
import java.util.Objects;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;
import org.neo4j.driver.async.ResultCursor;
import org.neo4j.driver.internal.util.Futures;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

class RecordPublisher implements Publisher<Record> {

  private final ResultCursor statementResultCursor;
  private final RecordConverter recordConverter;
  private final AtomicBoolean producing = new AtomicBoolean(false);
  private final AtomicLong pendingRequests = new AtomicLong();
  private Subscriber<? super Record> subscriber;

  RecordPublisher(ResultCursor statementResultCursor, RecordConverter recordConverter) {
    this.statementResultCursor = statementResultCursor;
    this.recordConverter = recordConverter;
  }

  public void subscribe(Subscriber<? super Record> subscriber) {
    this.subscriber = subscriber;
    subscriber.onSubscribe(new Subscription() {
      public void request(long l) {
        RecordPublisher.this.pendingRequests.addAndGet(l);
        RecordPublisher.this.maybeProduce();
      }

      public void cancel() {
        RecordPublisher.this.statementResultCursor.consumeAsync();
      }
    });
  }

  private void maybeProduce() {
    if (this.pendingRequests.get() != 0L && this.producing.compareAndSet(false, true)) {
      this.produce();
    }
  }

  private void produce() {
    CompletionStage<Record> recordFuture = null;//this.statementResultCursor.nextAsync();
    recordFuture.whenComplete((record, completionError) ->
    {
      Throwable error = Futures.completionExceptionCause(completionError);
      if (error != null) {
        this.subscriber.onError(error);
      } else if (record == null) {
        this.subscriber.onComplete();
      } else {
        long pending = this.pendingRequests.decrementAndGet();

        try {
          Record convertedRecord = Records.lazy(record.size(), () ->
          {
            Stream n10000 = null;//record.values().stream();
            RecordConverter n10001 = this.recordConverter;
            Objects.requireNonNull(n10001);
            return null;//Records.of((List)n10000.map(n10001::convertValue).collect(Collectors.toList()));
          });
          this.subscriber.onNext(convertedRecord);
        } catch (Throwable n7) {
          this.subscriber.onError(n7);
          return;
        }

        if (pending > 0L) {
          this.produce();
        } else {
          this.producing.set(false);
          this.maybeProduce();
        }
      }
    });
  }
}
