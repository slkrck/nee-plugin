/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.driver;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.stream.Record;
import io.nee.fabric.transaction.FabricTransactionInfo;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.driver.Driver;
import org.neo4j.driver.SessionConfig;
import org.neo4j.driver.TransactionConfig;
import org.neo4j.driver.reactive.RxResult;
import org.neo4j.driver.reactive.RxSession;
import org.neo4j.driver.reactive.RxTransaction;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/*import io.nee.fabric.shaded.driver.Driver;
import io.nee.fabric.shaded.driver.SessionConfig;
import io.nee.fabric.shaded.driver.TransactionConfig;
import io.nee.fabric.shaded.driver.reactive.RxResult;
import io.nee.fabric.shaded.driver.reactive.RxSession;
import io.nee.fabric.shaded.driver.reactive.RxTransaction;*/

class RxPooledDriver extends PooledDriver {

  private final Driver driver;

  RxPooledDriver(Driver driver, Consumer<PooledDriver> releaseCallback) {
    super(driver, releaseCallback);
    this.driver = driver;
  }

  public AutoCommitStatementResult run(String query, MapValue params, FabricConfig.Graph location,
      AccessMode accessMode,
      FabricTransactionInfo transactionInfo, List<RemoteBookmark> bookmarks) {
    SessionConfig sessionConfig = this.createSessionConfig(location, accessMode, bookmarks);
    RxSession session = this.driver.rxSession(sessionConfig);
    ParameterConverter parameterConverter = new ParameterConverter();
    Map<String, Object> paramMap = (Map) parameterConverter.convertValue(params);
    TransactionConfig transactionConfig = this.getTransactionConfig(transactionInfo);
    RxResult rxResult = session.run(query, paramMap, transactionConfig);
    return new RxPooledDriver.StatementResultImpl(session, rxResult, location.getId());
  }

  public Mono<FabricDriverTransaction> beginTransaction(FabricConfig.Graph location,
      AccessMode accessMode, FabricTransactionInfo transactionInfo,
      List<RemoteBookmark> bookmarks) {
    SessionConfig sessionConfig = this.createSessionConfig(location, accessMode, bookmarks);
    RxSession session = this.driver.rxSession(sessionConfig);
    Mono<RxTransaction> driverTransaction = this.getDriverTransaction(session, transactionInfo);
    return driverTransaction.map((tx) ->
    {
      return new FabricDriverRxTransaction(tx, session, location);
    });
  }

  private Mono<RxTransaction> getDriverTransaction(RxSession session,
      FabricTransactionInfo transactionInfo) {
    TransactionConfig transactionConfig = this.getTransactionConfig(transactionInfo);
    return Mono.from(session.beginTransaction(transactionConfig)).cache();
  }

  private static class StatementResultImpl extends AbstractRemoteStatementResult implements
      AutoCommitStatementResult {

    private final RxSession session;
    private final RxResult rxResult;
    private final CompletableFuture<RemoteBookmark> bookmarkFuture;

    public StatementResultImpl(RxSession session, RxResult rxResult, long sourceTag) {
      super(Mono.from(rxResult.keys()).flatMapMany(Flux::fromIterable),
          Mono.from(rxResult.consume()), sourceTag, session::close);
      //Flux n10001 = Mono.from(rxResult.keys()).flatMapMany(Flux::fromIterable);
      //Mono n10002 = Mono.from(rxResult.consume());
      Objects.requireNonNull(session);

      this.bookmarkFuture = new CompletableFuture();
      this.session = session;
      this.rxResult = rxResult;
    }

    public Mono<RemoteBookmark> getBookmark() {
      return Mono.fromFuture(this.bookmarkFuture);
    }

    protected Flux<Record> doGetRecords() {
      return null;
         /*
         return this.convertRxRecords(Flux.from(this.rxResult.records())).doOnComplete(() -> {
            RemoteBookmark bookmark = Utils.convertBookmark(this.session.lastBookmark());
            this.bookmarkFuture.complete(bookmark);
         });

          */
    }
  }
}
