/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.driver;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.stream.Record;
import io.nee.fabric.transaction.FabricTransactionInfo;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;
import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.driver.Driver;
import org.neo4j.driver.SessionConfig;
import org.neo4j.driver.TransactionConfig;
import org.neo4j.driver.async.AsyncSession;
import org.neo4j.driver.async.AsyncTransaction;
import org.neo4j.driver.async.ResultCursor;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/*
import io.nee.fabric.shaded.driver.Driver;
import io.nee.fabric.shaded.driver.SessionConfig;
import io.nee.fabric.shaded.driver.TransactionConfig;
import io.nee.fabric.shaded.driver.async.AsyncSession;
import io.nee.fabric.shaded.driver.async.AsyncTransaction;
import io.nee.fabric.shaded.driver.async.ResultCursor;*/

public class AsyncPooledDriver extends PooledDriver {

  private final Driver driver;

  AsyncPooledDriver(Driver driver, Consumer<PooledDriver> releaseCallback) {
    super(driver, releaseCallback);
    this.driver = driver;
  }

  public AutoCommitStatementResult run(String query, MapValue params, FabricConfig.Graph location,
      AccessMode accessMode,
      FabricTransactionInfo transactionInfo, List<RemoteBookmark> bookmarks) {
    SessionConfig sessionConfig = this.createSessionConfig(location, accessMode, bookmarks);
    AsyncSession session = this.driver.asyncSession(sessionConfig);
    ParameterConverter parameterConverter = new ParameterConverter();
    Map<String, Object> paramMap = (Map) parameterConverter.convertValue(params);
    TransactionConfig transactionConfig = this.getTransactionConfig(transactionInfo);
    Mono<ResultCursor> resultCursor = Mono
        .fromFuture(session.runAsync(query, paramMap, transactionConfig).toCompletableFuture());
    return new AsyncPooledDriver.StatementResultImpl(session, resultCursor, location.getId());
  }

  public Mono<FabricDriverTransaction> beginTransaction(FabricConfig.Graph location,
      AccessMode accessMode, FabricTransactionInfo transactionInfo,
      List<RemoteBookmark> bookmarks) {
    SessionConfig sessionConfig = this.createSessionConfig(location, accessMode, bookmarks);
    AsyncSession session = this.driver.asyncSession(sessionConfig);
    CompletionStage<AsyncTransaction> driverTransaction = this
        .getDriverTransaction(session, transactionInfo);
    return Mono.fromFuture(driverTransaction.toCompletableFuture()).map((tx) ->
    {
      return new FabricDriverAsyncTransaction(tx, session, location);
    });
  }

  private CompletionStage<AsyncTransaction> getDriverTransaction(AsyncSession session,
      FabricTransactionInfo transactionInfo) {
    TransactionConfig transactionConfig = this.getTransactionConfig(transactionInfo);
    return session.beginTransactionAsync(transactionConfig);
  }

  private static class StatementResultImpl extends AbstractRemoteStatementResult implements
      AutoCommitStatementResult {

    private final AsyncSession session;
    private final Mono<ResultCursor> statementResultCursor;
    private final RecordConverter recordConverter;
    private final CompletableFuture<RemoteBookmark> bookmarkFuture;

    public StatementResultImpl(AsyncSession session, Mono<ResultCursor> statementResultCursor,
        long sourceTag) {
      //Flux n10001 = statementResultCursor.map(ResultCursor::keys).flatMapMany(Flux::fromIterable);
      //Mono n10002 = statementResultCursor.map(ResultCursor::consumeAsync).flatMap(Mono::fromCompletionStage);
      //Objects.requireNonNull(session);
      super(statementResultCursor.map(ResultCursor::keys).flatMapMany(Flux::fromIterable),
          statementResultCursor.map(ResultCursor::consumeAsync).flatMap(Mono::fromCompletionStage),
          sourceTag, session::closeAsync);
      this.bookmarkFuture = new CompletableFuture();
      this.session = session;
      this.statementResultCursor = statementResultCursor;
      this.recordConverter = new RecordConverter(sourceTag);
    }

    protected Flux<Record> doGetRecords() {
      return this.statementResultCursor.flatMapMany((cursor) ->
      {
        return Flux.from(new RecordPublisher(cursor, this.recordConverter));
      }).doOnComplete(() ->
      {
        RemoteBookmark bookmark = Utils.convertBookmark(this.session.lastBookmark());
        this.bookmarkFuture.complete(bookmark);
      });
    }

    public Mono<RemoteBookmark> getBookmark() {
      return Mono.fromFuture(this.bookmarkFuture);
    }
  }
}
