/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.driver;

//import io.nee.fabric.shaded.driver.Value;
//import io.nee.fabric.shaded.driver.Values;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.neo4j.driver.Value;
import org.neo4j.driver.Values;
import org.neo4j.graphdb.spatial.CRS;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.BooleanValue;
import org.neo4j.values.storable.ByteArray;
import org.neo4j.values.storable.DateTimeValue;
import org.neo4j.values.storable.DateValue;
import org.neo4j.values.storable.DoubleValue;
import org.neo4j.values.storable.DurationValue;
import org.neo4j.values.storable.LocalDateTimeValue;
import org.neo4j.values.storable.LocalTimeValue;
import org.neo4j.values.storable.LongValue;
import org.neo4j.values.storable.NoValue;
import org.neo4j.values.storable.PointValue;
import org.neo4j.values.storable.StringValue;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.TimeValue;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.MapValue;

class ParameterConverter {

  Object convertValue(AnyValue serverValue) {
    if (serverValue instanceof NoValue) {
      return null;
    } else if (serverValue instanceof LongValue) {
      return this.convertLong((LongValue) serverValue);
    } else if (serverValue instanceof DoubleValue) {
      return this.convertDouble((DoubleValue) serverValue);
    } else if (serverValue instanceof BooleanValue) {
      return this.convertBoolean((BooleanValue) serverValue);
    } else if (serverValue instanceof StringValue) {
      return this.convertString((StringValue) serverValue);
    } else if (serverValue instanceof MapValue) {
      return this.convertMap((MapValue) serverValue);
    } else if (serverValue instanceof ListValue) {
      return this.convertList((ListValue) serverValue);
    } else if (serverValue instanceof ByteArray) {
      return this.convertBytes((ByteArray) serverValue);
    } else if (serverValue instanceof DateValue) {
      return this.convertDate((DateValue) serverValue);
    } else if (serverValue instanceof LocalTimeValue) {
      return this.convertLocalTime((LocalTimeValue) serverValue);
    } else if (serverValue instanceof LocalDateTimeValue) {
      return this.convertLocalDateTime((LocalDateTimeValue) serverValue);
    } else if (serverValue instanceof TimeValue) {
      return this.convertTime((TimeValue) serverValue);
    } else if (serverValue instanceof DateTimeValue) {
      return this.convertDateTime((DateTimeValue) serverValue);
    } else if (serverValue instanceof DurationValue) {
      return null; //this.convertDuration((DurationValue)serverValue);
    } else if (serverValue instanceof PointValue) {
      return null; //this.convertPoint((PointValue)serverValue);
    } else {
      throw new IllegalStateException("Unsupported type encountered: " + serverValue.getClass());
    }
  }

  private Long convertLong(LongValue serverValue) {
    return serverValue.asObjectCopy();
  }

  private Double convertDouble(DoubleValue serverValue) {
    return serverValue.asObjectCopy();
  }

  private boolean convertBoolean(BooleanValue serverValue) {
    return serverValue.booleanValue();
  }

  private byte[] convertBytes(ByteArray serverValue) {
    return serverValue.asObjectCopy();
  }

  private Map<String, Object> convertMap(MapValue serverValue) {
    Map<String, Object> driverValue = new HashMap();
    serverValue.foreach((key, value) ->
    {
      driverValue.put(key, this.convertValue(value));
    });
    return driverValue;
  }

  private List<Object> convertList(ListValue serverValue) {
    List<Object> driverValue = new ArrayList();
    serverValue.forEach((value) ->
    {
      driverValue.add(this.convertValue(value));
    });
    return driverValue;
  }

  private LocalDate convertDate(DateValue serverValue) {
    return serverValue.asObjectCopy();
  }

  private LocalTime convertLocalTime(LocalTimeValue serverValue) {
    return serverValue.asObjectCopy();
  }

  private LocalDateTime convertLocalDateTime(LocalDateTimeValue serverValue) {
    return serverValue.asObjectCopy();
  }

  private OffsetTime convertTime(TimeValue serverValue) {
    return serverValue.asObjectCopy();
  }

  private ZonedDateTime convertDateTime(DateTimeValue serverValue) {
    return serverValue.asObjectCopy();
  }

  private Value convertDuration(DurationValue serverValue) {
    return Values.isoDuration(serverValue.get(ChronoUnit.MONTHS), serverValue.get(ChronoUnit.DAYS),
        serverValue.get(ChronoUnit.SECONDS),
        (int) serverValue.get(ChronoUnit.NANOS));
  }

  private Value convertPoint(PointValue point) {
    double[] coordinate = point.coordinate();
    CRS crs = point.getCRS();
    return coordinate.length == 2 ? Values.point(crs.getCode(), coordinate[0], coordinate[1])
        : Values.point(crs.getCode(), coordinate[0], coordinate[1], coordinate[2]);
  }

  private String convertString(TextValue serverValue) {
    return serverValue.stringValue();
  }
}
