/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.driver;

import io.nee.fabric.executor.FabricException;
import io.nee.fabric.stream.Record;
import io.nee.fabric.stream.Records;
import io.nee.fabric.stream.StatementResult;
import io.nee.fabric.stream.summary.Summary;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;
import org.neo4j.driver.exceptions.ClientException;
import org.neo4j.driver.exceptions.Neo4jException;
import org.neo4j.driver.summary.ResultSummary;
import org.neo4j.kernel.api.exceptions.Status;
import org.neo4j.kernel.api.exceptions.Status.Code;
import org.neo4j.kernel.api.exceptions.Status.Fabric;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

//import io.nee.fabric.shaded.driver.exceptions.ClientException;
//import io.nee.fabric.shaded.driver.exceptions.Neo4jException;
//import io.nee.fabric.shaded.driver.summary.ResultSummary;

abstract class AbstractRemoteStatementResult implements StatementResult {

  private final Flux<String> columns;
  private final Mono<ResultSummary> summary;
  private final RecordConverter recordConverter;
  private final Runnable completionListener;
  private boolean completionInvoked;

  AbstractRemoteStatementResult(Flux<String> columns, Mono<ResultSummary> summary, long sourceTag) {
    this(columns, summary, sourceTag, () ->
    {
    });
  }

  AbstractRemoteStatementResult(Flux<String> columns, Mono<ResultSummary> summary, long sourceTag,
      Runnable completionListener) {
    this.columns = columns;
    this.summary = summary;
    this.recordConverter = new RecordConverter(sourceTag);
    this.completionListener = completionListener;
  }

  public Flux<String> columns() {
    return this.columns.onErrorMap(Neo4jException.class, this::translateError)
        .doOnError((ignored) ->
        {
          this.invokeCompletionListener();
        });
  }

  public Flux<Record> records() {
    return this.doGetRecords().onErrorMap(Neo4jException.class, this::translateError)
        .doFinally((signalType) ->
        {
          this.invokeCompletionListener();
        });
  }

  public Mono<Summary> summary() {
    return null;//this.summary.onErrorMap(Neo4jException.class, this::translateError).map(ResultSummaryWrapper::new);
  }

  protected abstract Flux<Record> doGetRecords();

  protected Flux<Record> convertRxRecords(Flux<Record> records) {
    return records.map((driverRecord) ->
    {
      return Records.lazy(driverRecord.size(), () ->
      {
        Stream n10000 = null;//driverRecord.values().stream();
        RecordConverter n10001 = this.recordConverter;
        Objects.requireNonNull(n10001);
        return null; //Records.of((List)n10000.map(n10001::convertValue).collect(Collectors.toList()));
      });
    });
  }

  private void invokeCompletionListener() {
    if (!this.completionInvoked) {
      this.completionListener.run();
      this.completionInvoked = true;
    }
  }

  private FabricException translateError(Neo4jException driverException) {
    if (driverException instanceof ClientException) {
      Optional<Status> serverCode = Code.all().stream().filter((code) ->
      {
        return code.code().serialize().equals(driverException.code());
      }).findAny();
      return serverCode.isEmpty() ? this.genericRemoteFailure(driverException)
          : new FabricException(serverCode.get(), driverException.getMessage(), driverException);
    } else {
      return this.genericRemoteFailure(driverException);
    }
  }

  private FabricException genericRemoteFailure(Neo4jException driverException) {
    throw new FabricException(Fabric.RemoteExecutionFailed,
        String
            .format("Remote execution failed with code %s and message '%s'", driverException.code(),
                driverException.getMessage()),
        driverException);
  }
}
