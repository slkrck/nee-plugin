/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.driver;


import io.nee.fabric.stream.summary.EmptySummary;
import java.util.Collection;
import java.util.List;
import org.neo4j.driver.summary.ResultSummary;
import org.neo4j.driver.summary.SummaryCounters;
import org.neo4j.graphdb.InputPosition;
import org.neo4j.graphdb.Notification;
import org.neo4j.graphdb.QueryStatistics;
import org.neo4j.graphdb.SeverityLevel;

public class ResultSummaryWrapper extends EmptySummary {

  private final QueryStatistics statistics;
  private final List<Notification> notifications;

  public ResultSummaryWrapper(ResultSummary summary) {
    this.statistics = null;
    this.notifications = null;
  }

  public Collection<Notification> getNotifications() {
    return this.notifications;
  }

  public QueryStatistics getQueryStatistics() {
    return this.statistics;
  }

  public static class SummaryCountersWrapper implements QueryStatistics {

    private final SummaryCounters counters;

    SummaryCountersWrapper(SummaryCounters counters) {
      this.counters = counters;
    }

    public int getNodesCreated() {
      return this.counters.nodesCreated();
    }

    public int getNodesDeleted() {
      return this.counters.nodesDeleted();
    }

    public int getRelationshipsCreated() {
      return this.counters.relationshipsCreated();
    }

    public int getRelationshipsDeleted() {
      return this.counters.relationshipsDeleted();
    }

    public int getPropertiesSet() {
      return this.counters.propertiesSet();
    }

    public int getLabelsAdded() {
      return this.counters.labelsAdded();
    }

    public int getLabelsRemoved() {
      return this.counters.labelsRemoved();
    }

    public int getIndexesAdded() {
      return this.counters.indexesAdded();
    }

    public int getIndexesRemoved() {
      return this.counters.indexesRemoved();
    }

    public int getConstraintsAdded() {
      return this.counters.constraintsAdded();
    }

    public int getConstraintsRemoved() {
      return this.counters.constraintsRemoved();
    }

    public int getSystemUpdates() {
      return 0;
    }

    public boolean containsUpdates() {
      return this.counters.containsUpdates();
    }

    public boolean containsSystemUpdates() {
      return false;
    }
  }

  static class NotificationWrapper implements Notification {

    private final Notification notification;

    NotificationWrapper(Notification notification) {
      this.notification = notification;
    }

    public String getCode() {
      return null;//notification.code();
    }

    public String getTitle() {
      return null;// notification.title();
    }

    public String getDescription() {
      return null;// notification.description();
    }

    public SeverityLevel getSeverity() {
      return null; //SeverityLevel.valueOf(notification.severity());
    }

    public InputPosition getPosition() {
      InputPosition pos = this.notification.getPosition();
      return pos;
    }
  }
}
