/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.driver;

import io.nee.fabric.config.FabricConfig;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.function.Function;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.ssl.SslPolicyScope;
import org.neo4j.driver.internal.security.SecurityPlan;
import org.neo4j.logging.Level;
import org.neo4j.ssl.SslPolicy;
import org.neo4j.ssl.config.SslPolicyLoader;

/*import io.nee.fabric.shaded.driver.Logging;
import io.nee.fabric.shaded.driver.internal.security.SecurityPlan;
import io.nee.fabric.shaded.driver.net.ServerAddress;*/

class DriverConfigFactory {

  private final FabricConfig fabricConfig;
  private final Level serverLogLevel;
  private final SSLContext sslContext;
  private final SslPolicy sslPolicy;

  DriverConfigFactory(FabricConfig fabricConfig, Config serverConfig,
      SslPolicyLoader sslPolicyLoader) {
    this.fabricConfig = fabricConfig;
    this.serverLogLevel = serverConfig.get(GraphDatabaseSettings.store_internal_log_level);
    if (sslPolicyLoader.hasPolicyForSource(SslPolicyScope.FABRIC)) {
      this.sslPolicy = sslPolicyLoader.getPolicy(SslPolicyScope.FABRIC);
      this.sslContext = this.createSslContext(this.sslPolicy);
    } else {
      this.sslPolicy = null;
      this.sslContext = null;
    }
  }

  Config createConfig(FabricConfig.Graph graph) {
      /*Config.ConfigBuilder builder = Config.builder();
      Boolean logLeakedSessions = (Boolean)this.getProperty(graph, FabricConfig.DriverConfig::getLogLeakedSessions);
      if (logLeakedSessions) {
         builder.withLeakedSessionsLogging();
      }

      Duration idleTimeBeforeConnectionTest = (Duration)this.getProperty(graph, FabricConfig.DriverConfig::getIdleTimeBeforeConnectionTest);
      if (idleTimeBeforeConnectionTest != null) {
         builder.withConnectionLivenessCheckTimeout(idleTimeBeforeConnectionTest.toMillis(), TimeUnit.MILLISECONDS);
      } else {
         builder.withConnectionLivenessCheckTimeout(-1L, TimeUnit.MILLISECONDS);
      }

      Duration maxConnectionLifetime = (Duration)this.getProperty(graph, FabricConfig.DriverConfig::getMaxConnectionLifetime);
      if (maxConnectionLifetime != null) {
         builder.withMaxConnectionLifetime(maxConnectionLifetime.toMillis(), TimeUnit.MILLISECONDS);
      }

      Duration connectionAcquisitionTimeout = (Duration)this.getProperty(graph, FabricConfig.DriverConfig::getConnectionAcquisitionTimeout);
      if (connectionAcquisitionTimeout != null) {
         builder.withConnectionAcquisitionTimeout(connectionAcquisitionTimeout.toMillis(), TimeUnit.MILLISECONDS);
      }

      Duration connectTimeout = (Duration)this.getProperty(graph, FabricConfig.DriverConfig::getConnectTimeout);
      if (connectTimeout != null) {
         builder.withConnectionTimeout(connectTimeout.toMillis(), TimeUnit.MILLISECONDS);
      }

      Integer maxConnectionPoolSize = (Integer)this.getProperty(graph, FabricConfig.DriverConfig::getMaxConnectionPoolSize);
      if (maxConnectionPoolSize != null) {
         builder.withMaxConnectionPoolSize(maxConnectionPoolSize);
      }

      Set<ServerAddress> serverAddresses = (Set)graph.getUri().getAddresses().stream().map((address) -> {
         return ServerAddress.of(address.getHostname(), address.getPort());
      }).collect(Collectors.toSet());
      return builder.withResolver((mainAddress) -> {
         return serverAddresses;
      }).withLogging(Logging.javaUtilLogging(this.getLoggingLevel(graph))).build();

       */
    return null;
  }

  SecurityPlan createSecurityPlan(FabricConfig.Graph graph) {
    return this.sslPolicy != null && graph.getDriverConfig().isSslEnabled()
        ? new DriverConfigFactory.SecurityPlanImpl(true, this.sslContext,
        this.sslPolicy.isVerifyHostname())
        : new DriverConfigFactory.SecurityPlanImpl(false, null, false);
  }

  <T> T getProperty(FabricConfig.Graph graph, Function<FabricConfig.DriverConfig, T> extractor) {
    FabricConfig.GraphDriverConfig graphDriverConfig = graph.getDriverConfig();
    if (graphDriverConfig != null) {
      T configValue = extractor.apply(graphDriverConfig);
      if (configValue != null) {
        return configValue;
      }
    }

    return extractor.apply(this.fabricConfig.getGlobalDriverConfig().getDriverConfig());
  }

  private java.util.logging.Level getLoggingLevel(FabricConfig.Graph graph) {
    Level loggingLevel = this.getProperty(graph, FabricConfig.DriverConfig::getLoggingLevel);
    if (loggingLevel == null) {
      loggingLevel = this.serverLogLevel;
    }

    switch (loggingLevel) {
      case NONE:
        return java.util.logging.Level.OFF;
      case ERROR:
        return java.util.logging.Level.SEVERE;
      case WARN:
        return java.util.logging.Level.WARNING;
      case INFO:
        return java.util.logging.Level.INFO;
      case DEBUG:
        return java.util.logging.Level.FINE;
      default:
        throw new IllegalArgumentException("Unexpected logging level: " + loggingLevel);
    }
  }

  private SSLContext createSslContext(SslPolicy sslPolicy) {
    try {
      KeyManagerFactory keyManagerFactory = null;
      if (sslPolicy.privateKey() != null && sslPolicy.certificateChain() != null) {
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(null, null);
        ks.setKeyEntry("client-private-key", sslPolicy.privateKey(), null,
            sslPolicy.certificateChain());
        keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(ks, null);
      }

      TrustManagerFactory trustManagerFactory = sslPolicy.getTrustManagerFactory();
      SSLContext ctx = SSLContext.getInstance("TLS");
      KeyManager[] keyManagers =
          keyManagerFactory == null ? null : keyManagerFactory.getKeyManagers();
      TrustManager[] trustManagers =
          trustManagerFactory == null ? null : trustManagerFactory.getTrustManagers();
      ctx.init(keyManagers, trustManagers, null);
      return ctx;
    } catch (IOException | GeneralSecurityException n7) {
      throw new IllegalArgumentException("Failed to build SSL context", n7);
    }
  }

  private final class SecurityPlanImpl implements SecurityPlan {

    private final boolean requiresEncryption;
    private final SSLContext sslContext;
    private final boolean requiresHostnameVerification;

    SecurityPlanImpl(boolean requiresEncryption, SSLContext sslContext,
        boolean requiresHostnameVerification) {
      this.requiresEncryption = requiresEncryption;
      this.sslContext = sslContext;
      this.requiresHostnameVerification = requiresHostnameVerification;
    }

    public boolean requiresEncryption() {
      return this.requiresEncryption;
    }

    public SSLContext sslContext() {
      return this.sslContext;
    }

    public boolean requiresHostnameVerification() {
      return this.requiresHostnameVerification;
    }
  }
}
