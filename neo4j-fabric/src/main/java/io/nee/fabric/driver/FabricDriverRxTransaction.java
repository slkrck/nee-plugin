/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.driver;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.stream.Record;
import io.nee.fabric.stream.StatementResult;
import java.util.Map;
import org.neo4j.driver.reactive.RxResult;
import org.neo4j.driver.reactive.RxSession;
import org.neo4j.driver.reactive.RxTransaction;
import org.neo4j.values.virtual.MapValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

//import io.nee.fabric.shaded.driver.reactive.RxResult;
//import io.nee.fabric.shaded.driver.reactive.RxSession;
//import io.nee.fabric.shaded.driver.reactive.RxTransaction;

class FabricDriverRxTransaction implements FabricDriverTransaction {

  private final ParameterConverter parameterConverter = new ParameterConverter();
  private final RxTransaction rxTransaction;
  private final RxSession rxSession;
  private final FabricConfig.Graph location;

  FabricDriverRxTransaction(RxTransaction rxTransaction, RxSession rxSession,
      FabricConfig.Graph location) {
    this.rxTransaction = rxTransaction;
    this.rxSession = rxSession;
    this.location = location;
  }

  public Mono<RemoteBookmark> commit() {
    return null;
      /*
      return Mono.from(this.rxTransaction.commit()).then(Mono.fromSupplier(() -> {
         return Utils.convertBookmark(this.rxSession.lastBookmark());
      })).doFinally((s) -> {
         this.rxSession.close();
      });

       */
  }

  public Mono<Void> rollback() {
    return Mono.from(this.rxTransaction.rollback()).then().doFinally((s) ->
    {
      this.rxSession.close();
    });
  }

  public StatementResult run(String query, MapValue params) {
    Map<String, Object> paramMap = (Map) this.parameterConverter.convertValue(params);
    RxResult rxStatementResult = this.rxTransaction.run(query, paramMap);
    return new FabricDriverRxTransaction.StatementResultImpl(rxStatementResult,
        this.location.getId());
  }

  private static class StatementResultImpl extends AbstractRemoteStatementResult {

    private final RxResult rxStatementResult;

    StatementResultImpl(RxResult rxStatementResult, long sourceTag) {
      super(Mono.from(rxStatementResult.keys()).flatMapMany(Flux::fromIterable),
          Mono.from(rxStatementResult.consume()), sourceTag);
      this.rxStatementResult = rxStatementResult;
    }

    protected Flux<Record> doGetRecords() {
      return null; //this.convertRxRecords(Flux.from(this.rxStatementResult.records()));
    }
  }
}
