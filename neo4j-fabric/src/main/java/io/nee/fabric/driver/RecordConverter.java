/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.driver;
/*
import io.nee.fabric.shaded.driver.Value;
import io.nee.fabric.shaded.driver.types.IsoDuration;
import io.nee.fabric.shaded.driver.types.MapAccessor;
import io.nee.fabric.shaded.driver.types.Node;
import io.nee.fabric.shaded.driver.types.Path;
import io.nee.fabric.shaded.driver.types.Point;
import io.nee.fabric.shaded.driver.types.Relationship;

 */

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.neo4j.driver.Value;
import org.neo4j.driver.types.IsoDuration;
import org.neo4j.driver.types.MapAccessor;
import org.neo4j.driver.types.Node;
import org.neo4j.driver.types.Path;
import org.neo4j.driver.types.Point;
import org.neo4j.driver.types.Relationship;
import org.neo4j.internal.helpers.collection.Iterables;
import org.neo4j.values.AnyValue;
import org.neo4j.values.storable.BooleanValue;
import org.neo4j.values.storable.ByteArray;
import org.neo4j.values.storable.CoordinateReferenceSystem;
import org.neo4j.values.storable.DateTimeValue;
import org.neo4j.values.storable.DateValue;
import org.neo4j.values.storable.DoubleValue;
import org.neo4j.values.storable.DurationValue;
import org.neo4j.values.storable.LocalDateTimeValue;
import org.neo4j.values.storable.LocalTimeValue;
import org.neo4j.values.storable.LongValue;
import org.neo4j.values.storable.PointValue;
import org.neo4j.values.storable.TextArray;
import org.neo4j.values.storable.TextValue;
import org.neo4j.values.storable.TimeValue;
import org.neo4j.values.storable.Values;
import org.neo4j.values.virtual.ListValue;
import org.neo4j.values.virtual.MapValue;
import org.neo4j.values.virtual.NodeValue;
import org.neo4j.values.virtual.PathValue;
import org.neo4j.values.virtual.RelationshipValue;
import org.neo4j.values.virtual.VirtualValues;

class RecordConverter {

  private static final int ID_MAX_BITS = 50;
  private static final long TAG_MAX_VALUE = 16383L;
  private final boolean hasSourceTag;
  private final long sourceTagValue;

  RecordConverter() {
    this.hasSourceTag = true;
    this.sourceTagValue = 0L;
  }

  RecordConverter(long sourceTag) {
    if (sourceTag >= 0L && 16383L >= sourceTag) {
      this.hasSourceTag = true;
      this.sourceTagValue = this.shiftToMsb(sourceTag);
    } else {
      throw new IllegalArgumentException("Source tags must be in range 0-16383. Got: " + sourceTag);
    }
  }

  private long shiftToMsb(long value) {
    return value << 50;
  }

  AnyValue convertValue(Value driverValue) {
    Object value = driverValue.asObject();
    return this.convertValue(value);
  }

  private AnyValue convertValue(Object driverValue) {
    if (driverValue == null) {
      return Values.NO_VALUE;
    } else if (driverValue instanceof Node) {
      return this.convertNode((Node) driverValue);
    } else if (driverValue instanceof Relationship) {
      return this.convertRelationship((Relationship) driverValue);
    } else if (driverValue instanceof Path) {
      return this.convertPath((Path) driverValue);
    } else if (driverValue instanceof Long) {
      return this.convertLong((Long) driverValue);
    } else if (driverValue instanceof Double) {
      return this.convertDouble((Double) driverValue);
    } else if (driverValue instanceof Boolean) {
      return this.convertBoolean((Boolean) driverValue);
    } else if (driverValue instanceof String) {
      return this.convertString((String) driverValue);
    } else if (driverValue instanceof Map) {
      return this.convertMap((Map) driverValue);
    } else if (driverValue instanceof List) {
      return this.convertList((List) driverValue);
    } else if (driverValue instanceof byte[]) {
      return this.convertBytes((byte[]) driverValue);
    } else if (driverValue instanceof LocalDate) {
      return this.convertDate((LocalDate) driverValue);
    } else if (driverValue instanceof LocalTime) {
      return this.convertLocalTime((LocalTime) driverValue);
    } else if (driverValue instanceof LocalDateTime) {
      return this.convertLocalDateTime((LocalDateTime) driverValue);
    } else if (driverValue instanceof OffsetTime) {
      return this.convertTime((OffsetTime) driverValue);
    } else if (driverValue instanceof ZonedDateTime) {
      return this.convertDateTime((ZonedDateTime) driverValue);
    } else if (driverValue instanceof IsoDuration) {
      return this.convertDuration((IsoDuration) driverValue);
    } else if (driverValue instanceof Point) {
      return this.convertPoint((Point) driverValue);
    } else {
      throw new IllegalStateException("Unsupported type encountered: " + driverValue.getClass());
    }
  }

  private LongValue convertLong(Long driverValue) {
    return Values.longValue(driverValue);
  }

  private DoubleValue convertDouble(Double driverValue) {
    return Values.doubleValue(driverValue);
  }

  private BooleanValue convertBoolean(Boolean driverValue) {
    return Values.booleanValue(driverValue);
  }

  private ByteArray convertBytes(byte[] driverValue) {
    return Values.byteArray(driverValue);
  }

  private long convertId(long driverValue) {
    return this.hasSourceTag ? driverValue | this.sourceTagValue : driverValue;
  }

  private NodeValue convertNode(Node driverValue) {
    String[] labels = Iterables.asArray(String.class, driverValue.labels());
    TextArray serverLabels = this.convertStringArray(labels);
    MapValue properties = this.convertMap(driverValue);
    return VirtualValues.nodeValue(this.convertId(driverValue.id()), serverLabels, properties);
  }

  private RelationshipValue convertRelationship(Relationship driverValue) {
    NodeValue startNode = this.convertNodeReference(this.convertId(driverValue.startNodeId()));
    NodeValue endNode = this.convertNodeReference(this.convertId(driverValue.endNodeId()));
    return this.convertRelationship(driverValue, startNode, endNode);
  }

  private RelationshipValue convertRelationship(Relationship driverValue, NodeValue startNode,
      NodeValue endNode) {
    TextValue type = this.convertString(driverValue.type());
    MapValue properties = this.convertMap(driverValue);
    return VirtualValues
        .relationshipValue(this.convertId(driverValue.id()), startNode, endNode, type, properties);
  }

  private MapValue convertMap(MapAccessor driverValue) {
    String[] keys = new String[driverValue.size()];
    AnyValue[] values = new AnyValue[driverValue.size()];
    int i = 0;

    for (Iterator n5 = driverValue.keys().iterator(); n5.hasNext(); ++i) {
      String key = (String) n5.next();
      keys[i] = key;
      values[i] = this.convertValue(driverValue.get(key));
    }

    return VirtualValues.map(keys, values);
  }

  private PathValue convertPath(Path driverValue) {
    Map<Long, NodeValue> encounteredNodes = new HashMap(driverValue.length() + 1);
    NodeValue[] nodes = new NodeValue[driverValue.length() + 1];
    RelationshipValue[] relationships = new RelationshipValue[driverValue.length()];
    int i = 0;

    Iterator n6;
    NodeValue startNode;
    for (n6 = driverValue.nodes().iterator(); n6.hasNext(); ++i) {
      Node driverNode = (Node) n6.next();
      startNode = this.convertNode(driverNode);
      nodes[i] = startNode;
      encounteredNodes.put(driverNode.id(), startNode);
    }

    i = 0;

    for (n6 = driverValue.relationships().iterator(); n6.hasNext(); ++i) {
      Relationship driverRelationship = (Relationship) n6.next();
      startNode = encounteredNodes.get(driverRelationship.startNodeId());
      NodeValue endNode = encounteredNodes.get(driverRelationship.endNodeId());
      RelationshipValue relationship = this
          .convertRelationship(driverRelationship, startNode, endNode);
      relationships[i] = relationship;
    }

    return VirtualValues.path(nodes, relationships);
  }

  private NodeValue convertNodeReference(long nodeId) {
    return VirtualValues.nodeValue(nodeId, this.convertStringArray(new String[0]),
        this.convertMap(Collections.emptyMap()));
  }

  private TextArray convertStringArray(String[] driverValue) {
    return Values.stringArray(driverValue);
  }

  private MapValue convertMap(Map<String, Object> driverValue) {
    String[] keys = new String[driverValue.size()];
    AnyValue[] values = new AnyValue[driverValue.size()];
    int i = 0;

    for (Iterator n5 = driverValue.entrySet().iterator(); n5.hasNext(); ++i) {
      Entry<String, Object> e = (Entry) n5.next();
      keys[i] = e.getKey();
      values[i] = this.convertValue(e.getValue());
    }

    return VirtualValues.map(keys, values);
  }

  private ListValue convertList(List<Object> driverValue) {
    AnyValue[] listValues = new AnyValue[driverValue.size()];

    for (int i = 0; i < driverValue.size(); ++i) {
      listValues[i] = this.convertValue(driverValue.get(i));
    }

    return VirtualValues.list(listValues);
  }

  private DateValue convertDate(LocalDate driverValue) {
    return DateValue.date(driverValue);
  }

  private LocalTimeValue convertLocalTime(LocalTime driverValue) {
    return LocalTimeValue.localTime(driverValue);
  }

  private LocalDateTimeValue convertLocalDateTime(LocalDateTime driverValue) {
    return LocalDateTimeValue.localDateTime(driverValue);
  }

  private TimeValue convertTime(OffsetTime driverValue) {
    return TimeValue.time(driverValue);
  }

  private DateTimeValue convertDateTime(ZonedDateTime driverValue) {
    return DateTimeValue.datetime(driverValue);
  }

  private DurationValue convertDuration(IsoDuration driverValue) {
    return DurationValue.duration(driverValue.months(), driverValue.days(), driverValue.seconds(),
        driverValue.nanoseconds());
  }

  private PointValue convertPoint(Point point) {
    CoordinateReferenceSystem coordinateReferenceSystem = CoordinateReferenceSystem
        .get(point.srid());
    return Double.isNaN(point.z()) ? Values
        .pointValue(coordinateReferenceSystem, point.x(), point.y())
        : Values.pointValue(coordinateReferenceSystem, point.x(), point.y(), point.z());
  }

  private TextValue convertString(String driverValue) {
    return Values.stringValue(driverValue);
  }
}
