/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.bootstrap;

import io.nee.fabric.auth.CredentialsProvider;
import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.driver.DriverPool;
import io.nee.fabric.eval.Catalog;
import io.nee.fabric.eval.UseEvaluation;
import io.nee.fabric.executor.FabricExecutor;
import io.nee.fabric.executor.FabricLocalExecutor;
import io.nee.fabric.executor.FabricQueryMonitoring;
import io.nee.fabric.executor.FabricRemoteExecutor;
import io.nee.fabric.functions.GraphIdsFunction;
import io.nee.fabric.localdb.FabricDatabaseManager;
import io.nee.fabric.pipeline.SignatureResolver;
import io.nee.fabric.planning.FabricPlanner;
import io.nee.fabric.transaction.TransactionManager;
import java.time.Clock;
import java.util.concurrent.Executor;
import java.util.function.Supplier;
import org.neo4j.collection.Dependencies;
import org.neo4j.configuration.Config;
import org.neo4j.cypher.internal.CypherConfiguration;
import org.neo4j.exceptions.KernelException;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.LogProvider;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;
import org.neo4j.ssl.config.SslPolicyLoader;

public class FabricServicesBootstrap {

  private final FabricConfig fabricConfig;

  public FabricServicesBootstrap(LifeSupport lifeSupport, Dependencies dependencies,
      LogService logService) {
    FabricServicesBootstrap.ServiceBootstrapper serviceBootstrapper = new FabricServicesBootstrap.ServiceBootstrapper(
        lifeSupport, dependencies);
    LogProvider internalLogProvider = logService.getInternalLogProvider();
    Config config = dependencies.resolveDependency(Config.class);
    this.fabricConfig = serviceBootstrapper
        .registerService(FabricConfig.from(config), FabricConfig.class);
    FabricDatabaseManager fabricDatabaseManager =
        serviceBootstrapper.registerService(
            new FabricDatabaseManager(this.fabricConfig, dependencies, internalLogProvider),
            FabricDatabaseManager.class);
    if (this.fabricConfig.isEnabled()) {
      JobScheduler jobScheduler = dependencies.resolveDependency(JobScheduler.class);
      Monitors monitors = dependencies.resolveDependency(Monitors.class);
      Supplier<GlobalProcedures> proceduresSupplier = dependencies
          .provideDependency(GlobalProcedures.class);
      CredentialsProvider credentialsProvider =
          serviceBootstrapper.registerService(new CredentialsProvider(), CredentialsProvider.class);
      SslPolicyLoader sslPolicyLoader = dependencies.resolveDependency(SslPolicyLoader.class);
      DriverPool driverPool = serviceBootstrapper.registerService(
          new DriverPool(jobScheduler, this.fabricConfig, config, Clock.systemUTC(),
              credentialsProvider, sslPolicyLoader), DriverPool.class);
      serviceBootstrapper
          .registerService(new FabricRemoteExecutor(driverPool), FabricRemoteExecutor.class);
      serviceBootstrapper
          .registerService(new FabricLocalExecutor(this.fabricConfig, fabricDatabaseManager),
              FabricLocalExecutor.class);
      serviceBootstrapper
          .registerService(new TransactionManager(dependencies), TransactionManager.class);
      CypherConfiguration cypherConfig = CypherConfiguration.fromConfig(config);
      Catalog catalog = Catalog.fromConfig(this.fabricConfig);
      SignatureResolver signatureResolver = new SignatureResolver(proceduresSupplier);
      FabricQueryMonitoring monitoring = new FabricQueryMonitoring(dependencies, monitors);
      FabricPlanner planner =
          serviceBootstrapper.registerService(
              new FabricPlanner(this.fabricConfig, cypherConfig, monitors, signatureResolver),
              FabricPlanner.class);
      UseEvaluation useEvaluation =
          serviceBootstrapper
              .registerService(new UseEvaluation(catalog, proceduresSupplier, signatureResolver),
                  UseEvaluation.class);
      FabricReactorHooks.register(internalLogProvider);
      Executor fabricWorkerExecutor = jobScheduler.executor(Group.FABRIC_WORKER);
      FabricExecutor fabricExecutor =
          new FabricExecutor(this.fabricConfig, planner, useEvaluation, internalLogProvider,
              monitoring, fabricWorkerExecutor);
      serviceBootstrapper.registerService(fabricExecutor, FabricExecutor.class);
    }
  }

  public void registerProcedures(GlobalProcedures globalProcedures) throws KernelException {
    if (this.fabricConfig.isEnabled()) {
      globalProcedures.register(new GraphIdsFunction(this.fabricConfig));
    }
  }

  private static class ServiceBootstrapper {

    private final LifeSupport lifeSupport;
    private final Dependencies dependencies;

    ServiceBootstrapper(LifeSupport lifeSupport, Dependencies dependencies) {
      this.lifeSupport = lifeSupport;
      this.dependencies = dependencies;
    }

    <T> T registerService(T dependency, Class<T> dependencyType) {
      this.dependencies.satisfyDependency(dependency);
      if (LifecycleAdapter.class.isAssignableFrom(dependencyType)) {
        this.lifeSupport.add((LifecycleAdapter) dependency);
      }

      return this.dependencies.resolveDependency(dependencyType);
    }
  }
}
