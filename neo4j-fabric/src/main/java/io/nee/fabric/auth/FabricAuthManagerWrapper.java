/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.auth;

import io.nee.kernel.enterprise.api.security.EnterpriseAuthManager;
import io.nee.kernel.enterprise.api.security.EnterpriseLoginContext;
import java.util.Arrays;
import java.util.Map;
import org.neo4j.internal.kernel.api.security.AuthSubject;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.kernel.api.security.AuthToken;
import org.neo4j.kernel.api.security.exception.InvalidAuthTokenException;

public class FabricAuthManagerWrapper implements EnterpriseAuthManager {

  private final EnterpriseAuthManager wrappedAuthManager;

  public FabricAuthManagerWrapper(EnterpriseAuthManager wrappedAuthManager) {
    this.wrappedAuthManager = wrappedAuthManager;
  }

  public static Credentials getCredentials(AuthSubject authSubject) {
    if (!(authSubject instanceof FabricAuthSubject)) {
      throw new IllegalArgumentException(
          "The submitted subject was not created by Fabric Authentication manager: " + authSubject);
    } else {
      return ((FabricAuthSubject) authSubject).getCredentials();
    }
  }

  public EnterpriseLoginContext login(Map<String, Object> authToken)
      throws InvalidAuthTokenException {
    boolean authProvided = !authToken.get("scheme").equals("none");
    String username = null;
    byte[] password = null;
    if (authToken.containsKey("principal") && authToken.containsKey("credentials")) {
      username = AuthToken.safeCast("principal", authToken);
      byte[] originalPassword = AuthToken.safeCastCredentials("credentials", authToken);
      password = Arrays.copyOf(originalPassword, originalPassword.length);
    }

    EnterpriseLoginContext wrappedLoginContext = this.wrappedAuthManager.login(authToken);
    Credentials credentials = new Credentials(username, password, authProvided);
    FabricAuthSubject fabricAuthSubject = new FabricAuthSubject(wrappedLoginContext.subject(),
        credentials);
    return new FabricLoginContext(wrappedLoginContext, fabricAuthSubject);
  }

  public void clearAuthCache() {
    this.wrappedAuthManager.clearAuthCache();
  }

  public void log(String message, SecurityContext securityContext) {
    this.wrappedAuthManager.log(message, securityContext);
  }

  public void init() {
  }

  public void start() {
  }

  public void stop() {
  }

  public void shutdown() {
  }
}
