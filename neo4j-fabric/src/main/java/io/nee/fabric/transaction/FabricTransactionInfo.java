/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.transaction;

import java.time.Duration;
import java.util.Map;
import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.internal.kernel.api.connectioninfo.ClientConnectionInfo;
import org.neo4j.internal.kernel.api.security.LoginContext;

public class FabricTransactionInfo {

  private final AccessMode accessMode;
  private final LoginContext loginContext;
  private final ClientConnectionInfo clientConnectionInfo;
  private final String databaseName;
  private final boolean implicitTransaction;
  private final Duration txTimeout;
  private final Map<String, Object> txMetadata;

  public FabricTransactionInfo(AccessMode accessMode, LoginContext loginContext,
      ClientConnectionInfo clientConnectionInfo, String databaseName,
      boolean implicitTransaction, Duration txTimeout, Map<String, Object> txMetadata) {
    this.accessMode = accessMode;
    this.loginContext = loginContext;
    this.clientConnectionInfo = clientConnectionInfo;
    this.databaseName = databaseName;
    this.implicitTransaction = implicitTransaction;
    this.txTimeout = txTimeout;
    this.txMetadata = txMetadata;
  }

  public AccessMode getAccessMode() {
    return this.accessMode;
  }

  public LoginContext getLoginContext() {
    return this.loginContext;
  }

  public ClientConnectionInfo getClientConnectionInfo() {
    return this.clientConnectionInfo;
  }

  public String getDatabaseName() {
    return this.databaseName;
  }

  public boolean isImplicitTransaction() {
    return this.implicitTransaction;
  }

  public Duration getTxTimeout() {
    return this.txTimeout;
  }

  public Map<String, Object> getTxMetadata() {
    return this.txMetadata;
  }
}
