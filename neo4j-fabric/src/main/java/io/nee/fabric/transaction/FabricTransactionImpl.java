/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.transaction;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.executor.Exceptions;
import io.nee.fabric.executor.FabricException;
import io.nee.fabric.executor.FabricLocalExecutor;
import io.nee.fabric.executor.FabricRemoteExecutor;
import io.nee.fabric.stream.StatementResult;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import org.neo4j.kernel.api.exceptions.Status;
import org.neo4j.kernel.api.exceptions.Status.Statement;
import org.neo4j.kernel.api.exceptions.Status.Transaction;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobHandle;
import org.neo4j.scheduler.JobScheduler;

public class FabricTransactionImpl implements FabricTransaction,
    FabricTransaction.FabricExecutionContext {

  private static final AtomicLong ID_GENERATOR = new AtomicLong();
  private final FabricTransactionInfo transactionInfo;
  private final TransactionBookmarkManager bookmarkManager;
  private final FabricRemoteExecutor remoteExecutor;
  private final FabricLocalExecutor localExecutor;
  private final Log userLog;
  private final Log internalLog;
  private final TransactionManager transactionManager;
  private final JobScheduler jobScheduler;
  private final FabricConfig fabricConfig;
  private final long id;
  private JobHandle timeoutHandle;
  private FabricRemoteExecutor.FabricRemoteTransaction remoteTransaction;
  private FabricLocalExecutor.FabricLocalTransaction localTransaction;
  private boolean terminated;
  private Status terminationStatus;

  FabricTransactionImpl(FabricTransactionInfo transactionInfo,
      TransactionBookmarkManager bookmarkManager, FabricRemoteExecutor remoteExecutor,
      FabricLocalExecutor localExecutor, LogService logService,
      TransactionManager transactionManager, JobScheduler jobScheduler,
      FabricConfig fabricConfig) {
    this.transactionInfo = transactionInfo;
    this.remoteExecutor = remoteExecutor;
    this.localExecutor = localExecutor;
    this.userLog = logService.getUserLog(FabricTransactionImpl.class);
    this.internalLog = logService.getInternalLog(FabricTransactionImpl.class);
    this.transactionManager = transactionManager;
    this.jobScheduler = jobScheduler;
    this.fabricConfig = fabricConfig;
    this.bookmarkManager = bookmarkManager;
    this.id = ID_GENERATOR.incrementAndGet();
  }

  public FabricTransactionInfo getTransactionInfo() {
    return this.transactionInfo;
  }

  public FabricRemoteExecutor.FabricRemoteTransaction getRemote() {
    return this.remoteTransaction;
  }

  public FabricLocalExecutor.FabricLocalTransaction getLocal() {
    return this.localTransaction;
  }

  public void begin() {
    this.internalLog.debug("Starting transaction %d", this.id);

    try {
      this.remoteTransaction = this.remoteExecutor
          .begin(this.transactionInfo, this.bookmarkManager);
      this.localTransaction = this.localExecutor.begin(this.transactionInfo);
      this.scheduleTimeout(this.transactionInfo);
      this.internalLog.debug("Transaction %d started", this.id);
    } catch (RuntimeException n2) {
      this.userLog.error("Transaction {} start failed", this.id);
      throw Exceptions.transform(Transaction.TransactionStartFailed, n2);
    }
  }

  public StatementResult execute(
      Function<FabricTransaction.FabricExecutionContext, StatementResult> runLogic) {
    if (this.terminated) {
      Status status = this.terminationStatus;
      if (status == null) {
        status = Statement.ExecutionFailed;
      }

      this.internalLog.error("Trying to execute query in a terminated transaction %d", this.id);
      throw new FabricException(status, "Trying to execute query in a terminated transaction");
    } else {
      try {
        return runLogic.apply(this);
      } catch (RuntimeException n3) {
        this.userLog.error("Query execution in transaction %d failed", this.id);
        this.rollback();
        throw Exceptions.transform(Statement.ExecutionFailed, n3);
      }
    }
  }

  public void commit() {
    if (this.terminated) {
      throw new FabricException(Transaction.TransactionCommitFailed,
          "Trying to commit terminated transaction");
    } else {
      this.terminated = true;
      this.internalLog.debug("Committing transaction %d", this.id);
      this.cancelTimeout();

      try {
        try {
          if (this.localTransaction != null) {
            this.localTransaction.commit();
          }
        } catch (Exception n9) {
          this.userLog.error(String.format("Local transaction %d commit failed", this.id));

          try {
            if (this.remoteTransaction != null) {
              this.remoteTransaction.rollback().block();
            }
          } catch (Exception n8) {
            this.userLog.error("Failed to rollback remote transaction", n8);
          }

          throw new FabricException(Transaction.TransactionCommitFailed,
              "Failed to commit transaction", n9);
        }

        try {
          if (this.remoteTransaction != null) {
            this.remoteTransaction.commit().block();
          }
        } catch (Exception n10) {
          this.userLog.error("Transaction %d commit failed", this.id);
          throw new FabricException(Transaction.TransactionCommitFailed,
              "Failed to commit remote transaction", n10);
        }
      } finally {
        this.transactionManager.removeTransaction(this);
      }

      this.internalLog.debug("Transaction %d committed", this.id);
    }
  }

  public void rollback() {
    if (this.remoteTransaction != null || this.localTransaction != null) {
      this.doRollback();
    }
  }

  public void markForTermination(Status reason) {
    this.internalLog.debug("Terminating transaction %d", this.id);
    this.terminationStatus = reason;
    this.localTransaction.markForTermination(reason);
    this.doRollback();
  }

  public Optional<Status> getReasonIfTerminated() {
    return Optional.empty();
  }

  public TransactionBookmarkManager getBookmarkManager() {
    return this.bookmarkManager;
  }

  void doRollback() {
    if (!this.terminated) {
      this.terminated = true;
      this.internalLog.debug("Rolling back transaction %d", this.id);
      this.cancelTimeout();
      Exception localError = null;

      try {
        if (this.localTransaction != null) {
          this.localTransaction.rollback();
        }
      } catch (Exception n7) {
        localError = n7;
      }

      try {
        if (this.remoteTransaction != null) {
          this.remoteTransaction.rollback().block();
        }
      } catch (Exception n8) {
        this.userLog.error("Remote transaction %d rollback failed", this.id);
        if (localError != null) {
          this.userLog
              .error(String.format("Local transaction %d rollback failed", this.id), localError);
        }

        throw new FabricException(Transaction.TransactionRollbackFailed,
            "Failed to rollback remote transaction", n8);
      } finally {
        this.transactionManager.removeTransaction(this);
      }

      if (localError != null) {
        this.userLog.error(String.format("Local transaction %d rollback failed", this.id));
        throw new FabricException(Transaction.TransactionRollbackFailed,
            "Failed to rollback local transaction", localError);
      } else {
        this.internalLog.debug("Transaction %d rolled back", this.id);
      }
    }
  }

  private void scheduleTimeout(FabricTransactionInfo transactionInfo) {
    if (transactionInfo.getTxTimeout() != null) {
      this.scheduleTimeout(transactionInfo.getTxTimeout());
    } else {
      this.scheduleTimeout(this.fabricConfig.getTransactionTimeout());
    }
  }

  private void scheduleTimeout(Duration duration) {
    if (!duration.equals(Duration.ZERO)) {
      this.timeoutHandle = this.jobScheduler
          .schedule(Group.SERVER_TRANSACTION_TIMEOUT, this::handleTimeout, duration.toSeconds(),
              TimeUnit.SECONDS);
    }
  }

  private void handleTimeout() {
    if (!this.terminated) {
      this.userLog.info("Terminating transaction %d because of timeout", this.id);
      this.terminationStatus = Transaction.TransactionTimedOut;
      this.doRollback();
    }
  }

  private void cancelTimeout() {
    if (this.timeoutHandle != null) {
      this.timeoutHandle.cancel();
    }
  }
}
