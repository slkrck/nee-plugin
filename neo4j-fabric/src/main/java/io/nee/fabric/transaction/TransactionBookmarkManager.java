/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.transaction;

import io.nee.fabric.bolt.FabricBookmark;
import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.driver.RemoteBookmark;
import io.nee.fabric.executor.FabricException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.neo4j.bolt.runtime.Bookmark;
import org.neo4j.bolt.txtracking.TransactionIdTracker;
import org.neo4j.kernel.api.exceptions.Status.Transaction;
import org.neo4j.kernel.database.DatabaseIdRepository;

public class TransactionBookmarkManager {

  private final FabricConfig fabricConfig;
  private final TransactionIdTracker transactionIdTracker;
  private final Duration bookmarkTimeout;
  private final Map<FabricConfig.Graph, TransactionBookmarkManager.GraphBookmarkData> graphBookmarkData = new ConcurrentHashMap();

  public TransactionBookmarkManager(FabricConfig fabricConfig,
      TransactionIdTracker transactionIdTracker, Duration bookmarkTimeout) {
    this.fabricConfig = fabricConfig;
    this.transactionIdTracker = transactionIdTracker;
    this.bookmarkTimeout = bookmarkTimeout;
  }

  public void processSubmittedByClient(List<Bookmark> bookmarks) {
    if (!bookmarks.isEmpty()) {
      TransactionBookmarkManager.BookmarksByType bookmarksByType = this.sortOutByType(bookmarks);
      this.processSystemDatabase(bookmarksByType);
      this.processFabricBookmarks(bookmarksByType);
    }
  }

  public List<RemoteBookmark> getBookmarksForGraph(FabricConfig.Graph graph) {
    TransactionBookmarkManager.GraphBookmarkData bookmarkData = this.graphBookmarkData.get(graph);
    return bookmarkData != null ? bookmarkData.bookmarksSubmittedByClient : List.of();
  }

  public void recordBookmarkReceivedFromGraph(FabricConfig.Graph graph, RemoteBookmark bookmark) {
    TransactionBookmarkManager.GraphBookmarkData bookmarkData =
        this.graphBookmarkData.computeIfAbsent(graph, (g) ->
        {
          return new GraphBookmarkData();
        });
    bookmarkData.bookmarkReceivedFromGraph = bookmark;
  }

  public FabricBookmark constructFinalBookmark() {
    List<FabricBookmark.GraphState> remoteStates = this.graphBookmarkData.entrySet().stream()
        .map((entry) ->
        {
          GraphBookmarkData
              bookmarkData =
              entry
                  .getValue();
          List graphBookmarks;
          if (bookmarkData.bookmarkReceivedFromGraph !=
              null) {
            graphBookmarks = List.of(
                bookmarkData.bookmarkReceivedFromGraph);
          } else {
            graphBookmarks =
                bookmarkData.bookmarksSubmittedByClient;
          }

          return new FabricBookmark.GraphState(
              entry.getKey()
                  .getId(), graphBookmarks);
        }).collect(Collectors.toList());
    return new FabricBookmark(remoteStates);
  }

  private TransactionBookmarkManager.BookmarksByType sortOutByType(List<Bookmark> bookmarks) {
    long systemDbTxId = -1L;
    List<FabricBookmark> fabricBookmarks = new ArrayList(bookmarks.size());
    Iterator n5 = bookmarks.iterator();

    while (n5.hasNext()) {
      Bookmark bookmark = (Bookmark) n5.next();
      if (bookmark instanceof FabricBookmark) {
        fabricBookmarks.add((FabricBookmark) bookmark);
      } else {
        if (!bookmark.databaseId().equals(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID)) {
          throw new FabricException(Transaction.InvalidBookmarkMixture,
              "Bookmark for unexpected database encountered: " + bookmark);
        }

        systemDbTxId = Math.max(systemDbTxId, bookmark.txId());
      }
    }

    return new TransactionBookmarkManager.BookmarksByType(systemDbTxId, fabricBookmarks);
  }

  private void processSystemDatabase(TransactionBookmarkManager.BookmarksByType bookmarksByType) {
    if (bookmarksByType.systemDbTxId != -1L) {
      this.transactionIdTracker.awaitUpToDate(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID,
          bookmarksByType.systemDbTxId, this.bookmarkTimeout);
    }
  }

  private void processFabricBookmarks(TransactionBookmarkManager.BookmarksByType bookmarksByType) {
    Map<Long, FabricConfig.Graph> graphsById =
        this.fabricConfig.getDatabase().getGraphs().stream()
            .collect(Collectors.toMap(FabricConfig.Graph::getId, Function.identity()));
    Iterator n3 = bookmarksByType.fabricBookmarks.iterator();

    while (n3.hasNext()) {
      FabricBookmark bookmark = (FabricBookmark) n3.next();
      bookmark.getGraphStates().forEach((graphState) ->
      {
        FabricConfig.Graph graph = graphsById.get(graphState.getRemoteGraphId());
        if (graph == null) {
          throw new FabricException(Transaction.InvalidBookmark,
              "Bookmark with non-existent remote graph ID database encountered: " +
                  bookmark
          );
        } else {
          TransactionBookmarkManager.GraphBookmarkData bookmarkData =
              this.graphBookmarkData
                  .computeIfAbsent(graph, (g) ->
                  {
                    return new GraphBookmarkData();
                  });
          bookmarkData.bookmarksSubmittedByClient.addAll(graphState.getBookmarks());
        }
      });
    }
  }

  private static class GraphBookmarkData {

    private final List<RemoteBookmark> bookmarksSubmittedByClient = new ArrayList();
    private RemoteBookmark bookmarkReceivedFromGraph;
  }

  private static class BookmarksByType {

    private final long systemDbTxId;
    private final List<FabricBookmark> fabricBookmarks;

    BookmarksByType(Long systemDbTxId, List<FabricBookmark> fabricBookmarks) {
      this.systemDbTxId = systemDbTxId;
      this.fabricBookmarks = fabricBookmarks;
    }
  }
}
