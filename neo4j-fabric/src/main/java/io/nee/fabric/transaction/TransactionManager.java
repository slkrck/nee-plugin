/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.transaction;

import io.nee.fabric.config.FabricConfig;
import io.nee.fabric.executor.FabricLocalExecutor;
import io.nee.fabric.executor.FabricRemoteExecutor;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.neo4j.common.DependencyResolver;
import org.neo4j.internal.kernel.api.security.LoginContext.IdLookup;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.internal.LogService;
import org.neo4j.scheduler.JobScheduler;

public class TransactionManager extends LifecycleAdapter {

  private final FabricRemoteExecutor remoteExecutor;
  private final FabricLocalExecutor localExecutor;
  private final LogService logService;
  private final JobScheduler jobScheduler;
  private final FabricConfig fabricConfig;
  private final Set<FabricTransactionImpl> openTransactions = ConcurrentHashMap.newKeySet();

  public TransactionManager(DependencyResolver dependencyResolver) {
    this.remoteExecutor = dependencyResolver.resolveDependency(FabricRemoteExecutor.class);
    this.localExecutor = dependencyResolver.resolveDependency(FabricLocalExecutor.class);
    this.logService = dependencyResolver.resolveDependency(LogService.class);
    this.jobScheduler = dependencyResolver.resolveDependency(JobScheduler.class);
    this.fabricConfig = dependencyResolver.resolveDependency(FabricConfig.class);
  }

  public FabricTransaction begin(FabricTransactionInfo transactionInfo,
      TransactionBookmarkManager transactionBookmarkManager) {
    transactionInfo.getLoginContext().authorize(IdLookup.EMPTY, transactionInfo.getDatabaseName());
    FabricTransactionImpl fabricTransaction =
        new FabricTransactionImpl(transactionInfo, transactionBookmarkManager, this.remoteExecutor,
            this.localExecutor, this.logService, this,
            this.jobScheduler, this.fabricConfig);
    fabricTransaction.begin();
    this.openTransactions.add(fabricTransaction);
    return fabricTransaction;
  }

  public void stop() {
    this.openTransactions.forEach(FabricTransactionImpl::doRollback);
  }

  void removeTransaction(FabricTransactionImpl transaction) {
    this.openTransactions.remove(transaction);
  }

  public Set<FabricTransactionImpl> getOpenTransactions() {
    return this.openTransactions;
  }
}
