/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.stream.summary;

import java.util.concurrent.atomic.AtomicInteger;
import org.neo4j.graphdb.QueryStatistics;

public class MergedQueryStatistics implements QueryStatistics {

  private final AtomicInteger nodesCreated = new AtomicInteger(0);
  private final AtomicInteger nodesDeleted = new AtomicInteger(0);
  private final AtomicInteger relationshipsCreated = new AtomicInteger(0);
  private final AtomicInteger relationshipsDeleted = new AtomicInteger(0);
  private final AtomicInteger propertiesSet = new AtomicInteger(0);
  private final AtomicInteger labelsAdded = new AtomicInteger(0);
  private final AtomicInteger labelsRemoved = new AtomicInteger(0);
  private final AtomicInteger indexesAdded = new AtomicInteger(0);
  private final AtomicInteger indexesRemoved = new AtomicInteger(0);
  private final AtomicInteger constraintsAdded = new AtomicInteger(0);
  private final AtomicInteger constraintsRemoved = new AtomicInteger(0);
  private final AtomicInteger systemUpdates = new AtomicInteger(0);
  private boolean containsUpdates;
  private boolean containsSystemUpdates;

  public void add(QueryStatistics delta) {
    this.nodesCreated.addAndGet(delta.getNodesCreated());
    this.nodesDeleted.addAndGet(delta.getNodesDeleted());
    this.relationshipsCreated.addAndGet(delta.getRelationshipsCreated());
    this.relationshipsDeleted.addAndGet(delta.getRelationshipsDeleted());
    this.propertiesSet.addAndGet(delta.getPropertiesSet());
    this.labelsAdded.addAndGet(delta.getLabelsAdded());
    this.labelsRemoved.addAndGet(delta.getLabelsRemoved());
    this.indexesAdded.addAndGet(delta.getIndexesAdded());
    this.indexesRemoved.addAndGet(delta.getIndexesRemoved());
    this.constraintsAdded.addAndGet(delta.getConstraintsAdded());
    this.constraintsRemoved.addAndGet(delta.getConstraintsRemoved());
    this.systemUpdates.addAndGet(delta.getSystemUpdates());
    if (delta.containsUpdates()) {
      this.containsUpdates = true;
    }

    if (delta.containsSystemUpdates()) {
      this.containsSystemUpdates = true;
    }
  }

  public int getNodesCreated() {
    return this.nodesCreated.get();
  }

  public int getNodesDeleted() {
    return this.nodesDeleted.get();
  }

  public int getRelationshipsCreated() {
    return this.relationshipsCreated.get();
  }

  public int getRelationshipsDeleted() {
    return this.relationshipsDeleted.get();
  }

  public int getPropertiesSet() {
    return this.propertiesSet.get();
  }

  public int getLabelsAdded() {
    return this.labelsAdded.get();
  }

  public int getLabelsRemoved() {
    return this.labelsRemoved.get();
  }

  public int getIndexesAdded() {
    return this.indexesAdded.get();
  }

  public int getIndexesRemoved() {
    return this.indexesRemoved.get();
  }

  public int getConstraintsAdded() {
    return this.constraintsAdded.get();
  }

  public int getConstraintsRemoved() {
    return this.constraintsRemoved.get();
  }

  public int getSystemUpdates() {
    return this.systemUpdates.get();
  }

  public boolean containsUpdates() {
    return this.containsUpdates;
  }

  public boolean containsSystemUpdates() {
    return this.containsSystemUpdates;
  }
}
