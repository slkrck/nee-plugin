/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.stream;

import org.neo4j.cypher.internal.runtime.InputCursor;
import org.neo4j.cypher.internal.runtime.InputDataStream;
import org.neo4j.values.AnyValue;

public class InputDataStreamImpl implements InputDataStream {

  private final Rx2SyncStream wrappedStream;
  private InputCursor inputCursor;

  public InputDataStreamImpl(Rx2SyncStream wrappedStream) {
    this.wrappedStream = wrappedStream;
    this.inputCursor = new InputDataStreamImpl.Cursor();
  }

  public InputCursor nextInputBatch() {
    return this.inputCursor;
  }

  private class Cursor implements InputCursor {

    private Record currentRecord;

    public boolean next() {
      this.currentRecord = InputDataStreamImpl.this.wrappedStream.readRecord();
      if (this.currentRecord != null) {
        return true;
      } else {
        InputDataStreamImpl.this.inputCursor = null;
        return false;
      }
    }

    public AnyValue value(int offset) {
      return this.currentRecord.getValue(offset);
    }

    public void close() throws Exception {
    }
  }
}
