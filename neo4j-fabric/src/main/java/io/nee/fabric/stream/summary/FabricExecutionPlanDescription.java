/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.stream.summary;

import io.nee.fabric.planning.FabricQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.neo4j.graphdb.ExecutionPlanDescription;
import scala.collection.JavaConverters;

public class FabricExecutionPlanDescription implements ExecutionPlanDescription {

  private final FabricQuery query;

  public FabricExecutionPlanDescription(FabricQuery query) {
    this.query = query;
  }

  public String getName() {
    return this.query.getClass().getSimpleName();
  }

  public List<ExecutionPlanDescription> getChildren() {
    return (List) JavaConverters.seqAsJavaList(this.query.children()).stream()
        .map(FabricExecutionPlanDescription::new).collect(Collectors.toList());
  }

  public Map<String, Object> getArguments() {
    HashMap<String, Object> arguments = new HashMap();
    if (this.query instanceof FabricQuery.RemoteQuery) {
      FabricQuery.RemoteQuery rq = (FabricQuery.RemoteQuery) this.query;
      arguments.put("query", rq.queryString());
    }

    return arguments;
  }

  public Set<String> getIdentifiers() {
    return JavaConverters.setAsJavaSet(this.query.columns().output().toSet());
  }

  public boolean hasProfilerStatistics() {
    return false;
  }

  public ProfilerStatistics getProfilerStatistics() {
    return null;
  }
}
