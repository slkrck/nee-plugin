/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.stream.summary;

import java.util.Collection;
import java.util.List;
import org.neo4j.graphdb.Notification;
import org.neo4j.graphdb.QueryExecutionType;
import org.neo4j.graphdb.QueryStatistics;

public class PartialSummary extends EmptySummary {

  private final QueryStatistics queryStatistics;
  private final QueryExecutionType executionType;
  private final List<Notification> notifications;

  public PartialSummary(QueryStatistics queryStatistics, QueryExecutionType executionType,
      List<Notification> notifications) {
    this.queryStatistics = queryStatistics;
    this.executionType = executionType;
    this.notifications = notifications;
  }

  public QueryStatistics getQueryStatistics() {
    return this.queryStatistics;
  }

  public QueryExecutionType executionType() {
    return this.executionType;
  }

  public Collection<Notification> getNotifications() {
    return this.notifications;
  }
}
