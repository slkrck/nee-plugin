/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.stream;

import io.nee.fabric.config.FabricConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxOperator;

public class Prefetcher {

  private static final Prefetcher.RecordOrError END = new Prefetcher.RecordOrError(null, null);
  private final FabricConfig.DataStream streamConfig;
  private final List<Prefetcher.PrefetchOperator> prefetchOperators = new ArrayList();

  public Prefetcher(FabricConfig.DataStream streamConfig) {
    this.streamConfig = streamConfig;
  }

  public synchronized Flux<Record> addPrefetch(Flux<Record> recordStream) {
    int operatorsCount = this.prefetchOperators.size() + 1;
    int newLowWatermark = this.computeLowWatermark(operatorsCount);
    int newHighWatermark = this.computeHighWatermark(operatorsCount);
    this.updateWatermarks(newLowWatermark, newHighWatermark);
    Prefetcher.PrefetchOperator prefetchOperator = new Prefetcher.PrefetchOperator(recordStream,
        newLowWatermark, newHighWatermark);
    this.prefetchOperators.add(prefetchOperator);
    return prefetchOperator;
  }

  private int computeHighWatermark(int operatorsCount) {
    return Math.max(1, this.streamConfig.getBufferSize() / operatorsCount);
  }

  private int computeLowWatermark(int operatorsCount) {
    return this.streamConfig.getBufferLowWatermark() / operatorsCount;
  }

  private void updateWatermarks(int lowWatermark, int highWatermark) {
    this.prefetchOperators.forEach((prefetchOperator) ->
    {
      prefetchOperator.bufferLowWatermark = lowWatermark;
      prefetchOperator.bufferHighWatermark = highWatermark;
    });
  }

  private synchronized void removeOperator(Prefetcher.PrefetchOperator operator) {
    this.prefetchOperators.remove(operator);
    if (this.prefetchOperators.size() > 0) {
      int operatorsCount = this.prefetchOperators.size();
      int newLowWatermark = this.computeLowWatermark(operatorsCount);
      int newHighWatermark = this.computeHighWatermark(operatorsCount);
      this.updateWatermarks(newLowWatermark, newHighWatermark);
    }
  }

  private static class RecordOrError {

    private final Record record;
    private final Throwable error;

    RecordOrError(Record record, Throwable error) {
      this.record = record;
      this.error = error;
    }
  }

  private class PrefetchOperator extends FluxOperator<Record, Record> {

    private final Queue<Prefetcher.RecordOrError> buffer;
    private final Prefetcher.PrefetchOperator.RecordSubscriber upstreamSubscriber;
    private final AtomicBoolean producing = new AtomicBoolean(false);
    private final AtomicLong pendingRequested = new AtomicLong(0L);
    private volatile int bufferLowWatermark;
    private volatile int bufferHighWatermark;
    private volatile boolean finished;
    private volatile Subscriber<Record> downstreamSubscriber;

    PrefetchOperator(Flux<Record> recordStream, int bufferLowWatermark, int bufferHighWatermark) {
      super(recordStream);
      this.bufferHighWatermark = bufferHighWatermark;
      this.bufferLowWatermark = bufferLowWatermark;
      this.buffer = new ArrayBlockingQueue(Prefetcher.this.streamConfig.getBufferSize() + 1);
      this.upstreamSubscriber = new Prefetcher.PrefetchOperator.RecordSubscriber();
      recordStream.subscribeWith(this.upstreamSubscriber);
    }

    private void maybeRequest() {
      int buffered = this.buffer.size();
      long pendingRequested = this.upstreamSubscriber.pendingRequested.get();
      long batchSize = (long) (this.bufferHighWatermark - buffered) - pendingRequested;
      if ((long) buffered + pendingRequested <= (long) this.bufferLowWatermark && batchSize != 0L) {
        this.upstreamSubscriber.request(batchSize);
      }
    }

    public void subscribe(CoreSubscriber subscriber) {
      this.downstreamSubscriber = subscriber;
      subscriber.onSubscribe(new Subscription() {
        public void request(long l) {
          PrefetchOperator.this.pendingRequested.addAndGet(l);
          PrefetchOperator.this.maybeProduce();
        }

        public void cancel() {
          PrefetchOperator.this.finish();
          PrefetchOperator.this.upstreamSubscriber.close();
        }
      });
    }

    private void maybeProduce() {
      if (this.buffer.peek() != null && this.downstreamSubscriber != null
          && this.pendingRequested.get() != 0L && !this.finished) {
        if (this.producing.compareAndSet(false, true)) {
          while (!this.finished && this.pendingRequested.get() > 0L) {
            Prefetcher.RecordOrError recordOrError = this.buffer.poll();
            if (recordOrError == null) {
              break;
            }

            if (recordOrError == Prefetcher.END) {
              this.downstreamSubscriber.onComplete();
              this.finish();
              break;
            }

            if (recordOrError.error != null) {
              this.downstreamSubscriber.onError(recordOrError.error);
              this.finish();
              break;
            }

            this.pendingRequested.decrementAndGet();
            this.downstreamSubscriber.onNext(recordOrError.record);
          }

          this.maybeRequest();
          this.producing.set(false);
          this.maybeProduce();
        }
      }
    }

    private void finish() {
      this.finished = true;
      Prefetcher.this.removeOperator(this);
    }

    private class RecordSubscriber implements Subscriber<Record> {

      private final AtomicLong pendingRequested = new AtomicLong(0L);
      private volatile Subscription subscription;

      public void onSubscribe(Subscription subscription) {
        this.subscription = subscription;
        PrefetchOperator.this.maybeRequest();
      }

      public void onNext(Record record) {
        this.pendingRequested.decrementAndGet();
        this.enqueue(new Prefetcher.RecordOrError(record, null));
      }

      public void onError(Throwable throwable) {
        this.enqueue(new Prefetcher.RecordOrError(null, throwable));
      }

      public void onComplete() {
        this.enqueue(Prefetcher.END);
      }

      void request(long numberOfRecords) {
        this.pendingRequested.addAndGet(numberOfRecords);
        this.subscription.request(numberOfRecords);
      }

      private void enqueue(Prefetcher.RecordOrError recordOrError) {
        PrefetchOperator.this.buffer.add(recordOrError);
        PrefetchOperator.this.maybeProduce();
      }

      void close() {
        this.subscription.cancel();
      }
    }
  }
}
