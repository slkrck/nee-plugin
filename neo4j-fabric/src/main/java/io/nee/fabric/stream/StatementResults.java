/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.stream;

import io.nee.fabric.executor.FabricException;
import io.nee.fabric.stream.summary.Summary;
import java.util.function.Function;
import org.neo4j.graphdb.QueryStatistics;
import org.neo4j.kernel.api.exceptions.Status.General;
import org.neo4j.kernel.impl.query.QueryExecution;
import org.neo4j.kernel.impl.query.QuerySubscriber;
import org.neo4j.values.AnyValue;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class StatementResults {

  public static StatementResult map(StatementResult statementResult,
      Function<Flux<Record>, Flux<Record>> func) {
    return new StatementResults.BasicStatementResult(statementResult.columns(),
        func.apply(statementResult.records()),
        statementResult.summary());
  }

  public static StatementResult initial() {
    return new StatementResults.BasicStatementResult(Flux.empty(), Flux.just(Records.empty()),
        Mono.empty());
  }

  public static StatementResult create(Function<QuerySubscriber, QueryExecution> execution) {
    try {
      StatementResults.QuerySubject querySubject = new StatementResults.QuerySubject();
      QueryExecution queryExecution = execution.apply(querySubject);
      querySubject.setQueryExecution(queryExecution);
      return create(Flux.fromArray(queryExecution.fieldNames()), Flux.from(querySubject),
          Mono.empty());
    } catch (RuntimeException n3) {
      return error(n3);
    }
  }

  public static StatementResult create(Flux<String> columns, Flux<Record> records,
      Mono<Summary> summary) {
    return new StatementResults.BasicStatementResult(columns, records, summary);
  }

  public static StatementResult error(Throwable err) {
    return new StatementResults.BasicStatementResult(Flux.error(err), Flux.error(err),
        Mono.error(err));
  }

  public static StatementResult trace(StatementResult input) {
    return new StatementResults.BasicStatementResult(input.columns(),
        input.records().doOnEach((signal) ->
        {
          if (signal.hasValue()) {
            System.out.println(String.join(", ",
                signal.getType()
                    .toString(),
                Records.show(
                    signal
                        .get())));
          } else if (signal.hasError()) {
            System.out.println(String.join(", ",
                signal.getType()
                    .toString(),
                signal.getThrowable()
                    .toString()));
          } else {
            System.out.println(String.join(", ",
                signal.getType()
                    .toString()));
          }
        }), input.summary());
  }

  private abstract static class RecordQuerySubscriber implements QuerySubscriber {

    private int numberOfFields;
    private AnyValue[] fields;
    private int fieldIndex;

    public void onResult(int numberOfFields) {
      this.numberOfFields = numberOfFields;
    }

    public void onRecord() {
      this.fields = new AnyValue[this.numberOfFields];
      this.fieldIndex = 0;
    }

    public void onField(AnyValue value) {
      this.fields[this.fieldIndex] = value;
      ++this.fieldIndex;
    }

    public void onRecordCompleted() {
      this.onNext(Records.of(this.fields));
    }

    abstract void onNext(Record n1);
  }

  private static class QuerySubject extends StatementResults.RecordQuerySubscriber implements
      Publisher<Record> {

    private Subscriber<? super Record> subscriber;
    private QueryExecution queryExecution;
    private Throwable cachedError;
    private boolean cachedCompleted;
    private boolean errorReceived;

    void setQueryExecution(QueryExecution queryExecution) {
      this.queryExecution = queryExecution;
    }

    public void onNext(Record record) {
      this.subscriber.onNext(record);
    }

    public void onError(Throwable throwable) {
      this.errorReceived = true;
      if (this.subscriber == null) {
        this.cachedError = throwable;
      } else {
        this.subscriber.onError(throwable);
      }
    }

    public void onResultCompleted(QueryStatistics statistics) {
      if (this.subscriber == null) {
        this.cachedCompleted = true;
      } else {
        this.subscriber.onComplete();
      }
    }

    public void subscribe(final Subscriber<? super Record> subscriber) {
      if (this.subscriber != null) {
        throw new FabricException(General.UnknownError, "Already subscribed");
      } else {
        this.subscriber = subscriber;
        Subscription subscription = new Subscription() {
          private final Object requestLock = new Object();
          private long pendingRequests;
          private boolean producing;

          public void request(long size) {
            synchronized (this.requestLock) {
              this.pendingRequests += size;
              if (this.producing) {
                return;
              }

              this.producing = true;
            }

            while (true) {
              boolean n17 = false;

              try {
                n17 = true;
                long toRequest;
                synchronized (this.requestLock) {
                  toRequest = this.pendingRequests;
                  if (toRequest == 0L) {
                    n17 = false;
                    break;
                  }

                  this.pendingRequests = 0L;
                }

                this.doRequest(toRequest);
              } finally {
                if (n17) {
                  synchronized (this.requestLock) {
                    this.producing = false;
                  }
                }
              }
            }

            synchronized (this.requestLock) {
              this.producing = false;
            }
          }

          private void doRequest(long size) {
            QuerySubject.this.maybeSendCachedEvents();

            try {
              QuerySubject.this.queryExecution.request(size);
              if (!QuerySubject.this.errorReceived) {
                QuerySubject.this.queryExecution.await();
              }
            } catch (Exception n4) {
              subscriber.onError(n4);
            }
          }

          public void cancel() {
            QuerySubject.this.queryExecution.cancel();
          }
        };
        subscriber.onSubscribe(subscription);
        this.maybeSendCachedEvents();
      }
    }

    private void maybeSendCachedEvents() {
      if (this.cachedError != null) {
        this.subscriber.onError(this.cachedError);
        this.cachedError = null;
      } else if (this.cachedCompleted) {
        this.subscriber.onComplete();
        this.cachedCompleted = false;
      }
    }
  }

  private static class BasicStatementResult implements StatementResult {

    private final Flux<String> columns;
    private final Flux<Record> records;
    private final Mono<Summary> summary;

    BasicStatementResult(Flux<String> columns, Flux<Record> records, Mono<Summary> summary) {
      this.columns = columns;
      this.records = records;
      this.summary = summary;
    }

    public Flux<String> columns() {
      return this.columns;
    }

    public Flux<Record> records() {
      return this.records;
    }

    public Mono<Summary> summary() {
      return this.summary;
    }
  }
}
