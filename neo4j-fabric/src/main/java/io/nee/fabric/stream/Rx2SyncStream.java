/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.stream;

import io.nee.fabric.executor.Exceptions;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import org.neo4j.kernel.api.exceptions.Status.Statement;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;

public class Rx2SyncStream {

  private static final Rx2SyncStream.RecordOrError END = new Rx2SyncStream.RecordOrError(null,
      null);
  private final Rx2SyncStream.RecordSubscriber recordSubscriber;
  private final BlockingQueue<Rx2SyncStream.RecordOrError> buffer;
  private final int batchSize;

  public Rx2SyncStream(Flux<Record> records, int batchSize) {
    this.batchSize = batchSize;
    this.buffer = new ArrayBlockingQueue(batchSize + 1);
    this.recordSubscriber = new Rx2SyncStream.RecordSubscriber();
    records.subscribeWith(this.recordSubscriber);
  }

  public Record readRecord() {
    this.maybeRequest();

    Rx2SyncStream.RecordOrError recordOrError;
    try {
      recordOrError = this.buffer.take();
    } catch (InterruptedException n3) {
      this.recordSubscriber.close();
      throw new IllegalStateException(n3);
    }

    if (recordOrError == END) {
      return null;
    } else if (recordOrError.error != null) {
      throw Exceptions.transform(Statement.ExecutionFailed, recordOrError.error);
    } else {
      return recordOrError.record;
    }
  }

  public void close() {
    this.recordSubscriber.close();
  }

  private void maybeRequest() {
    int buffered = this.buffer.size();
    long pendingRequested = this.recordSubscriber.pendingRequested.get();
    if (pendingRequested + (long) buffered == 0L) {
      this.recordSubscriber.request(this.batchSize);
    }
  }

  private static class RecordOrError {

    private final Record record;
    private final Throwable error;

    RecordOrError(Record record, Throwable error) {
      this.record = record;
      this.error = error;
    }
  }

  private class RecordSubscriber implements Subscriber<Record> {

    private final AtomicLong pendingRequested = new AtomicLong(0L);
    private volatile Subscription subscription;

    public void onSubscribe(Subscription subscription) {
      this.subscription = subscription;
    }

    public void onNext(Record record) {
      this.pendingRequested.decrementAndGet();
      Rx2SyncStream.this.buffer.add(new Rx2SyncStream.RecordOrError(record, null));
    }

    public void onError(Throwable throwable) {
      Rx2SyncStream.this.buffer.add(new Rx2SyncStream.RecordOrError(null, throwable));
    }

    public void onComplete() {
      Rx2SyncStream.this.buffer.add(Rx2SyncStream.END);
    }

    void request(long numberOfRecords) {
      this.pendingRequested.addAndGet(numberOfRecords);
      this.subscription.request(numberOfRecords);
    }

    void close() {
      this.subscription.cancel();
    }
  }
}
