/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.stream;

import org.neo4j.values.AnyValue;

public abstract class Record {

  public abstract AnyValue getValue(int n1);

  public abstract int size();

  public final int hashCode() {
    int hashCode = 1;

    for (int i = 0; i < this.size(); ++i) {
      hashCode = 31 * hashCode + this.getValue(i).hashCode();
    }

    return hashCode;
  }

  public final boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (!(o instanceof Record)) {
      return false;
    } else {
      Record that = (Record) o;
      if (this.size() != that.size()) {
        return false;
      } else {
        for (int i = 0; i < this.size(); ++i) {
          if (!this.getValue(i).equals(that.getValue(i))) {
            return false;
          }
        }

        return true;
      }
    }
  }
}
