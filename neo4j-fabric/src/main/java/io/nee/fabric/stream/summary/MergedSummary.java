/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.stream.summary;

import io.nee.fabric.executor.EffectiveQueryType;
import io.nee.fabric.planning.FabricPlan;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.neo4j.bolt.runtime.AccessMode;
import org.neo4j.graphdb.ExecutionPlanDescription;
import org.neo4j.graphdb.Notification;
import org.neo4j.graphdb.QueryExecutionType;
import org.neo4j.graphdb.QueryExecutionType.QueryType;
import org.neo4j.graphdb.QueryStatistics;

public class MergedSummary implements Summary {

  private final MergedQueryStatistics statistics;
  private final List<Notification> notifications;
  private final QueryExecutionType executionType;
  private final FabricExecutionPlanDescription executionPlanDescription;

  public MergedSummary(FabricPlan plan, AccessMode accessMode) {
    this.executionType = this.queryExecutionType(plan, accessMode);
    this.statistics = new MergedQueryStatistics();
    this.notifications = new ArrayList();
    if (plan.executionType() == FabricPlan.EXPLAIN()) {
      this.executionPlanDescription = new FabricExecutionPlanDescription(plan.query());
    } else {
      this.executionPlanDescription = null;
    }
  }

  public void add(QueryStatistics delta) {
    this.statistics.add(delta);
  }

  public void add(Collection<Notification> delta) {
    this.notifications.addAll(delta);
  }

  public QueryExecutionType executionType() {
    return this.executionType;
  }

  public ExecutionPlanDescription executionPlanDescription() {
    return this.executionPlanDescription;
  }

  public Collection<Notification> getNotifications() {
    return this.notifications;
  }

  public QueryStatistics getQueryStatistics() {
    return this.statistics;
  }

  private QueryExecutionType queryExecutionType(FabricPlan plan, AccessMode accessMode) {
    if (plan.executionType() == FabricPlan.EXECUTE()) {
      return QueryExecutionType.query(this.queryType(plan, accessMode));
    } else if (plan.executionType() == FabricPlan.EXPLAIN()) {
      return QueryExecutionType.explained(this.queryType(plan, accessMode));
    } else if (plan.executionType() == FabricPlan.PROFILE()) {
      return QueryExecutionType.profiled(this.queryType(plan, accessMode));
    } else {
      throw this.unexpected("execution type", plan.executionType().toString());
    }
  }

  private QueryType queryType(FabricPlan plan, AccessMode accessMode) {
    return EffectiveQueryType.effectiveQueryType(accessMode, plan.queryType());
  }

  private IllegalArgumentException unexpected(String type, String got) {
    return new IllegalArgumentException("Unexpected " + type + ": " + got);
  }
}
