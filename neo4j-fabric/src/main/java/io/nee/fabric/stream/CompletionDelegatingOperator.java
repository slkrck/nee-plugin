/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.fabric.stream;

import java.util.Objects;
import java.util.concurrent.Executor;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.CoreSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxOperator;

public class CompletionDelegatingOperator extends FluxOperator<Record, Record> {

  private final Flux<Record> recordStream;
  private final Executor executor;

  public CompletionDelegatingOperator(Flux<Record> recordStream, Executor executor) {
    super(recordStream);
    this.recordStream = recordStream;
    this.executor = executor;
  }

  public void subscribe(CoreSubscriber downstreamSubscriber) {
    this.recordStream
        .subscribeWith(new CompletionDelegatingOperator.UpstreamSubscriber(downstreamSubscriber));
  }

  private class UpstreamSubscriber implements Subscriber<Record> {

    private final Subscriber<Record> downstreamSubscriber;

    UpstreamSubscriber(Subscriber<Record> downstreamSubscriber) {
      this.downstreamSubscriber = downstreamSubscriber;
    }

    public void onSubscribe(Subscription subscription) {
      this.downstreamSubscriber.onSubscribe(subscription);
    }

    public void onNext(Record record) {
      this.downstreamSubscriber.onNext(record);
    }

    public void onError(Throwable throwable) {
      CompletionDelegatingOperator.this.executor.execute(() ->
      {
        this.downstreamSubscriber.onError(throwable);
      });
    }

    public void onComplete() {
      Executor n10000 = CompletionDelegatingOperator.this.executor;
      Subscriber n10001 = this.downstreamSubscriber;
      Objects.requireNonNull(n10001);
      n10000.execute(n10001::onComplete);
    }
  }
}
