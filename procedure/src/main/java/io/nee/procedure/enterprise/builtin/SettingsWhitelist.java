/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.procedure.enterprise.builtin;

import io.nee.kernel.impl.enterprise.configuration.EnterpriseEditionSettings;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.neo4j.configuration.Config;

public class SettingsWhitelist {

  private final List<Pattern> settingsWhitelist;

  public SettingsWhitelist(Config config) {
    this.settingsWhitelist = this
        .refreshWhiteList((List) config.get(EnterpriseEditionSettings.dynamic_setting_whitelist));
  }

  private List<Pattern> refreshWhiteList(List<String> whiteList) {
    return whiteList != null && !whiteList.isEmpty() ? whiteList.stream().map((s) ->
    {
      return s.replace("*", ".*");
    }).map(Pattern::compile).collect(Collectors.toList())
        : Collections.emptyList();
  }

  boolean isWhiteListed(String settingName) {
    return this.settingsWhitelist != null && this.settingsWhitelist.stream().anyMatch((pattern) ->
    {
      return pattern.matcher(settingName).matches();
    });
  }
}
