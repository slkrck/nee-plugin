/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.procedure.enterprise.builtin;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.neo4j.common.DependencyResolver;
import org.neo4j.internal.kernel.api.exceptions.ProcedureException;
import org.neo4j.internal.schema.IndexProviderDescriptor;
import org.neo4j.kernel.api.KernelTransaction;
import org.neo4j.kernel.impl.api.index.IndexingService;
import org.neo4j.procedure.Context;
import org.neo4j.procedure.Description;
import org.neo4j.procedure.Mode;
import org.neo4j.procedure.Name;
import org.neo4j.procedure.Procedure;
import org.neo4j.procedure.builtin.BuiltInProcedures.SchemaIndexInfo;
import org.neo4j.procedure.builtin.IndexProcedures;

public class EnterpriseBuiltInProcedures {

  @Context
  public KernelTransaction tx;
  @Context
  public DependencyResolver resolver;

  @Description("Create a named node key constraint. Backing index will use specified index provider and configuration (optional). Yield: name, labels, properties, providerName, status")
  @Procedure(name = "db.createNodeKey", mode = Mode.SCHEMA)
  public Stream<SchemaIndexInfo> createNodeKey(@Name("constraintName") String constraintName,
      @Name("labels") List<String> labels,
      @Name("properties") List<String> properties, @Name("providerName") String providerName,
      @Name(value = "config", defaultValue = "{}") Map<String, Object> config)
      throws ProcedureException {
    IndexProcedures indexProcedures = this.indexProcedures();
    IndexProviderDescriptor indexProviderDescriptor = this.getIndexProviderDescriptor(providerName);
    return indexProcedures
        .createNodeKey(constraintName, labels, properties, indexProviderDescriptor, config);
  }

  private IndexProviderDescriptor getIndexProviderDescriptor(String providerName) {
    return this.resolver.resolveDependency(IndexingService.class).indexProviderByName(providerName);
  }

  private IndexProcedures indexProcedures() {
    return new IndexProcedures(this.tx, this.resolver.resolveDependency(IndexingService.class));
  }
}
