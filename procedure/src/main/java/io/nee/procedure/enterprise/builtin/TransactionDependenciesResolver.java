/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.procedure.enterprise.builtin;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.neo4j.kernel.api.KernelTransactionHandle;
import org.neo4j.kernel.api.query.QuerySnapshot;
import org.neo4j.kernel.impl.locking.ActiveLock;
import org.neo4j.lock.ResourceType;

class TransactionDependenciesResolver {

  private final Map<KernelTransactionHandle, Optional<QuerySnapshot>> handleSnapshotsMap;
  private final Map<KernelTransactionHandle, Set<KernelTransactionHandle>> directDependencies;

  TransactionDependenciesResolver(
      Map<KernelTransactionHandle, Optional<QuerySnapshot>> handleSnapshotsMap) {
    this.handleSnapshotsMap = handleSnapshotsMap;
    this.directDependencies = this.initDirectDependencies();
  }

  private static Function<KernelTransactionHandle, List<ActiveLock>> getTransactionLocks() {
    return (transactionHandle) ->
    {
      return (List) transactionHandle.activeLocks().collect(Collectors.toList());
    };
  }

  private static void evaluateDirectDependencies(
      Map<KernelTransactionHandle, Set<KernelTransactionHandle>> directDependencies,
      Map<KernelTransactionHandle, List<ActiveLock>> handleLocksMap,
      KernelTransactionHandle txHandle,
      QuerySnapshot querySnapshot) {
    List<ActiveLock> waitingOnLocks = querySnapshot.waitingLocks();
    Iterator n5 = waitingOnLocks.iterator();

    while (n5.hasNext()) {
      ActiveLock activeLock = (ActiveLock) n5.next();
      Iterator n7 = handleLocksMap.entrySet().iterator();

      while (n7.hasNext()) {
        Entry<KernelTransactionHandle, List<ActiveLock>> handleListEntry = (Entry) n7.next();
        KernelTransactionHandle kernelTransactionHandle = handleListEntry.getKey();
        if (!kernelTransactionHandle.equals(txHandle) && isBlocked(activeLock,
            handleListEntry.getValue())) {
          Set<KernelTransactionHandle> kernelTransactionHandles = directDependencies
              .computeIfAbsent(txHandle, (handle) ->
              {
                return new HashSet();
              });
          kernelTransactionHandles.add(kernelTransactionHandle);
        }
      }
    }
  }

  private static boolean isBlocked(ActiveLock activeLock, List<ActiveLock> activeLocks) {
    return "EXCLUSIVE".equals(activeLock.mode()) ? haveAnyLocking(activeLocks,
        activeLock.resourceType(), activeLock.resourceId())
        : haveExclusiveLocking(activeLocks, activeLock.resourceType(), activeLock.resourceId());
  }

  private static boolean haveAnyLocking(List<ActiveLock> locks, ResourceType resourceType,
      long resourceId) {
    return locks.stream().anyMatch((lock) ->
    {
      return lock.resourceId() == resourceId && lock.resourceType() == resourceType;
    });
  }

  private static boolean haveExclusiveLocking(List<ActiveLock> locks, ResourceType resourceType,
      long resourceId) {
    return locks.stream().anyMatch((lock) ->
    {
      return "EXCLUSIVE".equals(lock.mode()) && lock.resourceId() == resourceId
          && lock.resourceType() == resourceType;
    });
  }

  private static String describe(Set<KernelTransactionHandle> allBlockers) {
    if (allBlockers.isEmpty()) {
      return "";
    } else {
      StringJoiner stringJoiner = new StringJoiner(", ", "[", "]");
      Iterator n2 = allBlockers.iterator();

      while (n2.hasNext()) {
        KernelTransactionHandle blocker = (KernelTransactionHandle) n2.next();
        stringJoiner.add(blocker.getUserTransactionName());
      }

      return stringJoiner.toString();
    }
  }

  boolean isBlocked(KernelTransactionHandle handle) {
    return this.directDependencies.get(handle) != null;
  }

  String describeBlockingTransactions(KernelTransactionHandle handle) {
    Set<KernelTransactionHandle> allBlockers = new TreeSet(
        Comparator.comparingLong(KernelTransactionHandle::getUserTransactionId));
    Set<KernelTransactionHandle> handles = this.directDependencies.get(handle);
    if (handles != null) {
      ArrayDeque blockerQueue = new ArrayDeque(handles);

      while (!blockerQueue.isEmpty()) {
        KernelTransactionHandle transactionHandle = (KernelTransactionHandle) blockerQueue.pop();
        if (allBlockers.add(transactionHandle)) {
          Set<KernelTransactionHandle> transactionHandleSet = this.directDependencies
              .get(transactionHandle);
          if (transactionHandleSet != null) {
            blockerQueue.addAll(transactionHandleSet);
          }
        }
      }
    }

    return describe(allBlockers);
  }

  Map<String, Object> describeBlockingLocks(KernelTransactionHandle handle) {
    Optional<QuerySnapshot> snapshot = this.handleSnapshotsMap.get(handle);
    return snapshot.map(QuerySnapshot::resourceInformation).orElse(Collections.emptyMap());
  }

  private Map<KernelTransactionHandle, Set<KernelTransactionHandle>> initDirectDependencies() {
    Map<KernelTransactionHandle, Set<KernelTransactionHandle>> directDependencies = new HashMap();
    Map<KernelTransactionHandle, List<ActiveLock>> transactionLocksMap =
        this.handleSnapshotsMap.keySet().stream()
            .collect(Collectors.toMap(Function.identity(), getTransactionLocks()));
    Iterator n3 = this.handleSnapshotsMap.entrySet().iterator();

    while (n3.hasNext()) {
      Entry<KernelTransactionHandle, Optional<QuerySnapshot>> entry = (Entry) n3.next();
      Optional<QuerySnapshot> snapshot = entry.getValue();
      if (snapshot.isPresent()) {
        KernelTransactionHandle txHandle = entry.getKey();
        evaluateDirectDependencies(directDependencies, transactionLocksMap, txHandle,
            snapshot.get());
      }
    }

    return directDependencies;
  }
}
