/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.procedure.enterprise.builtin;

import java.time.ZoneId;
import java.util.Map;
import java.util.Optional;
import org.neo4j.internal.kernel.api.connectioninfo.ClientConnectionInfo;
import org.neo4j.kernel.api.KernelTransactionHandle;
import org.neo4j.kernel.api.exceptions.InvalidArgumentsException;
import org.neo4j.kernel.api.query.QuerySnapshot;
import org.neo4j.kernel.impl.api.TransactionExecutionStatistic;
import org.neo4j.procedure.builtin.ProceduresTimeFormatHelper;

public class TransactionStatusResult {

  private static final String RUNNING_STATE = "Running";
  private static final String CLOSING_STATE = "Closing";
  private static final String BLOCKED_STATE = "Blocked by: ";
  private static final String TERMINATED_STATE = "Terminated with reason: %s";
  public final String transactionId;
  public final String username;
  public final Map<String, Object> metaData;
  public final String startTime;
  public final String protocol;
  public final String clientAddress;
  public final String requestUri;
  public final String currentQueryId;
  public final String currentQuery;
  public final long activeLockCount;
  public final String status;
  public final long elapsedTimeMillis;
  public final Long cpuTimeMillis;
  public final long waitTimeMillis;
  public final Long idleTimeMillis;
  public final Long allocatedBytes;
  public final Long allocatedDirectBytes;
  public final long pageHits;
  public final long pageFaults;
  public final String connectionId;
  public final String initializationStackTrace;
  public final String database;
  public Map<String, Object> resourceInformation;

  public TransactionStatusResult(String database, KernelTransactionHandle transaction,
      TransactionDependenciesResolver transactionDependenciesResolver,
      Map<KernelTransactionHandle, Optional<QuerySnapshot>> handleSnapshotsMap, ZoneId zoneId)
      throws InvalidArgumentsException {
    this.database = database;
    this.transactionId = (new DbmsTransactionId(database, transaction.getUserTransactionId()))
        .toString();
    this.username = transaction.subject().username();
    this.startTime = ProceduresTimeFormatHelper.formatTime(transaction.startTime(), zoneId);
    this.activeLockCount = transaction.activeLocks().count();
    Optional<QuerySnapshot> querySnapshot = handleSnapshotsMap.get(transaction);
    TransactionExecutionStatistic statistic = transaction.transactionStatistic();
    this.elapsedTimeMillis = statistic.getElapsedTimeMillis();
    this.cpuTimeMillis = statistic.getCpuTimeMillis();
    this.allocatedBytes = statistic.getHeapAllocatedBytes();
    this.allocatedDirectBytes = statistic.getDirectAllocatedBytes();
    this.waitTimeMillis = statistic.getWaitTimeMillis();
    this.idleTimeMillis = statistic.getIdleTimeMillis();
    this.pageHits = statistic.getPageHits();
    this.pageFaults = statistic.getPageFaults();
    if (querySnapshot.isPresent()) {
      QuerySnapshot snapshot = querySnapshot.get();
      this.currentQueryId = (new DbmsQueryId(database, snapshot.internalQueryId())).toString();
      this.currentQuery = snapshot.queryText();
    } else {
      this.currentQueryId = "";
      this.currentQuery = "";
    }

    ClientConnectionInfo clientInfo = transaction.clientInfo();
    this.protocol = clientInfo.protocol();
    this.clientAddress = clientInfo.clientAddress();
    this.requestUri = clientInfo.requestURI();
    this.connectionId = clientInfo.connectionId();
    this.resourceInformation = transactionDependenciesResolver.describeBlockingLocks(transaction);
    this.status = getStatus(transaction, transactionDependenciesResolver);
    this.metaData = transaction.getMetaData();
    this.initializationStackTrace = transaction.transactionInitialisationTrace().getTrace();
  }

  private static String getStatus(KernelTransactionHandle handle,
      TransactionDependenciesResolver transactionDependenciesResolver) {
    return handle.terminationReason().map((reason) ->
    {
      return String.format("Terminated with reason: %s", reason.code());
    }).orElseGet(() ->
    {
      return getExecutingStatus(handle, transactionDependenciesResolver);
    });
  }

  private static String getExecutingStatus(KernelTransactionHandle handle,
      TransactionDependenciesResolver transactionDependenciesResolver) {
    if (transactionDependenciesResolver.isBlocked(handle)) {
      String n10000 = transactionDependenciesResolver.describeBlockingTransactions(handle);
      return "Blocked by: " + n10000;
    } else {
      return handle.isClosing() ? "Closing" : "Running";
    }
  }
}
