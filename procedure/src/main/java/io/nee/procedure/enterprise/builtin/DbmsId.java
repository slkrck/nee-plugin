/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.procedure.enterprise.builtin;

import java.util.Objects;
import org.neo4j.configuration.helpers.DatabaseNameValidator;
import org.neo4j.configuration.helpers.NormalizedDatabaseName;
import org.neo4j.kernel.api.exceptions.InvalidArgumentsException;

class DbmsId {

  private final String separator;
  private final NormalizedDatabaseName database;
  private final long internalId;

  DbmsId(String queryIdText, String separator) throws InvalidArgumentsException {
    this.separator = separator;

    try {
      int i = queryIdText.lastIndexOf(separator);
      if (i != -1) {
        this.database = normalizeAndValidateDatabaseName(queryIdText.substring(0, i));
        if (this.database.name().length() > 0) {
          String qid = queryIdText.substring(i + separator.length());
          this.internalId = Long.parseLong(qid);
          if (this.internalId <= 0L) {
            throw new InvalidArgumentsException(
                "Negative ids are not supported (expected format: <databasename>" + separator
                    + "<id>)");
          }

          return;
        }
      }
    } catch (NumberFormatException n5) {
      throw new InvalidArgumentsException(
          "Could not parse id (expected format: <databasename>" + separator + "<id>)", n5);
    }

    throw new InvalidArgumentsException(
        "Could not parse id (expected format: <databasename>" + separator + "<id>)");
  }

  DbmsId(String database, long internalId, String separator) throws InvalidArgumentsException {
    this.separator = separator;
    this.database = new NormalizedDatabaseName(Objects.requireNonNull(database));
    if (internalId <= 0L) {
      throw new InvalidArgumentsException(
          "Negative ids are not supported (expected format: <databasename>" + separator + "<id>)");
    } else {
      this.internalId = internalId;
    }
  }

  private static NormalizedDatabaseName normalizeAndValidateDatabaseName(String databaseName) {
    NormalizedDatabaseName normalizedDatabaseName = new NormalizedDatabaseName(databaseName);
    DatabaseNameValidator.validateInternalDatabaseName(normalizedDatabaseName);
    return normalizedDatabaseName;
  }

  long internalId() {
    return this.internalId;
  }

  NormalizedDatabaseName database() {
    return this.database;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      DbmsId other = (DbmsId) o;
      return this.internalId == other.internalId && this.database.equals(other.database);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.database, this.internalId);
  }

  public String toString() {
    String n10000 = this.database.name();
    return n10000 + this.separator + this.internalId;
  }
}
