/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.procedure.enterprise.builtin;

import java.time.ZoneId;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.spatial.CRS;
import org.neo4j.graphdb.spatial.Coordinate;
import org.neo4j.graphdb.spatial.Point;
import org.neo4j.internal.kernel.api.connectioninfo.ClientConnectionInfo;
import org.neo4j.kernel.api.exceptions.InvalidArgumentsException;
import org.neo4j.kernel.api.query.ExecutingQuery;
import org.neo4j.kernel.api.query.QuerySnapshot;
import org.neo4j.kernel.impl.core.TransactionalEntityFactory;
import org.neo4j.kernel.impl.util.BaseToObjectValueWriter;
import org.neo4j.procedure.builtin.ProceduresTimeFormatHelper;
import org.neo4j.values.storable.CoordinateReferenceSystem;
import org.neo4j.values.virtual.MapValue;

public class QueryStatusResult {

  public final String queryId;
  public final String username;
  public final Map<String, Object> metaData;
  public final String query;
  public final Map<String, Object> parameters;
  public final String planner;
  public final String runtime;
  public final List<Map<String, String>> indexes;
  public final String startTime;
  public final String protocol;
  public final String clientAddress;
  public final String requestUri;
  public final String status;
  public final Map<String, Object> resourceInformation;
  public final long activeLockCount;
  public final long elapsedTimeMillis;
  public final Long cpuTimeMillis;
  public final long waitTimeMillis;
  public final Long idleTimeMillis;
  public final Long allocatedBytes;
  public final long pageHits;
  public final long pageFaults;
  public final String connectionId;
  public final String database;

  QueryStatusResult(ExecutingQuery query, TransactionalEntityFactory manager, ZoneId zoneId,
      String database) throws InvalidArgumentsException {
    this(query.snapshot(), manager, zoneId, database);
  }

  private QueryStatusResult(QuerySnapshot query, TransactionalEntityFactory manager, ZoneId zoneId,
      String database) throws InvalidArgumentsException {
    this.queryId = (new DbmsQueryId(database, query.internalQueryId())).toString();
    this.username = query.username();
    this.query = query.queryText();
    this.database = database;
    this.parameters = asRawMap(query.queryParameters(),
        new QueryStatusResult.ParameterWriter(manager));
    this.startTime = ProceduresTimeFormatHelper.formatTime(query.startTimestampMillis(), zoneId);
    this.elapsedTimeMillis = this.asMillis(query.elapsedTimeMicros());
    ClientConnectionInfo clientConnection = query.clientConnection();
    this.protocol = clientConnection.protocol();
    this.clientAddress = clientConnection.clientAddress();
    this.requestUri = clientConnection.requestURI();
    this.metaData = query.transactionAnnotationData();
    this.cpuTimeMillis = this.asMillis(query.cpuTimeMicros());
    this.status = query.status();
    this.resourceInformation = query.resourceInformation();
    this.activeLockCount = query.activeLockCount();
    this.waitTimeMillis = this.asMillis(query.waitTimeMicros());
    this.idleTimeMillis = this.asMillis(query.idleTimeMicros());
    this.planner = query.planner();
    this.runtime = query.runtime();
    this.indexes = query.indexes();
    this.allocatedBytes = query.allocatedBytes().orElse(null);
    this.pageHits = query.pageHits();
    this.pageFaults = query.pageFaults();
    this.connectionId = clientConnection.connectionId();
  }

  private static Map<String, Object> asRawMap(MapValue mapValue,
      QueryStatusResult.ParameterWriter writer) {
    HashMap<String, Object> map = new HashMap();
    mapValue.foreach((s, value) ->
    {
      value.writeTo(writer);
      map.put(s, writer.value());
    });
    return map;
  }

  private Long asMillis(Long micros) {
    return micros == null ? null : TimeUnit.MICROSECONDS.toMillis(micros);
  }

  private static class ParameterWriter extends BaseToObjectValueWriter<RuntimeException> {

    private final TransactionalEntityFactory entityFactory;

    private ParameterWriter(TransactionalEntityFactory entityFactory) {
      this.entityFactory = entityFactory;
    }

    protected Node newNodeEntityById(long id) {
      return this.entityFactory.newNodeEntity(id);
    }

    protected Relationship newRelationshipEntityById(long id) {
      return this.entityFactory.newRelationshipEntity(id);
    }

    protected Point newPoint(final CoordinateReferenceSystem crs, final double[] coordinate) {
      return new Point() {
        public String getGeometryType() {
          return "Point";
        }

        public List<Coordinate> getCoordinates() {
          return Collections.singletonList(new Coordinate(coordinate));
        }

        public CRS getCRS() {
          return crs;
        }
      };
    }
  }
}
