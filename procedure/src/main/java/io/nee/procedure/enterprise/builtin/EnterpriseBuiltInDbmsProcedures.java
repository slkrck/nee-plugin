/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.procedure.enterprise.builtin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.neo4j.common.DependencyResolver;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.configuration.SettingImpl;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.security.AuthorizationViolationException;
import org.neo4j.internal.helpers.TimeUtil;
import org.neo4j.internal.kernel.api.procs.ProcedureSignature;
import org.neo4j.internal.kernel.api.procs.UserFunctionSignature;
import org.neo4j.internal.kernel.api.security.SecurityContext;
import org.neo4j.kernel.api.KernelTransaction;
import org.neo4j.kernel.api.KernelTransactionHandle;
import org.neo4j.kernel.api.exceptions.InvalidArgumentsException;
import org.neo4j.kernel.api.net.NetworkConnectionTracker;
import org.neo4j.kernel.api.net.TrackedNetworkConnection;
import org.neo4j.kernel.api.procedure.GlobalProcedures;
import org.neo4j.kernel.api.procedure.SystemProcedure;
import org.neo4j.kernel.api.query.ExecutingQuery;
import org.neo4j.kernel.api.query.QuerySnapshot;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.kernel.impl.api.KernelTransactions;
import org.neo4j.kernel.impl.coreapi.InternalTransaction;
import org.neo4j.kernel.impl.query.FunctionInformation;
import org.neo4j.kernel.impl.query.QueryExecutionEngine;
import org.neo4j.kernel.impl.transaction.log.checkpoint.CheckPointer;
import org.neo4j.kernel.impl.transaction.log.checkpoint.SimpleTriggerInfo;
import org.neo4j.logging.Log;
import org.neo4j.procedure.Admin;
import org.neo4j.procedure.Context;
import org.neo4j.procedure.Description;
import org.neo4j.procedure.Mode;
import org.neo4j.procedure.Name;
import org.neo4j.procedure.Procedure;
import org.neo4j.resources.Profiler;
import org.neo4j.scheduler.ActiveGroup;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public class EnterpriseBuiltInDbmsProcedures {

  @Context
  public Log log;
  @Context
  public DependencyResolver resolver;
  @Context
  public Transaction transaction;
  @Context
  public SecurityContext securityContext;
  @Context
  public KernelTransaction kernelTransaction;

  private static Set<KernelTransactionHandle> getExecutingTransactions(
      DatabaseContext databaseContext) {
    return databaseContext.dependencies().resolveDependency(KernelTransactions.class)
        .executingTransactions();
  }

  @SystemProcedure
  @Description("List all accepted network connections at this instance that are visible to the user.")
  @Procedure(name = "dbms.listConnections", mode = Mode.DBMS)
  public Stream<ListConnectionResult> listConnections() {
    this.securityContext.assertCredentialsNotExpired();
    NetworkConnectionTracker connectionTracker = this.getConnectionTracker();
    ZoneId timeZone = this.getConfiguredTimeZone();
    return connectionTracker.activeConnections().stream().filter((connection) ->
    {
      return this.isAdminOrSelf(connection.username());
    }).map((connection) ->
    {
      return new ListConnectionResult(connection, timeZone);
    });
  }

  @SystemProcedure
  @Description("Kill network connection with the given connection id.")
  @Procedure(name = "dbms.killConnection", mode = Mode.DBMS)
  public Stream<ConnectionTerminationResult> killConnection(@Name("id") String id) {
    return this.killConnections(Collections.singletonList(id));
  }

  @SystemProcedure
  @Description("Kill all network connections with the given connection ids.")
  @Procedure(name = "dbms.killConnections", mode = Mode.DBMS)
  public Stream<ConnectionTerminationResult> killConnections(@Name("ids") List<String> ids) {
    this.securityContext.assertCredentialsNotExpired();
    NetworkConnectionTracker connectionTracker = this.getConnectionTracker();
    return ids.stream().map((id) ->
    {
      return this.killConnection(id, connectionTracker);
    });
  }

  private NetworkConnectionTracker getConnectionTracker() {
    return this.resolver.resolveDependency(NetworkConnectionTracker.class);
  }

  private ConnectionTerminationResult killConnection(String id,
      NetworkConnectionTracker connectionTracker) {
    TrackedNetworkConnection connection = connectionTracker.get(id);
    if (connection != null) {
      if (this.isAdminOrSelf(connection.username())) {
        connection.close();
        return new ConnectionTerminationResult(id, connection.username());
      } else {
        throw new AuthorizationViolationException("Permission denied.");
      }
    } else {
      return new ConnectionTerminationFailedResult(id);
    }
  }

  @SystemProcedure
  @Description("List all functions in the DBMS.")
  @Procedure(name = "dbms.functions", mode = Mode.DBMS)
  public Stream<EnterpriseBuiltInDbmsProcedures.FunctionResult> listFunctions() {
    this.securityContext.assertCredentialsNotExpired();
    QueryExecutionEngine queryExecutionEngine = this.resolver
        .resolveDependency(QueryExecutionEngine.class);
    List<FunctionInformation> providedLanguageFunctions = queryExecutionEngine
        .getProvidedLanguageFunctions();
    Stream<EnterpriseBuiltInDbmsProcedures.FunctionResult> languageFunctions = providedLanguageFunctions
        .stream().map((n) ->
        {
          return new EnterpriseBuiltInDbmsProcedures.FunctionResult(
              n);
        });
    Stream<EnterpriseBuiltInDbmsProcedures.FunctionResult> loadedFunctions =
        this.resolver.resolveDependency(GlobalProcedures.class).getAllNonAggregatingFunctions()
            .map((f) ->
            {
              return new EnterpriseBuiltInDbmsProcedures.FunctionResult(
                  f, false);
            });
    Stream<EnterpriseBuiltInDbmsProcedures.FunctionResult> loadedAggregationFunctions =
        this.resolver.resolveDependency(GlobalProcedures.class).getAllAggregatingFunctions()
            .map((f) ->
            {
              return new EnterpriseBuiltInDbmsProcedures.FunctionResult(
                  f, true);
            });
    return Stream
        .concat(Stream.concat(languageFunctions, loadedFunctions), loadedAggregationFunctions)
        .sorted(Comparator.comparing((a) ->
        {
          return a.name;
        }));
  }

  @SystemProcedure
  @Description("List all procedures in the DBMS.")
  @Procedure(name = "dbms.procedures", mode = Mode.DBMS)
  public Stream<EnterpriseBuiltInDbmsProcedures.ProcedureResult> listProcedures() {
    this.securityContext.assertCredentialsNotExpired();
    GlobalProcedures globalProcedures = this.resolver.resolveDependency(GlobalProcedures.class);
    return globalProcedures.getAllProcedures().stream().sorted(Comparator.comparing((a) ->
    {
      return a.name().toString();
    })).map(EnterpriseBuiltInDbmsProcedures.ProcedureResult::new);
  }

  @Admin
  @SystemProcedure
  @Description("Updates a given setting value. Passing an empty value will result in removing the configured value and falling back to the default value. Changes will not persist and will be lost if the server is restarted.")
  @Procedure(name = "dbms.setConfigValue", mode = Mode.DBMS)
  public void setConfigValue(@Name("setting") String setting, @Name("value") String value) {
    Config config = this.resolver.resolveDependency(Config.class);
    SettingImpl<Object> settingObj = (SettingImpl) config.getSetting(setting);
    SettingsWhitelist settingsWhiteList = this.resolver.resolveDependency(SettingsWhitelist.class);
    if (settingsWhiteList.isWhiteListed(setting)) {
      config.setDynamic(settingObj, settingObj.parse(StringUtils.isNotEmpty(value) ? value : null),
          "dbms.setConfigValue");
    } else {
      throw new AuthorizationViolationException("Failed to set value for `" + setting
          + "` using procedure `dbms.setConfigValue`: access denied.");
    }
  }

  @SystemProcedure
  @Description("List all queries currently executing at this instance that are visible to the user.")
  @Procedure(name = "dbms.listQueries", mode = Mode.DBMS)
  public Stream<QueryStatusResult> listQueries() throws InvalidArgumentsException {
    this.securityContext.assertCredentialsNotExpired();
    ZoneId zoneId = this.getConfiguredTimeZone();
    List<QueryStatusResult> result = new ArrayList();
    Iterator n3 = this.getDatabaseManager().registeredDatabases().values().iterator();

    while (n3.hasNext()) {
      DatabaseContext databaseContext = (DatabaseContext) n3.next();
      Iterator n5 = getExecutingTransactions(databaseContext).iterator();

      while (n5.hasNext()) {
        KernelTransactionHandle tx = (KernelTransactionHandle) n5.next();
        if (tx.executingQuery().isPresent()) {
          ExecutingQuery query = tx.executingQuery().get();
          if (this.isAdminOrSelf(query.username())) {
            result.add(new QueryStatusResult(query, (InternalTransaction) this.transaction, zoneId,
                databaseContext.databaseFacade().databaseName()));
          }
        }
      }
    }

    return result.stream();
  }

  @SystemProcedure
  @Description("List all transactions currently executing at this instance that are visible to the user.")
  @Procedure(name = "dbms.listTransactions", mode = Mode.DBMS)
  public Stream<TransactionStatusResult> listTransactions() throws InvalidArgumentsException {
    this.securityContext.assertCredentialsNotExpired();
    ZoneId zoneId = this.getConfiguredTimeZone();
    List<TransactionStatusResult> result = new ArrayList();
    Iterator n3 = this.getDatabaseManager().registeredDatabases().values().iterator();

    while (n3.hasNext()) {
      DatabaseContext databaseContext = (DatabaseContext) n3.next();
      Map<KernelTransactionHandle, Optional<QuerySnapshot>> handleQuerySnapshotsMap = new HashMap();
      Iterator n6 = getExecutingTransactions(databaseContext).iterator();

      while (n6.hasNext()) {
        KernelTransactionHandle tx = (KernelTransactionHandle) n6.next();
        if (this.isAdminOrSelf(tx.subject().username())) {
          handleQuerySnapshotsMap.put(tx, tx.executingQuery().map(ExecutingQuery::snapshot));
        }
      }

      TransactionDependenciesResolver transactionBlockerResolvers = new TransactionDependenciesResolver(
          handleQuerySnapshotsMap);
      Iterator n10 = handleQuerySnapshotsMap.keySet().iterator();

      while (n10.hasNext()) {
        KernelTransactionHandle tx = (KernelTransactionHandle) n10.next();
        result.add(
            new TransactionStatusResult(databaseContext.databaseFacade().databaseName(), tx,
                transactionBlockerResolvers, handleQuerySnapshotsMap,
                zoneId));
      }
    }

    return result.stream();
  }

  @SystemProcedure
  @Description("Kill transaction with provided id.")
  @Procedure(name = "dbms.killTransaction", mode = Mode.DBMS)
  public Stream<TransactionMarkForTerminationResult> killTransaction(
      @Name("id") String transactionId) throws InvalidArgumentsException {
    Objects.requireNonNull(transactionId);
    return this.killTransactions(Collections.singletonList(transactionId));
  }

  @SystemProcedure
  @Description("Kill transactions with provided ids.")
  @Procedure(name = "dbms.killTransactions", mode = Mode.DBMS)
  public Stream<TransactionMarkForTerminationResult> killTransactions(
      @Name("ids") List<String> transactionIds) throws InvalidArgumentsException {
    Objects.requireNonNull(transactionIds);
    this.securityContext.assertCredentialsNotExpired();
    this.log
        .warn("User %s trying to kill transactions: %s.", this.securityContext.subject().username(),
            transactionIds.toString());
    DatabaseManager<DatabaseContext> databaseManager = this.getDatabaseManager();
    DatabaseIdRepository databaseIdRepository = databaseManager.databaseIdRepository();
    Map<NamedDatabaseId, Set<DbmsTransactionId>> byDatabase = new HashMap();
    Iterator n5 = transactionIds.iterator();

    while (n5.hasNext()) {
      String idText = (String) n5.next();
      DbmsTransactionId id = new DbmsTransactionId(idText);
      Optional<NamedDatabaseId> namedDatabaseId = databaseIdRepository.getByName(id.database());
      namedDatabaseId.ifPresent((databaseIdx) ->
      {
        byDatabase.computeIfAbsent(databaseIdx, (ignore) ->
        {
          return new HashSet();
        }).add(id);
      });
    }

    Map<String, KernelTransactionHandle> handles = new HashMap(transactionIds.size());
    Iterator n16 = byDatabase.entrySet().iterator();

    while (true) {
      Optional maybeDatabaseContext;
      Entry entry;
      NamedDatabaseId databaseId;
      do {
        if (!n16.hasNext()) {
          return transactionIds.stream().map((idx) ->
          {
            return this.terminateTransaction(handles, idx);
          });
        }

        entry = (Entry) n16.next();
        databaseId = (NamedDatabaseId) entry.getKey();
        maybeDatabaseContext = databaseManager.getDatabaseContext(databaseId);
      }
      while (!maybeDatabaseContext.isPresent());

      Set<DbmsTransactionId> txIds = (Set) entry.getValue();
      DatabaseContext databaseContext = (DatabaseContext) maybeDatabaseContext.get();
      Iterator n12 = getExecutingTransactions(databaseContext).iterator();

      while (n12.hasNext()) {
        KernelTransactionHandle tx = (KernelTransactionHandle) n12.next();
        if (this.isAdminOrSelf(tx.subject().username())) {
          DbmsTransactionId txIdRepresentation = new DbmsTransactionId(databaseId.name(),
              tx.getUserTransactionId());
          if (txIds.contains(txIdRepresentation)) {
            handles.put(txIdRepresentation.toString(), tx);
          }
        }
      }
    }
  }

  private TransactionMarkForTerminationResult terminateTransaction(
      Map<String, KernelTransactionHandle> handles, String transactionId) {
    KernelTransactionHandle handle = handles.get(transactionId);
    String currentUser = this.securityContext.subject().username();
    if (handle == null) {
      return new TransactionMarkForTerminationFailedResult(transactionId, currentUser);
    } else if (handle.isClosing()) {
      return new TransactionMarkForTerminationFailedResult(transactionId, currentUser,
          "Unable to kill closing transactions.");
    } else {
      this.log.debug("User %s terminated transaction %d.", currentUser, transactionId);
      handle.markForTermination(org.neo4j.kernel.api.exceptions.Status.Transaction.Terminated);
      return new TransactionMarkForTerminationResult(transactionId, handle.subject().username());
    }
  }

  @SystemProcedure
  @Description("List the active lock requests granted for the transaction executing the query with the given query id.")
  @Procedure(name = "dbms.listActiveLocks", mode = Mode.DBMS)
  public Stream<ActiveLocksResult> listActiveLocks(@Name("queryId") String queryIdText)
      throws InvalidArgumentsException {
    this.securityContext.assertCredentialsNotExpired();
    DbmsQueryId dbmsQueryId = new DbmsQueryId(queryIdText);
    DatabaseManager<DatabaseContext> databaseManager = this.getDatabaseManager();
    DatabaseIdRepository databaseIdRepository = databaseManager.databaseIdRepository();
    Optional<NamedDatabaseId> maybeNamedDatabaseId = databaseIdRepository
        .getByName(dbmsQueryId.database());
    if (maybeNamedDatabaseId.isPresent()) {
      NamedDatabaseId namedDatabaseId = maybeNamedDatabaseId.get();
      Optional<DatabaseContext> maybeDatabaseContext = databaseManager
          .getDatabaseContext(namedDatabaseId);
      if (maybeDatabaseContext.isPresent()) {
        DatabaseContext databaseContext = maybeDatabaseContext.get();
        Iterator n9 = getExecutingTransactions(databaseContext).iterator();

        while (n9.hasNext()) {
          KernelTransactionHandle tx = (KernelTransactionHandle) n9.next();
          if (tx.executingQuery().isPresent()) {
            ExecutingQuery query = tx.executingQuery().get();
            if (query.internalQueryId() == dbmsQueryId.internalId()) {
              if (this.isAdminOrSelf(query.username())) {
                return tx.activeLocks().map(ActiveLocksResult::new);
              }

              throw new AuthorizationViolationException("Permission denied.");
            }
          }
        }
      }
    }

    return Stream.empty();
  }

  @SystemProcedure
  @Description("Kill all transactions executing the query with the given query id.")
  @Procedure(name = "dbms.killQuery", mode = Mode.DBMS)
  public Stream<EnterpriseBuiltInDbmsProcedures.QueryTerminationResult> killQuery(
      @Name("id") String idText) throws InvalidArgumentsException {
    return this.killQueries(Collections.singletonList(idText));
  }

  @SystemProcedure
  @Description("Kill all transactions executing a query with any of the given query ids.")
  @Procedure(name = "dbms.killQueries", mode = Mode.DBMS)
  public Stream<EnterpriseBuiltInDbmsProcedures.QueryTerminationResult> killQueries(
      @Name("ids") List<String> idTexts) throws InvalidArgumentsException {
    this.securityContext.assertCredentialsNotExpired();
    DatabaseManager<DatabaseContext> databaseManager = this.getDatabaseManager();
    DatabaseIdRepository databaseIdRepository = databaseManager.databaseIdRepository();
    Set<NamedDatabaseId> affectedDatabases = new HashSet();
    Set<DbmsQueryId> dbmsQueryIds = new HashSet(idTexts.size());
    Iterator n6 = idTexts.iterator();

    DbmsQueryId dbmsQueryId;
    while (n6.hasNext()) {
      String idText = (String) n6.next();
      dbmsQueryId = new DbmsQueryId(idText);
      dbmsQueryIds.add(dbmsQueryId);
      Optional<NamedDatabaseId> dbId = databaseIdRepository.getByName(dbmsQueryId.database());
      Objects.requireNonNull(affectedDatabases);
      dbId.ifPresent(affectedDatabases::add);
    }

    List<EnterpriseBuiltInDbmsProcedures.QueryTerminationResult> result = new ArrayList(
        dbmsQueryIds.size());
    Iterator n15 = affectedDatabases.iterator();

    while (true) {
      Optional maybeDatabaseContext;
      do {
        if (!n15.hasNext()) {
          n15 = dbmsQueryIds.iterator();

          while (n15.hasNext()) {
            dbmsQueryId = (DbmsQueryId) n15.next();
            result.add(
                new EnterpriseBuiltInDbmsProcedures.QueryFailedTerminationResult(dbmsQueryId, "n/a",
                    "No Query found with this id"));
          }

          return result.stream();
        }

        NamedDatabaseId databaseId = (NamedDatabaseId) n15.next();
        maybeDatabaseContext = databaseManager.getDatabaseContext(databaseId);
      }
      while (!maybeDatabaseContext.isPresent());

      DatabaseContext databaseContext = (DatabaseContext) maybeDatabaseContext.get();
      Iterator n11 = getExecutingTransactions(databaseContext).iterator();

      while (n11.hasNext()) {
        KernelTransactionHandle tx = (KernelTransactionHandle) n11.next();
        DbmsQueryId internalDbmsQueryId = new DbmsQueryId(
            databaseContext.databaseFacade().databaseName(),
            tx.executingQuery().map(ExecutingQuery::internalQueryId).orElse(-1L));
        if (dbmsQueryIds.remove(internalDbmsQueryId)) {
          result.add(this.killQueryTransaction(internalDbmsQueryId, tx));
        }
      }
    }
  }

  @Admin
  @SystemProcedure
  @Description("List the job groups that are active in the database internal job scheduler.")
  @Procedure(name = "dbms.scheduler.groups", mode = Mode.DBMS)
  public Stream<EnterpriseBuiltInDbmsProcedures.ActiveSchedulingGroup> schedulerActiveGroups() {
    JobScheduler scheduler = this.resolver.resolveDependency(JobScheduler.class);
    return scheduler.activeGroups().map(EnterpriseBuiltInDbmsProcedures.ActiveSchedulingGroup::new);
  }

  @Admin
  @SystemProcedure
  @Description("Begin profiling all threads within the given job group, for the specified duration. Note that profiling incurs overhead to a system, and will slow it down.")
  @Procedure(name = "dbms.scheduler.profile", mode = Mode.DBMS)
  public Stream<EnterpriseBuiltInDbmsProcedures.ProfileResult> schedulerProfileGroup(
      @Name("method") String method, @Name("group") String groupName,
      @Name("duration") String duration) throws InterruptedException {
    if (!"sample".equals(method)) {
      throw new IllegalArgumentException(
          "No such profiling method: '" + method + "'. Valid methods are: 'sample'.");
    } else {
      Profiler profiler = Profiler.profiler();
      Group group = null;
      Group[] n6 = Group.values();
      int n7 = n6.length;

      for (int n8 = 0; n8 < n7; ++n8) {
        Group value = n6[n8];
        if (value.groupName().equals(groupName)) {
          group = value;
          break;
        }
      }

      if (group == null) {
        throw new IllegalArgumentException("No such scheduling group: '" + groupName + "'.");
      } else {
        long durationNanos = TimeUnit.MILLISECONDS
            .toNanos(TimeUtil.parseTimeMillis.apply(duration));
        JobScheduler scheduler = this.resolver.resolveDependency(JobScheduler.class);
        long deadline = System.nanoTime() + durationNanos;

        try {
          scheduler.profileGroup(group, profiler);

          while (System.nanoTime() < deadline) {
            this.kernelTransaction.assertOpen();
            Thread.sleep(100L);
          }
        } finally {
          profiler.finish();
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(baos);
        profiler.printProfile(out, "Profiled group '" + group + "'.");
        out.flush();
        return Stream.of(new EnterpriseBuiltInDbmsProcedures.ProfileResult(baos.toString()));
      }
    }
  }

  @SystemProcedure
  @Description("Initiate and wait for a new check point, or wait any already on-going check point to complete. Note that this temporarily disables the `dbms.checkpoint.iops.limit` setting in order to make the check point complete faster. This might cause transaction throughput to degrade slightly, due to increased IO load.")
  @Procedure(name = "db.checkpoint", mode = Mode.DBMS)
  public Stream<EnterpriseBuiltInDbmsProcedures.CheckpointResult> checkpoint() throws IOException {
    CheckPointer checkPointer = this.resolver.resolveDependency(CheckPointer.class);
    KernelTransaction n10000 = this.kernelTransaction;
    Objects.requireNonNull(n10000);
    BooleanSupplier timeoutPredicate = n10000::isTerminated;
    long transactionId = checkPointer
        .tryCheckPoint(new SimpleTriggerInfo("Call to db.checkpoint() procedure"),
            timeoutPredicate);
    return Stream.of(
        transactionId == -1L ? EnterpriseBuiltInDbmsProcedures.CheckpointResult.TERMINATED
            : EnterpriseBuiltInDbmsProcedures.CheckpointResult.SUCCESS);
  }

  private DatabaseManager<DatabaseContext> getDatabaseManager() {
    return (DatabaseManager) this.resolver.resolveDependency(DatabaseManager.class);
  }

  private EnterpriseBuiltInDbmsProcedures.QueryTerminationResult killQueryTransaction(
      DbmsQueryId dbmsQueryId, KernelTransactionHandle handle) {
    Optional<ExecutingQuery> query = handle.executingQuery();
    ExecutingQuery executingQuery = query.orElseThrow(() ->
    {
      return new IllegalStateException(
          "Query should exist since we filtered based on query ids");
    });
    if (this.isAdminOrSelf(executingQuery.username())) {
      if (handle.isClosing()) {
        return new EnterpriseBuiltInDbmsProcedures.QueryFailedTerminationResult(dbmsQueryId,
            executingQuery.username(),
            "Unable to kill queries when underlying transaction is closing.");
      } else {
        handle.markForTermination(org.neo4j.kernel.api.exceptions.Status.Transaction.Terminated);
        return new EnterpriseBuiltInDbmsProcedures.QueryTerminationResult(dbmsQueryId,
            executingQuery.username(), "Query found");
      }
    } else {
      throw new AuthorizationViolationException("Permission denied.");
    }
  }

  private ZoneId getConfiguredTimeZone() {
    Config config = this.resolver.resolveDependency(Config.class);
    return config.get(GraphDatabaseSettings.db_timezone).getZoneId();
  }

  private boolean isAdminOrSelf(String username) {
    return this.securityContext.isAdmin() || this.securityContext.subject().hasUsername(username);
  }

  public enum CheckpointResult {
    SUCCESS(true, "Checkpoint completed."),
    TERMINATED(false,
        "Transaction terminated while waiting for the requested checkpoint operation to finish.");

    public final boolean success;
    public final String message;

    CheckpointResult(boolean success, String message) {
      this.success = success;
      this.message = message;
    }
  }

  public static class ProfileResult {

    public final String profile;

    public ProfileResult(String profile) {
      this.profile = profile;
    }
  }

  public static class ActiveSchedulingGroup {

    public final String group;
    public final long threads;

    ActiveSchedulingGroup(ActiveGroup activeGroup) {
      this.group = activeGroup.group.groupName();
      this.threads = activeGroup.threads;
    }
  }

  public static class QueryFailedTerminationResult extends
      EnterpriseBuiltInDbmsProcedures.QueryTerminationResult {

    public QueryFailedTerminationResult(DbmsQueryId dbmsQueryId, String username, String message) {
      super(dbmsQueryId, username, message);
    }
  }

  public static class QueryTerminationResult {

    public final String queryId;
    public final String username;
    public final String message;

    public QueryTerminationResult(DbmsQueryId dbmsQueryId, String username, String message) {
      this.queryId = dbmsQueryId.toString();
      this.username = username;
      this.message = message;
    }
  }

  public static class ProcedureResult {

    private static final List<String> ADMIN_PROCEDURES = Arrays
        .asList("changeUserPassword", "listRolesForUser");
    private static final List<String> NON_EDITOR_PROCEDURES = Arrays
        .asList("createLabel", "createProperty", "createRelationshipType");
    public final String name;
    public final String signature;
    public final String description;
    public final String mode;
    public final List<String> defaultBuiltInRoles;
    public final boolean worksOnSystem;

    public ProcedureResult(ProcedureSignature signature) {
      this.name = signature.name().toString();
      this.signature = signature.toString();
      this.description = signature.description().orElse("");
      this.mode = signature.mode().toString();
      this.worksOnSystem = signature.systemProcedure();
      this.defaultBuiltInRoles = new ArrayList();
      switch (signature.mode()) {
        case DBMS:
          if (!signature.admin() && !this.isAdminProcedure(signature.name().name())) {
            this.defaultBuiltInRoles.add("reader");
            this.defaultBuiltInRoles.add("editor");
            this.defaultBuiltInRoles.add("publisher");
            this.defaultBuiltInRoles.add("architect");
            this.defaultBuiltInRoles.add("admin");
            this.defaultBuiltInRoles.addAll(Arrays.asList(signature.allowed()));
          } else {
            this.defaultBuiltInRoles.add("admin");
          }
          break;
        case DEFAULT:
        case READ:
          this.defaultBuiltInRoles.add("reader");
        case WRITE:
          if (!NON_EDITOR_PROCEDURES.contains(signature.name().name())) {
            this.defaultBuiltInRoles.add("editor");
          }

          this.defaultBuiltInRoles.add("publisher");
        case SCHEMA:
          this.defaultBuiltInRoles.add("architect");
        default:
          this.defaultBuiltInRoles.add("admin");
          this.defaultBuiltInRoles.addAll(Arrays.asList(signature.allowed()));
      }
    }

    private boolean isAdminProcedure(String procedureName) {
      return this.name.startsWith("dbms.security.") && ADMIN_PROCEDURES.contains(procedureName);
    }
  }

  public static class FunctionResult {

    public final String name;
    public final String signature;
    public final String description;
    public final boolean aggregating;
    public final List<String> defaultBuiltInRoles;

    private FunctionResult(UserFunctionSignature signature, boolean isAggregation) {
      this.name = signature.name().toString();
      this.signature = signature.toString();
      this.description = signature.description().orElse("");
      this.defaultBuiltInRoles = Stream.of("admin", "reader", "editor", "publisher", "architect")
          .collect(Collectors.toList());
      this.defaultBuiltInRoles.addAll(Arrays.asList(signature.allowed()));
      this.aggregating = isAggregation;
    }

    private FunctionResult(FunctionInformation info) {
      this.name = info.getFunctionName();
      this.signature = info.getSignature();
      this.description = info.getDescription();
      this.aggregating = info.isAggregationFunction();
      this.defaultBuiltInRoles = Stream.of("admin", "reader", "editor", "publisher", "architect")
          .collect(Collectors.toList());
    }
  }
}
