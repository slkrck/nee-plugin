/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms.database;

import io.nee.kernel.impl.enterprise.configuration.EnterpriseEditionSettings;
import java.util.Optional;
import java.util.function.BiConsumer;
import org.neo4j.dbms.api.DatabaseExistsException;
import org.neo4j.dbms.api.DatabaseLimitReachedException;
import org.neo4j.dbms.api.DatabaseManagementException;
import org.neo4j.dbms.api.DatabaseNotFoundException;
import org.neo4j.dbms.database.AbstractDatabaseManager;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.graphdb.factory.module.edition.AbstractEditionModule;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;

public abstract class MultiDatabaseManager<DB extends DatabaseContext> extends
    AbstractDatabaseManager<DB> {

  private final long maximumNumberOfDatabases;
  private volatile boolean databaseManagerStarted;

  public MultiDatabaseManager(GlobalModule globalModule, AbstractEditionModule edition) {
    this(globalModule, edition, false);
  }

  MultiDatabaseManager(GlobalModule globalModule, AbstractEditionModule edition,
      boolean manageDatabasesOnStartAndStop) {
    super(globalModule, edition, manageDatabasesOnStartAndStop);
    this.maximumNumberOfDatabases = globalModule.getGlobalConfig()
        .get(EnterpriseEditionSettings.maxNumberOfDatabases);
  }

  public DB createDatabase(NamedDatabaseId namedDatabaseId) throws DatabaseManagementException {
    if (this.databaseMap.get(namedDatabaseId) != null) {
      throw new DatabaseExistsException(
          String.format("Database with name `%s` already exists.", namedDatabaseId.name()));
    } else if ((long) this.databaseMap.size() >= this.maximumNumberOfDatabases) {
      throw new DatabaseLimitReachedException(
          String.format(
              "Reached maximum number of active databases. Unable to create new database `%s`.",
              namedDatabaseId.name()));
    } else {
      DB databaseContext;
      try {
        this.log.info("Creating '%s' database.", namedDatabaseId.name());
        databaseContext = this.createDatabaseContext(namedDatabaseId);
      } catch (Exception n4) {
        throw new DatabaseManagementException(String
            .format("An error occurred! Unable to create new database `%s`.",
                namedDatabaseId.name()),
            n4);
      }

      this.databaseMap.put(namedDatabaseId, databaseContext);
      this.databaseIdRepository().cache(namedDatabaseId);
      return databaseContext;
    }
  }

  public Optional<DB> getDatabaseContext(NamedDatabaseId namedDatabaseId) {
    return Optional.ofNullable(this.databaseMap.get(namedDatabaseId));
  }

  public void dropDatabase(NamedDatabaseId namedDatabaseId) throws DatabaseNotFoundException {
    if ("system".equals(namedDatabaseId.name())) {
      throw new DatabaseManagementException("System database can't be dropped.");
    } else {
      this.forSingleDatabase(namedDatabaseId, this::dropDatabase);
    }
  }

  public void stopDatabase(NamedDatabaseId namedDatabaseId) throws DatabaseNotFoundException {
    this.forSingleDatabase(namedDatabaseId, this::stopDatabase);
  }

  public void startDatabase(NamedDatabaseId namedDatabaseId) throws DatabaseNotFoundException {
    this.forSingleDatabase(namedDatabaseId, this::startDatabase);
  }

  protected final void forSingleDatabase(NamedDatabaseId namedDatabaseId,
      BiConsumer<NamedDatabaseId, DB> consumer) {
    this.requireStarted(namedDatabaseId);
    DB ctx = this.databaseMap.get(namedDatabaseId);
    if (ctx == null) {
      throw new DatabaseNotFoundException(
          String.format("Database with name `%s` not found.", namedDatabaseId.name()));
    } else {
      consumer.accept(namedDatabaseId, ctx);
    }
  }

  public void start() throws Exception {
    if (!this.databaseManagerStarted) {
      this.databaseManagerStarted = true;
      super.start();
    }
  }

  public void stop() throws Exception {
    if (this.databaseManagerStarted) {
      super.stop();
      this.databaseManagerStarted = false;
    }
  }

  public final void shutdown() {
    this.databaseMap.clear();
  }

  protected void dropDatabase(NamedDatabaseId namedDatabaseId, DB context) {
    try {
      this.log.info("Drop '%s' database.", namedDatabaseId.name());
      Database database = context.database();
      database.drop();
      this.databaseIdRepository().invalidate(namedDatabaseId);
      this.databaseMap.remove(namedDatabaseId);
    } catch (Throwable n4) {
      throw new DatabaseManagementException(String
          .format("An error occurred! Unable to drop database with name `%s`.",
              namedDatabaseId.name()),
          n4);
    }
  }

  private void requireStarted(NamedDatabaseId namedDatabaseId) {
    if (!this.databaseManagerStarted) {
      throw new IllegalStateException(
          String.format("The database manager must be started in order to operate on database `%s`",
              namedDatabaseId.name()));
    }
  }
}
