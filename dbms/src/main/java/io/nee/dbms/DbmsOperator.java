/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public abstract class DbmsOperator {

  final Map<String, EnterpriseDatabaseState> desired = new ConcurrentHashMap();
  private OperatorConnector connector;

  final void connect(OperatorConnector connector) {
    Objects.requireNonNull(connector);
    this.connector = connector;
    connector.register(this);
  }

  protected Map<String, EnterpriseDatabaseState> desired0() {
    return this.desired;
  }

  final Map<String, EnterpriseDatabaseState> desired() {
    return Map.copyOf(this.desired0());
  }

  final ReconcilerResult trigger(ReconcilerRequest request) {
    return this.connector == null ? ReconcilerResult.EMPTY : this.connector.trigger(request);
  }
}
