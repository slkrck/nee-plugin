/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import io.nee.dbms.database.MultiDatabaseManager;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.dbms.DatabaseStateService;
import org.neo4j.dbms.OperatorState;
import org.neo4j.dbms.api.DatabaseManagementException;
import org.neo4j.dbms.database.DatabaseContext;
import org.neo4j.dbms.database.DatabaseStartAbortedException;
import org.neo4j.internal.helpers.Exceptions;
import org.neo4j.internal.helpers.ExponentialBackoffStrategy;
import org.neo4j.internal.helpers.TimeoutStrategy.Timeout;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.kernel.database.Database;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public class DbmsReconciler implements DatabaseStateService {

  protected final Map<String, EnterpriseDatabaseState> currentStates;
  private final ExponentialBackoffStrategy backoffStrategy;
  private final Executor executor;
  private final Map<String, CompletableFuture<ReconcilerStepResult>> reconcilerJobCache;
  private final Set<String> reconciling;
  private final MultiDatabaseManager<? extends DatabaseContext> databaseManager;
  private final BinaryOperator<EnterpriseDatabaseState> precedence;
  private final Log log;
  private final boolean canRetry;
  private final Transitions transitions;
  private final List<DatabaseStateChangedListener> listeners;

  DbmsReconciler(MultiDatabaseManager<? extends DatabaseContext> databaseManager, Config config,
      LogProvider logProvider, JobScheduler scheduler) {
    this.databaseManager = databaseManager;
    this.canRetry = config.get(GraphDatabaseSettings.reconciler_may_retry);
    this.backoffStrategy = new ExponentialBackoffStrategy(
        config.get(GraphDatabaseSettings.reconciler_minimum_backoff),
        config.get(GraphDatabaseSettings.reconciler_maximum_backoff));
    if (config.isExplicitlySet(GraphDatabaseSettings.reconciler_maximum_parallelism)) {
      int parallelism = config.get(GraphDatabaseSettings.reconciler_maximum_parallelism);
      parallelism = parallelism == 0 ? Runtime.getRuntime().availableProcessors() : parallelism;
      scheduler.setParallelism(Group.DATABASE_RECONCILER, parallelism);
    }

    this.executor = scheduler.executor(Group.DATABASE_RECONCILER);
    this.reconciling = new HashSet();
    this.currentStates = new ConcurrentHashMap();
    this.reconcilerJobCache = new ConcurrentHashMap();
    this.log = logProvider.getLog(this.getClass());
    this.precedence = EnterpriseOperatorState
        .minByOperatorState(EnterpriseDatabaseState::operatorState);
    this.transitions = this.prepareLifecycleTransitionSteps();
    this.listeners = new CopyOnWriteArrayList();
  }

  private static Map<String, EnterpriseDatabaseState> combineDesiredStates(
      Map<String, EnterpriseDatabaseState> combined,
      Map<String, EnterpriseDatabaseState> operator,
      BinaryOperator<EnterpriseDatabaseState> precedence) {
    return Stream.concat(combined.entrySet().stream(), operator.entrySet().stream()).collect(
        Collectors.toMap(Entry::getKey, Entry::getValue, precedence));
  }

  private static Map<String, EnterpriseDatabaseState> desiredStates(List<DbmsOperator> operators,
      BinaryOperator<EnterpriseDatabaseState> precedence) {
    return (Map) operators.stream().map(DbmsOperator::desired).reduce(new HashMap(), (l, r) ->
    {
      return combineDesiredStates(l, r, precedence);
    });
  }

  private static ReconcilerStepResult doTransitions(EnterpriseDatabaseState currentState,
      Stream<Transitions.Transition> steps,
      EnterpriseDatabaseState desiredState) {
    String oldThreadName = Thread.currentThread().getName();

    ReconcilerStepResult n4;
    try {
      Thread.currentThread().setName(oldThreadName + "-" + desiredState.databaseId().name());
      n4 = doTransitionStep(steps.iterator(),
          new ReconcilerStepResult(currentState, null, desiredState));
    } finally {
      Thread.currentThread().setName(oldThreadName);
    }

    return n4;
  }

  private static ReconcilerStepResult doTransitionStep(Iterator<Transitions.Transition> steps,
      ReconcilerStepResult result) {
    if (!steps.hasNext()) {
      return result;
    } else {
      try {
        EnterpriseDatabaseState nextState = steps.next().doTransition();
        return doTransitionStep(steps, result.withState(nextState));
      } catch (Throwable n3) {
        return result.withError(n3);
      }
    }
  }

  private static Optional<Throwable> shouldFailDatabaseWithCausePostSuccessfulReconcile(
      NamedDatabaseId namedDatabaseId,
      EnterpriseDatabaseState currentState, ReconcilerRequest request) {
    if (request.forceReconciliation()) {
      return Optional.empty();
    } else {
      return currentState != null && currentState.hasFailed() ? currentState.failure()
          : request.causeOfPanic(namedDatabaseId);
    }
  }

  private EnterpriseDatabaseState stateFor(NamedDatabaseId id) {
    return this.currentStates.getOrDefault(id.name(), EnterpriseDatabaseState.unknown(id));
  }

  public OperatorState stateOfDatabase(NamedDatabaseId namedDatabaseId) {
    return this.currentStates.getOrDefault(namedDatabaseId.name(),
        EnterpriseDatabaseState.unknown(namedDatabaseId)).operatorState();
  }

  public Optional<Throwable> causeOfFailure(NamedDatabaseId namedDatabaseId) {
    return this.currentStates.getOrDefault(namedDatabaseId.name(),
        EnterpriseDatabaseState.unknown(namedDatabaseId)).failure();
  }

  Collection<EnterpriseDatabaseState> statesSnapshot() {
    return Collections.unmodifiableCollection(this.currentStates.values());
  }

  public void registerListener(DatabaseStateChangedListener listener) {
    this.listeners.add(listener);
  }

  private void stateChanged(EnterpriseDatabaseState previousState,
      EnterpriseDatabaseState newState) {
    EnterpriseDatabaseState initialState = new EnterpriseDatabaseState(newState.databaseId(),
        EnterpriseOperatorState.INITIAL);
    if (previousState != null && !Objects
        .equals(previousState.databaseId(), newState.databaseId())) {
      EnterpriseDatabaseState droppedPrevious = new EnterpriseDatabaseState(
          previousState.databaseId(), EnterpriseOperatorState.DROPPED);
      this.listeners.forEach((listener) ->
      {
        listener.stateChange(previousState, droppedPrevious);
      });
      this.listeners.forEach((listener) ->
      {
        listener.stateChange(initialState, newState);
      });
    } else {
      this.listeners.forEach((listener) ->
      {
        listener.stateChange(previousState == null ? initialState : previousState, newState);
      });
    }
  }

  ReconcilerResult reconcile(List<DbmsOperator> operators, ReconcilerRequest request) {
    Stream<String> namesOfDbsToReconcile = operators.stream().flatMap((op) ->
    {
      return op.desired().keySet().stream();
    }).distinct();
    Map<String, CompletableFuture<ReconcilerStepResult>> reconciliation = namesOfDbsToReconcile
        .map((dbName) ->
        {
          return Pair.of(dbName,
              this.scheduleReconciliationJob(
                  dbName, request,
                  operators));
        }).collect(
            Collectors.toMap(Pair::first, Pair::other));
    return new ReconcilerResult(reconciliation);
  }

  protected EnterpriseDatabaseState getReconcilerEntryFor(NamedDatabaseId namedDatabaseId) {
    return this.currentStates
        .getOrDefault(namedDatabaseId.name(), EnterpriseDatabaseState.initial(namedDatabaseId));
  }

  private synchronized CompletableFuture<ReconcilerStepResult> scheduleReconciliationJob(
      String databaseName, ReconcilerRequest request,
      List<DbmsOperator> operators) {
    CompletableFuture<ReconcilerStepResult> cachedJob = this.reconcilerJobCache.get(databaseName);
    if (cachedJob != null && request.isSimple()) {
      return cachedJob;
    } else {
      CompletableFuture<Void> reconcilerJobHandle = new CompletableFuture();
      CompletableFuture<ReconcilerStepResult> job = reconcilerJobHandle.thenCompose((ignored) ->
      {
        return this.preReconcile(databaseName, operators, request);
      }).thenCompose((desiredState) ->
      {
        return this.doTransitions(databaseName,
            desiredState,
            request);
      }).whenComplete((result, throwable) ->
      {
        this.postReconcile(
            databaseName, request,
            result, throwable);
      });
      if (request.isSimple()) {
        this.reconcilerJobCache.put(databaseName, job);
      }

      reconcilerJobHandle.complete(null);
      return job;
    }
  }

  private CompletableFuture<EnterpriseDatabaseState> preReconcile(String databaseName,
      List<DbmsOperator> operators, ReconcilerRequest request) {
    return CompletableFuture.supplyAsync(() ->
    {
      try {
        this.log.debug("Attempting to acquire lock before reconciling state of database `%s`.",
            databaseName);
        this.acquireLockOn(databaseName);
        if (request.isSimple()) {
          this.reconcilerJobCache.remove(databaseName);
        }

        EnterpriseDatabaseState desiredState =
            desiredStates(operators, this.precedence).get(databaseName);
        if (desiredState == null) {
          throw new IllegalStateException(
              String.format(
                  "No operator desires a state for database %s any more. This is likely an error!",
                  databaseName));
        } else if (!Objects.equals(databaseName, desiredState.databaseId().name())) {
          throw new IllegalStateException(
              String.format(
                  "The supplied database name %s does not match that stored in its desired state %s!",
                  databaseName,
                  desiredState.databaseId().name()));
        } else {
          return desiredState;
        }
      } catch (InterruptedException n5) {
        Thread.currentThread().interrupt();
        throw new CompletionException(n5);
      }
    }, this.executor);
  }

  private CompletableFuture<ReconcilerStepResult> doTransitions(String databaseName,
      EnterpriseDatabaseState desiredState, ReconcilerRequest request) {
    EnterpriseDatabaseState currentState = this.getReconcilerEntryFor(desiredState.databaseId());
    ReconcilerStepResult initialResult = new ReconcilerStepResult(currentState, null, desiredState);
    if (currentState.equals(desiredState)) {
      return CompletableFuture.completedFuture(initialResult);
    } else {
      this.log
          .info("Database %s is requested to transition from %s to %s", databaseName, currentState,
              desiredState);
      if (currentState.hasFailed() && !request.forceReconciliation()) {
        String message =
            String.format(
                "Attempting to reconcile database %s to state '%s' but has previously failed. Manual force is required to retry.",
                databaseName, desiredState.operatorState().description());
        this.log.warn(message);
        Throwable previousError = currentState.failure().orElseThrow(IllegalStateException::new);
        return CompletableFuture.completedFuture(
            initialResult.withError(DatabaseManagementException.wrap(previousError)));
      } else {
        Timeout backoff = this.backoffStrategy.newTimeout();
        Stream<Transitions.Transition> steps = this
            .getLifecycleTransitionSteps(currentState, desiredState);
        NamedDatabaseId namedDatabaseId = desiredState.databaseId();
        return CompletableFuture.supplyAsync(() ->
        {
          return doTransitions(initialResult.state(), steps, desiredState);
        }, this.executor).thenCompose((result) ->
        {
          return this.handleResult(namedDatabaseId, desiredState, result,
              this.executor, backoff, 0);
        });
      }
    }
  }

  private CompletableFuture<ReconcilerStepResult> handleResult(NamedDatabaseId namedDatabaseId,
      EnterpriseDatabaseState desiredState,
      ReconcilerStepResult result, Executor executor, Timeout backoff, int retries) {
    boolean isFatalError = result.error() != null && this.isFatalError(result.error());
    if (result.error() != null && !isFatalError) {
      int attempt = retries + 1;
      this.log.warn("Retrying reconciliation of database %s to state '%s'. This is attempt %d.",
          namedDatabaseId.name(), desiredState.operatorState().description(), attempt);
      Stream<Transitions.Transition> remainingSteps = this
          .getLifecycleTransitionSteps(result.state(), desiredState);
      return CompletableFuture.supplyAsync(() ->
      {
        return doTransitions(result.state(), remainingSteps, desiredState);
      }, CompletableFuture
          .delayedExecutor(backoff.getAndIncrement(), TimeUnit.MILLISECONDS, executor))
          .thenCompose((retryResult) ->
          {
            return this.handleResult(namedDatabaseId, desiredState, retryResult, executor, backoff,
                attempt);
          });
    } else {
      return CompletableFuture.completedFuture(result);
    }
  }

  private void postReconcile(String databaseName, ReconcilerRequest request,
      ReconcilerStepResult result, Throwable throwable) {
    boolean n11 = false;

    try {
      n11 = true;
      this.currentStates.compute(databaseName, (name, previousState) ->
      {
        Optional<EnterpriseDatabaseState> failedState = this
            .handleReconciliationErrors(throwable, request, result, databaseName, previousState);
        EnterpriseDatabaseState nextState = failedState.orElseGet(() ->
        {
          return result.state();
        });
        this.stateChanged(previousState, nextState);
        return nextState;
      });
      n11 = false;
    } finally {
      if (n11) {
        this.releaseLockOn(databaseName);
        boolean errorExists = throwable != null || result.error() != null;
        String outcome = errorExists ? "failed" : "succeeded";
        this.log.debug("Released lock having %s to reconcile database `%s` to state %s.",
            outcome, databaseName, result.desiredState().operatorState().description());
      }
    }

    this.releaseLockOn(databaseName);
    boolean errorExists = throwable != null || result.error() != null;
    String outcome = errorExists ? "failed" : "succeeded";
    this.log.debug("Released lock having %s to reconcile database `%s` to state %s.",
        outcome, databaseName, result.desiredState().operatorState().description());
  }

  private Optional<EnterpriseDatabaseState> handleReconciliationErrors(Throwable throwable,
      ReconcilerRequest request, ReconcilerStepResult result,
      String databaseName, EnterpriseDatabaseState previousState) {
    String message;
    if (throwable != null) {
      message = String
          .format("Encountered unexpected error when attempting to reconcile database %s",
              databaseName);
      if (previousState == null) {
        this.log.error(message, throwable);
        return Optional.of(EnterpriseDatabaseState.failedUnknownId(throwable));
      } else {
        this.reportErrorAndPanicDatabase(previousState.databaseId(), message, throwable);
        return Optional.of(previousState.failed(throwable));
      }
    } else if (result.error() != null && Exceptions.contains(result.error(), (e) ->
    {
      return e instanceof DatabaseStartAbortedException;
    })) {
      message = String.format(
          "Attempt to reconcile database %s from %s to %s was aborted, likely due to %s being stopped or dropped meanwhile.",
          databaseName, result.state(), result.desiredState().operatorState().description(),
          databaseName);
      this.log.warn(message);
      return Optional.of(result.state());
    } else if (result.error() != null) {
      message = String.format(
          "Encountered error when attempting to reconcile database %s from state '%s' to state '%s'",
          databaseName, result.state(),
          result.desiredState().operatorState().description());
      this.reportErrorAndPanicDatabase(result.state().databaseId(), message, result.error());
      return Optional.of(result.state().failed(result.error()));
    } else {
      EnterpriseDatabaseState nextState = result.state();
      Optional<Throwable> failure = shouldFailDatabaseWithCausePostSuccessfulReconcile(
          nextState.databaseId(), previousState, request);
      Objects.requireNonNull(nextState);
      return failure.map(nextState::failed);
    }
  }

  private void reportErrorAndPanicDatabase(NamedDatabaseId namedDatabaseId, String message,
      Throwable error) {
    this.log.error(message, error);
    IllegalStateException panicCause = new IllegalStateException(message, error);
    this.panicDatabase(namedDatabaseId, panicCause);
  }

  private void releaseLockOn(String databaseName) {
    synchronized (this.reconciling) {
      this.reconciling.remove(databaseName);
      this.reconciling.notifyAll();
    }
  }

  private void acquireLockOn(String databaseName) throws InterruptedException {
    synchronized (this.reconciling) {
      while (this.reconciling.contains(databaseName)) {
        this.reconciling.wait();
      }

      this.reconciling.add(databaseName);
    }
  }

  private boolean isFatalError(Throwable t) {
    return !this.canRetry || t instanceof Error || t instanceof DatabaseStartAbortedException;
  }

  protected Transitions prepareLifecycleTransitionSteps() {
    return Transitions.builder().from(EnterpriseOperatorState.INITIAL)
        .to(EnterpriseOperatorState.DROPPED).doNothing().from(
            EnterpriseOperatorState.INITIAL).to(EnterpriseOperatorState.STOPPED)
        .doTransitions(this::create).from(
            EnterpriseOperatorState.INITIAL).to(EnterpriseOperatorState.STARTED)
        .doTransitions(this::create, this::start).from(
            EnterpriseOperatorState.STOPPED).to(EnterpriseOperatorState.STARTED)
        .doTransitions(this::start).from(EnterpriseOperatorState.STARTED).to(
            EnterpriseOperatorState.STOPPED).doTransitions(this::stop)
        .from(EnterpriseOperatorState.STOPPED).to(
            EnterpriseOperatorState.DROPPED).doTransitions(this::drop)
        .from(EnterpriseOperatorState.STARTED).to(
            EnterpriseOperatorState.DROPPED)
        .doTransitions(this::prepareDrop, this::stop, this::drop).build();
  }

  private Stream<Transitions.Transition> getLifecycleTransitionSteps(
      EnterpriseDatabaseState currentState, EnterpriseDatabaseState desiredState) {
    return this.transitions.fromCurrent(currentState).toDesired(desiredState);
  }

  protected void panicDatabase(NamedDatabaseId namedDatabaseId, Throwable error) {
    this.databaseManager.getDatabaseContext(namedDatabaseId).map((ctx) ->
    {
      return ctx.database().getDatabaseHealth();
    }).ifPresent((health) ->
    {
      health.panic(error);
    });
  }

  protected final EnterpriseDatabaseState stop(NamedDatabaseId namedDatabaseId) {
    this.databaseManager.stopDatabase(namedDatabaseId);
    return new EnterpriseDatabaseState(namedDatabaseId, EnterpriseOperatorState.STOPPED);
  }

  private EnterpriseDatabaseState prepareDrop(NamedDatabaseId namedDatabaseId) {
    this.databaseManager.getDatabaseContext(namedDatabaseId).map(DatabaseContext::database)
        .ifPresent(Database::prepareToDrop);
    return new EnterpriseDatabaseState(namedDatabaseId, EnterpriseOperatorState.STARTED);
  }

  protected final EnterpriseDatabaseState drop(NamedDatabaseId namedDatabaseId) {
    this.databaseManager.dropDatabase(namedDatabaseId);
    return new EnterpriseDatabaseState(namedDatabaseId, EnterpriseOperatorState.DROPPED);
  }

  protected final EnterpriseDatabaseState start(NamedDatabaseId namedDatabaseId) {
    this.databaseManager.startDatabase(namedDatabaseId);
    return new EnterpriseDatabaseState(namedDatabaseId, EnterpriseOperatorState.STARTED);
  }

  protected final EnterpriseDatabaseState create(NamedDatabaseId namedDatabaseId) {
    this.databaseManager.createDatabase(namedDatabaseId);
    return new EnterpriseDatabaseState(namedDatabaseId, EnterpriseOperatorState.STOPPED);
  }
}
