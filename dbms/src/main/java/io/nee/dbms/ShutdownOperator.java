/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import java.util.Map;
import java.util.stream.Collectors;
import org.neo4j.dbms.database.DatabaseManager;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.NamedDatabaseId;

class ShutdownOperator extends DbmsOperator {

  private final DatabaseManager<?> databaseManager;

  ShutdownOperator(DatabaseManager<?> databaseManager) {
    this.databaseManager = databaseManager;
  }

  void stopAll() {
    this.desired.clear();
    Map<String, EnterpriseDatabaseState> desireAllStopped = this.databaseManager
        .registeredDatabases().keySet().stream().filter((e) ->
        {
          return !e.equals(
              DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID);
        }).collect(
            Collectors.toMap(NamedDatabaseId::name, this::stoppedState));
    this.desired.putAll(desireAllStopped);
    this.trigger(ReconcilerRequest.force()).awaitAll();
    this.desired.put(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID.name(),
        this.stoppedState(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID));
    this.trigger(ReconcilerRequest.force()).await(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID);
  }

  private EnterpriseDatabaseState stoppedState(NamedDatabaseId id) {
    return new EnterpriseDatabaseState(id, EnterpriseOperatorState.STOPPED);
  }
}
