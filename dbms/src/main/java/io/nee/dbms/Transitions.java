/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.neo4j.internal.helpers.collection.Pair;
import org.neo4j.kernel.database.NamedDatabaseId;

final class Transitions {

  private final Map<Pair<EnterpriseOperatorState, EnterpriseOperatorState>, Transitions.TransitionFunction[]> transitionsTable;

  private Transitions(
      Map<Pair<EnterpriseOperatorState, EnterpriseOperatorState>, Transitions.TransitionFunction[]> transitionsTable) {
    this.transitionsTable = transitionsTable;
  }

  public static Transitions.TransitionsBuilder builder() {
    return new Transitions.TransitionsBuilder();
  }

  Transitions.TransitionLookup fromCurrent(EnterpriseDatabaseState current) {
    return new Transitions.TransitionLookup(current);
  }

  public Transitions extendWith(Transitions other) {
    HashMap<Pair<EnterpriseOperatorState, EnterpriseOperatorState>, Transitions.TransitionFunction[]> combined = new HashMap(
        this.transitionsTable);
    combined.putAll(other.transitionsTable);
    return new Transitions(combined);
  }

  private Stream<Transitions.Transition> lookup(Transitions.TransitionLookup lookup) {
    if (!Objects.equals(lookup.current.databaseId(), lookup.desired.databaseId())) {
      Stream<Transitions.Transition> dropCurrent =
          this.prepareTransitionFunctions(lookup.current.operatorState(),
              EnterpriseOperatorState.DROPPED, lookup.current.databaseId());
      Stream<Transitions.Transition> createNext =
          this.prepareTransitionFunctions(EnterpriseOperatorState.INITIAL,
              lookup.desired.operatorState(), lookup.desired.databaseId());
      return Stream.concat(dropCurrent, createNext);
    } else {
      return this.prepareTransitionFunctions(lookup.current.operatorState(),
          lookup.desired.operatorState(), lookup.current.databaseId());
    }
  }

  private Stream<Transitions.Transition> prepareTransitionFunctions(EnterpriseOperatorState current,
      EnterpriseOperatorState desired,
      NamedDatabaseId namedDatabaseId) {
    if (current == desired) {
      return Stream.empty();
    } else if (current == EnterpriseOperatorState.DROPPED) {
      throw new IllegalArgumentException(
          String
              .format("Trying to set database %s to %s, but is 'DROPPED', which is a final state.",
                  namedDatabaseId, desired));
    } else {
      Transitions.TransitionFunction[] transitions = this.transitionsTable
          .get(Pair.of(current, desired));
      if (transitions == null) {
        throw new IllegalArgumentException(
            String.format("%s -> %s is an unsupported state transition", current, desired));
      } else {
        return Arrays.stream(transitions).map((tn) ->
        {
          return tn.prepare(namedDatabaseId);
        });
      }
    }
  }

  @FunctionalInterface
  public interface Transition extends Supplier<EnterpriseDatabaseState> {

    EnterpriseDatabaseState doTransition();

    default EnterpriseDatabaseState get() {
      return this.doTransition();
    }
  }

  @FunctionalInterface
  public interface TransitionFunction {

    EnterpriseDatabaseState forTransition(NamedDatabaseId n1);

    default Transitions.Transition prepare(NamedDatabaseId namedDatabaseId) {
      return () ->
      {
        return this.forTransition(namedDatabaseId);
      };
    }
  }

  public interface BuildOrContinue {

    Transitions.NeedsTo from(EnterpriseOperatorState n1);

    Transitions build();
  }

  public interface NeedsDo {

    Transitions.BuildOrContinue doTransitions(Transitions.TransitionFunction... n1);

    Transitions.BuildOrContinue doNothing();
  }

  public interface NeedsTo {

    Transitions.NeedsDo to(EnterpriseOperatorState n1);
  }

  public interface NeedsFrom {

    Transitions.NeedsTo from(EnterpriseOperatorState n1);
  }

  public interface NeedsDesired {

    Stream<Transitions.Transition> toDesired(EnterpriseDatabaseState n1);
  }

  public static class TransitionsBuilder implements Transitions.NeedsFrom, Transitions.NeedsTo,
      Transitions.NeedsDo, Transitions.BuildOrContinue {

    private final Map<Pair<EnterpriseOperatorState, EnterpriseOperatorState>, Transitions.TransitionFunction[]> transitionsTable = new HashMap();
    private EnterpriseOperatorState from;
    private EnterpriseOperatorState to;
    private Transitions.TransitionFunction[] transitions;

    private TransitionsBuilder() {
    }

    public Transitions.NeedsTo from(EnterpriseOperatorState from) {
      this.storePreviousEntry();
      this.from = from;
      return this;
    }

    public Transitions.NeedsDo to(EnterpriseOperatorState to) {
      this.to = to;
      return this;
    }

    public Transitions.BuildOrContinue doTransitions(
        Transitions.TransitionFunction... transitions) {
      this.transitions = transitions;
      return this;
    }

    public Transitions.BuildOrContinue doNothing() {
      this.transitions = new Transitions.TransitionFunction[]{(id) ->
      {
        return new EnterpriseDatabaseState(id, this.to);
      }};
      return this;
    }

    public Transitions build() {
      this.storePreviousEntry();
      return new Transitions(this.transitionsTable);
    }

    private void storePreviousEntry() {
      if (this.from != null && this.to != null && this.transitions != null) {
        this.transitionsTable.put(Pair.of(this.from, this.to), this.transitions);
      } else if (this.from != null || this.to != null || this.transitions != null) {
        throw new IllegalStateException("TransitionFunction builder is only partially complete");
      }
    }
  }

  class TransitionLookup implements Transitions.NeedsDesired {

    private final EnterpriseDatabaseState current;
    private EnterpriseDatabaseState desired;

    private TransitionLookup(EnterpriseDatabaseState current) {
      Objects.requireNonNull(current, "You must specify a current state for a transition!");
      this.current = current;
    }

    public Stream<Transitions.Transition> toDesired(EnterpriseDatabaseState desired) {
      Objects.requireNonNull(desired, "You must specify a desired state for a transition!");
      this.desired = desired;
      return Transitions.this.lookup(this);
    }
  }
}
