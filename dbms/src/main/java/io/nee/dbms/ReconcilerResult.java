/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.neo4j.dbms.api.DatabaseManagementException;
import org.neo4j.internal.helpers.Exceptions;
import org.neo4j.kernel.database.NamedDatabaseId;

public final class ReconcilerResult {

  public static final ReconcilerResult EMPTY = new ReconcilerResult(Collections.emptyMap());
  private final Map<String, CompletableFuture<ReconcilerStepResult>> reconciliationFutures;
  private final CompletableFuture<Void> combinedFuture;

  ReconcilerResult(Map<String, CompletableFuture<ReconcilerStepResult>> reconciliationFutures) {
    this.reconciliationFutures = reconciliationFutures;
    this.combinedFuture = combineFutures(reconciliationFutures);
  }

  private static CompletableFuture<Void> combineFutures(
      Map<String, CompletableFuture<ReconcilerStepResult>> reconciliationFutures) {
    CompletableFuture<?>[] allFutures = (CompletableFuture[]) reconciliationFutures.values()
        .toArray((n) ->
        {
          return new CompletableFuture[n];
        });
    return CompletableFuture.allOf(allFutures);
  }

  public void join(NamedDatabaseId namedDatabaseId) throws DatabaseManagementException {
    CompletableFuture<ReconcilerStepResult> future = this.reconciliationFutures
        .get(namedDatabaseId.name());
    if (future != null) {
      ReconcilerStepResult result = future.join();
      if (result.error() != null) {
        throw new DatabaseManagementException(
            "A triggered DbmsReconciler job failed with the following cause", result.error());
      }
    }
  }

  public void join(Collection<NamedDatabaseId> namedDatabaseIds)
      throws DatabaseManagementException {
    this.collectAndThrowErrors(this.getFutures(namedDatabaseIds));
  }

  public void joinAll() throws DatabaseManagementException {
    this.collectAndThrowErrors(this.reconciliationFutures.values());
  }

  private void collectAndThrowErrors(Collection<CompletableFuture<ReconcilerStepResult>> futures)
      throws DatabaseManagementException {
    Map<NamedDatabaseId, Throwable> errors = futures.stream().map(CompletableFuture::join)
        .filter((result) ->
        {
          return result.error() != null;
        }).collect(Collectors.toMap((result) ->
            {
              return result
                  .desiredState()
                  .databaseId();
            },
            ReconcilerStepResult::error));
    if (!errors.isEmpty()) {
      String databases = errors.keySet().stream().map(NamedDatabaseId::name)
          .collect(Collectors.joining(",", "[", "]"));
      Throwable cause = errors.values().stream().reduce(null, Exceptions::chain);
      throw new DatabaseManagementException(
          String.format("Reconciliation failed for databases %s", databases), cause);
    }
  }

  public void await(NamedDatabaseId namedDatabaseId) {
    CompletableFuture<ReconcilerStepResult> future = this.reconciliationFutures
        .get(namedDatabaseId.name());
    if (future != null) {
      future.join();
    }
  }

  public void await(Collection<NamedDatabaseId> namedDatabaseIds) {
    CompletableFuture<?>[] futuresArray = (CompletableFuture[]) this.getFutures(namedDatabaseIds)
        .toArray((n) ->
        {
          return new CompletableFuture[n];
        });
    CompletableFuture.allOf(futuresArray).join();
  }

  void awaitAll() {
    this.combinedFuture.join();
  }

  void whenComplete(Runnable action) {
    this.combinedFuture.whenComplete((ignore, error) ->
    {
      action.run();
    });
  }

  private List<CompletableFuture<ReconcilerStepResult>> getFutures(
      Collection<NamedDatabaseId> namedDatabaseIds) {
    return (List) namedDatabaseIds.stream().map((id) ->
    {
      return (CompletableFuture) this.reconciliationFutures.get(id.name());
    }).flatMap(Stream::ofNullable).collect(Collectors.toList());
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ReconcilerResult that = (ReconcilerResult) o;
      return Objects.equals(this.reconciliationFutures, that.reconciliationFutures);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.reconciliationFutures);
  }

  public String toString() {
    return "ReconcilerResult{reconciliationFutures=" + this.reconciliationFutures + "}";
  }
}
