/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import io.nee.dbms.database.DatabaseOperationCountMonitor;
import io.nee.dbms.database.DatabaseOperationCountMonitorListener;
import io.nee.dbms.database.MultiDatabaseManager;
import java.util.stream.Stream;
import org.neo4j.bolt.txtracking.ReconciledTransactionTracker;
import org.neo4j.common.DependencyResolver;
import org.neo4j.dbms.api.DatabaseManagementException;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.event.TransactionData;
import org.neo4j.graphdb.event.TransactionEventListenerAdapter;
import org.neo4j.graphdb.factory.module.GlobalModule;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.extension.GlobalExtensions;
import org.neo4j.kernel.internal.GraphDatabaseAPI;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.kernel.lifecycle.LifecycleListener;
import org.neo4j.kernel.lifecycle.LifecycleStatus;
import org.neo4j.logging.LogProvider;
import org.neo4j.storageengine.api.TransactionIdStore;

public class StandaloneDbmsReconcilerModule extends LifecycleAdapter {

  protected final DatabaseIdRepository databaseIdRepository;
  private final GlobalModule globalModule;
  private final MultiDatabaseManager<?> databaseManager;
  private final LocalDbmsOperator localOperator;
  private final SystemGraphDbmsOperator systemOperator;
  private final ShutdownOperator shutdownOperator;
  private final ReconciledTransactionTracker reconciledTxTracker;
  private final DbmsReconciler reconciler;

  public StandaloneDbmsReconcilerModule(GlobalModule globalModule,
      MultiDatabaseManager<?> databaseManager, ReconciledTransactionTracker reconciledTxTracker,
      EnterpriseSystemGraphDbmsModel dbmsModel) {
    this(globalModule, databaseManager, reconciledTxTracker,
        createReconciler(globalModule, databaseManager), dbmsModel);
  }

  protected StandaloneDbmsReconcilerModule(GlobalModule globalModule,
      MultiDatabaseManager<?> databaseManager,
      ReconciledTransactionTracker reconciledTxTracker, DbmsReconciler reconciler,
      EnterpriseSystemGraphDbmsModel dbmsModel) {
    LogProvider internalLogProvider = globalModule.getLogService().getInternalLogProvider();
    this.globalModule = globalModule;
    this.databaseManager = databaseManager;
    this.databaseIdRepository = databaseManager.databaseIdRepository();
    this.localOperator = new LocalDbmsOperator(this.databaseIdRepository);
    this.reconciledTxTracker = reconciledTxTracker;
    this.systemOperator = new SystemGraphDbmsOperator(dbmsModel, reconciledTxTracker,
        internalLogProvider);
    this.shutdownOperator = new ShutdownOperator(databaseManager);
    this.reconciler = reconciler;
    DatabaseOperationCountMonitorListener databaseCountMonitor = new DatabaseOperationCountMonitorListener(
        globalModule.getGlobalMonitors().newMonitor(DatabaseOperationCountMonitor.class,
            new String[]{this.getClass().getName()}));
    reconciler.registerListener(databaseCountMonitor);
    globalModule.getGlobalLife()
        .addLifecycleListener(this.createReconcilerMonitorResetListener(databaseCountMonitor));
    globalModule.getGlobalDependencies().satisfyDependency(reconciler);
    globalModule.getGlobalDependencies()
        .satisfyDependencies(this.localOperator, this.systemOperator);
  }

  private static DbmsReconciler createReconciler(GlobalModule globalModule,
      MultiDatabaseManager<?> databaseManager) {
    return new DbmsReconciler(databaseManager, globalModule.getGlobalConfig(),
        globalModule.getLogService().getInternalLogProvider(),
        globalModule.getJobScheduler());
  }

  public void start() throws Exception {
    this.registerWithListenerService(this.globalModule, this.systemOperator);
    OperatorConnector connector = new OperatorConnector(this.reconciler);
    this.operators().forEach((op) ->
    {
      op.connect(connector);
    });
    this.startInitialDatabases();
  }

  private void startInitialDatabases() throws DatabaseManagementException {
    this.systemOperator.trigger(ReconcilerRequest.simple())
        .join(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID);
    GraphDatabaseAPI systemDatabase = this.getSystemDatabase(this.databaseManager);
    long lastClosedTxId = this.getLastClosedTransactionId(systemDatabase);
    this.systemOperator.updateDesiredStates();
    this.systemOperator.trigger(ReconcilerRequest.simple()).awaitAll();
    this.reconciledTxTracker.enable(lastClosedTxId);
  }

  private void stopAllDatabases() {
    this.shutdownOperator.stopAll();
  }

  public void stop() {
    this.stopAllDatabases();
  }

  public DbmsReconciler reconciler() {
    return this.reconciler;
  }

  protected Stream<DbmsOperator> operators() {
    return Stream.of(this.localOperator, this.systemOperator, this.shutdownOperator);
  }

  protected void registerWithListenerService(GlobalModule globalModule,
      final SystemGraphDbmsOperator systemOperator) {
    globalModule.getTransactionEventListeners()
        .registerTransactionEventListener("system", new TransactionEventListenerAdapter() {
          public void afterCommit(TransactionData txData, Object state,
              GraphDatabaseService systemDatabase) {
            systemOperator.transactionCommitted(txData.getTransactionId(), txData);
          }
        });
  }

  private GraphDatabaseAPI getSystemDatabase(MultiDatabaseManager<?> databaseManager) {
    return databaseManager.getDatabaseContext(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID)
        .orElseThrow().databaseFacade();
  }

  private long getLastClosedTransactionId(GraphDatabaseAPI db) {
    DependencyResolver resolver = db.getDependencyResolver();
    TransactionIdStore txIdStore = resolver.resolveDependency(TransactionIdStore.class);
    return txIdStore.getLastClosedTransactionId();
  }

  private LifecycleListener createReconcilerMonitorResetListener(
      DatabaseOperationCountMonitorListener databaseOperationCountMonitorListener) {
    return (instance, from, to) ->
    {
      if (instance instanceof GlobalExtensions && to.equals(LifecycleStatus.STARTED)) {
        databaseOperationCountMonitorListener.reset(this.reconciler.statesSnapshot());
      }
    };
  }
}
