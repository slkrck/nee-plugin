/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import org.eclipse.collections.api.multimap.set.MutableSetMultimap;
import org.eclipse.collections.impl.factory.Multimaps;
import org.neo4j.dbms.OperatorState;
import org.neo4j.kernel.availability.AvailabilityGuard;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.DatabaseStartupController;
import org.neo4j.kernel.database.NamedDatabaseId;

public class DatabaseStartAborter implements DatabaseStartupController {

  private final Duration ttl;
  private final Map<NamedDatabaseId, DatabaseStartAborter.CachedDesiredState> cachedDesiredStates = new ConcurrentHashMap();
  private final MutableSetMultimap<NamedDatabaseId, DatabaseStartAborter.PreventReason> abortPreventionSets;
  private final AvailabilityGuard globalAvailabilityGuard;
  private final EnterpriseSystemGraphDbmsModel dbmsModel;
  private final Clock clock;

  public DatabaseStartAborter(AvailabilityGuard globalAvailabilityGuard,
      EnterpriseSystemGraphDbmsModel dbmsModel, Clock clock, Duration ttl) {
    this.abortPreventionSets = Multimaps.mutable.set.empty();//.asSynchronized();
    this.globalAvailabilityGuard = globalAvailabilityGuard;
    this.dbmsModel = dbmsModel;
    this.clock = clock;
    this.ttl = ttl;
  }

  public void setAbortable(NamedDatabaseId databaseId, DatabaseStartAborter.PreventReason reason,
      boolean abortable) {
    if (abortable) {
      this.abortPreventionSets.remove(databaseId, reason);
    } else {
      this.abortPreventionSets.put(databaseId, reason);
    }
  }

  private boolean isAbortable(NamedDatabaseId databaseId) {
    return !this.abortPreventionSets.containsKey(databaseId);
  }

  public boolean shouldAbort(NamedDatabaseId namedDatabaseId) {
    if (!this.isAbortable(namedDatabaseId)) {
      return false;
    } else if (this.globalAvailabilityGuard.isShutdown()) {
      return true;
    } else if (Objects.equals(namedDatabaseId, DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID)) {
      return false;
    } else {
      DatabaseStartAborter.CachedDesiredState desiredState =
          this.cachedDesiredStates.compute(namedDatabaseId, (id, cachedState) ->
          {
            return cachedState != null && !cachedState.isTimeToDie() ? cachedState
                : this.getFreshDesiredState(namedDatabaseId);
          });
      return desiredState.state() == EnterpriseOperatorState.STOPPED
          || desiredState.state() == EnterpriseOperatorState.DROPPED;
    }
  }

  public void started(NamedDatabaseId namedDatabaseId) {
    this.cachedDesiredStates.remove(namedDatabaseId);
  }

  private DatabaseStartAborter.CachedDesiredState getFreshDesiredState(
      NamedDatabaseId namedDatabaseId) {
    String message = String
        .format("Failed to check if starting %s should abort as it doesn't exist in the system db!",
            namedDatabaseId);
    OperatorState state = this.dbmsModel.getStatus(namedDatabaseId).orElseThrow(() ->
    {
      return new IllegalStateException(message);
    });
    return new DatabaseStartAborter.CachedDesiredState(this.clock.instant(), state);
  }

  public enum PreventReason {
    STORE_COPY
  }

  private class CachedDesiredState {

    private final OperatorState state;
    private final Instant createdAt;

    private CachedDesiredState(Instant createdAt, OperatorState state) {
      this.state = state;
      this.createdAt = createdAt;
    }

    OperatorState state() {
      return this.state;
    }

    boolean isTimeToDie() {
      Duration elapsed = Duration
          .between(this.createdAt, DatabaseStartAborter.this.clock.instant());
      return elapsed.compareTo(DatabaseStartAborter.this.ttl) >= 0;
    }
  }
}
