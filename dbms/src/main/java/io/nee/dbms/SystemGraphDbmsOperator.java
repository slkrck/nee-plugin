/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import org.neo4j.bolt.txtracking.ReconciledTransactionTracker;
import org.neo4j.graphdb.event.TransactionData;
import org.neo4j.kernel.database.DatabaseIdRepository;
import org.neo4j.kernel.database.NamedDatabaseId;
import org.neo4j.logging.Log;
import org.neo4j.logging.LogProvider;

class SystemGraphDbmsOperator extends DbmsOperator {

  private final EnterpriseSystemGraphDbmsModel dbmsModel;
  private final ReconciledTransactionTracker reconciledTxTracker;
  private final Log log;

  SystemGraphDbmsOperator(EnterpriseSystemGraphDbmsModel dbmsModel,
      ReconciledTransactionTracker reconciledTxTracker, LogProvider logProvider) {
    this.dbmsModel = dbmsModel;
    this.reconciledTxTracker = reconciledTxTracker;
    this.desired.put(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID.name(),
        new EnterpriseDatabaseState(DatabaseIdRepository.NAMED_SYSTEM_DATABASE_ID,
            EnterpriseOperatorState.STARTED));
    this.log = logProvider.getLog(this.getClass());
  }

  void transactionCommitted(long txId, TransactionData transactionData) {
    this.reconcile(txId, transactionData, false);
  }

  void storeReplaced(long txId) {
    this.reconcile(txId, null, true);
  }

  private void reconcile(long txId, TransactionData transactionData, boolean asPartOfStoreCopy) {
    Collection<NamedDatabaseId> databasesToAwait = this.extractUpdatedDatabases(transactionData);
    this.updateDesiredStates();
    if (asPartOfStoreCopy) {
      this.reconciledTxTracker.disable();
    }

    ReconcilerResult reconcilerResult = this.trigger(ReconcilerRequest.simple());
    reconcilerResult.whenComplete(() ->
    {
      this.offerReconciledTransactionId(txId, asPartOfStoreCopy);
    });
    reconcilerResult.await(databasesToAwait);
  }

  synchronized void updateDesiredStates() {
    Map<String, EnterpriseDatabaseState> systemStates = this.dbmsModel.getDatabaseStates();
    Map n10001 = this.desired;
    Objects.requireNonNull(n10001);
    systemStates.forEach(n10001::put);
  }

  private Collection<NamedDatabaseId> extractUpdatedDatabases(TransactionData transactionData) {
    return (Collection) (transactionData == null ? Collections.emptySet()
        : this.dbmsModel.updatedDatabases(transactionData));
  }

  private void offerReconciledTransactionId(long txId, boolean asPartOfStoreCopy) {
    try {
      if (asPartOfStoreCopy) {
        this.reconciledTxTracker.enable(txId);
      } else {
        this.reconciledTxTracker.offerReconciledTransactionId(txId);
      }
    } catch (Throwable n5) {
      this.log.error("Failed to update the last reconciled transaction ID", n5);
    }
  }
}
