/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import java.util.Objects;
import java.util.Optional;
import org.neo4j.dbms.DatabaseState;
import org.neo4j.kernel.database.NamedDatabaseId;

public class EnterpriseDatabaseState implements DatabaseState {

  private final NamedDatabaseId namedDatabaseId;
  private final EnterpriseOperatorState operationalState;
  private final Throwable failure;

  public EnterpriseDatabaseState(NamedDatabaseId namedDatabaseId,
      EnterpriseOperatorState operationalState) {
    this(namedDatabaseId, operationalState, null);
  }

  private EnterpriseDatabaseState(NamedDatabaseId namedDatabaseId,
      EnterpriseOperatorState operationalState, Throwable failure) {
    this.namedDatabaseId = namedDatabaseId;
    this.operationalState = operationalState;
    this.failure = failure;
  }

  public static EnterpriseDatabaseState initial(NamedDatabaseId id) {
    return new EnterpriseDatabaseState(id, EnterpriseOperatorState.INITIAL, null);
  }

  public static EnterpriseDatabaseState unknown(NamedDatabaseId id) {
    return new EnterpriseDatabaseState(id, EnterpriseOperatorState.UNKNOWN, null);
  }

  static EnterpriseDatabaseState failedUnknownId(Throwable failure) {
    return new EnterpriseDatabaseState(null, EnterpriseOperatorState.UNKNOWN, failure);
  }

  public NamedDatabaseId databaseId() {
    return this.namedDatabaseId;
  }

  public EnterpriseOperatorState operatorState() {
    return this.operationalState;
  }

  public EnterpriseDatabaseState healthy() {
    return new EnterpriseDatabaseState(this.namedDatabaseId, this.operationalState, null);
  }

  public EnterpriseDatabaseState failed(Throwable failure) {
    return new EnterpriseDatabaseState(this.namedDatabaseId, this.operationalState, failure);
  }

  public boolean hasFailed() {
    return this.failure != null;
  }

  public Optional<Throwable> failure() {
    return Optional.ofNullable(this.failure);
  }

  public String toString() {
    NamedDatabaseId n10000 = this.namedDatabaseId;
    return "EnterpriseDatabaseState{databaseId=" + n10000 + ", operatorState="
        + this.operationalState + ", failed=" + this.hasFailed() + "}";
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      EnterpriseDatabaseState that = (EnterpriseDatabaseState) o;
      return this.hasFailed() == that.hasFailed() && Objects
          .equals(this.namedDatabaseId, that.namedDatabaseId) &&
          this.operationalState == that.operationalState;
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.namedDatabaseId, this.operationalState, this.hasFailed());
  }
}
