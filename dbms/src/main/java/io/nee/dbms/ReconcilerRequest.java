/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.dbms;

import java.util.Objects;
import java.util.Optional;
import org.neo4j.kernel.database.NamedDatabaseId;

public class ReconcilerRequest {

  private static final ReconcilerRequest SIMPLE = new ReconcilerRequest(false, null, null);
  private static final ReconcilerRequest FORCE = new ReconcilerRequest(true, null, null);
  private final boolean forceReconciliation;
  private final NamedDatabaseId panickedNamedDatabaseId;
  private final Throwable causeOfPanic;

  private ReconcilerRequest(boolean forceReconciliation, NamedDatabaseId panickedNamedDatabaseId,
      Throwable causeOfPanic) {
    this.forceReconciliation = forceReconciliation;
    this.panickedNamedDatabaseId = panickedNamedDatabaseId;
    this.causeOfPanic = causeOfPanic;
  }

  public static ReconcilerRequest simple() {
    return SIMPLE;
  }

  public static ReconcilerRequest force() {
    return FORCE;
  }

  public static ReconcilerRequest forPanickedDatabase(NamedDatabaseId namedDatabaseId,
      Throwable causeOfPanic) {
    return new ReconcilerRequest(false, namedDatabaseId, causeOfPanic);
  }

  boolean forceReconciliation() {
    return this.forceReconciliation;
  }

  Optional<Throwable> causeOfPanic(NamedDatabaseId namedDatabaseId) {
    boolean thisDatabaseHasPanicked =
        this.panickedNamedDatabaseId != null && this.panickedNamedDatabaseId
            .equals(namedDatabaseId);
    return thisDatabaseHasPanicked ? Optional.of(this.causeOfPanic) : Optional.empty();
  }

  boolean isSimple() {
    return this.causeOfPanic == null && !this.forceReconciliation;
  }

  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o != null && this.getClass() == o.getClass()) {
      ReconcilerRequest that = (ReconcilerRequest) o;
      return this.forceReconciliation == that.forceReconciliation && Objects
          .equals(this.panickedNamedDatabaseId, that.panickedNamedDatabaseId) &&
          Objects.equals(this.causeOfPanic, that.causeOfPanic);
    } else {
      return false;
    }
  }

  public int hashCode() {
    return Objects.hash(this.forceReconciliation, this.panickedNamedDatabaseId, this.causeOfPanic);
  }

  public String toString() {
    return "ReconcilerRequest{forceReconciliation=" + this.forceReconciliation
        + ", panickedDatabaseId=" + this.panickedNamedDatabaseId +
        ", causeOfPanic=" + this.causeOfPanic + "}";
  }
}
