/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.output;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.jmx.JmxReporter;
import com.codahale.metrics.jmx.ObjectNameFactory;
import io.nee.kernel.impl.enterprise.configuration.MetricsSettings;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.internal.helpers.HostnamePort;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.extension.context.ExtensionContext;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.Log;
import org.neo4j.scheduler.JobScheduler;

public class EventReporterBuilder {

  private static final String METRICS_JMX_BEAN_SUFFIX = ".metrics";
  private final Config config;
  private final MetricRegistry registry;
  private final Log logger;
  private final ExtensionContext extensionContext;
  private final LifeSupport life;
  private final ConnectorPortRegister portRegister;
  private final FileSystemAbstraction fileSystem;
  private final JobScheduler scheduler;

  public EventReporterBuilder(Config config, MetricRegistry registry, Log logger,
      ExtensionContext extensionContext, LifeSupport life,
      FileSystemAbstraction fileSystem, JobScheduler scheduler,
      ConnectorPortRegister portRegister) {
    this.config = config;
    this.registry = registry;
    this.logger = logger;
    this.extensionContext = extensionContext;
    this.life = life;
    this.fileSystem = fileSystem;
    this.scheduler = scheduler;
    this.portRegister = portRegister;
  }

  public CompositeEventReporter build() {
    CompositeEventReporter reporter = new CompositeEventReporter();
    if (this.config.get(MetricsSettings.csvEnabled)) {
      CsvOutput csvOutput = new CsvOutput(this.config, this.registry, this.logger,
          this.extensionContext, this.fileSystem, this.scheduler);
      reporter.add(csvOutput);
      this.life.add(csvOutput);
    }

    HostnamePort server;
    if (this.config.get(MetricsSettings.graphiteEnabled)) {
      server = this.config.get(MetricsSettings.graphiteServer);
      long period = this.config.get(MetricsSettings.graphiteInterval).toMillis();
      GraphiteOutput graphiteOutput = new GraphiteOutput(server, period, this.registry,
          this.logger);
      reporter.add(graphiteOutput);
      this.life.add(graphiteOutput);
    }

    if (this.config.get(MetricsSettings.prometheusEnabled)) {
      server = this.config.get(MetricsSettings.prometheusEndpoint);
      PrometheusOutput prometheusOutput = new PrometheusOutput(server, this.registry, this.logger,
          this.portRegister);
      reporter.add(prometheusOutput);
      this.life.add(prometheusOutput);
    }

    if (this.config.get(MetricsSettings.jmxEnabled)) {
      String domain = this.config.get(MetricsSettings.metricsPrefix) + ".metrics";
      JmxReporter jmxReporter = JmxReporter.forRegistry(this.registry).inDomain(domain)
          .createsObjectNamesWith(
              new EventReporterBuilder.MetricsObjectNameFactory()).build();
      this.life.add(new JmxOutput(jmxReporter));
    }

    return reporter;
  }

  private static class MetricsObjectNameFactory implements ObjectNameFactory {

    private static final String NAME = "name";

    public ObjectName createName(String type, String domain, String name) {
      try {
        ObjectName objectName = new ObjectName(domain, "name", name);
        String validatedName = objectName.isPropertyValuePattern() ? ObjectName.quote(name) : name;
        return new ObjectName(domain, "name", validatedName);
      } catch (MalformedObjectNameException n7) {
        try {
          return new ObjectName(domain, "name", ObjectName.quote(name));
        } catch (MalformedObjectNameException n6) {
          throw new RuntimeException(n6);
        }
      }
    }
  }
}
