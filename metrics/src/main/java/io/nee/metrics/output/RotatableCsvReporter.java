/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.output;

import com.codahale.metrics.Clock;
import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import com.codahale.metrics.Snapshot;
import com.codahale.metrics.Timer;
import java.io.File;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.Function;
import org.neo4j.io.IOUtils;
import org.neo4j.logging.RotatingFileOutputStreamSupplier;
import org.neo4j.logging.RotatingFileOutputStreamSupplier.RotationListener;

public class RotatableCsvReporter extends ScheduledReporter {

  private final Locale locale;
  private final Clock clock;
  private final File directory;
  private final Map<File, RotatableCsvReporter.CsvRotatableWriter> writers;
  private final BiFunction<File, RotationListener, RotatingFileOutputStreamSupplier> fileSupplierStreamCreator;
  private boolean stopped;

  RotatableCsvReporter(MetricRegistry registry, Locale locale, TimeUnit rateUnit,
      TimeUnit durationUnit, Clock clock, File directory,
      BiFunction<File, RotationListener, RotatingFileOutputStreamSupplier> fileSupplierStreamCreator) {
    super(registry, "csv-reporter", MetricFilter.ALL, rateUnit, durationUnit);
    this.locale = locale;
    this.clock = clock;
    this.directory = directory;
    this.fileSupplierStreamCreator = fileSupplierStreamCreator;
    this.writers = new HashMap();
  }

  static RotatableCsvReporter.Builder forRegistry(MetricRegistry registry) {
    return new RotatableCsvReporter.Builder(registry);
  }

  public synchronized void stop() {
    super.stop();
    this.stopped = true;
    this.writers.values().forEach(RotatableCsvReporter.CsvRotatableWriter::close);
  }

  public synchronized void report(SortedMap<String, Gauge> gauges,
      SortedMap<String, Counter> counters, SortedMap<String, Histogram> histograms,
      SortedMap<String, Meter> meters, SortedMap<String, Timer> timers) {
    if (!this.stopped) {
      long timestamp = TimeUnit.MILLISECONDS.toSeconds(this.clock.getTime());
      Iterator n8 = gauges.entrySet().iterator();

      Entry entry;
      while (n8.hasNext()) {
        entry = (Entry) n8.next();
        this.reportGauge(timestamp, (String) entry.getKey(), (Gauge) entry.getValue());
      }

      n8 = counters.entrySet().iterator();

      while (n8.hasNext()) {
        entry = (Entry) n8.next();
        this.reportCounter(timestamp, (String) entry.getKey(), (Counter) entry.getValue());
      }

      n8 = histograms.entrySet().iterator();

      while (n8.hasNext()) {
        entry = (Entry) n8.next();
        this.reportHistogram(timestamp, (String) entry.getKey(), (Histogram) entry.getValue());
      }

      n8 = meters.entrySet().iterator();

      while (n8.hasNext()) {
        entry = (Entry) n8.next();
        this.reportMeter(timestamp, (String) entry.getKey(), (Meter) entry.getValue());
      }

      n8 = timers.entrySet().iterator();

      while (n8.hasNext()) {
        entry = (Entry) n8.next();
        this.reportTimer(timestamp, (String) entry.getKey(), (Timer) entry.getValue());
      }
    }
  }

  private void reportTimer(long timestamp, String name, Timer timer) {
    Snapshot snapshot = timer.getSnapshot();
    this.report(timestamp, name,
        "count,max,mean,min,stddev,p50,p75,p95,p98,p99,p999,mean_rate,m1_rate,m5_rate,m15_rate,rate_unit,duration_unit",
        "%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,calls/%s,%s", timer.getCount(),
        this.convertDuration((double) snapshot.getMax()),
        this.convertDuration(snapshot.getMean()), this.convertDuration((double) snapshot.getMin()),
        this.convertDuration(snapshot.getStdDev()),
        this.convertDuration(snapshot.getMedian()),
        this.convertDuration(snapshot.get75thPercentile()),
        this.convertDuration(snapshot.get95thPercentile()),
        this.convertDuration(snapshot.get98thPercentile()),
        this.convertDuration(snapshot.get99thPercentile()),
        this.convertDuration(snapshot.get999thPercentile()),
        this.convertRate(timer.getMeanRate()), this.convertRate(timer.getOneMinuteRate()),
        this.convertRate(timer.getFiveMinuteRate()),
        this.convertRate(timer.getFifteenMinuteRate()), this.getRateUnit(), this.getDurationUnit());
  }

  private void reportMeter(long timestamp, String name, Meter meter) {
    this.report(timestamp, name, "count,mean_rate,m1_rate,m5_rate,m15_rate,rate_unit",
        "%d,%f,%f,%f,%f,events/%s", meter.getCount(),
        this.convertRate(meter.getMeanRate()), this.convertRate(meter.getOneMinuteRate()),
        this.convertRate(meter.getFiveMinuteRate()),
        this.convertRate(meter.getFifteenMinuteRate()), this.getRateUnit());
  }

  private void reportHistogram(long timestamp, String name, Histogram histogram) {
    Snapshot snapshot = histogram.getSnapshot();
    this.report(timestamp, name, "count,max,mean,min,stddev,p50,p75,p95,p98,p99,p999",
        "%d,%d,%f,%d,%f,%f,%f,%f,%f,%f,%f", histogram.getCount(),
        snapshot.getMax(), snapshot.getMean(), snapshot.getMin(), snapshot.getStdDev(),
        snapshot.getMedian(), snapshot.get75thPercentile(),
        snapshot.get95thPercentile(), snapshot.get98thPercentile(), snapshot.get99thPercentile(),
        snapshot.get999thPercentile());
  }

  private void reportCounter(long timestamp, String name, Counter counter) {
    this.report(timestamp, name, "count", "%d", counter.getCount());
  }

  private void reportGauge(long timestamp, String name, Gauge gauge) {
    this.report(timestamp, name, "value", "%s", gauge.getValue());
  }

  private void report(long timestamp, String name, String header, String line, Object... values) {
    File file = new File(this.directory, name + ".csv");
    RotatableCsvReporter.CsvRotatableWriter csvRotatableWriter = this.writers.computeIfAbsent(file,
        new RotatingCsvWriterSupplier(
            header,
            this.fileSupplierStreamCreator));
    csvRotatableWriter.writeValues(this.locale, timestamp, line, values);
  }

  private static class CsvRotatableWriter {

    private final PrintWriter printWriter;
    private final RotatingFileOutputStreamSupplier streamSupplier;

    CsvRotatableWriter(PrintWriter printWriter, RotatingFileOutputStreamSupplier streamSupplier) {
      this.printWriter = printWriter;
      this.streamSupplier = streamSupplier;
    }

    void close() {
      IOUtils.closeAllSilently(this.printWriter, this.streamSupplier);
    }

    void writeValues(Locale locale, long timestamp, String line, Object[] values) {
      this.streamSupplier.get();
      this.printWriter.printf(locale, String.format(locale, "%d,%s%n", timestamp, line), values);
      this.printWriter.flush();
    }
  }

  private static class RotatingCsvWriterSupplier implements
      Function<File, RotatableCsvReporter.CsvRotatableWriter> {

    private final String header;
    private final BiFunction<File, RotationListener, RotatingFileOutputStreamSupplier> fileSupplierStreamCreator;

    RotatingCsvWriterSupplier(String header,
        BiFunction<File, RotationListener, RotatingFileOutputStreamSupplier> fileSupplierStreamCreator) {
      this.header = header;
      this.fileSupplierStreamCreator = fileSupplierStreamCreator;
    }

    private static PrintWriter createWriter(OutputStream outputStream) {
      return new PrintWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
    }

    private static void writeHeader(PrintWriter printWriter, String header) {
      printWriter.println("t," + header);
      printWriter.flush();
    }

    public RotatableCsvReporter.CsvRotatableWriter apply(File file) {
      RotatingFileOutputStreamSupplier outputStreamSupplier = this.fileSupplierStreamCreator
          .apply(file,
              new HeaderWriterRotationListener());
      PrintWriter printWriter = createWriter(outputStreamSupplier.get());
      RotatableCsvReporter.CsvRotatableWriter writer = new RotatableCsvReporter.CsvRotatableWriter(
          printWriter, outputStreamSupplier);
      writeHeader(printWriter, this.header);
      return writer;
    }

    private class HeaderWriterRotationListener extends RotationListener {

      public void rotationCompleted(OutputStream out) {
        super.rotationCompleted(out);
        PrintWriter writer = RotatableCsvReporter.RotatingCsvWriterSupplier.createWriter(out);

        try {
          RotatableCsvReporter.RotatingCsvWriterSupplier
              .writeHeader(writer, RotatingCsvWriterSupplier.this.header);
        } catch (Throwable n6) {
          if (writer != null) {
            try {
              writer.close();
            } catch (Throwable n5) {
              n6.addSuppressed(n5);
            }
          }

          throw n6;
        }

        if (writer != null) {
          writer.close();
        }
      }
    }
  }

  public static class Builder {

    private final MetricRegistry registry;
    private final Clock clock;
    private Locale locale;
    private TimeUnit rateUnit;
    private TimeUnit durationUnit;
    private BiFunction<File, RotationListener, RotatingFileOutputStreamSupplier> outputStreamSupplierFactory;

    private Builder(MetricRegistry registry) {
      this.registry = registry;
      this.locale = Locale.getDefault();
      this.rateUnit = TimeUnit.SECONDS;
      this.durationUnit = TimeUnit.MILLISECONDS;
      this.clock = Clock.defaultClock();
    }

    RotatableCsvReporter.Builder formatFor(Locale locale) {
      this.locale = locale;
      return this;
    }

    RotatableCsvReporter.Builder convertRatesTo(TimeUnit rateUnit) {
      this.rateUnit = rateUnit;
      return this;
    }

    RotatableCsvReporter.Builder convertDurationsTo(TimeUnit durationUnit) {
      this.durationUnit = durationUnit;
      return this;
    }

    RotatableCsvReporter.Builder outputStreamSupplierFactory(
        BiFunction<File, RotationListener, RotatingFileOutputStreamSupplier> outputStreamSupplierFactory) {
      this.outputStreamSupplierFactory = outputStreamSupplierFactory;
      return this;
    }

    public RotatableCsvReporter build(File directory) {
      return new RotatableCsvReporter(this.registry, this.locale, this.rateUnit, this.durationUnit,
          this.clock, directory,
          this.outputStreamSupplierFactory);
    }
  }
}
