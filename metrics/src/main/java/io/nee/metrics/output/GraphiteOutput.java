/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.output;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;
import org.neo4j.internal.helpers.HostnamePort;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.logging.Log;

public class GraphiteOutput implements Lifecycle, EventReporter {

  private final HostnamePort hostnamePort;
  private final long period;
  private final MetricRegistry registry;
  private final Log logger;
  private GraphiteReporter graphiteReporter;

  GraphiteOutput(HostnamePort hostnamePort, long period, MetricRegistry registry, Log logger) {
    this.hostnamePort = hostnamePort;
    this.period = period;
    this.registry = registry;
    this.logger = logger;
  }

  public void init() {
    Graphite graphite = new Graphite(this.hostnamePort.getHost(), this.hostnamePort.getPort());
    this.graphiteReporter =
        GraphiteReporter.forRegistry(this.registry).convertRatesTo(TimeUnit.SECONDS)
            .convertDurationsTo(TimeUnit.MILLISECONDS).filter(
            MetricFilter.ALL).build(graphite);
  }

  public void start() {
    this.graphiteReporter.start(this.period, TimeUnit.MILLISECONDS);
    this.logger.info("Sending metrics to Graphite server at " + this.hostnamePort);
  }

  public void stop() {
    this.graphiteReporter.close();
  }

  public void shutdown() {
    this.graphiteReporter = null;
  }

  public void report(SortedMap<String, Gauge> gauges, SortedMap<String, Counter> counters,
      SortedMap<String, Histogram> histograms,
      SortedMap<String, Meter> meters, SortedMap<String, Timer> timers) {
    synchronized (this.graphiteReporter) {
      this.graphiteReporter.report(gauges, counters, histograms, meters, timers);
    }
  }
}
