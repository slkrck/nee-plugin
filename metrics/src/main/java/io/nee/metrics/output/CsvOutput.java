/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.output;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import io.nee.kernel.impl.enterprise.configuration.MetricsSettings;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.extension.context.ExtensionContext;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.logging.Log;
import org.neo4j.logging.RotatingFileOutputStreamSupplier;
import org.neo4j.logging.RotatingFileOutputStreamSupplier.RotationListener;
import org.neo4j.scheduler.Group;
import org.neo4j.scheduler.JobScheduler;

public class CsvOutput implements Lifecycle, EventReporter {

  private final Config config;
  private final MetricRegistry registry;
  private final Log logger;
  private final ExtensionContext extensionContext;
  private final FileSystemAbstraction fileSystem;
  private final JobScheduler scheduler;
  private RotatableCsvReporter csvReporter;
  private File outputPath;

  CsvOutput(Config config, MetricRegistry registry, Log logger, ExtensionContext extensionContext,
      FileSystemAbstraction fileSystem, JobScheduler scheduler) {
    this.config = config;
    this.registry = registry;
    this.logger = logger;
    this.extensionContext = extensionContext;
    this.fileSystem = fileSystem;
    this.scheduler = scheduler;
  }

  private static File absoluteFileOrRelativeTo(File baseDirectoryIfRelative,
      File absoluteOrRelativeFile) {
    return absoluteOrRelativeFile.isAbsolute() ? absoluteOrRelativeFile
        : new File(baseDirectoryIfRelative, absoluteOrRelativeFile.getPath());
  }

  public void init() throws IOException {
    File configuredPath = this.config.get(MetricsSettings.csvPath).toFile();
    if (configuredPath == null) {
      String n10002 = MetricsSettings.csvPath.name();
      throw new IllegalArgumentException(
          n10002 + " configuration is required since " + MetricsSettings.csvEnabled.name()
              + " is enabled");
    } else {
      Long rotationThreshold = this.config.get(MetricsSettings.csvRotationThreshold);
      Integer maxArchives = this.config.get(MetricsSettings.csvMaxArchives);
      this.outputPath = absoluteFileOrRelativeTo(this.extensionContext.directory(), configuredPath);
      this.csvReporter =
          RotatableCsvReporter.forRegistry(this.registry).convertRatesTo(TimeUnit.SECONDS)
              .convertDurationsTo(TimeUnit.MILLISECONDS).formatFor(
              Locale.US).outputStreamSupplierFactory(
              this.getFileRotatingFileOutputStreamSupplier(rotationThreshold, maxArchives)).build(
              this.ensureDirectoryExists(this.outputPath));
    }
  }

  public void start() {
    this.csvReporter
        .start(this.config.get(MetricsSettings.csvInterval).toMillis(), TimeUnit.MILLISECONDS);
    this.logger.info("Sending metrics to CSV file at " + this.outputPath);
  }

  public void stop() {
    this.csvReporter.stop();
  }

  public void shutdown() {
    this.csvReporter = null;
  }

  public void report(SortedMap<String, Gauge> gauges, SortedMap<String, Counter> counters,
      SortedMap<String, Histogram> histograms,
      SortedMap<String, Meter> meters, SortedMap<String, Timer> timers) {
    this.csvReporter.report(gauges, counters, histograms, meters, timers);
  }

  private BiFunction<File, RotationListener, RotatingFileOutputStreamSupplier> getFileRotatingFileOutputStreamSupplier(
      Long rotationThreshold,
      Integer maxArchives) {
    return (file, listener) ->
    {
      try {
        return new RotatingFileOutputStreamSupplier(this.fileSystem, file, rotationThreshold, 0L,
            maxArchives,
            this.scheduler.executor(Group.LOG_ROTATION), listener);
      } catch (IOException n6) {
        throw new RuntimeException(n6);
      }
    };
  }

  private File ensureDirectoryExists(File dir) throws IOException {
    if (!this.fileSystem.fileExists(dir)) {
      this.fileSystem.mkdirs(dir);
    }

    if (!this.fileSystem.isDirectory(dir)) {
      throw new IllegalStateException(
          "The given path for CSV files points to a file, but a directory is required: " + dir
              .getAbsolutePath());
    } else {
      return dir;
    }
  }
}
