/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.output;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.dropwizard.DropwizardExports;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentHashMap;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.internal.helpers.HostnamePort;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.logging.Log;

public class PrometheusOutput implements Lifecycle, EventReporter {

  private final HostnamePort hostnamePort;
  private final MetricRegistry registry;
  private final Log logger;
  private final ConnectorPortRegister portRegister;
  private final Map<String, Object> registeredEvents = new ConcurrentHashMap();
  private final MetricRegistry eventRegistry;
  protected PrometheusHttpServer server;

  PrometheusOutput(HostnamePort hostnamePort, MetricRegistry registry, Log logger,
      ConnectorPortRegister portRegister) {
    this.hostnamePort = hostnamePort;
    this.registry = registry;
    this.logger = logger;
    this.portRegister = portRegister;
    this.eventRegistry = new MetricRegistry();
  }

  public void init() {
    CollectorRegistry.defaultRegistry.register(new DropwizardExports(this.registry));
    CollectorRegistry.defaultRegistry.register(new DropwizardExports(this.eventRegistry));
  }

  public void start() throws Exception {
    if (this.server == null) {
      this.server = new PrometheusHttpServer(this.hostnamePort.getHost(),
          this.hostnamePort.getPort());
      this.portRegister.register("prometheus", this.server.getAddress());
      this.logger.info("Started publishing Prometheus metrics at http://" + this.server.getAddress()
          + "/metrics");
    }
  }

  public void stop() {
    if (this.server != null) {
      String address = this.server.getAddress().toString();
      this.server.stop();
      this.server = null;
      this.logger.info("Stopped Prometheus endpoint at http://" + address + "/metrics");
    }
  }

  public void shutdown() {
    this.stop();
  }

  public void report(SortedMap<String, Gauge> gauges, SortedMap<String, Counter> counters,
      SortedMap<String, Histogram> histograms,
      SortedMap<String, Meter> meters, SortedMap<String, Timer> timers) {

    if (!gauges.isEmpty()) {
      final String meterKey = gauges.firstKey();
      if (!this.registeredEvents.containsKey(meterKey)) {
        eventRegistry.register(meterKey, (Gauge) () -> registeredEvents.get(meterKey));
      }

      this.registeredEvents.put(meterKey, gauges.get(meterKey).getValue());
    }

    if (!meters.isEmpty()) {
      final String meterKey = meters.firstKey();
      if (!this.registeredEvents.containsKey(meterKey)) {
        this.eventRegistry.register(meterKey, new Counter() {
          public long getCount() {
            return ((Number) PrometheusOutput.this.registeredEvents.get(meterKey)).longValue();
          }
        });
      }

      this.registeredEvents.put(meterKey, meters.get(meterKey).getCount());
    }
  }
}
