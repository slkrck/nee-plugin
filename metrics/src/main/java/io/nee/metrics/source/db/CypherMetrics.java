/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.db;

import com.codahale.metrics.MetricRegistry;
import io.nee.metrics.metric.MetricsCounter;
import java.util.Objects;
import org.neo4j.annotations.documented.Documented;
import org.neo4j.cypher.PlanCacheMetricsMonitor;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.monitoring.Monitors;

@Documented(".Cypher metrics")
public class CypherMetrics extends LifecycleAdapter {

  private static final String CYPHER_PREFIX = "cypher";
  @Documented("The total number of times Cypher has decided to re-plan a query.")
  private static final String REPLAN_EVENTS_TEMPLATE = MetricRegistry
      .name("cypher", "replan_events");
  @Documented("The total number of seconds waited between query replans.")
  private static final String REPLAN_WAIT_TIME_TEMPLATE = MetricRegistry
      .name("cypher", "replan_wait_time");
  private final String replanEvents;
  private final String replanWaitTime;
  private final MetricRegistry registry;
  private final Monitors monitors;
  private final PlanCacheMetricsMonitor cacheMonitor = new PlanCacheMetricsMonitor();

  public CypherMetrics(String metricsPrefix, MetricRegistry registry, Monitors monitors) {
    this.replanEvents = MetricRegistry.name(metricsPrefix, REPLAN_EVENTS_TEMPLATE);
    this.replanWaitTime = MetricRegistry.name(metricsPrefix, REPLAN_WAIT_TIME_TEMPLATE);
    this.registry = registry;
    this.monitors = monitors;
  }

  public void start() {
    this.monitors.addMonitorListener(this.cacheMonitor);
    MetricRegistry n10000 = this.registry;
    String n10001 = this.replanEvents;
    PlanCacheMetricsMonitor n10004 = this.cacheMonitor;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::numberOfReplans));
    n10000 = this.registry;
    n10001 = this.replanWaitTime;
    n10004 = this.cacheMonitor;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::replanWaitTime));
  }

  public void stop() {
    this.registry.remove(this.replanEvents);
    this.registry.remove(this.replanWaitTime);
    this.monitors.removeMonitorListener(this.cacheMonitor);
  }
}
