/*
 * Copyright (c) 2002-2018 "Neo4j,"
 * Neo4j Sweden AB [http://neo4j.com]
 *
 * This file is part of Neo4j Enterprise Edition. The included source
 * code can be redistributed and/or modified under the terms of the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3
 * (http://www.fsf.org/licensing/licenses/agpl-3.0.html)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * Neo4j object code can be licensed independently from the source
 * under separate terms from the AGPL. Inquiries can be directed to:
 * licensing@neo4j.com
 *
 * More information is also available at:
 * https://neo4j.com/licensing/
 */
package io.nee.metrics.source.jvm;

import static com.codahale.metrics.MetricRegistry.name;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import org.neo4j.annotations.documented.Documented;

@Documented(".JVM threads metrics.")
public class ThreadMetrics extends JvmMetrics {

  @Documented("Estimated number of active threads in the current thread group.")
  public static final String THREAD_COUNT = name(JvmMetrics.VM_NAME_PREFIX, "thread.count");
  @Documented("The total number of live threads including daemon and non-daemon threads.")
  public static final String THREAD_TOTAL = name(JvmMetrics.VM_NAME_PREFIX, "thread.total");

  private final String metricsPrefix;
  private final MetricRegistry registry;
  private final ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();

  public ThreadMetrics(String metricsPrefix, MetricRegistry registry) {
    this.metricsPrefix = metricsPrefix;
    this.registry = registry;
  }

  public void start() {
    registry.register(globalMetricsName(THREAD_COUNT), (Gauge<Integer>) Thread::activeCount);
    registry
        .register(globalMetricsName(THREAD_TOTAL), (Gauge<Integer>) threadMXBean::getThreadCount);
  }

  public void stop() {
    registry.remove(globalMetricsName(THREAD_COUNT));
    registry.remove(globalMetricsName(THREAD_TOTAL));
  }

  private String globalMetricsName(String metricsName) {
    return name(metricsPrefix, metricsName);
  }
}
