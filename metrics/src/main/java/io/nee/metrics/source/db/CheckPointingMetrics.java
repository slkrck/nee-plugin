/*
 * Copyright (c) 2002-2018 "Neo4j,"
 * Neo4j Sweden AB [http://neo4j.com]
 *
 * This file is part of Neo4j Enterprise Edition. The included source
 * code can be redistributed and/or modified under the terms of the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3
 * (http://www.fsf.org/licensing/licenses/agpl-3.0.html)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * Neo4j object code can be licensed independently from the source
 * under separate terms from the AGPL. Inquiries can be directed to:
 * licensing@neo4j.com
 *
 * More information is also available at:
 * https://neo4j.com/licensing/
 */
package io.nee.metrics.source.db;

import static com.codahale.metrics.MetricRegistry.name;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import org.neo4j.annotations.documented.Documented;
import org.neo4j.kernel.impl.transaction.stats.CheckpointCounters;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;

@Documented(".Database checkpointing metrics")
public class CheckPointingMetrics extends LifecycleAdapter {

  private static final String CHECK_POINT_PREFIX = "neo4j.check_point";

  @Documented("The total number of check point events executed so far")
  public static final String CHECK_POINT_EVENTS = name(CHECK_POINT_PREFIX, "events");
  @Documented("The total time spent in check pointing so far")
  public static final String CHECK_POINT_TOTAL_TIME = name(CHECK_POINT_PREFIX, "total_time");
  @Documented("The duration of the check point event")
  public static final String CHECK_POINT_DURATION = name(CHECK_POINT_PREFIX,
      "check_point_duration");

  private final String metricsPrefix;
  private final MetricRegistry registry;
  private final CheckpointCounters checkpointCounters;

  public CheckPointingMetrics(String metricsPrefix, MetricRegistry registry,
      CheckpointCounters checkpointCounters) {
    this.metricsPrefix = metricsPrefix;
    this.registry = registry;
    this.checkpointCounters = checkpointCounters;
  }

  @Override
  public void start() {
    registry.register(globalMetricsName(CHECK_POINT_EVENTS),
        (Gauge<Long>) checkpointCounters::numberOfCheckPoints);
    registry.register(globalMetricsName(CHECK_POINT_TOTAL_TIME),
        (Gauge<Long>) checkpointCounters::checkPointAccumulatedTotalTimeMillis);

    registry.register(globalMetricsName(CHECK_POINT_DURATION),
        (Gauge<Long>) checkpointCounters::lastCheckpointTimeMillis);
  }

  @Override
  public void stop() {

    registry.remove(globalMetricsName(CHECK_POINT_EVENTS));
    registry.remove(globalMetricsName(CHECK_POINT_TOTAL_TIME));
    registry.remove(globalMetricsName(CHECK_POINT_DURATION));
  }

  private String globalMetricsName(String metricsName) {
    return name(metricsPrefix, metricsName);
  }
}
