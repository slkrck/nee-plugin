/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.jvm;

import static com.codahale.metrics.MetricRegistry.name;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.MemoryUsage;
import org.neo4j.annotations.documented.Documented;

@Documented(".JVM heap metrics.")
public class HeapMetrics extends JvmMetrics {

  @Documented("Heap committed.")
  public static final String HEAP_COMMITTED = name(JvmMetrics.VM_NAME_PREFIX, "heap.committed");

  @Documented("Heap used.")
  public static final String HEAP_USED = name(JvmMetrics.VM_NAME_PREFIX, "heap.used");

  @Documented("Heap max.")
  public static final String HEAP_MAX = name(JvmMetrics.VM_NAME_PREFIX, "heap.max");

  private final MetricRegistry registry;

  private final String metricsPrefix;

  public HeapMetrics(String metricsPrefix, MetricRegistry registry) {
    this.metricsPrefix = metricsPrefix;
    this.registry = registry;
  }

  public void start() {

    HeapMetrics.MemoryUsageSupplier memoryUsageSupplier = new HeapMetrics.MemoryUsageSupplier();

    registry.register(globalMetricsName(HEAP_COMMITTED),
        (Gauge<Long>) memoryUsageSupplier::getCommitted);
    registry.register(globalMetricsName(HEAP_USED), (Gauge<Long>) memoryUsageSupplier::getUsed);
    registry.register(globalMetricsName(HEAP_MAX), (Gauge<Long>) memoryUsageSupplier::getMax);
  }

  public void stop() {
    this.registry.remove(globalMetricsName(HEAP_COMMITTED));
    this.registry.remove(globalMetricsName(HEAP_USED));
    this.registry.remove(globalMetricsName(HEAP_MAX));
  }

  private String globalMetricsName(String metricsName) {
    return name(metricsPrefix, metricsName);
  }

  private static class MemoryUsageSupplier {

    private final MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
    private volatile MemoryUsage lastMemoryUsage;

    private MemoryUsageSupplier() {
      this.lastMemoryUsage = this.memoryMXBean.getHeapMemoryUsage();
    }

    private long getCommitted() {
      this.lastMemoryUsage = this.memoryMXBean.getHeapMemoryUsage();
      return this.lastMemoryUsage.getCommitted();
    }

    private long getUsed() {
      return this.lastMemoryUsage.getUsed();
    }

    private long getMax() {
      return this.lastMemoryUsage.getMax();
    }
  }
}
