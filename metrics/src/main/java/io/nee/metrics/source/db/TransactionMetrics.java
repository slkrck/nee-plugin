/*
 * Copyright (c) 2002-2018 "Neo4j,"
 * Neo4j Sweden AB [http://neo4j.com]
 *
 * This file is part of Neo4j Enterprise Edition. The included source
 * code can be redistributed and/or modified under the terms of the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3
 * (http://www.fsf.org/licensing/licenses/agpl-3.0.html)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * Neo4j object code can be licensed independently from the source
 * under separate terms from the AGPL. Inquiries can be directed to:
 * licensing@neo4j.com
 *
 * More information is also available at:
 * https://neo4j.com/licensing/
 */
package io.nee.metrics.source.db;

import static com.codahale.metrics.MetricRegistry.name;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import java.util.function.Supplier;
import org.neo4j.annotations.documented.Documented;
import org.neo4j.kernel.impl.transaction.stats.TransactionCounters;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.storageengine.api.TransactionIdStore;

@Documented(".Database transaction metrics")
public class TransactionMetrics extends LifecycleAdapter {

  private static final String TRANSACTION_PREFIX = "neo4j.transaction";

  @Documented("The total number of started transactions")
  public static final String TX_STARTED = name(TRANSACTION_PREFIX, "started");
  @Documented("The highest peak of concurrent transactions ever seen on this machine")
  public static final String TX_PEAK_CONCURRENT = name(TRANSACTION_PREFIX, "peak_concurrent");

  @Documented("The number of currently active transactions")
  public static final String TX_ACTIVE = name(TRANSACTION_PREFIX, "active");
  @Documented("The number of currently active read transactions")
  public static final String READ_TX_ACTIVE = name(TRANSACTION_PREFIX, "active_read");
  @Documented("The number of currently active write transactions")
  public static final String WRITE_TX_ACTIVE = name(TRANSACTION_PREFIX, "active_write");

  @Documented("The total number of committed transactions")
  public static final String TX_COMMITTED = name(TRANSACTION_PREFIX, "committed");
  @Documented("The total number of committed read transactions")
  public static final String READ_TX_COMMITTED = name(TRANSACTION_PREFIX, "committed_read");
  @Documented("The total number of committed write transactions")
  public static final String WRITE_TX_COMMITTED = name(TRANSACTION_PREFIX, "committed_write");

  @Documented("The total number of rolled back transactions")
  public static final String TX_ROLLBACKS = name(TRANSACTION_PREFIX, "rollbacks");
  @Documented("The total number of rolled back read transactions")
  public static final String READ_TX_ROLLBACKS = name(TRANSACTION_PREFIX, "rollbacks_read");
  @Documented("The total number of rolled back write transactions")
  public static final String WRITE_TX_ROLLBACKS = name(TRANSACTION_PREFIX, "rollbacks_write");

  @Documented("The total number of terminated transactions")
  public static final String TX_TERMINATED = name(TRANSACTION_PREFIX, "terminated");
  @Documented("The total number of terminated read transactions")
  public static final String READ_TX_TERMINATED = name(TRANSACTION_PREFIX, "terminated_read");
  @Documented("The total number of terminated write transactions")
  public static final String WRITE_TX_TERMINATED = name(TRANSACTION_PREFIX, "terminated_write");

  @Documented("The ID of the last committed transaction")
  public static final String LAST_COMMITTED_TX_ID = name(TRANSACTION_PREFIX,
      "last_committed_tx_id");
  @Documented("The ID of the last closed transaction")
  public static final String LAST_CLOSED_TX_ID = name(TRANSACTION_PREFIX, "last_closed_tx_id");

  // TODO: Do we really need to take in the metricsPrefix?
  private final String metricsPrefix;
  private final MetricRegistry registry;
  private final TransactionCounters transactionCounters;
  private final Supplier<TransactionIdStore> transactionIdStore;

  public TransactionMetrics(String metricsPrefix, MetricRegistry registry,
      Supplier<TransactionIdStore> transactionIdStore,
      TransactionCounters transactionCounters) {
    this.metricsPrefix = metricsPrefix;
    this.registry = registry;
    this.transactionIdStore = transactionIdStore;
    this.transactionCounters = transactionCounters;
  }

  @Override
  public void start() {
    registry.register(globalMetricsName(TX_STARTED),
        (Gauge<Long>) transactionCounters::getNumberOfStartedTransactions);
    registry.register(globalMetricsName(TX_PEAK_CONCURRENT),
        (Gauge<Long>) transactionCounters::getPeakConcurrentNumberOfTransactions);

    registry.register(globalMetricsName(TX_ACTIVE),
        (Gauge<Long>) transactionCounters::getNumberOfActiveTransactions);
    registry.register(globalMetricsName(READ_TX_ACTIVE),
        (Gauge<Long>) transactionCounters::getNumberOfActiveReadTransactions);
    registry.register(globalMetricsName(WRITE_TX_ACTIVE),
        (Gauge<Long>) transactionCounters::getNumberOfActiveWriteTransactions);

    registry.register(globalMetricsName(TX_COMMITTED),
        (Gauge<Long>) transactionCounters::getNumberOfCommittedTransactions);
    registry.register(globalMetricsName(READ_TX_COMMITTED),
        (Gauge<Long>) transactionCounters::getNumberOfCommittedReadTransactions);
    registry.register(globalMetricsName(WRITE_TX_COMMITTED),
        (Gauge<Long>) transactionCounters::getNumberOfCommittedWriteTransactions);

    registry.register(globalMetricsName(TX_ROLLBACKS),
        (Gauge<Long>) transactionCounters::getNumberOfRolledBackTransactions);
    registry.register(globalMetricsName(READ_TX_ROLLBACKS),
        (Gauge<Long>) transactionCounters::getNumberOfRolledBackReadTransactions);
    registry.register(globalMetricsName(WRITE_TX_ROLLBACKS),
        (Gauge<Long>) transactionCounters::getNumberOfRolledBackWriteTransactions);

    registry.register(globalMetricsName(TX_TERMINATED),
        (Gauge<Long>) transactionCounters::getNumberOfTerminatedTransactions);
    registry.register(globalMetricsName(READ_TX_TERMINATED),
        (Gauge<Long>) transactionCounters::getNumberOfTerminatedReadTransactions);
    registry.register(globalMetricsName(WRITE_TX_TERMINATED),
        (Gauge<Long>) transactionCounters::getNumberOfTerminatedWriteTransactions);

    registry.register(globalMetricsName(LAST_COMMITTED_TX_ID),
        (Gauge<Long>) () -> transactionIdStore.get().getLastCommittedTransactionId());
    registry.register(globalMetricsName(LAST_CLOSED_TX_ID),
        (Gauge<Long>) () -> transactionIdStore.get().getLastClosedTransactionId());
  }

  @Override
  public void stop() {
    registry.remove(globalMetricsName(TX_STARTED));
    registry.remove(globalMetricsName(TX_PEAK_CONCURRENT));

    registry.remove(globalMetricsName(TX_ACTIVE));
    registry.remove(globalMetricsName(READ_TX_ACTIVE));
    registry.remove(globalMetricsName(WRITE_TX_ACTIVE));

    registry.remove(globalMetricsName(TX_COMMITTED));
    registry.remove(globalMetricsName(READ_TX_COMMITTED));
    registry.remove(globalMetricsName(WRITE_TX_COMMITTED));

    registry.remove(globalMetricsName(TX_ROLLBACKS));
    registry.remove(globalMetricsName(READ_TX_ROLLBACKS));
    registry.remove(globalMetricsName(WRITE_TX_ROLLBACKS));

    registry.remove(globalMetricsName(TX_TERMINATED));
    registry.remove(globalMetricsName(READ_TX_TERMINATED));
    registry.remove(globalMetricsName(WRITE_TX_TERMINATED));

    registry.remove(globalMetricsName(LAST_COMMITTED_TX_ID));
    registry.remove(globalMetricsName(LAST_CLOSED_TX_ID));
  }

  private String globalMetricsName(String metricsName) {
    return name(metricsPrefix, metricsName);
  }
}
