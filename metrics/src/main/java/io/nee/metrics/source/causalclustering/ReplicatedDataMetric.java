/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.causalclustering;

import com.codahale.metrics.Gauge;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataMonitor;
import java.util.EnumMap;
import java.util.Map;

public class ReplicatedDataMetric implements ReplicatedDataMonitor {

  private static final Integer DEFAULT = 0;
  private final Map<ReplicatedDataIdentifier, Integer> visibleDataSize = new EnumMap(
      ReplicatedDataIdentifier.class);
  private final Map<ReplicatedDataIdentifier, Integer> invisibleDataSize = new EnumMap(
      ReplicatedDataIdentifier.class);

  public void setVisibleDataSize(ReplicatedDataIdentifier key, int size) {
    this.visibleDataSize.put(key, size);
  }

  public void setInvisibleDataSize(ReplicatedDataIdentifier key, int size) {
    this.invisibleDataSize.put(key, size);
  }

  public Gauge<Integer> getVisibleDataSize(ReplicatedDataIdentifier key) {
    return () ->
    {
      return this.result(this.visibleDataSize, key);
    };
  }

  public Gauge<Integer> getInvisibleDataSize(ReplicatedDataIdentifier key) {
    return () ->
    {
      return this.result(this.invisibleDataSize, key);
    };
  }

  private Integer result(Map<ReplicatedDataIdentifier, Integer> map, ReplicatedDataIdentifier key) {
    Integer result = map.get(key);
    return result == null ? DEFAULT : result;
  }
}
