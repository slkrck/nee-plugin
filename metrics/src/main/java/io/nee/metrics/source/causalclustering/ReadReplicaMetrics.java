/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.causalclustering;

import com.codahale.metrics.MetricRegistry;
import io.nee.metrics.metric.MetricsCounter;
import java.util.Objects;
import org.neo4j.annotations.documented.Documented;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.monitoring.Monitors;

@Documented(".Read Replica Metrics")
public class ReadReplicaMetrics extends LifecycleAdapter {

  private static final String CAUSAL_CLUSTERING_PREFIX = "causal_clustering.read_replica";
  @Documented("The total number of pull requests made by this instance.")
  private static final String PULL_UPDATES_TEMPLATE = MetricRegistry
      .name("causal_clustering.read_replica", "pull_updates");
  @Documented("The highest transaction id requested in a pull update by this instance.")
  private static final String PULL_UPDATE_HIGHEST_TX_ID_REQUESTED_TEMPLATE =
      MetricRegistry.name("causal_clustering.read_replica", "pull_update_highest_tx_id_requested");
  @Documented("The highest transaction id that has been pulled in the last pull updates by this instance.")
  private static final String PULL_UPDATE_HIGHEST_TX_ID_RECEIVED_TEMPLATE =
      MetricRegistry.name("causal_clustering.read_replica", "pull_update_highest_tx_id_received");
  private final String pullUpdates;
  private final String pullUpdateHighestTxIdRequested;
  private final String pullUpdateHighestTxIdReceived;
  private final Monitors monitors;
  private final MetricRegistry registry;
  private final PullRequestMetric pullRequestMetric = new PullRequestMetric();

  public ReadReplicaMetrics(String metricsPrefix, Monitors monitors, MetricRegistry registry) {
    this.pullUpdates = MetricRegistry.name(metricsPrefix, PULL_UPDATES_TEMPLATE);
    this.pullUpdateHighestTxIdRequested = MetricRegistry
        .name(metricsPrefix, PULL_UPDATE_HIGHEST_TX_ID_REQUESTED_TEMPLATE);
    this.pullUpdateHighestTxIdReceived = MetricRegistry
        .name(metricsPrefix, PULL_UPDATE_HIGHEST_TX_ID_RECEIVED_TEMPLATE);
    this.monitors = monitors;
    this.registry = registry;
  }

  public void start() {
    this.monitors.addMonitorListener(this.pullRequestMetric);
    MetricRegistry n10000 = this.registry;
    String n10001 = this.pullUpdates;
    PullRequestMetric n10004 = this.pullRequestMetric;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::numberOfRequests));
    n10000 = this.registry;
    n10001 = this.pullUpdateHighestTxIdRequested;
    n10004 = this.pullRequestMetric;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::lastRequestedTxId));
    n10000 = this.registry;
    n10001 = this.pullUpdateHighestTxIdReceived;
    n10004 = this.pullRequestMetric;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::lastReceivedTxId));
  }

  public void stop() {
    this.registry.remove(this.pullUpdates);
    this.registry.remove(this.pullUpdateHighestTxIdRequested);
    this.registry.remove(this.pullUpdateHighestTxIdReceived);
    this.monitors.removeMonitorListener(this.pullRequestMetric);
  }
}
