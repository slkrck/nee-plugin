/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.causalclustering;

import com.codahale.metrics.MetricRegistry;
import io.nee.metrics.metric.MetricsCounter;
import java.util.Objects;
import org.neo4j.annotations.documented.Documented;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.monitoring.Monitors;

@Documented(".CatchUp Metrics")
public class CatchUpMetrics extends LifecycleAdapter {

  @Documented("TX pull requests received from read replicas.")
  private static final String TX_PULL_REQUESTS_RECEIVED_TEMPLATE =
      MetricRegistry.name("causal_clustering.catchup.tx_pull_requests_received");
  private final String txPullRequestsReceived;
  private final Monitors monitors;
  private final MetricRegistry registry;
  private final TxPullRequestsMetric txPullRequestsMetric = new TxPullRequestsMetric();

  public CatchUpMetrics(String metricsPrefix, Monitors monitors, MetricRegistry registry) {
    this.txPullRequestsReceived = MetricRegistry
        .name(metricsPrefix, TX_PULL_REQUESTS_RECEIVED_TEMPLATE);
    this.monitors = monitors;
    this.registry = registry;
  }

  public void start() {
    this.monitors.addMonitorListener(this.txPullRequestsMetric);
    MetricRegistry n10000 = this.registry;
    String n10001 = this.txPullRequestsReceived;
    TxPullRequestsMetric n10004 = this.txPullRequestsMetric;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::txPullRequestsReceived));
  }

  public void stop() {
    this.registry.remove(this.txPullRequestsReceived);
    this.monitors.removeMonitorListener(this.txPullRequestsMetric);
  }
}
