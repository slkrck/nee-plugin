/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.causalclustering;

import io.nee.causalclustering.core.replication.monitoring.ReplicationMonitor;
import java.util.concurrent.atomic.AtomicLong;

public class ReplicationMetric implements ReplicationMonitor {

  private final AtomicLong newReplication = new AtomicLong();
  private final AtomicLong attempts = new AtomicLong();
  private final AtomicLong notReplicated = new AtomicLong();
  private final AtomicLong maybeReplicated = new AtomicLong();
  private final AtomicLong successfullyReplicated = new AtomicLong();

  public void clientRequest() {
    this.newReplication.getAndIncrement();
  }

  public void replicationAttempt() {
    this.attempts.getAndIncrement();
  }

  public void notReplicated() {
    this.notReplicated.getAndIncrement();
  }

  public void maybeReplicated() {
    this.maybeReplicated.getAndIncrement();
  }

  public void successfullyReplicated() {
    this.successfullyReplicated.getAndIncrement();
  }

  long newReplicationCount() {
    return this.newReplication.get();
  }

  long attemptCount() {
    return this.attempts.get();
  }

  long failCount() {
    return this.notReplicated.get();
  }

  long maybeCount() {
    return this.maybeReplicated.get();
  }

  long successCount() {
    return this.successfullyReplicated.get();
  }
}
