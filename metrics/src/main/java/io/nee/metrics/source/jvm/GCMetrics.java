/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.jvm;

import com.codahale.metrics.MetricRegistry;
import io.nee.metrics.metric.MetricsCounter;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.Iterator;
import java.util.Objects;
import org.neo4j.annotations.documented.Documented;

@Documented(".GC metrics.")
public class GCMetrics extends JvmMetrics {

  private static final String GC_PREFIX = MetricRegistry.name("vm", "gc");
  @Documented("Accumulated garbage collection time in milliseconds.")
  private static final String GC_TIME_TEMPLATE;
  @Documented("Total number of garbage collections.")
  private static final String GC_COUNT_TEMPLATE;

  static {
    GC_TIME_TEMPLATE = MetricRegistry.name(GC_PREFIX, "time", "%s");
    GC_COUNT_TEMPLATE = MetricRegistry.name(GC_PREFIX, "count", "%s");
  }

  private final String gcPrefix;
  private final String gcTime;
  private final String gcCount;
  private final MetricRegistry registry;

  public GCMetrics(String metricsPrefix, MetricRegistry registry) {
    this.registry = registry;
    this.gcPrefix = MetricRegistry.name(metricsPrefix, GC_PREFIX);
    this.gcTime = MetricRegistry.name(metricsPrefix, GC_TIME_TEMPLATE);
    this.gcCount = MetricRegistry.name(metricsPrefix, GC_COUNT_TEMPLATE);
  }

  public void start() {
    Iterator n1 = ManagementFactory.getGarbageCollectorMXBeans().iterator();

    while (n1.hasNext()) {
      GarbageCollectorMXBean gcBean = (GarbageCollectorMXBean) n1.next();
      MetricRegistry n10000 = this.registry;
      String n10001 = String.format(this.gcTime, prettifyName(gcBean.getName()));
      Objects.requireNonNull(gcBean);
      n10000.register(n10001, new MetricsCounter(gcBean::getCollectionTime));
      n10000 = this.registry;
      n10001 = String.format(this.gcCount, prettifyName(gcBean.getName()));
      Objects.requireNonNull(gcBean);
      n10000.register(n10001, new MetricsCounter(gcBean::getCollectionCount));
    }
  }

  public void stop() {
    this.registry.removeMatching((name, metric) ->
    {
      return name.startsWith(this.gcPrefix);
    });
  }
}
