/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.db;

import static com.codahale.metrics.MetricRegistry.name;

import com.codahale.metrics.MetricRegistry;
import io.nee.metrics.metric.MetricsCounter;
import org.neo4j.annotations.documented.Documented;
import org.neo4j.kernel.impl.transaction.stats.TransactionLogCounters;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;

@Documented(".Database transaction log metrics")
public class TransactionLogsMetrics extends LifecycleAdapter {

  private static final String TX_LOG_PREFIX = "log";

  @Documented("The total number of transaction log rotations executed so far.")
  private static final String LOG_ROTATION_EVENTS = name(TX_LOG_PREFIX, "rotation_events");

  @Documented("The total time spent in rotating transaction logs so far.")
  private static final String LOG_ROTATION_TOTAL_TIME = name(TX_LOG_PREFIX, "rotation_total_time");

  @Documented("The duration of the last log rotation event.")
  private static final String LOG_ROTATION_DURATION = name(TX_LOG_PREFIX, "rotation_duration");

  @Documented("The total number of bytes appended to transaction log.")
  private static final String LOG_APPENDED = name(TX_LOG_PREFIX, "appended_bytes");
  // TODO: Do we really need to take in the metricsPrefix?
  private final String metricsPrefix;
  private final MetricRegistry registry;
  private final TransactionLogCounters logCounters;

  public TransactionLogsMetrics(String metricsPrefix, MetricRegistry registry,
      TransactionLogCounters logCounters) {
    this.metricsPrefix = metricsPrefix;
    this.registry = registry;
    this.logCounters = logCounters;
  }

  public void start() {

    registry.register(globalMetricsName(LOG_ROTATION_EVENTS),
        new MetricsCounter(logCounters::numberOfLogRotations));
    registry.register(globalMetricsName(LOG_ROTATION_TOTAL_TIME),
        new MetricsCounter(logCounters::logRotationAccumulatedTotalTimeMillis));
    registry.register(globalMetricsName(LOG_ROTATION_DURATION),
        new MetricsCounter(logCounters::lastLogRotationTimeMillis));
    registry
        .register(globalMetricsName(LOG_APPENDED), new MetricsCounter(logCounters::appendedBytes));
  }

  public void stop() {
    this.registry.remove(globalMetricsName(LOG_ROTATION_EVENTS));
    this.registry.remove(globalMetricsName(LOG_ROTATION_TOTAL_TIME));
    this.registry.remove(globalMetricsName(LOG_ROTATION_DURATION));
    this.registry.remove(globalMetricsName(LOG_APPENDED));
  }

  private String globalMetricsName(String metricsName) {
    return name(metricsPrefix, metricsName);
  }
}
