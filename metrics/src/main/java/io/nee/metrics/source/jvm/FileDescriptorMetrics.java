/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.jvm;

import static com.codahale.metrics.MetricRegistry.name;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import org.apache.commons.lang3.SystemUtils;
import org.neo4j.annotations.documented.Documented;
import org.neo4j.io.os.OsBeanUtil;

@Documented(".JVM file descriptor metrics.")
public class FileDescriptorMetrics extends JvmMetrics {

  @Documented("The current number of open file descriptors.")
  public static final String FILE_COUNT = name(JvmMetrics.VM_NAME_PREFIX, "file.descriptors.count");

  @Documented("The maximum number of open file descriptors.")
  public static final String FILE_MAXIMUM = name(JvmMetrics.VM_NAME_PREFIX,
      "file.descriptors.maximum");

  private final MetricRegistry registry;

  public FileDescriptorMetrics(String metricsPrefix, MetricRegistry registry) {
    this.registry = registry;
  }

  public void start() {
    if (SystemUtils.IS_OS_UNIX) {
      registry.register(FILE_COUNT, (Gauge<Long>) OsBeanUtil::getOpenFileDescriptors);
      registry.register(FILE_MAXIMUM, (Gauge<Long>) OsBeanUtil::getMaxFileDescriptors);
    } else {
      registry.register(FILE_COUNT, (Gauge<Long>) () -> -1L);
      registry.register(FILE_MAXIMUM, (Gauge<Long>) () -> -1L);
    }
  }

  public void stop() {
    registry.remove(FILE_COUNT);
    registry.remove(FILE_MAXIMUM);
  }
}
