/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.causalclustering;

import com.codahale.metrics.Reservoir;
import com.codahale.metrics.Timer;
import io.nee.causalclustering.core.consensus.RaftMessageProcessingMonitor;
import io.nee.causalclustering.core.consensus.RaftMessages;
import java.time.Duration;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;

public class RaftMessageProcessingMetric implements RaftMessageProcessingMonitor {

  private final AtomicLong delay = new AtomicLong(0L);
  private final Timer timer;
  private final Map<RaftMessages.Type, Timer> typeTimers = new EnumMap(RaftMessages.Type.class);

  private RaftMessageProcessingMetric(Supplier<Timer> timerFactory) {
    RaftMessages.Type[] n2 = RaftMessages.Type.values();
    int n3 = n2.length;

    for (int n4 = 0; n4 < n3; ++n4) {
      RaftMessages.Type type = n2[n4];
      this.typeTimers.put(type, timerFactory.get());
    }

    this.timer = timerFactory.get();
  }

  public static RaftMessageProcessingMetric create() {
    return new RaftMessageProcessingMetric(Timer::new);
  }

  public static RaftMessageProcessingMetric createUsing(Supplier<Reservoir> reservoir) {
    return new RaftMessageProcessingMetric(() ->
    {
      return new Timer(reservoir.get());
    });
  }

  public long delay() {
    return this.delay.get();
  }

  public void setDelay(Duration delay) {
    this.delay.set(delay.toMillis());
  }

  public Timer timer() {
    return this.timer;
  }

  public Timer timer(RaftMessages.Type type) {
    return this.typeTimers.get(type);
  }

  public void updateTimer(RaftMessages.Type type, Duration duration) {
    long nanos = duration.toNanos();
    this.timer.update(nanos, TimeUnit.NANOSECONDS);
    this.typeTimers.get(type).update(nanos, TimeUnit.NANOSECONDS);
  }
}
