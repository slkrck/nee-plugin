/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.db;

import com.codahale.metrics.MetricRegistry;
import io.nee.dbms.database.DatabaseOperationCountMonitor;
import io.nee.metrics.metric.MetricsCounter;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import org.neo4j.annotations.documented.Documented;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.monitoring.Monitors;

@Documented(".Multi database count metrics")
public class DatabaseOperationCountMetrics extends LifecycleAdapter {

  @Documented("Database create operations.")
  public static final String DATABASE_CREATE_COUNT = MetricRegistry
      .name("db.operation.count", "create");
  @Documented("Database start operations.")
  public static final String DATABASE_START_COUNT = MetricRegistry
      .name("db.operation.count", "start");
  @Documented("Database stop operations.")
  public static final String DATABASE_STOP_COUNT = MetricRegistry
      .name("db.operation.count", "stop");
  @Documented("Database drop operations.")
  public static final String DATABASE_DROP_COUNT = MetricRegistry
      .name("db.operation.count", "drop");
  @Documented("Count of failed database operations.")
  public static final String DATABASE_FAILED_COUNT = MetricRegistry
      .name("db.operation.count", "failed");
  @Documented("Count of database operations recovered.")
  public static final String DATABASE_RECOVERED_COUNT = MetricRegistry
      .name("db.operation.count", "recovered");
  private static final String SERVER_PREFIX = "db.operation.count";
  private final String countCreate;
  private final String countStart;
  private final String countStop;
  private final String countDrop;
  private final String countFailed;
  private final String countRecovered;
  private final MetricRegistry registry;
  private final Monitors monitors;
  private final DatabaseOperationCountMetrics.DatabaseOperationCountMetric databaseCountMetric =
      new DatabaseOperationCountMetrics.DatabaseOperationCountMetric();

  public DatabaseOperationCountMetrics(String metricsPrefix, MetricRegistry registry,
      Monitors monitors) {
    this.registry = registry;
    this.monitors = monitors;
    this.countCreate = MetricRegistry.name(metricsPrefix, DATABASE_CREATE_COUNT);
    this.countStart = MetricRegistry.name(metricsPrefix, DATABASE_START_COUNT);
    this.countStop = MetricRegistry.name(metricsPrefix, DATABASE_STOP_COUNT);
    this.countDrop = MetricRegistry.name(metricsPrefix, DATABASE_DROP_COUNT);
    this.countFailed = MetricRegistry.name(metricsPrefix, DATABASE_FAILED_COUNT);
    this.countRecovered = MetricRegistry.name(metricsPrefix, DATABASE_RECOVERED_COUNT);
  }

  public void start() {
    this.monitors.addMonitorListener(this.databaseCountMetric);
    MetricRegistry n10000 = this.registry;
    String n10001 = this.countCreate;
    DatabaseOperationCountMetrics.DatabaseOperationCountMetric n10004 = this.databaseCountMetric;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::createCount));
    n10000 = this.registry;
    n10001 = this.countStart;
    n10004 = this.databaseCountMetric;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::startCount));
    n10000 = this.registry;
    n10001 = this.countStop;
    n10004 = this.databaseCountMetric;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::stopCount));
    n10000 = this.registry;
    n10001 = this.countDrop;
    n10004 = this.databaseCountMetric;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::dropCount));
    n10000 = this.registry;
    n10001 = this.countFailed;
    n10004 = this.databaseCountMetric;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::failedCount));
    n10000 = this.registry;
    n10001 = this.countRecovered;
    n10004 = this.databaseCountMetric;
    Objects.requireNonNull(n10004);
    n10000.register(n10001, new MetricsCounter(n10004::recoveredCount));
  }

  public void stop() {
    this.registry.remove(this.countCreate);
    this.registry.remove(this.countStart);
    this.registry.remove(this.countStop);
    this.registry.remove(this.countDrop);
    this.registry.remove(this.countFailed);
    this.registry.remove(this.countRecovered);
    this.monitors.removeMonitorListener(this.databaseCountMetric);
  }

  static class DatabaseOperationCountMetric implements DatabaseOperationCountMonitor {

    private final AtomicLong createCount = new AtomicLong(0L);
    private final AtomicLong startCount = new AtomicLong(0L);
    private final AtomicLong stopCount = new AtomicLong(0L);
    private final AtomicLong dropCount = new AtomicLong(0L);
    private final AtomicLong failedCount = new AtomicLong(0L);
    private final AtomicLong recoveredCount = new AtomicLong(0L);

    public long startCount() {
      return this.startCount.get();
    }

    public long createCount() {
      return this.createCount.get();
    }

    public long stopCount() {
      return this.stopCount.get();
    }

    public long dropCount() {
      return this.dropCount.get();
    }

    public long failedCount() {
      return this.failedCount.get();
    }

    public long recoveredCount() {
      return this.recoveredCount.get();
    }

    public void increaseCreateCount() {
      this.createCount.incrementAndGet();
    }

    public void increaseStartCount() {
      this.startCount.incrementAndGet();
    }

    public void increaseStopCount() {
      this.stopCount.incrementAndGet();
    }

    public void increaseDropCount() {
      this.dropCount.incrementAndGet();
    }

    public void increaseFailedCount() {
      this.failedCount.incrementAndGet();
    }

    public void increaseRecoveredCount() {
      this.recoveredCount.incrementAndGet();
    }

    public void resetCounts() {
      this.createCount.set(0L);
      this.startCount.set(0L);
      this.stopCount.set(0L);
      this.dropCount.set(0L);
      this.failedCount.set(0L);
      this.recoveredCount.set(0L);
    }
  }
}
