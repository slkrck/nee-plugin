/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.causalclustering;

import static com.codahale.metrics.MetricRegistry.name;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import io.nee.causalclustering.core.consensus.CoreMetaData;
import io.nee.causalclustering.core.consensus.RaftMessages;
import io.nee.causalclustering.discovery.akka.monitoring.ReplicatedDataIdentifier;
import java.util.Arrays;
import java.util.function.Supplier;
import org.neo4j.annotations.documented.Documented;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.monitoring.Monitors;


@Documented(".Core metrics")
public class CoreMetrics extends LifecycleAdapter {

  private static final String CAUSAL_CLUSTERING_PREFIX = "neo4j.causal_clustering.core";
  @Documented("Append index of the RAFT log")
  public static final String APPEND_INDEX = name(CAUSAL_CLUSTERING_PREFIX, "append_index");
  @Documented("Commit index of the RAFT log")
  public static final String COMMIT_INDEX = name(CAUSAL_CLUSTERING_PREFIX, "commit_index");
  @Documented("RAFT Term of this server")
  public static final String TERM = name(CAUSAL_CLUSTERING_PREFIX, "term");
  @Documented("Transaction retries")
  public static final String TX_RETRIES = name(CAUSAL_CLUSTERING_PREFIX, "tx_retries");
  @Documented("Is this server the leader?")
  public static final String IS_LEADER = name(CAUSAL_CLUSTERING_PREFIX, "is_leader");
  @Documented("In-flight cache total bytes")
  public static final String TOTAL_BYTES = name(CAUSAL_CLUSTERING_PREFIX, "in_flight_cache",
      "total_bytes");
  @Documented("In-flight cache max bytes")
  public static final String MAX_BYTES = name(CAUSAL_CLUSTERING_PREFIX, "in_flight_cache",
      "max_bytes");
  @Documented("In-flight cache element count")
  public static final String ELEMENT_COUNT = name(CAUSAL_CLUSTERING_PREFIX, "in_flight_cache",
      "element_count");
  @Documented("In-flight cache maximum elements")
  public static final String MAX_ELEMENTS = name(CAUSAL_CLUSTERING_PREFIX, "in_flight_cache",
      "max_elements");
  @Documented("In-flight cache hits")
  public static final String HITS = name(CAUSAL_CLUSTERING_PREFIX, "in_flight_cache", "hits");
  @Documented("In-flight cache misses")
  public static final String MISSES = name(CAUSAL_CLUSTERING_PREFIX, "in_flight_cache", "misses");
  @Documented("Delay between RAFT message receive and process")
  public static final String DELAY = name(CAUSAL_CLUSTERING_PREFIX, "message_processing_delay");
  @Documented("Timer for RAFT message processing")
  public static final String TIMER = name(CAUSAL_CLUSTERING_PREFIX, "message_processing_timer");
  @Documented("Raft replication new request count")
  public static final String REPLICATION_NEW = name(CAUSAL_CLUSTERING_PREFIX, "replication_new");
  @Documented("Raft replication attempt count")
  public static final String REPLICATION_ATTEMPT = name(CAUSAL_CLUSTERING_PREFIX,
      "replication_attempt");
  @Documented("Raft Replication success count")
  public static final String REPLICATION_SUCCESS = name(CAUSAL_CLUSTERING_PREFIX,
      "replication_success");
  @Documented("Raft Replication fail count")
  public static final String REPLICATION_FAIL = name(CAUSAL_CLUSTERING_PREFIX, "replication_fail");

  @Documented("Size of replicated data structures.")
  public static final String DISCOVERY_REPLICATED_DATA = name(CAUSAL_CLUSTERING_PREFIX,
      "discovery_replicated_data");
  @Documented("Raft Replication maybe count.")
  public static final String REPLICATION_MAYBE = name(CAUSAL_CLUSTERING_PREFIX,
      "replication_maybe");
  @Documented("Discovery cluster member size.")
  public static final String DISCOVERY_CLUSTER_MEMBERS = name(CAUSAL_CLUSTERING_PREFIX,
      "discovery_cluster_members");


  @Documented("Discovery cluster unreachable size.")
  public static final String DISCOVERY_CLUSTER_UNREACHABLE = name(CAUSAL_CLUSTERING_PREFIX,
      "discovery_cluster_unreachable");
  @Documented("Discovery cluster convergence.")
  public static final String DISCOVERY_CLUSTER_CONVERGED = name(CAUSAL_CLUSTERING_PREFIX,
      "discovery_cluster_converged");
  private static final String VISIBLE = "visible";
  private static final String INVISIBLE = "invisible";

  private final String metricsPrefix;
  private final RaftLogCommitIndexMetric raftLogCommitIndexMetric = new RaftLogCommitIndexMetric();
  private final RaftLogAppendIndexMetric raftLogAppendIndexMetric = new RaftLogAppendIndexMetric();
  private final RaftTermMetric raftTermMetric = new RaftTermMetric();
  private final TxPullRequestsMetric txPullRequestsMetric = new TxPullRequestsMetric();
  private final TxRetryMetric txRetryMetric = new TxRetryMetric();
  private final InFlightCacheMetric inFlightCacheMetric = new InFlightCacheMetric();
  private final RaftMessageProcessingMetric raftMessageProcessingMetric = RaftMessageProcessingMetric
      .create();
  private final ReplicationMetric replicationMetric = new ReplicationMetric();
  private final ReplicatedDataMetric discoveryReplicatedDataMetric = new ReplicatedDataMetric();
  private final ClusterSizeMetric discoveryClusterSizeMetric = new ClusterSizeMetric();
  //
  private final ReplicatedDataIdentifier[] replicatedDataIdentifiers = ReplicatedDataIdentifier
      .values();
  private final Monitors monitors;
  private final MetricRegistry registry;
  private final Supplier<CoreMetaData> coreMetaData;

  public CoreMetrics(String metricsPrefix, Monitors monitors, MetricRegistry registry,
      Supplier<CoreMetaData> coreMetaData) {
    this.metricsPrefix = metricsPrefix;
    this.monitors = monitors;
    this.registry = registry;
    this.coreMetaData = coreMetaData;
  }

  @Override
  public void start() {
    monitors.addMonitorListener(raftLogCommitIndexMetric);
    monitors.addMonitorListener(raftLogAppendIndexMetric);
    monitors.addMonitorListener(raftTermMetric);
    monitors.addMonitorListener(txPullRequestsMetric);
    monitors.addMonitorListener(txRetryMetric);
    monitors.addMonitorListener(inFlightCacheMetric);
    monitors.addMonitorListener(raftMessageProcessingMetric);
    monitors.addMonitorListener(replicationMetric);
    monitors.addMonitorListener(this.discoveryReplicatedDataMetric);
    monitors.addMonitorListener(this.discoveryClusterSizeMetric);

    registry.register(COMMIT_INDEX, (Gauge<Long>) raftLogCommitIndexMetric::commitIndex);
    registry.register(APPEND_INDEX, (Gauge<Long>) raftLogAppendIndexMetric::appendIndex);
    registry.register(TERM, (Gauge<Long>) raftTermMetric::term);
    registry.register(TX_RETRIES, (Gauge<Long>) txRetryMetric::transactionsRetries);
    registry.register(IS_LEADER, new LeaderGauge());
    registry.register(TOTAL_BYTES, (Gauge<Long>) inFlightCacheMetric::getTotalBytes);
    registry.register(HITS, (Gauge<Long>) inFlightCacheMetric::getHits);
    registry.register(MISSES, (Gauge<Long>) inFlightCacheMetric::getMisses);
    registry.register(MAX_BYTES, (Gauge<Long>) inFlightCacheMetric::getMaxBytes);
    registry.register(MAX_ELEMENTS, (Gauge<Long>) inFlightCacheMetric::getMaxElements);
    registry.register(ELEMENT_COUNT, (Gauge<Long>) inFlightCacheMetric::getElementCount);
    registry.register(DELAY, (Gauge<Long>) raftMessageProcessingMetric::delay);
    registry.register(TIMER, raftMessageProcessingMetric.timer());
    registry.register(REPLICATION_NEW, (Gauge<Long>) replicationMetric::newReplicationCount);
    registry.register(REPLICATION_ATTEMPT, (Gauge<Long>) replicationMetric::attemptCount);
    registry.register(REPLICATION_SUCCESS, (Gauge<Long>) replicationMetric::successCount);
    registry.register(REPLICATION_FAIL, (Gauge<Long>) replicationMetric::failCount);

    for (RaftMessages.Type type : RaftMessages.Type.values()) {
      registry.register(messageTimerName(type), raftMessageProcessingMetric.timer(type));
    }

    registry.register(REPLICATION_MAYBE, (Gauge<Long>) replicationMetric::maybeCount);
    registry.register(DISCOVERY_CLUSTER_CONVERGED, (Gauge) discoveryClusterSizeMetric::converged);
    registry.register(DISCOVERY_CLUSTER_MEMBERS, (Gauge) discoveryClusterSizeMetric::members);
    registry
        .register(DISCOVERY_CLUSTER_UNREACHABLE, (Gauge) discoveryClusterSizeMetric::unreachable);

    Arrays.stream(replicatedDataIdentifiers).forEach(replicatedDataIdentifier ->
    {
      registry.register(discoveryReplicatedDataName(replicatedDataIdentifier, VISIBLE),
          this.discoveryReplicatedDataMetric
              .getVisibleDataSize(replicatedDataIdentifier));
      registry.register(discoveryReplicatedDataName(replicatedDataIdentifier, INVISIBLE),
          this.discoveryReplicatedDataMetric
              .getInvisibleDataSize(replicatedDataIdentifier));
    });

  }

  private String discoveryReplicatedDataName(ReplicatedDataIdentifier identifier, String typeStr) {
    return name(DISCOVERY_REPLICATED_DATA, identifier.name().toLowerCase(), typeStr.toLowerCase());
  }

  @Override
  public void stop() {
    registry.remove(COMMIT_INDEX);
    registry.remove(APPEND_INDEX);
    registry.remove(TERM);
    registry.remove(TX_RETRIES);
    registry.remove(IS_LEADER);
    registry.remove(TOTAL_BYTES);
    registry.remove(HITS);
    registry.remove(MISSES);
    registry.remove(MAX_BYTES);
    registry.remove(MAX_ELEMENTS);
    registry.remove(ELEMENT_COUNT);
    registry.remove(DELAY);
    registry.remove(TIMER);
    registry.remove(REPLICATION_NEW);
    registry.remove(REPLICATION_ATTEMPT);
    registry.remove(REPLICATION_FAIL);
    registry.remove(REPLICATION_SUCCESS);

    registry.remove(REPLICATION_MAYBE);
    registry.remove(DISCOVERY_CLUSTER_CONVERGED);
    registry.remove(DISCOVERY_CLUSTER_MEMBERS);
    registry.remove(DISCOVERY_CLUSTER_UNREACHABLE);

    Arrays.stream(replicatedDataIdentifiers).forEach(replicatedDataIdentifier ->
    {
      registry.remove(discoveryReplicatedDataName(replicatedDataIdentifier, VISIBLE));
      registry.remove(discoveryReplicatedDataName(replicatedDataIdentifier, INVISIBLE));
    });

    for (RaftMessages.Type type : RaftMessages.Type.values()) {
      registry.remove(messageTimerName(type));
    }

    monitors.removeMonitorListener(raftLogCommitIndexMetric);
    monitors.removeMonitorListener(raftLogAppendIndexMetric);
    monitors.removeMonitorListener(raftTermMetric);
    monitors.removeMonitorListener(txPullRequestsMetric);
    monitors.removeMonitorListener(txRetryMetric);
    monitors.removeMonitorListener(inFlightCacheMetric);
    monitors.removeMonitorListener(raftMessageProcessingMetric);
    monitors.removeMonitorListener(replicationMetric);

    monitors.addMonitorListener(this.discoveryReplicatedDataMetric);
    monitors.addMonitorListener(this.discoveryClusterSizeMetric);
  }

  private String messageTimerName(RaftMessages.Type type) {
    return name(TIMER, type.name().toLowerCase());
  }

  private class LeaderGauge implements Gauge<Integer> {

    @Override
    public Integer getValue() {
      return coreMetaData.get().isLeader() ? 1 : 0;
    }
  }
}