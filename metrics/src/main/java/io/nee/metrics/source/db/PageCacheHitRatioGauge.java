/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.db;

import com.codahale.metrics.Clock;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.SlidingTimeWindowArrayReservoir;
import com.codahale.metrics.Snapshot;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import org.neo4j.internal.helpers.MathUtil;
import org.neo4j.io.pagecache.monitoring.PageCacheCounters;

class PageCacheHitRatioGauge implements Gauge<Double> {

  private final PageCacheCounters pageCacheCounters;
  private final SlidingTimeWindowArrayReservoir hitsWindow;
  private final SlidingTimeWindowArrayReservoir missesWindow;

  PageCacheHitRatioGauge(PageCacheCounters pageCacheCounters, Duration windowDuration,
      Clock clock) {
    long windowSeconds = windowDuration.getSeconds();
    this.hitsWindow = new SlidingTimeWindowArrayReservoir(windowSeconds, TimeUnit.SECONDS, clock);
    this.missesWindow = new SlidingTimeWindowArrayReservoir(windowSeconds, TimeUnit.SECONDS, clock);
    this.pageCacheCounters = pageCacheCounters;
  }

  PageCacheHitRatioGauge(PageCacheCounters pageCacheCounters) {
    this(pageCacheCounters, Duration.ofMinutes(1L), Clock.defaultClock());
  }

  public Double getValue() {
    this.hitsWindow.update(this.pageCacheCounters.hits());
    this.missesWindow.update(this.pageCacheCounters.faults());
    long hits = this.getHits();
    long misses = this.getMisses();
    return MathUtil.portion((double) hits, (double) misses);
  }

  private long getMisses() {
    Snapshot snapshot = this.missesWindow.getSnapshot();
    return Math.subtractExact(snapshot.getMax(), snapshot.getMin());
  }

  private long getHits() {
    Snapshot snapshot = this.hitsWindow.getSnapshot();
    return Math.subtractExact(snapshot.getMax(), snapshot.getMin());
  }
}
