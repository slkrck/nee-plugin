/*
 * Copyright (c) 2002-2018 "Neo4j,"
 * Neo4j Sweden AB [http://neo4j.com]
 *
 * This file is part of Neo4j Enterprise Edition. The included source
 * code can be redistributed and/or modified under the terms of the
 * GNU AFFERO GENERAL PUBLIC LICENSE Version 3
 * (http://www.fsf.org/licensing/licenses/agpl-3.0.html)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * Neo4j object code can be licensed independently from the source
 * under separate terms from the AGPL. Inquiries can be directed to:
 * licensing@neo4j.com
 *
 * More information is also available at:
 * https://neo4j.com/licensing/
 */
package io.nee.metrics.source.jvm;

import static com.codahale.metrics.MetricRegistry.name;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;

public class MemoryPoolMetrics extends JvmMetrics {

  public static final String MEMORY_POOL = name(JvmMetrics.VM_NAME_PREFIX, "memory.pool");
  private final MetricRegistry registry;
  private final String metricsPrefix;

  public MemoryPoolMetrics(String metricsPrefix, MetricRegistry registry) {
    this.metricsPrefix = metricsPrefix;
    this.registry = registry;
  }

  @Override
  public void start() {
    for (final MemoryPoolMXBean memPool : ManagementFactory.getMemoryPoolMXBeans()) {
      registry.register(globalMetricsName(name(MEMORY_POOL, prettifyName(memPool.getName()))),
          (Gauge<Long>) () -> memPool.getUsage().getUsed());
    }
  }

  @Override
  public void stop() {
    registry.removeMatching((name, metric) -> name.contains(MEMORY_POOL));
  }

  private String globalMetricsName(String metricsName) {
    return name(metricsPrefix, metricsName);
  }
}
