/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.causalclustering;

import io.nee.causalclustering.core.consensus.log.cache.InFlightCacheMonitor;
import java.util.concurrent.atomic.LongAdder;

public class InFlightCacheMetric implements InFlightCacheMonitor {

  private final LongAdder misses = new LongAdder();
  private final LongAdder hits = new LongAdder();
  private volatile long totalBytes;
  private volatile long maxBytes;
  private volatile int elementCount;
  private volatile int maxElements;

  public void miss() {
    this.misses.increment();
  }

  public void hit() {
    this.hits.increment();
  }

  public long getMisses() {
    return this.misses.sum();
  }

  public long getHits() {
    return this.hits.sum();
  }

  public long getMaxBytes() {
    return this.maxBytes;
  }

  public void setMaxBytes(long maxBytes) {
    this.maxBytes = maxBytes;
  }

  public long getTotalBytes() {
    return this.totalBytes;
  }

  public void setTotalBytes(long totalBytes) {
    this.totalBytes = totalBytes;
  }

  public long getMaxElements() {
    return this.maxElements;
  }

  public void setMaxElements(int maxElements) {
    this.maxElements = maxElements;
  }

  public long getElementCount() {
    return this.elementCount;
  }

  public void setElementCount(int elementCount) {
    this.elementCount = elementCount;
  }
}
