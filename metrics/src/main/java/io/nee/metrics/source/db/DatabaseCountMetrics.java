/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.source.db;

import static com.codahale.metrics.MetricRegistry.name;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.MetricRegistry;
import java.util.function.Supplier;
import org.neo4j.annotations.documented.Documented;
import org.neo4j.kernel.impl.store.stats.StoreEntityCounters;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;

@Documented(".Database data count metrics")
public class DatabaseCountMetrics extends LifecycleAdapter {

  private static final String COUNTS_PREFIX = "neo4j.count";
  @Documented("The total number of relationships in the database")
  public static final String COUNTS_RELATIONSHIP = name(COUNTS_PREFIX, "relationship");
  @Documented("The total number of nodes in the database")
  public static final String COUNTS_NODE = name(COUNTS_PREFIX, "node");

  private final String metricsPrefix;
  private final MetricRegistry registry;
  private final Supplier<StoreEntityCounters> countsSource;

  public DatabaseCountMetrics(String metricsPrefix, MetricRegistry registry,
      Supplier<StoreEntityCounters> countsSource) {
    this.metricsPrefix = metricsPrefix;
    this.registry = registry;
    this.countsSource = countsSource;
  }

  public void start() {

    this.registry.register(globalMetricsName(COUNTS_NODE), (Gauge<Long>) () ->
    {
      return (this.countsSource.get()).allNodesCountStore();
    });

    this.registry.register(globalMetricsName(COUNTS_RELATIONSHIP), (Gauge<Long>) () ->
    {
      return (countsSource.get()).allRelationshipsCountStore();
    });
  }

  public void stop() {
    this.registry.remove(globalMetricsName(COUNTS_NODE));
    this.registry.remove(globalMetricsName(COUNTS_RELATIONSHIP));
  }

  private String globalMetricsName(String metricsName) {
    return name(metricsPrefix, metricsName);
  }
}
