/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.global;

import com.codahale.metrics.MetricRegistry;
import io.nee.kernel.impl.enterprise.configuration.MetricsSettings;
import io.nee.metrics.source.db.BoltMetrics;
import io.nee.metrics.source.db.DatabaseOperationCountMetrics;
import io.nee.metrics.source.db.PageCacheMetrics;
import io.nee.metrics.source.jvm.FileDescriptorMetrics;
import io.nee.metrics.source.jvm.GCMetrics;
import io.nee.metrics.source.jvm.HeapMetrics;
import io.nee.metrics.source.jvm.MemoryBuffersMetrics;
import io.nee.metrics.source.jvm.MemoryPoolMetrics;
import io.nee.metrics.source.jvm.ThreadMetrics;
import io.nee.metrics.source.server.ServerMetrics;
import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.HttpConnector;
import org.neo4j.configuration.connectors.HttpsConnector;
import org.neo4j.kernel.extension.context.ExtensionContext;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.logging.internal.LogService;

public class GlobalMetricsExporter {

  private final MetricRegistry registry;
  private final LifeSupport life;
  private final Config config;
  private final LogService logService;
  private final ExtensionContext context;
  private final GlobalMetricsExtensionFactory.Dependencies dependencies;

  GlobalMetricsExporter(MetricRegistry registry, Config config, LogService logService,
      ExtensionContext context,
      GlobalMetricsExtensionFactory.Dependencies dependencies, LifeSupport life) {
    this.registry = registry;
    this.config = config;
    this.logService = logService;
    this.context = context;
    this.dependencies = dependencies;
    this.life = life;
  }

  public void export() {
    String globalMetricsPrefix = this.config.get(MetricsSettings.metricsPrefix);
    if (this.config.get(MetricsSettings.neoPageCacheEnabled)) {
      this.life.add(new PageCacheMetrics(globalMetricsPrefix, this.registry,
          this.dependencies.pageCacheCounters()));
    }

    if (this.config.get(MetricsSettings.jvmGcEnabled)) {
      this.life.add(new GCMetrics(globalMetricsPrefix, this.registry));
    }

    if (this.config.get(MetricsSettings.jvmHeapEnabled)) {
      this.life.add(new HeapMetrics(globalMetricsPrefix, this.registry));
    }

    if (this.config.get(MetricsSettings.jvmThreadsEnabled)) {
      this.life.add(new ThreadMetrics(globalMetricsPrefix, this.registry));
    }

    if (this.config.get(MetricsSettings.jvmMemoryEnabled)) {
      this.life.add(new MemoryPoolMetrics(globalMetricsPrefix, this.registry));
    }

    if (this.config.get(MetricsSettings.jvmBuffersEnabled)) {
      this.life.add(new MemoryBuffersMetrics(globalMetricsPrefix, this.registry));
    }

    if (this.config.get(MetricsSettings.jvmFileDescriptorsEnabled)) {
      this.life.add(new FileDescriptorMetrics(globalMetricsPrefix, this.registry));
    }

    if (this.config.get(MetricsSettings.boltMessagesEnabled)) {
      this.life
          .add(new BoltMetrics(globalMetricsPrefix, this.registry, this.dependencies.monitors()));
    }

    if (this.config.get(MetricsSettings.databaseOperationCountEnabled)) {
      this.life.add(new DatabaseOperationCountMetrics(globalMetricsPrefix, this.registry,
          this.dependencies.monitors()));
    }

    boolean httpOrHttpsEnabled =
        this.config.get(HttpConnector.enabled) || this.config.get(HttpsConnector.enabled);
    if (httpOrHttpsEnabled && this.config.get(MetricsSettings.neoServerEnabled)) {
      this.life.add(new ServerMetrics(globalMetricsPrefix, this.registry, this.logService,
          this.context.dependencySatisfier()));
    }
  }
}
