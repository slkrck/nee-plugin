/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.global;

import org.neo4j.configuration.Config;
import org.neo4j.configuration.connectors.ConnectorPortRegister;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.io.pagecache.monitoring.PageCacheCounters;
import org.neo4j.kernel.extension.ExtensionFactory;
import org.neo4j.kernel.extension.context.ExtensionContext;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.logging.internal.LogService;
import org.neo4j.monitoring.Monitors;
import org.neo4j.scheduler.JobScheduler;

public class GlobalMetricsExtensionFactory extends
    ExtensionFactory<GlobalMetricsExtensionFactory.Dependencies> {

  public GlobalMetricsExtensionFactory() {
    super("globalMetrics");
  }

  public Lifecycle newInstance(ExtensionContext context,
      GlobalMetricsExtensionFactory.Dependencies dependencies) {
    return new GlobalMetricsExtension(context, dependencies);
  }

  public interface Dependencies {

    Monitors monitors();

    PageCacheCounters pageCacheCounters();

    Config configuration();

    LogService logService();

    FileSystemAbstraction fileSystemAbstraction();

    JobScheduler scheduler();

    ConnectorPortRegister portRegister();
  }
}
