/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.global;

import com.codahale.metrics.MetricRegistry;
import io.nee.kernel.impl.enterprise.configuration.MetricsSettings;
import io.nee.metrics.output.CompositeEventReporter;
import io.nee.metrics.output.EventReporter;
import io.nee.metrics.output.EventReporterBuilder;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.extension.context.ExtensionContext;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.kernel.lifecycle.Lifecycle;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;

public class GlobalMetricsExtension implements Lifecycle, MetricsManager {

  private final LifeSupport life = new LifeSupport();
  private final Log logger;
  private final CompositeEventReporter reporter;
  private final MetricRegistry registry;
  private final ExtensionContext context;
  private final GlobalMetricsExtensionFactory.Dependencies dependencies;
  private boolean configured;

  public GlobalMetricsExtension(ExtensionContext context,
      GlobalMetricsExtensionFactory.Dependencies dependencies) {
    LogService logService = dependencies.logService();
    this.context = context;
    this.dependencies = dependencies;
    this.logger = logService.getUserLog(this.getClass());
    this.registry = new MetricRegistry();
    this.reporter =
        (new EventReporterBuilder(dependencies.configuration(), this.registry, this.logger, context,
            this.life, dependencies.fileSystemAbstraction(),
            dependencies.scheduler(), dependencies.portRegister())).build();
  }

  public void init() {
    this.configured = !this.reporter.isEmpty();
    LogService logService = this.dependencies.logService();
    Config config = this.dependencies.configuration();
    if (config.get(MetricsSettings.metricsEnabled)) {
      if (!this.configured) {
        this.logger.warn(
            "Metrics extension reporting is not configured. Please configure one of the available exporting options to be able to use metrics. Metrics extension is disabled.");
      } else {
        (new GlobalMetricsExporter(this.registry, config, logService, this.context,
            this.dependencies, this.life)).export();
        this.life.init();
      }
    }
  }

  public void start() {
    this.life.start();
  }

  public void stop() {
  }

  public void shutdown() {
    this.life.shutdown();
  }

  public EventReporter getReporter() {
    return this.reporter;
  }

  public MetricRegistry getRegistry() {
    return this.registry;
  }

  public boolean isConfigured() {
    return this.configured;
  }
}
