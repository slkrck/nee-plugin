/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.database;

import com.codahale.metrics.MetricRegistry;
import io.nee.kernel.impl.enterprise.configuration.MetricsSettings;
import io.nee.metrics.output.EventReporter;
import io.nee.metrics.source.causalclustering.CatchUpMetrics;
import io.nee.metrics.source.causalclustering.CoreMetrics;
import io.nee.metrics.source.causalclustering.ReadReplicaMetrics;
import io.nee.metrics.source.db.CheckPointingMetrics;
import io.nee.metrics.source.db.CypherMetrics;
import io.nee.metrics.source.db.DatabaseCountMetrics;
import io.nee.metrics.source.db.EntityCountMetrics;
import io.nee.metrics.source.db.StoreSizeMetrics;
import io.nee.metrics.source.db.TransactionLogsMetrics;
import io.nee.metrics.source.db.TransactionMetrics;
import org.neo4j.common.Edition;
import org.neo4j.configuration.Config;
import org.neo4j.kernel.extension.context.ExtensionContext;
import org.neo4j.kernel.impl.factory.OperationalMode;
import org.neo4j.kernel.lifecycle.LifeSupport;

public class DatabaseMetricsExporter {

  private final MetricRegistry registry;
  private final LifeSupport life;
  private final EventReporter reporter;
  private final Config config;
  private final ExtensionContext context;
  private final DatabaseMetricsExtensionFactory.Dependencies dependencies;

  DatabaseMetricsExporter(MetricRegistry registry, EventReporter reporter, Config config,
      ExtensionContext context,
      DatabaseMetricsExtensionFactory.Dependencies dependencies, LifeSupport life) {
    this.registry = registry;
    this.reporter = reporter;
    this.config = config;
    this.context = context;
    this.dependencies = dependencies;
    this.life = life;
  }

  public void export() {
    String metricsPrefix = this.databaseMetricsPrefix();
    if (this.config.get(MetricsSettings.neoTxEnabled)) {
      this.life.add(new TransactionMetrics(metricsPrefix, this.registry,
          this.dependencies.transactionIdStoreSupplier(),
          this.dependencies.transactionCounters()));
    }

    if (this.config.get(MetricsSettings.neoCheckPointingEnabled)) {
      this.life.add(new CheckPointingMetrics(metricsPrefix, this.registry,
          this.dependencies.checkpointCounters()));
    }

    if (this.config.get(MetricsSettings.neoTransactionLogsEnabled)) {
      this.life.add(new TransactionLogsMetrics(metricsPrefix, this.registry,
          this.dependencies.transactionLogCounters()));
    }

    if (this.config.get(MetricsSettings.neoCountsEnabled)
        && this.context.databaseInfo().edition != Edition.COMMUNITY &&
        this.context.databaseInfo().edition != Edition.UNKNOWN) {
      this.life.add(new EntityCountMetrics(metricsPrefix, this.registry,
          this.dependencies.storeEntityCounters()));
    }

    if (this.config.get(MetricsSettings.databaseCountsEnabled)
        && this.context.databaseInfo().edition != Edition.COMMUNITY &&
        this.context.databaseInfo().edition != Edition.UNKNOWN) {
      this.life.add(new DatabaseCountMetrics(metricsPrefix, this.registry,
          this.dependencies.storeEntityCounters()));
    }

    if (this.config.get(MetricsSettings.neoStoreSizeEnabled)) {
      this.life.add(
          new StoreSizeMetrics(metricsPrefix, this.registry, this.dependencies.scheduler(),
              this.dependencies.fileSystem(),
              this.dependencies.database().getDatabaseLayout()));
    }

    if (this.config.get(MetricsSettings.cypherPlanningEnabled)) {
      this.life.add(new CypherMetrics(metricsPrefix, this.registry, this.dependencies.monitors()));
    }

    if (this.config.get(MetricsSettings.causalClusteringEnabled)) {
      OperationalMode mode = this.context.databaseInfo().operationalMode;
      if (mode == OperationalMode.CORE) {
        this.life.add(
            new CoreMetrics(metricsPrefix, this.dependencies.clusterMonitors(), this.registry,
                this.dependencies.coreMetadataSupplier()));
        this.life
            .add(new CatchUpMetrics(metricsPrefix, this.dependencies.monitors(), this.registry));
      } else if (mode == OperationalMode.READ_REPLICA) {
        this.life.add(new ReadReplicaMetrics(metricsPrefix, this.dependencies.clusterMonitors(),
            this.registry));
        this.life
            .add(new CatchUpMetrics(metricsPrefix, this.dependencies.monitors(), this.registry));
      }
    }
  }

  private String databaseMetricsPrefix() {
    String n10000 = this.config.get(MetricsSettings.metricsPrefix);
    return n10000 + "." + this.dependencies.database().getNamedDatabaseId().name();
  }
}
