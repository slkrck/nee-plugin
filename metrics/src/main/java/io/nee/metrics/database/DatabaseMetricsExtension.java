/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.database;

import com.codahale.metrics.MetricRegistry;
import io.nee.metrics.global.MetricsManager;
import io.nee.metrics.output.EventReporter;
import java.util.Optional;
import org.neo4j.exceptions.UnsatisfiedDependencyException;
import org.neo4j.kernel.extension.context.ExtensionContext;
import org.neo4j.kernel.lifecycle.LifeSupport;
import org.neo4j.kernel.lifecycle.Lifecycle;

public class DatabaseMetricsExtension implements Lifecycle {

  private final LifeSupport life = new LifeSupport();
  private final ExtensionContext context;
  private final DatabaseMetricsExtensionFactory.Dependencies dependencies;

  public DatabaseMetricsExtension(ExtensionContext context,
      DatabaseMetricsExtensionFactory.Dependencies dependencies) {
    this.context = context;
    this.dependencies = dependencies;
  }

  public void init() {
    Optional<MetricsManager> optionalManager = this.getMetricsManager();
    optionalManager.ifPresent((metricsManager) ->
    {
      if (metricsManager.isConfigured()) {
        MetricRegistry metricRegistry = metricsManager.getRegistry();
        EventReporter eventReporter = metricsManager.getReporter();
        (new DatabaseMetricsExporter(metricRegistry, eventReporter,
            this.dependencies.configuration(), this.context,
            this.dependencies,
            this.life)).export();
      }
    });
    this.life.init();
  }

  public void start() {
    this.life.start();
  }

  public void stop() {
    this.life.stop();
  }

  public void shutdown() {
    this.life.shutdown();
  }

  private Optional<MetricsManager> getMetricsManager() {
    try {
      return Optional.of(this.dependencies.metricsManager());
    } catch (UnsatisfiedDependencyException n2) {
      return Optional.empty();
    }
  }
}
