/*
 * NEE Plugin
 *
 * Copyright: In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package io.nee.metrics.diagnostics;

import io.nee.kernel.impl.enterprise.configuration.MetricsSettings;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.neo4j.configuration.Config;
import org.neo4j.io.fs.FileSystemAbstraction;
import org.neo4j.kernel.diagnostics.DiagnosticsOfflineReportProvider;
import org.neo4j.kernel.diagnostics.DiagnosticsReportSource;
import org.neo4j.kernel.diagnostics.DiagnosticsReportSources;

public class MetricsDiagnosticsOfflineReportProvider extends DiagnosticsOfflineReportProvider {

  private FileSystemAbstraction fs;
  private Config config;

  public MetricsDiagnosticsOfflineReportProvider() {
    super("metrics");
  }

  public void init(FileSystemAbstraction fs, String defaultDatabaseName, Config config,
      File storeDirectory) {
    this.fs = fs;
    this.config = config;
  }

  protected List<DiagnosticsReportSource> provideSources(Set<String> classifiers) {
    File metricsDirectory = this.config.get(MetricsSettings.csvPath).toFile();
    if (this.fs.fileExists(metricsDirectory) && this.fs.isDirectory(metricsDirectory)) {
      List<DiagnosticsReportSource> files = new ArrayList();
      File[] n4 = this.fs.listFiles(metricsDirectory);
      int n5 = n4.length;

      for (int n6 = 0; n6 < n5; ++n6) {
        File file = n4[n6];
        files.add(DiagnosticsReportSources
            .newDiagnosticsFile("metrics/" + file.getName(), this.fs, file));
      }

      return files;
    } else {
      return Collections.emptyList();
    }
  }
}
